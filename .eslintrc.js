module.exports = {
  plugins: ['vue'],
  extends: ['plugin:vue/essential', '@vue/prettier'],
  env: {
    node: true,
  }
  rules: {
    // Semicolons: always require
    semi: ['error', 'always'],

    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

    // don't warn on variables starting with an underscore
    'no-unused-vars': [
      'warn',
      {
        argsIgnorePattern: '^_',
      },
    ],

    // don't warn about useless escapes in regexes
    'no-useless-escape': 'off',

    quotes: [
      'warn',
      'single',
      {
        allowTemplateLiterals: true,
      },
    ],
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 10,
    ecmaFeatures: {
      impliedStrict: true,
    },
  },
};
