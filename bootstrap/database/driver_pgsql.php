<?php

/**
 * DB_driver implementation for PostgreSQL.
 * @see DB_driver
 * @package database
 */
class DB_driver_pgsql implements DB_driver {
    protected $conn;
    protected $parent;

    protected $host;
    protected $port;
    protected $dbname;
    protected $user;
    protected $password;
    protected $options;

    public function __construct($parent, $options) {
        $this->parent = $parent;

        $this->host     = isset($options['host'])       ? $options['host']      : 'localhost';
        $this->port     = isset($options['port'])       ? $options['port']      : null;
        $this->dbname   = isset($options['dbname'])     ? $options['dbname']    : null;
        $this->user     = isset($options['user'])       ? $options['user']      : null;
        $this->password = isset($options['password'])   ? $options['password']  : null;
        $this->options  = isset($options['options'])    ? $options['options']   : null;
        $this->sslmode  = isset($options['sslmode'])    ? $options['sslmode']   : null;
    }

    public function disconnect() {
        if (is_resource($this->conn)) {
            pg_close($this->conn);
        }
        $this->conn = null;
    }

    public function get() {
        if (!isset($this->conn)) {
            $conn_string =
                'host='    . $this->host.
                ' dbname=' . $this->dbname.
                ' user='   . $this->user.
                ' password='.$this->password.
                (isset($this->port)    ? (' port='.$this->port)            : '').
                (isset($this->options) ? (' options=\''.$this->port.'\'')  : '').
                (isset($this->sslmode) ? (' sslmode=\''.$this->sslmode.'\'')  : '')
                ;

            // ignore errors since we're going to check those anyways
            ob_start();
            $this->conn = pg_connect($conn_string);
            $conn_error_string = trim(str_replace('</b>','',str_replace('<b>','',str_replace('<br>','', ob_get_clean()))));

            preg_match('/server:\s+(.*?)\sin\s(C:|\\\\)/i', $conn_error_string, $conn_error);
            if (isset($conn_error) && isset($conn_error[1])) {
                $conn_error = $conn_error[1];
            } else {
                $conn_error = '';
            }

            $conn_status = pg_connection_status($this->conn);

            if ($this->conn == false || $conn_status !== PGSQL_CONNECTION_OK) {
                if (is_resource($this->conn)) {
                    pg_close($this->conn);
                }
                header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
                die('Unable to connect to database ' . $this->dbname . ' -- ' . $conn_error);
                //return $this->parent->nonSqlError('Unable to connect to database');
            }
        }
        return $this->conn;
    }

    public function querybuilder($type, $args) {
        $type = strtolower($type);

        switch ($type) {
            // ----- DELETE
            case 'delete':
                return array_merge(array(
                        'DELETE FROM '.$args['table'].' WHERE ' . $args['where'],
                    ), $args['whereData']);
            // ----- UPDATE
            case 'update':
                return array_merge(array(
                        $this->parent->paramChars("UPDATE %b SET %hc WHERE ") . $args['where'],
                        $args['table'],
                        $args['params'],
                    ), $args['whereData']);
            // ----- INSERT UPDATE
            case 'insertupdate_assoc':
                $constraint = $this->determine_constraint($this->pg_create_constraint($args['table']), $args['keys']);
                return array(
                        $this->parent->paramChars("INSERT INTO %b %lb VALUES ".$args['var'].
                            " ON CONFLICT %lb DO UPDATE SET %hc"),
                        $args['table'],      // %b
                        $args['keys'],       // %lb
                        $args['values'],     // $var
                        $constraint,         // %lb
                        $args['updateData']  // %hc
                    );
            case 'insertupdate':
                $constraint = $this->determine_constraint($this->pg_create_constraint($args['table']), $args['keys']);
                return array_merge(array(
                        $this->parent->paramChars("INSERT INTO %b %lb VALUES ".$args['var'].
                            " ON CONFLICT %lb DO UPDATE SET ") . $args['update_str'],
                        $args['table'],      // %b
                        $args['keys'],       // %lb
                        $args['values'],     // $var
                        $constraint,         // %lb
                    ), $args['updateData']);

            // ----- INSERT
            case 'insertorreplace':
                return array(
                        $this->parent->paramChars("%l INTO %b %lb VALUES " . $args['var']),
                        $args['which'],      // %l
                        $args['table'],      // %b
                        $args['keys'],       // %lb
                        $args['values'],     // $var
                    );

            // ----- QUERY MODIFICATION
            case 'insertid_modquery':
                $primary_key = $this->pg_get_primary($args['table']);
                if (!empty($primary_key)) {
                    if (isset($primary_key[0]) && is_array($primary_key[0])) {
                        $args['params'][0] .= ' RETURNING (';
                        for ($i = 0, $len = count($primary_key); $i < $len; $i++) {
                            $args['params'][0] .= $primary_key[$i]['name'];
                            if ($i != $len-1) {
                                $args['params'][0] .= ', ';
                            }
                        }
                        $args['params'][0] .= ')';
                    } else {
                        $args['params'][0] .= ' RETURNING ' . $primary_key['name'];
                    }
                }
                return $args['params'];

            case 'insertignore_modquery':
                $args['params'][0] .= ' ON CONFLICT DO NOTHING';
                return $args['params'];

            case 'replace_modwhich':
                // remove row if exists
                $table = $args['table'];
                $constraints = $this->pg_create_constraint($table);

                foreach ($constraints as $constraint) {

                    // create WHERE part
                    $delete_string = '';
                    foreach ($constraint as $c) {
                        $delete_string .= "{$c}=%? AND ";
                        if (!in_array($c, $args['keys'])) {
                            continue 2;
                        }
                    }
                    $delete_string = substr($delete_string, 0, -strlen(' AND '));

                    // replace one row
                    if (array_values($args['data']) !== $args['data']) {
                        $values = [];

                        foreach ($constraint as $c) {
                            $values[] = $args['data'][$c];
                        }

                        $delete_result = $this->parent->delete($table, $delete_string, $values);
                    // replace multiple rows
                    } else {
                        foreach ($args['data'] as $data) {
                            $values = [];

                            foreach ($constraint as $c) {
                                $values[] = $data[$c];
                            }

                            $delete_result = $this->parent->delete($table, $delete_string, $values);
                        }
                    }
                }

                return 'INSERT';
            case 'insert_modwhich':
                return 'INSERT';

            case 'begin':
                if ($args['nested']) {
                    return 'SAVEPOINT ' . $args['savepoint'];
                } else {
                    return 'BEGIN';
                }
            case 'rollback':
                if ($args['nested']) {
                    return 'ROLLBACK TO SAVEPOINT ' . $args['savepoint'];
                } else {
                    return 'ROLLBACK';
                }
            case 'commit':
                if ($args['nested']) {
                    return 'RELEASE SAVEPOINT ' . $args['savepoint'];
                } else {
                    return 'COMMIT';
                }
        }
    }

    public function handleResult($type, $res, $query = null, $args = null) {
        $db = $this->get();
        $type = strtolower($type);
        switch ($type) {
            case 'insertid':
                // ----- BEGIN EXPLICIT ID INSERT UPDATE-CHECK
                $table = $args['table'];
                $ai_col  = $this->pg_get_nextval_colname($table);

                if ($ai_col == null) {
                    return null;
                }

                // check if AI (autoincrement) column is in list of things explicitly set
                if (in_array($ai_col, $args['keys'])) {
                    $nextval = $this->pg_get_nextval($table); // get sequence name

                    $res = pg_query($db, "SELECT setval('".$nextval."', (SELECT MAX(".$ai_col.") from " .
                        $this->formatTableName($table)."));");
                    if ($res == false) {
                        return $this->parent->sqlError('failed to manually set sequence value after explicitly provided value');
                    }
                }
                // ----- END EXPLICIT ID INSERT UPDATE-CHECK

                $insert_row = pg_fetch_row($res);
                $insert_id = $insert_row[0];
                return $insert_id;

            case 'affected_rows':
                return pg_affected_rows($res);
            case 'num_rows':
                return pg_num_rows($res);
            case 'executequery':
                $state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);

                if ($state == 0) {
                    return true;
                } else {
                    $error = pg_result_error($res);
                    $errno = $state;

                    return $this->parent->sqlError($error, $query, $errno);
                }
        }
    }

    // should return an array of associative arrays
    public function createQueryResult($res, $row_type, $column = null) {
        $result = [];

        if (isset($column)) {
            $result[$column] = [];
        }

        $ilen = pg_num_rows($res);
        $jlen = pg_num_fields($res);

        for ($i = 0; $i < $ilen; $i++) { // row
            if (!isset($column)) {
                $result[$i] = [];
            }
            for ($j = 0; $j < $jlen; $j++) { // column
                if (isset($column) && is_int($column) && $j !== $column) {
                    continue;
                }

                $fieldname  = pg_field_name($res, $j);

                if (isset($column) && is_string($column) && $fieldname != $column) {
                    continue;
                }

                $fieldtype  = pg_field_type($res, $j);
                $value      = pg_fetch_result($res, $i, $j);
                $is_null    = pg_field_is_null($res, $i, $j);

                $value = $is_null ? null : $this->pg_typecast($fieldtype, $value);

                $result_insert = isset($column) ? $column : $i;

                if ($row_type == 'assoc') {
                    $result[$result_insert][$fieldname] = $value;
                } else if ($row_type == 'list') {
                    $result[$result_insert][] = $value;
                }
            }
        }

        return $result;
    }

    public function executeQuery($query) {
        $db = $this->get();

        if ($db == null || $db == false) {
            return $this->parent->sqlError('queryHelper -- database not available');
        }

        if (!pg_connection_busy($db)) {
            pg_send_query($db, $query);
            $res = pg_get_result($db);
            return $res;
        } else {
            return $this->parent->sqlError('query error -- database connection is busy');
        }
    }

    public function escape($str) {
        return "'" . pg_escape_string(strval($str)) . "'";
    }

    public function formatTableName($table) {
        $db = $this->get();
        $table = trim($table, '`');

        return pg_escape_identifier($db, $table);
    }


    public function tableList() {
        $db = $this->get();
        $res = pg_query($db, 'SELECT * FROM pg_catalog.pg_tables');
        $result = [];
        while ($row = pg_fetch_assoc($res)) {
            $result[] = $row['tablename'];
        }
        return $result;
    }
    public function columnList($tablename, $types = false) {
        static $_cache_types = [];
        static $_cache_notypes = [];

        if ($types) {
            if (array_key_exists($tablename, $_cache_types)) {
                return $_cache_types[$tablename];
            }
        } else {
            if (array_key_exists($tablename, $_cache_notypes)) {
                return $_cache_notypes[$tablename];
            }
        }


        $db = $this->get();
        $query = <<<EOD
            SELECT attrelid::regclass, attnum, attname, format_type(atttypid, atttypmod) AS data_type
                FROM   pg_attribute
            WHERE  attrelid = quote_ident('{$tablename}')::regclass
                AND    attnum > 0
                AND    NOT attisdropped
            ORDER  BY attnum;
EOD;
        $res = pg_query($db, $query);
        $result = [];
        while ($row = pg_fetch_assoc($res)) {
            if ($types) {
                $result[$row['attname']] = $row['data_type'];
            } else {
                $result[] = $row['attname'];
            }
        }

        if ($types) {
            return $_cache_types[$tablename] = $result;
        } else {
            return $_cache_notypes[$tablename] = $result;
        }
    }

    public function checkUpdateData($tablename, $updateData) {
        if (array_values($updateData) !== $updateData) {
            $field_data = $this->columnList($tablename, true);

            $new_data = [];
            foreach ($updateData as $key => $value) {
                $new_data[$key] = $this->pg_untypecast($field_data[$key], $value);
            }
            return $new_data;
        } else {
            if (isset($updateData[0]) && is_array($updateData[0])) {
                $field_data = $this->columnList($tablename, true);

                $new_updateData = [];

                foreach ($updateData as $data) {
                    $new_data = [];
                    foreach ($data as $key => $value) {
                        $new_data[$key] = $this->pg_untypecast($field_data[$key], $value);
                    }
                    $new_updateData[] = $new_data;
                }

                return $new_updateData;
            }

            return $updateData;
        }
    }

    public function castBoolean($value) {
        return $value ? 'true' : 'false';
    }

    // ----------------------------------------------------------------------------------------------------
    // NON-INTERFACE FUNCTIONS:

    // returns an array of primary keys and unique indices column names
    public function pg_create_constraint($tablename) {
        // cache the results because the primary / unique keys
        // are unlikely to change within one http request
        static $_cache = array();

        if (array_key_exists($tablename, $_cache)) {
            return $_cache[$tablename];
        }

        $db = $this->get();

        $query = <<<EOD
            SELECT conname, contype, conkey
            FROM pg_constraint
            WHERE conrelid =
                (SELECT oid
                FROM pg_class
                WHERE relname LIKE '{$tablename}');
EOD;
        $res = pg_query($db, $query);
        if ($res == false) {
            return false;
        }

        $columns = [];
        foreach ($this->pg_defaultvalues($tablename) as $row) {
            $columns[] = $row['column_name'];
        }

        $constraints = [];
        while ($row = pg_fetch_array($res)) {
            // [0] -> conname (user-defined display name, ergo useless)
            // [1] -> contype ('u': unique", 'p': primary, 'f': foreign)
            // [2] -> conkey  (1-indexed array of columns in the format: `{#,#,...}`)
            if ($row[1] === 'f') continue; // skip foreign keys

            $constraints[] = array_reduce(
                explode(',', substr($row[2], 1, -1)),
                function($acc, $col_i) use ($columns) {
                    $acc[] = $columns[intval($col_i) - 1]; // subtract 1 b/c "conkey" is 1-indexed
                    return $acc;
                }, []
            );
        }

        $_cache[$tablename] = $constraints;

        return $constraints;
    }

    public function determine_constraint($constraints, $keys) {
        foreach ($constraints as $constraint) {
            if (count(array_intersect($constraint, $keys)) === count($constraint)) {
                return $constraint;
            }
        }
        return $this->parent->from($constraints, 0);
    }

    /* returns the primary key column for a table as
    array('name' => '<col_name>', 'type' => '<col_type') */
    public function pg_get_primary($tablename) {
        static $_cache = array();

        if (array_key_exists($tablename, $_cache)) {
            return $_cache[$tablename];
        }

        $db = $this->get();

        $query = <<<EOD
            SELECT a.attname, format_type(a.atttypid, a.atttypmod) AS data_type
            FROM   pg_index i
            JOIN   pg_attribute a ON a.attrelid = i.indrelid
                                 AND a.attnum = ANY(i.indkey)
            WHERE  i.indrelid = quote_ident('{$tablename}')::regclass
            AND    i.indisprimary;
EOD;
        $result = pg_query($db, $query);
        if ($result == false) {
            return false;
        }

        $real_result = [];
        while ($row = pg_fetch_assoc($result)) {
            $real_result[] = array('name' => $row['attname'], 'type' => $row['data_type']);
        }

        if (count($real_result) == 1) {
            $real_result = $real_result[0];
        }

        return $_cache[$tablename] = $real_result;
    }

    public function pg_typecast($pg_type, $pg_value) {
        $db = $this->get();

        $pg_type = trim(explode('(', $pg_type)[0]);

        switch ($pg_type) {
            case 'smallint':
                return intval($pg_value);
            case 'int2':
            case 'int4':
            case 'int8':
            case 'integer':
                return intval($pg_value);
            case 'bigint':
                return intval($pg_value);
            case 'bool':
            case 'boolean':
                return strval($pg_value) == 't';
            case 'numeric':
                return intval($pg_value);
            case 'float4':
            case 'float8':
            case 'real':
                return doubleval($pg_value);
            case 'double precision':
                return doubleval($pg_value);
            case 'money':
                return doubleval($pg_value);

            case 'date':
                if (is_int($pg_value)) return $pg_value;
                return intval(pg_fetch_array(pg_query($db, "SELECT EXTRACT(EPOCH FROM TIMESTAMP ".$this->escape($pg_value).")"))[0]);
            case 'time':
            case 'time without time zone':
                if (is_int($pg_value)) return $pg_value;
                return intval(pg_fetch_array(pg_query($db, "SELECT to_seconds(".$this->escape($pg_value).")"))[0]);
            case 'timetz':
            case 'time with time zone':
                if (is_int($pg_value)) return $pg_value;
                return intval(pg_fetch_array(pg_query($db, "SELECT to_seconds(".$this->escape($pg_value).")"))[0]);
            case 'timestamp':
            case 'timestamp without time zone':
                if (is_int($pg_value)) return $pg_value;
                return intval(pg_fetch_array(pg_query($db, "SELECT EXTRACT(EPOCH FROM TIMESTAMP ".$this->escape($pg_value).")"))[0]);
            case 'timestamptz':
            case 'timestamp with time zone':
                if (is_int($pg_value)) return $pg_value;
                return intval(pg_fetch_array(pg_query($db, "SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE ".$this->escape($pg_value).")"))[0]);
            case 'daterange':
            case 'interval':
                if (is_int($pg_value)) return $pg_value;
                return intval(pg_fetch_array(pg_query($db, "SELECT EXTRACT(EPOCH FROM INTERVAL ".$this->escape($pg_value).")"))[0]);

            case 'character':
            case 'char':
                return strval($pg_value);
            case 'character varying':
            case 'varchar':
                return strval($pg_value);
            case 'citext':
            case 'text':
                return strval($pg_value);
            case 'tsquery':
                return strval($pg_value);
            case 'tsvector':
                return strval($pg_value);
            case 'uuid':
                return strval($pg_value);
            case 'xml':
                return strval($pg_value);
            case 'json':
                return strval($pg_value);

            case 'bit':
                return $pg_value;
            case 'varbit':
            case 'bit varying':
                return $pg_value;
            case 'bytea':
                return $pg_value;

            case 'cidr':
                return $pg_value;
            case 'inet':
                return $pg_value;
            case 'macaddr':
                return $pg_value;
            case 'txid_snapshot':
                return $pg_value;

            case 'box':
                return $pg_value;
            case 'circle':
                return $pg_value;
            case 'line':
                return $pg_value;
            case 'lseg':
                return $pg_value;
            case 'path':
                return $pg_value;
            case 'point':
                return $pg_value;
            case 'polygon':
                return $pg_value;
            default:
                return $pg_value; // unknown type, let it go through - could be dynamically created column
                //return $this->parent->nonSqlError('unimplemented pg type ('.$pg_type.') for typecast');
        }
    }

    public function pg_untypecast($pg_type, $pg_value) {
        $db = $this->get();

        $pg_type = trim(explode('(', $pg_type)[0]);

        switch ($pg_type) {
            case 'smallint':
                return intval($pg_value);
            case 'int2':
            case 'int4':
            case 'int8':
            case 'integer':
                return intval($pg_value);
            case 'bigint':
                return intval($pg_value);
            case 'bool':
            case 'boolean':
                return ($pg_value ? 'true' : 'false');
            case 'numeric':
                return intval($pg_value);
            case 'float4':
            case 'float8':
            case 'real':
                return doubleval($pg_value);
            case 'double precision':
                return doubleval($pg_value);
            case 'money':
                return doubleval($pg_value);

            case 'date':
                if (is_int($pg_value) || is_double($pg_value) || is_float($pg_value)) {
                    return strval(pg_fetch_array(pg_query($db, "SELECT to_timestamp(".intval($pg_value).")::date"))[0]);
                }
                return $pg_value;
            case 'time':
            case 'time without time zone':
                if (is_int($pg_value) || is_double($pg_value) || is_float($pg_value)) {
                    return strval(pg_fetch_array(pg_query($db, "SELECT TO_CHAR('".intval($pg_value)." second'::interval, 'HH24:MI:SS.US')"))[0]);
                }
                return $pg_value;
            case 'timetz':
            case 'time with time zone':
                if (is_int($pg_value) || is_double($pg_value) || is_float($pg_value)) {
                    return strval(pg_fetch_array(pg_query($db, "SELECT TO_CHAR('".intval($pg_value)." second'::interval, 'HH24:MI:SS.US')"))[0]);
                }
                return $pg_value;
            case 'timestamp':
            case 'timestamp without time zone':
                if (is_int($pg_value) || is_double($pg_value) || is_float($pg_value)) {
                    return strval(pg_fetch_array(pg_query($db, "SELECT to_timestamp(".intval($pg_value).")"))[0]);
                }
                return $pg_value;
            case 'timestamptz':
            case 'timestamp with time zone':
                if (is_int($pg_value) || is_double($pg_value) || is_float($pg_value)) {
                    return strval(pg_fetch_array(pg_query($db, "SELECT to_timestamp(".intval($pg_value).")"))[0]);
                }
                return $pg_value;
            case 'daterange':
            case 'interval':
                if (is_int($pg_value) || is_double($pg_value) || is_float($pg_value)) {
                    return strval(pg_fetch_array(pg_query($db, "SELECT TO_CHAR('".intval($pg_value)." second'::interval, 'HH24:MI:SS')"))[0]);
                }
                return $pg_value;

            case 'character':
            case 'char':
                return strval($pg_value);
            case 'character varying':
            case 'varchar':
                return strval($pg_value);
            case 'citext':
            case 'text':
                return strval($pg_value);
            case 'tsquery':
                return strval($pg_value);
            case 'tsvector':
                return strval($pg_value);
            case 'uuid':
                return strval($pg_value);
            case 'xml':
                return strval($pg_value);
            case 'json':
                return strval($pg_value);

            case 'bit':
                return $pg_value;
            case 'varbit':
            case 'bit varying':
                return $pg_value;
            case 'bytea':
                return $pg_value;

            case 'cidr':
                return $pg_value;
            case 'inet':
                return $pg_value;
            case 'macaddr':
                return $pg_value;
            case 'txid_snapshot':
                return $pg_value;

            case 'box':
                return $pg_value;
            case 'circle':
                return $pg_value;
            case 'line':
                return $pg_value;
            case 'lseg':
                return $pg_value;
            case 'path':
                return $pg_value;
            case 'point':
                return $pg_value;
            case 'polygon':
                return $pg_value;
            default:
                return $this->parent->nonSqlError('unimplemented pg type ('.$pg_type.') for typecast');
        }
    }

    public function pg_defaultvalues($tablename) {
        static $_cache = array();
        if (array_key_exists($tablename, $_cache)) {
            return $_cache[$tablename];
        }

        $db = $this->get();
        $query = <<<EOD
            SELECT column_name, column_default
            FROM information_schema.columns
            WHERE (table_schema, table_name) = ('public', '{$tablename}')
            ORDER BY ordinal_position;
EOD;
        $res = pg_query($db, $query);
        $result = pg_fetch_all($res);

        return $_cache[$tablename] = $result;
    }

    public function pg_get_nextval($tablename) {
        static $_cache = array();
        if (array_key_exists($tablename, $_cache)) {
            return $_cache[$tablename];
        }

        $data = $this->pg_defaultvalues($tablename);
        foreach ($data as $row) {
            $defaultvalue = $row['column_default'];

            if (strpos($defaultvalue, 'nextval(') === 0) {
                return $_cache[$tablename] = explode("'", $defaultvalue)[1];
            }
        }
        return $_cache[$tablename] = null;
    }

    public function pg_get_nextval_colname($tablename) {
        $val = $this->pg_get_nextval($tablename);
        if ($val == null) {
            return null;
        }
        $val = substr($val, strlen($tablename) + 1, -4); // -4 is length of "_seq"
        return $val;
    }
}