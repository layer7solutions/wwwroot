<?php

/**
 * Database Driver Interface
 *
 * Database drivers should implement this class. The driver file name should be "driver_<drivername>.php"
 * and the class name should be "DB_driver_<drivername>".
 *
 * Implement new drivers to the specifications listed as comments above each function below
 *
 * Use `$this->parent->sqlError` to throw SQL errors and use `$this->parent->nonSqlError` to
 * throw non-SQL errors.
 *
 * @see DB
 * @package database
 */
interface DB_driver {

    /**
     * Construct the DB_driver.
     *
     * @param DB        $parent The DB class instance
     * @param array     $options The $options associative array passed to \DB::connect()
     *                  This array contains the host, dbname, user, etc.
     *
     * This function should at least save `$parent` to a `$this->parent` field
     * and save the $options somehow to be used for `get()`
     *
     * The database connection should not be made here. See `get()`
     */
    public function __construct($parent, $options);

    /**
     * Calling this function should disconnect the database connection.
     */
    public function disconnect();

    /**
     * Calling this function the first time should connect to the database and return
     * the database connection object.
     * Calling this function all subsequent times should return the same database connection object
     *
     * @return mixed database connection object.
     */
    public function get();

    /**
     * querybuilder function, should be implemented to handle all types
     *
     * @param string            $type what kind of query to build, make sure to do strtolower on it
     * @param array             $args associative arrays, contains all necessary variables to build query
     *
     * Types:
     *   - 'delete' :
     *   - 'update' :
     *   - 'insertupdate_assoc' :
     *   - 'insertupdate' :
     *   - 'insertorreplace' :
     *   - 'insertid_modquery'
     *   - 'insertignore_modquery'
     *   - 'replace_modwhich'
     *   - 'insert_modwhich'
     *   - 'begin'
     *   - 'commit'
     *   - 'rollback'
     *
     * @return array|string
     * For all non-'*_modwhich' types, they should return an array of parameters which is what
     * will be passed to queryHelper through call_user_func_array
     * (first parameter should be query string, all following should be placeholder variable values)
     * Use `$this->parent->paramChars('string')` to convert '%' to the set parameter character
     *
     * '*_modquery' are passed the array of parameters returned by 'insertorreplace' through
     * $args['params']
     *
     * 'replace_modwhich' and 'insert_modwhich' should return the $which of '$which INTO $table ...'
     *
     * ------
     *
     * The 'begin' (begin transaction), 'commit', and 'rollback' types should return just the query as a string.
     * If `$args['nested']` is true, then do nested transactions; if savepoints must be used, the savepoint
     * name you should use will be passed as `$args['savepoint']`
     *
     * ------
     *
     * To see what $args each type is given, search db.php for 'querybuilder' and check the second parameter.
     *
     * See `querybuilder` implementation of driver_pgsql.php as an example.
     */
    public function querybuilder($type, $args);

    /**
     * General result handler function, should be implemented to handle all types
     *
     * @param string    $type what kind of result is being handled, make sure to do strtolower on it
     * @param mixed     $res the raw SQL query result
     * @param string    $query the query string (only passed for 'executequery' type, null otherwise)
     * @param array     $args relevant arguments (currently only passed for 'insertid' type, null otherwise)
     *
     * @return mixed
     * Types and Expected Return:
     *   - 'insertid' : return insert id of insert statement
     *   - 'affected_rows' : return # of affected rows by update statement
     *   - 'num_rows' : return # of rows returned by last statement
     *   - 'executequery' : check $res for errors and throw sqlError if necessary
     *                      return true/false for success/failure
     */
    public function handleResult($type, $res, $query = null, $args = null);

    /**
     * This function should execute any given query and return the raw result
     *
     * @param string $query raw query
     * @return resource|mixed the raw result of the driver e.g. resource, or mysqli_result, etc.
     */
    public function executeQuery($query);

    /**
     * This function creates the result for `$db->query()`
     *
     * @param resource|mixed    $res the result returned from executeQuery()
     * @param string            $row_type 'assoc', 'column', or 'raw'
     * @param int|string        $column either an int (representing column #) or a string (column name) or null
     *
     * @return array[] This function should return an array of arrays (ie: an array of all the result rows).
     *     If $row_type is 'assoc' then return an array of associative arrays (column_name => column_value)
     *     If $row_type is 'column' then return an array of numeric arrays (column_number => column_value)
     *
     * If $column is not null then each row array should only have data for the specified column. The return
     * result is also a little different:
     *     The top level array should be associative and be `$column => result_data`.
     *     If done correctly there should only be one element in this top level array.
     *
     *         Example:
     *
     *         $result = [];
     *         if (isset($column)) {
     *             $result[$column] = [];
     *         }
     *         ...
     *
     *     If row_type is list, append values to result_data numerically.
     *
     *         Example:
     *
     *         $result[$column][] = $value;
     *
     *     If row_type is assoc, append values to result_data as field_name => value
     *
     *         Example:
     *
     *         $result[$column][$field] = $value;
     *
     *     See the implementation of createQueryResult() in driver_pgsql.php for a better example.
     */
    public function createQueryResult($res, $row_type, $column = null);

    /**
     * Escape a string.
     *
     * @param string    $str
     * @return string   escaped string
     */
    public function escape($str);

    /**
     * Escapes an identifier: table names, column names, etc.
     *
     * @param string    $table some identifier
     * @return string   escaped identifier.
     */
    public function formatTableName($table);

    /**
     * Return an array<string> of all tables (by name) in the current database.
     *
     * @return string[] table list
     */
    public function tableList();

    /**
     * Return an array<string> of all columns (by name) in the given table.
     *
     * @param string    $table
     * @param bool      $types if true, return the array as associative: field_name => field_type
     * @return array    column list
     */
    public function columnList($table, $types = false);

    /**
     * Gets passed the tablename and the updateData for any update function: update, insert, insertIgnore, etc.
     * Should return the updateData with or without modifications as necessary
     * This is the function where any implicit typecasting should be done, use columnList($table, true) to get
     * list of columns and their type.
     *
     * @param string    $table
     * @param array     $updateData
     * @return array    update data
     */
    public function checkUpdateData($table, $updateData);

    /**
     * Given some kind of variable, returns the driver-compatible literal true/false value as string.
     * For example, for postgresql:
     *      return $value ? 'true' : 'false';
     *
     * @param mixed     $value some value
     * @return string   boolean literal as string
     */
    public function castBoolean($value);

}