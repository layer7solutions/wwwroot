<?php /** @noinspection PhpUndefinedClassInspection */

require 'driver_interface.php';

/**
 * Database abstraction wrapper.
 *
 * See https://beta.layer7.solutions/docs/internal/database for usage documentation.
 */
class DB {
    // Options: general
    public $usenull = true;
    public $driver = null;
    public $nested_transactions = false;

    // Internal
    protected static $db_list = [];
    protected $insert_id = null;
    protected $affected_rows = null;
    protected $num_rows = null;
    protected $last_sql = null;
    protected $query_runtime = null;

    // Internal
    protected $param_char = '%';
    protected $named_param_sep = '_';
    protected $debug_mode = false; // for parse testing purposes, never enable for prod
    protected $nested_transactions_count = 0;

    // Options: Error handlers
    public $throw_exception_on_sql_error = false;
    public $throw_exception_on_nonsql_error = false;
    public $sql_error_handler = null;
    public $nonsql_error_handler = null;
    public $success_handler = null;

    protected static function add_to_db_list($instance) {
        if (empty(self::$db_list)) {
            register_shutdown_function('\DB::close_all_connections');
        }

        self::$db_list[] = $instance;
    }

    public static function close_all_connections() {
        foreach (self::$db_list as $instance) {
            $instance->disconnect();
        }
    }

    public function __construct($options = array()) {
        if (isset($options['driver'])) {
            $driver_name = $options['driver'];
        } else if (defined('DB_DEFAULT_DRIVER')) {
            $driver_name = DB_DEFAULT_DRIVER;
        } else {
            $this->nonSqlError('no database driver supplied');
            return;
        }

        require_once('driver_'.$driver_name.'.php');
        $driver_classname = 'DB_driver_'.$driver_name;
        $this->driver = new $driver_classname($this, $options);

        if (!($this->driver instanceof DB_driver)) {
            $e = new DBException('DB error -- invalid driver supplied: does not implemented DB_driver.class');
            throw $e;
        }

        self::add_to_db_list($this);
    }

    public static function connect($options = array()) {
        return new self($options);
    }

    public function __destruct() {
        $this->disconnect();
    }

    public function setDebugMode($state) {
        $this->debug_mode = $state;
    }

    // Connection functions
    // ------------------------------------------------------------------------------------------
    public function disconnect() {
        $this->driver->disconnect();
    }

    public function get() {
        return $this->driver->get();
    }

    public function getDriver() {
        return $this->driver;
    }

    // Error handlers
    // ------------------------------------------------------------------------------------------
    public function nonSqlError($error) {
        if ($this->throw_exception_on_nonsql_error) {
            $e = new DBException($error);
            throw $e;
        }

        $error_handler = is_callable($this->nonsql_error_handler) ?
            $this->nonsql_error_handler :
            'db_nonsql_error_handler';

        call_user_func($error_handler, array(
            'type' => 'nonsql',
            'error' => $error
        ));

        return false;
    }

    public function sqlError($error, $query = null, $errno = null) {
        if ($this->throw_exception_on_sql_error) {
            $e = new DBException($error, $query, $errno);
            throw $e;
        }

        $error_handler = is_callable($this->sql_error_handler) ?
            $this->sql_error_handler :
            'db_sql_error_handler';

        call_user_func($error_handler, array(
            'type' => 'sql',
            'error' => $error,
            'query' => $query,
            'errno' => $errno,
        ));

        return false;
    }

    // Basic Query
    // ------------------------------------------------------------------------------------------

    // Calls queryHelper with an empty array for the first "options" parameter (use default options)
    public function query() {
        $args = func_get_args();
        array_unshift($args, array());
        return call_user_func_array(array($this, 'queryHelper'), $args);
    }

    // queryHelper()
    //
    // Expected args (in order):
    //   options (assoc array)
    //   query statement (string)
    //   params... (varargs: param values; or assoc array)
    public function queryHelper() {
        $args = func_get_args();
        $options = array_shift($args);

        // ----- BEGIN CHECK PARAMS

        if (func_num_args() < 2) {
            return $this->nonSqlError('Expected at least 2 params, got ' . func_num_args());
        }
        if (!is_array($options)) {
            return $this->nonSqlError('queryHelper -- First param must be of type array, got ' .
                                    gettype($args[0]) . ' instead!');
        }
        if (!is_string($args[0])) {
            return $this->nonSqlError('queryHelper -- Second param must be of type string, got ' .
                                    gettype($args[1]) . ' instead!');
        }

        // ----- END CHECK PARAMS

        $sql = $this->parseQueryParams($args);

        $this->last_sql = $sql;

        if ($sql == false) {
            // if $sql is false, parseQueryParams should have already thrown the error
            // no need to throw an error again, just return false
            return false;
        }

        if ($this->debug_mode) {
            echo $sql;
        }

        $starttime = (microtime(true) * 1000);
        $res = $this->driver->executeQuery($sql);
        $runtime = (microtime(true) * 1000) - $starttime;

        // ----- BEING ERROR/SUCCESS HANDLING
        if ($res) {
            if (!$this->driver->handleResult('executeQuery', $res, $sql)) {
                return $this->nonSqlError('query error -- could not handle result');
            } else {
                // success

                $this->affected_rows = $this->driver->handleResult('affected_rows', $res);
                $this->num_rows = $this->driver->handleResult('num_rows', $res);
                $this->query_runtime = $runtime;

                if ($this->success_handler && is_callable($this->success_handler)) {
                    call_user_func($this->success_handler, array(
                        'query'     => $sql,
                        'runtime'   => $runtime,
                        'affected'  => $this->affected_rows
                    ));
                }
            }
        } else {
            return $this->sqlError('query error -- no results available after execution');
        }

        // ----- END ERROR/SUCCESS HANDLING

        return $this->prepareResult($res, $options);
    }

    public function prepareResult($res, $options) {
        $row_type = isset($options['row_type']) ? $options['row_type'] : 'assoc';
        $column = isset($options['column']) ? $options['column'] : null;

        //  validate row type
        switch ($row_type) {
            case 'raw':
                break;
            case 'assoc':
                break;
            case 'list':
                break;
            default:
                return $this->nonSqlError('invalid argument to queryHelper!');
        }

        // just return the original resource if row_type is raw
        if ($row_type == 'raw') {
            return $res;
        }

        return $this->driver->createQueryResult($res, $row_type, $column);
    }

    protected function parseQueryParams($args) {
        $statement = array_shift($args);
        $params = [];

        // available types - must be ordered from longer to shorter
        $types = [
            'll?', 'lls', 'l?', 'ls', 'li', 'ld', 'lt', 'lb', 'll',
            'hc', 'ha', 'ho', 'ss', 'b', 'l', 's', 'i', 'd', 't', '?',
        ];

        // build params array
        if (count($args) == 1 && is_array($args[0])) {
            foreach ($args[0] as $k => $v) {
                $params[$k] = $v;
            }
        } else {
            $ii = 0;
            foreach ($args as $arg) {
                $params[$ii++] = $arg;
            }
        }

        // parse variables
        $chars = str_split($statement);
        $query = '';
        $param_i = 0;

        // parse query statement
        for ($i = 0, $len = count($chars); $i < $len; $i++) {
            $ch = $chars[$i];

            // ----- BEING PARAM PARSING
            if ($ch == $this->param_char) { // param found
                $substr_cache = [];

                $type = null;
                $param_name = null;
                $param_number = null;

                // check against all existing types and extract the type found to the $type variable above
                // and the parameter name (if exists) to $param_name variable above
                //
                // if unknown type, $type will be left as not
                // if no parameter name exists, $param_name will be left as null
                foreach ($types as $t) {
                    $tl = strlen($t);

                    $ts = null;
                    if (array_key_exists($tl, $substr_cache)) {
                        $ts = $substr_cache[$tl];
                    } else {
                        $ts = substr($statement, $i+1, $tl);
                        $substr_cache[$tl] = $ts;
                    }

                    // check if type matches, otherwise continue
                    if ($ts == $t) {
                        $type = $t;
                        $sep_offset = $i + $tl + 1;

                        if (isset($chars[$sep_offset]) && $chars[$sep_offset] == $this->named_param_sep) {
                            // named parameter
                            $param_start = $sep_offset+1;
                            $param_len = 0;

                            /** @noinspection PhpStatementHasEmptyBodyInspection */
                            for (; isset($chars[$param_start + $param_len]) &&
                                    $this->named_param_is_valid_char($chars[$param_start + $param_len]);
                                   $param_len++
                            );

                            $param_name = substr($statement, $param_start, $param_len);
                        } else if (isset($chars[$sep_offset]) && is_numeric($chars[$sep_offset])) {
                            // ordered parameter
                            $num_start = $sep_offset;
                            $num_len = 0;

                            /** @noinspection PhpStatementHasEmptyBodyInspection */
                            for (; isset($chars[$num_start + $num_len]) &&
                                    is_numeric($chars[$num_start + $num_len]);
                                   $num_len++
                            );

                            $param_number = intval(substr($statement, $num_start, $num_len));
                        } /** @noinspection PhpStatementHasEmptyBodyInspection */ else {
                            // non-named parameter
                            // nothing needs to be done here
                        }
                        break;
                    }
                    continue;
                }

                if (empty($type)) {
                    return $this->nonSqlError('Badly formatted SQL query: unknown type');
                }

                // append parameter value
                $result = $this->queryParamHelper($type, $param_name, $params, $param_i++, $param_number);
                if ($result === false) {
                    return false;
                }

                $query = $query . $result;

                // skip over type stuff
                $i = $i + strlen($type) + (isset($param_name) ? strlen($param_name) + 1 : 0) + // +1 is for param sep
                                          (isset($param_number) ? strlen($param_number) : 0);
                continue;
            }
            // ----- END PARAM PARSING

            $query = $query . $ch;
        }

        return $query;
    }

    protected function queryParamHelper($type, $param_name, $params, $param_i, $param_number) {
        $key = isset($param_name) ? $param_name : (isset($param_number) ? $param_number : $param_i);
        $value = null;

        if (!isset($params[$key])) {
            return $this->nonSqlError('Badly formatted SQL query: key ('.$key.') for parameter not found');
        }

        $value = $params[$key];
        $result = null;

        // check if $type is an array type
        $array_types = array('ls', 'li', 'ld', 'lb', 'll', 'lt', 'l?', 'll?', 'lls', 'hc', 'ha', 'ho');
        $is_array_type = in_array($type, $array_types, true);

        // validate array type
        if ($is_array_type && !is_array($value)) {
            return $this->nonSqlError("Badly formatted SQL query: Expected array, got scalar instead!");
        } else if (!$is_array_type && is_array($value)) {
            $value = '';
        }

        // switch $type and properly sanitize
        switch ($type) {
            case 's':
                $result = $this->escape($value);
                break;
            case 'i':
                $result = intval($value);
                break;
            case 'd':
                $result = doubleval($value);
                break;
            case 'b':
                $result = $this->formatTableName($value);
                break;
            case 'l':
                $result = $value;
                break;
            case 'ss':
                $result = $this->escape("%" . str_replace(array('%', '_'), array('\%', '\_'), $value) . "%");
                break;
            case 't':
                $result = $this->escape($this->parseTS($value));
                break;

            case 'ls':
                $result = array_map(array($this, 'escape'), $value);
                break;
            case 'li':
                $result = array_map('intval', $value);
                break;
            case 'ld':
                $result = array_map('doubleval', $value);
                break;
            case 'lb':
                $result = array_map(array($this, 'formatTableName'), $value);
                break;
            case 'll':
                $result = $value;
                break;
            case 'lt':
                $result = array_map(array($this, 'escape'), array_map(array($this, 'parseTS'), $value));
                break;

            case '?':
                $result = $this->sanitize($value);
                break;
            case 'l?':
                $result = $this->sanitize($value, 'list');
                break;
            case 'll?':
                $result = $this->sanitize($value, 'doublelist');
                break;
            case 'lls':
                $result = $this->sanitize(array_map(array($this, 'escape'), $value), 'doublelist');
                break;
            case 'hc':
                $result = $this->sanitize($value, 'hash');
                break;
            case 'ha':
                $result = $this->sanitize($value, 'hash', ' AND ');
                break;
            case 'ho':
                $result = $this->sanitize($value, 'hash', ' OR ');
                break;
            default:
                return $this->nonSqlError('Badly formatted SQL query: unknown type');
        }

        if (is_array($result)) {
            $result = '(' . implode(',', $result) . ')';
        }

        return $result;
    }

    // Insert / Replace / Update / Delete functions
    // ------------------------------------------------------------------------------------------

    public function insertOrReplace($which, $table, $data, $options = array()) {
        $data = unserialize(serialize($data)); // break references within array
        $data = $this->driver->checkUpdateData($table, $data);
        $keys = $values = array();

        // is array of arrays?
        if (isset($data[0]) && is_array($data[0])) {
            // yes
            $var = '%ll?';
            foreach ($data as $item) {
                ksort($item);

                if (!$keys)
                    $keys = array_keys($item);
                $values[] = array_values($item);
            }
        } else {
            // no: just a normal array
            $var = '%l?';
            $keys = array_keys($data);
            $values = array_values($data);
        }

        $qdata = array(
            'which'     => $which,
            'table'     => $table,
            'data'      => $data,
            'options'   => $options,
            'keys'      => $keys,
            'values'    => $values,
            'var'       => $var,
        );

        if (strtolower($which) == 'replace') {
            $which = $this->driver->querybuilder('replace_modwhich', $qdata);
        } else if (strtolower($which) == 'insert') {
            $which = $this->driver->querybuilder('insert_modwhich', $qdata);
        }

        $qdata['which'] = $which;

        // ----- INSERT UPDATE (IF TRUE)
        if (strtolower($which) == 'insert' && is_array($this->from($options, 'update'))) {
            $updateData = $options['update'];
            $updateData = $this->driver->checkUpdateData($table, $updateData);

            $is_assoc = '';

            // check if updateData is an associative array
            if (array_values($updateData) !== $updateData) {
                $is_assoc = '_assoc';
            } else {
                $qdata['update_str'] = array_shift($updateData);
            }

            $qdata['updateData'] = $updateData;

            $res = call_user_func_array(array($this, 'query'),
                $this->driver->querybuilder('insertupdate'.$is_assoc, $qdata));
            if ($res == false) {
                return false;
            } else {
                $insertid = $this->driver->handleResult('insertid', $res, null, $qdata);
                if ($insertid == false) {
                    return false;
                }
                $this->insert_id = $insertid;
                return true;
            }
        }

        // ----- QUERY BUILDER - GET PARAM DATA
        $qdata['params'] = $this->driver->querybuilder('insertOrReplace', $qdata);

        // ----- INSERT IGNORE (MOD QUERY IF TRUE)
        if ($this->from($options, 'ignore')) {
            $qdata['params'] = $this->driver->querybuilder('insertignore_modquery', $qdata);
        }

        // ----- INSERT ID (MOD QUERY IF TRUE)
        if (strtolower($which) == 'insert') {
            $qdata['params'] = $this->driver->querybuilder('insertid_modquery', $qdata);
        }

        // ----- EXECUTE QUERY

        array_unshift($qdata['params'], array('row_type' => 'raw'));
        $res = call_user_func_array(array($this, 'queryHelper'), $qdata['params']);

        if ($res == false) {
            return false;
        } else {
            if (strtolower($which) == 'insert') {
                $insertid = $this->driver->handleResult('insertid', $res, null, $qdata);
                if ($insertid == false) {
                    return false;
                }
                $this->insert_id = $insertid;
                return true;
            }
            return true;
        }
    }

    public function insert($table, $data) {
        return $this->insertOrReplace('INSERT', $table, $data);
    }

    public function insertIgnore($table, $data) {
        return $this->insertOrReplace('INSERT', $table, $data, ['ignore' => true]);
    }

    public function replace($table, $data) {
        return $this->insertOrReplace('REPLACE', $table, $data);
    }

    // insertUpdate(table, insertData, updateData)
    public function insertUpdate() {
        $args = func_get_args();
        $table = array_shift($args);
        $insertData = array_shift($args);

        // set updateData to insertData if no updateData
        if (!isset($args[0])) {
            $args[0] = $insertData;
        }

        // build update params
        if (is_array($args[0])) {
            $updateData = $args[0];
        } else {
            $updateData = $args;
        }

        return $this->insertOrReplace('INSERT', $table, $insertData, array('update' => $updateData));
    }

    // update(table, updateData, where, whereData...)
    public function update() {
        $args = func_get_args();
        list($table, $params, $where) = array_slice($args, 0, 3);
        $whereData = array_slice($args, 3);

        $updateData = $this->driver->checkUpdateData($table, $params);

        $params = $this->driver->querybuilder('update', [
                'table'         => $table,
                'params'        => $updateData,
                'where'         => $where,
                'whereData'     => $whereData,
            ]);

        return call_user_func_array(array($this, 'query'), $params);
    }

    // delete(table, where, args)
    public function delete() {
        $args = func_get_args();

        $table = $this->formatTableName(array_shift($args));
        $where = array_shift($args);

        if (is_array($args[0])) {
            $args = $args[0];
        }

        $params = $this->driver->querybuilder('delete', [
                'table'         => $table,
                'where'         => $where,
                'whereData'     => $args,
            ]);

        return call_user_func_array(array($this, 'query'), $params);
    }

    // Sanitize & Validation functions
    // ------------------------------------------------------------------------------------------

    public function paramChars($str) {
        return str_replace('%', $this->param_char, $str);
    }

    protected function named_param_is_valid_char($ch) {
        return (ctype_alnum($ch) || $ch == '_' || $ch == '-');
    }

    protected function escape($str) {
        if (is_array($str)) {
            return array_map(array($this, 'escape'), $str);
        }
        return $this->driver->escape($str);
    }

    protected function formatTableName($table) {
        return $this->driver->formatTableName($table);
    }

    protected function parseTS($ts) {
        if (is_string($ts)) {
            return date('Y-m-d H:i:s', strtotime($ts));
        } else {
            if (is_object($ts) && ($ts instanceof \DateTime)) {
                return $ts->format('Y-m-d H:i:s');
            }
        }
    }

    protected function sanitize($value, $type='basic', $hashjoin=', ') {
        if ($type == 'basic') {
            if (is_object($value)) {
                if ($value instanceof DBSQLEval)
                    return $value->text;
                else if ($value instanceof \DateTime)
                    return $this->escape($value->format('Y-m-d H:i:s'));
                else
                    return $this->escape($value); // use __toString() value for objects, when possible
            }

            if (is_null($value))
                return $this->usenull ? 'NULL' : "''";
            else if (is_bool($value))
                return $this->driver->castBoolean($value);
            else if (is_int($value))
                return $value;
            else if (is_float($value))
                return $value;
            else if (is_array($value)) {
                $value = array_values($value);
                return '(' . implode(', ', array_map(array($this, 'sanitize'), $value)) . ')';
            } else
                return $this->escape($value);
        } else if ($type == 'list') {
            if (is_array($value)) {
                $value = array_values($value);
                return '(' . implode(', ', array_map(array($this, 'sanitize'), $value)) . ')';
            } else {
                return $this->nonSqlError("Expected array parameter, got something different!");
            }
        } else if ($type == 'doublelist') {
            if (is_array($value) && !empty($value) && array_values($value) === $value && is_array($value[0])) {
                $cleanvalues = array();
                foreach ($value as $sub_array) {
                    $cleanvalues[] = $this->sanitize($sub_array, 'list');
                }
                return implode(', ', $cleanvalues);
            } else if (is_array($value) && empty($value)) {
                return '';
            } else {
                return $this->nonSqlError("Expected double array parameter, got something different!");
            }
        } else if ($type == 'hash') {
            if (is_array($value)) {
                $pairs = array();
                foreach ($value as $k => $v) {
                    $pairs[] = $this->formatTableName($k) . '=' . $this->sanitize($v);
                }

                return implode($hashjoin, $pairs);
            } else {
                return $this->nonSqlError("Expected hash (associative array) parameter, got something different!");
            }
        } else {
            return $this->nonSqlError("Invalid type passed to sanitize()!");
        }

    }

    // Helper functions
    // ------------------------------------------------------------------------------------------

    // Get the value of key(s) in array without PHP complaining if
    // the index is undefined
    //
    // If undefined, this function will return null
    // $name can be an array of keys to get multiple values
    //
    // copied from bootstrap/common/secure.php as db.php should be standalone
    function from($source, $name, $map_func = null) {
        if (!isset($source))
            return null;
        if (is_array($name)) {
            $data = array();
            foreach ($name as $k) {
                $value = isset($source[$k]) ? $source[$k] : null;
                if (isset($map_func) && isset($value))
                    $value = call_user_func_array($map_func, array($value));
                $data[$k] = $value;
            }
            return $data;
        }
        $value = isset($source[$name]) ? $source[$name] : null;
        if (isset($map_func) &&isset($value))
            $value = call_user_func_array($map_func, array($value));
        return $value;
    }

    public function sqlEval() {
        $args = func_get_args();
        $text = call_user_func_array(array($this, 'parseQueryParams'), array($args));
        return new DBSQLEval($text);
    }

    // Query Functions
    // ------------------------------------------------------------------------------------------

    protected function queryWithOptions($args, $row_type = null, $column = null) {
        array_unshift($args, array('row_type' => $row_type, 'column' => $column));
        return call_user_func_array(array($this, 'queryHelper'), $args);
    }

    public function queryList() {
        return $this->queryWithOptions(func_get_args(), 'list');
    }
    public function queryFirstRow() {
        return $this->from($this->queryWithOptions(func_get_args(), 'assoc'), 0);
    }
    public function queryFirstList() {
        return $this->from($this->queryWithOptions(func_get_args(), 'list'), 0);
    }
    public function queryFirstColumn() {
        return $this->from($this->queryWithOptions(func_get_args(), 'list', 0), 0);
    }
    public function queryOneColumn() {
        $args = func_get_args();
        $column = array_shift($args);
        return $this->from($this->queryWithOptions($args, 'list', $column), $column);
    }
    public function queryFirstField() {
        $row = call_user_func_array(array($this, 'queryFirstList'), func_get_args());
        return $this->from($row, 0);
    }
    public function queryOneField() {
        $args = func_get_args();
        $column = array_shift($args);

        $row = call_user_func_array(array($this, 'queryFirstRow'), $args);
        return $this->from($row, $column);
    }
    public function queryRaw() {
        return $this->queryWithOptions(func_get_args(), 'raw');
    }

    public function insertId()     { return $this->insert_id; }
    public function count()        { return $this->num_rows; }
    public function affectedRows() { return $this->affected_rows; }
    public function runtime()      { return $this->query_runtime; }
    public function last_sql()     { return $this->last_sql; }

    public function tableList() { return $this->driver->tableList(); }
    public function columnList($table, $types = false) { return $this->driver->columnList($table, $types); }

    public function startTransaction() {
        if (!$this->nested_transactions || $this->nested_transactions_count == 0) {
            $this->query($this->driver->querybuilder('begin', ['nested' => false]));
            $this->nested_transactions_count = 1;
        } else {
            $savepoint = 'DB_LEVEL'.$this->nested_transactions_count++;
            $this->query($this->driver->querybuilder('begin', ['nested' => true, 'savepoint' => $savepoint]));
        }

        return $this->nested_transactions_count;
    }
    public function commit($all = false) {
        if ($this->nested_transactions && $this->nested_transactions_count > 0)
            $this->nested_transactions_count--;

        if (!$this->nested_transactions || $all || $this->nested_transactions_count == 0) {
            $this->query($this->driver->querybuilder('commit', ['nested' => false]));
            $this->nested_transactions_count = 0;
        } else {
            $savepoint = 'DB_LEVEL'.$this->nested_transactions_count;
            $this->query($this->driver->querybuilder('commit', ['nested' => true, 'savepoint' => $savepoint]));
        }

        return $this->nested_transactions_count;
    }
    public function rollback($all = false) {
        if ($this->nested_transactions && $this->nested_transactions_count > 0)
            $this->nested_transactions_count--;

        if (!$this->nested_transactions || $all || $this->nested_transactions_count == 0) {
            $this->query($this->driver->querybuilder('rollback', ['nested' => false]));
            $this->nested_transactions_count = 0;
        } else {
            $savepoint = 'DB_LEVEL'.$this->nested_transactions_count;
            $this->query($this->driver->querybuilder('rollback', ['nested' => true, 'savepoint' => $savepoint]));
        }

        return $this->nested_transactions_count;
    }
}

class DBSQLEval {
    public $text = '';

    function __construct($text) {
        $this->text = $text;
    }
}

class DBException extends \Exception {
    protected $query;

    function __construct($message = '', $query = '', $code = 0) {
        parent::__construct($message);
        $this->query = $query;
        $this->code = $code;
    }

    public function getQuery() { return $this->query; }
}

function db_sql_error_handler($args) {
    $out = [];

    if (isset($args['query'])) $out[] = 'QUERY: ' . $args['query'];
    if (isset($args['error'])) $out[] = 'ERROR: ('.$args['error'].')' . (isset($args['errno']) ? (' ['.$args['errno'].']') : '');

    if (php_sapi_name() == 'cli' && empty($_SERVER['REMOTE_ADDR'])) {
        echo implode("\n", $out)."\n";
    } else {
        echo implode("<br>\n", $out)."<br>\n";
    }
}

function db_nonsql_error_handler($args) {
    $out = [];

    if (isset($args['error'])) $out[] = "ERROR: " . $args['error'] . (isset($args['errno']) ? (' ['.$args['errno'].']') : '');

    if (php_sapi_name() == 'cli' && empty($_SERVER['REMOTE_ADDR'])) {
        echo implode("\n", $out)."\n";
    } else {
        echo implode("<br>\n", $out)."<br>\n";
    }
}