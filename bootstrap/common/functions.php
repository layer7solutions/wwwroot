<?php /** @noinspection PhpUndefinedClassInspection */

/*
functions.php
~~~~~~~~~~~~~

misc helper functions, most of which are used absolutely nowhere but meh ¯\_(ツ)_/¯
*/

// FORMAT FUNCTIONS/CONVERT FUNCTIONS
// --------------------------------------------------------------------------------

/**
 * Cast to boolean.
 *
 * @param mixed     $o some variable
 * @return bool     true if $o is truthy, false if falsey
 */
function to_bool($o): bool {
    return $o ? true : false;
}

/**
 * Returns 'true' or 'false' for the given boolean - useful for printing out booleans.
 *
 * @param mixed     $bool some variable
 * @return string   'true' if $bool is truthy, 'false' if falsey
 */
function bool_str($bool): string {
    return $bool ? 'true' : 'false';
}

/**
 * Returns a boolean from the given string.
 *
 * @param mixed     $o some variable
 * @return bool     true if the string has a true-like value.
 */
function bool_unstr($o): bool {
    if (is_bool($o)) {
        return $o;
    }
    if (is_string($o)) {
        return valid_boolean($o, true);
    }
    return to_bool($o);
}

/**
 * Prints out each passed in string in a div element. This function is only
 * used in some test files.
 *
 * @param string[varargs]
 */
function print_div(): void {
    $args = func_get_args();
    foreach ($args as $html) {
        if (is_string($html) || is_string_convertable($html)) {
            echo '<div>'.$html.'</div>';
        } else {
            echo '<div>';
            var_dump($html);
            echo '</div>';
        }
    }
}

/**
 * Convert an array to a string, a more readable way to print out arrays and
 * associative arrays.
 *
 * @param array     $arr
 * @param bool      $var_export_all=false if true, uses var_export() on each element
 * @return string   stringified array
 */
function array_str(array $arr, bool $var_export_all = false): string {
    $is_assoc = is_assoc($arr);
    $result = '[';
    foreach ($arr as $key => $value) {
        if ($is_assoc)
            $result .= $key . '=';
        if (is_array($value))
            $result .= array_str($value);
        else if ($var_export_all)
            $result .= var_export($value, true);
        else if (is_bool($value))
            $result .= bool_str($value);
        else if (is_string($value))
            $result .= '"'.$value.'"';
        else if ($value instanceof \DateTime)
            $result .= $value->format('Y-m-d H:i:s:uP');
        else if ($value instanceof \DateInterval)
            $result .= $value->format('%Y-%M-%D %H:%I:%S');
        else if (is_string_convertable($value))
            $result .= $value;
        else if ($value == null)
            $result .= 'NULL';
        else
            $result .= var_export($value, true);
        $result .= ', ';
    }
    return remove_suffix($result, ', ') . ']';
}

/**
 * Returns true if the given object convertable to a string, false otherwise
 * @return bool
 */
function is_string_convertable($value): bool {
    if (is_object($value) and method_exists($value, '__toString')) return true;
    if (is_null($value)) return true;
    return is_scalar($value);
}

/**
 * Check if the parameter can be represented as an integer.
 *
 * @param mixed $i
 * @return bool true if valid, false otherwise
 */
function valid_int($i): bool {
    $i = filter_var($i, FILTER_VALIDATE_INT);
    return ($i !== false);
}

/**
 * Check if the parameter can be represented as a boolean.
 *
 * @param mixed $o
 * @return bool true if valid, false otherwise
 */
function valid_boolean($o, $only_true_table = false): bool {
    static $_true_table = [
        true => true,
        't' => true,
        'true' => true,
        '1' => true,
        'y' => true,
        'yes' => true,
        'on' => true,
        'en' => true,
        'enable' => true,
        'enabled' => true,
        'activated' => true,
        'positive' => true,
        'allow' => true,
        'allowed' => true,
        '+' => true,
        '✓' => true,
        '✔' => true,
        '🗸' => true,
        '☑' => true,
        '🗹' => true,
        '✅' => true,
    ];
    static $_false_table = [
        false => true,
        'f' => true,
        'false' => true,
        '0' => true,
        'n' => true,
        'no' => true,
        'off' => true,
        'dis' => true,
        'disable' => true,
        'disabled' => true,
        'deactivated' => true,
        'negative' => true,
        'forbid' => true,
        'forbidden' => true,
        'x'  => true,
        '☐' => true,
        '☒' => true,
        '✗' => true,
        '✘' => true,
        '×' => true,
        '╳' => true,
        '☓' => true,
        '✕' => true,
        '✖' => true,
        '❌' => true,
        '❎' => true,
        '⨉' => true,
        '⨯' => true,
        '🗙' => true,
        '🗴' => true,
        '🞩' => true,
    ];
    if ($only_true_table) {
        return isset($_true_table[$o]) || isset($_true_table[strtolower($o)]);
    }
    return isset($_true_table[$o]) || isset($_false_table[$o]) ||
        isset($_true_table[$lo = strtolower($o)]) || isset($_false_table[$lo]);
}

/**
 * Check if the parameter can be represented as a boolean.
 *
 * @param mixed $o
 * @return bool true if valid, false otherwise
 */
function valid_bool($o): bool {
    return valid_boolean($o);
}

/**
 * Returns the Name or the Mime Type/Internet Media Type for the given File Extension.
 *
 * @param string $ext The file extension
 * @param bool $name whether to return the name (such as JavaScript) or the mime type (such as application/javascript)
 * @return string name or mime type, if no match for the give extension is found, "application/octet-stream"/"Binary Data" will be returned
 */
function file_type(string $ext, bool $name = false): string {
    static $_didRequire = false;
    if ($_didRequire === false) {
        require BOOTSTRAP_ROOT . 'common/file_type.php';
        $_didRequire = true;
    }
    return file_type_impl($ext, $name);
}

/**
 * JSON encode for using in HTML or within HTML attributes.
 *
 * @param mixed $obj
 * @return string
 */
function json_encode_for_html($obj): string {
    return htmlspecialchars(json_encode($obj));
}

// ARRAY FUNCTIONS
// --------------------------------------------------------------------------------

/**
 * Flatten an array.
 *
 * e.g. ['foo', ['bar'], 'a', ['b', [c'], 'd']]
 *   => ['foo', 'bar', 'a', 'b', 'c', 'd']
 *
 * @param array (varargs)
 * @return array
 */
function array_flatten(): array {
    $args_handle = function($args) use (&$args_handle) {
        $list = [];
        foreach ($args as $arg) {
            if (is_array($arg)) {
                $list = array_merge($list, $args_handle($arg));
            } else {
                $list[] = $arg;
            }
        }
        return $list;
    };

    return $args_handle(func_get_args());
}

/**
 * Returns an array containing the single variable or
 * itself if already an array.
 *
 * @param mixed $o
 * @return array
 */
function cast_array($o): array {
    return is_array($o) ? $o : array($o);
}

/**
 * Convert the given object to lowercase if possible. If a string array is passed in,
 * all elements will be converted.
 *
 * To convert *just* strings to lowercase, this function should be used. Whereas `strtolower` will
 * cast to string and convert `null` to empty string.
 *
 * @param mixed $o
 * @return mixed
 */
function to_lower($o) {
    if (is_string($o)) {
        return strtolower($o);
    } else if (is_array($o)) {
        return array_map('to_lower', $o);
    } else {
        return $o;
    }
}

function to_lower_array($o) {
    return to_lower(cast_array($o));
}

/**
 * Convert the given object to uppercase if possible. If a string array is passed in,
 * all elements will be converted.
 *
 * To convert *just* strings to uppercase, this function should be used. Whereas `strtoupper` will
 * cast to string and convert `null` to empty string.
 *
 * @param mixed $o
 * @return mixed
 */
function to_upper($o) {
    if (is_string($o)) {
        return strtoupper($o);
    } else if (is_array($o)) {
        return array_map('to_upper', $o);
    } else {
        return $o;
    }
}

function to_upper_array($o) {
    return to_upper(cast_array($o));
}

/**
 * If the given variable is an array and has only one element,
 * returns that element. Otherwise returns the given variable.
 * @return mixed
 */
function aor1($o) {
    if (is_array($o) && count($o) == 1) {
        return $o[0];
    }
    return $o;
}

/**
 * Converts an array in this format:
 *   [['foo', 'bar'], ['hello', 'world'], ['a', 'b', 'c']]
 * to this format:
 *   ['foo' => 'bar', 'hello' => 'world', 'a' => ['b','c']]
 *
 * @param array     $arr
 * @param callable  $mapFunc=null optionally call this on each row of the given array
 * @return array    new associative array
 */
function array_map_to_assoc(array $arr, callable $mapFunc = null): array {
    $res = [];

    foreach ($arr as $row) {
        if (isset($mapFunc)) {
            $row = $mapFunc($row);
        }
        $res[array_shift($row)] = aor1($row);
    }

    return $res;
}

/**
 * Converts an array in this format:
 *   ['foo' => 'bar', 'hello' => 'world', 'a' => ['b','c']]
 * to this format:
 *   [['foo', 'bar'], ['hello', 'world'], ['a', 'b', 'c']]
 *
 * @param array     $arr
 * @param callable  $mapFunc=null optionally call this on each generated row of the given array
 * @param array     $append=[] (optional) merge this array into the beginning of each row
 * @param array     $prepend=[] (optional) merge this array into the ending of each row
 * @return array    new sequential pairs array
 */
function array_map_to_seq(array $arr, callable $mapFunc=null, array $append=[], array $prepend=[]) {
    $res = [];

    foreach ($arr as $key => $items) {
        if (is_array($items)) {
            array_unshift($items, $key);
        } else {
            $items = [$key, $items];
        }

        if (!empty($append)) {
            $items = array_merge($items, (array) $append);
        }

        if (!empty($prepend)) {
            $items = array_merge((array) $prepend, $items);
        }

        if (isset($mapFunc)) {
            $items = $mapFunc($items);
        }

        $res[] = $items;
    }

    return $res;
}

/**
 * Remove all the specified keys from the given associative array.
 *
 * @param array     $arr
 * @param string[]  $removal_keys keys to remove
 * @return array    pruned array
 */
function array_remove_keys(array $arr, array $removal_keys): array {
    return array_diff_key($arr, array_flip($removal_keys));
}

/**
 * Check if the given associative array has all of the specified keys.
 *
 * @param array     $arr
 * @param string[]  $keys
 * @return bool     true if all keys are present, false otherwise
 */
function array_has_keys(array $arr, array $keys): bool {
    return !array_diff_key(array_flip($keys), $arr);
}

/**
 * Check if the given associative array has all of the specified keys.
 *
 * @param array     $arr
 * @param string[]  $keys
 * @return bool     true if all keys are present, false otherwise
 */
function array_keys_exists(array $arr, array $keys): bool {
    return array_has_keys($arr, $keys);
}

/**
 * Removes the specified value from the given array.
 *
 * @param array     &$arr
 * @param mixed     $del_val
 */
function array_remove_value(array &$arr, $del_val): void {
    if(($key = array_search($del_val, $arr)) !== false) {
        unset($arr[$key]);
    }
}

/**
 * Keep only the specified keys in the given associative array.
 *
 * @param array     $arr
 * @param string[]  $keep_keys keys to keep
 * @return array    pruned array
 */
function array_keep_keys(array $arr, array $keep_keys) {
    return array_intersect_key($arr, array_flip($keep_keys));
}

/**
 * Case-insensitive array_unique.
 *
 * @param array $arr
 * @return array
 */
function array_iunique(array $arr): array {
    return array_intersect_key(
        $arr,
        array_unique(array_map("to_lower", $arr))
    );
}

/**
 * Check if array is associative (the key must be string).
 *
 * @return bool true if associative, false if sequential
 */
function is_assoc(array $array): bool {
    return (bool) count(array_filter(array_keys($array), 'is_string'));
}

/**
 * Sort an list of strings alphabetically.
 *
 * @param string[]  $array
 * @return string[] sorted array
 */
function alphasort(array $array): array {
    natcasesort($array);
    return array_values($array);
}

/**
 * Get the first element of an array.
 *
 * @param array     $array
 * @return mixed    first element value
 */
function first(array $array) {
    return reset($array);
}

/**
 * Get the last element of an array.
 *
 * @param array     $array
 * @return mixed    last element value
 */
function last(array $array) {
    return end($array);
}

/**
 * Create powerset.
 *
 * ```
 * >>> powerSet(['a', 'b', 'c'])
 * => [[], ["a"], ["b"], ["a","b"], ["c"], ["a","c"], ["b","c"], ["a","b","c"]]
 * ```
 *
 * @param array $array
 * @param bool $include_empty_set
 * @return array
 */
function powerSet(array $array, bool $include_empty_set = true): array {
    $results = $include_empty_set ? array(array()) : array();

    foreach ($array as $element) {
        foreach ($results as $combination) {
            $results[] = array_merge($combination, array($element));
        }
    }

    return $results;
}

/**
 * Get all combinations/permutations of an array.
 *
 * e.g.
 * ```
 * >>> pc_permute(['quick', 'brown', 'fox'])
 * [
 *   ["quick", "brown", "fox"],
 *   ["brown", "quick", "fox"],
 *   ["quick", "fox", "brown"],
 *   ["fox", "quick", "brown"],
 *   ["brown", "fox", "quick"],
 *   ["fox", "brown", "quick"]
 * ]
 * ```
 *
 * @param array $items the array to permute
 * @return array $perms (optional) don't use, internal parameter for recursion
 * @return array
 */
function pc_permute(array $items, array $perms = []) {
    $back = array();

    if (empty($items)) {
        $back[] = $perms;
    } else {
        for ($i = count($items) - 1; $i >= 0; --$i) {
            $newitems = $items;
            $newperms = $perms;
            list($foo) = array_splice($newitems, $i, 1);
            array_unshift($newperms, $foo);
            $back = array_merge($back, pc_permute($newitems, $newperms));
        }
    }

    return $back;
}

/**
 * Implode and alternate between separators.
 *
 * ```
 * >>> alternateImplode([',', '+'], ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'g', 'h'])
 * => "a,b+c,d+e,f+g,h+i,g+h"
 * >>> alternateImplode([',', '+', '.'], ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'g', 'h'])
 * => "a,b+c.d,e+f.g,h+i.g,h```
 *
 * @param string[]  $sep the separators to alternate between starting with first element and
 *                  looping back to it once the last separator is reached
 * @param array     $arr the array to implode
 * @return string   imploded array
 */
function alternateImplode(array $sep, array $arr) {
    $buf = '';

    for ($i = 0, $j = 0, $A_l = count($arr), $S_stop = count($sep) - 1, $A_stop = count($arr) - 1;
            $i < $A_l; $i++) {

        $buf .= strval($arr[$i]);

        if ($i === $A_stop) {
            break;
        }

        $buf .= $sep[$j];

        if ($j == $S_stop) {
            $j = 0;
        } else {
            $j++;
        }
    }
    return $buf;
}

/** @noinspection PhpDocMissingThrowsInspection */
/**
 * Scramble the case of a string.
 *
 * ```
 * >>> scrambleCase("hello world!")
 * => "HEllo worLD!"
 * ```
 *
 * @param string $str
 * @return string
 */
function scrambleCase($str) {
    $buf = '';

    $charr = str_split($str);
    foreach ($charr as $ch) {
        /** @noinspection PhpUnhandledExceptionInspection */
        if ((bool) random_int(0, 1)) {
            $buf .= strtolower($ch);
        } else {
            $buf .= strtoupper($ch);
        }
    }

    return $buf;
}

// Statistics functions
// --------------------------------------------------------------------------------

/**
 * Use standard deviation to remove outliers from a dataset.
 *
 * @param int[]|double[]|float[]    $dataset
 * @param int|double|float          $magnitude
 * @param int[]|double[]|float[]    pruned dataset
 * @return array
 */
function remove_outliers(array $dataset, $magnitude = 1): array {
    $count = count($dataset);

    if ($count == 0) {
        return array();
    }
    // Calculate the mean
    $mean = array_sum($dataset) / $count;
    // Calculate standard deviation and times by magnitude
    $deviation = sqrt(array_sum(array_map("sd_square", $dataset, array_fill(0, $count, $mean))) / $count) * $magnitude;

    // Return filtered array of values that lie within $mean +- $deviation.
    return array_values(array_filter($dataset, function($x) use ($mean, $deviation) { return ($x <= $mean + $deviation && $x >= $mean - $deviation); }));
}

/**
 * @param int|float|double $x
 * @param int|float|double $mean
 * @return int|float|double
 */
function sd_square($x, $mean) {
    return pow($x - $mean, 2);
}

/**
 * Calculate the median.
 *
 * @param int[]|double[]|float[] $arr
 * @return int|double|float
 */
function calculate_median(array $arr) {
    $count = count($arr);

    if ($count == 0) {
        return -1;
    }

    $middleval = (int) floor(($count-1)/2);
    if($count % 2) {
        $median = $arr[$middleval];
    } else {
        $low = $arr[$middleval];
        $high = $arr[$middleval+1];
        $median = (($low+$high)/2);
    }
    return $median;
}

/**
 * Calculate an average.
 *
 * @param int[]|double[]|float[] $arr
 * @return int|double|float
 */
function calculate_average(array $arr) {
    $count = count($arr);

    if ($count == 0) {
        return -1;
    }

    $total = 0;
    foreach ($arr as $value) {
        $total = $total + $value;
    }

    return ($total/$count);
}

// GUID FUNCTIONS
// --------------------------------------------------------------------------------

/**
 * Generate a GUID.
 *
 * @param bool      $remove_dashes=true if true, returns the GUID without dashes
 * @return string   GUID without braces
 */
function GEN_GUID(bool $remove_dashes = true) {
    if (function_exists('com_create_guid') === true) {
        /** @noinspection PhpUndefinedFunctionInspection */
        $result = trim(com_create_guid(), '{}');
    } else {
        $result = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535),
            mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
    if ($remove_dashes)
        return str_replace('-', '', $result);
    else
        return $result;
}

/**
 * Remove dashes and the {} from a GUID.
 *
 * @param string    $guid
 * @return string   deformatted GUID
 */
function unformat_GUID(string $guid) {
    return str_replace('-', '', trim($guid, '{}'));
}

/**
 * Add dashes and optionally the {} to a GUID.
 *
 * @param string    $guid
 * @param bool      $add_braces
 * @return string   formatted GUID
 */
function format_GUID(string $guid, bool $add_braces = true) {
    $guid = substr_replace($guid, '-', 8, 0);
    $guid = substr_replace($guid, '-', 13, 0);
    $guid = substr_replace($guid, '-', 18, 0);
    $guid = substr_replace($guid, '-', 23, 0);
    return $add_braces ? '{'.$guid.'}' : $guid;
}

// FILE FUNCTIONS
// --------------------------------------------------------------------------------

/**
 * Recursive delete dir and all contents.
 *
 * @param string $dir the directory path to delete
 */
function rrmdir(string $dir): void {
    foreach(glob($dir . '/*') as $file) {
        if(is_dir($file)) rrmdir($file); else unlink($file);
    } rmdir($dir);
}

/**
 * Recursive glob, and optionally only files with a certain extension.
 *
 * @param string        $dir the directory path
 * @param string        $ext=null required file extension
 * @return string[]     list of files/directories
 */
function rglob($dir, $ext = null) {
    $result = array();
    if ($ext != null) {
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir)) as $filename) {
            if (endsWith($filename, ".$ext"))
                array_push($result, $filename);
        }
    } else {
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir)) as $filename) {
            if (endsWith($filename, DIRECTORY_SEPARATOR . "..") || endsWith($filename, DIRECTORY_SEPARATOR . ".")) {
                continue;
            }
            array_push($result, $filename);
        }
    }
    return $result;
}

/**
 * Glob a list of all directories in the given path.
 *
 * @param string        $path the directory path
 * @return string[]     list of directories
 */
function glob_all_dirs(string $path): array {
    $data = array();
    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
    foreach ($files as $file) {
        if (is_dir($file) === true) {
            $data[] = strval($file);
        }
    }
    return $data;
}

/**
 * Glob a list of all files in the given path.
 *
 * @param string        $path the directory path
 * @return string[]     list of files
 */
function glob_all_files(string $path): array {
    $data = array();
    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
    foreach ($files as $file) {
        if (is_file($file) === true) {
            $data[] = strval($file);
        }
    }
    return $data;
}

/**
 * Get the file perms of a file.
 *
 * @param string            $file
 * @param bool              $octal=false
 * @return string|false     file permissions, or false if file doesn't exist
 */
function file_perms(string $file, bool $octal = false) {
    if(!file_exists($file)) return false;
    $perms = fileperms($file);
    $cut = $octal ? 2 : 3;
    return substr(decoct($perms), $cut);
}

/**
 * Get the contents of the specified file, or return null if not found or error occurred.
 *
 * @param string        $file
 * @return string|null  the file contents or null if error/not found
 */
function file_contents(string $file): ?string {
    if (!file_exists($file)) {
        return null;
    }

    try {
        $content = file_get_contents($file);
    } catch (\Exception $e) {
        return null;
    }

    if ($content === false) {
        return null;
    }
    return $content;
}

/**
 * Safe file rewrite.
 *
 * @param string $fileName
 * @param string $dataToSave
 */
function safe_file_rewrite(string $fileName, string $dataToSave): void {
    if ($fp = fopen($fileName, 'w')) {
        $startTime = microtime();
        do {
            $canWrite = flock($fp, LOCK_EX);
           // If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
           if(!$canWrite) usleep(round(rand(0, 100)*1000));
        } while ((!$canWrite)and((microtime()-$startTime) < 1000));

        // File was locked so now we can store information
        if ($canWrite) {
            fwrite($fp, $dataToSave);
            flock($fp, LOCK_UN);
        }
        fclose($fp);
    }
}

// Current URL
// --------------------------------------------------------------------------------

/**
 * Get the current url.
 *
 * @return string
 */
function current_url(): string {
    return 'http' . (is_ssl() ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

/**
 * Get current url safe for echoing.
 *
 * @return string
 */
function safe_current_url() {
    return xssafe(rawurlencode(current_url()));
}

/**
 * Get the current url without the query parameter string.
 *
 * @return string
 */
function current_url_no_query(): string {
    return strtok(current_url(), '?');
}

/**
 * Encode a URI component.
 *
 * @param string $str
 * @return string
 */
function encodeURIComponent(string $str): string {
    $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
    return strtr(rawurlencode($str), $revert);
}

/**
 * Encode a URI.
 *
 * @param string $uri
 * @return string
 */
function encodeURI($uri) {
    return preg_replace_callback("{[^0-9a-z_.!~*'();,/?:@&=+$#-]}i", function ($m) {
        return sprintf('%%%02X', ord($m[0]));
    }, $uri);
}

// URL FUNCTIONS
// --------------------------------------------------------------------------------

/**
 * Gets the address that the provided URL redirects to,
 * or FALSE if there's no redirect.
 *
 * @param string $url
 * @return string
 */
function get_redirect_url(string $url): string {
    $redirect_url = null;

    $url_parts = @parse_url($url);
    if (!$url_parts) return false;
    if (!isset($url_parts['host'])) return false; //can't process relative URLs
    if (!isset($url_parts['path'])) $url_parts['path'] = '/';

    $sock = fsockopen($url_parts['host'], (isset($url_parts['port']) ? (int)$url_parts['port'] : 80), $errno, $errstr, 30);
    if (!$sock) return false;

    $request = "HEAD " . $url_parts['path'] . (isset($url_parts['query']) ? '?'.$url_parts['query'] : '') . " HTTP/1.1\r\n";
    $request .= 'Host: ' . $url_parts['host'] . "\r\n";
    $request .= "Connection: Close\r\n\r\n";
    fwrite($sock, $request);
    $response = '';
    while(!feof($sock)) $response .= fread($sock, 8192);
    fclose($sock);

    if (preg_match('/^Location: (.+?)$/m', $response, $matches)){
        if ( substr($matches[1], 0, 1) == "/" )
            return $url_parts['scheme'] . "://" . $url_parts['host'] . trim($matches[1]);
        else
            return trim($matches[1]);

    } else {
        return false;
    }

}

/**
 * Follows and collects all redirects, in order, for the given URL.
 *
 * @param string $url
 * @return string[]
 */
function get_all_redirects(string $url): array {
    $redirects = array();
    while ($newurl = get_redirect_url($url)){
        if (in_array($newurl, $redirects)){
            break;
        }
        $redirects[] = $newurl;
        $url = $newurl;
    }
    return $redirects;
}

/**
 * Gets the address that the URL ultimately leads to.
 * Returns $url itself if it isn't a redirect.
 *
 * @param string $url
 * @return string
 */
function get_final_url(string $url): string {
    $redirects = get_all_redirects($url);
    if (count($redirects)>0){
        return array_pop($redirects);
    } else {
        return $url;
    }
}

/**
 * Parses a querystring and returns an associative key,value array.
 *
 * @param string $str the querystring
 * @return array
 */
function proper_parse_str(string $str): array {
    # result array
    $arr = array();

    # split on outer delimiter
    $pairs = explode('&', ltrim($str, '?'));

    # loop through each pair
    foreach ($pairs as $i) {
        # split into name and value
        list($name,$value) = explode('=', $i, 2);

        $name = remove_suffix($name, '[]');

        # if name already exists
        if( isset($arr[$name]) ) {
            # stick multiple values into an array
            if( is_array($arr[$name]) ) {
                $arr[$name][] = $value;
            } else {
                $arr[$name] = array($arr[$name], $value);
            }
        }
        # otherwise, simply stick it in a scalar
        else {
            $arr[$name] = $value;
        }
    }

    # return result array
    return $arr;
}

/**
 * Check if SSL is enabled.
 *
 * @return bool true if SSL is enabled on the current server, fale otherwise
 */
function is_ssl() {
    if (isset($_SERVER['HTTPS']) ) {
        return strtolower($_SERVER['HTTPS']) !== 'off';
    } elseif (isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT']) ) {
        return true;
    }
    return false;
}

/**
 * Is the current user a bot? - not guaranteed to work, should work for most standard bots though
 *
 * @return bool true if user is probably a bot, false otherwise
 */
function is_bot(): bool {
    // Match stuff usually in a bot's user agent
    if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|indexer|slurp|spider/i', $_SERVER['HTTP_USER_AGENT'])) {
        return true;
    }

    // Just in case for some extra user agents
    if (in_array($_SERVER['HTTP_USER_AGENT'], array(
        'facebookexternalhit/1.1 (+https://www.facebook.com/externalhit_uatext.php)',
        'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)',
        'Slackbot-LinkExpanding 1.0 (+https://api.slack.com/robots)',
        'Slack-ImgProxy 0.19 (+https://api.slack.com/robots)',
        'Slackbot 1.0(+https://api.slack.com/robots)',
    ))) {
        return true;
    }

    return false;
}

/**
 * Get the host of a URL. Works for URLs that don't have a protocol.
 *
 * @param string        $url
 * @return string       the host
 */
function full_domain(string $url): string {
    $parseUrl = parse_url(add_protocol(trim($url)));
    return trim($parseUrl['host'] ? $parseUrl['host'] : array_shift(explode('/', $parseUrl['path'], 2)));
}

/**
 * Get the domain of the given url with an https protocol in front and trailing slash
 *
 * @param string        $protocol='https'
 * @return string
 */
function domain_url(string $url, string $protocol='https') {
    return $protocol . '://'.full_domain($url).'/';
}

/**
 * If a URL doesn't have a protocol, or doesn't have the specified protocol specified,
 * then return the url with a protocol added.
 *
 * @param string        $url
 * @param string        $protocol='https'
 * @return string       URL with protocol added
 */
function add_protocol(string $url, string $protocol = 'https') {
    $protocol = rtrim($protocol, ':/');

    $parts = explode('//', $url, 2);
    $URN   = count($parts) === 1 ? $parts[0] : $parts[1];

    return $protocol . '://' . $URN;
}

// DATETIME FUNCTIONS
// --------------------------------------------------------------------------------

/**
 * Convert a timestamp to our preferred date format (D M j Y g:i:s a). The 'D' part,
 * for showing the day name, isn't included unless $show_day is true.
 *
 * @param int|float|string  $timestamp the UNIX timestamp as an int, float, or numeric string
 * @param bool              $no_time=false (optional) if true, the 'g:i:s a' part won't be included.
 * @param bool              $show_day=false (optional) if true, the 'D' part will be included
 * @return string           timestamp converted to date format
 */
function timeConvert($timestamp, bool $no_time = false, bool $show_day = false): string {
    static $_user_exists = null;

    if (!isset($_user_exists)) {
        $_user_exists = function_exists('user');
    }

    if (empty($timestamp) || (!is_int($timestamp) && !is_float($timestamp))) {
        return xssafe(strval($timestamp));
    }

    $format = '';

    if ($show_day) {
        $format .= 'D ';
    }

    $format .= 'M j Y ';

    if (!$no_time) {
        $format .= 'g:i:s a';
    }

    $format .= ' T';

    if ($_user_exists && user()->logged_in() && !empty(user()->prefs->get_php_name_timezone())) {
        $dt = new \DateTime();
        $dt->setTimezone(user()->prefs->get_php_timezone());
        $dt->setTimestamp(intval($timestamp));
        return xssafe($dt->format($format));
    }

    return xssafe((string) date($format, $timestamp));
}

/**
 * Human timing from UNIX timestamp.
 *
 * @param int           $time a UNIX timestamp
 * @param string        $suffix (optional) the suffix; default is 'ago' if past, 'from now' if future
 * @return string       the human timing string
 */
function human_timing(?int $time, string $suffix = null): ?string {
    if (!isset($time))
        return null;
    if (empty($time) || !is_int($time))
        return 'never';

    $time = time() - $time;
    $suffix = isset($suffix) ? $suffix : ($time < 0 ? 'from now' : 'ago');
    $time = abs($time);

    if ($time <= 1)
        return 'Just now';
    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return ($numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'')) . ' ' . $suffix;
    }
}

function get_timezones_by_region() {
    return [
        'U.S. Minor Outlying Islands' => [
            '(GMT-12:00) International Date Line West' => 'Pacific/Wake',
            '(GMT-11:00) Midway Atoll' => 'Pacific/Apia',
            '(GMT-11:00) American Samoa' => 'Pacific/Apia',
        ],
        'U.S. Non-Contiguous; French Polynesia' => [
            '(GMT-10:00) Hawaii' => 'Pacific/Honolulu',
            '(GMT-10:00) French Polynesia' => 'Pacific/Honolulu',
            '(GMT-09:00) Alaska' => 'America/Anchorage',
        ],
        'U.S. West; Canada West; Mexico' => [
            '(GMT-08:00) Pacific Time (US & Canada)' => 'America/Los_Angeles',
            '(GMT-08:00) Vancouver, BC' => 'America/Vancouver',
            '(GMT-08:00) Tijuana' => 'America/Los_Angeles',
            '(GMT-07:00) Arizona' => 'America/Phoenix',
            '(GMT-07:00) Chihuahua' => 'America/Chihuahua',
            '(GMT-07:00) La Paz' => 'America/Chihuahua',
            '(GMT-07:00) Mazatlan' => 'America/Chihuahua',
            '(GMT-07:00) Mountain Time (US & Canada)' => 'America/Denver',
        ],
        'U.S. Central/East; Canada Central/East; Mexico; Central America; Western South America' => [
            '(GMT-06:00) Central America' => 'America/Managua',
            '(GMT-06:00) Central Time (US & Canada)' => 'America/Chicago',
            '(GMT-06:00) Guadalajara' => 'America/Mexico_City',
            '(GMT-06:00) Mexico City' => 'America/Mexico_City',
            '(GMT-06:00) Monterrey' => 'America/Mexico_City',
            '(GMT-06:00) Saskatchewan' => 'America/Regina',
            '(GMT-05:00) Bogota' => 'America/Bogota',
            '(GMT-05:00) Eastern Time (US & Canada)' => 'America/New_York',
            '(GMT-05:00) Indiana (East)' => 'America/Indiana/Indianapolis',
            '(GMT-05:00) Lima' => 'America/Bogota',
            '(GMT-05:00) Quito' => 'America/Bogota',
        ],
        'Canada East; Central/East South America; Greenland' => [
            '(GMT-04:00) Atlantic Time (Canada)' => 'America/Halifax',
            '(GMT-04:00) Puerto Rico' => 'America/Puerto_Rico',
            '(GMT-04:00) U.S. Virgin Islands' => 'America/Puerto_Rico',
            '(GMT-04:00) British Virgin Islands' => 'America/Puerto_Rico',
            '(GMT-04:00) Caracas' => 'America/Caracas',
            '(GMT-04:00) La Paz' => 'America/Caracas',
            '(GMT-04:00) Santiago' => 'America/Santiago',
            '(GMT-03:30) Newfoundland' => 'America/St_Johns',
            '(GMT-03:00) Brasilia' => 'America/Sao_Paulo',
            '(GMT-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires',
            '(GMT-03:00) Georgetown' => 'America/Argentina/Buenos_Aires',
            '(GMT-03:00) Greenland' => 'America/Godthab',
        ],
        'Britain; Mid-Atlantic; West Africa' => [
            '(GMT-02:00) Mid-Atlantic' => 'America/Noronha',
            '(GMT-01:00) Azores' => 'Atlantic/Azores',
            '(GMT-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde',
            '(GMT) Casablanca' => 'Africa/Casablanca',
            '(GMT) Edinburgh' => 'Europe/London',
            '(GMT) Greenwich Mean Time : Dublin' => 'Europe/London',
            '(GMT) Lisbon' => 'Europe/London',
            '(GMT) London' => 'Europe/London',
            '(GMT) Monrovia' => 'Africa/Casablanca',
        ],
        'West Europe; West Africa' => [
            '(GMT+01:00) Amsterdam' => 'Europe/Berlin',
            '(GMT+01:00) Belgrade' => 'Europe/Belgrade',
            '(GMT+01:00) Berlin' => 'Europe/Berlin',
            '(GMT+01:00) Bern' => 'Europe/Berlin',
            '(GMT+01:00) Bratislava' => 'Europe/Belgrade',
            '(GMT+01:00) Brussels' => 'Europe/Paris',
            '(GMT+01:00) Budapest' => 'Europe/Belgrade',
            '(GMT+01:00) Copenhagen' => 'Europe/Paris',
            '(GMT+01:00) Ljubljana' => 'Europe/Belgrade',
            '(GMT+01:00) Madrid' => 'Europe/Paris',
            '(GMT+01:00) Paris' => 'Europe/Paris',
            '(GMT+01:00) Prague' => 'Europe/Belgrade',
            '(GMT+01:00) Rome' => 'Europe/Berlin',
            '(GMT+01:00) Sarajevo' => 'Europe/Sarajevo',
            '(GMT+01:00) Skopje' => 'Europe/Sarajevo',
            '(GMT+01:00) Stockholm' => 'Europe/Berlin',
            '(GMT+01:00) Vienna' => 'Europe/Berlin',
            '(GMT+01:00) Warsaw' => 'Europe/Sarajevo',
            '(GMT+01:00) West Central Africa' => 'Africa/Lagos',
            '(GMT+01:00) Zagreb' => 'Europe/Sarajevo',
        ],
        'East Europe; Central Africa; Middle East' => [
            '(GMT+02:00) Athens' => 'Europe/Istanbul',
            '(GMT+02:00) Bucharest' => 'Europe/Bucharest',
            '(GMT+02:00) Cairo' => 'Africa/Cairo',
            '(GMT+02:00) Harare' => 'Africa/Johannesburg',
            '(GMT+02:00) Helsinki' => 'Europe/Helsinki',
            '(GMT+02:00) Istanbul' => 'Europe/Istanbul',
            '(GMT+02:00) Jerusalem' => 'Asia/Jerusalem',
            '(GMT+02:00) Kyiv' => 'Europe/Helsinki',
            '(GMT+02:00) Minsk' => 'Europe/Istanbul',
            '(GMT+02:00) Pretoria' => 'Africa/Johannesburg',
            '(GMT+02:00) Riga' => 'Europe/Helsinki',
            '(GMT+02:00) Sofia' => 'Europe/Helsinki',
            '(GMT+02:00) Tallinn' => 'Europe/Helsinki',
            '(GMT+02:00) Vilnius' => 'Europe/Helsinki',
        ],
        'East Europe; East Africa; Middle East; West Russia' => [
            '(GMT+03:00) Baghdad' => 'Asia/Baghdad',
            '(GMT+03:00) Kuwait' => 'Asia/Riyadh',
            '(GMT+03:00) Moscow' => 'Europe/Moscow',
            '(GMT+03:00) Nairobi' => 'Africa/Nairobi',
            '(GMT+03:00) Riyadh' => 'Asia/Riyadh',
            '(GMT+03:00) St. Petersburg' => 'Europe/Moscow',
            '(GMT+03:00) Volgograd' => 'Europe/Moscow',
            '(GMT+03:30) Tehran' => 'Asia/Tehran',
            '(GMT+04:00) Abu Dhabi' => 'Asia/Muscat',
            '(GMT+04:00) Baku' => 'Asia/Tbilisi',
            '(GMT+04:00) Muscat' => 'Asia/Muscat',
            '(GMT+04:00) Tbilisi' => 'Asia/Tbilisi',
            '(GMT+04:00) Yerevan' => 'Asia/Tbilisi',
            '(GMT+04:00) Dubai' => 'Asia/Dubai',
        ],
        'West/Central Asia; India; Russia' => [
            '(GMT+04:30) Kabul' => 'Asia/Kabul',
            '(GMT+05:00) Ekaterinburg' => 'Asia/Yekaterinburg',
            '(GMT+05:00) Islamabad' => 'Asia/Karachi',
            '(GMT+05:00) Karachi' => 'Asia/Karachi',
            '(GMT+05:00) Tashkent' => 'Asia/Karachi',
            '(GMT+05:30) Chennai' => 'Asia/Calcutta',
            '(GMT+05:30) Kolkata' => 'Asia/Calcutta',
            '(GMT+05:30) Mumbai' => 'Asia/Calcutta',
            '(GMT+05:30) New Delhi' => 'Asia/Calcutta',
            '(GMT+05:45) Kathmandu' => 'Asia/Katmandu',
            '(GMT+06:00) Almaty' => 'Asia/Novosibirsk',
            '(GMT+06:00) Astana' => 'Asia/Dhaka',
            '(GMT+06:00) Dhaka' => 'Asia/Dhaka',
            '(GMT+06:00) Novosibirsk' => 'Asia/Novosibirsk',
        ],
        'North Asia; South East Asia; Russia' => [
            '(GMT+06:00) Sri Jayawardenepura' => 'Asia/Colombo',
            '(GMT+06:30) Rangoon' => 'Asia/Rangoon',
            '(GMT+07:00) Bangkok' => 'Asia/Bangkok',
            '(GMT+07:00) Hanoi' => 'Asia/Bangkok',
            '(GMT+07:00) Jakarta' => 'Asia/Bangkok',
            '(GMT+07:00) Krasnoyarsk' => 'Asia/Krasnoyarsk',
        ],
        'China; Russia; Australia' => [
            '(GMT+08:00) Beijing' => 'Asia/Hong_Kong',
            '(GMT+08:00) Chongqing' => 'Asia/Hong_Kong',
            '(GMT+08:00) Hong Kong' => 'Asia/Hong_Kong',
            '(GMT+08:00) Shanghai' => 'Asia/Shanghai',
            '(GMT+08:00) Irkutsk' => 'Asia/Irkutsk',
            '(GMT+08:00) Kuala Lumpur' => 'Asia/Singapore',
            '(GMT+08:00) Perth' => 'Australia/Perth',
            '(GMT+08:00) Singapore' => 'Asia/Singapore',
            '(GMT+08:00) Taipei' => 'Asia/Taipei',
            '(GMT+08:00) Ulaan Bataar' => 'Asia/Irkutsk',
            '(GMT+08:00) Urumqi' => 'Asia/Hong_Kong',

        ],
        'East Asia; Koreas; Japan; East Russia; Australia; U.S. Guam' => [
            '(GMT+09:00) Osaka' => 'Asia/Tokyo',
            '(GMT+09:00) Sapporo' => 'Asia/Tokyo',
            '(GMT+09:00) Tokyo' => 'Asia/Tokyo',
            '(GMT+09:00) Kyoto' => 'Asia/Tokyo',
            '(GMT+09:00) Shibuya' => 'Asia/Tokyo',
            '(GMT+09:00) Seoul' => 'Asia/Seoul',
            '(GMT+09:00) Yakutsk' => 'Asia/Yakutsk',
            '(GMT+09:30) Adelaide' => 'Australia/Adelaide',
            '(GMT+09:30) Darwin' => 'Australia/Darwin',
            '(GMT+10:00) Brisbane' => 'Australia/Brisbane',
            '(GMT+10:00) Canberra' => 'Australia/Sydney',
            '(GMT+10:00) Guam' => 'Pacific/Guam',
            '(GMT+10:00) U.S. Northern Mariana Islands' => 'Pacific/Guam',
            '(GMT+10:00) Hobart' => 'Australia/Hobart',
            '(GMT+10:00) Melbourne' => 'Australia/Sydney',
            '(GMT+10:00) Port Moresby' => 'Pacific/Guam',
            '(GMT+10:00) Sydney' => 'Australia/Sydney',
            '(GMT+10:00) Vladivostok' => 'Asia/Vladivostok',
            '(GMT+11:00) Magadan' => 'Asia/Magadan',
            '(GMT+11:00) New Caledonia' => 'Asia/Magadan',
            '(GMT+11:00) Solomon Is.' => 'Asia/Magadan',
        ],
        'East Russia; West Pacific Islands; New Zealand' => [
            '(GMT+12:00) Auckland' => 'Pacific/Auckland',
            '(GMT+12:00) Fiji' => 'Pacific/Fiji',
            '(GMT+12:00) Kamchatka' => 'Pacific/Fiji',
            '(GMT+12:00) Marshall Is.' => 'Pacific/Fiji',
            '(GMT+12:00) Wellington' => 'Pacific/Auckland',
            '(GMT+13:00) Nuku\'alofa' => 'Pacific/Tongatapu',
        ]
    ];
}

const TZ_COMPOSITE_SEP = '|';

function tz_create_composite($php_name, $display_name) {
    return $php_name . TZ_COMPOSITE_SEP . $display_name;
}

function tz_split_composite($composite_name) {
    if (empty($composite_name)) {
        return [
            'php_name' => null,
            'display_name' => null,
        ];
    }

    list($php_name, $display_name) = explode(TZ_COMPOSITE_SEP, $composite_name);

    return [
        'php_name' => $php_name,
        'display_name' => $display_name,
    ];
}

// MARKDOWN FUNCTIONS
// --------------------------------------------------------------------------------

/**
 * Converts some text to markdown.
 *
 * @param string    $text the text to convert
 * @param bool      $allow_html should HTML be allowed? if false, HTML within $text will be escaped
 * @return string   the $text converted to markdown
 */
function markdown(string $text, bool $allow_html = false) {
    static $parsedown = null;
    if ($parsedown === null) {
        include BOOTSTRAP_ROOT . 'common/Parsedown.php';
        $parsedown = new Parsedown();
    }

    $parsedown->setBreaksEnabled(true); // should convert \n to <br/>?
    $parsedown->setMarkupEscaped(!$allow_html); // should escape HTML?
    $parsedown->setUrlsLinked(true); // should automatically link urls?

    return $parsedown->text($text);
}

// STRING FUNCTIONS
// --------------------------------------------------------------------------------

/**
 * Checks if a string starts with a value.
 *
 * @param string    $haystack the target string
 * @param string    $needle the needle value
 * @param bool      $ci if true, performs operation as case-insensitive
 * @return bool     returns true if $haystack starts with $needle, false otherwise
 */
function startsWith(?string $haystack, string $needle, bool $ci=false): string {
    if (!isset($haystack))
        return false;
    if ($needle === "")
        return true;
    if ($ci)
        return stripos($haystack, $needle) === 0;
    else
        return strpos($haystack, $needle) === 0;
}

/**
 * Checks if a string ends with a value.
 *
 * @param string    $haystack the target string
 * @param string    $needle the needle value
 * @param bool      $ci if true, performs operation as case-insensitive
 * @return bool     returns true if $haystack ends with $needle, false otherwise
 */
function endsWith(?string $haystack, string $needle, bool $ci=false): string {
    if (!isset($haystack))
        return false;
    if ($needle === "")
        return true;
    if ($ci)
        return strtolower(substr($haystack, -strlen($needle))) === strtolower($needle);
    else
        return substr($haystack, -strlen($needle)) === $needle;
}

/**
 * Checks if a string contains a value.
 *
 * @param string    $haystack the target string
 * @param string    $needle the needle value
 * @param bool      $ci if true, performs operation as case-insensitive
 * @return bool     returns true if $haystack contains $needle, false otherwise
 */
function str_contains(?string $haystack, string $needle, bool $ci=false): string {
    if (!isset($haystack))
        return false;
    if ($needle === "")
        return true;
    if ($ci)
        return stripos($haystack, $needle) !== FALSE;
    else
        return strpos($haystack, $needle) !== FALSE;
}

/**
 * If the target string starts with $prefix, the target string will
 * be returned with the prefix replaced with $replacement.
 *
 * @param string            $str the target string
 * @param string|string[]   $prefix the prefix
 * @param string            $replacement the replacement
 * @param bool              $ci if true, performs operation as case-insensitive
 * @return string           the string with the prefix replaced
 */
function replace_prefix(string $str, $prefix, string $replacement = '', bool $ci=false): string {
    if (empty($prefix)) {
        return $str;
    }

    $cmp_str = $ci ? strtolower($str) : $str;
    $cmp_fixes = $ci ? array_map('strtolower', (array) $prefix) : ((array) $prefix);

    foreach ($cmp_fixes as $cmp_fix) {
        if (substr($cmp_str, 0, strlen($cmp_fix)) === $cmp_fix) {
            $str = $replacement . substr($str, strlen($cmp_fix));
            break;
        }
    }

    return $str;
}

/**
 * If the target string starts with $prefix, the target string will
 * be returned with the prefix removed.
 *
 * @param string            $str the target string
 * @param string|string[]   $prefix the prefix
 * @param bool              $ci if true, performs operation as case-insensitive
 * @return string           the string without the prefix
 */
function remove_prefix(string $str, $prefix, bool $ci=false): string {
    return replace_prefix($str, $prefix, '', $ci);
}

/**
 * If the target string ends with $suffix, the target string will
 * be returned with the suffix replaced with $replacement.
 *
 * @param string            $str the target string
 * @param string|string[]   $suffix the suffix(es)
 * @param string            $replacement the replacement
 * @param bool              $ci if true, performs operation as case-insensitive
 * @return string           the string with the suffix replaced
 */
function replace_suffix(string $str, $suffix, string $replacement = '', bool $ci=false): string {
    if (empty($suffix)) {
        return $str;
    }

    $cmp_str = $ci ? strtolower($str) : $str;
    $cmp_fixes = $ci ? array_map('strtolower', (array) $suffix) : ((array) $suffix);

    foreach ($cmp_fixes as $cmp_fix) {
        if (substr($cmp_str, -strlen($cmp_fix)) === $cmp_fix) {
            $str = substr($str, 0, -strlen($cmp_fix)) . $replacement;
            break;
        }
    }

    return $str;
}

/**
 * If the target string ends with $suffix, the target string will
 * be returned with the suffix removed.
 *
 * @param string            $str the target string
 * @param string|string[]   $suffix the suffix(es)
 * @param bool              $ci if true, performs operation as case-insensitive
 * @return string           the string without the suffix
 */
function remove_suffix(string $str, $suffix, bool $ci=false): string {
    return replace_suffix($str, $suffix, '', $ci);
}

/**
 * Cuts off a string at a specified character limit and appends `&hellip;` if the
 * text had to be cut off.
 *
 * @param string    $str the target string
 * @param int       $char_count the character limit
 * @return string   the teaser text
 */
function teaser(string $str, int $char_count): string {
    if (strlen($str) <= $char_count) {
        return $str;
    } else {
        return rtrim(substr($str, 0, strpos($str, ' ', $char_count)), '.') . '&hellip;';
    }
}

/**
 * Returns everything before the given value in a string.
 *
 * @param string    $str the target string
 * @param string    $search the separator string
 * @return string   the divided string
 */
function str_before(string $str, string $search): string {
    return $search == '' ? $str : explode($search, $str, 2)[0];
}

/**
 * Returns everything after the given value in a string.
 *
 * @param string    $str the target string
 * @param string    $search the separator string
 * @return string   the divided string
 */
function str_after(string $str, string $search): string {
    return $search == '' ? $str : last(explode($search, $str, 2));
}

// ILIST PACK/UNPACK
// --------------------------------------------------------------------------------

/**
 * Uncompresses an int list compressed with IListPack()
 *
 * @param string    $raw compressed int list
 * @return int[]    list of ints
 */
function IListUnpack(string $raw): array {
    $list = [];
    $skip = 0;

    $raw = ltrim($raw, '@');

    if (valid_int($raw)) {
        return [intval($raw)];
    } else if (str_contains($raw, '|') || str_contains($raw, ',')) {
        return array_map('intval', explode('|', str_replace(',','|',$raw)));
    }

    $raw = str_split($raw);

    $xset = function($d, $dl, $start) use ($raw, &$list) {
        $x = $start;
        $seq = false;
        $last_append = null;

        for (; $x < count($raw); $x++) {
            $c = $raw[$x];
            $cc = ord($c);

            if (48 <= $cc && $cc <= 57) {
                if ($seq) {
                    $seq_last = intval($d . $c);

                    for ($k = $last_append+1; $k <= $seq_last; $k++) {
                        $list[] = $k;
                    }

                    $last_append = $seq_last;
                    $seq = false;
                } else {
                    $list[] = ($last_append = intval($d . $c));
                }
            } else if ($c == '~') {
                $seq = true;
            } else {
                break;
            }
        }

        return $x - $start;
    };

    $yset = function($d, $dl, $start) use ($raw, &$list) {
        $x = $start;

        $number = '';

        for (; $x < count($raw); $x++) {
            $c = $raw[$x];
            $cc = ord($c);

            if (48 <= $cc && $cc <= 57) {
                $number .= $c;
                if (strlen($number) >= $d) {
                    $list[] = intval($number);
                    $number = '';
                }
            } else {
                break;
            }
        }

        return $x - $start;
    };

    for ($i = 0, $ilen = count($raw); $i < $ilen; $i++) {
        $c = $raw[$i];
        $cc = ord($c);

        if (97 <= $cc && $cc <= (97+25)) {
            $dl = ($cc - 97) + 1;
            $d = '';

            for ($j = 1; $j < ($dl+1); $j++, $skip++) {
                $d .= $raw[$i + $j];
            }

            $d = intval($d);

            $skip += $xset($d,$dl,$i+(++$skip));
        } else if ($c == '!' || $c == '#') {
            $d = '';

            for ($j = $i+1; $j < $ilen; $j++, $skip++) {
                $jc = $raw[$j];
                if ($jc == ':') {
                    $skip += 1;
                    break;
                }
                $d .= $jc;
            }

            $dl = strlen($d);
            $d = intval($d);

            if ($c == '!') {
                $skip += $xset($d,$dl,$i+(++$skip));
            } else if ($c == '#') {
                $skip += $yset($d,$dl,$i+(++$skip));
            }
        }

        $i += $skip-1;
        $skip = 0;
    }

    sort($list);
    return $list;
}

// TREE DISPLAY
// --------------------------------------------------------------------------------

/**
 * Create a file tree-like display for the given tree.
 *
 * @param array     $map the tree structure
 * @return string   the tree output string
 */
function tree_display(array $map, $lvl = 0, $LVLSTOR = [], $parent_clr = true, $parent_keylen=null): string {
    static $CH_PATH = '│';
    static $CH_ITEM = '├─ ';
    static $CH_LAST = '└─ ';

    $lvl_bar = function($lvl, $last) use ($CH_PATH, &$LVLSTOR, $parent_clr, $parent_keylen) {
        if (isset($LVLSTOR[$lvl])) {
            $ret = $LVLSTOR[$lvl];
        } else {
            if ($lvl == 0) {
                $ret = $last = '';
            } else if ($lvl == 1) {
                $ret = ($parent_keylen == 1) ? '' : ' ';
            } else if (isset($LVLSTOR[$lvl - 1])) {
                $ret = $LVLSTOR[$lvl - 1];
                $ret .= $parent_clr ? ' ' : $CH_PATH;
                $ret .= ($parent_keylen == 1) ? '  ' : '   ';
            } else {
                $ret = ''; // should be unreachable
            }
            $LVLSTOR[$lvl] = $ret;
        }
        return $ret . $last;
    };

    $map_keys = array_keys($map);
    $last_k = end($map_keys);
    $str = '';

    foreach ($map as $k => $v) {
        $last = ($k === $last_k ? $CH_LAST : $CH_ITEM);
        $str .= $lvl_bar($lvl, $last) . (is_string($v) ? $v : $k) . "\n";
        if (is_array($v)) {
            $str .= tree_display($v, $lvl + 1, $LVLSTOR, $k === $last_k, strlen($k));
        }
    }

    return $str;
}