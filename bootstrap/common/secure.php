<?php
// **********************************************************************
// This file contains helper functions and classes
// to help out with security
// **********************************************************************

/**
 * Get the value of key(s) in array without PHP complaining if the index is undefined.
 *
 * @param array|ArrayAccess|null    $source
 * @param string|string[]           $name the key or an array of keys
 * @param callable                  $map_func (optional) the return value, if not null, will be
 *                                  mapped with this callable
 * @return mixed                    the value from the given key, or array of values from the given
 *                                  array of keys, or returns null if the key is undefined in the
 *                                  source
 */
function from($source, $name, callable $map_func = null) {
    if (!isset($source))
        return null;
    if (is_array($name)) {
        $data = array();
        foreach ($name as $k) {
            $value = isset($source[$k]) ? $source[$k] : null;
            if (isset($map_func) && isset($value))
                $value = call_user_func_array($map_func, array($value));
            $data[$k] = $value;
        }
        return $data;
    }
    $value = isset($source[$name]) ? $source[$name] : null;
    if (isset($map_func) && isset($value))
        $value = call_user_func_array($map_func, array($value));
    return $value;
}

/**
 * Store the value to the array with the specified path, creating child arrays as necessary.
 *
 * @param array     &$source the source array to modify
 * @param string    $path the key path separated by periods
 * @param mixed     $value=[] (optional) set the last key in $path to this value
 * @return array    returns the same source array
 */
function to(array &$source, string $path, $value=[]): array {
    if (func_num_args() < 2) {
        throw new BadFunctionCallException(
            'to($source, $name, $value) requires at least three parameters');
    }

    $args = explode('.', $path);
    $loc  = &$source;

    while (!empty($args)) {
        $name = array_shift($args);

        if (!isset($loc[$name])) {
            $loc[$name] = [];
        }

        $loc = &$loc[$name];
    }

    $loc = $value;

    return $source;
}

// ==========================================================================================
// XSS MITIGATION FUNCTIONS
// ==========================================================================================

/**
 * Makes the given data XSS-safe for echoing by escaping HTML.
 *
 * @param string|int|double|float|bool $data the data to escape
 * @return mixed the escaped data, returns empty string for unsupported $data type
 */
function xssafe($data, string $encoding='UTF-8') {
    if (is_string($data)) {
        return htmlspecialchars($data, ENT_QUOTES | ENT_HTML5, $encoding);
    } else if (is_int($data)) {
        return intval($data);
    } else if (is_double($data)) {
        return doubleval($data);
    } else if (is_float($data)) {
        return floatval($data);
    } else if (is_bool($data)) {
        return boolval($data);
    } else if (is_null($data)) {
        return null;
    } else if (is_array($data)) {
        if (is_assoc($data)) {
            $ret = [];
            foreach ($data as $k => $v) {
                $ret[xssafe($k)] = xssafe($v);
            }
            return $ret;
        }
        return array_map('xssafe', $data);
    } else {
        return '';
    }
}

/**
 * Safely echo a non-html string. Use this instead of
 * echo for any output that should not contain HTML.
 *
 * @param string|string[] $data data to escape and echo
 * @param string $sep=', ' join $data with this delimeter if $data is string[]
 */
function xecho($data, string $sep = ', '): void {
    if (!isset($data)) {
        return;
    }

    if (is_array($data)) {
        $data = implode($sep, $data);
    }

    echo xssafe($data);
}

// ECHO IF
// --------------------------------------------------------------------------------

/**
 * echo the $data if $cond is truthy.
 *
 * @param mixed $data
 * @param mixed $cond if truthy, $data will be echoed
 */
function echo_if($data, $cond): void {
    if ($cond) {
        echo $data;
    }
}

/**
 * xecho the $data if $cond is truthy.
 *
 * @param mixed $data
 * @param mixed $cond if truthy, $data will be xechoed
 */
function xecho_if($data, $cond): void {
    if ($cond) {
        xecho($data);
    }
}

// ==========================================================================================
// GET CLIENT IP & INFO
// ==========================================================================================

/**
 * Get the client's user agent string.
 *
 * @return string
 */
function get_user_agent(): string {
    return $_SERVER['HTTP_USER_AGENT'] ?? '';
}

/**
 * Returns true if the client specified a "Do not tracker" header
 * @return bool true if DNT is enabled, false otherwise
 */
function is_dnt() {
    return isset($_SERVER['HTTP_DNT']) && ($_SERVER['HTTP_DNT'] === "1" || $_SERVER['HTTP_DNT'] == 1);
}

/**
 * Get IP address.
 *
 * @return string the remote IP address
 */
function get_ip(): ?string {
    return isset($_SERVER['REMOTE_ADDR']) ? trim($_SERVER['REMOTE_ADDR']) : get_client_ip();
}

/**
 * Get Client IP address.
 *
 * Disclaimer: $_SERVER['REMOTE_ADDR'] is the most reliable source for
 * an IP address since HTTP_* headers can be easily spoofed. The purpose
 * of this function is to attempt to determine the IP address of a client
 * sitting behind a proxy (if any).
 *
 * @return string the client's IP address (probably)
 */
function get_client_ip(): ?string {
    $options = [
        'HTTP_CLIENT_IP',
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED',
        'HTTP_X_CLUSTER_CLIENT_IP',
        'HTTP_FORWARDED_FOR',
        'HTTP_FORWARDED',
        'REMOTE_ADDR',
    ];
    foreach ($options as $key){
        if (isset($_SERVER[$key])){
            foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe

                if (validate_ip($ip)){
                    return $ip;
                }
            }
        }
    }
    return null;
}

/**
 * Validate IP address.
 *
 * @param string IP address to validate.
 * @return bool true if valid IP address, false otherwise
 */
function validate_ip(string $ip): bool {
    if (empty($ip))
        return false;
    return (filter_var($ip, FILTER_VALIDATE_IP,
                        FILTER_FLAG_NO_PRIV_RANGE |
                        FILTER_FLAG_NO_RES_RANGE) !== false);
}

function browser_info() {
    static $_browser = null;
    if (!isset($_browser)) {
        require BOOTSTRAP_ROOT . "common/Browser.php";
        $_browser = new \Browser();
    }
    return $_browser;
}

// ==========================================================================================
// COOKIE FUNCTIONS
// ==========================================================================================

/**
 * Set a cookie.
 *
 * @param string    $name cookie name
 * @param string    $value cookie value
 * @param int       $expire=31536000 time-to-expire in seconds
 * @param string    $path='/' cookie path
 */
function set_cookie(string $name, string $value, int $expire = 31536000, string $path = '/', $domain = null): void {
    setcookie($name, $value, $expire === 0 ? 0 : time() + $expire, $path, $domain, is_ssl(), true);
}

/**
 * Get the value of a cookie
 *
 * @param string $name the name of the cookie
 * @return string|null the cookie value or null if not found
 */
function get_cookie(string $name): ?string {
    return from($_COOKIE, $name);
}

/**
 * Check if a cookie exists.
 *
 * @param string    $name the name of the cookie
 * @return bool     true if exists, false otherwisse
 */
function has_cookie(string $name): bool {
    return isset($_COOKIE[$name]);
}

/**
 * Delete a cookie.
 *
 * @param string[varargs] list of cookies to delete
 */
function delete_cookie(): void {
    $names = func_get_args();
    foreach ($names as $name) {
        setcookie($name, "", 1);
        setcookie($name, false);
        unset($_COOKIE[$name]);
    }
}

// ==========================================================================================
// MEMCACHED
// ==========================================================================================

/**
 * An abstraction for memcached/memcache. Use cached_available() to check if the cache is available
 * for usage.
 *
 * @param string|string[]   $k (optional) string key, or string array of keys
 * @param mixed             $v (optional) value
 * @param int               $expires if greater than 2592000, it'll be considered a unix timestamp.
 *                          Otherwise it'll be considered time-to-expire in seconds. If this
 *                          parameter is not given, then there will be no expiry.
 *                          If set to `-1`, the $value parameter will be ignored and the key will be
 *                          deleted
 * @return mixed|bool       If only $k is specified, then the value for the given key will be
 *                          returned or `false` if the key was not found.
 *                          If $k is an array, then an associative array of key-value pairs will be
 *                          returned, unknown keys not included.
 *                          If only $k is specified and the key(s) start with `?` then a boolean
 *                          value will be returned indicating whether or not it exists.
 *                          If $k and $v are specified (and optionally $expires), a boolean value
 *                          will be returned indicating if the set operation was succesful.
 *                          If no parameters are specified, then the resource object will be
 *                          returned.
 */
function cached() {
    static $_resource = null; // memcached or memcache
    static $_d = false; // true if memcached, false if memcache

    // 0. CREATE RESOURCE IF NOT EXISTS
    //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (!isset($_resource)) {
        if (class_exists('Memcached')) {
            $_resource = new \Memcached();
            $_d = true;
        } else if (class_exists('Memcache')) {
            $_resource = new \Memcache();
            $_d = false;
        } else {
            cached_available(false);
            return false;
        }

        // addServer() returns true/false for success for both memcache and memcached
        cached_available( $_resource->addServer(MEMCACHED_HOST, MEMCACHED_PORT) );
    }

    // 1. VALIDATE/NORMALIZE KEY parameter
    //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    // key is always first argument, other args vary depending on the amount of arguments, the $kT
    // value, and the types of the args
    $k = func_num_args() === 0 ? null : func_get_arg(0);

    $kT = 0; // keyType (0 -> string, 1 -> string seq array, 2 -> K-V pair assoc array)

    $kE = []; // keyExists -> list of keys to check if exists

    if (is_string($k)) {
        $kT = 0;
        if (strlen($k) === 0) {
            throw new InvalidArgumentException('cached() - bad key, can\'t be empty');
        }
        if ($k[0] === '?') {
            $k = substr($k, 1);
            $kE[MEMCACHED_PREFIX . $k] = 1;
        }
        $k = MEMCACHED_PREFIX . $k;
        if (strlen($k) > 250) {
            throw new InvalidArgumentException('cached() - bad key, too long (max: '.
                (250 - strlen(MEMCACHED_PREFIX)) .')');
        }
    } else if (is_array($k)) {
        if (is_assoc($k)) {
            $kT = 2;
        } else {
            $kT = 1;
        }

        $k_new = [];

        foreach ($k as $kk => $kv) {
            if ($kT === 2) {
                if (!is_string($kk)) {
                    throw new InvalidArgumentException('cached() - bad key, must be string(s)');
                }
                if (strlen($kk) === 0) {
                    throw new InvalidArgumentException('cached() - bad key, can\'t be empty');
                }
                if ($kk[0] === '?') {
                    $kk = substr($kk, 1);
                    $kE[MEMCACHED_PREFIX . $kk] = 1;
                }
                if (strlen($kk) > 250) {
                    throw new InvalidArgumentException('cached() - bad key, too long (max: '.
                        (250 - strlen(MEMCACHED_PREFIX)) .')');
                }
                $k_new[MEMCACHED_PREFIX . $kk] = $kv;
            } else {
                if (!is_string($kv)) {
                    throw new InvalidArgumentException('cached() - bad key, must be strings');
                }
                if (strlen($kv) === 0) {
                    throw new InvalidArgumentException('cached() - bad key, can\'t be empty');
                }
                if ($kv[0] === '?') {
                    $kv = substr($kv, 1);
                    $kE[MEMCACHED_PREFIX . $kv] = 1;
                }
                if (strlen($kv) > 250) {
                    throw new InvalidArgumentException('cached() - bad key, too long (max: '.
                        (250 - strlen(MEMCACHED_PREFIX)) .')');
                }
                $k_new[] = MEMCACHED_PREFIX . $kv;
            }
        }

        $k = $k_new;
    } else if (func_num_args() !== 0) {
        throw new InvalidArgumentException('cached() - bad key, must be string(s)');
    }

    // 2. DETERMINE RESOURCE FUNCTION CALL NAME AND ARGUMENTS
    //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    $fname = null; // function name
    $fargs = null; // function arguments
    $feach = false; // function each
    $retKV = false; // true if the function is expected to return a KV-pair
    $flags = false;

    switch (func_num_args()) {
        case 0:
            // no parameters - return resource object
            return $_resource;
        case 1:
            // One parameter, either:
            //   - Get operation            with K (kT:0)
            //   - Multi-Get operation      with multi-K (kT:1)
            //   - Multi-Set operation      with K-V (kT:2)             no Expiry

            if ($kT === 0) {
                $fname = 'get';
                $fargs = ($_d ? [$k] : [$k, &$flags]);
            } else if ($kT === 1) {
                // don't use getMulti() because it sucks: it returns NULL for keys that don't exist
                // meaning we can't tell the difference between keys whose value is NULL and keys
                // that actually don't exist
                $fname = 'get';
                $fargs = ($_d ? array_map('cast_array', $k) : [$k]);
                $feach = $_d;
                $retKV = true;
            } else if ($kT === 2) {
                $fname = ($_d ? 'setMulti' : 'set');
                $fargs = ($_d ? [$k] : array_map_to_seq($k));
                $feach = !$_d;
            }

            break;
        case 2:
            // Two parameters, either:
            //   - Set operation            with K (kT:0) and V         no Expiry
            //   - Multi-Set operation      with K-V (kT:2)             with Expiry
            //   - Multi-Delete operation   with multi-K (kT:1)         Expiry:-1

            if ($kT === 0) {
                $v = func_get_arg(1);
                $fname = 'set';
                $fargs = $_d ? [$k, $v] : [$k, $v, 0];
            } else if ($kT === 1) {
                if (func_get_arg(1) === -1) {
                    if ($_d) {
                        $fname = 'deleteMulti';
                        $fargs = [$k];
                    } else {
                        $fname = 'delete';
                        $fargs = array_map('cast_array', $k); // k is an array of strings here
                        $feach = true;
                    }
                } else {
                    throw new InvalidArgumentException('cached() - bad key, must be string or K-V pair');
                }
            } else if ($kT === 2) {
                $expires = func_get_arg(1);

                if ($expires <= 0) {
                    throw new InvalidArgumentException('cached() - bad expiry');
                } else {
                    if ($_d) {
                        $fname = 'setMulti';
                        $fargs = [$k, $expires];
                    } else {
                        $fname = 'set';
                        $fargs = array_map_to_seq($k, null, [0, $expires]);
                        $feach = true;
                    }
                }
            }
            break;
        case 3:
            // Three parameters: either:
            //   - Set operation            with K (kT:0), V,           with Expiry
            //   - Delete operation         with K (kT:0), V:?          Expiry:-1

            $v = func_get_arg(1);
            $expires = func_get_arg(2);

            if ($kT === 0) {
                if ($expires === -1) {
                    $fname = 'delete';
                    $fargs = [$k];
                } else if ($expires >= 0) {
                    $fname = 'set';
                    $fargs = $_d ? [$k, $v, $expires] : [$k, $v, 0, $expires];
                } else {
                    throw new InvalidArgumentException('cached() - bad expiry');
                }
            } else if ($kT === 1) {
                throw new InvalidArgumentException('cached() - invalid key type for 3 parameters');
            } else if ($kT === 2) {
                throw new InvalidArgumentException('cached() - invalid key type for 3 parameters');
            }

            break;
        default:
            throw new \BadFunctionCallException('cached() - too many arguments');
    }

    // 3. RESOURCE FUNCTION CALL
    //    ~~~~~~~~~~~~~~~~~~~~~~

    if (empty($fname) || empty($fargs)) {
        // should never reach this point, if it does then something went wrong in the section above
        throw new \LogicException('cached() - something went wrong during parameter validation');
    }

    if ($feach) {
        $ret = [];

        $status = true;
        foreach ($fargs as $REAL_ARGS) {
            $result = call_user_func_array(array($_resource, $fname), $REAL_ARGS);

            if ($fname === 'get' && $kT === 1 && $retKV) {
                $real_key = $REAL_ARGS[0];
                $key = remove_prefix($real_key, MEMCACHED_PREFIX);

                $was_found = $_d ? $_resource->getResultCode() !== \Memcached::RES_NOTFOUND
                    : $flags !== false;

                if (isset($kE[$real_key])) {
                    $ret[$key] = $was_found;
                } else if ($was_found) {
                    $ret[$key] = $result;
                }
            } else if (!$result) {
                $status = false;
            }
        }

        return $fname === 'get' ? $ret : $status;
    } else {
        $REAL_ARGS = $fargs;

        $result = call_user_func_array(array($_resource, $fname), $REAL_ARGS);

        if ($fname === 'get' && $kT === 0 && !$retKV && isset($kE[$k])) {
            // For memcache (not memcached), if $flags (initially set to false) is unchanged, then
            // the key was not found.
            return $_d ?
                $_resource->getResultCode() !== \Memcached::RES_NOTFOUND
                : $flags !== false;
        }

        if ($retKV) {
            $ret = [];

            // $result will not have keys that were not found, so loop over all keys in kE and set
            // their value to false. And for keys that were found, override those values to true

            foreach ($k as $kk => $kv) {
                $tmp = $kT === 1 ? $kv : $kk;
                if (isset($kE[$tmp])) {
                    $ret[remove_prefix($tmp, MEMCACHED_PREFIX)] = false;
                }
            }

            foreach ($result as $k => $v) {
                $ret[remove_prefix($k, MEMCACHED_PREFIX)] = isset($kE[$k]) ? true : $v;
            }

            return $ret;
        }

        return $result;
    }
}

/**
 * Check if a key exists in the cache.
 *
 * @param string|string[]   $k the key or keys to check if exists
 * @return bool|array       true if exists, false otherwise. If $k is an array then a key to boolean
 *                          pair associative array will be returned for all keys with a true value
 *                          indicating the key exists, and false otherwise.
 */
function cached_exists($k) {
    if (is_array($k)) {
        $k = array_map(function($item) {
            return '?' . $item;
        }, $k);
    } else {
        $k = '?' . $k;
    }
    return cached($k);
}

/**
 * Delete key(s) from the cache.
 *
 * @param string|string[]   the key or keys to delete
 * @return bool             true if success, false otherwise
 */
function cached_delete($k): bool {
    if (is_array($k)) {
        return cached($k, -1) == true;
    } else {
        return cached($k, null, -1) == true;
    }
}

/**
 * Check if memcache(d) is available.
 *
 * @return bool true if available, false otherwise
 */
function cached_available(bool $new_state = null): bool {
    static $_value = null;

    if (!isset($_value) && !isset($new_state)) {
        cached();
        return cached_available();
    }

    return isset($new_state) ? ($_value = $new_state) : $_value;
}

// ==========================================================================================
// HASHING & CRYPTOGRAPHY FUNCTIONS
// ==========================================================================================

/** @noinspection PhpDocMissingThrowsInspection */
/**
 * Generate a cryptographically secure token.
 *
 * @param int number of bytes
 * @return string the crypto token
 */
function safe_token(int $length=16) {
    /** @noinspection PhpUnhandledExceptionInspection */
    return bin2hex(random_bytes($length));
}

function safe_keygen() {
    return sodium_crypto_secretbox_keygen();
}

function safe_hash(string $message, string $key, int $length=SODIUM_CRYPTO_GENERICHASH_BYTES, bool $raw = false): string {
    $h = sodium_crypto_generichash($message, $key, $length);
    if ($raw) {
        return $h;
    } else {
        return safe_b64encode($h);
    }
}

function safe_b64encode(string $bin): string {
    return sodium_bin2base64($bin, 1);
}

function safe_b64decode(string $b64): string {
    return sodium_base642bin($b64, 1);
}

/** @noinspection PhpDocMissingThrowsInspection */
/**
 * Encrypt a message
 *
 * @param string $message - message to encrypt
 * @param string $key - encryption key
 * @return string
 */
function safe_encrypt(string $message, string $key): string {
    /** @noinspection PhpUnhandledExceptionInspection */
    $nonce = random_bytes(
        SODIUM_CRYPTO_SECRETBOX_NONCEBYTES
    );

    $cipher = base64_encode(
        $nonce.
        sodium_crypto_secretbox(
            $message,
            $nonce,
            $key
        )
    );
    sodium_memzero($message);
    sodium_memzero($key);
    return $cipher;
}

/**
 * Decrypt a message
 *
 * @param string $encrypted - message encrypted with safeEncrypt()
 * @param string $key - encryption key
 * @return string
 * @throws SafeCryptoError
 */
function safe_decrypt(string $encrypted, string $key): string {
    $decoded = base64_decode($encrypted);
    if ($decoded === false) {
        throw new SafeCryptoError('SAFE_DECRYPT_ERROR: the encoding failed');
    }
    if (mb_strlen($decoded, '8bit') < (SODIUM_CRYPTO_SECRETBOX_NONCEBYTES + SODIUM_CRYPTO_SECRETBOX_MACBYTES)) {
        throw new SafeCryptoError('SAFE_DECRYPT_ERROR: the message was truncated');
    }
    $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
    $ciphertext = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');

    $plain = sodium_crypto_secretbox_open(
        $ciphertext,
        $nonce,
        $key
    );
    if ($plain === false) {
        throw new SafeCryptoError('SAFE_DECRYPT_ERROR: the message was tampered with in transit');
    }
    sodium_memzero($ciphertext);
    sodium_memzero($key);
    return $plain;
}

class SafeCryptoError extends \Exception {

    public function __construct($message = '', $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

}