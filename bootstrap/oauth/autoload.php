<?php

function autoload_oauth2_server(): void {
    require BOOTSTRAP_ROOT.'oauth/oauth2-server-php/src/OAuth2/Autoloader.php';
    \OAuth2\Autoloader::register();
}

function autoload_oauth2_client(?array $grant_types = ['AuthorizationCode', 'RefreshToken']): void {
    require_once(BOOTSTRAP_ROOT."oauth/oauth2-client-php/Client.php");
    require_once(BOOTSTRAP_ROOT."oauth/oauth2-client-php/GrantType/IGrantType.php");

    if (empty($grant_types)) {
        return;
    }

    foreach ($grant_types as $grant_type) {
        require_once(BOOTSTRAP_ROOT."oauth/oauth2-client-php/GrantType/".$grant_type.".php");
    }
}