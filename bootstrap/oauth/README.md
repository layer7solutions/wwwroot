The third-party libraries, "bshaffer/oauth2-server-php" and "adoy/PHP-OAuth2", both respectively
licensed under the MIT and LGPL licenses, are purposely part of the repository and not in the
`vendor/` folder or the `composer.json` because of our own code modifications.