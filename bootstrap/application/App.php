<?php

class App {

    const SYSROUTE_REPORT_VIOLATION_CSP = '/report_violation/CSP';
    const SYSROUTE_REPORT_VIOLATION_TEST = '/report_violation/test';

    // Entrypoint Functions
    // ------------------------------------------------------------------------------------------

    /**
     * Start the applications, this function calls `self::load()` then `self::dispatch()`.
     * If the `{APP_ROOT}/onload.php` file returns a string, that string will be used as the
     * $route_file parameter when `dispatch()` is called.
     * @return mixed the result of the `dispatch()` function
     */
    public static function start() {
        return self::dispatch(... self::load());
    }

    /**
     * Load the application. If an `onload.php` file exists in the APP_ROOT, it will be included.
     * @return mixed
     */
    public static function load(): array {
        $route_file = null;

        if (file_exists(APP_ROOT.'onload.php')) {
            $ret = include APP_ROOT.'onload.php';

            if (is_string($ret)) {
                $route_file = $ret;
            }
        }

        return (array) $route_file;
    }

    /**
     * Dispatch a response to the HTTP request based on a specified route configuration.
     *
     * @param string    $route_file (optional) the route relative to the APP_ROOT, if not specified,
     *                  the framework will look for a '{APP_ROOT}/routes.php' file.
     * @param string    $req_uri (optional) the request URI. If not specified, the framework will
     *                  use the application request URI supplied by the HTTP server through
     *                  `App::request_uri()`
     * @param bool      $no_exec=false (optional) whether or not to return the callback or
     *                  to execute the callback
     * @return mixed    the result of the callback if no_exec=0 or the callback itself if
     *                  no_exec=1
     */
    public static function dispatch(string $route_file=null, string $req_uri=null, bool $no_exec=false) {
        App::router()->unpack()->pack(APP_ROOT . ltrim($route_file ?? 'routes.php', '/'));

        define('BOOTSTRAP_END_TIME', (microtime(true) * 1000));
        define('BOOTSTRAP_TIME_TAKEN', BOOTSTRAP_END_TIME - BOOTSTRAP_START_TIME);

        return App::router()->dispatch($req_uri, null, $no_exec);
    }

    // DB Function
    // ------------------------------------------------------------------------------------------

    /**
     * Get a DB instance. This function will return the same DB instance if called multiple times
     * on the same $dbname. All DB connections will be automatically closed on script execution end.
     *
     * @param string    $dbname the database name
     * @param string    $user (optional) the dbuser username, if not specified the default
     *                  username will be used
     * @param string    $password (optional) the dbuser password, if not specified the default
     *                  password will be used
     * @return DB       a DB class
     */
    public static function db(string $dbname, string $user = null, string $password = null): DB {
        static $_cache = [];

        $user       = $user     ?? ($dbname == 'oauth2' ? OAUTH2DB_USERNAME : PGSQL_USERNAME);
        $password   = $password ?? ($dbname == 'oauth2' ? OAUTH2DB_PASSWORD : PGSQL_PASSWORD);

        if (!isset($_cache[$user])) {
            $_cache[$user] = [];
        } else if (isset($_cache[$user][$dbname])) {
            return $_cache[$user][$dbname];
        }

        $db = DB::connect([
            'host'      => PGSQL_SERVER,
            'user'      => $user,
            'password'  => $password,
            'dbname'    => $dbname,
            'sslmode'   => 'require',
        ]);

        $db->throw_exception_on_sql_error = true;
        $db->throw_exception_on_nonsql_error = true;

        return $_cache[$user][$dbname] = $db;
    }

    // Helper functions for Controllers
    // ------------------------------------------------------------------------------------------

    /**
     * Run the default action of the controller with the specified name.
     *
     * @param string    $name the controller name, the 'Controller` suffix in the class name is not
     *                  considered part of the controller name.
     * @param array     $params (optional) pass these parameters optionally
     * @return mixed
     */
    public static function runController(string $name, array $params = null) {
        $name   = explode(':', $name, 2);
        $params = $params ?? App::router()->params();

        $result = self::getController($name[0])->run_action($name[1] ?? 'default_action', $params);

        if ($result instanceof View) {
            $result->dispatch($params);
        } else if (is_string($result)) {
            echo $result;
        } else if ($result === false) {
            self::router()->error(404);
        }

        return $result;
    }

    /**
     * Get an instance of the controller class with the specified name.
     *
     * @param string        $name the controller name, the 'Controller` suffix in the class name
     *                      is not considered part of the controller name.
     * @return Controller   a Controller class
     */
    public static function getController(string $name): Controller {
        if ($name === 'ReportViolation') {
            require_once(BOOTSTRAP_ROOT . 'application/ReportViolationController.php');
            return new ReportViolationController();
        }
        $name = str_replace('/', '\\', $name);
        $name = str_replace('.', '\\', $name);
        $fullyQualifiedName = 'app\\controllers\\'.$name.'Controller';
        return new $fullyQualifiedName;
    }

    // Helper functions for HTTP Request and Router
    // ------------------------------------------------------------------------------------------

    /**
     * Returns the server HTTP request method, always uppercase.
     * @return string
     */
    public static function request_method(): string {
        $_cache = null;
        if (isset($_cache)) {
            return $_cache;
        }
        return $_cache = strtoupper($_SERVER['REQUEST_METHOD']);
    }

    /**
     * Returns the HTTP request body.
     *
     * @param bool $do_parse if true, then the request body will be parsed as a query string and returned as an
     * associative array, otherwise the raw body will be returned as string
     * @return string|array
     */
    public static function request_body($do_parse = false): string {
        $http_body = file_get_contents('php://input');
        if ($do_parse) {
            $res = [];
            parse_str($http_body, $res);
            return $res;
        }
        return $http_body;
    }

    /**
     * Returns the server request URI
     * Always starts with a slash and never ends with one.
     *
     * @param bool      $keep_query=false (optional) whether or not to keep the query string
     * @param string    $rel=null (optional) optionally add an additional path to be relative to
     * @return string   the request URI
     */
    public static function request_uri(bool $keep_query = false, string $rel = null): string {
        static $_request_uri;
        static $_request_uri_no_query;

        if (!isset($_request_uri)) {
            if (defined('PROXY_URI')) {
                $_request_uri = PROXY_URI;
            } else {
                $_request_uri = $_SERVER['REQUEST_URI'];
            }

            $_request_uri           = '/'.trim($_request_uri, '/');
            $_request_uri_no_query  = '/'.trim(strtok($_request_uri, '?'), '/');
        }

        $ret_uri = $keep_query ? $_request_uri : $_request_uri_no_query;

        if (isset($rel)) {
            return remove_prefix($ret_uri, '/'.ltrim($rel, '/'), true);
        }

        return $ret_uri;
    }

    /**
     * Returns the application Router singleton.
     *
     * @return Router
     */
    public static function router(): Router {
        static $_router;
        if (!isset($_router))
            $_router = new Router();
        $_router->clearGroup();
        return $_router;
    }

}