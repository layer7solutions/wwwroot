<?php

use SuperClosure\Serializer as ClosureSerializer;
use SuperClosure\SerializableClosure;

/**
 * This class is the interface for adding new routes from the application route file. This class
 * provides a layer of abstraction over FastRoute and route callback handling.
 */
class Router {
    // FastRoute
    private $dispatcher;
    private $params;
    private $collector;
    private $options = [];

    // Param Hooks
    private $param_hooks = [];

    // Route prefix
    private $group_array  = [];
    private $group_concat = '';

    // Serializers
    const SERIALIZED_LABEL = "__SERIALIZED__";
    private $closureSerializer;

    /**
     * if `true`, then there are no routes currently in the collector. It is set to `false`
     * initially so that the call to `reset()` in the constructor will execute.
     * @var bool
     */
    private $empty = false;

    /**
     * Construct the Router interface.
     *
     * @param array $options (optional) additional options for FastRoute
     */
    public function __construct(array $options = []) {
        /* FastRoute options */
        $options += [
            'routeParser'       => '\\FastRoute\\RouteParser\\Std',
            'dataGenerator'     => '\\FastRoute\\DataGenerator\\GroupCountBased',
            'dispatcher'        => '\\FastRoute\\Dispatcher\\GroupCountBased',
            'routeCollector'    => '\\FastRoute\\RouteCollector',
            'cacheDisabled'     => false,
        ];
        $this->options = $options;

        $this->closureSerializer = new ClosureSerializer();

        $this->reset();
    }

    // ------------------------------------------------------------------------------------------
    // PACK/UNPACK
    // ------------------------------------------------------------------------------------------

    /**
     * Clear all routes in this Router.
     * @return Router self
     */
    public function reset(): Router {
        if ($this->empty) { // don't reset if nothing to reset
            return $this;
        }

        $this->collector = null;
        $this->dispatcher = null;
        $this->clearGroup();

        return $this;
    }

    /**
     * Alias of `reset()`
     * @return Router self
     */
    public function unpack(): Router {
        return $this->reset();
    }

    /**
     * Loads the internal route dispatcher with the collection of all routes in this Router.
     * This function should be called before `dispatch()`.
     */
    public function pack(string $route_file): Router {
        $cache_file = $route_file . '.cache';

        if (!$this->options['cacheDisabled'] && file_exists($cache_file)) {
            $cached_data = require $cache_file;

            if (is_array($cached_data['paramHooks'])) {
                $this->param_hooks = $cached_data['paramHooks'];
            }

            if (is_array($cached_data['dispatchData'])) {
                $this->dispatcher = new $this->options['dispatcher']($cached_data['dispatchData']);
                return $this;
            }
        }

        $this->collector = new $this->options['routeCollector'](
            new $this->options['routeParser'], new $this->options['dataGenerator']
        );

        require BOOTSTRAP_ROOT.'application/sysroute.php';
        require $route_file;

        $dispatchData = $this->collector->getData();

        if (!$this->options['cacheDisabled']) {
            file_put_contents($cache_file,
                "<?php return [\n" .
                "'paramHooks' => " .var_export($this->param_hooks, true) .
                ",\n" .
                "'dispatchData' => " .var_export($dispatchData, true) .
                "];");
        }

        $this->dispatcher = new $this->options['dispatcher']($dispatchData);

        return $this;
    }

    /**
     * Returns whether or not this Router is packed.
     * @return bool packed state
     * @see pack()
     */
    public function isPacked(): bool {
        return empty($this->dispatcher) !== false;
    }

    // ------------------------------------------------------------------------------------------
    // HTTP-METHOD-SPECIFIC ROUTE FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Add an ANY method route.
     * @return Router self
     */
    public function ANY($route_paths, $callbacks): Router {
        return $this->route(['GET','POST','PUT','DELETE','PATCH'], $route_paths, $callbacks);
    }

    /**
     * Add a GET method route.
     * @return Router self
     */
    public function GET($route_paths, $callbacks): Router {
        return $this->route('GET', $route_paths, $callbacks);
    }

    /**
     * Add a POST method route.
     * @return Router self
     */
    public function POST($route_paths, $callbacks): Router {
        return $this->route('POST', $route_paths, $callbacks);
    }

    /**
     * Add a PUT method route.
     * @return Router self
     */
    public function PUT($route_paths, $callbacks): Router {
        return $this->route('PUT', $route_paths, $callbacks);
    }

    /**
     * Add a PATCH method route.
     * @return Router self
     */
    public function PATCH($route_paths, $callbacks): Router {
        return $this->route('PATCH', $route_paths, $callbacks);
    }

    /**
     * Add a DELETE method route.
     * @return Router self
     */
    public function DELETE($route_paths, $callbacks): Router {
        return $this->route('DELETE', $route_paths, $callbacks);
    }

    /**
     * Add a HEAD method route.
     * @return Router self
     */
    public function HEAD($route_paths, $callbacks): Router {
        return $this->route('HEAD', $route_paths, $callbacks);
    }

    /**
     * Add an OPTIONS method route.
     * @return Router self
     */
    public function OPTIONS($route_paths, $callbacks): Router {
        return $this->route('OPTIONS', $route_paths, $callbacks);
    }

    // ------------------------------------------------------------------------------------------
    // ROUTE GROUPING FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Clear all groups.
     * @return Router self
     */
    public function clearGroup(): Router {
        $this->setGroup();
        return $this;
    }

    /**
     * Set the group. This will override any existing groups.
     *
     * @param string    $route_group
     * @return Router   self
     */
    public function setGroup(string $route_group = null): Router {
        $this->group_array  = [$route_group ?? ''];
        $this->group_concat = ($route_group ?? '');
        return $this;
    }

    /**
     * Add a group. Groups may be nested, use `endGroup()` to go up level when you're done with
     * a group.
     *
     * E.g.
     * ```
     * <?php
     * router->addGroup('/example')
     *     ->GET('/foo', function() { ... })
     *     ->GET('/bar', function() { ... });
     * ?>
     * ```
     * Is the same as:
     * ```
     * <?php
     * router->GET('/example/foo', function() { ... });
     * router->GET('/example/bar', function() { ... });
     * ?>
     * ```
     *
     * @param string    $route_group
     * @return Router   self
     */
    public function addGroup(string $route_group): Router {
        $this->group_array[] = $route_group;

        $this->group_concat = implode('', $this->group_array);
        return $this;
    }

    /**
     * Replace the current group. Does not modify parent groups.
     *
     * @param string    $route_group
     * @return Router   self
     */
    public function replaceGroup(string $route_group): Router {
        $this->endGroup();
        $this->addGroup($route_group);
        return $this;
    }

    /**
     * End the current group and go up to the parent group (if any).
     * @return Router self
     */
    public function endGroup(): Router {
        array_pop($this->group_array);

        $this->group_concat = implode('', $this->group_array);
        return $this;
    }

    // ------------------------------------------------------------------------------------------
    // PARAMETER HOOKS
    // ------------------------------------------------------------------------------------------

    /**
     * Add a parameter hook. If the matched route contains a parameter with the specified
     * `$param_name` then `$callback` will be called with the parameter value passed to it,
     * and the result of the callback will become the new value.
     *
     * @param string|string[]   $param_name the parameter placeholder name
     * @param callable          $callback the callback in response to the parameter
     */
    public function setParameterHook($param_name, $callback) {
        if (is_array($param_name)) {
            foreach ($param_name as $x) {
                $this->param_hooks[$x] = $this->normalizeCallback($callback);
            }
        } else {
            $this->param_hooks[$param_name] = $this->normalizeCallback($callback);
        }
        return $this;
    }

    /**
     * Remove a parameter hook.
     *
     * @param string $param_name the parameter placeholder name
     */
    public function removeParameterHook(string $param_name): void {
        unset($this->param_hooks[$param_name]);
    }

    // ------------------------------------------------------------------------------------------
    // ROUTE AND DISPATCH FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Add a route. You can also use one of the `{METHOD}($route_paths, $callbacks)` shorthands,
     * such as the `GET()` function.
     *
     * @param string[]|string   $methods the HTTP method in all uppercase, can also be an array of
     *                          HTTP methods to match multiple methods.
     * @param string[]|string   $route_paths the route paths, can be a single route path or an array
     *                          of multiple
     * @param callable|mixed    The callback. Can be a callable; a string as `#{AppController}` or
     *                          `%{PHPCODE}`; or an array as `[callable, args...]`
     * @return Router           self
     */
    public function route($methods, $route_paths, $callbacks): Router {
        $this->empty = false;

        $route_paths = (array) $route_paths;

        for ($i = 0; $i < count($route_paths); $i++) {
            $path = '/' . trim($this->group_concat . $route_paths[$i], '/');

            $this->collector->addRoute($methods, $path, [$i, $this->normalizeCallback($callbacks)]);
        }

        return $this;
    }

    /**
     * Dispatch this Router using the given request path and the loaded route paths.
     *
     * @param string    $req_uri (optional) the request path to match to the route paths, will use
     *                  the server REQUEST_URI if not provided
     * @param string    $req_method (optional) the request HTTP method, will use the server
     *                  REQUEST_METHOD if not provided. Should be in all uppercase.
     * @param bool      $no_exec=false (optional) whether or not to execute the callback or
     *                  to return it without execution
     * @return mixed    the result of the callback if $no_exec=0 or the callback itself if
     *                  $no_exec=1
     */
    public function dispatch(string $req_uri = null, string $req_method = null, bool $no_exec = false) {
        $req_uri    = $req_uri ?? App::request_uri();
        $req_method = $req_method ?? App::request_method();

        $routeInfo = $this->dispatcher->dispatch($req_method, $req_uri);

        // Note that `$this->error` (a void function) will call `exit()`

        switch ($routeInfo[0]) {
            case \FastRoute\Dispatcher::NOT_FOUND: // ERROR 404
                /** @noinspection PhpVoidFunctionResultUsedInspection */
                return $no_exec ? new RouteFail(404, null) : $this->error(404);
            case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED: // ERROR 405
                /** @noinspection PhpVoidFunctionResultUsedInspection */
                return $no_exec ? new RouteFail(405, $routeInfo[1]) : $this->error(405);
            case \FastRoute\Dispatcher::FOUND:
                $callback_info          = $routeInfo[1];
                $callbacks              = $callback_info[1];
                $this->params           = $this->handleParameterHooks($routeInfo[2], $callbacks);
                $this->params['METHOD'] = $req_method;
                $this->params['URI']    = $req_uri;
                $this->params['ROUTE#'] = $callback_info[0];

                return $no_exec ? aor1($callbacks) : $this->handleCallbacks([ $callbacks ]);
        }
    }

    /**
     * Dispatch an error page for the given HTTP response code if a `net.error.{RESPONSE_CODE}`
     * template exists.
     *
     * @param int       $response_code
     * @param string    $title (optional) if not set, uses default title
     * @param array     $params (optional) parameters to pass to the View
     */
    public function error(int $response_code, string $title = null, array $params=[]) {
        if (!isset($title)) {
            switch ($response_code) {
                case 400: $title = '400 Bad Request'; break;
                case 403: $title = '403 Forbidden'; break;
                case 404: $title = '404 Not Found'; break;
                case 405: $title = '405 Method Not Allowed'; break;
                case 500: $title = '500 Internal Server Error'; break;
                case 503: $title = '503 Service Unavailable'; break;
                default:  $title = strval($response_code);
            }
        }

        view('net.error.'.$response_code)
            ->title($title)
            ->http_code($response_code)
            ->using($params)
            ->dispatchAndExit();
    }

    /**
     * Get the route path parameter(s).
     *
     * @param string    $name=null get the parameter by this name\
     * @return mixed    if $name=null then all the parameters will be returned as an associative
     *                  array, else the value for the specified parameter name will be returned.
     */
    public function params(string $name = null) {
        return isset($name) ? $this->params[$name] : $this->params;
    }

    // ------------------------------------------------------------------------------------------
    // HANDLING CALLBACKS
    // ------------------------------------------------------------------------------------------

    private function normalizeCallback($callback) {
        if ($callback instanceof \Closure) {
            return [self::SERIALIZED_LABEL, 'closureSerializer', $this->closureSerializer->serialize($callback)];
        }
        return $callback;
    }

    /**
     * Internal function for handling parameter hooks
     */
    private function handleParameterHooks(array $params, $callbacks) {
        foreach ($params as $param_name => $param_value) {
            if (isset($this->param_hooks[$param_name])) {
                $params[$param_name] = $this->runCallback(
                    $this->param_hooks[$param_name],
                    [$param_value, $callbacks]
                );
            }
        }
        return $params;
    }

    /**
     * Internal function for executing callbacks.
     */
    private function runCallback($callback, $parameters = null) {
        $ch0 = is_string($callback) ? substr($callback, 0, 1) : null;

        if (isset($ch0)) {
            if ($ch0 == '%') {
                return eval('return '.substr($callback, 1).';');
            } else if ($ch0 == '#') {
                $callback = substr($callback, 1);

                if (str_contains($callback, '[')) {
                    list($callback, $extra_params) = explode('[', rtrim($callback, ' ]'), 2);
                    foreach (explode(',', $extra_params) as $extra) {
                        list($extra_name, $extra_value) = explode('=', $extra);

                        $this->params[trim($extra_name)] = eval('return '.$extra_value.';');
                    }
                }

                return App::runController($callback, $this->params);
            } else if (is_callable($callback)) {
                return !isset($parameters) ? $callback($this->params) : $callback(... $parameters);
            }
        } else if (is_array($callback) && count($callback) == 3 && $callback[0] === self::SERIALIZED_LABEL) {
            $unserialized = null;

            if ($callback[1] === 'closureSerializer') {
                $unserialized = $this->closureSerializer->unserialize($callback[2]);
            } else {
                $unserialized = unserialize($callback[2]);
            }

            return !isset($parameters)
                ? $this->runCallback($unserialized)
                : $this->runCallback($unserialized, $parameters);
        } else if (is_array($callback) && count($callback) >= 1 && is_callable($callback[0])) {
            $func_name      = $callback[0];
            $func_params    = array_slice($callback, 1);

            return call_user_func_array($func_name, $func_params);
        } else if (is_callable($callback)) {
            return !isset($parameters) ? $callback($this->params) : $callback(... $parameters);
        }

        return null;
    }

    /**
     * Internal function for executing callbacks.
     */
    private function handleCallbacks(array $callbacks) {
        $ret = [];

        foreach ($callbacks as $callback) {
            $ret[] = $this->runCallback($callback);
        }

        return aor1($ret);
    }
}

/**
 * An error in which the Router did not find a route for the given URI, such as in the case that
 * there is no available route or the route found does not allow the specified HTTP method.
 */
class RouteFail {
    public $status;
    public $detail;

    public function __construct($status_code, $detail) {
        $this->status = $status_code;
        $this->detail = $detail;
    }
}