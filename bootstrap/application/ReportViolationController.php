<?php

final class ReportViolationController extends Controller {

    protected function CSP(array $params) {
        $data = App::request_body();

        if (empty($data)) {
            http_response_code(400);
            exit;
        }

        db('application')->insert('report_violations', [
            'violation_type'    => 'CSP',
            'report_data'       => $data,
            'report_utc'        => time(),
            'site_url'          => SITE_URL,
            'site_session'      => !empty($_SESSION) ? json_encode($_SESSION) : null,
        ]);

        http_response_code(204);
        exit;
    }

    protected function test(array $params) {
        die('testing');
    }

}
