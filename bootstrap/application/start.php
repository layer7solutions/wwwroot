<?php
// Note that this file is included from the global scope

// SET CONSTANTS
// --------------------------------------------------------------------------------
define('BOOTSTRAP_START_TIME', (microtime(true) * 1000));

if (!defined('SITE_PROXY')) {
    define('SITE_PROXY', '');
}

$_REQUEST = $_GET + $_POST;

// MISC
// --------------------------------------------------------------------------------

/**
 * Check if the specified test feature is enabled.
 *
 * i.e. `/?feature=featurename`; multiple features can be separated by `|`.
 *
 * The `feature` parameter is used for testing experimental features or forcing conditions for
 * testing.
 */
function test_feature_enabled(string $feature_name, string $source='feature'): bool {
    static $_cache = [];

    if (!isset($_cache[$source]) && isset($_REQUEST[$source])) {
        $_cache[$source] = [];

        foreach (explode('|', strtolower($_REQUEST[$source])) as $k) {
            $_cache[$source][$k] = true;
        }
    }

    if (!isset($_cache[$source])) {
        return false;
    } else {
        return isset($_cache[$source][strtolower($feature_name)]);
    }
}

// SET ERROR REPORTING
// --------------------------------------------------------------------------------

/**
 * Dispatch our 500 error page defined in VIEWS_500 in `config.php`. This function exists at a
 * low level because a 500 error could indicate that problems exist at higher levels, thus using
 * the Views system may not work.
 */
function invoke_500_error_page(): void {
    http_response_code(500);
    header('Content-type: text/html; charset=utf-8', true, 500);

    if (!defined('VIEWS_500')) {
        die('An internal error occurred. Please try again later.');
    }

    readfile(VIEWS_500);
    die;
}

function exceptionHandler($exception) {
    $traceline = "#%s %s(%s): %s(%s)";
    $msg = "PHP Fatal error:\nUncaught exception '%s' with message '%s' in %s:%s\n".
            "Stack trace:\n%s\n  thrown in %s on line %s";

    // alter your trace as you please, here
    $trace = $exception->getTrace();
    foreach ($trace as $key => $stackPoint) {
        // I'm converting arguments to their type
        // (prevents passwords from ever getting logged as anything other than 'string')
        if (isset($trace[$key]['args']))
            $trace[$key]['args'] = array_map('gettype', $trace[$key]['args']);
    }

    // build your tracelines
    $result = array();
    foreach ($trace as $key => $stackPoint) {
        $result[] = sprintf(
            $traceline,
            $key,
            $stackPoint['file'] ?? 'Unknown',
            $stackPoint['line'] ?? '0',
            $stackPoint['function'],
            ( isset($stackPoint['args']) ? implode(', ', $stackPoint['args'])  : '' )
        );
    }

    // trace always ends with {main}
    /** @noinspection PhpUndefinedVariableInspection */
    $result[] = '#' . ++$key . ' {main}';

    // write tracelines into main template
    $msg = sprintf(
        $msg,
        get_class($exception),
        $exception->getMessage(),
        $exception->getFile(),
        $exception->getLine(),
        implode("\n", $result),
        $exception->getFile(),
        $exception->getLine()
    );

    // log or echo as you please
    error_log($msg);
}

set_error_handler(function($severity, $message, $file, $line) {
    // throw exceptions for all errors including E_USER_NOTICE
    if (!(error_reporting() & $severity)) {
        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
});

if (SITE_IS_STAGING || (isset($GLOBALS['__application_test_mode']) && $GLOBALS['__application_test_mode'])) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    ini_set('log_errors', 0);
    set_exception_handler('exceptionHandler');
} else {
    error_reporting(E_ALL);
    ini_set('display_errors', 0);
    ini_set('log_errors', 1);
    set_exception_handler(function($exception) {
        exceptionHandler($exception);
        invoke_500_error_page();
    });
}


// INCLUDE NECESSARY FILES (order is important here)
// --------------------------------------------------------------------------------

// First include global helper/utility functions
require BOOTSTRAP_ROOT . 'common/functions.php';                // General helper functions
require BOOTSTRAP_ROOT . 'common/secure.php';                   // Security & safe coding helper functions
require BOOTSTRAP_ROOT . 'oauth/autoload.php';

// Database
require BOOTSTRAP_ROOT . 'database/db.php';                     // Database wrapper class

// Dispatch system
require BOOTSTRAP_ROOT . 'dispatch/render.php';                 // Dispatch - Render system
require BOOTSTRAP_ROOT . 'dispatch/View.php';                   // Dispatch - Template system
require BOOTSTRAP_ROOT . 'dispatch/SubresourceService.php';     // Dispatch - Subresource system
require BOOTSTRAP_ROOT . 'dispatch/fastroute/bootstrap.php';    // Dispatch - Core Router

// Application classes
require BOOTSTRAP_ROOT . 'application/App.php';                 // Application - helper functions
require BOOTSTRAP_ROOT . 'application/Router.php';              // Application - Router wrapper
require BOOTSTRAP_ROOT . 'application/Controller.php';          // Application - Abstract Controller

if (isset($GLOBALS['__application_test_mode']) && $GLOBALS['__application_test_mode'] === true) {
    // If `__application_test_mode` was false, then we would be calling  `App::start()`
    // instead of this `if` block.
    // `App::start()` is just `return self::dispatch(... self::load())`. What we want to do here is
    // *just* call `App::load()` and not `App::dispatch`, which would kick off the Route Collection,
    // Routing, and Dispatch process.

    App::load();
    return;
}

// Start routing
App::start();