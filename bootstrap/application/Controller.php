<?php

abstract class Controller {
    public $middleware = [];
    public $interceptor = [];

    /**
     * Construct Controller.
     */
    public function __construct() {
        // Do nothing
    }

    public function error() {
        return App::router()->error(... func_get_args());
    }

    /**
     * Add middleware.
     *
     * @param callable $func the middleware
     * @param bool $prepend=false
     */
    public function middleware(callable $func, bool $prepend=false): void {
        if ($prepend) {
            array_unshift($this->middleware, $func);
        } else {
            $this->middleware[] = $func;
        }
    }

    /**
     * Add interceptor.
     *
     * @param callable $func the interceptor
     * @param bool $prepend=false
     */
    public function interceptor(callable $func, bool $prepend=false): void {
        if ($prepend) {
            array_unshift($this->interceptor, $func);
        } else {
            $this->interceptor[] = $func;
        }
    }

    /**
     * Run the specified action (method within the Controller class). This function will also
     * run all middleware added to this Controller before executing the action. If any middleware
     * function returns `false` or returns an isset value, that will be the result and the action
     * method will not be called.
     *
     * @param string    $action_name the action method name
     * @param array     $params parameters to pass to the actionmethod
     * @return mixed    the result of the action method
     */
    public function run_action(string $action_name, array $params) {
        foreach ($this->middleware as $func) {
            $result = null;

            if (is_callable($func)) {
                $result = $func($params);
            } else if (is_object($func) && ($func instanceof Closure)) {
                $result = $func->call($this, $params);
            } else {
                continue;
            }

            if ($result === false) {
                return false;
            }

            if (isset($result)) {
                return $result;
            }
        }

        $res = call_user_func_array(array($this, $action_name), array($params));

        foreach ($this->interceptor as $func) {
            $result = null;

            if (is_callable($func)) {
                $result = $func($res, $params);
            } else if (is_object($func) && ($func instanceof Closure)) {
                $result = $func->call($this, $res, $params);
            } else {
                continue;
            }

            if ($result === false) {
                return false;
            }

            if (isset($result)) {
                $res = $result;
            }
        }

        return $res;
    }

    /**
     * The default action.
     *
     * @param array $params
     * @return mixed
     */
    protected function default_action(array $params) {
        return false;
    }

}
