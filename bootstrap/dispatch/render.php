<?php

/*
render.php
~~~~~~~~~~

for dispatching a result back to the client
*/

// ================================================================================
// REDIRECTING
// ================================================================================

/**
 * Local Redirect - only for internal pages, use redirect_ext for external.
 * If the 'page' parameter is not given, it redirects to the homepage.
 *
 * @param string $page the page to redirect to
 */
function redirect(string $page = '') {
    try {
        $no_protocol = explode('//',SITE_URL,2)[1];
        _redirect_helper($page, SITE_URL, 'http://'.$no_protocol, 'https://'.$no_protocol);
    } finally {
        die;
    }
}

/**
 * Locally redirect to whatever $_REQUEST['cont] or, if not set, homepage
 */
function redirect_cont() {
    try {
        redirect($_REQUEST['cont'] ?? '');
    } finally {
        die;
    }
}

/**
 * Redirect to a Reddit.com page.
 *
 * @param string $page the page to redirect to
 */
function redirect_reddit(string $page) {
    try {
        $reddit_http = 'http://www.reddit.com/';
        $reddit_https = 'https://www.reddit.com/';
        _redirect_helper($page, $reddit_https, $reddit_http, $reddit_https);
    } finally {
        die;
    }
}

/**
 * Redirect to an external site (never use on any user input).
 *
 * @param string $url the URL to redirect to
 */
function redirect_ext(string $url) {
    try {
        header('Location: ' . $url);
    } finally {
        die;
    }
}

/**
 * Internal redirect helper function. This function will force the `$page` URL passed in to
 * take the `$base_url` as its base if it is not already.
 *
 * e.g.
 *
 * ```
 * <?php
 *
 * // redirects to "https://example.com/https://www.google.com/"
 * _redirect_helper(
 *     'https://www.google.com/', // $page
 *     'https://example.com/',    // $base_url
 *     'http://example.com/',     // $http_base
 *     'https://example.com/');   // $https_base
 *
 * // redirects to "https://example.com/foobar"
 * _redirect_helper(
 *     '/foobar',
 *     'https://example.com/',
 *     'http://example.com/',
 *     'https://example.com/');
 *
 * // also redirects to "https://example.com/foobar"
 * _redirect_helper(
 *     'https://www.example.com/foobar',
 *     'https://example.com/',
 *     'http://example.com/',
 *     'https://example.com/');
 * ?>
 * ```
 *
 * @param string $page the page to redirect to
 * @param string $base_url the preferred base URL (with protocol), should end with a `/`
 * @param string $http_base the base URL with `http://` protocol, should end with a `/`
 * @param string $https_base the base URL with `https://` protocol, should end with a `/`
 */
function _redirect_helper(string $page, string $base_url, string $http_base, string $https_base) {
    if (empty($page)) {
        header('Location: ' . $base_url);
        die();
    }

    if (startsWith($page, $http_base))
        $page = remove_prefix($page, $http_base);
    if (startsWith($page, $https_base))
        $page = remove_prefix($page, $https_base);

    header('Location: ' . $base_url . ltrim($page, '/'));
    die();
}

// ================================================================================
// STASH
// ================================================================================

/**
 * Check if a value exists in the stash for the given key.
 * @return bool
 */
function is_stashed(string $key) {
    return stash($key, null, 1);
}

/**
 * Remove an item from the stash by its key.
 *
 * @param string $key the key of the item to remove
 * @return mixed the value that was stored at the key (if any). If there was no value, then null
 * will be returned, keep in mind that it's possible that the key exists and was literally storing
 * `null`. Use `is_stashed(string $key)` to check if a key exists in the stash.
 */
function unstash(string $key) {
    return stash($key, null, 2);
}

/**
 * Stash something or return the value of something stashed
 * (this function is used for page rendering).
 *
 * @param string    $key (optional)
 * @param mixed     $value (optional)
 * @return mixed    Returns entire stash if $key not provided, returns the value for the specified
 *                  key if $value not provided, or returns the same value provided if value is
 *                  specified.
 */
function stash(string $key = null, $value = null, int $_op = 0) {
    static $_stash = [];

    if (!isset($key))
        return $_stash;

    switch ($_op) {
        case 0: // get/set
            if (!isset($value))
                return from($_stash, $key);
            return ($_stash[$key] = $value);
        case 1: // check
            return array_key_exists($key, $_stash);
        case 2: // remove
            $old = from($_stash, $key);
            unset($_stash[$key]);
            return $old;
        case 3: // fuzzy compare equality
            return from($_stash, $key) == $value;
        case 4: // strict compare equality
            return from($_stash, $key) === $value;
        case 5: // string compare
            return strcmp($value, from($_stash, $key));
        case 6: // xssafe get
            return xssafe(from($_stash, $key));
        case 7: // int get
            return intval(from($_stash, $key));
        case 8: // bool get
            return bool_unstr(from($_stash, $key));
        case 9: // array get
            return (array) from($_stash, $key);
    }
}

/**
 * Unsafe stash get.
 */
function u_stash(string $key) {
    return stash($key);
}

/**
 * Stash int get
 */
function i_stash(string $key) {
    return stash($key, null, 7);
}

/**
 * Stash bool get
 */
function b_stash(string $key) {
    return stash($key, null, 8);
}

/**
 * Stash array get
 */
function a_stash(string $key) {
    return stash($key, null, 9);
}

/**
 * Stash xss-safe array get
 */
function xa_stash(string $key) {
    return xssafe(a_stash($key));
}

/**
 * Return the value of something stashed with HTML escaped.
 *
 * @param string $key
 * @return mixed escaped data from stash
 */
function x_stash(string $key) {
    return stash($key, null, 6);
}

// ================================================================================
// RENDER UTILITIES
// ================================================================================

/**
 * Checks if a template exists within the views root.
 *
 * @param string    $view the template to check for
 * @return bool     true if exists, false otherwise
 */
function template_exists(string $view): bool {
    $view = str_replace(".", DIRECTORY_SEPARATOR, $view);
    $view_root = VIEWS_ROOT;
    return file_exists("{$view_root}/{$view}.html.php");
}

/**
 * Render a template without layout (i.e. not a page). Use this for rendering templates
 * within another template.
 *
 * @param string    $view the template to render
 * @param array     $locals (optional) associative array of variables to stash
 */
function render_template(string $view, array $locals = null): void {
    render($view, $locals, [
        'layout' => false,
    ]);
}

/**
 * Render a template without layout to a string (rather than echoing).
 *
 * @param string    $view the template to render
 * @param array     $locals (optional) associative array of variables to stash
 * @return string   the rendered template as a string
 */
function render_to_string(string $view, array $locals = null): string {
    ob_start();
    render_template($view, $locals);
    return trim(ob_get_clean());
}

// create_content_id
// ~~~~~~~~~~~~~~~~~
function create_content_id(string $view, string $next_view=null): string {
    $content_id = '__CONTENT-' . $view . '__';
    if (isset($next_view)) {
        stash($content_id, $next_view);
    }
    return $content_id;
}

// render_content_id
// ~~~~~~~~~~~~~~~~~
function render_content_id(string $view = null): void {
    $content_id = create_content_id($view ?? render_current());
    if (is_stashed($content_id)) {
        $prev = render_current();

        $next_view = u_stash($content_id);
        $require_path = VIEWS_ROOT . str_replace(".", DIRECTORY_SEPARATOR, $next_view) . ".html.php";

        render_current($next_view);
        require $require_path;

        if ($prev !== false && render_current() !== false) {
            render_current($prev);
        }
    }
}

/**
 * Echo the image src URL for a given image name under the SITE_IMAGES root.
 *
 * @param string $image_name
 */
function img_src(string $image_name): void {
    echo SITE_IMAGES.$image_name;
}

/**
 * Check if a body class exists. This function can be used within template files.
 *
 * @param string    $body_class the body CSS class to check for
 * @return bool     true if it exists, false otherwise
 */
function body_class_exists(string $body_class): bool {
    return is_stashed('VIEW') ? stash('VIEW')->bodyclass_exists($body_class) : false;
}

// ================================================================================
// RENDERING
// ================================================================================

/**
 * Returns the name of the current template being rendered, or returns false if nothing is
 * rendering. Never specify the `$_view` parameter, it is used internally.
 *
 * @return string|false
 */
function render_current(string $_view = null) {
    static $_current = false;
    return isset($_view) ? ($_current = $_view) : $_current;
}

/**
 * Render a template. Possible options include:
 *
 * - **http_code**: the HTTP response code (default: 200)
 * - **headers**: string array of HTTP response headers. Must not be associative, i.e.
 *    `['X-Something: foobar']` instead of ['X-Something' => 'foobar']
 * - **csp**: the value of the `Content-Security-Policy` header
 * - **layout**:
 *      - if `true`, the default layout template will be used
 *      - if `false`, no layout template will be used. Use this to dispatch a template within
 *        a template or to dispatch an HTML snippet
 *      - if a string, the template with this name will be used as the layout template
 * - **HEAD**: the webpage head, from the doctype string to the opening `body` tag
 *   (default: `<!DOCTYPE html><html><head><meta charset="utf-8" /><title>{SITE_TITLE}</title></head><body>`)
 * - **FOOT**: the webpage foot, from the ending `body` tag to the ending `html` tag
 *   (default: `"</body></html>"`)
 *
 * All options are optional.
 *
 * @param string    $view template name to render
 * @param array     $locals (optional) associative array of variables to stash
 * @param array     $options (optional) additional options for dispatch, such as `http_code`,
 *                  `headers`, `csp`, `layout`, `HEAD`, and `FOOT`
 */
function render(string $view, array $locals = null, array $options = []): void {
    if (is_array($locals)) {
        foreach ($locals as $key => $value) {
            stash($key, $value);
        }
    }

    if (from($options, 'layout') === false) {
        create_content_id('TMPL', $view);
        render_content_id('TMPL');
    } else {
        http_response_code($options['http_code'] ?? 200);
        header('Content-type: text/html; charset=utf-8', true, $options['http_code'] ?? 200);
        header('X-Content-Type-Options: nosniff', false);
        header('X-XSS-Protection: 1; mode=block', false);
        header('X-Frame-Options: SAMEORIGIN', false);
        header('X-Clacks-Overhead: GNU Terry Pratchett', false);
        header('Referrer-Policy: same-origin', false);
        header('Strict-Transport-Security: max-age=31536000; includeSubDomains', false);
        if (isset($options['csp'])) {
            header('Content-Security-Policy: ' . $options['csp'], false);
        }
        if (!empty($options['headers'])) {
            foreach ($options['headers'] as $custom_header) {
                header($custom_header, false);
            }
        }

        echo $options['HEAD'] ??
                    '<!DOCTYPE html><html><head><meta charset="utf-8" />' .
                    '<title>'.SITE_TITLE.'</title></head><body>';

        create_content_id('ROOT', $options['layout'] ?? VIEWS_LAYOUT);
        create_content_id($options['layout'] ?? VIEWS_LAYOUT, $view);
        render_current('ROOT');
        render_content_id();

        echo $options['FOOT'] ??
                    '</body></html>';

        render_current(false);
    }
}

/**
 * Dispatch a JSON response.
 *
 * @param mixed     $obj the value that will be json-encoded and dispatched
 * @param int       $code=200 (optional) the HTTP response code
 * @param string[]  $headers (optional) additional HTTP headers
 * @param bool      $prettify=false (optional) if true, the response will be prettified
 * @param string    $prefix (optional) if set, this string will be prepended to the response
 */
function dispatch_json($obj, int $code = 200, array $headers = [], bool $prettify = false, string $prefix = null): void {
    header('Content-Type: application/json', true, $code ?? 200);
    foreach ($headers as $header) {
        header($header, false);
    }

    $prettify = to_bool($_REQUEST['prettify'] ?? $prettify);
    $prefix = $prefix ?? '';

    echo ($prefix . $prettify ? json_encode($obj, JSON_PRETTY_PRINT) : json_encode($obj));
    die;
}

/**
 * Dispatch a raw HTML response; may be be a snippet, not necessarily a full webpage.
 *
 * @param string    $html the HTML snippet
 * @param int       $code=200 (optional) the HTTP response code
 * @param string[]  $headers (optional) additional HTTP headers
 */
function dispatch_html(string $html, int $code = 200, array $headers = []): void {
    header('Content-Type: text/html; charset=utf-8', true, $code ?? 200);
    foreach ($headers as $header) {
        header($header, false);
    }

    echo $html;
    die;
}