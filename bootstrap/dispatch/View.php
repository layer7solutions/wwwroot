<?php

/**
 * Construct new View class. Alias for `View::make($templates)`.
 *
 * @param mixed string varargs: templates
 * @return View
 */
function view(): View {
    return View::make(func_get_args());
}

/**
 * View - template system class.
 */
class View {
    private static $profiles = [];

    private $subresources = null;
    private $params = [];

    private $rendering_options = [
        // templates
        'base_template'     => null,
        'layout_template'   => VIEWS_LAYOUT,
        'profiles'          => [],

        // headers
        'http_code'         => 200,

        // head-meta
        'title'             => SITE_TITLE,
        'lang'              => 'en',
        'viewport'          => 'width=device-width, initial-scale=1',
        'uacompat'          => 'IE=edge',
        'icontype'          => 'ico',

        // body-meta
        'bodyclass'         => '',
    ];

    /**
     * Use `View::make()` instead.
     */
    private function __construct() {
        // empty
    }

    /**
     * Construct a new View.
     *
     * @param string[] $templates
     * @return View
     */
    public static function make(array $templates): View {
        $view = new self;
        $view->params['VIEW'] = $view;
        $view->subresources = new \SubresourceService();
        $view->addInitOpt('AppRequests', 'CSRF_TOKEN', session_csrf_token());
        $view->addJSVars([
            'SITE_URL'          => SITE_URL,
            'CURRENT_URL'       => current_url(),
            'SITE_JS'           => SITE_JS,
            'SITE_CSS'          => SITE_CSS,
            'SITE_IMAGES'       => SITE_IMAGES,
            'SITE_API'          => rtrim(SITE_API, '/'),
            'SITE_IS_STAGING'   => SITE_IS_STAGING,
        ]);

        for ($i = 0, $len = count($templates), $prev_content_id = null; $i < $len; $i++) {
            $content_id = $view->useProfile($templates[$i]);

            if ($i == 0) {
                // set base template if first arg
                $view->rendering_option('base_template', $templates[$i], false);
            } else if (!empty($prev_content_id)) {
                create_content_id($prev_content_id, $templates[$i]);
            }

            $prev_content_id = $content_id;
        }

        return $view;
    }

    /**
     * Get or set a rendering option.
     *
     * @param string        $prop the rendering option name
     * @param mixed         $v (optional) the option value
     * @param string|bool   $append (optional) if a value already exists for the specified option
     *                      name, if a string use this parameter as the separator between the
     *                      current value and the specified parameter value. If this parameter is a
     *                      bool and is false, then the current value will be overwritten. If true,
     *                      then the property will be treated as an array and will append the passed
     *                      in value as an array element, converting the exist value to array if
     *                      necessary.
     * @param bool          $usexssafe if true, xssafe will be invoked on the returned value for
     *                      get operations
     *
     * @return View|string|array    returns self if set operation, or returns the option value if get
     *                              operation
     */
    private function rendering_option(string $prop, $v = null, $append = '', bool $usexssafe=true) {
        if (isset($v)) {
            if (!empty($this->rendering_options[$prop])) {
                if (is_string($append)) {
                    // append as string
                    $this->rendering_options[$prop] .= $append . $v;
                } else if (is_bool($append)) {
                    if (!$append) {
                        $this->rendering_options[$prop] = $v; // overwrite
                        return $this;
                    }
                    if (!is_array($this->rendering_options[$prop])) {
                        // if not already an array, change to array
                        $this->rendering_options[$prop] = array($this->rendering_options[$prop]);
                    }
                    // append as element
                    $this->rendering_options[$prop][] = $v;
                }
            } else {
                if ($append === true) {
                    $this->rendering_options[$prop] = array($v);
                } else {
                    $this->rendering_options[$prop] = $v;
                }
            }
            return $this;
        } else {
            if ($usexssafe)
                return xssafe($this->rendering_options[$prop]);
            else
                return $this->rendering_options[$prop];
        }
    }

    // DISPATCH
    // --------------------------------------------------------------------------------

    /**
     * Get, set or remove the layout template option.
     *
     * @param bool|string|null  $layout true - use default layout, false - use no layout,
     *                          string - use the layout template with this name, null - return
     *                          the current layout
     * @return View|mixed       self if set operation or layout template value if get operation
     */
    public function useLayout($layout = null) {
        return $this->rendering_option('layout_template',
            $layout === true ? VIEWS_LAYOUT : $layout, false, false);
    }

    /**
     * Dispatch the View as the server response back to the client.
     *
     * @param array $params the parameters that will be available to the view template through the
     *              `stash()` function
     */
    public function dispatch(array $params = []): void {
        [$original, $this->subresources] = [$this->subresources, new \SubresourceService()];

        $this->submitProfile($this->rendering_option('layout_template'));
        foreach ($this->rendering_option('profiles') as $profile)
            $this->submitProfile($profile);

        [$compiled, $this->subresources] = [$this->subresources->mergeService($original), $original];

        $page_head  = '<!DOCTYPE html><html lang="'.$this->rendering_option('lang').'">';
        $page_head .= '<head><meta charset="utf-8" />';
        $page_head .= '<meta http-equiv="X-UA-Compatible" content="'.$this->rendering_option('uacompat').'" />';
        $page_head .= '<meta name="viewport" content="'.$this->rendering_option('viewport').'" />';
        $page_head .= '<link rel="icon" href="'.SITE_URL.'favicon.'.$this->rendering_option('icontype').'" />';
        $page_head .= '<title>'.$this->rendering_option('title').'</title>';
        $page_head .= $compiled->getResponseByZone('head-end') . '</head>';
        $page_head .= '<body class="'.$this->rendering_option('bodyclass').'">';
        $page_foot  = $compiled->getResponseByZone('body-end').'</body></html>';

        render($this->rendering_option('base_template'), $this->params, [
            'layout'        => $this->rendering_option('layout_template'),
            'http_code'     => $this->rendering_option('http_code'),
            'csp'           => SITE_IS_STAGING ? null : $compiled->getResponseByArea('CSP'),
            'headers'       => $compiled->getResponseByArea('header'),
            'HEAD'          => $page_head,
            'FOOT'          => $page_foot,
        ]);
    }

    /**
     * Same as `dispatch()` but the PHP execution exits afterwards.
     *
     * @see dispatch()
     */
    public function dispatchAndExit(array $params = []): void {
        $this->dispatch($params);
        exit;
    }

    // PROFILES
    // --------------------------------------------------------------------------------

    /**
     * Use this profile in this view. Only effects the 'profiles' rendering option, the profile
     * data will not be applied to the View until `dispatch()` is called.
     *
     * @param string        $profile_name the profile name
     * @return string|false the profile name or `false` if no registered profile exists with the
     *                      specified name
     */
    public function useProfile(string $profile_name): string {
        if (!isset(self::$profiles[$profile_name]))
            return false;
        $this->rendering_option('profiles', $profile_name, true);
        return self::$profiles[$profile_name]['view'];
    }

    /**
     * Check if this View is using the specified profile.
     *
     * @param string $profile_name
     * @return bool
     */
    public function hasProfile(string $profile_name): bool {
        return in_array($profile_name, $this->rendering_option('profiles'));
    }

    /**
     * Applies the profile data to the current View.
     *
     * @param string $profile_name the profile name
     */
    private function submitProfile(string $profile_name): void {
        if (!isset(self::$profiles[$profile_name]))
            return;
        $data = self::$profiles[$profile_name]['data'];
        $data = is_callable($data) ? $data($this) : $data;
        if (is_array($data))
            $this->using($data);
    }

    /**
     * Register a profile. The profile will be automatically applied to any Views that use
     * a template with the same name as the profile.
     *
     * @param string            $profile_name the profile name
     * @param callable|array    $profile_data array data or callable that returns array data
     */
    public static function addProfile(string $profile_name, $profile_data = []): void {
        self::$profiles[$profile_name = str_replace('/', '.', $profile_name)] = [
            'data' => $profile_data,
            'view' => $profile_name,
        ];
    }

    // HEAD/META SETTINGS
    // --------------------------------------------------------------------------------

    /**
     * Get or set the webpage title.
     *
     * @param string    $value new value
     * @return mixed    self if set operation, the value if get operation
     */
    public function title(string $value = null) {
        return $this->rendering_option('title', $value, false);
    }

    /**
     * Get or set the lang meta directive.
     *
     * @param string    $value new value
     * @return mixed    self if set operation, the value if get operation
     */
    public function lang(string $value = null) {
        return $this->rendering_option('lang', $value, false);
    }

    /**
     * Get or set the viewport meta directive.
     *
     * @param string    $value new value
     * @return mixed    self if set operation, the value if get operation
     */
    public function viewport(string $value = null) {
        return $this->rendering_option('viewport', $value, false);
    }

    /**
     * Get or set the favicon icon type.
     *
     * @param string    $value new value
     * @return mixed    self if set operation, the value if get operation
     */
    public function favicontype(string $value = null) {
        return $this->rendering_option('icontype', $value, false);
    }

    /**
     * Get or set the uacompat meta directive.
     *
     * @param string    $value new value
     * @return mixed    self if set operation, the value if get operation
     */
    public function uacompat(string $value = null) {
        return $this->rendering_option('uacompat', $value, false);
    }

    /**
     * Get or set the HTTP response code.
     *
     * @param int       $value new value
     * @return mixed    self if set operation, the value if get operation
     */
    public function http_code(int $value = null) {
        return $this->rendering_option('http_code', $value, false);
    }

    /**
     * Get or set the body class.
     *
     * @param string    $value new value
     * @return mixed    self if set operation, the value if get operation
     */
    public function bodyclass(string $value = null) {
        return $this->rendering_option('bodyclass', $value, ' ');
    }

    /**
     * Check if a body class exists.
     *
     * @param string|string[]   $bodyclass the bodyclass(es) to check for
     * @return bool             true if the bodyclass(es) exist, false otherwise
     */
    public function bodyclass_exists($bodyclass) {
        $haystack = explode(' ', $this->rendering_option('bodyclass'));
        $needle   = is_array($bodyclass) ? $bodyclass : explode(' ', trim($bodyclass));

        return !array_diff($needle, $haystack);
    }

    /**
     * Set the webpage title, bodyclass, and HTTP response code all at once.
     *
     * @param string    $title,
     * @param string    $bodyclass
     * @param int       $http_code
     * @return View     self
     */
    public function meta(string $title, string $bodyclass, int $http_code=200): View {
        return $this->title($title)->bodyclass($bodyclass)->http_code($http_code);
    }

    // SUBRESOURCES
    // --------------------------------------------------------------------------------

    /**
     * Add a stylesheet.
     *
     * @param string|string[]   $stylesheets
     * @return View             self
     */
    public function stylesheets($stylesheets): View {
        foreach ((array) $stylesheets as $fname)
            $this->subresources->add('style', $fname);
        return $this;
    }

    /**
     * Add a stylesheet, optionally with ID.
     *
     * @param string            $stylesheet
     * @param string            $id=null (optional) CSS ID
     * @param array             $attr (optional) custom attributes
     * @return View             self
     */
    public function stylesheet(string $stylesheet, string $id=null, array $attr=[]): View {
        $this->subresources->add('style', [
            'fname'     => $stylesheet,
            'id'        => isset($id) ? $id : null,
            'attr'      => $attr,
        ]);
        return $this;
    }

    /**
     * Add a script.
     *
     * @param string|string[]   $scripts
     * @return View             self
     */
    public function scripts($scripts): View {
        foreach ((array) $scripts as $fname)
            $this->subresources->add('script', $fname);
        return $this;
    }

    /**
     * Add a script, optionally with ID.
     *
     * @param string            $script
     * @param string            $id=null (optional) CSS ID
     * @param array             $attr (optional) custom attributes
     * @return View             self
     */
    public function script(string $script, string $id=null, array $attr=[]): View {
        $this->subresources->add('script', [
            'fname'     => $script,
            'id'        => isset($id) ? $id : null,
            'attr'      => $attr,
        ]);
        return $this;
    }

    /**
     * Add an async script.
     *
     * @param string|string[]   $scripts
     * @return View             self
     */
    public function async_scripts($scripts): View {
        foreach ((array) $scripts as $fname)
            $this->subresources->add('script', [
                'fname' => $fname,
                'async' => true,
            ]);
        return $this;
    }

    /**
     * Add a deferred script.
     *
     * @param string|string[]   $scripts
     * @return View             self
     */
    public function deferred_scripts($scripts): View {
        foreach ((array) $scripts as $fname)
            $this->subresources->add('script', [
                'fname' => $fname,
                'defer' => true,
            ]);
        return $this;
    }

    /**
     * Add a stylesheet from another site on this server.
     *
     * @param string        $sitename the `SITE_NAME` config value in the other site's `config.php`
     * @param string        $url the full URL to the resource on the other site
     * @param string        $resource_id the resource ID, matching the `X` that would be put into
     *                      the `view(...)->stylesheets(X)` method on the other site.
     * @param array         $opts=[] (optional) additional options
     * @return View         self
     */
    public function virtual_stylesheet(string $sitename, string $resource_id, string $url, array $opts = []): View {
        $this->subresources->addSubLocal('style', $sitename, $url, $resource_id, $opts);
        return $this;
    }

    /**
     * Add a script from another site on this server.
     *
     * @param string        $sitename the `SITE_NAME` config value in the other site's `config.php`
     * @param string        $url the full URL to the resource on the other site
     * @param string        $resource_id the resource ID, matching the `X` that would be put into
     *                      the `view(...)->scripts(X)` method on the other site.
     * @param array         $opts=[] (optional) additional options
     * @return View         self
     */
    public function virtual_script(string $sitename, string $resource_id, string $url, array $opts = []): View {
        $this->subresources->addSubLocal('script', $sitename, $url, $resource_id, $opts);
        return $this;
    }

    /**
     * Add a lib (alias of `usinglib`).
     *
     * @param string|string[]   $libs
     * @return View             self
     */
    public function uselib($lib): View {
        return $this->usinglib($lib);
    }

    /**
     * Add a lib.
     *
     * @param string|string[]   $libs
     * @return View             self
     */
    public function usinglib($libs): View {
        foreach ((array) $libs as $fname) {
            if ($this->subresources->addLib($fname))
                continue;
            $this->subresources->add('style', 'lib/' . $fname);
            $this->subresources->add('script', 'lib/' . $fname);
        }
        return $this;
    }

    /**
     * Manually add a Content-Security-Policy value.
     *
     * **Warning:** by adding a URL to the CSP header,
     * it allows that URL or any sub-URL of it to be used in the page! For example, adding
     * `https://i.imgur.com/` would allow any image from imgur.
     *
     * @param string            $type the CSP directive type
     * @param string|string[]   $value the URL value to prepend to the directive value
     * @return View             self
     */
    public function addCSP(string $type, $values) {
        foreach ((array) $values as $value) {
            $this->subresources->addDirect('CSP', [$type, $value]);
        }
        return $this;
    }

    /**
     * Get the integrity for the specified style resource and update the CSP.
     *
     * @param string        $style the resource name
     * @return string|null  the integrity, or null if not successful
     */
    public function getStyleIntegrity(string $style): ?string {
        return $this->subresources->getIntegrity('style', $style);
    }

    /**
     * Get the integrity for the specified script resource and update the CSP.
     *
     * @param string        $script the resource name
     * @return string|null  the integrity, or null if not successful
     */
    public function getScriptIntegrity(string $script): ?string {
        return $this->subresources->getIntegrity('script', $script);
    }

    // PARAMETER GET/SET
    // --------------------------------------------------------------------------------

    /**
     * Get or set a template parameter.
     *
     * @param string    $k the param key
     * @param mixed     $v (optional) new value
     * @return mixed    self if set operation, the value if get operation
     */
    public function use(string $k, $v = null) {
        if (func_num_args() === 2) { // `null` can be a valid value
            $this->params[$k] = $v;
            return $this;
        } else if (is_array($k) && is_assoc($k)) {
            foreach ($k as $kk => $vv) {
                $this->use($kk, $vv);
            }
            return $this;
        } else {
            return $this->params[$k];
        }
    }

    /**
     * Check if a template parameter exists
     *
     * @param string    $k the key of the parameter to check
     * @return bool     true if it exists, false otherwise
     */
    public function usable(string $k): bool {
        return array_key_exists($k, $this->params);
    }

    /**
     * Batch add template parameters.
     *
     * @param array     $data associative key to value array
     * @return View     self
     */
    public function using(array $data): View {
        foreach ($data as $k => $v) {
            $this->use($k, $v);
        }
        return $this;
    }

    /**
     * Set a template parameter if the condition is true
     *
     * @param string    $k the param key
     * @param bool      $cond the condition to set the template parameter
     * @param mixed     $v the new value, or a callable that returns the new value
     * @return mixed    self
     */
    public function useIf(string $k, bool $cond, $v): View {
        return $cond ? $this->use($k, is_callable($v) ? $v($this) : $v) : $this;
    }

    /**
     * Batch add template params if the first parameter is true.
     *
     * @param bool              $cond the condition to add template params
     * @param array|callable    $data associative key to value array or a callable that returns the
     *                          former
     * @return View             self
     */
    public function usingIf(bool $cond, $data): View {
        return $cond ? $this->using(is_callable($data) ? $data($this) : $data) : $this;
    }

    // INIT OPTS
    // --------------------------------------------------------------------------------

    /**
     * Add a global JavaScript variable.
     *
     * @param string    $var_name the variable name, can contain dots to add the variable to
     *                  existing objects, e.g. 'app.somevar'
     * @param mixed     $var_data the variable value
     * @return View     self
     */
    public function addJSVar(string $var_name, $var_data): View {
        $this->subresources->addInitVar($var_name, $var_data);
        return $this;
    }

    /**
     * Batch add global JavaScript variables.
     *
     * @param array     $data associative key to value array
     * @return View     self
     */
    public function addJSVars(array $vars_array): View {
        foreach ($vars_array as $k => $v) {
            $this->subresources->addInitVar($k, $v);
        }
        return $this;
    }

    /**
     * Add an JavaScript init option.
     *
     * @param string    $initName the init name (should match the first parameter of `init()` in
     *                  the relevant JavaScript file for the page)
     * @param string    $opt_name the option name, will be the key in the `opts` parameter passed
     *                  to the JS `init()` callback
     * @param mixed     $opt_data the value of the option
     * @return View     self
     */
    public function addInitOpt(string $initname, string $opt_name, $opt_data): View {
        $this->subresources->addInitOpt($initname, $opt_name, $opt_data);
        return $this;
    }

    /**
     * Batch add JavaScript init options.
     *
     * @param array     $data associative key to value array
     * @return View     self
     */
    public function addInitOpts(array $opts_array): View {
        foreach ($opts_array as $k => $v) {
            $this->subresources->addInitOpt($k, $v);
        }
        return $this;
    }
}