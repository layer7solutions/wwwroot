<?php

require BOOTSTRAP_ROOT . 'dispatch/area/AbstractArea.php';
require BOOTSTRAP_ROOT . 'dispatch/area/SubresourceArea.php';
require BOOTSTRAP_ROOT . 'dispatch/area/CSPBuilderArea.php';

/**
 * SubresourceService.php
 * ~~~~~~~~~~~~~~~~~~~~~~
 *
 * Handles subresource dependencies like styles, scripts, fonts;
 * and also handles preloading and CSP generation.
 */
final class SubresourceService {

    /**
     * For security, we only allow third-party libraries that are listed here.
     * @var array
     */
    const LIBRARIES = [
        'jquery' =>  [
            'area' => 'script',
            'url' => 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js',
            'integrity' => 'sha384-xBuQ/xzmlsLoJpyjoggmTEz8OWUFM0/RC5BsqQBDX2v5cMvDHcMakNTNrHIW2I5f',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'
            ],
            'preload' => false,
        ],
        'jquery-1-7-2' => [
            'area' => 'script',
            'url' => 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js',
            'integrity' => 'sha384-SDFvKZaD/OapoAVqhWJM8vThqq+NQWczamziIoxiMYVNrVeUUrf2zhbsFvuHOrAh',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'
            ],
            'preload' => false,
        ],
        'lunr' => [
            'area' => 'script',
            'url' => 'https://cdnjs.cloudflare.com/ajax/libs/lunr.js/2.3.6/lunr.min.js',
            'integrity' => 'sha384-YdPYeaxM24+H15hfctQFgRYpwaquoibf+6a8fpgW6ZW6urvEZaYICEzGPAql4qQm',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/lunr.js/2.3.6/lunr.min.js'
            ],
            'preload' => false,
        ],
        'code_prettify' => [
            'area' => 'script',
            'url' => 'https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js',
            'integrity' => 'sha384-Jp7FwG5KzETm/MUyxDMnd+hPZ0j/s/ekpD65zns5NIGkKYujpBg5H1VrDQroAlH7 sha256-weUWuhW0uXf7qTR7y2MS4cXAOth8vU68wr1i019KsQ0=',
            'csp' => [
                'script' => 'https://cdn.rawgit.com/google/code-prettify/master/loader/',
                'style' => 'https://cdn.rawgit.com/google/code-prettify/master/loader/prettify.css'
            ],
            'crossorigin' => 'anonymous',
            'preload' => false,
        ],
        'classListPolyfill' => [
            'area' => 'script',
            'url' => 'https://cdnjs.cloudflare.com/ajax/libs/classlist/2014.01.31/classList.min.js',
            'integrity' => 'sha384-JwsigiAwIQ/fqnnekO0f0uVnKcqiR+8Sb+/tDFzYz7TyHgFOuW0EGcQEWntwPq2I',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/classlist/2014.01.31/classList.min.js'
            ],
            'preload' => false,
        ],
        'autolinker' => [
            'area' => 'script',
            'url' => 'https://cdnjs.cloudflare.com/ajax/libs/autolinker/3.11.0/Autolinker.min.js',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/autolinker/3.11.0/Autolinker.min.js',
            ],
            'preload' => false,
        ],
        'velocity' => [
            'area' => 'script',
            'url' => 'https://cdnjs.cloudflare.com/ajax/libs/velocity/2.0.5/velocity.min.js',
            'integrity' => 'sha384-OAa+lnzjUAtY24vqAEB8CYxD/8pX99G3ieMIN16c7UyXUDfFrAEMK+5VDIBDkc55',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/velocity/2.0.5/velocity.min.js'
            ],
            'preload' => false,
        ],
        'selectr-js' => [
            'area' => 'script',
            'url' => 'https://unpkg.com/mobius1-selectr@2.4.13/dist/selectr.min.js',
            'integrity' => 'sha384-kpRfuhIjTUhRJPetumAT2UykEKnqXdurw75vUs2015lqpEhsRfkoOF/gMcrWr02F',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://unpkg.com/mobius1-selectr@2.4.13/dist/selectr.min.js'
            ],
            'preload' => false,
        ],
        'selectr-css' => [
            'area' => 'style',
            'url' => 'https://unpkg.com/mobius1-selectr@2.4.13/dist/selectr.min.css',
            'integrity' => 'sha384-O1PAcYRxlWavYIL+m80+1ZJEeTsz7WNgQ2OuONoGg4dgLExWaGMPQXVs5hlSbNIE',
            'crossorigin' => 'anonymous',
            'csp' => [
                'style' => 'https://unpkg.com/mobius1-selectr@2.4.13/dist/selectr.min.css'
            ],
            'preload' => false,
        ],
        'hammer' => [
            'area' => 'script',
            'url' => 'https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js',
            'integrity' => 'sha384-Cs3dgUx6+jDxxuqHvVH8Onpyj2LF1gKZurLDlhqzuJmUqVYMJ0THTWpxK5Z086Zm',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js'
            ],
            'preload' => false,
        ],
        'tippy' => [
            'area' => 'script',
            'url' => 'https://cdnjs.cloudflare.com/ajax/libs/tippy.js/3.4.1/tippy.all.min.js',
            'integrity' => 'sha384-Gr2n4kiRGkbURUNlwmVJMNdICEwp0zdoAYZhtrVNxNQspK+jvvILAYjsr12sh1WH',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/tippy.js/3.4.1/tippy.all.min.js'
            ],
            'preload' => false,
            'defer' => true,
        ],
        'moment' => [
            'area' => 'script',
            'url' => 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js',
            'integrity' => 'sha384-fYxN7HsDOBRo1wT/NSZ0LkoNlcXvpDpFy6WzB42LxuKAX7sBwgo7vuins+E1HCaw',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js',
            ],
            'preload' => false,
        ],
        'shvl' => [
            'area' => 'script',
            'url' => 'https://unpkg.com/shvl@2.0.0/dist/shvl.umd.js',
            'integrity' => 'sha384-n0B6PYlv7kDlQxTuXKDjkpOysxK5zKGzu170bzOm/P2VSjeDHW7EfJsYsCEvezFK',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://unpkg.com/shvl@2.0.0/dist/shvl.umd.js'
            ],
            'preload' => false,
        ],
        'deepmerge' => [
            'area' => 'script',
            'url' => 'https://unpkg.com/deepmerge@4.0.0/dist/umd.js',
            'integrity' => 'sha384-V/5Cg6p+Nyk5zweaDJzeixVwUpPdR/lnM2GftVQxuSSu0wFWL3KZ6U62ytpsqEGa',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://unpkg.com/deepmerge@4.0.0/dist/umd.js'
            ],
            'preload' => false,
        ],
        'vue' => [
            'area' => 'script',
            'url' => 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js',
            'integrity' => 'sha384-8t+aLluUVnn5SPPG/NbeZCH6TWIvaXIm/gDbutRvtEeElzxxWaZN+G/ZIEdI/f+y',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js'
            ],
            'preload' => false,
        ],
        'vuex' => [
            'area' => 'script',
            'url' => 'https://cdnjs.cloudflare.com/ajax/libs/vuex/3.1.1/vuex.min.js',
            'integrity' => 'sha384-GIuPvKLubNw7l6uylbFJuboOEkyUGornNrBpOzyjnX38dmiknlqWy+qN8a87EMZK sha256-6XOQHnT/ZYiLvi5YuVz7lX1dsxa7GFoQb1Q9mRdtHWU=',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/vuex/3.1.1/vuex.min.js'
            ],
            'preload' => false,
        ],
        'vuex-persistedstate' => [
            'area' => 'script',
            'url' => 'https://unpkg.com/vuex-persistedstate@2.5.4/dist/vuex-persistedstate.umd.js',
            'integrity' => 'sha384-xXlS3G0OCYYVLnbAoPfWkW88OKE635zD8HCShknJUOQDT3EhjtZjSrv0UeTh0R0K',
            'crossorigin' => 'anonymous',
            'csp' => [
                'script' => 'https://unpkg.com/vuex-persistedstate@2.5.4/dist/vuex-persistedstate.umd.js'
            ],
            'preload' => false,
        ],
        'zmdi' => [
            'area' => 'fonticon',
            'url' => 'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css',
            'integrity' => 'sha384-D4wURFBFb4uPtzPg8AaqEK+EDGBoc/Fb7oSvGS5/6/as85Nnyh500ZnDa54R6B1T',
            'crossorigin' => 'anonymous',
            'csp' => [
                'font' => 'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/fonts/',
                'style' => 'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css',
            ],
            'preload' => [
                ['font', 'font/woff2', 'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/fonts/Material-Design-Iconic-Font.woff2?v=2.2.0'],
            ],
        ],
        'lato_font' => [
            'area' => 'font',
            'url' => 'https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i',
            'integrity' => false,
            'crossorigin' => 'anonymous',
            'csp' => [
                'font' => 'https://fonts.gstatic.com/',
                'style' => 'https://fonts.googleapis.com/',
            ],
            'preload' => false,
        ],
        'fontawesome' => [
            'area' => 'fonticon',
            'url' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
            'integrity' => 'sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN',
            'crossorigin' => 'anonymous',
            'csp' => [
                'font' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/',
                'style' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
            ],
            'preload' => [
                ['font', 'font/woff2', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0'],
            ],
        ],
        'default_fonts' => [
            'area' => 'font',
            'url' => 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700|Source+Sans+Pro:300,300i,400,400i,600,600i,700',
            'integrity' => false,
            'crossorigin' => 'anonymous',
            'csp' => [
                'font' => 'https://fonts.gstatic.com/',
                'style' => 'https://fonts.googleapis.com/',
            ],
            'preload' => false,
        ],
        'roboto_font' => [
            'area' => 'font',
            'url' => 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700',
            'integrity' => false,
            'crossorigin' => 'anonymous',
            'csp' => [
                'font' => 'https://fonts.gstatic.com/',
                'style' => 'https://fonts.googleapis.com/',
            ],
            'preload' => false,
        ]
    ];

    /**
     * @var AbstractArea[] collections of resource areas within this SubresourceService
     */
    private $resource_areas = [];

    /**
     * Order of compliation of resource areas.
     * @var array
     */
    private $resource_zones = [
        'head-end' => [
            'initopt',
            'fonticon',
            'style',
            'script',
            'font',
        ],
        'body-end' => [
        ],
    ];

    /**
     * Construct the Subresource Service. The default resource areas are initialized within
     * this constructor.
     */
    public function __construct() {
        $this->registerResourceArea(new CSPBuilderArea());
        $this->registerResourceArea(new SubresourceArea([
            'area' => 'initopt',
            'compiler' => function($res) {
                return
                    '<meta ' .
                        'name="x-init-opts" ' .
                        'content="' . implode(',', array_keys($res)) . '" ' .
                        implode(' ',array_values($res)) .
                    '/>';
            },
        ]));
        $this->registerResourceArea(new SubresourceArea([
            'area' => 'script',
            'ext' => 'js',
            'generator' => function($data) {
                $html = '<script type="text/javascript"';
                $html .= ' src="' . $data['url'] . '"';
                $html .= ' crossorigin="' . $data['crossorigin'] . '"';
                if (!empty($data['integrity']) && !SITE_IS_STAGING)
                    $html .= ' integrity="' . $data['integrity'] . '"';
                if (!empty($data['async']))
                    $html .= ' async ';
                if (!empty($data['defer']))
                    $html .= ' defer ';
                if (!empty($data['id']))
                    $html .= ' id="'.$data['id'].'" ';
                foreach ($data['attr'] ?? [] as $key => $value) {
                    $html .= " $key=\"$value\" ";
                }
                $html .= '></script>';
                return $html;
            },
            'location_from_fname' => function($data) {
                $http_path = $data['http_path'] ?? SITE_JS;
                $file_path = $data['file_path'] ?? JS_ROOT;
                $file_ver = $data['file_version'] ?? JS_VERSION;

                return [
                    $http_path . $data['fname'] . '.js?v=' . $file_ver,
                    $file_path . $data['fname'] . '.js',
                ];
            },
            'csp_directive' => 'script-src',
        ]));
        $this->registerResourceArea(new SubresourceArea([
            'area' => 'fonticon',
            'generator' => function($data) {
                $html = '<link rel="stylesheet" type="text/css" media="all"';
                $html .= ' href="' . $data['url'] . '"';
                $html .= ' crossorigin="' . $data['crossorigin'] . '"';
                if (!empty($data['integrity']) && !SITE_IS_STAGING)
                    $html .= ' integrity="' . $data['integrity'] . '"';
                if (!empty($data['id']))
                    $html .= ' id="'.$data['id'].'" ';
                foreach ($data['attr'] ?? [] as $key => $value) {
                    $html .= " $key=\"$value\" ";
                }
                $html .= ' />';
                return $html;
            },
            'location_from_fname' => function($data) {
                $http_path = $data['http_path'] ?? SITE_CSS;
                $file_path = $data['file_path'] ?? CSS_ROOT;
                $file_ver = $data['file_version'] ?? CSS_VERSION;

                return [
                    $http_path . $data['fname'] . '.css?v=' . $file_ver,
                    $file_path . $data['fname'] . '.css',
                ];
            },
            'csp_default_use_dirname' => true,
            'csp_directive' => 'font-src',
        ]));
        $this->registerResourceArea(new SubresourceArea([
            'area' => 'style',
            'ext' => 'css',
            'generator' => function($data) {
                $html = '<link rel="stylesheet" type="text/css" media="all"';
                $html .= ' href="' . $data['url'] . '"';
                $html .= ' crossorigin="' . $data['crossorigin'] . '"';
                if (!empty($data['integrity'] && !SITE_IS_STAGING))
                    $html .= ' integrity="' . $data['integrity'] . '"';
                if (!empty($data['id']))
                    $html .= ' id="'.$data['id'].'" ';
                foreach ($data['attr'] ?? [] as $key => $value) {
                    $html .= " $key=\"$value\" ";
                }
                $html .= ' />';
                return $html;
            },
            'location_from_fname' => function($data) {
                $http_path = $data['http_path'] ?? SITE_CSS;
                $file_path = $data['file_path'] ?? CSS_ROOT;
                $file_ver = $data['file_version'] ?? CSS_VERSION;

                return [
                    $http_path . $data['fname'] . '.css?v=' . $file_ver,
                    $file_path . $data['fname'] . '.css',
                ];
            },
            'csp_directive' => 'style-src',
        ]));
        $this->registerResourceArea(new SubresourceArea([
            'area' => 'font',
            'generator' => function($data) {
                $html = '<link rel="stylesheet" type="text/css" media="all"';
                $html .= ' href="' . $data['url'] . '"';
                $html .= ' crossorigin="' . $data['crossorigin'] . '"';
                if (!empty($data['integrity']) && !SITE_IS_STAGING)
                    $html .= ' integrity="' . $data['integrity'] . '"';
                if (!empty($data['id']))
                    $html .= ' id="'.$data['id'].'" ';
                foreach ($data['attr'] ?? [] as $key => $value) {
                    $html .= " $key=\"$value\" ";
                }
                $html .= ' />';
                return $html;
            },
            'location_from_fname' => function($data) {
                $http_path = $data['http_path'] ?? SITE_CSS;
                $file_path = $data['file_path'] ?? CSS_ROOT;
                $file_ver = $data['file_version'] ?? CSS_VERSION;

                return [
                    $http_path . $data['fname'] . '.css?v=' . $file_ver,
                    $file_path . $data['fname'] . '.css',
                ];
            },
            'csp_default_use_dirname' => true,
            'csp_directive' => 'font-src',
        ]));
        $this->registerResourceArea(new SubresourceArea([
            'area' => 'header',
            'generator' => function($data, $old_data) {
                if (empty($data) || !is_string($data))
                    return false;
                if (empty($old_data))
                    return $data;
                return $old_data . ',' . $data;
            },
            'compiler' => function($res) {
                $headers = [];
                foreach ($res as $key => $value) {
                    $headers[] = $key . ": " . $value;
                }
                return $headers;
            },
            'initial' => [
                ["Link", "<https://fonts.gstatic.com>; rel=preconnect, ".
                         "<https://fonts.googleapis.com>; rel=preconnect"]
            ]
        ]));
    }

    /**
     * Add a resource area to the Service.
     */
    private function registerResourceArea(AbstractArea $area): void {
        $this->resource_areas[(string) $area] = $area;
    }

    /**
     * Merge the contents of another SubresourceService with this one.
     *
     * @param SubresourceService    $other the other Service
     * @param bool                  $appendPrependN whether to append or prepend the other contents
     * @return SubresourceService   self
     */
    public function mergeService(SubresourceService $other, bool $appendPrependN = true): SubresourceService {
        foreach ($other->resource_areas as $areaname => $area) {
            $this->resource_areas[$areaname]->merge($area, $appendPrependN);
        }
        return $this;
    }

    // LIB FUNCTIONS
    // ~~~~~~~~~~~~~

    /**
     * Check if the given resource name is a library.
     *
     * @param string    $name the resource name
     * @return bool     true if a lib, false otherwise
     */
    public function isLib(string $name): bool {
        return isset(self::LIBRARIES[$name]);
    }

    /**
     * Add a resource library by name.
     *
     * @param string    $name the resource name
     * @return bool     true if successful, false otherwise
     */
    public function addLib(string $name): bool {
        return !$this->isLib($name) ? false : $this->add(self::LIBRARIES[$name]['area'], $name);
    }

    // GET RESPONSE FUNCTIONS
    // ~~~~~~~~~~~~~~~~~~~~~~

    /**
     * Get the resource response by resource area name
     *
     * @param string $area_name
     * @return string|mixed
     */
    public function getResponseByArea(string $area_name) {
        return $this->resource_areas[$area_name]->get();
    }

    /**
     * Get the resource response by resource zone name
     *
     * @param string $zone_name
     * @return string|mixed
     */
    public function getResponseByZone(string $zone_name) {
        return implode('', array_map(array($this, 'getResponseByArea'), $this->resource_zones[$zone_name]));
    }

    // INIT OPTS FUNCTIONS
    // ~~~~~~~~~~~~~~~~~~~

    /**
     * Add an init option.
     *
     * @param string    $initName the init name (should match the first parameter of `init()` in
     *                  the relevant JavaScript file for the page)
     * @param string    $opt_name the option name, will be the key in the `opts` parameter passed
     *                  to the JS `init()` callback
     * @param mixed     $opt_data the value of the option
     * @return bool     true if successful, false otherwise
     */
    public function addInitOpt(string $initName, string $opt_name, $opt_data): bool {
        if (!preg_match('/^[A-Za-z0-9_]+$/', $initName) || !preg_match('/^[A-Za-z0-9_\.]+$/', $opt_name)) {
            return false;
        }

        $safe_name = xssafe($initName . '-' . str_replace('.', '-', $opt_name));
        $safe_data = xssafe(json_encode($opt_data, JSON_UNESCAPED_UNICODE));

        return $this->resource_areas['initopt']->addResource(
            $safe_name,
                "data-{$safe_name}=\"{$safe_data}\""
        );
    }

    /**
     * Add an init variable.
     *
     * @param string    $var_name the name of the variable - e.g. `foobar` will create a global
     *                  variable `foobar` or `something.foobar` will create a property inside the
     *                  global object variable `something`
     * @param mixed     $var_data the variable value
     * @return bool     true if successful, false otherwise
     */
    public function addInitVar(string $var_name, $var_data): bool {
        return $this->addInitOpt('var', $var_name, $var_data);
    }

    // ADD RESOURCE FUNCTIONS
    // ~~~~~~~~~~~~~~~~~~~~~~

    /**
     * Add a resource (only for SubresourceAreas, not 'header' or 'CSP')
     *
     * @param string        $areaname the SubresourceArea name
     * @param string|array  $options
     * @return bool         true if successful, false otherwise
     */
    public function add(string $areaname, $options): bool {
        if (is_string($options)) {
            $options = ['fname' => str_replace(".", "/", $options)];
        }
        return $this->resource_areas[$areaname]->addResource($this->_getData($areaname, $options));
    }

    /**
     * Add a resource directly.
     *
     * @param string        $areaname the SubresourceArea name
     * @param array         $parameters the parameters array for the area
     * @return bool         true if successful, false otherwise
     */
    public function addDirect(string $areaname, array $parameters): bool {
        return $this->resource_areas[$areaname]->addResource(... $parameters);
    }

    /**
     * Add a resource that exists on a different site using this same framework
     * on the current server.
     *
     * @param string        $areaname the SubresourceArea name
     * @param string        $sitename the `SITE_NAME` config value in the other site's `config.php`
     * @param string        $url the full URL to the resource on the other site
     * @param string        $resource_id the resource ID, matching the `X` that would be put into
     *                      the `view(...)->stylesheets(X)` or `view(...)->scripts(X)` methods.
     * @param array         $opts=[] (optional) additional options
     * @return bool         true if successful, false otherwise
     */
    public function addSubLocal(string $areaname, string $sitename, string $url, string $resource_id, array $opts = []): bool {
        $opts += [
            'sitename'      => $sitename,
            'area'          => $areaname,
            'resource_id'   => $resource_id,
            'url'           => $url,
            'csp'           => full_domain($url),
        ];
        $opts['integrity'] = $opts['integrity'] ?? $this->getCachedLocalIntegrity($opts, true)[1];
        return $this->resource_areas[$areaname]->addResource($this->_getData($areaname, $opts));
    }

    /**
     * Get the integrity of a specific resource with the assumption that resource of interest will
     * not be served with the page but will be added at some later point by JavaScript. This
     * function will also add the CSP value for the resource to the CSP header.
     *
     * @param string        $areaname the SubresourceArea name
     * @param string|array  $options
     * @return string|null  the integrity, or null if not successful
     */
    public function getIntegrity(string $areaname, $options): ?string {
        if (is_string($options)) {
            $options = ['fname' => str_replace(".", "/", $options)];
        }
        $data = $this->_getData($areaname, $options);
        if ($data === false || !is_array($data)) {
            return null;
        }
        return $data['integrity'] ?? null;
    }

    // INTERNAL FUNCTIONS
    // ~~~~~~~~~~~~~~~~~~

    /**
     * Get necessary data (e.g. `'integrity'`, `'csp'`, `'url'`) for a SubresourceArea.
     *
     * @param string        $areaname the SubresourceArea name
     * @param array         $data input data
     * @return array|false  the output data as an associative array or false if unsuccessful
     */
    protected function _getData(string $area_name, array $data) {
        if (isset($data['fname'])) {
            $data = array_merge($data, self::LIBRARIES[$data['fname']] ?? [
                'resource_name' => $data['fname'],
                'area'          => $area_name,
                'integrity'     => null,
                'crossorigin'   => 'anonymous',
                'csp'           => "'self'",
                'filepath'      => null,
                'preload'       => false,
            ]);

            $location_from_fname = $this->resource_areas[$data['area']]['location_from_fname'];
            if (empty($data['url']) && isset($location_from_fname)) {
                list($data['url'], $data['filepath']) = $location_from_fname($data);
            }
        }

        if (empty($data['url'])) {
            return false;
        }

        if (isset($data['filepath']) && !file_exists($data['filepath'])) {
            return false;
        }

        if (!empty($data['preload'])) {
            foreach ($data['preload'] as list($area, $mimetype, $url)) {
                $this->resource_areas['header']->addResource(
                    'Link',
                        '<'.$url.'>; rel=preload; as=' . $area .
                        '; type="' . $mimetype . '"; crossorigin="anonymous"'
                );
            }
        }
        if (!empty($data['csp'])) {
            if (!is_array($data['csp'])) {
                $data['csp'] = [$data['area'] => $data['csp'] ?? null];
            }
            foreach ($data['csp'] as $area => $csp_value) {
                $csp_directive = $this->resource_areas[$area]['csp_directive'];
                if (!isset($csp_value)) {
                    $csp_value = $data['url'];
                    if ($this->resource_areas[$area]['csp_default_use_dirname']) {
                        $csp_value = dirname($csp_value);
                    }
                }
                if (is_array($csp_value)) {
                    foreach ($csp_value as $v) {
                        $this->resource_areas['CSP']->addResource($csp_directive, $v);
                    }
                } else {
                    $this->resource_areas['CSP']->addResource($csp_directive, $csp_value);
                }
            }
        }

        $data['crossorigin'] = $data['crossorigin'] ?? 'anonymous';

        $data['integrity'] = $data['integrity'] ?? $this->createLocalIntegrity([
            'sitename'    => $data['sitename'] ?? SITE_NAME,
            'area'        => $data['area'],
            'filepath'    => $data['filepath'],
            'resource_id' => $data['resource_name'] ?? $data['filepath'],
            'SHA_types'   => [384,256],
        ]);

        return isset($data['integrity']) ? $data : false;
    }

    /**
     * Get the cached local integrity for a sublocal resource
     *
     * @param array     $opts the options, requires at least `sitename`, `area`, and `resource_id`
     * @param bool      $ignore_version_match=false if true, ignore whether or not the version of
     *                  the cached integrity matches the current versions
     * @return array    returns an array `[$cached_key, $cached_value]` where `$cached_value` may be
     *                  null if the integrity is not currently cached or if the integrity's version
     *                  does not match the current version
     */
    private function getCachedLocalIntegrity(array $opts, bool $ignore_version_match = false): array {
        $cached_key = implode('.', ['SRI', $opts['sitename'], $opts['area'], $opts['resource_id']]);

        if (!$ignore_version_match) {
            $CURRENT_VERSION = $opts['area'] == 'script' ? JS_VERSION : CSS_VERSION;
            $version_key = implode('.', ['SRIv', $opts['sitename'], $opts['area'], $opts['resource_id']]);

            if (cached($version_key) !== $CURRENT_VERSION) {
                cached($version_key, $CURRENT_VERSION); // update to current version
                return [$cached_key, null]; // return null to compute new integrity and update cache
            }
        }

        return [$cached_key, cached($cached_key)];
    }

    /**
     * Create local integrity from the specified options.
     *
     * @param array     $opts the options, requires at least `sitename`, `area`, `resource_id`,
     *                  `filepath`, and `SHA_types`
     * @return string   the local resource integrity string
     */
    private function createLocalIntegrity(array $opts): string {
        list($cached_key, $cached_integrity) = $this->getCachedLocalIntegrity($opts);

        if (!empty($cached_integrity)) {
            return $cached_integrity;
        } else {
            $content = file_contents($opts['filepath']);

            $integrity = implode(' ', array_map(function($SHA_type) use ($content) {
                return 'sha'.$SHA_type.'-'.base64_encode(hash('sha'.$SHA_type, $content, true));
            }, $opts['SHA_types']));

            cached($cached_key, $integrity);
            return $integrity;
        }
    }
}