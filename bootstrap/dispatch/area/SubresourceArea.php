<?php

/**
 * Standard subresource area.
 */
final class SubresourceArea extends AbstractArea {
    public $resources = [];

    /**
     * Constructs the SubresourceArea and sets the `compiler` property the the default compiler if
     * one is not specified in the passed in properties.
     *
     * @param array $properties
     */
    public function __construct(array $properties) {
        parent::__construct($properties);
        $this->properties['compiler'] = $this->properties['compiler'] ?? self::getDefaultCompiler();
    }

    /**
     * Add a resource either as `addResource($value)` or `addResource($key, $value)`.
     *
     * @return bool true if successful, false otherwise
     */
    public function addResource($a, $b=null): bool {
        if (empty($a)) {
            return false;
        }

        if (isset($b)) {
            $key = $a;
            $value = $b;
        } else {
            $key = null;
            $value = $a;
        }

        if (array_key_exists('generator', $this->properties)) {
            if (isset($key))
                $value = $this->properties['generator']($value, from($this->resources, $key), $this);
            else
                $value = $this->properties['generator']($value, $this);
            if (empty($value))
                return false;
        }

        if (isset($key)) {
            $this->resources[$key] = $value;
        } else {
            $this->resources[] = $value;
        }

        return true;
    }

    /**
     * Get the compiled response of all resources currently in this resource area.
     *
     * @return mixed
     */
    public function get() {
        return $this->properties['compiler']($this->resources, $this);
    }

    /**
     * Merge another SubresourceArea into this area.
     */
    public function merge(AbstractArea $other, bool $appendPrependN = true): void {
        if (!($other instanceof SubresourceArea)) {
            throw new InvalidArgumentException();
        }
        $this->resources = $appendPrependN
            ? array_merge($this->resources, $other->resources)
            : array_merge($other->resources, $this->resources);
    }

    /**
     * Get the default compiler.
     *
     * @return callable
     */
    public static function getDefaultCompiler(): callable {
        return function($res, $inst) {
            return (string) implode('', $res);
        };
    }

}