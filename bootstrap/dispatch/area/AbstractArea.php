<?php

/**
 * Abstract resource area.
 */
abstract class AbstractArea implements ArrayAccess {
    public $properties;

    /**
     * Construct the resource area. All classes that extend this abstract class must call the
     * this parent constructor.
     *
     * @param array $properties requires at least an 'area' property that contains the unique name
     *              of this area
     */
    public function __construct(array $properties = []) {
        $this->properties = $properties;

        if (empty($this->properties['area'])) {
            throw new \BadFunctionCallException('AbstractArea required to be constructed with at '.
                'least a "area" property in the \'$properties\' argument');
        }

        foreach ($this->properties['initial'] ?? [] as $data) {
            call_user_func_array(array($this, 'addResource'), $data);
        }
    }

    /**
     * Add a resource to this area.
     */
    public abstract function addResource($a, $b=null): bool;

    /**
     * Get the combined response of all resources in this area.
     */
    public abstract function get();

    /**
     * @param AbstractArea $other
     * @param bool $appendPrependN
     */
    public abstract function merge(AbstractArea $other, bool $appendPrependN = true): void;

    /**
     * Returns the area name.
     */
    public function __toString() {
        return $this->properties['area'];
    }

    // ArrayAccess implementation
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~

    public function offsetSet($offset, $value) {
        throw new BadMethodCallException('Not supported');
    }

    public function offsetExists($offset) {
        return isset($this->properties[$offset]);
    }

    public function offsetUnset($offset) {
        throw new BadMethodCallException('Not supported');
    }

    public function offsetGet($offset) {
        return isset($this->properties[$offset]) ? $this->properties[$offset] : null;
    }

}