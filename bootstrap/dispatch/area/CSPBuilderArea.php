<?php

/**
 * Content-Security-Policy Builder Area
 */
final class CSPBuilderArea extends AbstractArea {
    private $csp = [
        'font-src'   => SITE_HOST . " 'self'",
        'style-src'  => SITE_HOST . " 'unsafe-inline' 'self'",
        'script-src' => SITE_HOST . " 'unsafe-eval' 'unsafe-inline' 'self'",
        'media-src'  => SITE_HOST . " " . SITE_STATIC . " 'self'",
        'img-src'    => SITE_HOST . " " . SITE_STATIC . " data: 'self'",
    ];
    private $csp_seen = [
        'font-src'   => ["'self'" => true, SITE_HOST => true],
        'style-src'  => ["'self'" => true, "'unsafe-inline'" => true, SITE_HOST => true],
        'script-src' => ["'self'" => true, "'unsafe-inline'" => true, "'unsafe-eval'" => true, SITE_HOST => true],
        'media-src'  => ["'self'" => true, SITE_STATIC => true, SITE_HOST => true],
        'img-src'    => ["'self'" => true, "data:" => true, SITE_STATIC => true, SITE_HOST => true],
    ];

    /**
     * Constructs the CSPBuilderArea with 'CSP' as the area name.
     */
    public function __construct() {
        parent::__construct(['area' => 'CSP']);
    }

    /**
     * Add a CSP directive value.
     *
     * @param string    $type the CSP directive name
     * @param string    $value the value of the CSP directive, if the directive already has a value
     *                  then this $value will be prepended
     * @return bool     true if successful, false otherwise
     */
    public function addResource($type, $value=null): bool {
        if (!isset($value))
            throw new InvalidArgumentException();
        if (!array_key_exists($type, $this->csp)) {
            return false;
        } else if (isset($this->csp_seen[$type][$value])) {
            return true; // prevent duplicates
        } else {
            $this->csp_seen[$type][$value] = true;
        }
        $this->csp[$type] = $value . ' ' . $this->csp[$type];
        return true;
    }

    /**
     * Get the full CSP header value response.
     *
     * @return string
     */
    public function get() {
        // Default Directive
        // ~~~~~~~~~~~~~~~~~
        $out = "default-src 'self'";

        // Non-Customizable Directives
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~
        $out .= "; child-src 'self'";
        $out .= "; connect-src ". SITE_HOST ." 'self'";
        $out .= "; form-action 'self'";
        $out .= "; frame-ancestors 'self'";
        $out .= "; frame-src 'self'";
        $out .= "; manifest-src 'self'";
        $out .= "; object-src 'self'";
        $out .= "; worker-src 'self'";

        // Reporting/Other Directives
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~
        $out .= "; require-sri-for script"; // no "style" b/c google fonts has dynamic responses
        $out .= "; report-uri " . App::SYSROUTE_REPORT_VIOLATION_CSP;
        $out .= "; block-all-mixed-content";

        // Customizable Directives
        // ~~~~~~~~~~~~~~~~~~~~~~~
        foreach ($this->csp as $type => $value) {
            $out .= "; " . $type . " " . $value;
        }

        return $out;
    }

    /**
     * Merge another CSPBuilderArea into this area.
     */
    public function merge(AbstractArea $other, bool $appendPrependN = true): void {
        if (!($other instanceof CSPBuilderArea)) {
            throw new InvalidArgumentException();
        }
        foreach ($other->csp_seen as $type => $values) {
            foreach (array_keys($values) as $value) {
                $this->addResource($type, $value);
            }
        }
    }

}