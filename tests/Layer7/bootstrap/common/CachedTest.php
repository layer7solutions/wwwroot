<?php

final class CachedTest extends BaseTest {

    /**
     * Use a key prefix to avoid conflict with any main application keys
     * @var string
     */
    const TEST_KEY_PREFIX = 'PHPUnit_CachedTest_';

    /**
     * @var int
     */
    const TEST_SLEEP_TIME = 3;

    public function testCached() {
        $this->assertTrue(cached_available());

        $value1 = time();
        $value2 = 50;

        // ONE KEY
        // ~~~~~~~

        // set
        $this->assertTrue(cached(self::TEST_KEY_PREFIX . 'key1', $value1));

        // get
        $this->assertSame($value1, cached(self::TEST_KEY_PREFIX . 'key1'));

        // set again
        $this->assertTrue(cached(self::TEST_KEY_PREFIX . 'key1', $value2));

        // get again
        $this->assertSame($value2, cached(self::TEST_KEY_PREFIX . 'key1'));

        // delete
        $this->assertTrue(cached_delete(self::TEST_KEY_PREFIX . 'key1'));

        // check if deleted
        $this->assertFalse(cached(self::TEST_KEY_PREFIX . 'key1'));
    }

    /**
     * Samples for testInvalidParamsToCached()
     */
    public function advancedSamples() {
        $sample = [];

        $num_samples = 10;

        for ($i = 0; $i < $num_samples; $i++) {
            $keys = [];
            $KV_pair = [];

            for ($j = 0, $num_keys = rand(5,20); $j < $num_keys; $j++) {
                $k = self::TEST_KEY_PREFIX . safe_token(4);

                $keys[] = $k;
                $KV_pair[$k] = safe_token(8);
            }

            $sample[] = [$keys, $KV_pair, $i === ($num_samples-1)];
        }

        return $sample;
    }

    /**
     * @dataProvider advancedSamples
     */
    public function testAdvancedCached(array $keys, array $KV_pair, bool $test_expiry=false) {
        $this->assertEmpty(cached($keys));

        // store
        $this->assertTrue(cached($KV_pair));

        // get all at once and check if same
        $x = cached($keys);
        $this->assertcount(count($keys), $x);
        $this->assertSame($KV_pair, $x);

        // get each individually and check if same
        foreach ($KV_pair as $k => $v) {
            $this->assertSame($v, cached($k));
        }

        // test deleting all at once
        $this->assertTrue(cached_delete($keys));
        $this->assertEmpty(cached($keys));

        // store again
        $this->assertTrue(cached($KV_pair));
        $this->assertSame($KV_pair, $x);

        // test deleting each individually
        foreach ($KV_pair as $k => $v) {
            $this->assertSame($v, cached($k));
            $this->assertTrue(cached_delete($k));
        }
        foreach ($keys as $k) {
            $this->assertFalse(cached($k)); // check if gone
        }
        $this->assertEmpty(cached($keys));

        if ($test_expiry) {
            $this->assertEmpty(cached($keys));
            $this->assertTrue(cached($KV_pair, self::TEST_SLEEP_TIME)); // store with expiry
            $this->assertSame($KV_pair, cached($keys)); // check if still there
            sleep(self::TEST_SLEEP_TIME); // sleep for expiry time
            $this->assertEmpty(cached($keys)); // check if gone

            foreach ($KV_pair as $k => $v) {
                // store each individually with expiry
                $this->assertTrue(cached($k, $v, self::TEST_SLEEP_TIME));
            }

            sleep(self::TEST_SLEEP_TIME); // sleep for expiry time

            foreach ($keys as $k) {
                $this->assertFalse(cached($k)); // check if gone
            }
            $this->assertEmpty(cached($keys));
        }
    }

    /**
     * Samples for testInvalidParamsToCached()
     */
    public function invalidParamsSamples(): array {
        return [
            // test key validation
            [true],
            [false],
            [null],
            [0],
            [5],
            [-5],
            [[7]],
            [[7 => true]],
            [[self::TEST_KEY_PREFIX.'k1', 7]],
            [[self::TEST_KEY_PREFIX.'k1' => 20, 7 => true]],
            [[7, self::TEST_KEY_PREFIX.'k1']],
            [[7 => true, self::TEST_KEY_PREFIX.'k1' => 20]],

            [''],
            [['']],
            [['' => 'v1']],
            [''],
            [[self::TEST_KEY_PREFIX.'k1', '']],
            [[self::TEST_KEY_PREFIX.'k1' => 'v1', '' => 'v2']],
            [['', self::TEST_KEY_PREFIX.'k1']],
            [['' => 'v2', self::TEST_KEY_PREFIX.'k1' => 'v1']],
            [str_repeat('a', 250)], // 250 -> max key length

            // test bad combos and bad expiry
            [[self::TEST_KEY_PREFIX.'k1', 'v1'], 'v'],
            [[self::TEST_KEY_PREFIX.'k1' => 'v1'], 'v'],
            [self::TEST_KEY_PREFIX.'k1', 'v1', -2],
            [[self::TEST_KEY_PREFIX.'k1', self::TEST_KEY_PREFIX.'k2'], 'v', 60],
            [[self::TEST_KEY_PREFIX.'k1' => 'v1'], 'v', 60],
            [[self::TEST_KEY_PREFIX.'k1', self::TEST_KEY_PREFIX.'k2'], 'v', -2],
            [[self::TEST_KEY_PREFIX.'k1' => 'v1'], 'v', -2],
        ];
    }

    /**
     * @dataProvider invalidParamsSamples
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidParamsToCached(): void {
        cached(... func_get_args());
    }

    /**
     * @expectedException \BadFunctionCallException
     */
    public function testTooManyParamsToCached(): void {
        cached('testTooManyParamsToCached', true, 60, '4thParamShouldntBeHere');
    }

    public function testExists(): void {
        $KV_pair = [
            self::TEST_KEY_PREFIX . 'k1' => 'a1',
            self::TEST_KEY_PREFIX . 'k2' => 'b2',
            self::TEST_KEY_PREFIX . 'k3' => 'c3',
            self::TEST_KEY_PREFIX . 'k4' => 'd4',
            self::TEST_KEY_PREFIX . 'k5' => 'e5',
        ];
        $keys = array_keys($KV_pair);
        $sample = array_keep_keys($KV_pair, [
            self::TEST_KEY_PREFIX . 'k1',
            self::TEST_KEY_PREFIX . 'k3',
            self::TEST_KEY_PREFIX . 'k4'
        ]);
        $verify = [
            self::TEST_KEY_PREFIX . 'k1' => true,
            self::TEST_KEY_PREFIX . 'k2' => false,
            self::TEST_KEY_PREFIX . 'k3' => true,
            self::TEST_KEY_PREFIX . 'k4' => true,
            self::TEST_KEY_PREFIX . 'k5' => false,
        ];

        cached_delete($keys);
        $this->assertEmpty(cached($keys));

        cached($sample);
        $this->assertSame($verify, cached_exists($keys));

        $tmp = [];
        for ($i = 0, $len = count($keys); $i < $len; $i++) {
            $res = cached_exists($keys[$i]);
            $this->assertSame($verify[$keys[$i]], $res);
            $tmp[$keys[$i]] = $res;
        }

        $this->assertSame($verify, $tmp);
        cached_delete($keys);
        $this->assertEmpty(cached($keys));

        $res = cached_exists($keys);
        for ($i = 0, $len = count($keys); $i < $len; $i++) {
            $this->assertFalse(cached_exists($keys[$i]));
            $this->assertFalse($res[$keys[$i]]);
        }
    }

}