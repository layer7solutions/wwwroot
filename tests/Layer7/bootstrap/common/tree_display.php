<?php
header('Content-Type: text/html');
echo '<html><body><pre>';

$data = [
    'AAA' => [
        'B' => [
            'C' => [
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
            ],
            'J',
            'KAA' => [
                'L' => [
                    'M',
                    'N',
                    'O',
                    'P',
                ],
                'Q' => [
                    'SAA' => [
                        'T',
                        'U',
                        'V',
                        'W',
                        'X',
                        'Y',
                        'Z',
                        'AA'
                    ],
                    'AB',
                ],
                'AC',
                'AD',
                'AE',
                'AF' => [
                    'AG',
                    'AH',
                    'AI',
                    'AJ',
                    'AK',
                    'AL',
                    'AM',
                    'AN',
                    'AO',
                    'AP'
                ]
            ]
        ],
        'AQ' => [
            'AR' => [
                'AS' => [
                    'AT',
                    'AU',
                    'AV',
                    'AW',
                ],
                'AX',
                'AY',
                'AZ',
                'BA',
                'BB',
                'BC' => [
                    'BD',
                    'BE'
                ],
                'BF' => [
                    'BG'
                ],
                'BH'
            ]
        ]
    ]
];
echo tree_display($data);

$sum = 0;
for ($i = 0; $i < 100; $i++) {
    $start = microtime(true) * 1000.0;
    tree_display($data);
    $time = (microtime(true) * 1000.0) - $start;
    echo $time . "\n";
    $sum += $time;
}
$avg = $sum / 100;
echo "Average: $avg\n";