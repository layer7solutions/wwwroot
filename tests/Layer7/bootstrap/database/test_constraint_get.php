<?php

include 'db.php';
include '../common/functions.php';
include '../../config.php';

$db = \DB::connect([
    'host' => PGSQL_SERVER,
    'user' => PGSQL_USERNAME,
    'password' => PGSQL_PASSWORD,
    'dbname' => 'ModApplications'
]);

echo '<html><body>';

echo array_str($db->getDriver()->pg_create_constraint('develop_test')) . '<br/>';
echo array_str($db->getDriver()->pg_get_primary('develop_test'));

echo '</body></html>';