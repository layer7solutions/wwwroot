<?php

echo 'Running column list test...<br/>';

$r0 = $db->columnList('develop_test2');
assert($r0[0] == 'item_id');
assert($r0[1] == 'text_a');
assert($r0[2] == 'text_b');
assert($r0[3] == 'text_c');

$r1 = $db->columnList('develop_test2', true);
assert($r1['item_id'] == 'integer');
assert($r1['text_a'] == 'text');
assert($r1['text_b'] == 'text');
assert($r1['text_c'] == 'text');