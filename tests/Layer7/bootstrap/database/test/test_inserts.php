<?php

echo 'Running insert/update/replace/delete tests...<br/>';

// Test standard insert
$db->insert('develop_test2', array('text_c' => 'insert_test'));
$i0 = $db->insertId();
assert($db->queryFirstField('SELECT text_c FROM develop_test2 WHERE item_id=%i', $i0) == 'insert_test');

// Test multiple row insertion
$db->insert('develop_test2', array(
    array(
        'text_b' => 'ayy',
        'text_c' => 'insert_test',
    ),
    array(
        'text_b' => 'lmao',
        'text_c' => 'insert_test',
    )
));
assert(!empty($db->queryFirstRow('SELECT * FROM develop_test2 WHERE text_b=%s', 'ayy')));
assert($db->count() == 1);
assert(!empty($db->queryFirstRow('SELECT * FROM develop_test2 WHERE text_b=%s', 'lmao')));
assert($db->count() == 1);

// Test insertIgnore
$db->insertIgnore('develop_test2', array('item_id' => 1)); // no assert needed - if doesn't error: success

// Test insertUpdate
function test0($id) {
    global $db;
    $db->insertUpdate('develop_test2', array(
        'item_id' => $id,
        'text_b' => 'Hello, World!',
        'text_c' => 'insert_test',
    ), 'text_b=%s', 'Ayy Lmao');
}
test0($i0);
assert($db->queryFirstField('SELECT text_b FROM develop_test2 WHERE item_id=%i', $i0) == 'Ayy Lmao');
$item_max = $db->queryFirstField('SELECT max(item_id) FROM develop_test2');
test0($item_max + 1);
assert($db->queryFirstField('SELECT text_b FROM develop_test2 WHERE item_id=%i', $item_max + 1) == 'Hello, World!');
$db->insertUpdate('develop_test2', array(
    'item_id' => $i0,
    'text_b' => 'stuff',
    'text_c' => 'insert_test',
));
assert($db->queryFirstField('SELECT text_b FROM develop_test2 WHERE item_id=%i', $i0) == 'stuff');

// Test replace
$db->replace('develop_test2', array(
    'item_id' => $i0,
    'text_b' => 'asdfghjkl'
));
assert($db->queryFirstField('SELECT text_b FROM develop_test2 WHERE item_id=%i', $i0) == 'asdfghjkl');

// Test update
$db->update('develop_test2', array(
    'text_a' => 'asdf',
    'text_b' => 'ghjkl',
    'text_c' => 'insert_test',
), "item_id=%i", $i0);
assert($db->queryFirstField('SELECT text_a FROM develop_test2 WHERE item_id=%i', $i0) == 'asdf');
assert($db->queryFirstField('SELECT text_b FROM develop_test2 WHERE item_id=%i', $i0) == 'ghjkl');

// Test delete
assert(!empty($db->query('SELECT * FROM develop_test2 WHERE text_c=%s', 'insert_test')));
$db->delete('develop_test2', "text_c=%s", 'insert_test');
assert(empty($db->query('SELECT * FROM develop_test2 WHERE text_c=%s', 'insert_test')));
assert(empty($db->query('SELECT * FROM develop_test2 WHERE item_id=%i', $i0)));