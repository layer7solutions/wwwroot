<?php

echo 'Running autoincrement test...<br/>';

$item_max = $db->queryFirstField('SELECT max(item_id) FROM develop_test2');

$db->insert('develop_test2', array('item_id' => $item_max + 1, 'text_a' => 'id explicit insert'));
$db->insert('develop_test2', array('text_a' => 'normally auto-incrementing insert'));
assert($db->insertId() == $item_max + 2);

// cleanup
$db->delete('develop_test2', 'item_id=%i', $item_max + 1);
$db->delete('develop_test2', 'item_id=%i', $item_max + 2);