<?php

include '../db.php';
include '../../common/functions.php';
include '../../../config.php';

$db = \DB::connect([
    'host' => PGSQL_SERVER,
    'user' => PGSQL_USERNAME,
    'password' => PGSQL_PASSWORD,
    'dbname' => 'ModApplications'
]);

$GLOBALS['success'] = true;

assert_options(ASSERT_ACTIVE, 1);
assert_options(ASSERT_WARNING, 0);
assert_options(ASSERT_QUIET_EVAL, 1);
assert_options(ASSERT_CALLBACK,
    function($file, $line, $code) {
        echo "<hr>Assertion Failed:
            File '$file'<br />
            Line '$line'<br />
            Code '$code'<br /><hr />";
        $GLOBALS['success'] = false;
    }
);

echo '<!doctype html><html><body>';