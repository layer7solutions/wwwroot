<?php

echo 'Running transactions test...<br/>';

function test_transaction($where, $val) {
    global $db;

    $depth = $db->startTransaction();
    assert($depth == 1);

    $db->query("UPDATE develop_test2 SET text_c=%s WHERE text_a=%s AND text_b=%s", $val, 'transaction_test_row', $where);

    $counter = $db->affectedRows();
    if ($counter >= 3) {
        $depth = $db->commit();
        assert($depth == 0);
    } else {
        $depth = $db->rollback();
        assert($depth == 0);
    }
}

function test_nested_transaction($where, $val) {
    global $db;

    $depth = $db->startTransaction();
    assert($depth == 1);

    $db->query("UPDATE develop_test2 SET text_c=%s WHERE text_a=%s AND text_b=%s", $val, 'transaction_test_row', $where);
    $counter = $db->affectedRows();

    $depth = $db->startTransaction();
    assert($depth == 2);

    if ($counter == 1) {
        $db->query("UPDATE develop_test2 SET text_c=%s WHERE text_a=%s", 'qwertyuiop', $where);
    } else {
        $depth = $db->rollback();
        assert($depth == 1);
    }

    $depth = $db->commit(true);
    assert($depth == 0);
}

$rand_string = substr(str_shuffle(MD5(microtime())), 0, 10);

test_transaction('a', $rand_string);
test_transaction('b', $rand_string);

$db->nested_transactions = true;
test_nested_transaction('a', $rand_string);
test_nested_transaction('b', $rand_string);
$db->nested_transactions = false;

// cleanup
$db->update('develop_test2', array(
    'text_c' => null,
), "text_a=%s", 'transaction_test_row');