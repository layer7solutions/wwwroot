<?php

include 'test_header.php';

include 'test_columnlist.php';
include 'test_selection_queries.php';
include 'test_inserts.php';
include 'test_autoincrement.php';
include 'test_transactions.php';
include 'test_injection.php';

include 'test_footer.php';