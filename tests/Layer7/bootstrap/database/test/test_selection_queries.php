<?php

echo 'Running selection query tests...<br/>';

// test basic query
$r0 = $db->query('SELECT * FROM develop_test2 WHERE text_a=%s_a', ['a' => 'query_test_item'] );
$r1 = $db->query('SELECT * FROM develop_test2 WHERE text_a=%s_a AND item_id=%i_item_id',
    ['a' => 'query_test_item', 'item_id' => 8] );
$r2 = $db->query('SELECT * FROM develop_test2 WHERE text_a=%s', 'query_test_item');
$r3 = $db->query('SELECT * FROM develop_test2 WHERE item_id=%i', 8);
$r4 = $db->query('SELECT * FROM develop_test2 WHERE item_id=%?', 8);
assert($r0 == $r1);
assert($r0 == $r2);
assert($r0 == $r3);
assert($r0 == $r4);
assert(!empty($r0[0]));
assert($r0[0]['text_b'] == 'lorem');
assert($r0[0]['text_c'] == 'ipsum');

// check expected return value of queries of no result
$r0 = $db->query('SELECT * FROM develop_test2 WHERE text_a=%s', 'nothing that exists');
$r1 = $db->queryFirstRow('SELECT * FROM develop_test2 WHERE text_a=%s', 'nothing that exists');
assert(is_array($r0) && empty($r0));
assert($r1 === null);

// test queryFirstRow
$r0 = $db->queryFirstRow('SELECT * FROM develop_test2 WHERE text_a=%s_a', ['a' => 'query_test_item'] );
$r1 = $db->queryFirstRow('SELECT * FROM develop_test2 WHERE text_a=%s_a AND item_id=%i_item_id', ['a' => 'query_test_item', 'item_id' => 8] );
$r2 = $db->queryFirstRow('SELECT * FROM develop_test2 WHERE text_a=%s', 'query_test_item');
$r3 = $db->queryFirstRow('SELECT * FROM develop_test2 WHERE item_id=%i', 8);
$r4 = $db->queryFirstRow('SELECT * FROM develop_test2 WHERE item_id=%?', 8);
assert($r0 == $r1);
assert($r0 == $r2);
assert($r0 == $r3);
assert($r0 == $r4);
assert($r0['text_b'] == 'lorem');
assert($r0['text_c'] == 'ipsum');

// test queryFirstList
$r0 = $db->queryFirstList('SELECT text_b, text_c FROM develop_test2 WHERE item_id=%?', 8);
assert($r0[0] == 'lorem');
assert($r0[1] == 'ipsum');

// test queryFirstColumn
$r0 = $db->queryFirstColumn('SELECT DISTINCT text_b FROM develop_test2');
assert(in_array('eb59f98569', $r0));
assert(in_array(null, $r0));
assert(in_array('lorem', $r0));

// test queryOneColumn
$r0 = $db->queryOneColumn('text_b', 'SELECT * FROM develop_test2');
assert(in_array('eb59f98569', $r0));
assert(in_array(null, $r0));
assert(in_array('lorem', $r0));
assert(in_array("1' or 'text_c' = 'bad", $r0));

// test queryFirstField
$r0 = $db->queryFirstField('SELECT * FROM develop_test2 WHERE item_id=%i', 8);
$r1 = $db->queryFirstField('SELECT text_b FROM develop_test2 WHERE item_id=%i', 8);
assert($r0 == 8);
assert($r1 == 'lorem');

// test queryOneField
$r0 = $db->queryOneField('text_b', 'SELECT * FROM develop_test2 WHERE item_id=%i', 8);
assert($r0 == 'lorem');