<?php

echo 'Running SQL injection vulnerability test...<br/>';

$text_a_row6 = 'can you remove me?';
$text_c_row7 = 'can you change me?';

// ensure proper escaping
$r0 = "1' or 'text_c' = 'bad";
$db->query('UPDATE develop_test2 SET text_b=%s WHERE item_id=%i', $r0, 7);
assert($db->queryOneField('text_c', "SELECT * FROM develop_test2 WHERE item_id=%i", 7) == $text_c_row7);

// ensure values that aren't supposed to be literals aren't treated as such
$time_start = time();
$db->query('SELECT * FROM develop_test2 WHERE item_id=%? AND text_c=%?', 7, 'sleep(60)');
$time_duration = time() - $time_start;
assert($time_duration < 5); // although try sleep for 60s, shouldn't be that long so check 5s for failure

// ensure stacked queries don't succeed
$db->query('SELECT * FROM develop_test2 WHERE item_id=%? AND text_c=%?', 7,
    "whatever'; DELETE FROM develop_test2 WHERE item_id=6; --");
assert(!empty($db->query('SELECT * FROM develop_test2 WHERE item_id=%i', 6)));