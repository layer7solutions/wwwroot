<?php

include 'db.php';
include '../common/functions.php';
include '../../config.php';

$db = \DB::connect([
    'host' => PGSQL_SERVER,
    'user' => PGSQL_USERNAME,
    'password' => PGSQL_PASSWORD,
    'dbname' => 'Zion'
]);
$db2 = \DB::connect([
    'host' => PGSQL_SERVER,
    'user' => PGSQL_USERNAME,
    'password' => PGSQL_PASSWORD,
    'dbname' => 'SweeperBot'
]);

echo '<html><body>';

echo '<b>modlog</b></br>';
echo array_str($db->getDriver()->pg_create_constraint('modlog')) . '<br/>';
echo array_str($db->getDriver()->pg_get_primary('modlog')) . '<br/></br>';

echo '<b>reddit_thing</b></br>';
echo array_str($db->getDriver()->pg_create_constraint('reddit_thing')) . '<br/>';
echo array_str($db->getDriver()->pg_get_primary('reddit_thing')) . '<br/></br>';

echo '<b>subreddit</b></br>';
echo array_str($db->getDriver()->pg_create_constraint('subreddit')) . '<br/>';
echo array_str($db->getDriver()->pg_get_primary('subreddit')) . '<br/></br>';

echo '<b>modmail</b></br>';
echo array_str($db->getDriver()->pg_create_constraint('modmail')) . '<br/>';
echo array_str($db->getDriver()->pg_get_primary('modmail')) . '<br/></br>';

echo '<b>ClanRecruitmentApps</b></br>';
echo array_str($db2->getDriver()->pg_create_constraint('ClanRecruitmentApps')) . '<br/>';
echo array_str($db2->getDriver()->pg_get_primary('ClanRecruitmentApps')) . '<br/></br>';

echo '<b>ClanRecruitmentBlacklist</b></br>';
echo array_str($db2->getDriver()->pg_create_constraint('ClanRecruitmentBlacklist')) . '<br/>';
echo array_str($db2->getDriver()->pg_get_primary('ClanRecruitmentBlacklist')) . '<br/></br>';

echo '<b>message_data</b></br>';
echo array_str($db2->getDriver()->pg_create_constraint('message_data')) . '<br/>';
echo array_str($db2->getDriver()->pg_get_primary('message_data')) . '<br/></br>';

echo '</body></html>';