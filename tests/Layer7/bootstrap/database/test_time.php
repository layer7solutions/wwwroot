<?php

include 'db.php';
include '../common/functions.php';
include '../../config.php';

$db = \DB::connect([
    'host' => PGSQL_SERVER,
    'user' => PGSQL_USERNAME,
    'password' => PGSQL_PASSWORD,
    'dbname' => 'ModApplications'
]);

echo '<html><body>';

function test_insert() {
    global $db;

    $db->insert('develop_test', array(
        'item_name' => 'time test',
        'col_i' => $db->sqleval("now()"),
        'col_j' => $db->sqleval("now()"),
        'col_k' => $db->sqleval("now()"),
        'col_l' => $db->sqleval("now()"),
        'col_m' => '3 minutes',
    ));
}

function test_update($id, $j, $m) {
    global $db;

    $db->update('develop_test', array(
        'col_i' => time(),
        'col_j' => $j,
        'col_k' => time(),
        'col_l' => time(),
        'col_m' => $m
    ), "item_id=%i", $id);
}

function display_results() {
    global $db;

    print_div('QueryFirstRow: '.array_str(
        $db->queryFirstRow('SELECT item_id,item_name,col_i,col_j,col_k,col_l,col_m FROM develop_test WHERE item_id=%i', 19)
    ));
}

display_results();

echo '<br/>';

test_insert();
test_update(19, 1450, 240);

echo '<br/>';

display_results();


echo '</body></html>';