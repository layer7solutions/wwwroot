<?php

$databases = ['application', 'Zion', 'TheTraveler', 'oauth2', 'DestinyReddit'];

foreach ($databases as $dbname) {
    $enc1 = db($dbname)->queryFirstField('SHOW SERVER_ENCODING');
    $enc2 = db($dbname)->queryFirstField('SHOW CLIENT_ENCODING');
    echo "$dbname Encoding: Server: $enc1, Client: $enc2;\n";
    assert($enc1 === $enc2, "Server encoding ($enc1) does not match client encoding ($enc2)");
}