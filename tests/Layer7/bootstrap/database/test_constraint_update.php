<?php

include 'db.php';
include '../common/functions.php';
include '../../config.php';

$db = \DB::connect([
    'host' => PGSQL_SERVER,
    'user' => PGSQL_USERNAME,
    'password' => PGSQL_PASSWORD,
    'dbname' => 'ModApplications'
]);

echo '<html><body>';


function test_insert($id = 0, $col_x = null) {
    global $db;

    $array = array(
        'item_id' => $id,
        'item_name' => 'lorem ipsum'
    );

    if (isset($col_x)) {
        $array['col_x'] = $col_x;
    }

    $db->insert('develop_test', $array);
}
function test_insertIgnore($id = 0, $col_x = null) {
    global $db;

    $array = array(
        'item_id' => $id,
        'item_name' => 'lorem ipsum'
    );

    if (isset($col_x)) {
        $array['col_x'] = $col_x;
    }

    $db->insertIgnore('develop_test', $array);
}
function test_replace($id, $col_x = null) {
    global $db;

    $array = array(
        'item_id' => $id,
        'item_name' => 'asdfghjkl'
    );

    if (isset($col_x)) {
        $array['col_x'] = $col_x;
    }

    $db->replace('develop_test', $array);
}

test_replace(20, 'blah');

echo '</body></html>';