<?php

use app\lib\data\FieldSelectionParser;

final class FieldSelectionParserTest extends BaseTest {

    private $expected_model = [
        'amount' => [
            '__self__' => true,
            '__path__' => "amount"
        ],
        'listing' => [
            '__self__' => true,
            '__path__' => "listing"
        ],
        'stuff' => [
            '__self__' => false,
            '__path__' => "stuff",
            'foo' => [
                '__self__' => true,
                '__path__' => "stuff.foo"
            ],
            'bar' => [
                '__self__' => false,
                '__path__' => "stuff.bar",
                'things' => [
                    '__self__' => true,
                    '__path__' => "stuff.bar.things",
                    'morestuff' => [
                        '__self__' => true,
                        '__path__' => "stuff.bar.things.morestuff"
                    ]
                ],
                'items' => [
                    '__self__' => true,
                    '__path__' => "stuff.bar.items",
                    'morestuff' => [
                        '__self__' => true,
                        '__path__' => "stuff.bar.items.morestuff"
                    ],
                    'other' => [
                        '__self__' => false,
                        '__path__' => "stuff.bar.items.other",
                        'foo' => [
                            '__self__' => true,
                            '__path__' => "stuff.bar.items.other.foo"
                        ],
                        'bar' => [
                            '__self__' => false,
                            '__path__' => "stuff.bar.items.other.bar",
                            'barstuff1' => [
                                '__self__' => true,
                                '__path__' => "stuff.bar.items.other.bar.barstuff1"],
                            'barstuff2' => [
                                '__self__' => true,
                                '__path__' => "stuff.bar.items.other.bar.barstuff2"
                            ]
                        ],
                        'foobar' => [
                            '__self__' => true,
                            '__path__' => "stuff.bar.items.other.foobar"
                        ]
                    ]
                ]
            ]
        ]
    ];

    /**
     * @throws \app\lib\data\InvalidQueryException
     */
    public function testParser(): void {
        $parser = new FieldSelectionParser();
        $query = 'amount,listing,stuff.foo,stuff.bar.{things,items,items.morestuff,items.other.{foo,bar{barstuff1,barstuff2},foobar},things.morestuff}';

        $model = $parser->parse($query);

        $this->assertEquals($this->expected_model, $model);

        $this->assertTrue((bool) $parser->check_field($model, 'amount'));
        $this->assertTrue((bool) $parser->check_field($model, 'listing'));
        $this->assertFalse((bool) $parser->check_field($model, 'stuff'));
        $this->assertTrue((bool) $parser->check_field($model, 'stuff.foo'));
        $this->assertFalse((bool) $parser->check_field($model, 'stuff.bar'));
        $this->assertTrue((bool) $parser->check_field($model, 'stuff.bar.things'));
        $this->assertTrue((bool) $parser->check_field($model, 'stuff.bar.things.morestuff'));
        $this->assertTrue((bool) $parser->check_field($model, 'stuff.bar.items.morestuff'));
        $this->assertFalse((bool) $parser->check_field($model, 'stuff.bar.items.other'));
        $this->assertTrue((bool) $parser->check_field($model, 'stuff.bar.items.other.foo'));
        $this->assertFalse((bool) $parser->check_field($model, 'stuff.bar.items.other.bar'));
        $this->assertTrue((bool) $parser->check_field($model, 'stuff.bar.items.other.bar.barstuff1'));
        $this->assertTrue((bool) $parser->check_field($model, 'stuff.bar.items.other.bar.barstuff2'));
        $this->assertTrue((bool) $parser->check_field($model, 'stuff.bar.items.other.foobar'));
        $this->assertTrue((bool) $parser->check_field($model, 'listing.x'));
        $this->assertTrue((bool) $parser->check_field($model, 'stuff.bar.things.morestuff.x'));
        $this->assertTrue((bool) $parser->check_field($model, 'stuff.bar.items.other.bar.barstuff1.x'));
        $this->assertFalse((bool) $parser->check_field($model, 'stuff.bar.x.things.morestuff'));
        $this->assertFalse((bool) $parser->check_field($model, 'stuff.bar.items.other.x'));
        $this->assertFalse((bool) $parser->check_field($model, 'x'));
        $this->assertFalse((bool) $parser->check_field($model, 'x.y'));
        $this->assertFalse((bool) $parser->check_field($model, 'x.y.z'));
    }

}
