<?php
use app\lib\RT;

// t3_6r98ve - text post
// t3_6u0lcb - link post to youtube video
// t3_6u0les - text post containing youtube video
// t1_dlozlj5 - comment containing 2 youtube videos

// This file tests RT for compliance with the specs
// and for media channel related properties.

function check_thing($thing_id) {
    var_dump(RT::get_data($thing_id));
    var_dump(RT::get_media_info($thing_id));
}

echo "\n\n### text post:\n";
check_thing('t3_6r98ve');

echo "\n\n### link post:\n";
check_thing('t3_6u0lcb');

echo "\n\n### text post containing link:\n";
check_thing('t3_6u0les');

echo "\n\n### comment containing 2 youtube videos:\n";
check_thing('t1_dlozlj5');