<?php

final class SubredditInternalOrderTest extends BaseTest {

    public function samples() {
        $pool = ['aww', 'LifeProTips', 'videos', 'DestinyTheGame', 'Layer7', 'YT_Killer'];

        $powerset = powerSet($pool);

        $samples = [];

        foreach ($powerset as $set) {
            $perms = pc_permute($set);

            foreach ($perms as $perm) {
                $samples[] = array($perm);
            }
        }

        return $samples;
    }

    /**
     * @dataProvider samples
     */
    public function testPoolOrder(array $sample) {
        $provider1 = subreddit_provider_controlled($sample);
        $provider2 = subreddit_provider_global();
        $provider3 = subreddit_provider_explicit();

        $this->assertSame($sample, $provider1->get());

        $this->assertSame($sample, subreddit($sample, $provider1)->to_array());
        $this->assertSame($sample, subreddit($sample, $provider2)->to_array());
        $this->assertSame($sample, subreddit($sample, $provider3)->to_array());
    }

}