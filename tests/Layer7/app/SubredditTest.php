<?php

use app\lib\Subreddit;

use app\lib\IllegalSubredditException;
use app\lib\InvalidSubredditException;
use app\lib\UnknownSubredditException;
use app\lib\providers\SubredditSourceProvider;

/**
 * Test subreddit construction and providers. Note that if `$provider` is `null` then the default,
 * AdministrativeSubredditProvider, will be used.
 *
 * ---
 *
 * NB! Doing `all-subbie1+subbie2+subbie3` is different from `all-subbie1,subbie2,subbie3`
 * Using "plus" is concatenation and "comma" is forming list.
 *
 * So `all-subbie1,subbie2,subbie3` is equivalent to `array_diff($all, [$subbie1,$subbie2,$subbie3])`
 * which is valid.
 *
 * Whereas `all-subbie1+subbie2+subbie3` is equivalent to
 * ```php
 * $tmp = array_diff($all,$subbie1);
 * array_push($tmp, $subbie2, $subbie3)
 * ```
 * which is not considered valid by our format.
 *
 * ---
 *
 * NB! The ExplicitSubredditProvider takes no effort to validate, normalize, or sanitize the input
 * parameters. Which is why it is called "Explicit". So this provider is not used for user input.
 * The purpose of this provider is for internal usage, i.e. copying Subreddit objects or created
 * subsets of Subreddit objects. Since the original Subreddit object has been validated and
 * normalized, we know that any subset of it is also validated and normalized.
 *
 * ---
 *
 * Note that the `$expected` parameter passed to checkSubredditEquality() is case-sensitive.
 *
 * Also note that after completion of the constructor, the internal array of the Subreddit must be
 * in the same order as the subreddits passed into the constructor (with duplicates removed). So
 * `checkSubredditEquality()` will test that the subreddit's internal array is in the exact same
 * order as the `$expected` parameter.
 */
final class SubredditTest extends BaseTest {

    // TEST CONSTRUCTION
    // ------------------------------------------------------------------------------------------

    /**
     * Samples for testConstruction()
     */
    public function samples(): array {
        return [
            ['aww', ['aww'], null],

            // test string format
            ['aww+videos+DestinyTheGame', ['aww','videos','DestinyTheGame'], null],
            ['aww,DestinyTheGame,videos', ['aww','DestinyTheGame','videos'], null],
            ['aww+DestinyTheGame+videos', ['aww','DestinyTheGame','videos'], null],
            ['aww,DestinyTheGame+videos', ['aww','DestinyTheGame','videos'], null],

            // test array format
            [['aww','videos'], ['aww','videos'], null],
            [['videos','aww'], ['videos', 'aww'], null],
            [['aww','videos','DestinyTheGame'], ['aww','videos','DestinyTheGame'], null],

            // test different case in string format
            ['AWw+vIdeOs+destinythegame', ['aww','videos','DestinyTheGame'], null],
            ['aWw,dESTinyTHEgaMe,viDeoS', ['aww','DestinyTheGame','videos'], null],
            ['awW+DESTINYTHEGAME+Videos', ['aww','DestinyTheGame','videos'], null],
            ['aWW,Destinythegame+VIDEOS', ['aww','DestinyTheGame','videos'], null],

            // test different case in array format
            [['AWw','vIdeOs'], ['aww','videos'], null],
            [['VIdEos','awW'], ['videos', 'aww'], null],
            [['aWw','VIDEOS','destInyThEgAme'], ['aww','videos','DestinyTheGame'], null],

            // test duplicates, with same and different casses
            ['aww,DestinyTheGame+videos+DestinyTheGame', ['aww','DestinyTheGame','videos'], null],
            ['aww,DestinyTheGame+videos+DESTINYTHEGAME', ['aww','DestinyTheGame','videos'], null],
            [['aww','DestinyTheGame','videos','DestinyTheGame'], ['aww','DestinyTheGame','videos'], null],
            [['aww','DestinyTheGame','videos','DESTINYTHEGAME'], ['aww','DestinyTheGame','videos'], null],
        ];
    }

    /**
     * Testing normal construction.
     *
     * @dataProvider samples
     */
    public function testConstruction($sample, array $expected, SubredditSourceProvider $provider=null): void {
        $this->checkSubredditEquality($expected, subreddit($sample, $provider));
    }

    /**
     * Samples for testProviders()
     */
    public function providerSamples(): array {
        $g = subreddit_provider_global(); // Global provider (G)
        $e = subreddit_provider_explicit(); // Explicit provider (E)

        // all samples
        $expectedAll = $g->get(); // 'all' should be default first parameter
        $expectedAll2 = $g->get('all'); // explicit 'all'
        $expectedAllLogs = $g->get('modlog');

        // negated samples
        $neg_expectedSample0 = array_values(array_diff($g->get(), ['aww']));
        $neg_expectedSample1 = array_values(array_diff($g->get(), ['aww','videos','DestinyTheGame']));
        $neg_expectedSample2 = array_values(array_diff($g->get(), ['aww','DestinyTheGame','videos']));

        // all:modlog with negated
        $negLogs_expectedSample0 = array_values(array_diff($expectedAllLogs, ['aww']));
        $negLogs_expectedSample1 = array_values(array_diff($expectedAllLogs, ['aww','videos','DestinyTheGame']));
        $negLogs_expectedSample2 = array_values(array_diff($expectedAllLogs, ['aww','DestinyTheGame','videos']));

        return [
            // TEST GLOBAL
            // ~~~~~~~~~~~

            // test sample 0 and 1
            ['all',                                     $expectedAll,               $g],
            ['all',                                     $expectedAll2,              $g],
            ['all-aww',                                 $neg_expectedSample0,       $g],

            // test sample 2
            ['all-aww,DestinyTheGame,videos',           $neg_expectedSample2,       $g],
            ['all-aww-DestinyTheGame-videos',           $neg_expectedSample2,       $g],
            ['all-aww-DestinyTheGame,videos',           $neg_expectedSample2,       $g],
            ['all-aww,DestinyTheGame-videos',           $neg_expectedSample2,       $g],

            // test all:modlog and alias
            ['all:modlog',                              $expectedAllLogs,           $g],
            ['all:logs',                                $expectedAllLogs,           $g],
            ['all:log',                                 $expectedAllLogs,           $g],
            ['all:modlog-aww',                          $negLogs_expectedSample0,   $g],
            ['all:modlog-aww,DestinyTheGame,videos',    $negLogs_expectedSample2,   $g],
            ['all:logs-aww-DestinyTheGame-videos',      $negLogs_expectedSample2,   $g],

            // TEST EXPLICIT
            // ~~~~~~~~~~~~~

            ['aww', ['aww'], $e],
            ['vIdeOs', ['vIdeOs'], $e],
            [['aww','DestinyTheGame', 'videos'], ['aww','DestinyTheGame', 'videos'], $e],
            [['AWw','vIdeOs'], ['AWw','vIdeOs'], $e],
            [['VIdEos','awW'], ['VIdEos','awW'], $e],
            [['aWw','VIDEOS','destInyThEgAme'], ['aWw','VIDEOS','destInyThEgAme'], $e],
        ];
    }

    /**
     * Test construction with providers.
     *
     * @dataProvider providerSamples
     */
    public function testProviders($sample, array $expected, SubredditSourceProvider $provider=null): void {
        $this->checkSubredditEquality($expected, subreddit($sample, $provider));
    }

    // TEST CONTROLLED CONSTRUCTION WITH BASIC SAMPLES
    // ------------------------------------------------------------------------------------------
    /**
     * Test construction with controlled provider pools.
     *
     * @dataProvider samples
     */
    public function testConstructionWithControlledProvider($sample, array $expected, SubredditSourceProvider $_=null): void {
        // The Controlled Subreddit Provider should work with improper casing in the provided pool

        $provider0 = subreddit_provider_controlled(['aww', 'videos', 'DestinyTheGame']); // case-accurate
        $provider1 = subreddit_provider_controlled(['aww', 'videos', 'destinythegame']); // all lowercase
        $provider2 = subreddit_provider_controlled(['AWW', 'VIDEOS', 'DESTINYTHEGAME']); // all uppercase
        $provider3 = subreddit_provider_controlled(['aWw', 'vIdEOs', 'dEStiNyThEgAMe']); // case scrambed

        $this->assertSame(['aww', 'videos', 'DestinyTheGame'], $provider0->get());
        $this->assertSame(['aww', 'videos', 'DestinyTheGame'], $provider1->get());
        $this->assertSame(['aww', 'videos', 'DestinyTheGame'], $provider2->get());
        $this->assertSame(['aww', 'videos', 'DestinyTheGame'], $provider3->get());

        $this->checkSubredditEquality($expected, subreddit($sample, $provider0));
        $this->checkSubredditEquality($expected, subreddit($sample, $provider1));
        $this->checkSubredditEquality($expected, subreddit($sample, $provider2));
        $this->checkSubredditEquality($expected, subreddit($sample, $provider3));
    }

    // TEST CONTROLLED CONSTRUCTION WITH VALID SAMPLES
    // ------------------------------------------------------------------------------------------
    /**
     * Samples for testControlledValidSamples()
     */
    public function controlledValidSamples(): array {
        $samples = [
            // Zero Subreddit Pool
            // ~~~~~~~~~~~~~~~~~~~

            [[],[],[]], // empty array
            ['',[],[]], // empty string

            // Single Subreddit Pool
            // ~~~~~~~~~~~~~~~~~~~~~

            [[],        [],         ['aww']], // empty array
            ['',        [],         ['aww']], // empty string

            [['aww'],   ['aww'],    ['aww']], // as array
            ['aww',     ['aww'],    ['aww']], // as string

            // mixed casing
            [['aWw'],   ['aww'],    ['aww']], // as array
            ['aWw',     ['aww'],    ['aww']], // as string
            [['AWW'],   ['aww'],    ['aww']], // as array
            ['AWW',     ['aww'],    ['aww']], // as string

            // Double Subreddit Pool
            // ~~~~~~~~~~~~~~~~~~~~~

            // 0 sample
            [[],                [],      ['aww', 'videos']], // as array

            // 1 sample
            [['aww'],           ['aww'], ['aww', 'videos']], // as array
            ['aww',             ['aww'], ['aww', 'videos']], // as string

            // 1 sample, mixed casing
            [['AWw'],           ['aww'], ['aww', 'videos']], // as array
            ['AWw',             ['aww'], ['aww', 'videos']], // as string

            // 1 sample, mixed casing, ensure order of the pool doesn't matter
            [['AWw'],           ['aww'], ['videos', 'aww']],
            ['AWw',             ['aww'], ['videos', 'aww']],

            // 2 sample
            [['aww', 'videos'], ['aww', 'videos'], ['aww', 'videos']], // as array
            [['videos', 'aww'], ['videos', 'aww'], ['aww', 'videos']], // as array
            ['aww+videos',      ['aww', 'videos'], ['aww', 'videos']], // as string
            ['aww,videos',      ['aww', 'videos'], ['aww', 'videos']], // as string
            ['videos+aww',      ['videos', 'aww'], ['aww', 'videos']], // as string
            ['videos,aww',      ['videos', 'aww'], ['aww', 'videos']], // as string

            // 2 sample, mixed casing
            [['Aww', 'vIdEOs'], ['aww', 'videos'], ['aww', 'videos']], // as array
            [['VideOs', 'aWW'], ['videos', 'aww'], ['aww', 'videos']], // as array
            ['aWw+Videos',      ['aww', 'videos'], ['aww', 'videos']], // as string
            ['AwW,videoS',      ['aww', 'videos'], ['aww', 'videos']], // as string
            ['ViDeOs+AWw',      ['videos', 'aww'], ['aww', 'videos']], // as string
            ['vIDeOs,aWw',      ['videos', 'aww'], ['aww', 'videos']], // as string

            // 2 sample, mixed casing, ensure order of pool doesn't matter
            [['Aww', 'vIdEOs'], ['aww', 'videos'], ['videos', 'aww']], // as array
            [['VideOs', 'aWW'], ['videos', 'aww'], ['videos', 'aww']], // as array
            ['aWw+Videos',      ['aww', 'videos'], ['videos', 'aww']], // as string
            ['AwW,videoS',      ['aww', 'videos'], ['videos', 'aww']], // as string
            ['ViDeOs+AWw',      ['videos', 'aww'], ['videos', 'aww']], // as string
            ['vIDeOs,aWw',      ['videos', 'aww'], ['videos', 'aww']], // as string
        ];

        // Multiple Subreddit Pool
        // ~~~~~~~~~~~~~~~~~~~~~~~

        $pool = ['aww', 'videos', 'DestinyTheGame'];

        foreach (powerSet($pool) as $set) {
            // ------ Normal Case
            $samples[] = [$set, $set, $pool];
            $samples[] = [implode(',', $set), $set, $pool];
            $samples[] = [implode('+', $set), $set, $pool];
            $samples[] = [alternateImplode(['+', ','], $set), $set, $pool];
            $samples[] = [alternateImplode([',', '+'], $set), $set, $pool];

            // ------ Shuffled Pool
            $shuffle_pool = $pool;
            shuffle($shuffle_pool);
            $samples[] = [$set, $set, $shuffle_pool];

            // ------ Scrambled Case
            $scrambledSet = array_map('scrambleCase', $set);
            $samples[] = [$scrambledSet, $set, $pool];
            $samples[] = [implode(',', $scrambledSet), $set, $pool];
            $samples[] = [implode('+', $scrambledSet), $set, $pool];
            $samples[] = [alternateImplode(['+', ','], $scrambledSet), $set, $pool];
            $samples[] = [alternateImplode([',', '+'], $scrambledSet), $set, $pool];
        }

        // Test Exclusions
        // ~~~~~~~~~~~~~~~

        $pool = ['aww', 'videos', 'DestinyTheGame', 'LifeProTips'];

        $samples = array_merge($samples, [
            // subtract none
            ['all', ['aww', 'videos', 'DestinyTheGame', 'LifeProTips'], $pool],

            // subtract one
            ['all-aww', ['videos', 'DestinyTheGame', 'LifeProTips'], $pool],
            ['all-videos', ['aww', 'DestinyTheGame', 'LifeProTips'], $pool],
            ['all-DestinyTheGame', ['aww', 'videos', 'LifeProTips'], $pool],
            ['all-LifeProTips', ['aww', 'videos', 'DestinyTheGame'], $pool],

            // subtract two
            ['all-aww-videos', ['DestinyTheGame', 'LifeProTips'], $pool],
            ['all-aww,videos', ['DestinyTheGame', 'LifeProTips'], $pool],

            // subtract some and in reverse order
            ['all-aww-videos-DestinyTheGame', ['LifeProTips'], $pool],
            ['all-DestinyTheGame-videos-aww', ['LifeProTips'], $pool],

            // subtract first and last
            ['all-aww-LifeProTips', ['videos', 'DestinyTheGame'], $pool],
            ['all-aww,LifeProTips', ['videos', 'DestinyTheGame'], $pool],
            ['all-LifeProTips-aww', ['videos', 'DestinyTheGame'], $pool],
            ['all-LifeProTips,aww', ['videos', 'DestinyTheGame'], $pool],

            // subtract all
            ['all-aww-videos-DestinyTheGame-LifeProTips', [], $pool],
            ['all-aww,videos,DestinyTheGame,LifeProTips', [], $pool],
        ]);

        return $samples;
    }

    /**
     * Test construction with controlled provider pools.
     *
     * @dataProvider controlledValidSamples
     */
    public function testControlledValidSamples($sample, array $expected, array $pool): void {
        $provider = subreddit_provider_controlled($pool);
        $this->assertSame($pool, $provider->get());
        $this->checkSubredditEquality($expected, subreddit($sample, $provider));
    }

    // TEST CONTROLLED CONSTRUCTION WTIH INVALID SAMPLES
    // ------------------------------------------------------------------------------------------
    /**
     * Samples for testControlledInvalidSamples()
     */
    public function controlledInvalidSamples(): array {
        $samples = [
            // Zero Subreddit Pool
            // ~~~~~~~~~~~~~~~~~~~
            [['aww'], []],
            ['aww',   []],

            // Single Subreddit Pool
            // ~~~~~~~~~~~~~~~~~~~~~

            // Single Violation
            [['aww'], ['videos']],
            ['aww',   ['videos']],

            // Single Violation, with Valid Element
            [['aww', 'videos'], ['videos']],
            [['videos', 'aww'], ['videos']],
            ['aww+videos', ['videos']],
            ['aww,videos', ['videos']],
            ['videos+aww', ['videos']],
            ['videos,aww', ['videos']],

            // Multiple Violations
            [['aww', 'DestinyTheGame'], ['videos']],
            ['aww+DestinyTheGame', ['videos']],
            ['aww,DestinyTheGame', ['videos']],

            // Multiple Violations, with Valid Element

            // as array
            [['videos', 'aww', 'DestinyTheGame'], ['videos']], // valid element at start
            [['aww', 'videos', 'DestinyTheGame'], ['videos']], // valid element in middle
            [['aww', 'DestinyTheGame', 'videos'], ['videos']], // valid element at end

            // as string, with "+"
            ['videos+aww+DestinyTheGame', ['videos']], // valid element at start
            ['aww+videos+DestinyTheGame', ['videos']], // valid element in middle
            ['aww+DestinyTheGame+videos', ['videos']], // valid element at end

            // as string, with ","
            ['videos,aww,DestinyTheGame', ['videos']], // valid element at start
            ['aww,videos,DestinyTheGame', ['videos']], // valid element in middle
            ['aww,DestinyTheGame,videos', ['videos']], // valid element at end

            // as string, with "+" and "," mixed
            ['videos,aww+DestinyTheGame', ['videos']], // valid element at start
            ['videos+aww,DestinyTheGame', ['videos']], // valid element at start
            ['aww,videos+DestinyTheGame', ['videos']], // valid element in middle
            ['aww+videos,DestinyTheGame', ['videos']], // valid element in middle
            ['aww,DestinyTheGame+videos', ['videos']], // valid element at end
            ['aww+DestinyTheGame,videos', ['videos']], // valid element at end

            // Double Subreddit Pool
            // ~~~~~~~~~~~~~~~~~~~~~
            [['DestinyTheGame'], ['videos', 'aww']],
            [['DestinyTheGame'], ['videos', 'aww']],

            [['aww', 'DestinyTheGame'], ['videos', 'aww']],
            [['videos', 'DestinyTheGame'], ['videos', 'aww']],

            [['DestinyTheGame', 'aww'], ['videos', 'aww']],
            [['DestinyTheGame', 'videos'], ['videos', 'aww']],

            [['DestinyTheGame', 'aww', 'videos'], ['videos', 'aww']],
            [['DestinyTheGame', 'videos', 'aww'], ['videos', 'aww']],

            [['aww', 'videos', 'DestinyTheGame'], ['videos', 'aww']],
            [['videos', 'aww', 'DestinyTheGame'], ['videos', 'aww']],

            [['aww', 'DestinyTheGame', 'aww'], ['videos', 'aww']],
            [['videos', 'DestinyTheGame', 'videos'], ['videos', 'aww']],

            [['aww', 'DestinyTheGame', 'videos'], ['videos', 'aww']],
            [['videos', 'DestinyTheGame', 'aww'], ['videos', 'aww']],

            [['aww', 'DestinyTheGame', 'videos'], ['videos', 'aww']],
            [['videos', 'DestinyTheGame', 'aww'], ['videos', 'aww']],

            [['aww', 'DestinyTheGame', 'videos', 'Layer7Dev'], ['videos', 'aww']],
            [['videos', 'DestinyTheGame', 'aww', 'Layer7Dev'], ['videos', 'aww']],
        ];

        // Multiple Subreddit Pool
        // ~~~~~~~~~~~~~~~~~~~~~~~

        $x = ['aww', 'videos', 'Layer7Dev', 'DestinyTheGame', 'Layer7'];

        foreach (powerSet($x, false) as $set) {
            if (!in_array('Layer7Dev', $set) && !in_array('DestinyTheGame', $set)) {
                continue;
            }

            // ----- normal case

            $samples[] = [$set, ['aww', 'videos', 'Layer7']];
            $samples[] = [$set, ['videos', 'aww', 'Layer7']];

            $samples[] = [implode('+', $set), ['aww', 'videos', 'Layer7']];
            $samples[] = [implode(',', $set), ['videos', 'aww', 'Layer7']];

            $samples[] = [alternateImplode([',', '+'], $set), ['aww', 'videos', 'Layer7']];
            $samples[] = [alternateImplode(['+', ','], $set), ['aww', 'videos', 'Layer7']];

            // ----- scrambled case

            $scrambledSet = array_map('scrambleCase', $set);

            $samples[] = [$scrambledSet, ['aww', 'videos', 'Layer7']];
            $samples[] = [$scrambledSet, ['videos', 'aww', 'Layer7']];

            $samples[] = [implode('+', $scrambledSet), ['aww', 'videos', 'Layer7']];
            $samples[] = [implode(',', $scrambledSet), ['videos', 'aww', 'Layer7']];

            $samples[] = [alternateImplode([',', '+'], $scrambledSet), ['aww', 'videos', 'Layer7']];
            $samples[] = [alternateImplode(['+', ','], $scrambledSet), ['aww', 'videos', 'Layer7']];
        }

        // Test Exclusions
        // ~~~~~~~~~~~~~~~

        // Trying to subtract with subreddits that aren't in the pool should throw an exception

        $samples = array_merge($samples, [
            // subtract 1 violation, from 1-size pool
            ['all-aww', ['videos']],

            // subtract 1 violation, with 1 valid, from 1-size pool
            ['all-aww-videos', ['videos']],
            ['all-videos-aww', ['videos']],

            // subtract 1 violation, from 2-size pool
            ['all-aww', ['videos', 'DestinyTheGame']],

            // subtract 1 violation, with 2 valids, from 1-size pool
            ['all-aww-videos-DestinyTheGame', ['videos', 'DestinyTheGame']],
            ['all-videos-aww-DestinyTheGame', ['videos', 'DestinyTheGame']],
            ['all-videos-DestinyTheGame-aww', ['videos', 'DestinyTheGame']],
        ]);

        return $samples;
    }

    /**
     * Test construction with controlled provider pools.
     *
     * @dataProvider controlledInvalidSamples
     * @expectedException \app\lib\IllegalSubredditException
     */
    public function testControlledInvalidSamples($sample, array $pool): void {
        try {
            $provider = subreddit_provider_controlled($pool);
            $this->assertSame($pool, $provider->get());
            subreddit($sample, $provider);
        } catch(\ErrorException $e) {
            exceptionHandler($e);
        }
    }


    // TEST EXCEPTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Samples for testInvalidConstruction()
     *
     * The `all` leading token can only be used with subtraction. `all` cannot be used with
     * concatenation. And two subreddits cannot be subtracted.
     */
    public function invalidSamples(): array {
        return [
            ['aww-DestinyTheGame'],
            ['aww-DestinyTheGame+videos'],
            ['aww+DestinyTheGame-videos'],
            ['aww-DestinyTheGame,videos'],
            ['aww,DestinyTheGame-videos'],
            ['all+aww'],
            ['all,aww'],
            ['all-aww+DestinyTheGame'],
            ['all:modlog,aww'],
            ['all:modlog+aww'],
            ['all:modlog+aww-videos'],
        ];
    }

    /**
     * Test constructions that should fail due to invalid syntax.
     *
     * @dataProvider invalidSamples
     * @expectedException \app\lib\InvalidSubredditException
     */
    public function testInvalidConstruction($badValue): void {
        subreddit($badValue);
    }

    /**
     * Samples for testUnknownConstruction()
     *
     * Make sure the "unknown" subreddits in these samples aren't anything we have in our database.
     */
    public function unknownSamples(): array {
        return [
            // test on its own
            ['__UNKNOWNSUBREDDIT__'],
            [['__UNKNOWNSUBREDDIT__']],

            // test with a known subreddit (even just one unknown subreddit should cause the entire
            // construction to fail)
            ['aww+__UNKNOWNSUBREDDIT__'],
            ['aww+__UNKNOWNSUBREDDIT__'],
            [['aww','__UNKNOWNSUBREDDIT__']],

            ['__UNKNOWNSUBREDDIT__+DestinyTheGame'],
            ['__UNKNOWNSUBREDDIT__,DestinyTheGame'],
            [['__UNKNOWNSUBREDDIT__','DestinyTheGame']],

            ['aww+__UNKNOWNSUBREDDIT__+DestinyTheGame'],
            ['aww+__UNKNOWNSUBREDDIT__,DestinyTheGame'],
            [['aww','__UNKNOWNSUBREDDIT__','DestinyTheGame']],
        ];
    }

    /**
     * Test constructions that should fail due to an unknown subreddit, which is not necessarily
     * a subreddit that does not exist on Reddit, but at least a subreddit that does not exist in
     * our database because the subreddit does not have TheSentinelBot.
     *
     * @dataProvider unknownSamples
     * @expectedException \app\lib\UnknownSubredditException
     */
    public function testUnknownConstruction($unknownValue): void {
        subreddit($unknownValue);
    }

}