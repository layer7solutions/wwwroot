<?php
use app\lib\RT;

$c0_t0 = 't3_6r98ve';
$c0_t1 = 't1_dl3b2tf';
$c0_t2 = 't1_dl3b33q';
$c0_t3 = 't1_dl3b3kp';

$c1_t0 = 't3_6r98ve';
$c1_t1 = 't1_dl3b2tf';
$c1_t2 = 't1_dl3b433';
$c1_t3 = 't1_dl3b5n3';
$c1_t4 = 't1_dl3b5ro';

// TEST GET CHILDREN TREE

echo "\n\n### get_children({$c0_t0})\n\n";
$r0 = RT::get_children($c0_t0);
var_dump(RT::build_tree($r0));

echo "\n\n### get_children({$c0_t0}, get_data=false)\n\n";
$r1 = RT::get_children($c0_t0, null, false);
var_dump(RT::build_tree($r1));

echo "\n\n### get_children({$c0_t0}, max_rec_depth=2)\n\n";
$r2 = RT::get_children($c0_t0, 2);
var_dump(RT::build_tree($r2));

echo "\n\n### get_children({$c1_t2}, max_rec_depth=2, get_data=false)\n\n";
$r2 = RT::get_children($c1_t2, 2, false);
var_dump(RT::build_tree($r2));

echo "\n\n### get_children({$c1_t2}, max_rec_depth=1, get_data=false)\n\n";
$r2 = RT::get_children($c1_t2, 1, false);
var_dump(RT::build_tree($r2));

// TEST GET DIRECT CHILDREN TREE

echo "\n\n### get_direct_children({$c0_t0})\n\n";
$r0 = RT::get_direct_children($c0_t0);
var_dump(RT::build_tree($r0));

echo "\n\n### get_direct_children({$c0_t1})\n\n";
$r0 = RT::get_direct_children($c0_t1);
var_dump(RT::build_tree($r0));

// TEST GET CONTEXT TREE

echo "\n\n### get_context({$c0_t1})\n\n";
$r3 = RT::get_context($c0_t1);
var_dump(RT::build_tree($r3));

echo "\n\n### get_context({$c0_t3})\n\n";
$r4 = RT::get_context($c0_t3);
var_dump(RT::build_tree($r4));

echo "\n\n### get_context({$c1_t4}, max_rec_depth=2, get_data=false)\n\n";
$r5 = RT::get_context($c1_t4, 2, false);
var_dump(RT::build_tree($r5));