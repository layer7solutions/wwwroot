<?php

use app\lib\RT;

$links = [
    'https://www.reddit.com/r/videos/comments/85vrfn/why_no_white_panther/', // link post with youtube
    'https://www.reddit.com/r/LifeProTips/comments/85suc0/lpt_when_doing_research_for_a_paper_annotate_each/', // text post, no links
    /*
    'https://www.reddit.com/r/videos/comments/85vrfn/why_no_white_panther/dw0p18j/', // no media
    'https://www.reddit.com/r/videos/comments/85vrfn/why_no_white_panther/dw0q4qh/', // twitter
    'https://www.reddit.com/r/videos/comments/85vrfn/why_no_white_panther/dw0s0z8/', // youtube
    'https://www.reddit.com/r/videos/comments/7ejban/net_neutrality_videos_discussion_megathread/', // text post with youtube link
    'https://www.reddit.com/r/videos/comments/7ejban/net_neutrality_videos_discussion_megathread/dq5d6aw/', // comment with a bunch of youtube links
*/];

foreach ($links as $link) {
    var_dump($link);
    $thing_id = RT::id_from_link($link);
    var_dump($thing_id);

    $x = RT::get_media_info($thing_id);
    var_dump($x);
    echo "\n\n";
}