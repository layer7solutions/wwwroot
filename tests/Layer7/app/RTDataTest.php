<?php
use app\lib\RT;

// This file tests RT in general.
// i.e. we're making sure it works

// t3_6r98ve
//   t1_dl3b2tf
//     t1_dl3b33q
//       t1_dl3b3kp

$t0 = 't3_6r98ve';
$t1 = 't1_dl3b2tf';
$t2 = 't1_dl3b33q';
$t3 = 't1_dl3b3kp';

echo "\n\n### get_data({$t0})\n\n";
var_dump(RT::get_data($t0));

echo "\n\n### get_attr({$t0}, data)\n\n";
var_dump(RT::get_attr($t0, 'data'));

echo "\n\n### get_attr({$t0}, subreddit)\n\n";
var_dump(RT::get_attr($t0, 'subreddit'));

echo "\n\n### get_subreddit({$t0})\n\n";
var_dump(RT::get_subreddit($t0));

echo "\n\n--------------------------------------------------------------------------------\n\n";

echo "\n\n### get_mod_actions({$t0})\n\n";
var_dump(RT::get_mod_actions($t0));

echo "\n\n### get_mod_actions({$t1})\n\n";
var_dump(RT::get_mod_actions($t1));

echo "\n\n### get_mod_actions({$t2})\n\n";
var_dump(RT::get_mod_actions($t2));

echo "\n\n### get_mod_actions({$t3})\n\n";
var_dump(RT::get_mod_actions($t3));

echo "\n\n### get_mod_actions(t1_dl3b5n3)\n\n";
var_dump(RT::get_mod_actions('t1_dl3b5n3'));

echo "\n\n--------------------------------------------------------------------------------\n\n";

echo "\n\n### get_children({$t0})\n\n";
var_dump(RT::get_children($t0));

echo "\n\n### get_children({$t0}, get_data=false)\n\n";
var_dump(RT::get_children($t0, null, false));

echo "\n\n### get_children({$t0}, max_rec_depth=2)\n\n";
var_dump(RT::get_children($t0, 2));

echo "\n\n### get_direct_children({$t0})\n\n";
var_dump(RT::get_direct_children($t0));

echo "\n\n### get_direct_children({$t0}, get_data=false)\n\n";
var_dump(RT::get_direct_children($t0, false));

echo "\n\n--------------------------------------------------------------------------------\n\n";

echo "\n\n### get_children({$t1})\n\n";
var_dump(RT::get_children($t1));

echo "\n\n--------------------------------------------------------------------------------\n\n";

echo "\n\n### get_parent_id({$t1})\n\n";
var_dump(RT::get_parent_id($t1));

echo "\n\n### get_parent({$t1})\n\n";
var_dump(RT::get_parent($t1));

echo "\n\n### get_parent_attr({$t1}, data)\n\n";
var_dump(RT::get_parent_attr($t1, 'data'));

echo "\n\n### get_context({$t1})\n\n";
var_dump(RT::get_context($t1));

echo "\n\n### get_context({$t3})\n\n";
var_dump(RT::get_context($t3));

echo "\n\n### get_context({$t3}, max_rec_depth=2)\n\n";
var_dump(RT::get_context($t3, 2));

echo "\n\n### get_context({$t3}, get_data=false)\n\n";
var_dump(RT::get_context($t3, null, false));