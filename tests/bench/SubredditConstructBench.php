<?php

/**
 * @BeforeMethods({"init"})
 */
class SubredditConstructBench {

    public function init() {
        subreddit(['aww']); // call once before benchmark to force initialization of lazy-initialized objects
    }

    /**
     * @Revs(5000)
     */
    public function benchConstructionArray1() {
        subreddit(['aww']);
    }

    /**
     * @Revs(5000)
     */
    public function benchConstructionArray2() {
        subreddit(['aww','videos']);
    }

    /**
     * @Revs(5000)
     */
    public function benchConstructionArray3() {
        subreddit(['aww','videos','Layer7','DestinyThegame','YT_Killer','Layer7Dev']);
    }

    /**
     * @Revs(5000)
     */
    public function benchConstructionString1() {
        subreddit('aww');
    }

    /**
     * @Revs(5000)
     */
    public function benchConstructionString2() {
        subreddit('aww+videos');
    }

    /**
     * @Revs(5000)
     */
    public function benchConstructionString3() {
        subreddit('aww+videos+Layer7+DestinyTheGame+YT_Killer+Layer7Dev');
    }
}