<?php

use PHPUnit\Framework\TestCase;

use app\lib\Subreddit;
use app\lib\providers\SubredditSourceProvider;

abstract class BaseTest extends TestCase {

    public function checkSubredditEquality(array $expected, Subreddit $sr, bool $fromSerialized=false) {
        $expectedCount = count($expected);

        $this->assertInstanceOf(Subreddit::class, $sr);
        $this->assertInstanceOf(SubredditSourceProvider::class, $sr->getProvider());

        // make sure internal array is all strings
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        $this->assertContainsOnly('string', $sr->to_array());

        // check matching count
        // ~~~~~~~~~~~~~~~~~~~~
        $this->assertCount($expectedCount, $sr->to_array());

        // check Countable implementation
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        $this->assertTrue($expectedCount === count($sr));

        // check equality
        // ~~~~~~~~~~~~~~
        $this->assertSame($expected, $sr->to_array());
        $this->assertSame($this->arrayToLower($expected), $sr->to_lower_array());

        // check ArrayAccess implementation
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        for ($i = 0; $i < $expectedCount; $i++) {
            $this->assertTrue(isset($sr[$i]));

            $this->assertSame($expected[$i], $sr[$i]);

            $this->assertSame($expected[$i], $sr->to_array()[$i]);
            $this->assertSame($expected[$i], $sr->to_array($i));

            $this->assertSame(strtolower($expected[$i]), $sr->to_lower_array()[$i]);
            $this->assertSame(strtolower($expected[$i]), $sr->to_lower_array($i));

            $check1 = false;
            $check2 = false;

            try {
                $sr[$i] = 'foobar';
            } catch (\BadMethodCallException $e) {
                $check1 = true;
            }

            try {
                unset($sr[$i]);
            } catch (\BadMethodCallException $e) {
                $check2 = true;
            }

            $this->assertTrue($check1);
            $this->assertTrue($check2);
        }
        $this->assertFalse(isset($sr[$expectedCount]));

        // check Iterator implementation
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        $i = 0;
        foreach ($sr as $item) {
            $this->assertSame($expected[$i], $item);
            $this->assertSame($sr[$i], $item);
            $i++;
        }

        // check Iterator implementation again to make
        // sure it properly resets to the first element

        $i = 0;
        foreach ($sr as $item) {
            $this->assertSame($expected[$i], $item);
            $this->assertSame($sr[$i], $item);
            $i++;
        }

        // check Serializable implementation
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if (!$fromSerialized) {
            $serialized = serialize($sr);
            $new_sr = unserialize($serialized);

            // check serialized Subreddit with $fromSerialized=true
            $this->checkSubredditEquality($expected, $new_sr, true);

            // make sure providers are the same
            $this->assertSame(get_class($sr->getProvider()), get_class($new_sr->getProvider()));
        }
    }

    /**
     * Return the same array with all values converted to lowercase.
     *
     * @param array $a
     * @return array
     */
    public function arrayToLower(array $a) {
        return array_map('strtolower', $a);
    }

     /**
     * Determine if two associative arrays are similar
     *
     * Both arrays must have the same indexes with identical values
     * without respect to key ordering
     *
     * @param array $a
     * @param array $b
     * @return bool
     */
    public function arraysAreSimilar($a, $b) {
        // if the indexes don't match, return immediately
        if (count(array_diff_assoc($a, $b))) {
            return false;
        }

        // we know that the indexes, but maybe not values, match.
        // compare the values between the two arrays
        foreach($a as $k => $v) {
            if ($v !== $b[$k]) {
                return false;
            }
        }

        // we have identical indexes, and no unequal values
        return true;
    }

}