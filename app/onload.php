<?php
// This file is included from within App::load
// Note that functions declared within functions are in the global namespace

date_default_timezone_set('UTC'); // Set timezone to UTC-0

// CACHE BUSTER
// --------------------------------------------------------------------------------
// increment these version numbers when you need to bust the cache
define('CSS_VERSION', '2.0.1c'); // ProductionClient.css
define('JS_VERSION', '2.0.5c'); // ProductionClient.js

// APPLICATION INCLUDES
// --------------------------------------------------------------------------------
require APP_ROOT.'lib/session/session.php'; // defines session_init()
require APP_ROOT.'lib/User.php';
require APP_ROOT.'api/api.php'; // defines api_start()

use app\lib\{Subreddit,User};
use app\lib\providers\SubredditSourceProvider;

// set oauth api scopes
api_scopes(array(
    'identity'          => 'access your reddit username and the list of subreddits you mod',
    'tsbread'           => 'view the list of blacklisted media items',
    'tsbedit'           => 'add and remove media items to/from blacklists',
    'logsread'          => 'view mod logs and matrices',
    'srprefread'        => 'lookup subreddit settings',
    'srprefedit'        => 'change subreddit settings on TSB subreddits you have full perms on',
    'modmailread'       => 'read and search modmail',
    'dirtbagprefread'   => 'lookup dirtbag settings',
    'dirtbagprefedit'   => 'change dirtbag settings',
    'thingread'         => 'read, lookup, and search reddit comments and posts',
    'linkshorten'       => 'use the link shortener service',
    'disconotify'       => 'use the Discord Notification service',
));
api_default_scope('identity');

// DECLARE ALIASES
// --------------------------------------------------------------------------------

function db(string $dbname, string $user = null, string $password = null): DB {
    return App::db($dbname, $user, $password);
}

function subreddit_provider_global() {
    return SubredditSourceProvider::getInstance(
            app\lib\providers\AdministrativeSubredditProvider::class);
}

function subreddit_provider_explicit() {
    return SubredditSourceProvider::getInstance(
            app\lib\providers\ExplicitSubredditProvider::class);
}

function subreddit_provider_controlled($srIn) {
    return SubredditSourceProvider::getInstance(
            app\lib\providers\ControlledSubredditProvider::class, [$srIn]);
}

/**
 * @param null $srIn
 * @param SubredditSourceProvider|null $provider
 * @return Subreddit
 * @throws \InvalidArgumentException
 * @throws \app\lib\InvalidSubredditException
 * @throws \app\lib\IllegalSubredditException
 * @throws \app\lib\UnknownSubredditException
 * @throws \app\lib\SubredditException
 */
function subreddit($srIn=null, SubredditSourceProvider $provider=null): Subreddit {
    $provider = $provider ?? subreddit_provider_global();

    if (!($provider instanceof SubredditSourceProvider)) {
        throw new InvalidArgumentException(
            'Second parameter to subreddit($srIn, $provider) '.
            'must be instance of SubredditSourceProvider');
    }

    return new Subreddit($srIn, $provider);
}

function subreddit_explicit($srIn=null) {
    return subreddit($srIn, SubredditSourceProvider::getInstance(
        app\lib\providers\ExplicitSubredditProvider::class));
}

/**
 * Get a User object. If no parameter or `null` is passed in, then current user will be returned.
 * Otherwise the User object for the specified username will be returned.
 *
 * @param string $username (optional)
 * @return User
 */
function user(string $username = null): User {
    return isset($username) ? User::user($username) : User::current_user();
}

// REGISTER SUBREDDIT MODULES
// --------------------------------------------------------------------------------
// Registering a Subreddit module simply adds the module name -> class path to a
// static field.
// Modules are lazily inintialized per Subreddit instance.
//
//                          Module name     Class path
Subreddit::register_module('mediaban',     app\lib\mixins\MediaBlacklistModule::class);
Subreddit::register_module('botban',       app\lib\mixins\BotbanModule::class);
Subreddit::register_module('domainban',    app\lib\mixins\DomainModule::class);

Subreddit::register_module('dirtbag',      app\lib\mixins\DirtbagModule::class);
Subreddit::register_module('logs',         app\lib\mixins\LogsModule::class);
Subreddit::register_module('modmail',      app\lib\mixins\ModmailModule::class);
Subreddit::register_module('users',        app\lib\mixins\UsersModule::class);

Subreddit::register_module('config',       app\lib\SubredditConfig::class);

// CHECK TEST MODE
// --------------------------------------------------------------------------------
// Everything after this point is for session, routing, and presentation.
// If we're running unit tests (which is from the command line), then anything
// after this point is unnecessary.

if (isset($GLOBALS['__application_test_mode']) && $GLOBALS['__application_test_mode'] === true) {
    return;
}

// SETUP BASE VIEW PROFILE
// --------------------------------------------------------------------------------
// We setup this profile for both API and Web requests in case the user lands on
// the `/api/{version}/authorize` page
View::addProfile('net.layout', function(View $view) {
    if (!$view->hasProfile('net.applayout') || !user()->logged_in()) {
        $view->bodyclass('daymode');
    } else {
        $view->bodyclass(user()->property('nightmode_enabled') ? 'nightmode' : 'daymode');
    }

    $view->favicontype('ico?v=2.4');
    $view->use('logo_url', 'brand/logo-standard.svg');
    $view->addJSVar('CURRENT_USER', user()->get_username());
    if (user()->logged_in()) {
        $view->use('user_has_timezone_set', !empty(user()->prefs->get_php_name_timezone()));
        $view->addJSVar('USER_TZ_DISPLAY', user()->prefs->get_display_timezone());
        $view->addJSVar('USER_TZ_PHP_NAME', user()->prefs->get_php_name_timezone());
        $view->addJSVar('USER_TZ_OFFSET', user()->prefs->get_gmt_timezone_offset());
        $view->addJSVar('USER_TZ_ABRV', user()->prefs->get_abrv_timezone());
    }
    $view->usinglib(['deepmerge', 'shvl', 'vue', 'vuex', 'vuex-persistedstate']);
    $view->stylesheets('ProductionClient');
    $view->scripts('ProductionClient');
    $view->async_scripts('CheckCompat');
    $view->scripts('ext/general');
    $view->usinglib([
        'moment',
        'tippy',
        'fontawesome',
        'zmdi',
        'default_fonts',
    ]);
});

// CHECK API REQUEST
// --------------------------------------------------------------------------------
if (startsWith(App::request_uri(), '/api/')) {
    api_start();
    exit;
}

// INIT WEB REQUEST
// --------------------------------------------------------------------------------
session_init();

if (user()->logged_in()) {
    user()->session->createOrUpdate(false);
}

View::addProfile('net.applayout', function(View $view) {
    $view->viewport('width=1024');
    $view->bodyclass('page--in_app application full-nav');
    $view->use('no_footer', true);
});
View::addProfile('net.xyzlayout', function(View $view) {
    $view->bodyclass('page--xyz');
});
View::addProfile('blacklist.simpleblacklist', function(View $view) {
    $view->usinglib(['selectr-css', 'selectr-js'])
        ->bodyclass('page--sbt')
        ->scripts('ext/blacklist/SimpleBlacklist')
        ->stylesheets('ext/blacklist/simpleblacklist');
});
View::addProfile('prefs.layout', function(View $view) {
    $view->bodyclass('page--prefs')
        ->stylesheets('ext/prefs/common')
        ->scripts('ext/prefs/common');
});

return 'routes/routes.php';