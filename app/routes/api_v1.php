<?php
// API Resource Routes
// ~~~~~~~~~~~~~~~~~~~

use app\lib\Subreddit;
use app\lib\SubredditException;
use app\lib\InvalidSubredditException;
use app\lib\IllegalSubredditException;
use app\lib\UnknownSubredditException;

App::router()

    ->setParameterHook('subreddit', function($value, $resourceName) {
        // SR_GUARD - this parameter hook will intercept `subreddit` parameters in the route
        // patterns below in this file. If the SR_GUARD fails, then an SR_INVALID, SR_FORBIDDEN, or
        // SR_UNKNOWN error will be dispatched back to the client.

        if (api_prefer('no_sr_guard') && user()->is_globalist()) {
            // If the SR guard is dropped, try creating the Subreddit object. If it fails, quietly
            // exit the hook without change and pass the subreddit value off for the APIResource to
            // handle.

            try {
                return user()->modded($value);
            } catch (\app\lib\SubredditException $e) {
                return $value;
            }
        }

        try {
            return user()->modded($value);
        } catch (InvalidSubredditException $e) {
            api_dispatchError('SR_INVALID', $e->getMessage(), $e->getDetail(), 400);
        } catch (IllegalSubredditException $e) {
            api_dispatchError('SR_FORBIDDEN', $e->getMessage(), $e->getDetail(), 403);
        } catch (UnknownSubredditException $e) {
            api_dispatchError('SR_UNKNOWN', $e->getMessage(), $e->getDetail(), 404);
        } catch (SubredditException $e) {
            api_dispatchError('SR_INVALID', $e->getMessage(), $e->getDetail(), 400);
        }
        exit;
    })

;

App::router()

    ->addGroup('/identity')
        ->GET('[/{field}[/{field2}]]',              'Identity')
        ->POST('[/{field}[/{field2}]]',             'Identity')
    ->endGroup()

    ->addGroup('/admin')
        ->ANY('/oauth_data',                        'OAuthData')
        ->ANY('/test/{test_name}',                  'UnitTest')
    ->endGroup()

    ->addGroup('/logs')
        ->GET('/search',                            'LogsView')
        ->GET('/view',                              'LogsView')
    ->endGroup()

    ->addGroup('/data')
        ->GET('/media_info',                        'MediaInfo')
        ->GET('/thing',                             'DataThing')
        ->GET('/tsb_agents',                        'TSBAgents')
    ->endGroup()

    ->addGroup('/shortlink')
        ->GET('[/{short_id}]',                      'LinkShorten')
        ->DELETE('/{short_id}',                     'LinkShorten')
        ->PATCH('/{short_id}',                      'LinkShorten')
        ->POST('/',                                 'LinkShorten')
    ->endGroup()

    ->addGroup('/blacklist')
        ->GET('/globalqueue/{action}',              'BlacklistGlobal')
        ->POST('/globalqueue/{action}',             'BlacklistGlobal')
    ->endGroup()

    ->addGroup('/r/{subreddit}')
        ->GET('/info',                              'SubredditInfo')

        ->GET('/blacklist',                         'Blacklist')
        ->PUT('/blacklist',                         'Blacklist')
        ->DELETE('/blacklist',                      'Blacklist')
        ->GET('/blacklist/reports',                 'BlacklistReports')

        ->GET('/logs/matrix',                       'LogsMatrix')

        ->GET('/modmail/search',                    'ModmailSearch')
        ->GET('/modmail[/{id}]',                    'ModmailLookup')

        ->GET('/settings',                          'SubredditSettings')

        ->GET('/dirtbag/settings',                  'DirtbagSettings')
        ->PUT('/dirtbag/settings',                  'DirtbagSettings')
        ->DELETE('/dirtbag/settings',               'DirtbagSettings')
    ->endGroup()

    ->addGroup('/discord')
        ->PUT('/notify',                            'DiscoNotify')
    ->endGroup()

;