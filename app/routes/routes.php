<?php
/*
routes.php
~~~~~~~~~~
# -> runs controller
% -> runs string as PHP code
*/

App::router()

    // MISC
    // --------------------------------------------------------------------------------
    ->GET('/[homepage]',                        '#Landing')
    ->GET('/about',                             '#Landing:about')
    ->GET('/overview',                          '#Overview')
    ->GET('/contact',                           '%redirect_reddit("/message/compose?to=%2Fr%2FLayer7")')
    ->GET('/discord',                           '%redirect_ext("https://discord.gg/XfYcVhH")')

    // ADMIN
    // --------------------------------------------------------------------------------
    ->addGroup('/admin')
        ->GET('/',                              '#Admin')
        ->ANY('/websync',                       '#Admin:websync')
        ->GET('/adminer',                       '#Admin:adminer')
        ->GET('/globalqueue',                   '#Admin:globalqueue')
        ->GET('/userlist',                      '#Admin:userlist')
        ->GET('/oauth_data',                    '#Admin:oauth_data')
        ->GET('/enable',                        '#Admin:enable')
        ->GET('/disable',                       '#Admin:disable')
        ->GET('/page500',                       '#Admin:page500')
        ->GET('/phpinfo',                       '#Admin:phpinfo')
    ->endGroup()

    // LOGIN / LOGOUT
    // --------------------------------------------------------------------------------
    ->GET('/login',                             '#Login:login')
    ->POST('/login',                            '#Login:login')
    ->GET('/login/interstitial',                '#Login:interstitial')
    ->POST('/logout',                           '#Login:logout')

    // MODULES
    // --------------------------------------------------------------------------------
    ->addGroup('/blacklist')
        ->GET('/',                              '#MediaBlacklist:edit_page')
        ->GET('/edit',                          '#MediaBlacklist:edit_page')
        ->GET('/view',                          '#MediaBlacklist:view_page')
        ->GET('/reports',                       '#MediaBlacklist:reports_page')
        ->GET('/global',                        '#MediaBlacklist:globalblacklist_page')
    ->endGroup()
    ->addGroup('/logs')
        ->GET('/',                              '%redirect("/logs/view")')
        ->GET('/view',                          '#Logs:view_page')
        ->GET('/matrix',                        '#Logs:matrix_selection_page')
        ->GET('/matrix/{subreddit}',            '#Logs:matrix_page')
    ->endGroup()
    ->addGroup('/data')
        ->GET('/',                              '#Data')
        ->GET('/explore',                       '#Data:explore')
        ->GET('/users[/{subreddit}]',           '#Data:users')
        ->POST('/users/{subreddit}',            '#Data:users_POST')
    ->endGroup()
    ->GET('/botban',                            '#Botban')
    ->GET('/domainban',                         '#Domains')
    ->GET('/dirtbag',                           '#Dirtbag:selection_page')
    ->GET('/dirtbag/{subreddit}',               '#Dirtbag:settings_page')
    ->GET('/modmail/search',                    '#Modmail:searchpage')
    ->GET('/modmail[/{id}]',                    '#Modmail:lookuppage')
    ->GET('/shortlink',                         '#LinkShortener')

    // PREFS
    // --------------------------------------------------------------------------------
    ->addGroup('/prefs')
        ->GET('/',                              '#Prefs')
        ->GET('/apps',                          '#Prefs:apps_page')
        ->POST('/apps',                         '#Prefs:apps_postAction')
        ->GET('/account-{page:activity|data}',  '#Prefs:account_page')
        ->GET('/r[/{subreddit}]',               '#Prefs:subreddit_page')
        ->GET('/subreddit[/{subreddit}]',       '#Prefs:subreddit_page')
        ->ANY('/nightmode-enable',              '#Prefs:nightmode_enable')
        ->ANY('/nightmode-disable',             '#Prefs:nightmode_disable')
    ->endGroup()

    // DOCS
    // --------------------------------------------------------------------------------
    ->GET('/docs/internal[/{p1}]',              '#Documentation:internal[p0="internal"]')
    ->GET('/docs[/{p0}[/{p1}]]',                '#Documentation')
    ->GET('/developers[/{p1}]',                 '#Documentation[p0="apidocs"]')
    ->GET('/get-started',                       '%redirect("/docs/get-started")')
    ->GET('/getting-started',                   '%redirect("/docs/get-started")')
    ->GET('/faq',                               '%redirect("/docs/faq")')
    ->GET('/api',                               '%redirect("/developers")')
    ->GET('/tos',                               '%redirect("/terms")')
    ->GET('/privacy[/{p1}]',                    '#Documentation:legal[p0="privacy"]')
    ->GET('/terms[/{p1}]',                      '#Documentation:legal[p0="terms"]')

    // layer7.xyz
    // --------------------------------------------------------------------------------

    ->addGroup('/xyz')
        ->GET('/ntpls',                         '#Landing:ntpls')
        ->GET('/{short_id:[a-zA-Z0-9]+}',       '#LinkShortenerFollow')
    ->endGroup()

    ->GET('/testing_stuff/{testing_foo}/{testing_bar}', function($params) {
        var_dump($params);
        echo "foobar";
        if (true === true) {
            echo " lorem ipsum";
        }
        die;
    })


    ->setParameterHook('testing_foo', function($value, $resourceName) {
        if ($value === "foo") {
            return "foobar!!!";
        }
        return "foo_" . $value . "_foo";
    })


    ->setParameterHook('testing_bar', function($value, $resourceName) {
        if ($value === "bar") {
            return "FOOBAR!";
        }
        return "bar_" . $value . "_bar";
    })

;