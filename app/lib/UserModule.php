<?php
namespace app\lib;

use app\lib\User;

trait UserModule {
    /**
     * @var User
     */
    protected $parent;

    /**
     * @var string|null
     */
    protected $username;
}