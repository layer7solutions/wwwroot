<?php
namespace app\lib;

class SubredditConfig extends SubredditModule {
    const SETTINGS = [
        [
            'property'  => 'sentinel_enabled',
            'label'     => ['Sentinel Enabled', 'posts'],
            'type'      => 'boolean',
        ],
        [
            'property'  => 'modlog_enabled',
            'label'     => 'Mod Action Logging Enabled',
            'type'      => 'boolean',
        ],
        [
            'property'  => 'modmail_enabled',
            'label'     => ['Modmail Logging Enabled', 'mail'],
            'type'      => 'boolean',
        ],
        [
            'property'  => 'botban_enabled',
            'label'     => ['Bot Ban Enabled', 'posts'],
            'type'      => 'boolean',
        ],
        [
            'property'  => 'domainblacklist_enabled',
            'label'     => ['Domain Blacklisting Enabled', 'posts'],
            'type'      => 'boolean',
        ],
        [
            'property'  => 'dirtbag_enabled',
            'label'     => ['Dirtbag Enabled', 'posts'],
            'type'      => 'boolean',
        ],
    ];

    /**
     * @var array populated by `self::init()` using `self::SETTINGS`
     */
    private static $changeable_properties = [];

    /**
     * @var array populated by `self::init()` using `self::SETTINGS`
     */
    private static $accessible_properties = [
        'id' => true,
        'subreddit_name' => true,
        'redditbot_name' => true,
        'redditbot_permissions' => true,
    ];

    public static function can_access(string $property): bool {
        return isset(self::$accessible_properties[$property]);
    }
    public static function can_change(string $property): bool {
        return isset(self::$changeable_properties[$property]);
    }

    public static function init() {
        static $_didInit = false;

        if ($_didInit) return;
        else $_didInit = true;

        foreach (self::SETTINGS as $setting) {
            self::$accessible_properties[$setting['property']] = true;
            self::$changeable_properties[$setting['property']] = $setting;
        }
    }

    public function get(string $property = null) {
        if (!isset($property)) {
            $raw_data = $this->subreddit->row();
            $ret = [];

            foreach (self::$accessible_properties as $k => $v) {
                $ret[$k] = $raw_data[$k];
            }

            return $ret;
        }
        if (!self::can_access($property)) {
            return null;
        }
        return $this->subreddit->column($property);
    }

    public function set(string $property, $value) {
        $result =  [
            'property' => $property,
            'success' => false,
            'new' => null,
            'old' => null,
        ];

        if (!self::can_change($property)) {
            return $result;
        }

        $result['old'] = $this->get($property);

        switch (self::$changeable_properties[$property]['type']) {
            case 'boolean':
                $value = bool_unstr($value);
                break;
            case 'int':
                $value = intval($value);
                break;
            case 'double':
                $value = doubleval($value);
                break;
            case 'string':
                $value = strval($value);
                break;
            default:
                return $result;
        }

        $this->subreddit->column($property, $value, true);
        $result['new'] = $value;
        $result['success'] = true;
        return $result;
    }

    public function log() {
        $query = 'SELECT * FROM settings_history WHERE subreddit IN %ls_srlist ORDER BY changed_utc DESC';
        $res = db('application')->query($query, ['srlist' => $this->subreddit->to_lower_array()]);
        $ret = [];

        foreach ($res as $entry) {
            if (!self::can_access($entry['setting'])) {
                continue;
            }
            if (!isset($ret[$entry['subreddit']])) {
                $ret[$entry['subreddit']] = [];
            }
            $ret[$entry['subreddit']][] = $entry;
        }

        if (empty($ret)) {
            return [];
        }

        return count($this->subreddit) === 1 ? reset($ret) : $ret;
    }

}

SubredditConfig::init();