<?php
namespace app\lib;

// RT - Reddit Thing
class RT {

    // FIELDS
    // ~~~~~~

    private static $rec_tree_children;
    private static $rec_tree_parent;
    private static $select_fields;
    private static $alias_to_field_table;
    private static $thing_union;
    private static $select_union;
    private static $select_tree;

    public static function init() {
        static $_didInit = false;

        if ($_didInit) return;
        else $_didInit = true;

        self::$rec_tree_children =
            "WITH RECURSIVE rec_tree AS (
                SELECT thing_id, parent_id, 1 as rec_depth, 'rec_tree_children' as rec_cte
                FROM   reddit_thing
                WHERE  parent_id=%s

                UNION ALL

                SELECT c.thing_id, c.parent_id, t.rec_depth + 1, 'rec_tree_children' as rec_cte
                FROM   reddit_thing c, rec_tree t
                WHERE  c.parent_id = t.thing_id
            )";
        self::$rec_tree_parent =
            "WITH RECURSIVE rec_tree AS (
                SELECT thing_id, parent_id, 0 as rec_depth, 'rec_tree_parent' as rec_cte
                FROM   reddit_thing
                WHERE  thing_id=%s

                UNION ALL

                SELECT p.thing_id, p.parent_id, t.rec_depth + 1, 'rec_tree_parent' as rec_cte
                FROM   reddit_thing p, rec_tree t
                WHERE  t.parent_id = p.thing_id
            )";
        self::$select_fields =
            "
            sr.subreddit_name as subreddit,
            rt.thing_id,
            (CASE
                WHEN (rt.thing_title IS NULL) THEN 'comment'
                WHEN (rt.link_url ~* ('^https?:\/\/(.*?)?\.reddit\.com\/(.*?)\/comments\/' || substr(rt.thing_id,4) || '(\/.*?)?') ) THEN 'self post'
                ELSE 'link post'
            END) as type,
            rt.author,
            rt.created_utc,
            rt.parent_thing_id as parent_post_id,
            rt.parent_id as parent_thing_id,
            rt.permalink,
            rt.thing_title as title,
            rt.thing_data as data,
            rt.link_url
            ";
        self::$alias_to_field_table = [
            'title'             => 'thing_title',
            'data'              => 'thing_data',
            'parent_thing_id'   => 'parent_id',
            'parent_post_id'    => 'parent_thing_id',
        ];
        self::$thing_union =
            ", thing_union AS (
                SELECT ".self::$select_fields.",
                    t.rec_depth,
                    t.rec_cte
                FROM rec_tree t
                JOIN reddit_thing rt ON (t.thing_id = rt.thing_id)
                LEFT JOIN subreddit sr ON (rt.subreddit_id = sr.id)
            )";
        self::$select_union = " SELECT * FROM thing_union";
        self::$select_tree  = " SELECT * FROM rec_tree";
    }

    // TREE
    // ~~~~

    // Build a tree from the result of get_children or get_context.
    // Results from both functions are always in order from highest
    // level thing to the lowest level thing
    //
    // $thing_list - the result of get_children or get_context, this
    //               function will return null if this parameter is empty
    //
    // Do not remove any of the following properties of any items
    // from the results of get_children and get_context:
    //    thing_id, parent_id, rec_depth, rec_cte
    // else this function might fail
    public static function build_tree($thing_list) {
        if (empty($thing_list))
            return null;

        $bucket = [];

        $cte_type = from($thing_list[0], 'rec_cte');

        if ($cte_type == 'rec_direct_children') {
            return [
                $thing_list[0]['parent_id'] => [
                    'children' => $thing_list
                ]
            ];
        }

        foreach ($thing_list as $thing) {
            unset($rt);
            $rt = $thing;

            $rt['children'] = [];

            $thing_id = $rt['thing_id'];
            $parent_id = $rt['parent_id'];

            if (!array_key_exists($parent_id, $bucket)) {
                // if the parent isn't already in the bucket, then it probably means
                // the current $thing item is the first item in the list (i.e. the highest)

                $is_t3 = startsWith($thing_id, 't3_');

                if (!$is_t3 && $cte_type != 'rec_tree_parent') {
                    $bucket[$parent_id] = [
                        'children' => [&$rt]
                    ];
                }
                $bucket[$thing_id] = &$rt;
                continue;
            }

            $bucket[$parent_id]['children'][] = &$rt;
            $bucket[$thing_id] = &$rt;
        }

        $tree = reset($bucket);
        return [key($bucket) => $tree];
    }

    // SELF
    // ~~~~

    public static function id_from_link(string $link): ?string {
        static $_regex = '/^(?:(?:https?:)?\/\/)?(?:(?:www\.|[a-zA-Z]{2}\.)?reddit\.com\/'.
                        '(?:r\/[A-Za-z0-9_\-]{1,21}\/)?comments\/([A-Za-z0-9]+)(?:(?:\/[^\/]*\/)'.
                        '([A-Za-z0-9]+)?)?(?:\/.*)?|redd\.it\/([A-Za-z0-9]+)\/?)$/';
        $matches = Common::matcher($link, $_regex);
        $amount = count($matches);

        if ($amount == 2) {
            return 't1_'.$matches[1];
        } else if ($amount == 1) {
            return 't3_'.$matches[0];
        } else {
            return null;
        }
    }

    public static function get_subreddit($thing_id) {
        return db('Zion')->queryFirstField(
                    "SELECT subreddit_name FROM subreddit
                    WHERE id=(SELECT subreddit_id FROM reddit_thing WHERE thing_id=%s)", $thing_id);
    }

    public static function get_data($thing_id) {
        return db('Zion')->queryFirstRow(
                    "SELECT" . self::$select_fields .
                    "FROM (
                        reddit_thing rt
                        LEFT JOIN subreddit sr ON ((rt.subreddit_id = sr.id))
                    ) WHERE thing_id=%s",
                    $thing_id);
    }

    public static function get_attr($thing_id, $attr) {
        if ($attr == 'subreddit') {
            return self::get_subreddit($thing_id);
        }
        if (array_key_exists($attr, self::$alias_to_field_table)) {
            $attr = self::$alias_to_field_table[$attr];
        }

        return db('Zion')->queryFirstField(
                    "SELECT %b FROM reddit_thing WHERE thing_id=%s", $attr, $thing_id);
    }

    public static function get_media_info($thing_id) {
        if (empty($thing_id)) {
            return null;
        }
        return db('Zion')->query(
            "SELECT
                mi.media_author as author,mi.media_channel_id as id,
                mp.platform_name as platform,mi.media_url as url
            FROM (
                media_info mi
                LEFT JOIN media_platform mp
                ON (mi.media_platform_id = mp.id)
            )
            WHERE thing_id=%s",
            $thing_id);
    }

    public static function get_mod_actions($thing_id) {
        return db('Zion')->query(
            "SELECT id, mod as moderator, action, actionreason as reason, action_utc, description, modactionid
            FROM modlog WHERE thing_id IN %ls ORDER BY action_utc ASC", [(array) $thing_id]);
    }

    public static function fetch_submission_data($thing_id, string $property=null) {
        if (empty($thing_id) || !startsWith($thing_id, 't3_')) {
            return false;
        }

        $thing_id = remove_prefix($thing_id, 't3_');

        try {
            $response = user()->client()->fetch('https://oauth.reddit.com/comments/'.$thing_id.'/.json');

            if ($response['code'] < 200 || $response['code'] >= 300) {
                return false;
            }

            $thing = $response['result'][0]['data']['children'][0];

            if ($thing['kind'] !== 't3') {
                return false;
            }

            $data = $thing['data'];

            return isset($property) ? from($data, $property) : $data;
        } catch (\Exception $e) {
            return false;
        }
    }

    // CHILDREN
    // ~~~~~~~~

    public static function get_children($thing_id, $max_rec_depth = 0, $get_data = true) {
        $query = self::$rec_tree_children;
        if ($get_data == true) {
            $query .= self::$thing_union;
            $query .= self::$select_union;
        } else {
            $query .= self::$select_tree;
        }
        if (!empty($max_rec_depth) && is_int($max_rec_depth) && $max_rec_depth >= 1) {
            $query .= " WHERE rec_depth <= " . intval($max_rec_depth);
        }

        return db('Zion')->query($query, $thing_id);
    }

    public static function get_direct_children($thing_id, $get_data = true) {
        if ($get_data) {
            return db('Zion')->query(
               "SELECT ".self::$select_fields.",
                    1 as rec_depth,
                    'rec_direct_children' as rec_cte
                FROM (
                    reddit_thing rt
                    LEFT JOIN subreddit sr ON ((rt.subreddit_id = sr.id))
                )
                WHERE parent_id=%s", $thing_id);
        } else {
            return db('Zion')->query(
               "SELECT rt.thing_id, rt.parent_id, 1 as rec_depth, 'rec_direct_children' as rec_cte
                FROM reddit_thing rt
                WHERE parent_id=%s", $thing_id);
        }
    }

    // PARENTS
    // ~~~~~~~

    // note that the first item of the array returned is the thing passed in
    public static function get_context($thing_id, $max_rec_depth = 0, $get_data = true) {
        $query = self::$rec_tree_parent;
        if ($get_data == true) {
            $query .= self::$thing_union;
            $query .= self::$select_union;
        } else {
            $query .= self::$select_tree;
        }
        if (!empty($max_rec_depth) && is_int($max_rec_depth) && $max_rec_depth >= 1) {
            $query .= " WHERE rec_depth <= " . intval($max_rec_depth);
        }

        return array_reverse(db('Zion')->query($query, $thing_id));
    }

    public static function get_parent_id($thing_id) {
        return db('Zion')->queryFirstField(
            'SELECT parent_id FROM reddit_thing WHERE thing_id=%s', $thing_id);
    }

    public static function get_parent_subreddit($thing_id) {
        // parent and child should both be in the same subreddit
        // so might as well get the subreddit of the child rather than
        // spend extra time fetching the id of the parent
        return self::get_subreddit($thing_id);
    }

    public static function get_parent_attr($thing_id, $attr) {
        if ($attr == 'subreddit') {
            return self::get_parent_subreddit($thing_id);
        }
        if (array_key_exists($attr, self::$alias_to_field_table)) {
            $attr = self::$alias_to_field_table[$attr];
        }

        return db('Zion')->queryFirstField(
            "SELECT %b FROM reddit_thing WHERE thing_id=(SELECT parent_id FROM reddit_thing WHERE thing_id=%s)",
            $attr, $thing_id);
    }

    public static function get_parent($thing_id) {
        return db('Zion')->queryFirstRow(
            "SELECT" . self::$select_fields .
            "FROM (
                reddit_thing rt
                LEFT JOIN subreddit sr ON ((rt.subreddit_id = sr.id))
            ) WHERE thing_id=(SELECT parent_id FROM reddit_thing WHERE thing_id=%s)",
            $thing_id);
    }

}

RT::init();