<?php
namespace app\lib;

use app\lib\Common;
use app\lib\SubredditMethods;
use app\lib\providers\SubredditSourceProvider;
use app\lib\providers\ExplicitSubredditProvider;

/**
 * Subreddit object: represents zero, one, or more subreddits.
 * The collection of subreddits a Subreddit object can have are to be
 * considered immutable, only defined at the construction of the object.
 *
 * @property-read \app\lib\mixins\MediaBlacklistModule mediaban
 * @property-read \app\lib\mixins\BotbanModule botban
 * @property-read \app\lib\mixins\DomainModule domainban
 * @property-read \app\lib\mixins\DirtbagModule dirtbag
 * @property-read \app\lib\mixins\LogsModule logs
 * @property-read \app\lib\mixins\ModmailModule modmail
 * @property-read \app\lib\mixins\UsersModule users
 * @property-read \app\lib\SubredditConfig config
 *
 * @property-read int|int[] id
 * @property-read string title
 * @property-read string|string[] name
 * @property-read array row
 * @property-read array rows
 * @property-read array about
 * @property-read array moderators
 * @property-read array all_moderators
 * @property-read array post_moderators
 * @property-read array logged_moderators
 *
 * @method array id(bool $mapping=false)
 * @method string title()
 * @method string|string[] name()
 * @method array row()
 * @method array rows()
 * @method mixed column(string $prop, $new_value=null, bool $add_to_history=false)
 * @method array about()
 * @method array moderators(bool $mapping=false)
 * @method array all_moderators
 * @method array past_moderators
 * @method array logged_moderators(bool $mapping=false)
 */
class Subreddit implements \ArrayAccess, \Countable, \Serializable, \Iterator {
    /**
     * Registered modules map: from `{moduleName} => {classpath}`. Modules are accessed through
     * `$subreddit->{moduleName}` which provides an instantiation of the class at `{classpath}`
     * dedicated to that subreddit object with the subreddit object passed to the module object.
     * @var array
     */
    private static $REGISTERED_MODULES = [];

    /**
     * The SubredditSourceProvider for this Subreddit object.
     * @var SubredditSourceProvider
     */
    protected $provider;

    /**
     * String list of case-accurate subreddits within this subreddit object. There can be
     * zero, one, or many elements.
     * @var string[]
     */
    protected $subreddits = [];

    /**
     * Clone of `$subreddits` but all lowercase
     * @var string[]
     */
    protected $lower_subreddits = [];

    /**
     * Amount of subreddits in this Subreddit object.
     * @var int
     */
    protected $amount = 0;

    /**
     * The original value passed into the constructor to construct this Subreddit object.
     * @var mixed
     */
    protected $srIn = null;

    /**
     * Module instantiations for this Subreddit object. Modules are registered into the
     * `$REGISTERED_MODULES` static field, and are lazily loaded into Subreddit objects when
     * the relevant object is used.
     */
    protected $modules = [];

    /**
     * String representation of this Subreddit object.
     * @var string
     */
    protected $title = null;

    /**
     * Iterator position
     * @var int
     */
    protected $itr_pos = 0;

    /**
     * Construct the subreddit object.
     *
     * @param string|string[]|int[]|Subreddit $srIn
     * @param SubredditSourceProvider $provider
     * @throws IllegalSubredditException
     * @throws InvalidSubredditException
     * @throws UnknownSubredditException
     */
    public function __construct($srIn = null, SubredditSourceProvider $provider) {
        if ($srIn instanceof Subreddit) {
            // an existing `Subreddit` object can be passed in to clone that Subreddit object with
            // the same or a different source provider.
            if ($provider instanceof ExplicitSubredditProvider) {
                $srIn = $srIn->subreddits;
            } else {
                $srIn = $srIn->srIn;
            }
        }

        $this->srIn = $srIn;
        $this->provider = $provider;

        if ($provider instanceof ExplicitSubredditProvider) {
            $this->subreddits = (array) $srIn;
        } else {
            if (empty($srIn)) {
                $this->subreddits = [];
            } else if (is_array($srIn)) {
                $seen = [];

                $keys_to_remove = []; // for removing duplicates

                $str_bucket = [];
                $int_bucket = [];

                $seen_preOpCount = 0;

                foreach ($srIn as $sr) {
                    if (is_string($sr)) {
                        $sr = strtolower($sr);

                        if (isset($seen[$sr])) {
                            continue;
                        }

                        $str_bucket[] = $sr;
                    } else if (is_int($sr)) {
                        if (isset($seen[$sr])) {
                            continue;
                        }

                        $int_bucket[] = $sr;
                    }

                    $seen[$sr] = true;
                    $keys_to_remove[$sr] = true;
                    $seen_preOpCount++;
                }

                $seen_postOpCount = 0;

                if (!empty($str_bucket)) {
                    // true -> mapping from lower sr to real name
                    $str_bucket = SubredditMethods::_name($str_bucket, true);

                    foreach ($str_bucket as $lower => $realname) {
                        $seen[$lower] = $realname;
                        unset($keys_to_remove[$lower]);
                        $seen_postOpCount++;
                    }
                }

                if (!empty($int_bucket)) {
                    // true -> mapping from int ID to real name
                    $int_bucket = SubredditMethods::_nameFromID($int_bucket, true);

                    foreach ($int_bucket as $id => $realname) {
                        $seen[$id] = $realname;
                        unset($keys_to_remove[$id]);
                        $seen_postOpCount++;
                    }
                }

                if ($seen_preOpCount !== $seen_postOpCount) {
                    $this->provider->call_handlers('unknown', $this);
                }

                if (!empty($keys_to_remove)) {
                    foreach ($keys_to_remove as $key) {
                        unset($seen[$key]);
                    }
                }

                if (empty($int_bucket) || empty($str_bucket)) {
                    // if only string or only int bucket, then there's no possibility of duplicates at this point since
                    // we removed duplicates within the buckets
                    $this->subreddits = array_values($seen);
                    $this->lower_subreddits = array_keys($seen);
                } else {
                    // also have to call array_values again after unique to fix indices
                    $this->subreddits = array_values(array_unique(array_values($seen)));
                    $this->lower_subreddits = array_values(array_unique(array_keys($seen)));
                }
            } else if (is_string($srIn) && ($srIn[0] ?? null) === '@') {
                $srIn = IListUnpack($srIn);
                $this->subreddits = (array) SubredditMethods::_nameFromID($srIn);

                if (empty($this->subreddits)) {
                    $this->provider->call_handlers('unknown', $this);
                }
            } else if (is_int($srIn)) {
                $this->subreddits = (array) SubredditMethods::_nameFromID((array) $srIn);

                if (empty($this->subreddits)) {
                    $this->provider->call_handlers('unknown', $this);
                }
            } else if (is_string($srIn)) {
                [$this->subreddits, $this->title] =
                    Common::parse_multi_str($srIn,
                        array($this->provider, 'get'),
                        $this->provider->getName(),
                        array($this->provider, 'sanitizer')
                    );

                if ($this->subreddits === false) {
                    if ($this->title === 'illegal' || $this->title === 'unknown') {
                        $this->provider->call_handlers($this->title, $this);
                        return;
                    }

                    $this->provider->call_handlers('invalid', $this);
                    return;
                }
            } else {
                $this->provider->call_handlers('invalid', $this);
                return;
            }

            $this->lower_subreddits = to_lower($this->subreddits);

            if (!$this->provider->has($this->lower_subreddits, true)) {
                $this->provider->call_handlers('illegal', $this);
                return;
            }
        }

        $this->normal_init();
    }

    /**
     * Secondary constructor if `srIn` was considered valid and a `$this->subreddits` is available.
     *
     * This function is called from the main constructor and from unserialize().
     */
    private function normal_init(): void {
        // ----- Update amount
        $this->amount = count($this->subreddits);

        if (!isset($this->lower_subreddits)) {
            $this->lower_subreddits = to_lower($this->subreddits);
        }

        // ----- Default provider
        if (!isset($this->provider)) {
            $this->provider = subreddit_provider_global();
        }

        // ----- Default title handler
        if (!isset($this->title)) {
            $this->title = implode(', ', $this->subreddits) ?: 'None';
        }

        // ---- Initialize default modules
        $this->modules['DefaultMethods'] = new SubredditMethods($this);

        // ---- Run Subreddit object construction hooks
        $this->provider->call_handlers('success', $this);
    }

    public function getProvider(): SubredditSourceProvider {
        return $this->provider;
    }

    // Module registrar functions
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~
    private function _module($name) {
        if (array_key_exists($name, $this->modules)) {
            return $this->modules[$name];
        }

        if (($classPath = from(self::$REGISTERED_MODULES, $name)) !== null) {
            return $this->modules[$name] = new $classPath($name, $this);
        } else if (method_exists($this->modules['DefaultMethods'], $name)) {
            return $this->modules['DefaultMethods'];
        }

        return null;
    }

    public static function register_module(string $name, string $classpath) {
        self::$REGISTERED_MODULES[$name] = $classpath;
    }

    public static function unregister_module(string $name) {
        unset(self::$REGISTERED_MODULES[$name]);
    }

    // Magic object implementation
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public function __isset($name) {
        return $this->_module($name) !== null;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws UnknownSubredditModuleException
     */
    public function __call($name, $arguments) {
        $module = $this->_module($name);

        if (!isset($module)) {
            throw new UnknownSubredditModuleException($this, $name);
        } else if ($module->getRefName() == 'SubredditMethods') {
            return call_user_func_array(array($module, $name), $arguments);
        } else {
            return $module(...$arguments);
        }
    }

    /**
     * @param $name
     * @return mixed|null
     * @throws UnknownSubredditModuleException
     */
    public function __get($name) {
        $module = $this->_module($name);

        if (!isset($module)) {
            throw new UnknownSubredditModuleException($this, $name);
        } else if ($module->getRefName() == 'SubredditMethods') {
            return call_user_func_array(array($module, $name), []); // function as a property
        } else {
            return $module;
        }
    }

    public function __toString() {
        return $this->title;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getSrIn() {
        return $this->srIn;
    }

    // API access helpers
    // ~~~~~~~~~~~~~~~~~~
    public function api_directCall($resource_name, $method, $path = [], $query = []) {
        $path['subreddit'] = $this;
        return api_directCall($resource_name, $method, $path, $query);
    }

    public function api_call($resource_name, $method, $query = []) {
        return $this->api_directCall($resource_name, $method, [], $query);
    }

    // ArrayAccess implementation
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~
    public function offsetSet($offset, $value) {
        throw new \BadMethodCallException('Not supported (immutable object).');
    }

    public function offsetExists($offset) {
        return isset($this->subreddits[$offset]);
    }

    public function offsetUnset($offset) {
        throw new \BadMethodCallException('Not supported (immutable object).');
    }

    public function offsetGet($offset) {
        return isset($this->subreddits[$offset]) ? $this->subreddits[$offset] : null;
    }

    public function to_array(int $index = null) {
        return isset($index) ? $this->subreddits[$index] : $this->subreddits;
    }

    public function to_lower_array(int $index = null) {
        return isset($index) ? $this->lower_subreddits[$index] : $this->lower_subreddits;
    }

    public function to_id_array() {
        return (array) $this->id;
    }

    /**
     * Check if this Subreddit object contains the given subreddit(s).
     *
     * @param string|string[]|Subreddit $subreddits
     * @return bool true if contains all, false otherwise
     */
    public function has($subreddits): bool {
        if (is_string($subreddits)) {
            return in_array(strtolower($subreddits), $this->lower_subreddits);
        }
        return !array_diff(self::_static_to_lower_array($subreddits), $this->lower_subreddits);
    }

    /**
     * Check if this Subreddit object contains at least one of the given subreddit(s).
     *
     * @param string|string[]|Subreddit $subreddits
     * @return bool true if contains at least one, false otherwise
     */
    public function has_any($subreddits): bool {
        if (is_string($subreddits))
            return $this->has($subreddits);
        return count(array_intersect(self::_static_to_lower_array($subreddits), $this->lower_subreddits)) > 0;
    }

    public static function _static_to_lower_array($subreddits) {
        if ($subreddits instanceof Subreddit) {
            $subreddits = $subreddits->to_lower_array();
        } else {
            $subreddits = (array) to_lower($subreddits);
        }
        return $subreddits;
    }

    // Countable implementation
    // ~~~~~~~~~~~~~~~~~~~~~~~~

    public function count() {
        return $this->amount;
    }

    // Serializable implementation
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public function serialize() {
        return serialize([
            'subreddits' => $this->subreddits,
            'srIn'       => $this->srIn,
            'provider'   => SubredditSourceProvider::serialize($this->provider),
        ]);
    }

    public function unserialize($data) {
        $data = unserialize($data);

        $this->subreddits = $data['subreddits'];
        $this->srIn       = $data['srIn'];
        $this->provider   = SubredditSourceProvider::unserialize($data['provider']);

        $this->normal_init();
    }

    // Iterator implementation
    // ~~~~~~~~~~~~~~~~~~~~~~~

    /*
     * Order of operations when using a foreach loop:
     *
     * 1. Before the first iteration of the loop, Iterator::rewind() is called.
     * 2. Before each iteration of the loop, Iterator::valid() is called.
     * 3a. It Iterator::valid() returns false, the loop is terminated.
     * 3b. If Iterator::valid() returns true, Iterator::current() and
     *     Iterator::key() are called.
     * 4. The loop body is evaluated.
     * 5. After each iteration of the loop, Iterator::next() is called and we repeat from step 2 above.
     *
     * The loop isn't terminated until Iterator::valid() returns false or the loop body executes a
     * break statement.
     */

    function rewind() {
        $this->itr_pos = 0;
    }

    function current() {
        return $this->subreddits[$this->itr_pos];
    }

    function key() {
        return $this->itr_pos;
    }

    function next() {
        $this->itr_pos++;
    }

    function valid() {
        return $this->itr_pos < $this->amount;
    }
}

abstract class SubredditException extends \Exception {

    /**
     * @var Subreddit
     */
    protected $subreddit;

    /**
     * @var mixed
     */
    protected $detail;

    public function __construct(Subreddit $subreddit, string $message = null, $detail = null) {
        parent::__construct($message);
        $this->subreddit = $subreddit;

        // get provider unqualfied class name
        $providerUQName = basename(str_replace('\\', '/', get_class($subreddit->getProvider())));

        $this->detail = [
            'meta' => [
                'subreddits'    => $subreddit->to_array(),
                'amount'        => $subreddit->count(),
                'title'         => $subreddit->getTitle(),
                'srIn'          => $subreddit->getSrIn(),
                'provider'      => $providerUQName,
            ]
        ];

        $this->detail['moreinfo'] = $detail;
    }

    /**
     * Get the subreddit object.
     *
     * @return Subreddit
     */
    public function getSubreddit(): Subreddit {
        return $this->subreddit;
    }

    /**
     * Get error detail.
     *
     * @return mixed
     */
    public function getDetail() {
        return $this->detail;
    }

}

/**
 * Represents an attempt to access a Subreddit module that does not exist or has not been
 * registered.
 */
class UnknownSubredditModuleException extends SubredditException {

    /**
     * Construct the UnknownSubredditModuleException object.
     *
     * @param Subreddit $subreddit the Subreddit object
     * @param string    $name the attempted module ref name
     */
    function __construct(Subreddit $subreddit, string $name) {
        parent::__construct($subreddit,
            'the Subreddit object has no module by the reference name "' . $name . '"', $name);
    }

}

/**
 * Represents an attempt to construct a Subreddit that the SubredditSourceProvider does not
 * provide.
 */
class IllegalSubredditException extends SubredditException {

    /**
     * Construct the IllegalSubredditException object.
     *
     * @param Subreddit $subreddit the Subreddit object
     */
    function __construct(Subreddit $subreddit) {
        parent::__construct($subreddit,
            'subreddit object failed to initialize due to insufficient permissions (user does not '.
                'mod one or more of the specified subreddits)');
    }

}

/**
 * Represents an faulty Subreddit object initialization due to an argument with
 * invalid syntax being passed into the constructor.
 */
class InvalidSubredditException extends SubredditException {

    /**
     * Construct the InvalidSubredditException object.
     *
     * @param Subreddit $subreddit the Subreddit object
     */
    function __construct(Subreddit $subreddit) {
        parent::__construct($subreddit,
            'subreddit object failed to initialize due to a syntax error');
    }

}

/**
 * Represents an faulty Subreddit object initialization due to an unknown Subreddit being passed
 * into the constructor.
 */
class UnknownSubredditException extends SubredditException {

    /**
     * Construct the InvalidSubredditException object.
     *
     * @param Subreddit $subreddit the Subreddit object
     */
    function __construct(Subreddit $subreddit) {
        parent::__construct($subreddit,
            'subreddit object failed to initialize due to an unknown subreddit (subreddit either ' .
                'does not exist or does not have TSB)');
    }

}