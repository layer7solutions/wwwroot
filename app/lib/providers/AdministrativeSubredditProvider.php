<?php
namespace app\lib\providers;

use app\lib\Common;
use app\lib\Subreddit;
use app\lib\providers\SubredditSourceProvider;

class AdministrativeSubredditProvider extends SubredditSourceProvider {

    /**
     * AdministrativeSubredditProvider.
     */
    protected function __construct(string $name=null) {
        parent::__construct($name ?? 'all TSB subreddits', [
            'singleton' => true
        ]);
    }

    /**
     * @override
     */
    protected function init(): void {
        $this->add_subreddits(true);
    }

    /**
     * @override
     */
    public function has_category($category): bool {
        return true;
    }

}