<?php
namespace app\lib\providers;

use app\lib\Common;
use app\lib\Subreddit;
use app\lib\providers\SubredditSourceProvider;

class ControlledSubredditProvider extends SubredditSourceProvider {

    /**
     * @var string[]
     */
    private $input_list;

    /**
     * ControlledSubredditProvider.
     *
     * @param Subreddit|string[]|string $srIn
     */
    protected function __construct($srIn) {
        if ($srIn instanceof Subreddit) {
            $this->input_list = $srIn->to_array();

            if (empty($this->input_list)) {
                parent::__construct('No subreddits');
            } else {
                parent::__construct((string) $srIn);
            }
        } else {
            $this->input_list = (array) $srIn;

            if (empty($this->input_list)) {
                parent::__construct('No subreddits');
            } else {
                parent::__construct(implode(', ', $this->input_list));
            }
        }
    }

    /**
     * @override
     */
    protected function init(): void {
        $this->add_subreddits($this->input_list);
    }

    /**
     * @override
     */
    public function sanitizer(array $data, string $type): array {
        $res = parent::sanitizer($data, $type);

        if (is_array($res) && isset($res[0]) && $res[0] === false) {
            return $res;
        }

        if ($type === 'exclude' && !$this->has($res)) {
            return [false, 'illegal'];
        }

        return $res;
    }

}