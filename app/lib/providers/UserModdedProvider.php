<?php
namespace app\lib\providers;

use app\lib\providers\SubredditSourceProvider;
use app\lib\User;
use app\lib\UserModule;
use app\lib\Subreddit;

class UserModdedProvider extends SubredditSourceProvider {
    use UserModule;

    /**
     * UserModdedProvider.
     *
     * @param User $user the user upon which this provider operates
     */
    protected function __construct(User $user) {
        parent::__construct('subreddits you mod', [
            'ignore_unknown' => true
        ]);

        $this->parent   = $user;
        $this->username = $user->get_username();
    }

    /**
     * @override
     */
    protected function init(): void {
        if (!isset($this->username)) {
            return;
        }

        $this->add_subreddits(
            db('application')->queryFirstColumn(
                    'SELECT "name" FROM user_modded WHERE username=%s', $this->username) ?? []);
    }

    /**
     * @override
     */
    public function get(string $category='all'): array {
        return isset($this->username) ? parent::get($category) : [];
    }

    /**
     * @override
     */
    public function has_category($category): bool {
        return isset($this->username) && parent::has_category($category);
    }

    /**
     * @override
     */
    public function has($subreddits, bool $_isAlreadyProper=false): bool {
        return isset($this->username) && parent::has($subreddits);
    }

    /**
     * @override
     */
    protected function has_singular(string $lower_sr): bool {
        if (!isset($this->username)) {
            return false;
        }

        if ($this->parent->is_admin() && $lower_sr === 'thesentinelbot') {
            return true;
        }

        return parent::has_singular($lower_sr);
    }

    /**
     * @override
     */
    public function sanitizer(array $data, string $type): array {
        $res = parent::sanitizer($data, $type);

        if (is_array($res) && isset($res[0]) && $res[0] === false) {
            return $res;
        }

        if ($type === 'exclude' && !$this->has($res)) {
            return [false, 'illegal'];
        }

        return $res;
    }

}