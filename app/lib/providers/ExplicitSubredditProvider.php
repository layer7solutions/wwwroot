<?php
namespace app\lib\providers;

use app\lib\providers\AdministrativeSubredditProvider;

/**
 * This class is significant because of its name (which is used with `instanceof`) instead of its
 * contents.
 */
class ExplicitSubredditProvider extends AdministrativeSubredditProvider {

    protected function __construct() {
        parent::__construct('');
    }

}