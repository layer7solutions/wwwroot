<?php
namespace app\lib\providers;

use app\lib\Common;
use app\lib\Subreddit;
use app\lib\SubredditException;
use app\lib\InvalidSubredditException;
use app\lib\IllegalSubredditException;
use app\lib\UnknownSubredditException;

abstract class SubredditSourceProvider {

    /**
     * Associative array of column name => property name
     * @var array
     */
    protected static $categ_map;
    protected static $categ_selectSQL;

    /**
     * singletone cache of class name => instance
     * @var array
     */
    protected static $singletons;

    /**
     * arguments used to construct the provider
     * @var array
     */
    protected $construction_args;

    /**
     * source provider name
     * @var string
     */
    protected $name;

    /**
     * construction handlers
     * @var array
     */
    protected $handlers = [];

    /**
     * next handler id
     * @var int
     */
    protected $handlers_next_id = 0;

    /**
     * map $category => $subreddits
     * @var array
     */
    private $source = [];

    /**
     * array of $subreddit => true. Effectively a hashset.
     * @var array
     */
    private $sourceset = [];

    /**
     * @var bool
     */
    protected $did_init = false;

    /**
     * @var array
     */
    private $options;

    /**
     * Construct the SubredditSourceProvider.
     *
     * @param string $name the source provider name
     */
    protected function __construct(string $name, array $options=[]) {
        $this->name = $name;
        $this->options = $options + [
            'singleton' => false,
            'ignore_unknown' => false,
        ];

        $this->reset_subreddits();
    }

    /**
     * Static initializer. Called at the bottom of this file.
     */
    public static function static_init(): void {
        static $_didInit = false;
        if ($_didInit) {
            return;
        } else {
            $_didInit = true;
        }

        self::$categ_map = Common::categories(true);
        self::$categ_selectSQL = implode(', ', array_keys(self::$categ_map));
    }

    // FACTORY FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Get instance of the specified provider type.
     *
     * @param string    $class fully qualified class name for the SubredditSourceProvider
     * @param array     $args=[] arguments to pass to the provider constructor
     * @return          SubredditSourceProvider
     */
    public static function getInstance($class, $args=[]): SubredditSourceProvider {
        if (isset(self::$singletons[$class])) {
            return self::$singletons[$class];
        }

        $provider = new $class(... $args);

        if (!($provider instanceof SubredditSourceProvider)) {
            throw new \InvalidArgumentException('$class must be of SubredditSourceProvider type');
        }

        $provider->construction_args = $args;

        if ($provider->isSingleton()) {
            self::$singletons[$class] = $provider;
        }

        return $provider;
    }

    public static function serialize(SubredditSourceProvider $provider) {
        return serialize([get_class($provider), $provider->construction_args]);
    }

    public static function unserialize($data) {
        return self::getInstance(... unserialize($data));
    }

    // GENERAL FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Check if this provider is a singleton.
     *
     * @return bool
     */
    public function isSingleton(): bool {
        return $this->options['singleton'];
    }

    /**
     * Get the source provider name.
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * Get the source provider name.
     * @return string
     */
    public function __toString() {
        return $this->name;
    }

    // CONTROL FUNCTIONS
    // ------------------------------------------------------------------------------------------

    protected abstract function init(): void;

    public function reset_subreddits(): void {
        $this->source['all'] = [];

        foreach (array_values(self::$categ_map) as $category) {
            $this->source[$category] = [];
        }

        $this->sourceset = [];
        $this->did_init = false;
    }

    /**
     * Add some subreddits to this provider. This method does not support checking for unknown
     * subreddits, make sure the subreddits added are known. This method will add the given
     * subreddits to the provider in the same order that they are specified by the parameter.
     *
     * @param string[]|true     $srlist subreddits to add or `true` to add all subreddits. This
     *                          parameter is not case sensitive.
     */
    public function add_subreddits($srlist): void {
        if (empty($srlist)) {
            return;
        }

        // DB result not guaranteed to be in same order as $srlist
        $res = db('Zion')->query(
            sprintf(
                "SELECT subreddit_name::text, LOWER(subreddit_name::text) as lower, %s
                FROM subreddit %s",
                self::$categ_selectSQL,
                ($srlist === true)
                    ? 'ORDER BY subreddit_subscribers DESC NULLS LAST'
                    : 'WHERE subreddit_name in %ls_srlist'
            ),
            ['srlist' => $srlist]);

        if ($srlist === true) {
            foreach ($res as $row) {
                $this->__addSingleSubredditByRow($row);
            }
            return;
        }

        $dataset = [];

        foreach ($res as $row) {
            if (isset($this->sourceset[$row['lower']])) {
                continue; // already added
            }

            $dataset[$row['lower']] = $row;
        }

        foreach ($srlist as $lower_sr) {
            $lower_sr = strtolower($lower_sr);

            if (!isset($dataset[$lower_sr])) {
                if ($this->options['ignore_unknown']) {
                    continue;
                }
                throw new \LogicException(
                    'SubredditSourceProvider->add_subreddits does not support adding '.
                    'unknown subreddits ('.$lower_sr.')');
            }

            $this->__addSingleSubredditByRow($dataset[$lower_sr]);
        }
    }

    private function __addSingleSubredditByRow($row) {
        $this->sourceset[$row['lower']] = true;

        foreach (self::$categ_map as $categ_enabled => $category) {
            if ($row[$categ_enabled]) {
                $this->source[$category][] = $row['subreddit_name'];
            }
        }

        $this->source['all'][] = $row['subreddit_name'];
    }

    public function run_init(): void {
        if ($this->did_init) {
            return;
        } else {
            $this->did_init = true;
        }

        $this->init();
    }

    // IMPLEMENTATION FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Get a case-accurate list of all subreddits in this SubredditSourceProvider with the given
     * category.
     *
     * @param string $category='all'
     * @return string[]
     */
    public function get(string $category='all'): array {
        $this->run_init();

        $category = Common::category($category);

        if (!isset($category)) {
            return [];
        } else {
            return $this->source[$category] ?? [];
        }
    }

    /**
     * Check if this SubredditSourceProvider has any subreddits with the given category.
     *
     * @param string|string[] $category
     * @return bool
     */
    public function has_category($category): bool {
        $this->run_init();

        if (is_array($category)) {
            foreach ($category as $cat) {
                if ($this->has_category($cat)) {
                    return true;
                }
            }
            return false;
        }

        return !empty($this->source[Common::category($category)]);
    }

    /**
     * Check if this SubredditSourceProvider has the specified subreddits.
     *
     * @param string|string[]|Subreddit $subreddits
     * @return bool
     */
    public function has($subreddits, bool $_isAlreadyProper=false): bool {
        $this->run_init();

        if (!$_isAlreadyProper) {
            $subreddits = Subreddit::_static_to_lower_array($subreddits);
        }

        foreach ($subreddits as $lower_sr) {
            if (!$this->has_singular($lower_sr)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check if this SubredditSourceProvider has the specified subreddit.
     *
     * @param string    $subreddit
     * @return          bool
     */
    protected function has_singular(string $lower_sr): bool {
        return isset($this->sourceset[$lower_sr]);
    }

    /**
     * Multi-string parse sanitize function.
     *
     * @param string[]      $data
     * @param string        $type 'include' or 'exclude'
     * @return array        sanitized data; or `[false]` or `[false, 'errormsg']` if failed
     */
    public function sanitizer(array $data, string $type): array {
        $likeId = [];
        $likeName = [];

        foreach ($data as $item) {
            if (valid_int($item)) {
                $likeId[] = intval($item);
            } else {
                $likeName[] = $item;
            }
        }

        $ret = array_merge(
            (array) \app\lib\SubredditMethods::_name($likeName),
            (array) \app\lib\SubredditMethods::_nameFromID($likeId)
        );

        if (count($ret) !== count($data)) {
            return [false, 'unknown'];
        }

        return $ret;
    }

    // CONSTRUCTION HANDLERS
    // ------------------------------------------------------------------------------------------

    /**
     * Add a construction handler.
     *
     * @param string        $type
     * @param callable      $handler
     * @return int          the handler id for the just added handler
     */
    public function add_handler(string $type, callable $handler): int {
        if (!isset($this->handlers[$type])) {
            $this->handlers[$type] = [];
        }

        $this->handlers[$type][$this->handlers_next_id] = $handler;

        return $this->handlers_next_id++;
    }

    /**
     * Remove a construction handler by it's handler type and ID.
     *
     * @param string        $type
     * @param int           $handler_id the handler ID returned by `add_handler()`
     */
    public function remove_handler(string $type, int $handler_id): void {
        unset($this->handlers[$type][$handler_id]);
    }

    /**
     * Call handlers for the specified type.
     *
     * @param string        $type
     * @param Subreddit     $subreddit the Subreddit event source, passed as first arg to handlers
     * @return bool         true if any handlers were called for the given type, false otherwise
     * @throws IllegalSubredditException
     * @throws InvalidSubredditException
     * @throws UnknownSubredditException
     */
    public function call_handlers(string $type, Subreddit $subreddit) {
        if (empty($this->handlers[$type])) {
            switch ($type) {
                case 'success':
                    // do nothing here
                    break;
                case 'invalid':
                    throw new InvalidSubredditException($subreddit);
                    break;
                case 'illegal':
                    throw new IllegalSubredditException($subreddit);
                    break;
                case 'unknown':
                    throw new UnknownSubredditException($subreddit);
                default:
                    trigger_error("Unknown handler type", E_USER_ERROR);
                    break;
            }
            return false;
        }

        foreach ($this->handlers[$type] as $handler) {
            $handler($subreddit);
        }

        return true;
    }

}

SubredditSourceProvider::static_init();