<?php
namespace app\lib\data;

interface QueryParser {

    public function parse($query);

}