<?php
namespace app\lib\data;

class GeneralQueryParser implements QueryParser {
    const PARSE_VALID_OPS = ['=', '!=', '<', '>', '<=', '>=', '~', '~='];

    // this array is a set of the first characters of all valid
    // operators listed in 'PARSE_VALID_OPS'
    const PARSE_VALID_OPS_FIRSTCH = ['=', '!', '<', '>', '~'];

    const PARSE_REGEX_VALIDATE_KEY = '/^(\w\w?|\w[\w\.\:]+\w)$/';

    private $options = [
        'no_logic' => false,
        'no_logical_OR' => false,
    ];

    public function __construct($options = []) {
        $this->options = $options + $this->options;
    }

    // FIRST PASS
    // --------------------------------------------------------------------------------
    /**
     * @var GeneralQueryCursor
     */
    private $cursor = null;
    /**
     * @var GeneralQueryCursorGroup
     */
    private $cursor_group = null;
    private $result = [];
    private $current_token = '';

    private $in_op = false;
    private $in_str = false;
    private $in_token_value = false;
    private $in_token_paren = false;

    /**
     * @param bool $no_erase_result
     * @param bool $only_new_cursor
     */
    private function clear($no_erase_result = false, $only_new_cursor = false) {
        $new_cursor = new GeneralQueryCursor();

        $this->cursor = $new_cursor;

        if ($only_new_cursor) {
            $this->cursor_group->addCursor($new_cursor);
            return;
        }

        $this->cursor_group = new GeneralQueryCursorGroup([$new_cursor]);
        $this->current_token = '';

        $this->in_op = false;
        $this->in_str = false;
        $this->in_token_value = false;
        $this->in_token_paren = false;

        if ($no_erase_result == false)
            $this->result = [];
    }

    /**
     * @param $is_value_token
     *
     *   if false - Indicates the token being ended is a key.
     *              The token will be set to the current cursor's nextKey
     *   if true  - Indicates the token being ended is a value
     */
    private function end_token($is_value_token) {
        if (empty($this->current_token) && $this->cursor_group->size() >= 2) {
            $cursor0 = $this->cursor_group[$this->cursor_group->size() - 2];

            if ($cursor0->hasNextKey()) {
                $key = $cursor0->removeNextKey();

                // remove the last cursor so that 'cursor0' becomes the last cursor
                // then add the data of the previous last cursor to cursor0 with
                // nextKey
                $value = $this->cursor_group->removeLastCursor()->data;
                $this->cursor_group->getLastCursor()[$key] = $value;
            }

            // update the cursor to the actual last cursor of the group
            $this->cursor = $this->cursor_group->getLastCursor();
            return;
        }

        if ($is_value_token) {
            if ($this->cursor->hasNextKey()) {
                $this->cursor->removeNextKey($this->current_token);
            } else {
                $this->cursor->data[] = $this->current_token;
            }
        } else {
            $this->cursor->setNextKey($this->current_token);
        }

        $this->current_token = '';
    }

    /**
     * @param $logical_operator
     * @throws InvalidQueryException
     */
    public function check_group($logical_operator) {
        if ($logical_operator == 'AND') {
            return;
        }
        if ($logical_operator == 'OR' && $this->options['no_logical_OR'] === true) {
            throw new InvalidQueryException('logical OR not allowed');
        }

        while ($this->cursor_group->size() > 1 || !empty($this->current_token)) {
            $this->end_token(true);
        }

        $data = $this->cursor_group->getFirstCursor()->data;
        if (!empty($data)) {
            $this->result[] = $this->second_pass($data);
        }

        if ($logical_operator != 'END')
            $this->result[] = $logical_operator;

        $this->clear(true);
    }

    /**
     * @param $query
     * @return array
     * @throws InvalidQueryException
     */
    public function parse($query) {
        $this->clear();

        while (startsWith($query, '{') && endsWith($query, '}')) {
            $query = substr($query, 1, -1);
        }
        $charr = str_split($query);

        $this->is_balanced($charr); // throws exception if not

        $idx = 0;
        $skip = 0;
        $prevch = null;

        foreach ($charr as $ch) {
            try {
                if ($skip > 0) {
                    $skip--;
                    continue;
                } else if ($this->in_op) {
                    if (in_array($this->in_op . $ch, self::PARSE_VALID_OPS)) {
                        $this->in_op .= $ch;
                        continue;
                    } else if (in_array($this->in_op, self::PARSE_VALID_OPS)) {
                        if ($this->options['no_logic'] === true) {
                            throw new InvalidQueryException('operators not allowed');
                        }

                        $this->current_token .= '|' . $this->in_op; // we want operator to be part of the key
                        $this->in_op = false;

                        $this->end_token(false);
                        $this->in_token_value = true;
                        // no continue
                    } else {
                        $this->in_op = false;
                        // no continue
                    }
                } else if ($this->in_str && ($prevch == "\\" || $ch == "\\")) {
                    if ($prevch != "\\") {
                        continue;
                    }
                    switch ($ch) {
                        case "\\":
                            $this->current_token .= "\\";
                            break;
                        case "n":
                            $this->current_token .= "\n";
                            break;
                        case "r":
                            $this->current_token .= "\r";
                            break;
                        case "t":
                            $this->current_token .= "\t";
                            break;
                    }
                } else if ($this->in_str === $ch) {
                    $this->in_str = false;
                    continue;
                } else if ($this->in_str === false && ($ch === '"' || $ch === "'")) {
                    $this->in_str = $ch;
                    continue;
                }

                if ($this->in_str === false) {
                    if ($ch == ',' || $ch == ';') {
                        $this->end_token(true);
                        if (!$this->in_token_paren) {
                            $this->in_token_value = false;
                        }
                        if ($ch == ';') {
                            $this->check_group('OR');
                        }

                        continue;
                    } else if ($ch == '(' || $ch == '{') {
                        if ($ch == '{') {
                            if ($this->in_token_value) {
                                throw new InvalidQueryException(
                                    "expansions can't be used inside token values");
                            }
                            $this->end_token(false);
                        } else if ($ch == '(') {
                            // in_token_value is set to true when an operator is encountered
                            if ($this->in_token_value) {
                                $this->in_token_paren = true;
                            } else {
                                $this->check_group('PAREN_START');
                                continue;
                            }
                        }
                        $this->clear(true, true);
                        continue;
                    } else if ($ch == ')' || $ch == '}') {
                        if ($this->in_token_value) {
                            $this->end_token(true);
                        }

                        if ($ch == '}') {
                            if ($this->in_token_value) {
                                throw new InvalidQueryException(
                                    "expansions can't be used inside token values");
                            }
                            $this->end_token(true);
                        } else if ($ch == ')') {
                            if ($this->in_token_paren) {
                                $this->in_token_paren = false;
                                $this->in_token_value = false;
                            } else {
                                $this->check_group('PAREN_END');
                            }
                        }
                        continue;
                    } else if (in_array($ch, self::PARSE_VALID_OPS_FIRSTCH)) {
                        $this->in_op = $ch;
                        continue;
                    }
                }

                if ($this->in_str === false && ($ch === ' ' || $ch === "\t" || $ch === "\n" || $ch === "\r")) {
                    continue; // ignore whitespace if not in string
                }

                $this->current_token .= $ch;
            } finally {
                $prevch = $ch;
                $idx++;
            }
        }

        $this->check_group('END');

        return $this->result;
    }



    // SECOND PASS
    // --------------------------------------------------------------------------------

    /**
     * @param $x
     * @return array
     * @throws InvalidQueryException
     */
    private function second_pass($x) {
        $res = [];

        foreach ($x as $key => $value) {
            if (is_int($key)) {
                if (!preg_match(self::PARSE_REGEX_VALIDATE_KEY, $value)) {
                    if (empty($value))
                        continue;
                    throw new InvalidQueryException("invalid key - $value");
                }
                $res[] = $value;
                continue;
            }

            try {
                $original_key = $key;
                if (endsWith($key, '.')) {
                    $key = rtrim($key, '.');
                    $op = '.';
                } else {
                    $keysplit = explode('|', $key, 2);
                    if (count($keysplit) !== 2) {
                        $key = $keysplit[0];
                        $op = '.';
                    } else {
                        list($key, $op) = $keysplit;
                    }
                }
            } catch (\Exception $e) {
                if (empty($original_key))
                    continue;
                throw new InvalidQueryException("invalid key - $original_key");
            }

            if (!preg_match(self::PARSE_REGEX_VALIDATE_KEY, $key)) {
                throw new InvalidQueryException("invalid key - $key");
            }

            // handle special values
            switch ($value) {
                case 'NOW':
                    $value = time();
                    break;
            }

            // handle special operators and check if valid operator
            switch ($op) {
                case '.':
                    foreach ((array) $value as $value_key => $value_item) {
                        if (is_array($value_item)) {
                            $res = array_merge($res, $this->second_pass([
                                $key . '.' . $value_key => $value_item
                            ]));
                        } else {
                            $res[] = $key . '.' . $value_item;
                        }
                    }
                    break;
                default:
                    if ($this->options['no_logic'] === true) {
                        throw new InvalidQueryException('operators not allowed');
                    }
                    if (!in_array($op, self::PARSE_VALID_OPS)) {
                        throw new InvalidQueryException('unknown operator');
                    }
                    $res[] = [
                        'key' => $key,
                        'op' => $op,
                        'value' => $value,
                    ];
                    break;
            }
        }
        return $res;
    }

    /**
     * is_balanced($charr)
     *
     *  - Checks if parentheses, curly braces, and square brackets are balanced.
     *  - Also checks if strings are closed.
     *  - Symbols will be properly ignored if inside a string.
     *  - Quotes within a string will be properly ignored if preceded by a backslash.
     *
     * @param $charr array of characters of the query
     * @return bool
     * @throws InvalidQueryException
     */
    private function is_balanced($charr) {
        $in_str = false;

        $open = [
            '(' => 0,
            '{' => 0,
            '[' => 0,
        ];

        $closing_to_opening = [
            ')' => '(',
            '}' => '{',
            ']' => '[',
        ];

        $prev_ch_is_backslash = false;

        foreach ($charr as $ch) {
            if (!$prev_ch_is_backslash && ($ch == '"' || $ch == "'")) {
                if ($in_str) {
                    if ($in_str == $ch) {
                        $in_str = false;
                    }
                } else {
                    $in_str = $ch;
                }
                continue;
            }

            if (!$in_str) {
                if (array_key_exists($ch, $open)) {
                    $open[$ch]++;
                } else if (array_key_exists($ch, $closing_to_opening)) {
                    $opening_ch = $closing_to_opening[$ch];

                    if ($open[$opening_ch] < 0) {
                        $word = ([
                            '(' => 'PARENTHESES',
                            '{' => 'CURLY_BRACKET',
                            '[' => 'SQUARE_BRACKET',
                        ])[$opening_ch];
                        throw new InvalidQueryException("encountered unopened $word");
                    }

                    $open[$opening_ch]--;
                }
            }

            if ($ch == "\\") {
                $prev_ch_is_backslash = true;
            } else {
                $prev_ch_is_backslash = false;
            }
        }

        if ($in_str) {
            throw new InvalidQueryException("encountered unclosed string");
        }

        foreach ($open as $ch => $amount_open) {
            if ($amount_open !== 0) {
                $word = ([
                    '(' => 'PARENTHESES',
                    '{' => 'CURLY_BRACKET',
                    '[' => 'SQUARE_BRACKET',
                ])[$ch];
                throw new InvalidQueryException("encountered unclosed $word");
            }
        }
        return true;
    }
}

class GeneralQueryCursor implements \ArrayAccess {
    public $data;
    public $next_key;

    public function __construct($data = []) {
        $this->data = $data;
    }

    public function nextKey() {
        return $this->next_key;
    }

    public function setNextKey($new_next_key) {
        $this->next_key = $new_next_key;
    }

    public function hasNextKey() {
        return isset($this->next_key);
    }

    public function removeNextKey($value = null) {
        $key = $this->next_key;

        if (isset($value)) {
            $this->data[$key] = $value;
        }

        $this->next_key = null;
        return $key;
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }
}

class GeneralQueryCursorGroup implements \ArrayAccess {
    public $container;

    public function __construct($container = []) {
        $this->container = $container;
    }

    public function size() {
        return count($this->container);
    }

    public function addCursor($new_cursor) {
        $this->container[] = $new_cursor;
    }

    public function getLastCursor() {
        return $this->container[$this->size() - 1];
    }

    public function removeLastCursor() {
        return array_pop($this->container);
    }

    public function getFirstCursor() {
        return $this->container[0];
    }

    public function removeFirstCursor() {
        return array_shift($this->container);
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }
}