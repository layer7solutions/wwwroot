<?php
namespace app\lib\data;

abstract class QueryPlanner {
    protected $options;
    protected $TYPE_SPEC;

    public function __construct($options) {
        $this->options = $options;
    }

    public function type_spec($type) {
        if (!isset($this->TYPE_SPEC)) {
            $this->TYPE_SPEC = [
                'string' => [
                    // Validation:
                    'can_have_children' => false,
                    'meta_properites' => [ 'length' ],
                    'operators' => ['=', '!=', '~=', '~'],
                    'placeholder_type' => '%s',
                    // Creates the 'AND' component for the WHERE clause:
                    'value_map' => function($value) {
                        return (string) $value;
                    },
                ],
                'integer' => [
                    'can_have_children' => false,
                    'operators' => ['=', '!=', '<', '>', '>=', '<='],
                    'placeholder_type' => '%i',
                    'value_map' => function($value) {
                        return intval($value);
                    }
                ],
                'double' => [
                    'can_have_children' => false,
                    'operators' => ['=', '!=', '<', '>', '>=', '<='],
                    'placeholder_type' => '%d',
                    'value_map' => function($value) {
                        return doubleval($value);
                    }
                ],
                'object' => [
                    'can_have_children' => true,
                    'operators' => [],
                    'placeholder_type' => '%?',
                ],
                'list' => [
                    'can_have_children' => true,
                    'meta_properties' => [ 'count' ],
                    'operators' => ['=', '!=', '<', '>', '>=', '<=', '~='],
                    'placeholder_type' => '%l?',
                ],
            ];
        }
        return $this->TYPE_SPEC[$type];
    }

    public abstract function build($groups_by_clause);
    public abstract function fields();

    /**
     * @param $path
     * @return array
     * @throws InvalidQueryException
     */
    protected function find_spec($path) {
        $parts = preg_split('/([\.\:])/', $path, -1, PREG_SPLIT_DELIM_CAPTURE);
        $in_meta_property = false;

        $spec = ['children' => $this->fields()];
        $meta = null;

        foreach ($parts as $part) {
            if ($part == '.') {
                if ($in_meta_property) {
                    throw new InvalidQueryException('invalid property - "' . $path . '"');
                } else {
                    continue;
                }
            } else if ($part == ':') {
                $in_meta_property = true;
                continue;
            }

            if ($in_meta_property) {
                if (isset($meta)) {
                    throw new InvalidQueryException('can only have one meta property - "' . $path . '"');
                }
                if (!isset($spec['meta_properties'])) {
                    throw new InvalidQueryException('invalid property - "' . $path . '"');
                }
                if (!in_array($part, $spec['meta_properties'])) {
                    throw new InvalidQueryException('unknown meta property - "' . $path . '"');
                }
                $meta = $part;
            } else {
                if (!isset($spec['children'][$part])) {
                    throw new InvalidQueryException('unknown property - "' . $path . '"');
                }
                $spec = $spec['children'][$part];
            }
        }

        $type_spec = $this->type_spec($spec['type']);
        return [$spec, $meta, $spec['type'], $type_spec, $type_spec['placeholder_type']];
    }

    /**
     * @param $item
     * @return array
     * @throws InvalidQueryException
     */
    protected function preprocess_item($item) {
        if (is_string($item)) {
            $key    = $item;
            $op     = null;
            $value  = null;
        } else {
            $key    = $item['key'];
            $op     = $item['op'];
            $value  = $item['value'];
        }

        list(
            $spec,
            $meta_property,
            $type,
            $type_spec,
            $placeholder,
        ) = $this->find_spec($key);

        if (isset($value) && isset($type_spec['value_map'])) {
            try {
                $new_value = [];
                foreach ((array) $value as $v) {
                    $new_value[] = $type_spec['value_map']($v);
                }
                if ($type != 'list' && count($value = $new_value) == 1) {
                    $value = $value[0];
                }
            } catch(\Exception $e) {
                throw new InvalidQueryException(
                    'invalid value for property ' . $key . ', got: ' .
                        var_export($value, true) . '');
            }
        }

        if (is_array($value) && $spec['type'] != 'list') {
            $placeholder = '%l'.ltrim($placeholder, '%');
            $op = ' IN ';
        }

        $ret = [
            'column'        => $spec['column'] ?? $key,
            'op'            => $op,
            'value'         => $value,
            'placeholder'   => $placeholder,
            'type_spec'     => $type_spec,
            'spec'          => $spec,
            'type'          => $type,
            'meta_property' => $meta_property,
            'sql'           => false,
        ];

        if (isset($op) && isset($spec['resolve'])) {
            $res = $spec['resolve']($ret);

            if (isset($res['value']) && is_array($res['value']) && $spec['type'] != 'list') {
                $res['op'] = ' IN ';
                $res['placeholder'] = '%l'.ltrim($res['placeholder'] ?? $placeholder, '%');
            }

            $ret = array_merge($ret, $res);
        }

        return $ret;
    }

}