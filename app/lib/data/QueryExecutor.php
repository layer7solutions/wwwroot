<?php
namespace app\lib\data;

class QueryExecutor {
    private $options;
    private $db;

    public function __construct($options = []) {
        $options['readonly']        = $options['readonly']  ?? true;
        $options['db_name']         = $options['db_name']   ?? 'Zion';
        $options['db_username']     = $options['readonly']  ? PGSQL_READONLY_USERNAME : PGSQL_USERNAME;
        $options['db_password']     = $options['readonly']  ? PGSQL_READONLY_PASSWORD : PGSQL_PASSWORD;
        $options['db_timeout']      = 5000; // in milliseconds

        $this->db = db(
            $options['db_name'],
            $options['db_username'],
            $options['db_password']
        );
        $this->db->throw_exception_on_sql_error = true;

        $this->options = $options;
    }

    public function execute($query, $params) {
        $this->db->startTransaction(); // must use transactions in order to use statement_timeout
        $this->db->query('SET LOCAL statement_timeout=%i', $this->options['db_timeout']);
        try {
            $res = $this->db->query($query, $params);
            $this->db->commit();
            return new QueryExecutionResult('The query completed succesfully.', $res);
        } catch(\DBException $e) {
            if ($e->getCode() == 57014) {
                return new QueryExecutionResult('Query execution timed out.', null, $e);
            } else {
                return new QueryExecutionResult('An internal error occurred.', null, $e);
            }
        }
    }
}

class QueryExecutionResult {
    public $message; // the display message to client user
    public $result; // the result of the query execution (if success)
    public $error; // the error that occurred (if failure)

    public function __construct($message, $result = null, $error = null) {
        $this->message = $message;
        $this->result = $result;
        $this->error = $error;
    }
}