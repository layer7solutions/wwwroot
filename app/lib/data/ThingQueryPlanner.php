<?php
namespace app\lib\data;

use app\lib\data\InvalidQueryException;

class ThingQueryPlanner extends QueryPlanner {

    public function __construct($options = []) {
        parent::__construct($options);
    }

    /**
     * @return array
     */
    public function fields() {
        static $_fields;
        return isset($_fields) ? $_fields : ($_fields = [
            'thing_id' => [
                'type' => 'string',
                'desc' => "the thing id",
            ],
            'type' => [
                'type' => 'string',
                'desc' => "either 'comment' or 'post'",
                'resolve' => function($data) {
                    if ($data['value'] == 'post') {
                        $data['sql'] = 'rt.thing_title IS NOT NULL';
                    } else if ($data['value'] == 'comment') {
                        $data['sql'] = 'rt.thing_title IS NULL';
                    } else {
                        $data['sql'] = '';
                    }
                    $data['value'] = null;
                    return $data;
                },
            ],
            'subreddit' => [
                'type' => 'string',
                'desc' => "subreddit this thing was posted in",
                'column' => "subreddit_id",
                'resolve' => function($data) {
                    $data['placeholder'] = '%i';
                    $data['value'] = subreddit($data['value'])->id;
                    return $data;
                },
            ],
            'author' => [
                'type' => 'string',
                'desc' => "the user who posted this thing",
            ],
            'created_utc' => [
                'type' => 'integer',
                'desc' => "the unix timestamp at which this thing was posted",
                'resolve' => function($data) {
                    $data['placeholder'] = '%s';
                    if (is_numeric($data['value'])) {
                        $data['value'] = doubleval($data['value']);
                        if ($data['value'] < 0) {
                            $data['value'] = time() + $data['value'];
                        }
                        $data['value'] = date('Y-m-d H:i:s', $data['value']);
                    }
                    return $data;
                }
            ],
            'parent_post_id' => [
                'type' => 'string',
                'desc' => "parent post id, null if this thing is a post itself",
            ],
            'parent_thing_id' => [
                'type' => 'string',
                'desc' => "parent thing id, null if this thing is a post itself",
            ],
            'permalink' => [
                'type' => 'string',
                'desc' => "reddit link to this thing",
            ],
            'data' => [
                'type' => 'string',
                'desc' => "the content of this thing, null if the thing has no content",
            ],
            'title' => [
                'type' => 'string',
                'desc' => "the title of this thing, null if the thing is a comment",
            ],
            'url' => [
                'type' => 'string',
                'desc' => "the url this post points to, null if the thing is not a post",
            ],
            'mod' => [
                'type' => 'object',
                'children' => [
                    'actions' => [
                        'type' => 'list',
                        'children' => [
                            'moderator' => [
                                'type' => 'string',
                                'desc' => "moderator who performed the action",
                            ],
                            'action' => [
                                'type' => 'string',
                                'desc' => "action type (removelink,approvelink,removecomment,...)",
                            ],
                            'reason' => [
                                'type' => 'string',
                                'desc' => "action reason",
                            ],
                            'description' => [
                                'type' => 'string',
                                'desc' => "action description",
                            ],
                            'modactionid' => [
                                'type' => 'string',
                                'desc' => "reddit mod action id",
                            ],
                            'action_utc' => [
                                'type' => 'integer',
                                'desc' => "unix timestamp at which this action occurred",
                            ],
                        ]
                    ]
                ]
            ]
        ]);
    }

    /**
     * @param $groups_by_clause
     * @return array
     * @throws \app\lib\data\InvalidQueryException
     */
    public function build($groups_by_clause) {
        $clauses = [
            'fields' => [
                'data'    => '*',
                'prefix'  => 'SELECT',
                'sep'     => ', ',
            ],
            'from'   => [
                'data'    => 'reddit_thing rt',
                'prefix'  => 'FROM',
                'sep'     => '',
            ],
            'where'  => [
                'data'    => '',
                'prefix'  => 'WHERE',
                'sep'     => ' AND ',
            ],
            'order'  => [
                'data'    => 'rt.created_utc DESC',
                'prefix'  => 'ORDER BY',
                'sep'     => '',
            ],
            'limit'  => [
                'data'    => 25,
                'prefix'  => 'LIMIT',
                'sep'     => '',
            ],
        ];
        $group_separators = [
            'PAREN_START'   => ['(', false],
            'PAREN_END'     => [')', true],
            'OR'            => [' OR ', true],
        ];

        $sql_query = '';
        $sql_param = [];

        foreach ($groups_by_clause as $clause_type => $groups) {
            if (empty($clauses[$clause_type])) {
                continue; // ignore if invalid clause type
            }

            $buffer = '';

            $item_sep = $clauses[$clause_type]['sep'];

            foreach ($groups as $group) {
                // if the group is a string, then it denotes a separation of
                // operations in the clause string
                if (is_string($group)) {
                    list($group_sep, $is_group_end) = $group_separators[$group];

                    if (!$is_group_end) {
                        $buffer .= $item_sep;
                    }
                    $buffer .= $group_sep;
                    continue;
                } else foreach ($group as $item) {
                    if (!empty($buffer)) $buffer .= $item_sep; // TODO: check if group start

                    $stuff = $this->preprocess_item($item);
                    $column         = $stuff['column'];
                    $op             = $stuff['op'];
                    $value          = $stuff['value'];
                    $placeholder    = $stuff['placeholder'];
                    $sql            = $stuff['sql'];

                    if (isset($op) && isset($value)) {
                        $sql_param[] = $value;
                    }

                    if ($sql === false) {
                        $sql = $column;
                        if (isset($op)) {
                            $sql .= $op . $placeholder;
                        }
                    }

                    $buffer .= $sql;
                }
            }

            $clauses[$clause_type]['data'] = $buffer;
        }

        // compile clauses into full SQL query
        foreach ($clauses as $clause_data) {
            if (!empty($clause_data['data'])) {
                $sql_query .= $clause_data['prefix'].' ';
                $sql_query .= $clause_data['data'].' ';
            }
        }

        return [
            'query'  => $sql_query,
            'params' => $sql_param
        ];
    }
}