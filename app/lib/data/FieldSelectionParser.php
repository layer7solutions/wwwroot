<?php
namespace app\lib\data;

class FieldSelectionParser extends GeneralQueryParser {

    public function __construct($options = []) {
        parent::__construct([
            'no_logic' => true,
            'no_logical_OR' => true,
        ] + $options);
    }

    /**
     * parse field selection query
     *
     * @param $query
     * @return array|bool
     * @throws InvalidQueryException
     */
    public function parse($query) {
        if ($query === null || $query === '*') {
            return true;
        }
        $query = str_replace('(', '{', $query);
        $query = str_replace(')', '}', $query);
        return $this->third_pass(parent::parse($query));
    }

    public function check_field($model, string $field, bool $must_expand=false) {
        if ($model === true) {
            return 1;
        }

        $parts = explode('.', $field);
        $cursor = &$model;
        $i = 0;
        $i_last = count($parts) - 1;

        foreach ($parts as $part) {
            if (!isset($cursor[$part])) {
                return from($cursor, '__self__') === true;
            }
            $cursor = &$cursor[$part];
            $i++;
        }

        $ret = from($cursor, '__self__') === true ? 1 : 0;

        if (!$ret && $must_expand && count($cursor) > 2) {
            return 2;
        }

        return $ret;
    }

    public function normalize($model, array $data) {
        if ($model === true) {
            return $data;
        }

        $res = [];

        foreach ($data as $key => $value) {
            if (!isset($model[$key])) {
                continue;
            }
            if ($model[$key]['__self__'] === true) {
                $res[$key] = $value;
            } else {
                $res[$key] = $this->normalize($model[$key], $data[$key]);
            }
        }

        return $res;
    }

    /**
     * Third pass on query parsing.
     *
     * @param $x
     * @return array
     * @throws InvalidQueryException
     */
    private function third_pass($x) {
        if (count($x) !== 1 || !is_array($x[0])) {
            throw new InvalidQueryException('unknown parse failure');
        }

        $model = [];
        $x = $x[0];

        foreach ($x as $field) {
            $parts = explode('.', $field);
            $cursor = &$model;
            foreach ($parts as $part) {
                if (!isset($cursor[$part])) {
                    $cursor[$part] = [
                        '__self__' => false,
                        '__path__' => (isset($cursor['__path__']) ? $cursor['__path__'] . '.' : '') . $part,
                    ];
                }
                $cursor = &$cursor[$part];
            }
            $cursor = [
                '__self__' => true,
                '__path__' => $field,
            ];
        }

        return $model;
    }
}