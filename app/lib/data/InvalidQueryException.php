<?php
namespace app\lib\data;

class InvalidQueryException extends \Exception {

    public function __construct($message = '', $code = 0, \Exception $previous = null) {
        if (!empty($message)) {
            $message = ': ' . $message;
        }
        parent::__construct('Bad Query' . $message, $code, $previous);
    }

}