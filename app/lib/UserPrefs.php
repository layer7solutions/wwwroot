<?php
namespace app\lib;

use app\lib\Common;

class UserPrefs {
    use UserModule;

    const FIELDS = [
        'nightmode_enabled' => [
            'type' => 'boolean',
            'type_class' => null,
            'type_nullable' => false,
        ],
        'no_ipstore' => [
            'type' => 'boolean',
            'type_class' => null,
            'type_nullable' => false,
        ],
        'session_lifetime' => [
            'type' => 'integer',
            'type_class' => null,
            'type_nullable' => true,
        ],
        'user_timezone' => [
            'type' => 'string',
            'type_class' => null,
            'type_nullable' => false,
        ]
    ];

    private $tz_cache = null;
    private $tz_obj = null;

    public function __construct(User $user) {
        $this->parent   = $user;
        $this->username = $user->get_username();
    }

    public function can_change(string $field) {
        return isset(self::FIELDS[$field]);
    }

    public function can_access(string $field) {
        return isset(self::FIELDS[$field]);
    }

    public function get_all() {
        $ret = [];
        foreach (self::FIELDS as $name => $meta) {
            $ret[$name] = $this->get($name);
        }
        return $ret;
    }

    public function get(string $field) {
        return $this->parent->property($field);
    }

    public function get_composite_timezone() {
        if (isset($this->tz_cache)) {
            return $this->tz_cache;
        }
        return $this->tz_cache = tz_split_composite($this->parent->property('user_timezone'));
    }

    /**
     * Get the user's timezone as a PHP DateTimeZone object.
     *
     * @return \DateTimeZone
     */
    public function get_php_timezone() {
        if (isset($this->tz_obj)) {
            return $this->tz_obj;
        }
        return $tz_obj = new \DateTimeZone($this->get_php_name_timezone() ?: date_default_timezone_get());
    }

    /**
     * Get the GMT offset of the user's timezone.
     *
     * @return int|float the offset in hours, may be a float (e.g. `9.5`) for half-hour offsets
     */
    public function get_gmt_timezone_offset() {
        $tz = $this->get_php_timezone();
        $offset = $tz->getOffset(new \DateTime()); // gets offset in seconds
        $offset /= 3600; // 3600 seconds in an hour
        return $offset; // return offset in hours
    }

    /**
     * Get the abbreviation of the user's timezone, typically three letters,
     * e.g. "PST" for Pacific Standard Time.
     *
     * @return string
     */
    public function get_abrv_timezone() {
        $dt = new \DateTime();
        $dt->setTimezone($this->get_php_timezone());
        return xssafe($dt->format('T'));
    }

    /**
     * Get the PHP name of the user's timezone.
     * See http://php.net/manual/en/timezones.php for a list of PHP timezone names.
     *
     * @return string
     */
    public function get_php_name_timezone() {
        return $this->get_composite_timezone()['php_name'];
    }

    /**
     * Get a display name for the timezone in the format of "(GMT[<+|-><offset>]) <DisplayName>".
     * e.g.
     *   - "(GMT-10:00) Hawaii"
     *   - "(GMT-06:00) Central Time (US & Canada)"
     *   - "(GMT-08:00) Vancouver, BC"
     *   - "(GMT) London"
     *   - "(GMT+09:00) Tokyo"
     *   - "(GMT+09:30) Darwin"
     * You can get just the display name by splitting on the first instance of ") ".
     *
     * @return string
     */
    public function get_display_timezone() {
        return $this->get_composite_timezone()['display_name'];
    }

    public function set(string $field, $value) {
        if (!isset(self::FIELDS[$field])) {
            return false;
        }

        $t = gettype($value);

        if ($value === null) {
            if (!isset(self::FIELDS[$field]['type_nullable'])) {
                return false;
            }
            if (!self::FIELDS[$field]['type_nullable']) {
                return false;
            }
        }

        if ($value !== null && $t !== self::FIELDS[$field]['type']) {
            return false;
        }

        if ($t === 'object') {
            if (!isset(self::FIELDS[$field]['type_class'])) {
                return false;
            }

            $x = self::FIELDS[$field]['type_class'];
            if (!($value instanceof $x)) {
                return false;
            }
        }

        $this->parent->property($field, $value);
        return true;
    }

    public function setFromJSON(string $field, string $json) {
        $value = json_decode($json, true);
        return $this->set($field, $value);
    }
}