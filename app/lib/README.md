# app/lib

## Terminology

Our Domain Layer is comprised of four layers:

**Helper Layer:** general helper and utility functions.

**Service Layer:** a service does something. Typically services are a collection of static
functions or global functions, no objects. Services are lower-level implementations.

**Object Layer** the object layer is comprised of our core objects: Subreddit and User. Our website
provides tools for monitoring, automatic moderation, analytics, and lookup related to subreddits and
users, so everything stems off of these two objects.

**Module Layer:** a module is connected to a Subreddit or User. A module can wrap around and
abstract a service, multiple services, or it can have its own logic. Not all modules use any
services and not all services are associated with a particular module.

Modules are accessed by `Object->{module_name}` or `Object->{module_name}()`. Modules can be either
objects (object module), functions (function module), or both. The name of the module class is not
necessarily the name of the module.

## Structure

```
app/lib
 ├─ data/                                   (service)
 ├─ logs/                                   (service)
 ├─ mixins/                                 (modules)
 ├─ providers/
 │   ├─ AdministrativeSubredditProvider.php (service)
 │   ├─ ControlledSubredditProvider.php     (service)
 │   ├─ ExplicitSubredditProvider.php       (service)
 │   ├─ SubredditSourceProvider.php         (service)
 │   └─ UserModdedProvider.php              (service)
 │
 ├─ services/                               (services)
 ├─ session/                                (service)
 ├─ util/                                   (helpers)
 │
 ├─ Common.php                              (helpers)
 ├─ RT.php                                  (service)
 ├─ Subreddit.php                           (object)
 ├─ SubredditConfig.php                     (module)
 ├─ SubredditMethods.php                    (module)
 ├─ SubredditModule.php                     (helper)
 ├─ User.php                                (object)
 ├─ UserModdedSubs.php                      (module)
 ├─ UserModule.php                          (helper)
 └─ UserSession.php                         (module)
```

### data/

Services for subreddit thing data (WIP).

### logs/

Services for mod logs, abstracted by the `mixins/LogsModule`.

### mixins/

Contains Subreddit module classes.

### services/

Contains services that cannot be defined as a module of Subreddit or User.

### session/

Contains services for session implementation, abstracted by the `UserSession` module.

### util/

The `util/` folder has various utilities.

### Subreddit modules

Subreddits can have any number of modules registered to it. The `SubredditMethods.php` module is
registered by default. All other modules have to be manually registered, modules are registered at
`app/onload.php`. Subreddit modules must extend `SubredditModule.php` and are lazy loaded, meaning
modules are registered as known to the Subreddit class, but not the module objects are not
initialized until they are used.

### User modules

The User class has a defined set of modules, and is not open to having new modules registered. All
User modules must use the `UserModule.php` trait. There are two modules: `UserSession.php` and
`UserModdedSubs.php`, whose module names are `session` and `modded` respectively. Session is an
object module and modded is an object and function module.

## Subreddits, Subreddit Providers, and User Modded

The Subreddit class is a collection of reddit subreddits, it can have zero, one, or many subreddits
stored to it. The Subreddit object is considered immutable, meaning the collection of reddit
subreddits it has is constant, only defined through the constructor. You must create a new Subreddit
object if you want to represent a different set of subreddits.

### Subreddit object

A Subreddit object is constructed with two parameters: the input and the provider.

The **input** can be one of the following:

  - A string that follows our subreddit notation:

    https://layer7.solutions/developers/types#subreddit_notation

  - A string array of subreddits

  - An integer array of subreddit IDs matching the `id` column in our `Zion.subreddit` database
    table

  - An array that is a combination of strings and integers, this array will be filtered into two
    separate arrays for strings and integers, then it'll treat them independently then merge them

  - A string starting with "@" following our IListPack/IListUnpack functions.

The **provider** is an object of a class that extends `SubredditSourceProvider`. A Subreddit source
provider is something that provides the pool of reddit subreddits accessible to the Subreddit
constructor. Any subreddits not in the provider will not be recognized by the Subreddit construction
and will be omitted from the final collection and IllegalSubredditException may be thrown.

There are three types of source providers currently available:

  - **AdministrativeSubredditProvider:** provides a list of all subreddits that TSB mods, this
    is the global list, i.e. the administrative list unrestricted by any permissions

  - **ExplicitSubredditProvider:** this provider extends AdministrativeSubredditProvider, but
    for this provider the *input* must be a string array. The final collection will be set
    directly to the *input* parameter. This provider should never be used with user input. This
    provider is useful for input that we know is properly validated and normalized. For example,
    this provider is useful for copying Subreddit objects or creating subsets of existing Subreddit
    objects because a subset of a Subreddit object, which is validated and normalized, is also
    guaranteed to be validated and normalized.

  - **UserModdedProvider:** this provider is limited to subreddits that have TSB that a specific
    user mods. This provider is how we do permission checking to check if a User mods a subreddit.

  - **ControlledSubredditProvider:** this Subreddit provider is "controlled" by the code logic
    unlike Administrative and UserModded which get their pool of subreddits from the database. The
    set of subreddits this provider provides is passed into the constructor.

The Subreddit construction can throw three exceptions:

  - **IllegalSubredditException:** the subreddit input contains a subreddit that we recognize but is
    not provided by the provider

  - **InvalidSubredditException:** the subreddit input has a syntax error

  - **UnknownSubredditException:** the subreddit input contains a subreddit that we do not recognize

A subreddit being "recognized" means whether or not the AdministrativeSubredditProvider has
that subreddit.

Providers have a few functions: `get($category)`, `has_category($category)`, and `has($subreddits)`.
Using the `has($subreddits)` function is preferred to catching IllegalSubredditException.

The Subreddit object also has the functions: `has($subreddits)` and `has_any($subreddits)`.

### UserModdedSubs

**UserModdedSubs** extends the Subreddit class and uses UserModdedProvider. UserModdedSubs is a User
module, it's available as `$user_object->modded` which is equivalent to `$user_object->modded('all')`
or as `$user_object->modded($input)`, where `$input` follows the same rules as the input parameter
for Subreddit object construction.

The UserModdedSubs object also has the functions: `has($subreddits)`, `has_any($subreddits)`, and
`has_category($category)`.

Examples:

  - `user()->modded->has(['aww', 'videos])` will check if the user mods both aww and videos
  - `user()->modded->has_any(['aww', 'videos])` will check if the user mods either aww or videos
  - `user()->modded`, `user()->modded()`, and `user()->modded('all')` will return a UserModdedSubs
    object consisting of the collection of all TSB subreddits the user mods. The UserModdedSubs for
    all subreddits the user mods is cached and will return the same object. All other UserModdedSubs
    objects are not cached.

  - `user()->modded('all-aww')` will return a UserModdedSubs object consisting of the collection of
    all TSB subreddits the user mods, except for aww. This will throw IllegalSubredditException if
    the user does not mod aww

  - `user()->modded('all:logs-aww')` will return a UserModdedSubs object consisting of the
    collection of all TSB subreddits in the "logs" category that the user mods, except for aww. This
    will throw IllegalSubredditException if the user does not mod aww

## Global Shortcuts

  - `user()` returns the User object currently logged in user
  - `user($username)` returns the User object for a specific user
  - `subreddit($input)` returns a Subreddit object using AdministrativeSubredditProvider,
    meaning that using this global function provides no permission checking