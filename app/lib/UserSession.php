<?php
namespace app\lib;

use app\lib\services\WebsyncProcess;
use app\lib\UserModule;

class UserSession {
    use UserModule;

    /**
     * Reddit access tokens expire after 1 hour (3600 seconds). But mark the access tokens to expire
     * in 55 minutes instead so we have some leeway time.
     * @var int
     */
    const ACCESS_TOKEN_EXPIRY = 3600 - 300;

    /**
     * The Reddit OAuth scopes we want
     */
    const ACCESS_SCOPES = 'identity mysubreddits read';

    /**
     * The user's OAuth2 client object
     * @var \OAuth2Client\Client
     */
    private $oauth_client = null;

    public function __construct(User $user) {
        $this->parent = $user;
        $this->username = $user->get_username();
    }

    // STATE QUERY FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Get the user's refresh token.
     * @return string|null the refresh token, or null if not available
     */
    public function getRefreshToken(): ?string {
        return strval($this->parent->property('refresh_token'));
    }

    /**
     * Create an expiry timestamp for an access token that will be immediately created.
     * @return int
     */
    private static function getAccessTokenExpiryNow(): int {
        return time() + self::ACCESS_TOKEN_EXPIRY;
    }

    /**
     * Get the user's Reddit OAuth2 access token.
     *
     * @return string|null the access token, or null if no access token available or expired.
     */
    public function getAccessToken(): ?string {
        if (!isset($this->username)) {
            return null;
        }

        $tok = strval($this->parent->property('access_token'));
        $exp = intval($this->parent->property('access_token_expiry'));

        if (empty($tok) || $exp < time()) {
            // return null if expired
            return null;
        }

        return $tok;
    }

    /**
     * Check if the user is blacklisted from the website.
     *
     * @return bool true if blacklisted, false otherwise
     */
    public function isBlacklisted($only_web = false): bool {
        if (!isset($this->username)) {
            return false;
        }

        return db('application')->queryFirstField(
            "SELECT count(1) FROM user_service_ban WHERE username=%s " .
                ($only_web ? 'AND api_only=false' : ''),
            $this->username) !== 0;
    }

    /**
     * Get the user's session id.
     *
     * @return string
     */
    public function id(): string {
        $ret = $this->parent->property('session_id');
        if (empty($ret)) {
            $ret = db('application')->queryFirstField(
                "SELECT session_id FROM user_session WHERE username=%s
                ORDER BY session_expires DESC NULLS LAST",
                $this->username);
            $this->parent->property('session_id', $ret);
        }
        return strval($ret);
    }

    /**
     * This function should destroy any personal information linked to this user account. This
     * includes IP addresses, browser/platform info.
     *
     * Note: this function only does a single time wipe, in order to stop IP address storing on
     * this account all together, the `no_ipstore` property must be set to true.
     *
     * Note: this will have no effect on apache access/error logs, which will continue storing
     * IP addresses, albeit not linked to any specific accounts. But it won't be possible to take
     * an IP address from the access logs and figure which account it belongs to, because the only
     * place we store IP addresses linked to specific accounts are the `user_session` and the
     * `token_tracker` table.
     */
    public function destroy_personal_information() {
        if (!isset($this->username)) {
            return false;
        }
        session_get_handler()->delete_ipstore($this->username);
        return true;
    }

    // STATIC AUTHENTICATION AND DB-SETUP
    // ------------------------------------------------------------------------------------------

    /**
     * Switches the current session to the specified user. This function will also update the
     * current User and `user()` global function to the specified user.
     *
     * This function should work outside of the authorize process.
     *
     * @param string    $username the user to switch the current session to
     * @param array     $oauth contains the keys: client, access_token, access_token_expiry, refresh_token
     * @return bool     true if succesful, false otherwise
     */
    public static function switchSession(string $username, array $oauth): bool {
        // clean the slate
        session_get_handler()->renew();

        // set session variables
        $_SESSION['r_username'] = $username;
        $_SESSION['logged_in']  = true;
        $_SESSION['auth_utc']   = time();

        // commit (close) the session, then restart
        session_commit();
        session_start();

        User::reset_current_user();
        User::clear_user_list_cache();

        user()->clearInternalCache();

        // can't use either user()->properties() or user()->property() here because it can get
        // overriden by createOrUpdate()

        if (user()->get_username() !== $username) {
            return false;
        }
        if (user()->logged_in() !== true) {
            return false;
        }

        user()->session->createOrUpdate(true, $oauth);

        return true;
    }

    // USER CREATION/UPDATE
    // ------------------------------------------------------------------------------------------

    /**
     * This function is called upon every visit to the website by a logged-in user.
     *
     * @param bool $is_login_request =false true indicates if this visit was the redirect of a
     *              successful login, false indicates this is a normal access
     * @param array $oauth contains the keys: client, access_token, access_token_expiry, refresh_token
     * @return int  the last access UNIX timestamp before this one
     * @throws \OAuth2Client\Exception
     */
    public function createOrUpdate(bool $is_login_request = false, array $oauth = []): int {
        if (!isset($this->username)) {
            return 0;
        }

        if (isset($oauth['client'])) {
            $this->oauth_client = $oauth['client'];
        }

        $CURRTIME = time();

        $data = [
            'previous' =>
                $this->parent->properties(), // will return empty array if this is first login
            'initial' => [
                'username'              => $this->username,
                'last_access'           => $CURRTIME,
                'last_login'            => $CURRTIME,
                'first_login'           => $CURRTIME,
                'refresh_token'         => $oauth['refresh_token'] ?? $this->getRefreshToken(),
                'access_token'          => $oauth['access_token']  ?? $this->getAccessToken(),
                'access_token_expiry'   => $oauth['access_token_expiry'] ?? self::getAccessTokenExpiryNow(),
                'has_seen_tos'          => true,
                'session_id'            => session_id(),
            ],
            'update' => [
                'last_access'           => $CURRTIME,
                'session_id'            => session_id(),
            ]
        ];

        if ($is_login_request) {
            $data['update']['last_login'] = $CURRTIME;
            $data['update']['refresh_token'] = $data['initial']['refresh_token'];
            $data['update']['access_token'] = $data['initial']['access_token'];
            $data['update']['access_token_expiry'] = $data['initial']['access_token_expiry'];
        }

        else if (isset($data['previous']['access_token_expiry']) &&
                  $data['previous']['access_token_expiry'] <= time()) {
            // the access token has expired, so set it to null
            $data['update']['access_token'] = null;
        }

        if (isset($data['previous']['session_lifetime'])) {
            $ACCOUNT_EXP = $data['previous']['session_lifetime'];
            $CLIENT_EXP = get_cookie(SESSION_LIFETIME_COOKIE);

            // use fuzzy equals to compare int (ACCOUNT_EXP) and string (CLIENT_EXP)
            if ($CLIENT_EXP !== null && $ACCOUNT_EXP != $CLIENT_EXP) {
                // If cookie does not match account-level session_lifetime value, then fix the
                // cookie value to match
                set_cookie(SESSION_LIFETIME_COOKIE, strval($ACCOUNT_EXP));
                session_reset_expiry($ACCOUNT_EXP);
            }

            if ($CLIENT_EXP === null) {
                set_cookie(SESSION_LIFETIME_COOKIE, strval($ACCOUNT_EXP));
                session_reset_expiry($ACCOUNT_EXP);
            }
        }

        db('application')->insertUpdate('user_data', $data['initial'], $data['update']);

        // will be 0 if first login
        $last_access = from($data['previous'], 'last_access') ?? 0;

        // clear internal cache once more for good measure
        user()->clearInternalCache();

        if (empty($last_access)) {
            // this is the very first login for this user
            $this->parent->modded->update_all();
        } else {
            WebsyncProcess::process_flags($this->username);
        }

        return $last_access;
    }

    // USER AUTHENTICATION
    // ------------------------------------------------------------------------------------------

    /**
     * Log the user out of their session.
     *
     * @return bool true if successful, false otherwise or if no user was logged in
     */
    public function exit() {
        if (!isset($this->username)) {
            session_safe_destroy();
            return false;
        }

        // delete refresh/access tokens
        $this->parent->property('refresh_token', null);
        $this->parent->property('access_token', null);
        $this->parent->property('access_token_expiry', 0);

        // invalidate CSRF tokens belonging to the user, but only for this sessions to avoid
        // interrupting other sessions the user is still logged into on other devices
        session_csrf_token_destroy_by_user($this->username, $this->id());

        if ($this->username === from($_SESSION, 'r_username')) {
            // If this user object is the current session user, then use PHP library to
            // destroy the session
            session_safe_destroy();
        } else {
            // Otherwise, obtain the user's session id and use that to delete the session row in
            // database using the session save handler

            $sess_id = user($this->username)->session->id();

            if (!session_get_handler()->delete($sess_id)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Log a user in. This function will stop script execution upon function completion; 1) this
     * function may redirect the client to the Reddit authorization page 2) this function will
     * redirect the client to `$_COOKIE['cont_after_login']` if set or `/overview` otherwise upon
     * successful authorization 3) the function will redirect the client to an error page if an
     * error occurred at any point during the authorization process.
     */
    public static function authorize() {
        if (user()->logged_in()) {
            redirect_cont();
        }

        session_safe_destroy();

        if (isset($_REQUEST['error'])) {
            self::dispatch_auth_fail_page(strval($_REQUEST['error']));
        }

        if (isset($_REQUEST['cont'])) {
            set_cookie('cont_after_login', strval($_REQUEST['cont']), 3600);
        }

        // 0. Check if we have the authorization code
        //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        if (!isset($_REQUEST["code"])) {
            // If we don't have the "authorization_code", redirect the user to the Reddit
            // authorization page. They can choose Yes/No to authenticate.
            //
            //   Yes: the user is redirected back here with the "code" query parameter set
            //    No: the user is redirected back here with the "error" query parameter set

            redirect_ext(self::createOAuthClientObject()['client']->getAuthenticationUrl(
                OAUTH_AUTHORIZE_URL,
                OAUTH_REDIRECT_URL,
                [
                    "scope"     => self::ACCESS_SCOPES,
                    "state"     => safe_token(),
                    "duration"  => "permanent", // no expiry for refresh tokens
                ]
            ));
            exit;
        }

        // 1. Retrieve access and refresh token w/ authorization code
        //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        $oauth = self::createOAuthClientObject([
            "authorization_code",
            [
                "code"          => $_REQUEST["code"],
                "redirect_uri"  => OAUTH_REDIRECT_URL
            ]
        ]);

        if (empty($oauth['client']) || empty($oauth['access_token']) || empty($oauth['refresh_token'])) {
            self::dispatch_auth_fail_page(
                "We were unable to retrieve authentication tokens from Reddit. Try again later.");
        }

        // 2. Retrieve Reddit username
        //    ~~~~~~~~~~~~~~~~~~~~~~~~
        $userdata = $oauth['client']->fetch("https://oauth.reddit.com/api/v1/me.json");

        if (empty($userdata['result']['name']) || !is_string($userdata['result']['name'])) {
            self::dispatch_auth_fail_page(
                "We were unable to retrieve your username from the Reddit API. Try again later.");
        }

        // 3. Initialize the session
        //    ~~~~~~~~~~~~~~~~~~~~~~
        $oauth['access_token_expiry'] = self::getAccessTokenExpiryNow();

        if (!self::switchSession($userdata['result']['name'], $oauth)) {
            self::dispatch_auth_fail_page(
                'Failed to initialize your session. Try again later.');
        }

        // NB! The `user()` global function is now available to use for the newly logged in
        // user after this point.

        // 4. Check if user is allowed to login
        //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        if (user()->session->isBlacklisted(true)) {
            self::dispatch_auth_fail_page(
                'Your account is blacklisted from using this website. Contact the moderators ' .
                'at /r/Layer7 if you would like to appeal this ban');
        }

        // 5. Redirect the user to where they want to go
        //    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        $cont = get_cookie('cont_after_login');

        if (!empty($cont)) {
            delete_cookie('cont_after_login');
            redirect($cont);
        }

        redirect('/overview');
    }

    /**
     * Retrieve the OAuth2 client object for this user.
     *
     * @return \OAuth2Client\Client|null the client object or null if not available
     * @throws \OAuth2Client\Exception
     */
    public function get_client(): ?\OAuth2Client\Client {
        if (!isset($this->username)) {
            return null;
        }
        if (isset($this->oauth_client)) {
            return $this->oauth_client;
        }

        $access_token = $this->getAccessToken();

        if (!empty($access_token)) {
            return $this->oauth_client = self::createOAuthClientObject($access_token)['client'];
        }

        $refresh_token = $this->getRefreshToken();

        if (empty($refresh_token)) { // this should never happen
            $this->exit();
            redirect(current_url());
            exit;
        }

        $oauth = self::createOAuthClientObject([
            'refresh_token',
            [
                'refresh_token' => $refresh_token
            ]
        ]);

        $this->parent->property('access_token', $oauth["access_token"]);
        $this->parent->property('access_token_expiry', self::getAccessTokenExpiryNow());

        return $this->oauth_client = $oauth['client'];
    }


    // INTERNAL STATIC HELPERS
    // ------------------------------------------------------------------------------------------

    /**
     * Dispatch the designated error page for authorization failures. This function will stop
     * script execution.
     *
     * @param string $error the specific error message to display to the client
     */
    public static function dispatch_auth_fail_page(string $error) {
        // This function will exit the script, so no need to call die/exit when after
        // using this function below.
        session_safe_destroy();

        if ($error == 'access_denied') {
            redirect_cont();
        }

        view('net.loginfailed')
            ->meta('Login failed', 'page--login page--login_failed')
            ->stylesheets('ext/logininterstitial')
            ->use('error', $error)
            ->dispatchAndExit();
    }

    /**
     * A helper function for creating the OAuth2 Client object.
     *
     * @param string|array  $access_token_getopts string for the acess token itself or an array
     *                      in the format [$grant_type, array $parameters, array $extra_headers]
     * @return array        ['client' => \OAuth2Client\Client, 'access_token' => string, 'refresh_token' => string]
     * @throws \OAuth2Client\Exception
     */
    private static function createOAuthClientObject($access_token_getopts = null): array {
        autoload_oauth2_client();

        $client = new \OAuth2Client\Client(
            OAUTH_CLIENT_ID,
            OAUTH_CLIENT_SECRET,
            \OAuth2Client\Client::AUTH_TYPE_AUTHORIZATION_BASIC,
            CACERT_FILE
        );
        $client->setCurlOption(CURLOPT_USERAGENT, OAUTH_USER_AGENT);
        $client->setCustomHeaders([ 'User-Agent' => OAUTH_USER_AGENT]);

        $result = [
            'client' => $client,
            'access_token' => null,
            'refresh_token' => null,
        ];

        if (empty($access_token_getopts)) {
            return $result;
        }

        if (is_string($access_token_getopts)) {
            // if $access_token_getopts is a string, then assume it is the actual access token
            $result['access_token'] = $access_token_getopts;
        }

        else if (is_array($access_token_getopts)) {
            $response = $client->retrieveAccessToken(OAUTH_ACCESS_TOKEN_URL, ... $access_token_getopts);

            if (empty($response['result']['access_token'])) {
                return $result;
            }

            // The response should always have the access_token, whether or not it has the
            // refresh_token depends on the grant type passed through $access_token_getopts:
            // 'authorization_code' will respond with an access token and 'refresh_token' will not.

            $result['access_token'] = $response['result']['access_token'];
            $result['refresh_token'] = $response['result']['refresh_token'] ?? null;
        }

        if (isset($result['access_token'])) {
            $client->setAccessToken($result['access_token']);
            $client->setAccessTokenType(\OAuth2Client\Client::ACCESS_TOKEN_BEARER);
        }

        return $result;
    }

}