<?php
namespace app\lib;

use app\lib\util\CachedMap;

// Note: static_init() is called at the end of this file

final class SubredditMethods extends SubredditModule {

    /**
     * Cache prefix for ID to real subreddit name.
     * @var string
     */
    const PREFIX_ID_TO_NAME     = 'SrCache_ID2N_';

    /**
     * Cache prefix for lowercase subreddit name to ID.
     * @var string
     */
    const PREFIX_LCNAME_TO_ID   = 'SrCache_LC2ID_';

    /**
     * Cache prefix for lowercase subreddit name to real name.
     * @var string
     */
    const PREFIX_LCNAME_TO_NAME = 'SrCache_LC2N_';

    /**
     * Cache store.
     * @var array Key => CachedMap pair array.
     */
    private static $cache = [];

    public static function getThing() {
        return self::$cache['nameFromID'];
    }

    /**
     * Static initializer. Runs only once (calling it again will have no effect). This function
     * initializes any necessary cache resources.
     */
    public static function static_init() {
        static $_didInit = false;
        if ($_didInit) {
            return;
        } else {
            $_didInit = true;
        }

        // These 3 below don't ever need to be invalidated because the ID and real names of the
        // subreddits are guaranteed to never change. So no need to set expiry.
        self::$cache['nameFromID']  = new CachedMap(self::PREFIX_ID_TO_NAME);
        self::$cache['ID']          = new CachedMap(self::PREFIX_LCNAME_TO_ID);
        self::$cache['name']        = new CachedMap(self::PREFIX_LCNAME_TO_NAME);

        // this one is subject to change since moderators come and go
        self::$cache['moderators']  = new CachedMap();
    }

    /**
     * SubredditModule default constructor.
     *
     * @param string $subreddit the Subreddit object that this  instance operates under
     */
    public function __construct(Subreddit $subreddit) {
        parent::__construct('SubredditMethods', $subreddit);
    }

    /**
     * Get a list of case-sensitive subreddit names from a list of IDs
     *
     * @param int|int[]     $idlist the ID(s)
     * @param bool          $mapping if true, KV-pair array, otherwise a V-array
     *
     * @return string|string[]|array
     */
    public static function _nameFromID($idlist, $mapping = false) {
        $id_cache = self::$cache['ID'];

        $ret = self::$cache['nameFromID']->cached($idlist, function($keys, &$ret) use ($id_cache) {
            $r = db('Zion')->query(
                "SELECT
                   id,
                   subreddit_name::text as rc,
                   LOWER(subreddit_name::text) as lc
                 FROM
                   subreddit
                 WHERE
                   id IN %li",
                [$keys]);
            foreach ($r as $row) {
                $ret[$row['id']] = $row['rc'];
                $id_cache->put($row['lc'], $row['id']);
            }
        });

        return $mapping ? $ret : aor1(array_values($ret));
    }

    /**
     * Get the ID of all the subreddit(s) in the Subreddit object.
     *
     * @param bool              $mapping if true, KV-pair array, otherwise a V-array
     * @return int|int[]|array  an int if only one subreddit, array of ints if multiple, or a
     *                          KV-pair if $mapping is true
     */
    public function id($mapping = false) {
        return self::_id($this->subreddit->to_lower_array(), $mapping);
    }

    /**
     * Get the ID(s) of all the given subreddits.
     *
     * @param string|string[]   $subreddits the subreddit(s)
     * @param bool              $mapping if true, KV-pair array, otherwise a V-array
     * @return int|int[]|array  an int if only one subreddit, array of ints if multiple, or a
     *                          KV-pair if $mapping is true
     */
    public static function _id($subreddits, $mapping = false) {
        $nameFromID_cache = self::$cache['nameFromID'];

        $ret = self::$cache['ID']->cached(to_lower_array($subreddits), function($keys, &$ret) use ($nameFromID_cache) {
            $r = db('Zion')->query(
                "SELECT
                   id,
                   subreddit_name::text as rc,
                   LOWER(subreddit_name::text) as lc
                 FROM
                   subreddit
                 WHERE
                   subreddit_name IN %ls",
                [$keys]);
            foreach ($r as $row) {
                $ret[$row['lc']] = $row['id'];
                $nameFromID_cache->put($row['id'], $row['rc']);
            }
        });

        return $mapping ? $ret : aor1(array_values($ret));
    }

    /**
     * Get the title of these subreddit(s). This is a single string representing all the subreddits
     * in the Subreddit object in multi_str form.
     *
     * @return string
     */
    public function title() {
        return (string) $this->subreddit;
    }

    /**
     * Get the name(s) of these subreddit(s).
     *
     * @return string|string[]  a single string if only one subreddit or an array of strings if
     *                          multiple subreddits
     */
    public function name() {
        return self::_name($this->subreddit->to_lower_array());
    }

    /**
     * Get the name(s) of the given subreddit(s).
     *
     * @param string|string[]   $subreddits the subreddit(s)
     * @param bool              $mapping if true, KV-pair array, otherwise a V-array
     * @return string|string[]  a single string if only one subreddit or an array of strings if
     *                          multiple subreddits, or a KV-array if $mapping is true
     */
    public static function _name($subreddits, $mapping = false) {
        $ret = self::$cache['name']->cached(to_lower_array($subreddits), function($keys, &$ret) {
            $r = db('Zion')->query(
                "SELECT
                   subreddit_name::text as rc,
                   LOWER(subreddit_name::text) as lc
                 FROM
                   subreddit
                 WHERE
                   subreddit_name IN %ls",
                [$keys]);
            foreach ($r as $row) {
                $ret[$row['lc']] = $row['rc'];
            }
        });

        return $mapping ? $ret : aor1(array_values($ret));
    }

    /**
     * Get the database row of the first subreddit in this Subreddit object.
     *
     * @return array Column => Data pair array
     */
    public function row() {
        $query = 'SELECT * FROM subreddit WHERE subreddit_name=%s';
        return db('Zion')->queryFirstRow($query, $this->subreddit[0]);
    }

    /**
     * Get the database rows of all of the subreddits in this Subreddit object.
     *
     * @return array array of Column => Data pair arrays
     */
    public function rows() {
        $query = 'SELECT * FROM subreddit WHERE subreddit_name IN %ls_srlist';
        return db('Zion')->query($query, ['srlist' => $this->subreddit->to_array()]);
    }

    /**
     * Get or set a specific column for all of the subreddits in this Subreddit object.
     *
     * @param string    $prop the column property
     * @param mixed     $new_value (optional) new value for the column, if not provided then will
     *                  do a get operation with just the $prop parameter
     * @param bool      $add_to_history=false if true, logs the change to the database using the
     *                  current user as the change author
     * @return mixed    the column value if get operation, or void if set operation
     */
    public function column($prop, $new_value = null, $add_to_history = false) {
        $params = ['column' => $prop, 'srlist' => $this->subreddit->to_array()];

        $CURR_VALUES = array_map_to_assoc(
            db('Zion')->queryList(
                'SELECT subreddit_name, %b_column FROM subreddit WHERE subreddit_name IN %ls_srlist',
                    $params));

        if (func_num_args() == 1) {
            return count($this->subreddit) === 1 ? reset($CURR_VALUES) : $CURR_VALUES;
        } else {
            db('Zion')->update('subreddit', [$prop => $new_value],
                "subreddit_name IN %ls", $params['srlist']);
        }

        if ($add_to_history) {
            db('application')->insert('settings_history', aor1(array_map(
                function($subreddit) use ($CURR_VALUES, $prop, $new_value) {
                    $old_value = $CURR_VALUES[$subreddit];
                    return [
                        'subreddit'     => strtolower($subreddit),
                        'setting'       => $prop,
                        'new_value'     => is_bool($new_value) ? bool_str($new_value) : $new_value,
                        'old_value'     => is_bool($old_value) ? bool_str($old_value) : $old_value,
                        'changed_utc'   => time(),
                        'changed_by'    => user()->get_username(),
                    ];
                }, $params['srlist']
            )));
        }
    }

    /**
     * Subreddit about info. Only for the first subreddit in this Subreddit object,
     *
     * @return array
     * @throws \OAuth2Client\Exception
     */
    public function about() {
        static $_cache = [];

        if (array_key_exists($sr = strtolower($this->subreddit[0]), $_cache)) {
            return $_cache[$sr];
        }

        // If user mods the sub, get the data from 'modded_subs' instead since we
        // already have that so we don't need to make an unecessary request
        if (user()->modded->has($sr)) {
            $data = db('application')->queryFirstRow(
                "SELECT * FROM user_modded WHERE username=%s AND subreddit=%s",
                    user()->get_username(),
                    $sr);

            return $_cache[$sr] = $data;
        }

        $response = user()->client()->fetch("https://oauth.reddit.com/r/{$sr}/about.json");
        $data = $response['result']['data'];
        if (empty($data) || isset($data['children'])) {
            return $_cache[$sr] = array();
        }
        return $_cache[$sr] = self::data_pipe($data);
    }

    /**
     * Get a list of all CURRENT moderators in order from oldest to newest mod. But not guaranteed
     * to be in any particular order if there are multiple subreddits in this Subreddit object.
     *
     * @param bool      $mapping if true, KV-pair array, otherwise a V-array
     * @return array    list of moderators
     */
    public function moderators(bool $mapping = false) {
        $ret = self::$cache['moderators']->cached($this->subreddit->to_lower_array(), function($keys, &$ret) {
            $res = db('application')->query(
                "SELECT
                   moderator,
                   subreddit
                 FROM
                   about_moderators
                 WHERE
                   subreddit IN %ls
                 ORDER BY
                   rank ASC",
                [$keys]);

            foreach ($res as $row) {
                if (!isset($ret[$row['subreddit']])) {
                    $ret[$row['subreddit']] = [];
                }
                $ret[$row['subreddit']][] = $row['moderator'];
            }
        });

        return $mapping ? $ret : array_flatten(array_values($ret));
    }

    /**
     * Get a list of all moderators and anyone on file who's ever moderated the subreddit. This
     * includes current moderators, past moderators, and any admins who've done a mod action.
     *
     * @return string[]     list of moderators in order from oldest to newest for current moderators
     *                      and past moderators or admins at the end in no particular order
     */
    public function all_moderators() {
        static $_cache;
        if (isset($_cache)) {
            return $_cache;
        }

        $list = array_values(array_merge($this->moderators(), $this->logged_moderators()));
        return $_cache = array_values(array_unique($list));
    }

    /**
     * Get a list of all past moderators in no particular order.
     *
     * @return string[]  list of past moderators, not guaranteed to be in any particular order
     */
    public function past_moderators() {
        static $_cache;
        return $_cache ?? $_cache = array_values(array_diff($this->all_moderators(), $this->moderators()));

    }

    /**
     * Get a list of logged moderators in no particular order. "Logged" meaning they've had a mod
     * action. For example, if a current moderator has never done a single mod action then they
     * won't be returned by this function (but they will be returned by `moderators()` or
     * `all_moderators()`)
     *
     * @param bool      $mapping if true, returns a KV-pair array, otherwise returns a V-array
     * @return array    list of logged moderators
     */
    public function logged_moderators(bool $mapping = false) {
        $srIdList = $this->subreddit->to_id_array();

        $doQuery = function($subredditId) {
            $srComponent1 = 'WHERE subreddit_id = %i_subreddit';
            $srComponent2 = 'AND subreddit_id = %i_subreddit';

            if (empty($subredditId)) {
                if (!user()->admin()) {
                    return [];
                }
                $srComponent1 = '';
                $srComponent2 = '';
            }

            $query = "WITH RECURSIVE t(n) AS ("
            . " SELECT MIN(mod) FROM modlog {$srComponent1}"
            . " UNION"
            . " SELECT (SELECT mod FROM modlog WHERE mod > n {$srComponent2} ORDER BY mod LIMIT 1)"
            . " FROM t WHERE n IS NOT NULL"
            . ") SELECT n AS mod FROM t WHERE n IS NOT NULL";

            return db('Zion')->queryFirstColumn($query, [ 'subreddit' => $subredditId ]);
        };

        $srIn = strtolower($this->subreddit->getSrIn());
        if ($srIn === 'all' || startsWith($srIn, 'all:')) {
            if (user()->admin()) {
                return $doQuery(null);
            } else {
                $srIdList = user()->modded('all:logs')->to_id_array();
            }
        }

        $ret = [];

        if (empty($srIdList)) $ret;

        if ($mapping) {
            foreach ($srIdList as $srId) {
                $ret[self::_nameFromID($srId)] = $doQuery($srId);
            }
            return $ret;
        } else {
            $ret = [];
            foreach ($srIdList as $srId) {
                array_push($ret, ... $doQuery($srId));
            }
            return $ret;
        }
    }

    /**
     * Normalizes subreddit info returned by reddit for storage in our own database.
     *
     * @param array     $data subreddit info
     * @param bool      $implode_arrays whether or not to implode values that are arrays (w/ comma)
     * @return array
     */
    public static function data_pipe(array $data, bool $implode_arrays = false): array {
        $r = array(
            'name'                      => $data['display_name'],
            'id'                        => $data['name'],
            'type'                      => $data['subreddit_type'],
            'submit_type'               => $data['submission_type'],
            'subscribers'               => $data['subscribers'],
            'online'                    => $data['accounts_active'],
            'title'                     => $data['title'],
            'description'               => $data['public_description'],
            'sidebar'                   => null, // $data['description'],
            'over18'                    => $data['over18'],
            'created'                   => $data['created'],
            'created_utc'               => $data['created_utc'],
            'quarantine'                => $data['quarantine'],
            'icon_img'                  => $data['icon_img'],
            'user_is_moderator'         => $data['user_is_moderator'],
            'user_is_subscriber'        => $data['user_is_subscriber'],
            'user_is_banned'            => $data['user_is_banned'],
        );

        if (isset($data['mod_permissions'])) {
            $r['mod_permissions'] = $data['mod_permissions'];
        }

        if ($implode_arrays) {
            foreach ($r as $rk => $rv) {
                if (is_array($rv)) {
                    $r[$rk] = implode(',', $rv);
                }
            }
        }

        return $r;
    }
}

SubredditMethods::static_init();