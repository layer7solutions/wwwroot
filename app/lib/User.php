<?php
namespace app\lib;

use app\lib\UserModdedSubs;
use app\lib\UserPrefs;
use app\lib\UserSession;
use app\lib\providers\SubredditSourceProvider;
use app\lib\providers\UserModdedProvider;
use app\lib\Subreddit;
use app\lib\util\AdminTools;

/**
 * Class User
 *
 * @package app\lib
 * @method array real_modded
 * @method UserModdedSubs modded($subreddits)
 * @property-read UserSession session
 * @property-read UserPrefs prefs
 * @property-read UserModdedSubs modded
 */
class User implements \Serializable {
    private static $user_list = [];
    private static $current_user = null;

    private $username;
    private $cache = [];

    private $modded_provider;
    private $session_provider;

    /**
     * User object constructor. Use `User::user($username)` to create a new User object. This
     * constructor is private as so to keep a cache of Users constructed and avoid reconstructing
     * and causing unnecessary work for the same user.
     */
    private function __construct($username) {
        $this->username = $username;

        $this->session_provider = new UserSession($this);
        $this->modded_provider  = SubredditSourceProvider::getInstance(UserModdedProvider::class, [$this]);
        $this->prefs_manager    = new UserPrefs($this);
    }

    public function getModdedProvider() {
        return $this->modded_provider;
    }

    public function serialize() {
        return serialize($this->username);
    }

    public function unserialize($data) {
        $this->__construct(unserialize($data));
    }

    /**
     * Clears the internal cache for this user object.
     */
    public function clearInternalCache(): void {
        $this->cache = [];
    }

    /**
     * Get an associative array of all properties.
     *
     * @return array
     */
    public function properties(): array {
        return $this->cache['props'] ??
                ($this->cache['props'] =
                    !isset($this->username) ? []
                    : (db('application')->queryFirstRow(
                        'SELECT * FROM user_data WHERE username=%s', $this->username) ?? []));
    }

    /**
     * Get or set a property.
     *
     * @param string        $prop the property
     * @param mixed         $new_value (optional)
     * @param bool          $temporary=false (optional)
     * @return mixed|bool   the property value on get, or boolean success status on set
     */
    public function property(string $prop, $new_value=null, bool $temporary=false) {
        if (!isset($this->username)) {
            return null;
        }

        if (strtolower($prop) === 'username') {
            // username is the primary key, don't allow changing it.
            return $this->username;
        }

        if (func_num_args() === 2) {
            if (!array_key_exists($prop, $this->properties())) {
                return false;
            }

            if ($prop === 'session_lifetime' && (
                    !is_int($new_value)
                    || ($new_value < SESSION_LIFETIME_MINIMUM && $new_value !== 0)
                    || $new_value > SESSION_LIFETIME_MAXIMUM)) {
                return false;
            }

            if (!$temporary) {
                db('application')->update('user_data', [
                    $prop => $new_value,
                ], "username=%s", $this->username);

                if ($prop === 'no_ipstore' && $new_value === true) {
                    $this->session_provider->destroy_personal_information();
                }

                if ($prop === 'session_lifetime') {
                    set_cookie(SESSION_LIFETIME_COOKIE, strval($new_value));
                    session_reset_expiry($new_value);
                }
            }

            $this->cache['props'][$prop] = $new_value;

            return $temporary || db('application')->affectedRows() !== 0;
        }

        return from($this->properties(), $prop);
    }

    /**
     * Retrieve the user's Reddit OAuth2 client object, or null if not retrievable.
     *
     * @return \OAuth2Client\Client|null
     * @throws \OAuth2Client\Exception
     */
    public function client(): ?\OAuth2Client\Client {
        return $this->session_provider->get_client();
    }

    /**
     * Is the user logged in?
     *
     * @return bool true if the user is currently logged in, false otherwise
     */
    public function logged_in(): bool {
        return isset($this->username, $_SESSION['r_username'], $_SESSION['logged_in'])
            && $this->username === $_SESSION['r_username'] && $_SESSION['logged_in'] === true;
    }

    /**
     * Get user's Reddit username.
     *
     * @return string|null the user's reddit username, null if not logged in
     */
    public function get_username(): ?string {
        return $this->username;
    }

    /**
     * Returns the user's Reddit username or empty string if not retrievable.
     *
     * @return string
     */
    public function __toString() {
        return $this->username ?? '';
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws IllegalSubredditException
     * @throws InvalidSubredditException
     * @throws UnknownSubredditException
     */
    public function __call($name, $arguments) {
        switch ($name) {
            case 'session':
            case 'prefs':
                throw new \Exception(
                    'The "'.$name.'" user module is an object module, not a function module');
            case 'real_modded':
                if (!isset($this->username)) {
                    return [];
                }
                $ret = db('application')->query(
                    "SELECT * FROM user_modded WHERE username=%s
                    ORDER BY record_last_updated DESC NULLS LAST", $this->username);
                return $ret;
            case 'modded':
                if (empty($arguments) || from($arguments, 0) === 'all') {
                    return $this->__get($name);
                }

                $provider = $this->admin() ? subreddit_provider_global() : $this->modded_provider;

                return new UserModdedSubs($this, $arguments[0], $provider);
            default:
                throw new \Exception('Unknown user module');
        }
    }

    /**
     * @param $name
     * @return mixed
     * @throws IllegalSubredditException
     * @throws InvalidSubredditException
     * @throws UnknownSubredditException
     */
    public function __get($name) {
        switch ($name) {
            case 'session':
                return $this->session_provider;
            case 'prefs':
                return $this->prefs_manager;
            case 'modded':
                if (isset($this->cache['modded_all'])) {
                    return $this->cache['modded_all'];
                }

                $provider = $this->admin() ? subreddit_provider_global() : $this->modded_provider;

                return $this->cache['modded_all'] = new UserModdedSubs($this, 'all', $provider);
            default:
                throw new \Exception('Unknown user module');
        }
    }

    // ROLES
    // --------------------------------------------------------------------------------

    /**
     * Get or set the admin mode state.
     *
     * @param bool $new_state
     * @param bool $temporary
     * @return bool
     */
    public function admin(bool $new_state = null, bool $temporary = false): bool {
        if (!isset($this->username) || !$this->is_admin()) {
            return false;
        } else if (isset($new_state)) {
            if (!$temporary) {
                $this->property('admin_mode', $new_state);
            }
            unset($this->cache['modded_all']);
            return $this->cache['admin_mode'] = $new_state;
        } else {
            return $this->cache['admin_mode'] ??
                        ($this->cache['admin_mode'] = $this->property('admin_mode'));
        }
    }

    /**
     * Is the user a Layer7 admin (i.e. do they mod /r/Layer7 with full permissions)?
     *
     * @return bool true if admin, false otherwise
     */
    public function is_admin(): bool {
        return $this->cache['is_admin'] ??
            ($this->cache['is_admin'] =
                isset($this->username) &&
                (1 === db('application')->queryFirstField(
                    "SELECT COUNT(1) FROM user_modded WHERE username=%s AND
                    subreddit='layer7' AND mod_permissions='all'",
                    $this->username)
                )
            );
    }

    /**
     * Checks if the user has authority on /r/TheSentinelBot (which is the global blacklist).
     *
     * @return bool true if so, false otherwise
     */
    public function is_globalist(): bool {
        return $this->is_admin() || $this->modded->has('TheSentinelBot');
    }

    // STATIC FUNCTIONS
    // --------------------------------------------------------------------------------

    /**
     * Set the current user.
     *
     * @param string|User the username or User object of the user to be the current user
     */
    public static function set_current_user($username): void {
        self::$current_user = $username instanceof User ? $username->get_username() : $username;
    }

    /**
     * Get the user object for the specified Reddit username.
     *
     * @param string $username
     * @return User
     */
    public static function user(string $username = null): User {
        if (isset(self::$user_list[$username])) {
            return self::$user_list[$username];
        } else {
            try {
                return self::$user_list[$username] = new self($username);
            } catch(\Exception $e) {
                return self::$user_list[$username] = new self(null);
            }
        }
    }

    public static function clear_user_list_cache() {
        self::$user_list = [];
    }

    /**
     * Get the current session User object.
     *
     * @return User
     */
    public static function current_user(): User {
        return self::user(self::$current_user ?? $_SESSION['r_username'] ?? null);
    }

    /**
     * Resets the current user to the user currently defined in the session.
     */
    public static function reset_current_user(): void {
        self::$current_user = null;
    }
}