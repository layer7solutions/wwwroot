<?php

class SessionSaveHandler implements SessionHandlerInterface {
    private $db;
    private $lifetime;

    /**
     * Invoked whenever `session_start()` is called (before `read()`)
     *
     * @param string    $savePath the default session save path (ignored, not using filesystem)
     * @param string    $sessionName the session name
     * @return bool     true if success, false otherwise
     */
    public function open($savePath, $sessionName) {
        $this->db = \App::db('application');
        $this->lifetime = ini_get('session.gc_maxlifetime');
        return true;
    }

    /**
     * Invoked whenever `session_write_close()` is called
     *
     * @return bool true if success, false otherwise
     */
    public function close() {
        $this->gc($this->lifetime);
        return true;
    }

    /**
     * Invoked whenever `session_start()` is called (after `open()`)
     *
     * @param string    $id the session id
     * @return string   the session data, or an empty string if there's no data to read
     */
    public function read($id) {
        $res = $this->db->queryFirstField(
            'SELECT session_data FROM user_session WHERE session_id=%s AND session_expires > %i',
            $id, time());
        return !empty($res) ? $res : '';
    }

    /**
     * Invoked when `session_write_close()` is called
     *
     * @param string    $id the session id
     * @param string    $data the session data
     * @return bool     true if success, false otherwise
     */
    public function write($id, $data) {
        $expiry = time() + $this->lifetime;

        if (isset($_SESSION['EXPIRES'])) {
            $expiry = $_SESSION['EXPIRES'];
        }

        $username = $_SESSION['r_username'] ?? null;

        $no_ipstore = empty($username) ? false : db('application')->queryFirstField(
            'SELECT no_ipstore FROM user_data WHERE username=%s', $username);

        $browser = browser_info();
        $client_time = time();
        $client_ip = $no_ipstore ? null : get_client_ip();
        $client_platform = $no_ipstore ? 'n/a' : $browser->getPlatform();
        $client_browser = $no_ipstore ? 'n/a' : ($browser->getBrowser().' '.$browser->getVersion());

        $this->db->insertUpdate('user_session', array(
            'session_id'        => $id,
            'session_expires'   => $expiry,
            'session_data'      => $data,
            'session_site'      => SITE_NAME,
            'session_ipaddr'    => $client_ip,
            'session_platform'  => $client_platform,
            'session_browser'   => $client_browser,
            'session_lastutc'   => $client_time,
            'username'          => $username,
            'delete_token'      => safe_token(8),
        ), array(
            'session_expires'   => $expiry,
            'session_data'      => $data,
            'session_ipaddr'    => $client_ip,
            'session_platform'  => $client_platform,
            'session_browser'   => $client_browser,
            'session_lastutc'   => $client_time,
            'username'          => $username,
        ));

        return !empty($this->db->affectedRows());
    }

    /**
     * End the current session and start a new one - by setting the current session to expire in 1
     * minute, closing it, then starts a new session.
     *
     * This method is not part of the SessionHandlerInterface.
     */
    public function renew() {
        session_safe_destroy(); // Nuke the old session
        session_unset(); // Delete it's contents
        session_start(); // Load new session
        session_regenerate_id(); // Ensure it has a new id
        $_SESSION = []; // Ensure it has a clean slate
        session_csrf_token_clear_all(); // clear CSRF token states
    }

    /**
     * Invoked when `session_destroy()` or `session_regenerate_id(true)` is called.
     *
     * @param string    $id the session id
     * @return bool     true if success, false otherwise
     */
    public function destroy($id) {
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        return $this->delete($id);
    }

    /**
     * Delete some user's session.
     *
     * @param string    $id the session to delete
     * @return bool     true if successful, false otherwise
     */
    public function delete($id) {
        $this->db->delete('user_session', 'session_id=%s', $id);

        return !empty($this->db->affectedRows());
    }

    /**
     * Delete all of the user's sessions.
     *
     * @param string    $user the username
     * @return bool     true if successful, false otherwise
     */
    public function safe_delete_all(string $user) {
        $this->db->delete('user_session', 'username=%s', $user);

        return !empty($this->db->affectedRows());
    }

    /**
     * Delete all of the user's sessions except the specified session.
     *
     * @param string    $user the username
     * @param string    $id the session not to delete
     * @return bool     true if successful, false otherwise
     */
    public function safe_delete_all_others(string $user, string $id) {
        $this->db->delete('user_session', 'username=%s AND session_id!=%s', $user, $id);

        return !empty($this->db->affectedRows());
    }

    /**
     * Delete some user's session.
     *
     * @param string    $user the user_session
     * @param string    $delete_token the delete_token of the session to delete
     * @return bool     true if successful, false otherwise
     */
    public function safe_delete(string $user, string $delete_token) {
        $sess_user = $this->db->queryFirstField(
            'SELECT username FROM user_session WHERE delete_token=%s', $delete_token);

        if (empty($sess_user) || $sess_user !== $user) {
            return false;
        }

        $this->db->delete('user_session', 'delete_token=%s', $delete_token);

        return !empty($this->db->affectedRows());
    }

    /**
     * Get the username for the given session id.
     *
     * @param string    $id the session
     * @return string   the username of the user the session belongs to
     */
    public function get_user($id) {
        return $this->db->queryFirstField('SELECT username FROM user_session WHERE session_id=%s', $id);
    }

    /**
     * Get the delete token for the given session id.
     *
     * @param string    $id the session
     * @return string   the delete token
     */
    public function get_delete_token($id) {
        return $this->db->queryFirstField('SELECT delete_token FROM user_session WHERE session_id=%s', $id);
    }

    /**
     * Delete IP addresses linked to the specified username.
     */
    public function delete_ipstore(string $user) {
        $this->db->update('user_session', array(
            'session_ipaddr' => null,
            'session_platform' => null,
            'session_browser' => null,
        ), "username=%s", $user);
        $this->db->update('token_tracker', array(
            'ip_address' => null,
        ), "user_for=%s", $user);
    }

    /**
     * Purges old session data.
     *
     * @param int       $maxlifetime the GC max lifetime
     * @return bool     true if success, false otherwise
     */
    public function gc($maxlifetime) {
        $this->db->delete('user_session', 'session_expires < %i', time());
        return true;
    }

    /**
     * Get all sessions for the given user.
     *
     * @param string the username of the user to fetch sessions of
     * @return string array of user_session rows
     */
    public function get_sessions(string $username) {
        return $this->db->query('SELECT * FROM user_session WHERE username=%s', $username);
    }
}