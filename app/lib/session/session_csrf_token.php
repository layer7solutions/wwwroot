<?php

/**
 * Returns `true` if the CSRF token passed to the current HTTP request is valid, `false` if invalid
 * or if no CSRF token was provided.
 * @return bool
 */
function session_csrf_token_validity(bool $check_headers=false): bool {
    return session_csrf_token_validate(null, !$check_headers);
}

/**
 * Returns the newly generated CSRF token to pass back to the client, or `"null"` if no token was
 * generated.
 * @return string
 */
function session_csrf_token(): string {
    return session_csrf_token_generate();
}

/**
 * Echo out a hidden form input for the session_csrf_token
 */
function session_csrf_token_field() {
    ?><input type="hidden" name="token" value="<?php echo session_csrf_token(); ?>" /><?php
}

/**
 * Returns the token data for the CSRF token passed to the current HTTP request if valid, or an
 * empty array if invalid or if no CSRF token was provided.
 *
 * @param string    $property (optional) to retrieve a specific property within the token data, null
 *                  if not available
 * @return mixed
 */
function session_csrf_token_data(string $property=null, array $_data=null) {
    static $_cache = [];
    if (isset($property)) {
        return $_cache[$property] ?? null;
    }
    if (func_num_args() > 1) {
        if ($_data === null) {
            return $_cache = null;
        }
        unset($_data['token']);
        $_cache = $_data;
    }
    return $_cache;
}

/**
 * Reset CSRF-token-related states to their initial state.
 */
function session_csrf_token_clear_all() {
    session_csrf_token_data(null, null);
    session_csrf_token_validate(null, false, null);
    session_csrf_token_generate(null, null);
}

/**
 * If validating the CSRF token failed, this function will return a variable detailing the reason.
 * Will return `null` if the validation was successful.
 *
 * @return mixed
 */
function session_csrf_token_rejected_reason($_newReason=null) {
    static $_reason = null;
    return isset($_newReason) ? $_reason = $_newReason : $_reason;
}

/**
 * Checks if the CSRF token is valid. Use `session_csrf_token_validity()` to check the result.
 *
 * @param bool $no_headers=false if true, the X-Requested-With header and the HTTP referer won't
 *             be validated.
 * @return bool
 */
function session_csrf_token_validate(string $scope=null, bool $no_headers=false, ?bool $force_new=false): bool {
    static $_valid = null;

    if ($force_new === null) {
        $_token = null;
        return false;
    }

    if (isset($_valid) && $force_new === false) {
        return $_valid;
    }

    // -----  Check headers
    if (!$no_headers) {
        if (($_SERVER['HTTP_X_REQUESTED_WITH'] ?? null) !== 'XMLHttpRequest') {
            session_csrf_token_rejected_reason(["HEADER_CHECK_FAILED", "X-Requested-With"]);
            return $_valid = false;
        }
        if (parse_url($_SERVER['HTTP_REFERER'] ?? null, PHP_URL_HOST) !== SITE_HOST) {
            session_csrf_token_rejected_reason(["HEADER_CHECK_FAILED", "Referer"]);
            return $_valid = false;
        }
    }

    // ----- Preparation
    $token_received = $_SERVER['HTTP_X_CSRF_TOKEN'] ?? $_REQUEST['token'] ?? 'null';
    $token_parts = explode('|', $token_received, 2);
    $token_query = "SELECT * FROM token_tracker WHERE token=%s";

    // ----- Validate token
    if (from($token_parts, 1) !== safe_hash(from($token_parts, 0), USERAUTH_SECRET_KEY)) {
        session_csrf_token_rejected_reason(["HASH_CMP_FAIL"]);
        return $_valid = false;
    }

    // ----- Get and check token data
    $token_data = db('application')->queryFirstRow($token_query, $token_received);

    if (empty($token_data) || empty($token_data['user_for'])) {
        session_csrf_token_rejected_reason(["NO_DATA", "Token was verified but no associated data was found"]);
        return $_valid = false;
    }
    if (!empty($token_data['ip_address']) && !user()->property('no_ipstore') && get_client_ip() !== $token_data['ip_address']) {
        session_csrf_token_rejected_reason(["IP_CMP_FAIL", get_client_ip()]);
        return $_valid = false;
    }
    if (!empty($token_data['expires']) && $token_data['expires'] < time()) {
        session_csrf_token_rejected_reason(["TOKEN_EXPIRED", $token_data['expires']]);
        return $_valid = false;
    }

    if (isset($scope)) {
        $scopelist = explode(' ', $token_data['scope'] ?? '');
        if (!in_array($scope, $scopelist)) {
            session_csrf_token_rejected_reason(["SCOPE_CMP_FAIL", $scope, $scopelist]);
            return $_valid = false;
        }
    }

    // ----- The token has been used, set it to expire in 60 seconds
    db('application')->update('token_tracker', [
        'expires' => time() + 60
    ], "token=%s", $token_received);

    // ----- Run a clean up of tokens older than 1 week
    session_csrf_token_gc();

    // ----- Set session variables
    session_csrf_token_data(null, $token_data);
    return $_valid = true;
}

/**
 * Clean up expired tokens.
 */
function session_csrf_token_gc(): void {
    db('application')->delete('token_tracker', "created<%i OR expires<%i", time() - 604800, time());
}

/**
 * Regenerate the session CSRF token. Use `session_csrf_token()` to get the result.
 */
function session_csrf_token_generate(string $scopes=null, ?bool $force_new=false) {
    static $_token = null;

    if ($force_new === null) {
        $_token = null;
        return 'null';
    }

    if (isset($_token) && $force_new === false) {
        return $_token;
    }

    if (!user()->logged_in()) {
        return $_token = 'null';
    }

    $nonce = safe_token(16);
    $hash  = safe_hash($nonce, USERAUTH_SECRET_KEY);
    $token = $nonce . '|' . $hash;

    db('application')->insert('token_tracker', array(
        'token'         => $token,
        'created'       => time(),
        'user_for'      => user()->get_username(),
        'ip_address'    => user()->property('no_ipstore') ? null : get_client_ip(),
        'scope'         => $scopes,
        'session_id'    => session_id(),
    ));

    return $_token = $token;
}

/**
 * Destroy a specific token.
 *
 * @param string $token
 */
function session_csrf_token_destroy(string $token) {
    db('application')->delete('token_tracker', 'token=%s', $token);
}

/**
 * Destroy all tokens belonging to a user.
 *
 * @param string $user
 * @param string $session_id (optional) if specified, limit to only a specific session of the user
 */
function session_csrf_token_destroy_by_user(string $user, string $session_id=null) {
    if (isset($session_id)) {
        db('application')->delete('token_tracker', 'user_for=%s AND session_id=%s',
            $user, $session_id);
    } else {
        db('application')->delete('token_tracker', 'user_for=%s', $user);
    }
}

/**
 * Destroy all tokens belonging to an ip address.
 *
 * @param string $ip_address
 */
function session_csrf_token_destroy_by_ipaddress(string $ip_address) {
    db('application')->delete('token_tracker', 'ip_address=%s', $ip_address);
}