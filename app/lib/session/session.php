<?php

require APP_ROOT.'lib/session/SessionSaveHandler.php';
require APP_ROOT.'lib/session/session_csrf_token.php';

const SESSION_LIFETIME_DEFAULT = 2678400; // 1 month
const SESSION_LIFETIME_MINIMUM = 600; // 10 minutes
const SESSION_LIFETIME_MAXIMUM = 31536000; // 1 year
const SESSION_LIFETIME_COOKIE  =  SITE_NAME . 'sessexp';

/**
 * Gets the SessionSaveHandler singleton.
 *
 * @return SessionSaveHandler
 */
function session_get_handler(): SessionSaveHandler {
    static $_handler = null;
    return isset($_handler) ? $_handler : ($_handler = new SessionSaveHandler());
}

/**
 * Safely and quietly attempt to destroy the current session. Does not error if there is no
 * session to destroy or if session is already destroyed.
 *
 * @return bool true if a session was destroyed, false otherwise
 */
function session_safe_destroy(): bool {
    try {
        delete_cookie(SESSION_LIFETIME_COOKIE);
    } catch(\Exception $e) {
        // pass
    }
    if (session_status() === PHP_SESSION_ACTIVE) {
        try {
            session_destroy();
        } catch (\ErrorException $e) {
            return false;
        }
        return true;
    }
    return false;
}

/**
 * Configures and starts the session.
 */
function session_init(): void {
    $EXPIRY = SESSION_LIFETIME_DEFAULT;

    if (has_cookie(SESSION_LIFETIME_COOKIE)) {
        $tmp_exp = get_cookie(SESSION_LIFETIME_COOKIE);

        if (valid_int($tmp_exp)) {
            $tmp_exp = intval($tmp_exp);

            if ($tmp_exp === 0) {
                $EXPIRY = 0;
            } else if ($tmp_exp < SESSION_LIFETIME_MINIMUM || $tmp_exp > SESSION_LIFETIME_MAXIMUM) {
                delete_cookie(SESSION_LIFETIME_COOKIE);
            } else {
                $EXPIRY = $tmp_exp;
            }
        }
    }

    $algos = hash_algos();
    if (in_array('sha512', $algos)) {
        ini_set('session.hash_function', 'sha512');
    } else if (in_array('sha256', $algos)) {
        ini_set('session.hash_function', 'sha256');
    }

    ini_set('session.hash_bits_per_character', 6);
    ini_set('session.use_trans_sid', 0);
    ini_set('session.cookie_lifetime', SESSION_LIFETIME_MAXIMUM); // client side cookie max lifetime

    if (is_ssl()) {
        ini_set('session.cookie_secure', 1); // only on https
    }

    ini_set('session.cookie_httponly', 1);
    ini_set('session.use_cookies', 1);
    ini_set('session.use_only_cookies', 1);
    ini_set('session.cache_expire', 30);
    ini_set('default_socket_timeout', 60);

    ini_set('session.entropy_file', '/dev/urandom');
    ini_set('session.entropy_length', 256);

    // make sure `session.gc_maxlifetime` is set in the `php.ini` because on debian-based systems
    // changing gc_maxlifetime at runtime may have no effect
    ini_set('session.gc_maxlifetime', SESSION_LIFETIME_MAXIMUM); // server side cookie lifetime

    $session_name = session_name(SITE_NAME.'sessid');
    session_set_cookie_params($EXPIRY, '/', SITE_HOST);

    $handler = session_get_handler();
    session_set_save_handler($handler, true);
    session_register_shutdown();
    session_start();
}

function session_reset_expiry(int $new_expiry) {
    if (session_status() !== PHP_SESSION_ACTIVE || !isset($_COOKIE[SITE_NAME.'sessid'])) {
        return;
    }

    $id = session_id(); // $_COOKIE[SITE_NAME.'sessid'];
    $params = session_get_cookie_params();

    delete_cookie(SITE_NAME.'sessid');
    set_cookie(SITE_NAME.'sessid', $id, $new_expiry, $params['path'] ?? '/', $params['domain'] ?? SITE_HOST);
}