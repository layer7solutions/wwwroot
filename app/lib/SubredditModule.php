<?php
namespace app\lib;

abstract class SubredditModule {
    protected $subreddit;
    protected $name;

    public function __construct(string $name, Subreddit $subreddit) {
        $this->name = $name;
        $this->subreddit = $subreddit;
    }

    public function getRefName(): string {
        return $this->name;
    }

}