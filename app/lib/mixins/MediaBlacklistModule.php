<?php
namespace app\lib\mixins;

use app\lib\SubredditModule;
use app\lib\Common;
use app\lib\util\MediaTools;

class MediaBlacklistModule extends SubredditModule {

    const REPORTS_LIMIT = 2000;
    const REPORTS_DEFAULT = 1000;
    const REPORTS_NETWORK_LIMIT = 25;
    const REPORTS_MULTI_MODE = 'multi';
    const REPORTS_GRAPH_DISABLED = true;

    // reports($subject_type, $subject, $offset = null, $limit = 1000)
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // no need to worry about $rdepth and the parameters after it, those are "internal"
    // properties used for the recursion
    //
    // $subject_type types:
    //   - user
    //   - channel
    //
    // This function returns removal/blacklist results associated with
    // the given reddit user or media channel id
    public function reports($subject_type, $subject_in, $offset = 0, $limit = null,
            $rdepth = 1, $matterSeenMap = [], $no_total = false) {

        $params = [
            'srlist'    => $this->subreddit->to_lower_array(),
            'sridlist'  => cast_array($this->subreddit->id()),
            'limit'     => min(intval($limit ?? self::REPORTS_DEFAULT), self::REPORTS_LIMIT),
            'offset'    => intval($offset ?? 0),
        ];

        $create_matter_id = function($name, $type) {
            return $type . ';' . $name;
        };

        // TYPE CONFIG
        // ~~~~~~~~~~~

        $relation_types = [
            'channel' => [
                'primary' => 'media_channel_id',
                'data' => [
                    'media_author',
                    'media_channel_url',
                    'media_platform'
                ],
                'search_matter' => [],
                'search_param' => 'channelsubject',
                'formatter' => function($subject) {
                    return MediaTools::info($subject)['id'] ?: $subject;
                }
            ],
            'user' => [
                'primary' => 'author',
                'data' => [],
                'search_matter' => [],
                'search_param' => 't3subject',
                'formatter' => function($subject) {
                    return Common::reddit_username($subject);
                }
            ],
            'channel_name' => [
                'primary' => 'media_author',
                'data' => [],
                'search_matter' => [],
                'search_param' => 'channelname',
                'custom_sql' => '(tsv @@ plainto_tsquery(%s_channelname))',
                'formatter' => function($subject) {
                    return $subject;
                }
            ],
        ];

        // create search vector
        // --------------------------------------------------------------------------------

        $tmp = ['type' => $subject_type, 'subject' => $subject_in];
        $tmp = $tmp['type'] === self::REPORTS_MULTI_MODE ? $subject_in : [$tmp];
        $subject_matter = [];

        foreach ($tmp as $item) {
            list('type' => $type, 'subject' => $subject) = $item;

            $relation_types[$type]['search_matter'][] =
                ($subject = $relation_types[$type]['formatter']($subject));

            $subject_matter[] = [
                'subject'   => $subject,
                'type'      => $type,
                'matter_id' => $create_matter_id($subject, $type),
            ];
        }

        // calculate subject components
        // --------------------------------------------------------------------------------
        $subject_components = [];
        $fromvar_components = [];

        if ($subject_type == 'channel_name') {
            for ($i = 0; $i < count($relation_types['channel_name']['search_matter']); $i++) {
                $fromvar_components[] = "plainto_tsquery(%s_channelnamequery{$i}) as channelnamequery{$i}";
                $subject_components[] = "(tsv @@ channelnamequery{$i})";
                $params["channelnamequery{$i}"] = $relation_types['channel_name']['search_matter'][$i];
            }

            $subject_component = '(' . implode(' OR ', $subject_components) . ')';
            $fromvar_component = implode(', ', $fromvar_components);
        } else {
            foreach ($relation_types as $type => $typedata) {
                $primary = $typedata['primary'];
                $param   = $typedata['search_param'];
                $param_N = count($typedata['search_matter']) <=> 1;

                if ($param_N == -1) // skip if matter count is 0
                    continue;

                $subject_components[$primary] = $primary . ['=%s_', ' IN %ls_'][$param_N] . $param;
                $params[$param] = aor1($typedata['search_matter']);
            }

            $subject_component = '(' . implode(' OR ', array_values($subject_components)) . ')';
            $fromvar_component = '';
        }

        // perform queries
        // --------------------------------------------------------------------------------
        $queryString =
          "SELECT
            removal_id as id,
            EXTRACT(EPOCH FROM action_utc) as action_utc,
            removed,
            subreddit_name as subreddit,
            author,
            permalink,
            thing_id,
            thing_title,
            thing_data,
            EXTRACT(EPOCH FROM created_utc) as thing_created,
            media_author,
            media_channel_id,
            media_channel_url,
            media_url as media_link,
            platform_name as media_platform ";
        $queryTotalString =
          "SELECT
            COUNT(*)
          FROM
            thesentinel_view
          WHERE
            {$subject_component}
            AND media_channel_id IS NOT NULL
            AND subreddit_name IN %ls_srlist";

        if ($subject_type == 'channel_name') {
            $queryString =
             "SELECT
                sa.id,
                sa.removed,
                EXTRACT(EPOCH FROM sa.action_utc) as action_utc,
                sr.subreddit_name as subreddit,
                x.author,
                x.thing_id,
                x.thing_title,
                x.thing_data,
                EXTRACT(EPOCH FROM x.created_utc) as thing_created,
                x.permalink,
                x.media_author,
                x.media_channel_id,
                x.media_url as media_link,
                x.media_platform_id,
                sb.media_channel_url,
                mp.platform_name as media_platform,
                x.tsv
              FROM (
                (
                  SELECT
                    rt.subreddit_id,
                    rt.author,
                    rt.thing_id,
                    rt.thing_title,
                    rt.thing_data,
                    rt.created_utc,
                    rt.permalink,
                    mi.media_author,
                    mi.media_channel_id,
                    mi.media_url,
                    mi.media_platform_id,
                    mi.tsv
                  FROM (
                    reddit_thing rt
                    LEFT JOIN media_info mi ON (rt.thing_id = mi.thing_id)
                  ), {$fromvar_component}
                  WHERE {$subject_component}
                    AND subreddit_id IN %li_sridlist
                  ORDER BY rt.created_utc DESC
                  LIMIT %i_limit
                  OFFSET %i_offset
                ) as x
                LEFT JOIN sentinel_blacklist sb ON (
                  sb.id = (
                    SELECT min(sentinel_blacklist.id) as min
                      FROM sentinel_blacklist
                      WHERE (sentinel_blacklist.media_channel_id = x.media_channel_id)
                  )
                )
                LEFT JOIN sentinel_actions sa ON (
                  sa.id = (
                    SELECT max(sentinel_actions.id) as max
                      FROM sentinel_actions
                      WHERE (sentinel_actions.thing_id = x.thing_id)
                  )
                )
                LEFT JOIN subreddit        sr  ON    (sr.id = x.subreddit_id)
                LEFT JOIN media_platform   mp  ON    (mp.id = x.media_platform_id)
              )
              ORDER BY created_utc DESC";
            $queryTotalString =
             "SELECT
                count(*)
              FROM (
                (
                  SELECT rt.subreddit_id, rt.thing_id, mi.media_channel_id, mi.media_platform_id
                  FROM (
                    reddit_thing rt
                    LEFT JOIN media_info mi ON (rt.thing_id = mi.thing_id)
                  ), {$fromvar_component}
                  WHERE {$subject_component}
                    AND subreddit_id IN %li_sridlist
                ) as x
                LEFT JOIN sentinel_blacklist sb ON (
                  sb.id = (
                    SELECT min(sentinel_blacklist.id) as min
                      FROM sentinel_blacklist
                      WHERE (sentinel_blacklist.media_channel_id = x.media_channel_id)
                  )
                )
                LEFT JOIN sentinel_actions sa ON (
                  sa.id = (
                    SELECT max(sentinel_actions.id) as max
                      FROM sentinel_actions
                      WHERE (sentinel_actions.thing_id = x.thing_id)
                  )
                )
                LEFT JOIN subreddit        sr  ON    (sr.id = x.subreddit_id)
                LEFT JOIN media_platform   mp  ON    (mp.id = x.media_platform_id)
              )";
        } else if ($subject_type == self::REPORTS_MULTI_MODE) {
            $queryString .=
            "FROM (" . array_reduce(
                array_keys($subject_components),
                function($acc, $primary_name) use ($subject_components) {
                    if (strlen($acc) != 0) {
                        $acc .= " UNION ALL ";
                    }
                    $acc .=
                    "SELECT
                        ROW_NUMBER() OVER (PARTITION BY {$primary_name} ORDER BY created_utc DESC) AS r,
                        t.*
                    FROM
                        thesentinel_view t
                    WHERE
                        media_channel_id IS NOT NULL
                        AND {$subject_components[$primary_name]}
                        AND subreddit_name IN %ls_srlist";
                    return $acc;
                }, ""
            ) . ") x
            WHERE
              x.r <= {$params['limit']}
            ORDER BY created_utc DESC
            OFFSET %i_offset";
        } else {
            $queryString .=
            "FROM
              thesentinel_view
            WHERE
              {$subject_component}
              AND media_channel_id IS NOT NULL
              AND subreddit_name IN %ls_srlist
            ORDER BY created_utc DESC
            LIMIT %i_limit
            OFFSET %i_offset";
        }

        $listing = db('Zion')->query($queryString, $params);

        // return results
        // --------------------------------------------------------------------------------
        return [
            'matter' =>
                $subject_matter,

            'listing' =>
                $listing,

            'relations' =>
                $this->report_network(
                    $listing, $relation_types, $rdepth, $matterSeenMap, $create_matter_id),

            'amount' =>
                count($listing),

            'offset' =>
                $params['offset'],

            'total' =>
                $no_total ? null : db('Zion')->queryFirstField($queryTotalString, $params),
        ];
    }

    private function report_network($listing, $relation_types, $rdepth, $matterSeenMap, $create_matter_id) {
        $network = [];
        if (self::REPORTS_GRAPH_DISABLED) {
            return $network;
        }

        $source_check = [];

        $relations = [];
        $relationCounter = [];

        $rdepth_passed = $rdepth > 0;
        $rdepth_next = $rdepth - 1;

        $directUpdateMap = [];
        $directList = [];

        foreach ($relation_types as $type => $typedata) {
            foreach ($typedata['search_matter'] as $matter) {
                $matter_id = $create_matter_id($matter, $type);

                $network[$matter_id] = [];
                $source_check[$matter] = $matter_id;
            }
        }

        foreach ($listing as $data) {
            $source_matter_id = null;
            foreach ($relation_types as $type => $typedata) {
                $subject = $data[$typedata['primary']];

                if (isset($source_check[$subject])) {
                    $source_matter_id = $source_check[$subject];
                    $matterSeenMap[$source_matter_id] = true;
                    break;
                }
            }

            foreach ($relation_types as $type => $typedata) {
                $name = $data[$typedata['primary']];
                $matter_id = $create_matter_id($name, $type);

                if ($matter_id == $source_matter_id) {
                    continue;
                } else if (isset($relations[$matter_id])) {
                    $relationCounter[$matter_id]++;
                    continue;
                } else {
                    $relationCounter[$matter_id] = 1;
                }

                $relation_node = [
                    'matter' => $name,
                    'type' => $type,
                    'data' => [],
                    'directed_from' => [
                        'id'     => $source_matter_id,
                        'amount' => 0,
                    ],
                    'directs_to' => null,
                ];

                foreach ($typedata['data'] as $col) {
                    $relation_node['data'][$col] = $data[$col];
                }

                if (isset($matterSeenMap[$matter_id])) {
                    $relation_node['directs_to'] = 'SOURCE_REACHED';
                    $relations[$matter_id] = $relation_node;
                } else if ($rdepth_passed) {
                    $directList[] = [
                        'type' => $type,
                        'subject' => $name,
                        'matter_id' => $matter_id
                    ];
                    $relations[$matter_id] = $relation_node;

                    if (!isset($directUpdateMap[$matter_id])) {
                        $directUpdateMap[$matter_id] = [];
                    }
                    $directUpdateMap[$matter_id][] = &$relations[$matter_id];
                } else {
                    $relation_node['directs_to'] = 'DEPTH_LIMIT_REACHED';
                    $relations[$matter_id] = $relation_node;
                }
            }
        }

        if (!empty($directList)) {
            $directResults =
                $this->reports(
                    self::REPORTS_MULTI_MODE,
                    $directList,
                    0,
                    self::REPORTS_NETWORK_LIMIT,
                    $rdepth_next,
                    $matterSeenMap,
                    true // we don't need total
                )['relations'];
            foreach ($directResults as $matter_id => $relation) {
                foreach ($directUpdateMap[$matter_id] as &$relation_node) {
                    $relation_node['directs_to'] = $relation;
                }
            }
        }

        foreach ($relationCounter as $matter_id => $counted_amount) {
            $relations[$matter_id]['directed_from']['amount'] = $counted_amount;
            $network[$relations[$matter_id]['directed_from']['id']][] = $relations[$matter_id];
        }

        return $network;
    }

    public function list($a = 25, $b = 0, $listing_type = 'offset', $subjects = null) {
        $subreddits = $this->subreddit->to_lower_array();
        $id_list    = (array) $this->subreddit->id();

        $subject_component = '';
        /* TODO: fix this
        if (!empty($subjects)) {
            $subjects = (array) $subjects;
            $subject_component = " AND {$conf['subject_col']} IN %ls_subjects";
        }*/

        switch ($listing_type) {
            case 'page':
                $page_size  = intval($a);
                $page       = intval($b);
                $total      = db('Zion')->queryFirstField(
                    "SELECT COUNT(*) FROM sentinel_blacklist WHERE subreddit_id IN %li_srlist"
                    .$subject_component, [
                        'srlist' => $id_list,
                        'subjects' => $subjects,
                    ]);
                $page_count = ceil($total / $page_size);

                if ($page <= 0) {
                    $page = $page_count + $page;
                    if ($page < 1) $page = 1;
                }
                if ($page > $page_count) {
                    $page = $page_count;
                }

                $offset = ($page-1)*$page_size;
                $limit = $page_size;
                break;
            case 'offset':
                $amount     = intval($a);
                $offset     = intval($b);
                $total      = db('Zion')->queryFirstField(
                    'SELECT COUNT(*) FROM sentinel_blacklist WHERE subreddit_id IN %li_srlist'
                    .$subject_component, [
                        'srlist' => $id_list,
                        'subjects' => $subjects,
                    ]);
                $limit = $amount;
                break;
            default:
                throw new \InvalidArgumentException('listing_type must either be "page" or "offset"');
        }

        // ----- FETCH DATA

        if ($total != 0) {
            $results = db('Zion')->query("
                SELECT
                  bl.id,
                  bl.subreddit_id,
                  sr.subreddit_name as subreddit,
                  bl.media_channel_id,
                  bl.media_channel_url,
                  bl.media_author,
                  bl.media_platform_id,
                  lower(mp.platform_name::text) as media_platform,
                  EXTRACT(EPOCH FROM bl.blacklist_utc) as blacklist_utc,
                  bl.blacklist_by
                FROM
                (
                  sentinel_blacklist bl
                  LEFT JOIN subreddit sr ON ((sr.id = bl.subreddit_id))
                  LEFT JOIN media_platform mp ON ((mp.id = bl.media_platform_id))
                )
                WHERE
                  subreddit_id IN %li_srlist {$subject_component}
                ORDER BY blacklist_utc DESC NULLS LAST
                LIMIT {$limit}
                OFFSET {$offset}",
                [
                    'srlist' => $id_list,
                    'subjects' => $subjects,
                ]);
        } else {
            $results = [];
        }

        $listing = [];
        foreach ($results as $row) {
            $listing[] = [
                'requestid'         => $row['id'],
                'moderator'         => $row['blacklist_by'],
                'subreddit'         => $row['subreddit'],
                'timestamp'         => $row['blacklist_utc'],
                'media_author'      => $row['media_author'],
                'media_channel_id'  => $row['media_channel_id'],
                'media_channel_url' => $row['media_channel_url'],
                'media_platform'    => $row['media_platform'],
            ];
        }
        // ----- RETURN

        switch ($listing_type) {
            case 'page':
                if (!isset($page_size, $page, $page_count)) {
                    throw new \LogicException();
                }
                return [
                    'listing'       => $listing,
                    'fetch_type'    => $listing_type,
                    'page_size'     => $page_size,
                    'page'          => $page,
                    'page_count'    => $page_count,
                ];
            case 'offset':
                if (!isset($amount)) {
                    throw new \LogicException();
                }
                return [
                    'listing'       => $listing,
                    'fetch_type'    => $listing_type,
                    'amount'        => $amount,
                    'offset'        => $offset,
                    'total'         => $total,
                ];
        }
    }

    protected function create_media_set($media_urls) {
        $subreddits = $this->subreddit->to_array();
        $media_urls = array_flatten($media_urls);

        $media_set = [];
        foreach ($media_urls as $media_url) {
            $media_set[] = [
                'media_url' => $media_url,
                'subreddits' => $subreddits,
            ];
        }
        return $media_set;
    }

    protected function in_present($present, $channel_id, $subreddit) {
        $subreddit = strtolower($subreddit);
        foreach ($present as $item) {
            if ($item['channel_id'] == $channel_id && strtolower($item['subreddit']) == $subreddit) {
                return true;
            }
        }
        return false;
    }

    public function check() {
        $media_set = $this->create_media_set(func_get_args());

        $media_list = [];
        $check_sql = [];

        if (empty($media_set)) {
            return false;
        }

        foreach ($media_set as $media_item) {
            $media_url  = add_protocol($media_item['media_url']);
            $subreddits = to_lower_array($media_item['subreddits']);
            $media_info = MediaTools::info($media_url);

            if (empty($media_info) || empty($media_info['id'])) {
                continue;
            }

            $platform       = $media_info['platform'];
            $channel_id     = $media_info['id'];
            $channel_author = $media_info['author'];
            $channel_url    = $media_info['url'];

            $media_list[$channel_id] = [
                'media_url'         => $media_url,
                'media_platform'    => $platform,
                'media_platform_id' => MediaTools::platform_id($platform),
                'subreddit'         => $subreddits,
                'subreddit_id'      => subreddit($subreddits)->id(true),
                'channel_id'        => $channel_id,
                'channel_author'    => $channel_author,
                'channel_url'       => $channel_url
            ];

            foreach ($subreddits as $sr) {
                $check_sql[] = [$channel_id, $sr];
            }
        }

        if (empty($check_sql)) {
            return [
                'present' => [],
                'data' => [],
                'processed' => [],
            ];
        }

        $result = db('Zion')->query("
            SELECT
              bl.id,
              bl.subreddit_id,
              sr.subreddit_name as subreddit,
              bl.media_channel_id,
              bl.media_channel_url,
              bl.media_author,
              bl.media_platform_id,
              lower(mp.platform_name::text) as media_platform,
              EXTRACT(EPOCH FROM bl.blacklist_utc) as blacklist_utc,
              bl.blacklist_by
            FROM
            (
              sentinel_blacklist bl
              LEFT JOIN subreddit sr ON ((sr.id = bl.subreddit_id))
              LEFT JOIN media_platform mp ON ((mp.id = bl.media_platform_id))
            )
            WHERE
              (bl.media_channel_id, sr.subreddit_name) in (%ll?_medialist)",
              ['medialist' => $check_sql]);

        $present_channel_ids = [];
        foreach ($result as $row) {
            $present_channel_ids[] = [
                'channel_id'    => $row['media_channel_id'],
                'channel_url'   => $row['media_channel_url'],
                'platform'      => $row['media_platform'],
                'platform_id'   => $row['media_platform_id'],
                'subreddit'     => $row['subreddit'],
                'subreddit_id'  => $row['subreddit_id'],
                'moderator'     => $row['blacklist_by'],
                'timestamp'     => $row['blacklist_utc'],
                'humantiming'   => human_timing($row['blacklist_utc'])
            ];
        }

        return [
            'present'   => $present_channel_ids,
            'data'      => $media_list,
            'processed' => [],
        ];
    }

    public function remove() {
        $media_set = $this->create_media_set(func_get_args());

        $check_data = $this->check(func_get_args());
        $present    = $check_data['present'];
        $data       = $check_data['data'];

        $update_data = [];
        $processed = [];

        foreach ($data as $k => $media_item) {
            foreach ($media_item['subreddit_id'] as $subreddit => $subreddit_id) {
                if ($this->in_present($present, $media_item['channel_id'], $subreddit)) {
                    $update_data[] = [$media_item['channel_id'], $subreddit_id];
                    $processed[] = [$media_item['channel_id'], $subreddit];
                }
            }
        }

        if (!empty($update_data)) {
            db('Zion')->query("
                DELETE FROM
                    sentinel_blacklist
                WHERE
                    (media_channel_id,subreddit_id) IN (%ll?_medialist)
                ",
                ['medialist' => $update_data]);
        }

        return [
            'data'      => $data,
            'present'   => $present,
            'processed' => $processed,
        ];
    }

    public function add() {
        $media_set = $this->create_media_set(func_get_args());

        $check_data = $this->check(func_get_args());
        $present    = $check_data['present'];
        $data       = $check_data['data'];

        $insert_data = [];
        $update_data = [];
        $processed = [];

        foreach ($data as $k => $media_item) {
            $media_url      = add_protocol($media_item['media_url']);
            $channel_id     = $media_item['channel_id'];
            $channel_url    = $media_item['channel_url'];
            $channel_author = $media_item['channel_author'];
            $platform_id    = $media_item['media_platform_id'];

            foreach ($media_item['subreddit_id'] as $subreddit => $subreddit_id) {
                if (!$this->in_present($present, $channel_id, $subreddit)) {
                    $insert_data[] = array(
                        'subreddit_id'      => $subreddit_id,
                        'media_channel_id'  => $channel_id,
                        'media_channel_url' => $channel_url,
                        'media_author'      => $channel_author,
                        'media_platform_id' => $platform_id,
                        'blacklist_utc'     => date('Y-m-d H:i:s', time()),
                        'blacklist_by'      => user()->get_username(),
                    );
                    $processed[] = [$channel_id, $subreddit];
                }
            }
        }

        if (!empty($insert_data)) {
            db('Zion')->insert('sentinel_blacklist', $insert_data);
        }

        return [
            'data'      => $data,
            'present'   => $present,
            'processed' => $processed
        ];
    }

}