<?php
namespace app\lib\mixins;

use app\lib\SubredditModule;
use app\lib\Common;

class DirtbagModule extends SubredditModule {

    // settings($new_settings)
    //
    // No parameter -> fetch settings
    // Otherwise save settings with '$new_settings'
    public function settings($new_settings = null) {
        $subreddit = $this->subreddit[0];

        if (isset($new_settings)) {
            // get query parameter values
            $modifiedBy = user()->get_username();

            if (is_string($new_settings)) {
                $new_settings = rawurldecode($new_settings);

                // just in case, the dirtbag website is finicky
                $new_settings = json_decode($new_settings, true);
                $settings     = json_encode($new_settings);
            } else {
                $settings = json_encode($new_settings);
            }

            $res = Common::call_url(
                "https://dirtbag.snoonotes.com/api/Settings/{$subreddit}?modifiedBy={$modifiedBy}",
                "POST",
                array(
                    'Accept: */*',
                    'Content-Type: application/json; charset=utf-8',
                    self::get_auth_header(),
                    'Content-Length: '.mb_strlen($settings, 'UTF-8'),
                ),
                $settings);

            return true;
        } else {
            // send GET request
            return self::GET("dirtbag", "/api/Settings/{$subreddit}");
        }
    }

    public function purgeSettings() {
        $subreddit = $this->subreddit[0];
        return self::DELETE("dirtbag", "/api/Settings/{$subreddit}/Purge");
    }

    // STATIC FUNCTIONS
    // --------------------------------------------------------------------------------

    public static function get_auth_header() {
        if (!empty($authorization = cached('dirtbag_auth'))) {
            return $authorization;
        }

        $response =
            self::call_simple(null, '/Auth/connect/token', 'POST', null,
                'scope=dirtbag&grant_type=client_credentials&client_id='.DIRTBAG_ID.'&client_secret='.DIRTBAG_SECRET);

        $token   = $response['access_token'];
        $expires = $response['expires_in'];
        $type    = $response['token_type'];

        $authorization = 'Authorization: '.$type.' '.$token;

        /* cache the authorization with the expiry 20 minutes before the actual expiration time*/
        cached('dirtbag_auth', $authorization, $expires - 1200);

        return $authorization;
    }

    private static function GET($subdomain = null, $url, $headers = null, $body = null) {
        return self::call($subdomain, $url, 'GET', $headers, $body);
    }

    private static function POST($subdomain = null, $url, $headers = null, $body = null) {
        return self::call($subdomain, $url, 'POST', $headers, $body);
    }

    private static function DELETE($subdomain = null, $url, $headers = null, $body = null) {
        return self::call($subdomain, $url, 'DELETE', $headers, $body);
    }

    private static function PUT($subdomain = null, $url, $headers = null, $body = null) {
        return self::call($subdomain, $url, 'PUT', $headers, $body);
    }

    private static function call($subdomain = null, $url, $http_method = 'GET', $http_header, $http_contentbody = null) {
        return self::call_simple($subdomain, $url, $http_method, self::get_auth_header()."\r\n".$http_header, $http_contentbody);
    }

    private static function call_simple($subdomain, $url, $http_method = 'GET', $http_header = null, $http_contentbody = null) {
        $subdomain = $subdomain ?: '';
        if ($subdomain)
            $subdomain = $subdomain . '.';
        $url = 'https://'.$subdomain.'snoonotes.com/'.ltrim($url,'/');
        return Common::call_json($url, $http_method, $http_header, $http_contentbody);
    }
}