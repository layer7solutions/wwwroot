<?php
namespace app\lib\mixins;

use app\lib\SubredditModule;
use app\lib\logs\Matrix;
use app\lib\logs\Logs;

class LogsModule extends SubredditModule {

    public function matrix($lower_bound, $upper_bound, $moderators, $action_types = null) {
        return Matrix::genMatrix(
            $this->subreddit->to_array(),
            $upper_bound,
            $lower_bound,
            $moderators,
            $action_types
        );
    }

}