-- DOCUMENT COLUMN
ALTER TABLE media_info ADD COLUMN tsv tsvector;

-- DOCUMENT INDEX
CREATE INDEX tsv_idx ON media_info USING gin(tsv);

-- POPULATE DOCUMENTS FOR EXISTING ROWS
UPDATE media_info SET tsv = to_tsvector(coalesce(media_author,''));

-- DOCUMENT CREATION TRIGGER FUNCTION
CREATE FUNCTION media_info_search_trigger() RETURNS trigger AS $$
begin
  new.tsv :=
    to_tsvector(coalesce(new.media_author,''));
  return new;
end
$$ LANGUAGE plpgsql;

-- DOCUMENT CREATION TRIGGER CALLS FUNCTION
CREATE TRIGGER media_info_tsvectorupdate BEFORE INSERT OR UPDATE
ON media_info FOR EACH ROW EXECUTE PROCEDURE media_info_search_trigger();

-- LOOKUP QUERY
SELECT id, media_author, media_channel_id, media_url, media_platform_id FROM (
  SELECT id, media_author, media_channel_id, media_url, media_platform_id, tsv
  FROM media_info, plainto_tsquery('YOUR QUERY') AS q
  WHERE (tsv @@ q)
) AS t1 ORDER BY ts_rank_cd(t1.tsv, plainto_tsquery('YOUR QUERY')) DESC LIMIT 25;

-- LOOKUP QUERY THESENTINEL_VIEW
SELECT
  sa.id,
  sa.removed,
  EXTRACT(EPOCH FROM sa.action_utc) as action_utc,
  sr.subreddit_name,
  x.author,
  x.thing_id,
  x.thing_title,
  x.thing_data,
  EXTRACT(EPOCH FROM x.created_utc) as thing_created,
  x.permalink,
  x.media_author,
  x.media_channel_id,
  x.media_url as media_link,
  x.media_platform_id,
  sb.media_channel_url,
  mp.platform_name as media_platform,
  x.tsv
FROM (
  (
    SELECT
      rt.subreddit_id,
      rt.author,
      rt.thing_id,
      rt.thing_title,
      rt.thing_data,
      rt.created_utc,
      rt.permalink,
      mi.media_author,
      mi.media_channel_id,
      mi.media_url,
      mi.media_platform_id,
      mi.tsv
    FROM (
      reddit_thing rt
      LEFT JOIN media_info mi ON (rt.thing_id = mi.thing_id)
    ), plainto_tsquery('YOUR QUERY') as q
    WHERE (tsv @@ q)
    --AND subreddit_id IN ('subreddit')
    ORDER BY rt.created_utc DESC
    LIMIT 25
    OFFSET 0
  ) as x
  LEFT JOIN sentinel_blacklist sb ON (
    sb.id = (
      SELECT min(sentinel_blacklist.id) as min
        FROM sentinel_blacklist
        WHERE (sentinel_blacklist.media_channel_id = x.media_channel_id)
    )
  )
  LEFT JOIN sentinel_actions sa ON (
    sa.id = (
      SELECT max(sentinel_actions.id) as max
        FROM sentinel_actions
        WHERE (sentinel_actions.thing_id = x.thing_id)
    )
  )
  LEFT JOIN subreddit        sr  ON    (sr.id = x.subreddit_id)
  LEFT JOIN media_platform   mp  ON    (mp.id = x.media_platform_id)
)
ORDER BY created_utc DESC

-- GET TOTAL QUERY
SELECT count(*)
FROM (
  (
    SELECT rt.subreddit_id, rt.thing_id, mi.media_channel_id, mi.media_platform_id,
    FROM (
      reddit_thing rt
      LEFT JOIN media_info mi ON (rt.thing_id = mi.thing_id)
    ), plainto_tsquery('YOUR QUERY') as q
    WHERE (tsv @@ q)
    --AND subreddit_id IN ('subreddit')
  ) as x
  LEFT JOIN sentinel_blacklist sb ON (
    sb.id = (
      SELECT min(sentinel_blacklist.id) as min
        FROM sentinel_blacklist
        WHERE (sentinel_blacklist.media_channel_id = x.media_channel_id)
    )
  )
  LEFT JOIN sentinel_actions sa ON (
    sa.id = (
      SELECT max(sentinel_actions.id) as max
        FROM sentinel_actions
        WHERE (sentinel_actions.thing_id = x.thing_id)
    )
  )
  LEFT JOIN subreddit        sr  ON    (sr.id = x.subreddit_id)
  LEFT JOIN media_platform   mp  ON    (mp.id = x.media_platform_id)
)