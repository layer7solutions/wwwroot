--- These extensions ship with PostgreSQL, you must enable them first:
CREATE EXTENSION unaccent;

-- DOCUMENT COLUMN
ALTER TABLE modmail ADD COLUMN document tsvector;

-- DOCUMENT INDEX
CREATE INDEX document_idx ON modmail USING gin(document);

-- POPULATE DOCUMENTS FOR EXISTING ROWS
UPDATE modmail SET document =
    setweight(to_tsvector('simple', unaccent(coalesce(subject,'')) ), 'A') ||
    setweight(to_tsvector('simple', unaccent(coalesce(body,'')) ), 'B') ||
    setweight(to_tsvector('simple', coalesce(message_from,'') ), 'C') ||
    setweight(to_tsvector('simple', coalesce(nullif(message_to, '{}'),'') ), 'D');

-- DOCUMENT CREATION TRIGGER FUNCTION
CREATE FUNCTION modmail_search_trigger() RETURNS trigger AS $$
begin
  new.document :=
    setweight(to_tsvector('simple', unaccent(coalesce(new.subject,'')) ), 'A') ||
    setweight(to_tsvector('simple', unaccent(coalesce(new.body,'')) ), 'B') ||
    setweight(to_tsvector('simple', coalesce(new.message_from,'') ), 'C') ||
    setweight(to_tsvector('simple', coalesce(nullif(new.message_to, '{}'),'') ), 'D');
  return new;
end
$$ LANGUAGE plpgsql;

-- DOCUMENT CREATION TRIGGER CALLS FUNCTION
CREATE TRIGGER modmail_tsvectorupdate BEFORE INSERT OR UPDATE
ON modmail FOR EACH ROW EXECUTE PROCEDURE modmail_search_trigger();

-- LOOKUP QUERY
SELECT *
FROM modmail
WHERE document @@ plainto_tsquery('simple', 'YOUR QUERY')
ORDER BY ts_rank_cd(document, plainto_tsquery('simple', 'YOUR QUERY')) DESC LIMIT 25;