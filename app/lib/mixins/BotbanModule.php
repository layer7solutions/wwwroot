<?php
namespace app\lib\mixins;

use app\lib\SubredditModule;

class BotbanModule extends SubredditModule {
    use SimpleBlacklistTrait;

    public function config() {
        return array(
            'sr_names'          => $this->subreddit->to_array(),
            'sr_idlist'         => $this->subreddit->id(),

            'table'             => 'botban',
            'columns'           => [
                'username',
                'bannedby',
                'bannedon',
            ],
            'subject_col'       => 'username',
            'time_col'          => 'bannedon',
            'mod_col'           => 'bannedby',
            'aliases'           => [
                'username'      => 'user',
                'bannedby'      => 'banned_by',
                'bannedon'      => 'banned_utc',
            ],
            'display_names'     => [
                'banned_by'     => 'banned by',
                'banned_utc'    => 'banned time',
            ],
            'sr_category'       => 'botban',
            'subject_type'      => 'user',
            'subject_name'      => 'user',
            'subject_plural'    => 'users',
            'subject_plural_suffix' => ' or reddit user links.'
        );
    }

}