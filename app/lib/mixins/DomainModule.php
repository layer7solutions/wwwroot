<?php
namespace app\lib\mixins;

use app\lib\SubredditModule;

class DomainModule extends SubredditModule {
    use SimpleBlacklistTrait;

    public function config() {
        return array(
            'sr_names'          => $this->subreddit->to_array(),
            'sr_idlist'         => $this->subreddit->id(),

            'table'             => 'domain_info',
            'columns'           => [
                'domain',
                'action_utc',
                'added_by',
            ],
            'subject_col'       => 'domain',
            'time_col'          => 'action_utc',
            'mod_col'           => 'added_by',
            'aliases'           => [
                'added_by'      => 'banned_by',
                'action_utc'    => 'banned_utc',
            ],
            'values'            => [
                'banned'        => true,
            ],
            'display_names'     => [
                'banned_by'     => 'banned by',
                'banned_utc'    => 'banned time'
            ],
            'sr_category'       => 'domain',
            'subject_type'      => 'domain',
            'subject_name'      => 'domain',
            'subject_plural'    => 'domains',
        );
    }

}