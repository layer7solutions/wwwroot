<?php
namespace app\lib\mixins;

use app\lib\SubredditModule;

class UsersModule extends SubredditModule {
    const ORDER_THING_CREATED_UTC = 0;
    const ORDER_AUTHOR_CREATED_UTC = 1;

    const DEFAULT_SEARCH_RESULTS = 50;
    const MAX_SEARCH_RESULTS = 300;

    private function _limit(int $limit = null) {
        return max(1, min(intval($limit ?? self::DEFAULT_SEARCH_RESULTS), self::MAX_SEARCH_RESULTS));
    }

    private function _offset(int $offset = null) {
        return max(0, $offset ?? 0);
    }

    public function get_new_users(int $ord_type = null, int $offset = null, int $limit = null) {
        $offset = $this->_offset($offset);
        $limit = $this->_limit($limit);

        switch ($ord_type) {
            case self::ORDER_AUTHOR_CREATED_UTC:
                $ord = 'authorcreated_utc';
                break;
            case self::ORDER_THING_CREATED_UTC:
            default:
                $ord = 'current_utc';
                break;
        }

        $query_stmt =
            "SELECT * FROM users
            WHERE subreddit_id IN %li_srids
            ORDER BY {$ord} DESC NULLS LAST
            OFFSET %i_offset LIMIT %i_limit";

        $query_params = [
            'limit'     => $limit,
            'offset'    => $offset,
            'srids'     => (array) $this->subreddit->id
        ];

        return db('Zion')->query($query_stmt, $query_params);
    }

}