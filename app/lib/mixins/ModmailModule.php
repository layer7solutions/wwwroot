<?php
namespace app\lib\mixins;

use app\lib\SubredditModule;

class ModmailModule extends SubredditModule {
    const MOST_RECENT = 0;
    const MOST_RELEVANT = 1;
    const MOST_AGED = 2;
    const DEFAULT_SEARCH_RESULTS = 50;
    const MAX_SEARCH_RESULTS = 300;

    private function _query($text) {
        return trim(strval($text));
    }

    private function _limit($limit = null) {
        return max(1, min(intval($limit ?? self::DEFAULT_SEARCH_RESULTS), self::MAX_SEARCH_RESULTS));
    }

    public function search($text, $limit=null, $ord=self::MOST_RECENT) {
        $query = $this->_query($text);
        $limit = $this->_limit($limit);

        switch ($ord) {
            case self::MOST_RELEVANT:
                $ord_clause = "ORDER BY ts_rank_cd(document, plainto_tsquery('simple', %s_query)) DESC";
                break;
            case self::MOST_AGED:
                $ord_clause = 'ORDER BY created_utc ASC';
                break;
            case self::MOST_RECENT:
            default:
                $ord_clause = 'ORDER BY created_utc DESC';
                break;
        }

        $query_stmt =
           "SELECT
              mm.id,
              mm.thing_id,
              sr.id as subreddit_id,
              sr.subreddit_name as subreddit,
              mm.message_root_thing_id,
              mm.message_from,
              mm.message_to,
              mm.created_utc,
              mm.subject,
              mm.body,
              mm.parent_thing_id,
              mm.document
            FROM
            (
              SELECT *
              FROM modmail
              WHERE
                subreddit_id IN %li_srids
                AND
                document @@ plainto_tsquery('simple', %s_query)
              {$ord_clause}
              LIMIT %i_limit
            ) mm
            LEFT JOIN subreddit sr ON ((sr.id = mm.subreddit_id))
            {$ord_clause}";

        $query_params = [
            'query' => $query,
            'limit' => $limit,
            'srids' => (array) $this->subreddit->id
        ];

        return $this->_execute($query_stmt, $query_params);
    }

    private function _execute($stmt, $params) {
        if (empty($params['query'])) {
            return [];
        } else {
            return db('Zion')->query($stmt, $params);
        }
    }

    public function getConversationsWithAuthors(array $authorlist) {
        $query_stmt =
            "SELECT
              m.message_root_thing_id, m.message_from, count(*) as message_count FROM modmail m
            WHERE
              m.subreddit_id=96
              AND m.created_utc > NOW() - '2 day'::INTERVAL
              AND m.message_from  IN %ls_authors
            GROUP BY
              m.message_root_thing_id, m.message_from";
        $query_params = [
            'authors' => (array) $authorlist
        ];
        $result = $this->_execute($query_stmt, $query_params);

        $map = [];
        foreach ($result as $row) {
            $map[$row['message_root_thing_id']][$row['message_from']] = $row['message_count'];
        }

        return $map;
    }

}