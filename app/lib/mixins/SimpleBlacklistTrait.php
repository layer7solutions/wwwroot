<?php

namespace app\lib\mixins;

trait SimpleBlacklistTrait {
    private $conf = null;

    // do not call this function, use conf() instead
    // this function is to be implemented by the parent class
    abstract function config(): array;

    /**
     * @return array
     */
    public function jsconf() {
        $sbtconf = $this->config();
        $sbtconf['select_options'] = [];

        $subreddits = alphasort(user()->modded('all:'.$sbtconf['sr_category'])->to_array());
        foreach ($subreddits as $sr) {
            $sbtconf['select_options'][] = [
                'value' => $sr,
                'text' => $sr,
            ];
        }

        return $sbtconf;
    }

    /**
     * obtains the config from the parent class,
     * checks if it's valid
     * makes some modifications to it
     * then stores it into $this->conf
     *
     * @return array|null
     * @throws \Exception
     */
    private function conf() {
        if (!isset($this->conf)) {
            $conf = $this->config();

            // --- check if valid config

            $required_keys = [
                'table', 'sr_names', 'sr_idlist', 'columns', 'time_col', 'subject_col', 'mod_col',
                'sr_category', 'subject_type', 'subject_name', 'subject_plural',
            ];

            if (!array_has_keys($conf, $required_keys)) {
                $this->conf_is_valid = false;
                throw new \Exception("SimpleBlacklistTrait : invalid config");
            }

            // aliases - optional property, set to empty array if not exists
            if (!isset($conf['aliases'])) {
                $conf['aliases'] = [];
            } else {
                // don't allow aliasing these two columns
                unset($conf['aliases']['id']);
                unset($conf['aliases']['subreddit_id']);
            }

            // values - optional property for default values, set to empty array if not exists
            if (!isset($conf['values'])) {
                $conf['values'] = [];
            }

            // sr_idlist - cast to array
            $conf['sr_idlist'] = cast_array($conf['sr_idlist']);

            // --- add special columns if not already present

            $special_columns = [
                $conf['mod_col']        => 'mod_col',
                $conf['time_col']       => 'time_col',
                $conf['subject_col']    => 'subject_col',
            ];

            foreach (array_merge(array_keys($special_columns), ['subreddit_id', 'id']) as $name) {
                if (!in_array($name, $conf['columns'])) {
                    array_unshift($conf['columns'], $name);
                }
            }

            // --- create select string and other properties

            $select_str = '';
            foreach ($conf['columns'] as $col) {
                $name_part  = 'tbl.'.$col;
                $name_alias = from($conf['aliases'], $col);

                // the time column must be converted to timestamp
                if ($col == $conf['time_col']) {
                    $name_part  = "EXTRACT(EPOCH FROM {$name_part})";
                    $name_alias = $name_alias ?? $col;
                }

                if (isset($special_columns[$col])) {
                    $conf[$special_columns[$col].'_alias'] = $name_alias ?? $col;
                }

                $select_str .= $name_part;
                if ($name_alias !== null) {
                    $select_str .= ' as ';
                    $select_str .= $name_alias;
                }
                $select_str .= ', ';

                if ($col == 'subreddit_id') {
                    // put subreddit_name (as "subreddit") directly after subreddit_id
                    $select_str .= 'sr.subreddit_name as subreddit, ';
                }
            }

            $conf['select_str'] = rtrim($select_str, ', ');

            // --- initialize the field

            $this->conf = $conf;
        }

        return $this->conf;
    }

    /**
     * @param $subject
     * @return array
     * @throws \Exception
     */
    public function check($subject) {
        $conf = $this->conf();

        $by_subreddit = array_combine($conf['sr_names'], array_fill(0,count($conf['sr_names']),[]));

        $res = db('Zion')->query("
            SELECT {$conf['select_str']}
            FROM
            (
              (SELECT * FROM {$conf['table']} WHERE {$conf['subject_col']} IN %ls_subjects) as tbl
              LEFT JOIN subreddit sr ON ((tbl.subreddit_id = sr.id))
            )
            WHERE subreddit_id IN %li_srlist
        ", [
            'subjects'  => cast_array($subject),
            'srlist'    => $conf['sr_idlist'],
        ]);

        foreach ($res as $item) {
            $item_subject    = $item[$conf['subject_col_alias']];
            $item_subreddit  = $item['subreddit'];

            $by_subreddit[$item_subreddit][$item_subject] = $item;
        }

        return $by_subreddit;
    }

    /**
     * @param $subject
     * @param null $mod
     * @return array
     * @throws \app\lib\IllegalSubredditException
     * @throws \app\lib\InvalidSubredditException
     * @throws \app\lib\SubredditException
     * @throws \app\lib\UnknownSubredditException
     */
    public function add($subject, $mod = null) {
        $conf = $this->conf();

        $check       = $this->check($subject);
        $sr_idlist   = $this->subreddit->id(true);
        $insert      = [];
        $ret         = [];

        $columns_list = array_unique(array_merge($conf['columns'], array_keys($conf['values'])));

        foreach ($check as $sr => $items) {
            // subtract from "subject" array items that are already banned
            $ban_subjects = array_udiff((array) $subject, array_keys($items), 'strcasecmp');

            foreach ($ban_subjects as $ban_subject) {
                $to_insert = [];
                $to_return = [];

                foreach ($columns_list as $col) {
                    $col_alias = from($conf['aliases'], $col) ?? $col;

                    $value = null;
                    switch ($col) {
                        case 'id':
                            continue;
                        case 'subreddit_id':
                            $value = $sr_idlist[strtolower($sr)];
                            break;
                        case $conf['time_col']:
                            $value = time();
                            break;
                        case $conf['subject_col']:
                            $value = $ban_subject;
                            break;
                        case $conf['mod_col']:
                            $value = $mod ?? user()->get_username();
                            break;
                        default:
                            // check default values
                            if (isset($conf['values'][$col])) {
                                $value = $conf['values'][$col];
                            }
                            break;
                    }

                    if (isset($value)) {
                        $to_insert[$col] = $value; // insert using real names
                        $to_return[$col_alias] = $value; // return using alias names

                        if ($col == 'subreddit_id') {
                            // put subreddit_name (as "subreddit") directly after subreddit_id
                            $to_return['subreddit'] = subreddit($sr)->name;
                        }
                    }
                }

                $insert[] = $to_insert;
                $ret[] = $to_return;
            }
        }

        if (!empty($insert)) {
            db('Zion')->insert($conf['table'], $insert);
        }

        return ['banned' => $ret];
    }

    /**
     * @param $subject
     * @return array
     * @throws \Exception
     */
    public function remove($subject) {
        $conf   = $this->conf();
        $check  = $this->check($subject);
        $ret    = [];

        foreach (array_values($check) as $item) {
            $ret = array_merge($ret, array_values($item));
        }

        if (!empty($ret)) {

            db('Zion')->query("DELETE FROM {$conf['table']} WHERE id IN %li_idlist", [
                'idlist' => array_values(array_map(function($item) {
                    return $item['id'];
                }, $ret)),
            ]);

        }

        return ['unbanned' => $ret];
    }

    /**
     * @param $a
     * @param $b
     * @param string $listing_type
     * @param null $subjects
     * @return array
     * @throws \Exception
     */
    public function list($a, $b, $listing_type = 'offset', $subjects = null) {
        $conf = $this->conf();

        // --- Create subject component (if needed)
        $subject_component = '';
        if (!empty($subjects)) {
            $subjects = (array) $subjects;
            $subject_component = " AND {$conf['subject_col']} IN %ls_subjects";
        }

        // --- Get total amount of items in the blacklist

        $stmt_total  = "SELECT COUNT(*) FROM {$conf['table']} WHERE subreddit_id IN %li_srlist"
                            .$subject_component;
        $total       = db('Zion')->queryFirstField($stmt_total,
                        [
                            'srlist' => (array) $conf['sr_idlist'],
                            'subjects' => $subjects,
                        ]);

        // --- Calculate necessary variables based on $listing_type
        switch ($listing_type) {
            case 'page':
                $page_size  = intval($a);
                $page       = intval($b);
                $page_count = ceil($total / $page_size);

                // if page is negative, then offset from last page
                if ($page <= 0) {
                    $page = $page_count + $page;
                    if ($page < 1) $page = 1;
                }

                // if page is greater than last page, set to last page
                if ($page > $page_count) {
                    $page = $page_count;
                }

                // compute offset & limit to use for query
                $offset     = ($page-1)*$page_size;
                $limit      = $page_size;
                break;
            case 'offset':
                $amount     = intval($a);
                $offset     = intval($b);
                $limit      = $amount;
                break;
            default:
                throw new \InvalidArgumentException('listng_type must either be "page" or "offset"');
        }

        // --- Run query

        if ($total == 0) {
            $listing = [];
        } else {
            $listing = db('Zion')->query($sql_query = "
                SELECT {$conf['select_str']}
                FROM
                (
                  {$conf['table']} tbl
                  LEFT JOIN subreddit sr ON ((sr.id = tbl.subreddit_id))
                )
                WHERE subreddit_id IN %li_srlist {$subject_component}
                ORDER BY {$conf['time_col_alias']} DESC NULLS LAST
                LIMIT {$limit}
                OFFSET {$offset}",
                [
                    'srlist' => (array) $conf['sr_idlist'],
                    'subjects' => $subjects,
                ]);
        }


        // --- Return results

        $ret = [
            'listing'       => $listing,
            'fetch_type'    => $listing_type,
        ];

        switch ($listing_type) {
            case 'page':
                if (!isset($page_size, $page, $page_count)) {
                    throw new \LogicException();
                }
                $ret += [
                    'page_size'     => $page_size,
                    'page'          => $page,
                    'page_count'    => $page_count,
                ];
                break;
            case 'offset':
                if (!isset($amount)) {
                    throw new \LogicException();
                }
                $ret += [
                    'amount'        => $amount,
                    'offset'        => $offset,
                    'total'         => $total,
                ];
                break;
        }
        return $ret;
    }

}