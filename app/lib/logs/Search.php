<?php
namespace app\lib\logs;

class SearchException extends \Exception {
    public function __construct($message = '', $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class Search {
    const MAX_AMOUNT = 300;

    public static function query($asi_query, $amount, $before = null, $after = null, $did_adjust = false) {
        $start_time = round(microtime(true) * 1000);
        $query = self::prepare($asi_query, $amount, $before, $after);
        $out['sqldebug']['prep_rt'] = round(microtime(true) * 1000) - $start_time;

        $out = Common::EMPTY_SEARCH_TEMPLATE;

        if ($query === false) return false;
        if ($query === null)  return $out;

        try {
            db('Zion')->query('SET LOCAL statement_timeout=%i', 30);

            $raw = db('Zion')->query($query['search'], $query['params']);

            db('Zion')->commit();
        } catch(\DBException $e) {
            if ($e->getCode() == 57014) {
                return new SearchException('Search timed out', null, $e);
            } else {
                return new SearchException('An internal error occurred', null, $e);
            }
        }

        $out['sqldebug']['did_adjust']      = $did_adjust;
        $out['sqldebug']['query_asi']       = $asi_query;
        $out['sqldebug']['query_pstmt']     = $query['search'];
        $out['sqldebug']['query_stmt']      = db('Zion')->last_sql();
        $out['sqldebug']['query_params']    = $query['params'];
        $out['sqldebug']['query_rt']        = db('Zion')->runtime();

        if (isset($before) && !empty($raw) && count($raw) < $amount) {
            return self::query($asi_query, $amount, null, null, true);
        }

        $offset_id  = isset($before) ? intval($before) : (isset($after) ? intval($after) : null);
        $reached_id = isset($offset_id) ? false : true;

        $skipped = 0;
        $count   = 0;

        if (!isset($offset_id)) {
            $before = false;
        }

        if ($query['dir'] == 'ASC') {
            $raw = array_reverse($raw);
        }
        foreach ($raw as $r) {
            if (!$reached_id && isset($offset_id)) {
                if ($r['id'] === $offset_id) {
                    $reached_id = true;
                }
                $skipped++;
                continue;
            } else {
                $count++;
            }

            $r_new = [
                'id'            => $r['id'],
                'thingid'       => $r['thing_id'],
                'mod'           => $r['mod'],
                'author'        => $r['author'],
                'action'        => $r['action'],
                'reason'        => $r['actionreason'],
                'description'   => $r['description'],
                'subject_user'  => $r['subject_user'],
                'permalink'     => $r['permalink'],
                'action_utc'    => $r['action_utc'],
                'timestamp'     => $r['action_utc'],
                'human_timing'  => human_timing($r['action_utc']),
                'subreddit'     => $r['subreddit'],
                'modactionid'   => $r['modactionid'],
                'parentthingid' => $r['parent_thing_id'],
                'thing_title'   => $r['thing_title'],
                'thing_data'    => $r['thing_data'],
                'flair_class'   => $r['flair_class'],
                'flair_text'    => $r['flair_class'],
            ];

            if ($query['dir'] == 'ASC') {
                array_unshift($out['listing'], $r_new);
            } else {
                $out['listing'][] = $r_new;
            }

            if ($count >= $amount) {
                if (count($raw) - $skipped - $count <= 0) {
                    $after = false;
                }
                break;
            }
        }

        $search_history_insert_data = [
            'search_user'       => user()->get_username(),
            'search_query'      => $asi_query,
            'search_utc'        => time(),
            'search_amount'     => $amount,
            'search_offset'     => $offset_id,
            'search_offset_dir' => (isset($before) && $before !== false) ?
                                        'before' :
                                        ((isset($after) && $after !== false) ? 'after' : null),
        ];
        if ($search_history_insert_data['search_offset_dir'] === null) {
            unset($search_history_insert_data['search_offset']);
            unset($search_history_insert_data['search_offset_dir']);
        }
        db('application')->insert('search_history', $search_history_insert_data);

        $real_amount = count($out['listing']);

        if ($real_amount < $amount) {
            $after = false;
        }

        if (!empty($out['listing'])) {
            $before = $before !== false ? intval($out['listing'][0]['id']) : null;
            $after  = $after !== false  ? intval($out['listing'][$real_amount - 1]['id']) : null;
        } else {
            $before = $after = null;
        }

        $out['datainfo']['amount']  = $real_amount;
        $out['datainfo']['before']  = $before;
        $out['datainfo']['after']   = $after;
        $out['sqldebug']['skipped'] = $skipped;
        $out['sqldebug']['full_rt'] = round(microtime(true) * 1000) - $start_time;

        return $out;
    }

    public static function prepare($asi_query, $amount, $before = null, $after = null, $filter = '', $filter2 = '') {
        $fn = (new AuthenticatedQueryBuilder($asi_query))->get();

        if ($fn === false || $fn == null)
            return $fn;

        $id         = isset($before) ? intval($before) : (isset($after) ? intval($after) : null);
        $direction  = isset($before) ? 'ASC' : 'DESC';
        $limit      = min(intval($amount), self::MAX_AMOUNT) + 1;

        // add in offset
        if (isset($before)) {
            $filter2 .= "AND ml.action_utc::timestamp >= (SELECT action_utc FROM modlog WHERE id={$id}) ";
        } else if (isset($after)) {
            $filter2 .= "AND ml.action_utc::timestamp <= (SELECT action_utc FROM modlog WHERE id={$id}) ";
        }

        // add in limit
        if (isset($id)) {
            $filter2 .= "ORDER BY ml.action_utc {$direction} LIMIT ({$limit} + ".
                "(SELECT COUNT(*) FROM modlog WHERE action_utc = (SELECT action_utc FROM modlog WHERE id = {$id}))".
            ")";
        } else {
            $filter2 .= " ORDER BY ml.action_utc {$direction} LIMIT {$limit}";
        }

        // return
        return array(
            'search'        => $fn['search']($filter, $filter2),
            'one'           => $fn['one'],
            'total'         => $fn['total'],
            'params'        => $fn['params'],
            'dir'           => $direction,
        );
    }

}