<?php
namespace app\lib\logs;

use app\lib\User;

// The framework cannot intercept the subreddits because the 'query' parameter
// is a JSON string. So this class will compute an intersection between the
// subreddits the current user mods and the subreddits received in the JSON string.
class AuthenticatedQueryBuilder {
    private $asi_query = null;
    private $query_data = null;

    public function __construct($asi_query) {
        if (empty($asi_query)) {
            $asi_query = false;
        } else if (is_string($asi_query)) {
            $asi_query = trim($asi_query);
            if (!startsWith($asi_query, '{')) {
                $asi_query = null;
            } else {
                $asi_query = json_decode(trim($asi_query), true);
            }
        }
        $this->asi_query = $asi_query;
    }

    private function real_get($conf) {
        $gets = [
            'total' => "COUNT(*)",
            'one' => "COUNT(1)",
            'search' =>
               ("ml.id,".
                "ml.mod::text,".
                "ml.action::text,".
                "ml.actionreason::text,".
                "ml.modactionid,".
                "EXTRACT(EPOCH FROM ml.action_utc) as action_utc,".
                "ml.subreddit_id,".
                "ml.author_name as subject_user,".
                "ml.description,".
                "sr.subreddit_name::text as subreddit,".
                "rt.thing_id,".
                "rt.author::text,".
                "EXTRACT(EPOCH FROM rt.created_utc) as created_utc,".
                "EXTRACT(EPOCH FROM rt.edited_utc) as edited_utc,".
                "rt.parent_thing_id,".
                "rt.permalink,".
                "rt.thing_data,".
                "rt.thing_title,".
                "rt.link_url,".
                "rt.flair_class::text,".
                "rt.flair_text::text")
        ];

        $out = $this->real_get_params($conf);

        if ($out === false) {
            return false;
        }

        foreach ($gets as $key => $selectval) {
            $out[$key] = function($additional, $additional2 = '') use ($selectval, $out) {
                $where2 = trim($out['where2'] . ' ' . $additional2);
                $where2 = remove_suffix($where2, [' AND', ' and']);

                return
                "SELECT * FROM (".
                "SELECT " . $selectval.
                " FROM (".
                  "(SELECT * FROM modlog ".
                     "WHERE " . $out['where']." ".$additional.
                  ") as ml ".
                  "LEFT JOIN subreddit sr ON ((sr.id = ml.subreddit_id)) ".
                  "LEFT JOIN reddit_thing rt ON ((rt.thing_id = ml.thing_id))".
                ") WHERE action_utc is not null {$where2}) as x ORDER BY action_utc DESC";
            };
        }

        return ($this->query_data = $out);
    }

    private function real_get_params($tokensconf) {
        $where  = '';
        $where2 = '';
        $params = [];

        try {
            $encountered_in = false;

            // If 'tokens' exists:
            if (!empty($this->asi_query) && !empty($this->asi_query['tokens'])) {
                $tokens_list = [];

                // Merge duplicate (label,modifier) pairs by using a map of:
                // ['<label>(_<optional_modifier>)' => [merged token data]]
                // And then afterwards call array_values on it
                foreach ($this->asi_query['tokens'] as $token) {
                    if (!isset($token['label']) || !isset($token['values'])) {
                        return false;
                    }

                    $name = strtolower($token['label'] . (isset($token['modifier']) ? '_'.$token['modifier'] : ''));

                    if (!isset($tokens_list[$name])) {
                        $tokens_list[$name] = $token;
                    } else {
                        // Merge values
                        $tokens_list[$name]['values'] = array_merge($tokens_list[$name]['values'], $token['values']);

                        // If error, set error
                        if (isset($token['error']) && $token['error'] !== false) {
                            $tokens_list[$name]['error'] = false;
                        }
                    }
                }

                $tokens_list = array_values($tokens_list);

                // Process the list
                foreach ($tokens_list as $token) {
                    $label      = strtolower(from($token, 'label'));
                    $modifier   = from($token, 'modifier');
                    $values     = from($token, 'values');

                    if (!isset($label) || !isset($values)) {
                        return false;
                    }

                    if ((isset($token['error']) && $token['error'] !== false) || count($values) == 0) {
                        return false;
                    }

                    if ($label == 'in') {
                        $encountered_in = true;
                    }

                    if (isset($tokensconf[$label])) {
                        $data = $tokensconf[$label];

                        if (!empty($modifier) && isset($data['modifiers'][$modifier])) {
                            $res = $data['modifiers'][$modifier]['query']($values);
                        } else {
                            $res = $data['query']($values);
                        }

                        if ($res === false) {
                            return false;
                        }

                        $params = array_merge($params, $res['params']);

                        if (!empty($res['query'])) {
                            if (isset($data['part']) && $data['part'] === 2) {
                                $where2 .= $res['query'].' AND ';
                            } else {
                                $where .= $res['query'].' AND ';
                            }
                        }
                    } else {
                        return false;
                    }
                }
            }

            if (!$encountered_in) {
                $res = $tokensconf['in']['query']([]);

                $params = array_merge($params, $res['params']);
                $where .= $res['query'].' AND ';
            }
        } catch(\Exception $e) {
            return false;
        }

        if (!empty($where2)) {
            $where2 = ' AND ' . $where2;
        }

        return [
            'where'  => remove_suffix($where, ' AND '),
            'where2' => remove_suffix($where2, ' AND '),
            'params' => $params,
        ];
    }

    public function get() {
        if (isset($this->query_data)) {
            return $this->query_data;
        }
        if (!isset($this->asi_query)) {
            return false;
        }
        if (empty(self::get_user_subreddits())) {
            return null;
        }

        return $this->real_get(array(
            'in' => array(
                'query' => (function($values) {
                    if (empty($values)) {
                        $srlist = self::get_user_subreddits();
                    } else {
                        $srlist = array_uintersect(self::get_user_subreddits(), $values, 'strcasecmp');
                    }

                    $srlist = array_values($srlist);

                    if (empty($srlist)) {
                        return array(
                            'query' => '',
                            'params' => [],
                        );
                    }

                    return array(
                        'query' => 'subreddit_id IN (SELECT id FROM subreddit WHERE subreddit_name in %ls_srlist)',
                        'params' => ['srlist' => $srlist],
                    );
                }),
            ),
            'after' => array(
                'query' => (function($values) {
                    $timestamp = self::parse_datetime($values[0]);
                    return ($timestamp !== false) ? array(
                        'query' => 'action_utc > %s_tsafter',
                        'params' => ['tsafter' => date('Y-m-d H:i:s', $timestamp)],
                    ) : false;
                }),
            ),
            'before' => array(
                'query' => (function($values) {
                    $timestamp = self::parse_datetime($values[0]);
                    return ($timestamp !== false) ? array(
                        'query' => 'action_utc < %s_tsbefore',
                        'params' => ['tsbefore' => date('Y-m-d H:i:s', $timestamp)],
                    ) : false;
                }),
            ),
            'during' => array(
                'query' => (function($values) {
                    $timestamp = self::parse_datetime($values[0]);
                    return ($timestamp !== false) ? array(
                        'query' => 'action_utc >= %s_tsdate AND action_utc <= %s_tsdateupper',
                        'params' => [
                            'tsdate' => date('Y-m-d H:i:s', $timestamp),
                            'tsdateupper' => date('Y-m-d H:i:s', $timestamp + 86400)
                        ],
                    ) : false;
                }),
            ),
            'mod' => array(
                'query' => (function($values) {
                    if (empty($values) || empty($values[0])) {
                        return array(
                            'query' => '',
                            'params' => [],
                        );
                    }

                    return array(
                        'query' => 'mod IN %ls_modlist',
                        'params' => ['modlist' => $values],
                    );
                }),
            ),
            'action' => array(
                'query' => (function($values) {
                    if (empty($values) || empty($values[0])) {
                        return array(
                            'query' => '',
                            'params' => [],
                        );
                    }

                    return array(
                        'query' => 'action IN %ls_actionlist',
                        'params' => ['actionlist' => $values],
                    );
                }),
            ),
            'reason' => array(
                'query' => (function($values) {
                    $val = str_replace('_','\\_',str_replace('%','\\%',$values[0]));
                    return array(
                        'query' => '(actionreason ILIKE %s_reasonnormal OR description ILIKE %s_reasonnormal)',
                        'params' => ['reasonnormal' => '%'.$val.'%'],
                    );
                }),
                'modifiers' => array(
                    'includes-word' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',str_replace('%','\\%',$values[0]));
                            return array(
                                'query' => '(actionreason ~ %s_reasonincludesword OR description ~ %s_reasonincludesword)',
                                'params' => ['reasonincludesword' => '( |^)'.preg_quote($val).'( |$|\.|\?|!)'],
                            );
                        }),
                    ),
                    'includes' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',str_replace('%','\\%',$values[0]));
                            return array(
                                'query' => '(actionreason ILIKE %s_reasonincludes OR description ILIKE %s_reasonincludes)',
                                'params' => ['reasonincludes' => '%'.$val.'%'],
                            );
                        }),
                    ),
                    'starts-with' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',str_replace('%','\\%',$values[0]));
                            return array(
                                'query' => '(actionreason ILIKE %s_reasonstartswith OR description ILIKE %s_reasonstartswith)',
                                'params' => ['reasonstartswith' => $val.'%'],
                            );
                        }),
                    ),
                    'ends-with' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',str_replace('%','\\%',$values[0]));
                            return array(
                                'query' => '(actionreason ILIKE %s_reasonendswith OR description ILIKE %s_reasonendswith)',
                                'params' => ['reasonendswith' => '%'.$val],
                            );
                        }),
                    ),
                    'full-exact' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => '(actionreason::text=%s_reasonexact OR description::text=%s_reasonexact)',
                                'params' => ['reasonexact' => $values[0]],
                            );
                        }),
                    ),
                    'full-text' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => "(trim(both ' ' from actionreason)=%s_reasontext OR trim(both ' ' from description)=%s_reasontext)",
                                'params' => ['reasontext' => $values[0]],
                            );
                        }),
                    ),
                    'regex' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => '(actionreason ~* %s_reasonregex OR description ~* %s_reasonregex)',
                                'params' => ['reasonregex' => $values[0]],
                            );
                        }),
                    ),
                    'shorter-than' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => '(COALESCE(char_length(actionreason), 0) + COALESCE(char_length(description),0) < %i_reasonst)',
                                'params' => ['reasonst' => $values[0]],
                            );
                        }),
                    ),
                    'longer-than' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => '(COALESCE(char_length(actionreason), 0) + COALESCE(char_length(description),0) > %i_reasonlt)',
                                'params' => ['reasonlt' => $values[0]],
                            );
                        }),
                    ),
                ),
            ),
            'author' => array(
                'part' => 2,
                'query' => (function($values) {
                    if (empty($values) || empty($values[0])) {
                        return array(
                            'query' => '',
                            'params' => [],
                        );
                    }
                    $values = to_lower_array($values);
                    return array(
                        'query' => 'lower(ml.author_name) IN %ls_authorlist',
                        'params' => ['authorlist' => $values],
                    );
                }),
                'modifiers' => array(
                    'includes' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',$values[0]);
                            return array(
                                'query' => 'ml.author_name ILIKE %s_authorincludes',
                                'params' => ['authorincludes' => '%'.$val.'%'],
                            );
                        }),
                    ),
                    'starts-with' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',$values[0]);
                            return array(
                                'query' => 'ml.author_name ILIKE %s_authorstartswith',
                                'params' => ['authorstartswith' => $val.'%'],
                            );
                        }),
                    ),
                    'ends-with' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',$values[0]);
                            return array(
                                'query' => 'ml.author_name ILIKE %s_authorendswith',
                                'params' => ['authorendswith' => '%'.$val],
                            );
                        }),
                    ),
                    'exact' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'ml.author_name::text=%s_authorexact',
                                'params' => ['authorexact' => $values[0]],
                            );
                        }),
                    ),
                    'regex' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'ml.author_name ~* %s_authorregex',
                                'params' => ['authorregex' => $values[0]],
                            );
                        }),
                    ),
                    'shorter-than' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'char_length(ml.author_name) < %i_authorst AND char_length(ml.author_name) > 0',
                                'params' => ['authorst' => $values[0]],
                            );
                        }),
                    ),
                    'longer-than' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'char_length(ml.author_name) > %i_authorlt',
                                'params' => ['authorlt' => $values[0]],
                            );
                        }),
                    ),
                ),
            ),
            'title' => array(
                'part' => 2,
                'query' => (function($values) {
                    $val = str_replace('_','\\_',$values[0]);
                    return array(
                        'query' => 'rt.thing_title ILIKE %s_titlenormal',
                        'params' => ['titlenormal' => '%'.$val.'%'],
                    );
                }),
                'modifiers' => array(
                    'includes' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',$values[0]);
                            return array(
                                'query' => 'rt.thing_title ILIKE %s_titleincludes',
                                'params' => ['titleincludes' => '%'.$val.'%'],
                            );
                        }),
                    ),
                    'starts-with' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',$values[0]);
                            return array(
                                'query' => 'rt.thing_title ILIKE %s_titlestartswith',
                                'params' => ['titlestartswith' => $val.'%'],
                            );
                        }),
                    ),
                    'ends-with' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',$values[0]);
                            return array(
                                'query' => 'rt.thing_title ILIKE %s_titleendswith',
                                'params' => ['titleendswith' => '%'.$val],
                            );
                        }),
                    ),
                    'exact' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'rt.thing_title::text=%s_titleexact',
                                'params' => ['titleexact' => $values[0]],
                            );
                        }),
                    ),
                    'regex' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'rt.thing_title ~* %s_titleregex',
                                'params' => ['titleregex' => $values[0]],
                            );
                        }),
                    ),
                    'shorter-than' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'char_length(rt.thing_title) < %i_titlest AND char_length(rt.thing_title) > 0',
                                'params' => ['titlest' => $values[0]],
                            );
                        }),
                    ),
                    'longer-than' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'char_length(rt.thing_title) > %i_titlelt',
                                'params' => ['titlelt' => $values[0]],
                            );
                        }),
                    ),
                ),
            ),
            'body' => array(
                'part' => 2,
                'query' => (function($values) {
                    $val = str_replace('_','\\_',$values[0]);
                    return array(
                        'query' => 'rt.thing_data ILIKE %s_bodynormal',
                        'params' => ['bodynormal' => '%'.$val.'%'],
                    );
                }),
                'modifiers' => array(
                    'includes' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',$values[0]);
                            return array(
                                'query' => 'rt.thing_data ILIKE %s_bodyincludes',
                                'params' => ['bodyincludes' => '%'.$val.'%'],
                            );
                        }),
                    ),
                    'starts-with' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',$values[0]);
                            return array(
                                'query' => 'rt.thing_data ILIKE %s_bodystartswith',
                                'params' => ['bodystartswith' => $val.'%'],
                            );
                        }),
                    ),
                    'ends-with' => array(
                        'query' => (function($values) {
                            $val = str_replace('_','\\_',$values[0]);
                            return array(
                                'query' => 'rt.thing_data ILIKE %s_bodyendswith',
                                'params' => ['bodyendswith' => '%'.$val],
                            );
                        }),
                    ),
                    'exact' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'rt.thing_data::text=%s_bodyexact',
                                'params' => ['bodyexact' => $values[0]],
                            );
                        }),
                    ),
                    'regex' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'rt.thing_data ~* %s_bodyregex',
                                'params' => ['bodyregex' => $values[0]],
                            );
                        }),
                    ),
                    'shorter-than' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'char_length(rt.thing_data) < %i_bodyst AND char_length(rt.thing_data) > 0',
                                'params' => ['bodyst' => $values[0]],
                            );
                        }),
                    ),
                    'longer-than' => array(
                        'query' => (function($values) {
                            return array(
                                'query' => 'char_length(rt.thing_data) > %i_bodylt',
                                'params' => ['bodylt' => $values[0]],
                            );
                        }),
                    ),
                ),
            ),
            'thingid' => array(
                'query' => (function($values) {
                    if (empty($values) || empty($values[0])) {
                        return array(
                            'query' => '',
                            'params' => [],
                        );
                    }

                    $thing_types = ['t1_', 't2_', 't3_', 't4_', 't5_'];

                    $real_values = [];

                    foreach ($values as $val) {
                        if (!str_contains($val, '_')) {
                            foreach ($thing_types as $t) {
                                $real_values[] = $t.$val;
                            }
                        } else {
                            $real_values[] = $val;
                        }
                    }

                    return array(
                        'query' => 'thing_id IN %ls_thingidlist',
                        'params' => ['thingidlist' => $real_values],
                    );
                }),
            ),
        ));
    }

    private static function get_user_subreddits() {
        return user()->modded('all:logs')->to_array();
    }

    private static function parse_datetime($date_str) {
        // if integer, assume already timestamp
        if (valid_int($date_str)) {
            return intval($date_str);
        }

        $days = ['mon','tue','wed','thu','fri','sat','sun'];

        $date_str = trim(strtolower($date_str));
        $part_len = count(explode(' ', $date_str));
        $date_format = '';

        $only_date = false;

        if (endsWith($date_str, 'am') || endsWith($date_str, 'pm')) {
            $date_format = 'M d Y h:i a';
            if ($part_len <= 4) {
                $date_format = 'M d Y';
                $only_date = true;
            }
            if (str_contains($date_str, ':') && $part_len <= 2) {
                $date_format = 'h:i a';
            }
        } else {
            $date_format = 'M d Y H:i';
            if ($part_len <= 4) {
                $date_format = 'M d Y';
                $only_date = true;
            }
            if (str_contains($date_str, ':') && $part_len <= 2) {
                $date_format = 'H:i';
            }
        }

        if (in_array(substr($date_str, 0, 3),$days)) {
            $date_format = 'D ' . $date_format;
        }

        $date = date_create_from_format($date_format, $date_str);
        if ($date === false) {
            return false;
        } else {
            $date->setTimezone(new \DateTimeZone('Europe/London'));

            if ($only_date) {
                $date->modify('today'); // set to the beginning of the day (which date_create_from_format doesn't do)
            }

            return $date->getTimestamp();
        }
    }
}