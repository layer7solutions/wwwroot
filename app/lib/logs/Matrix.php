<?php
namespace app\lib\logs;

use app\lib\Subreddit;

class Matrix {

    public static function getRowTemplate() {
        return Common::getRowTemplate();
    }

    private static function formatMatrix($raw_matrix, $moderators) {
        $row_template = Common::getRowTemplate();


        $matrix = [];
        $count_list = [];
        $columns = $row_template;
        $rows = [];

        foreach ($moderators as $mod) {
            $matrix[$mod] = $row_template;
            $rows[$mod] = 0;
        }

        $max_count = $min_count = empty($raw_matrix) ? -1 : $raw_matrix[0]['count'];
        $sum_count = 0;

        foreach($raw_matrix as $row) {
            if (!array_key_exists($row['action'], $row_template)) {
                // if this is reached, it means reddit added a new mod action type
                // that isn't in \app\lib\modlogs\Common yet
                continue;
            }

            if (isset($matrix[$row['mod']][$row['action']])) {
                $matrix[$row['mod']][$row['action']] += $row['count'];
            } else {
                $matrix[$row['mod']][$row['action']] = $row['count'];
            }

            $columns[$row['action']] += $row['count'];
            $rows[$row['mod']] += $row['count'];

            if ($row['count'] > $max_count) {
                $max_count = $row['count'];
            }
            if ($row['count'] < $min_count) {
                $min_count = $row['count'];
            }

            $sum_count += $row['count'];
            $count_list[] = $row['count'];
        }

        sort($count_list);

        $trimmed_list = remove_outliers($count_list);

        return [
            'matrix' => $matrix,
            'matrix_columns' => $columns,
            'matrix_rows' => $rows,
            'matrix_minval' => $min_count,
            'matrix_maxval' => $max_count,
            'matrix_sum' => $sum_count,
            'matrix_trimmed_minval' => from($trimmed_list, 0),
            'matrix_trimmed_maxval' => from($trimmed_list, count($trimmed_list) - 1),
        ];
    }

    // generate modlog matrix
    // @params
    //   subreddit   - the subreddit
    //   lower_bound - lower bound timestamp in seconds
    //   upper_bound - upper bound timestamp in seconds
    //   moderators  - array of strings of moderator usernames
    //   actions     - list of actions to get, set to null to get all actions
    public static function genMatrix(
            array $subreddit, int $upper_bound, int $lower_bound, array $moderators, array $actions) {
        $start_time = (microtime(true) * 1000);
        $srcomponent = (array) $subreddit;
        if (empty($actions)) $actions = Common::getActionTypes();

        // Note: explicitly declaring which mods or actions we want to get (even if all of them)
        // in the WHERE clause is always faster.

        $data = self::formatMatrix(
            db('Zion')->query(
            "SELECT mod, action, COUNT(action) AS count FROM modlog_view2 WHERE"
            . " subreddit IN %ls_subreddit"
            . " AND mod IN %ls_moderators"
            . " AND action IN %ls_actions"
            . " AND action_utc BETWEEN %s_timeLowerBound AND %s_timeUpperBound"
            . " GROUP BY mod, action",
            array(
                'subreddit'         => $srcomponent,
                'moderators'        => $moderators,
                'timeLowerBound'    => date('Y-m-d H:i:s', $lower_bound),
                'timeUpperBound'    => date('Y-m-d H:i:s', $upper_bound),
                'actions'           => $actions,
            )),
            $moderators
        );

        db('application')->insert('matrix_history', array(
            'subreddit'     => strtolower(implode(',', $srcomponent)),
            'user'          => user()->get_username(),
            'lower_bound'   => $lower_bound,
            'upper_bound'   => $upper_bound,
            'moderators'    => implode(',', $moderators),
            'actions'       => strtolower(implode(',', $actions)),
            'history_utc'   => time(),
        ));

        return [
            'query_runtime' => db('Zion')->runtime(),
            'matrix_runtime' => (microtime(true) * 1000) - $start_time,
            'matrix_subreddits' => (array) $subreddit,
        ] + $data;
    }
}