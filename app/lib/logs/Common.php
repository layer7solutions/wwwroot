<?php
namespace app\lib\logs;

class Common {
    const MOD_ACTIONS_TEMPLATE = [

        // USER ACCESS
        'banuser'                   => ["name" => "banuser", "group" => "user access", "display_name" => "ban user"],
        'unbanuser'                 => ["name" => "unbanuser", "group" => "user access", "display_name" => "unban user"],
        'muteuser'                  => ["name" => "muteuser", "group" => "user access", "display_name" => "mute user"],
        'unmuteuser'                => ["name" => "unmuteuser", "group" => "user access", "display_name" => "unmute user"],
        'addcontributor'            => ["name" => "addcontributor", "group" => "user access", "display_name" => "add contributor"],
        'removecontributor'         => ["name" => "removecontributor", "group" => "user access", "display_name" => "remove contributor"],

        // POST ENFORCEMENT
        'approvelink'               => ["name" => "approvelink", "group" => "post enforcement", "display_name" => "approve post"],
        'removelink'                => ["name" => "removelink", "group" => "post enforcement", "display_name" => "remove post"],
        'approvecomment'            => ["name" => "approvecomment", "group" => "post enforcement", "display_name" => "approve comment"],
        'removecomment'             => ["name" => "removecomment", "group" => "post enforcement", "display_name" => "remove comment"],
        'spamlink'                  => ["name" => "spamlink", "group" => "post enforcement", "display_name" => "spam post"],
        'spamcomment'               => ["name" => "spamcomment", "group" => "post enforcement", "display_name" => "spam comment"],

        // MOD PRIVILEGE
        'invitemoderator'           => ["name" => "invitemoderator", "group" => "mod privilege", "display_name" => "invite moderator"],
        'uninvitemoderator'         => ["name" => "uninvitemoderator", "group" => "mod privilege", "display_name" => "uninvite moderator"],
        'acceptmoderatorinvite'     => ["name" => "acceptmoderatorinvite", "group" => "mod privilege", "display_name" => "accept moderator invite"],
        'addmoderator'              => ["name" => "addmoderator", "group" => "mod privilege", "display_name" => "add moderator"],
        'removemoderator'           => ["name" => "removemoderator", "group" => "mod privilege", "display_name" => "remove moderator"],
        'setpermissions'            => ["name" => "setpermissions", "group" => "mod privilege", "display_name" => "set mod permissions"],
        'distinguish'               => ["name" => "distinguish", "group" => "mod privilege", "display_name" => "distinguish"],

        // CONFIGURATION
        'editsettings'              => ["name" => "editsettings", "group" => "configuration", "display_name" => "edit community settings"],
        'createrule'                => ["name" => "createrule", "group" => "configuration", "display_name" => "create rule"],
        'deleterule'                => ["name" => "deleterule", "group" => "configuration", "display_name" => "delete rule"],
        'editrule'                  => ["name" => "editrule", "group" => "configuration", "display_name" => "edit rule"],
        'reorderrules'              => ["name" => "reorderrules", "group" => "configuration", "display_name" => "reorder rules"],
        'community_styling'         => ["name" => "community_styling", "group" => "configuration", "display_name" => "change community styling"],
        'community_widgets'         => ["name" => "community_widgets", "group" => "configuration", "display_name" => "change community widgets"],
        'modmail_enrollment'        => ["name" => "modmail_enrollment", "group" => "configuration", "display_name" => "enroll into new modmail"],

        // POST STATUS
        'lock'                      => ["name" => "lock", "group" => "post status", "display_name" => "lock post"],
        'unlock'                    => ["name" => "unlock", "group" => "post status", "display_name" => "unlock post"],
        'sticky'                    => ["name" => "sticky", "group" => "post status", "display_name" => "sticky post"],
        'unsticky'                  => ["name" => "unsticky", "group" => "post status", "display_name" => "unsticky post"],
        'ignorereports'             => ["name" => "ignorereports", "group" => "post status", "display_name" => "ignore reports"],
        'unignorereports'           => ["name" => "unignorereports", "group" => "post status", "display_name" => "unignore reports"],
        'spoiler'                   => ["name" => "spoiler", "group" => "post status", "display_name" => "mark post as spoiler"],
        'unspoiler'                 => ["name" => "unspoiler", "group" => "post status", "display_name" => "unmark post as spoiler"],
        'marknsfw'                  => ["name" => "marknsfw", "group" => "post status", "display_name" => "mark post as NSFW"],
        'setsuggestedsort'          => ["name" => "setsuggestedsort", "group" => "post status", "display_name" => "set suggested sort"],
        'setcontestmode'            => ["name" => "setcontestmode", "group" => "post status", "display_name" => "enable contest mode"],
        'unsetcontestmode'          => ["name" => "unsetcontestmode", "group" => "post status", "display_name" => "disable contest mode"],
        'markoriginalcontent'       => ["name" => "markoriginalcontent", "group" => "post status", "display_name" => "mark as original content"],

        // WIKI
        'wikibanned'                => ["name" => "wikibanned", "group" => "wiki", "display_name" => "ban wiki contributor"],
        'wikicontributor'           => ["name" => "wikicontributor", "group" => "wiki", "display_name" => "add wiki contributor"],
        'wikiunbanned'              => ["name" => "wikiunbanned", "group" => "wiki", "display_name" => "unban wiki contributor"],
        'wikipagelisted'            => ["name" => "wikipagelisted", "group" => "wiki", "display_name" => "change wiki page listing"],
        'removewikicontributor'     => ["name" => "removewikicontributor", "group" => "wiki", "display_name" => "remove wiki contributor"],
        'wikirevise'                => ["name" => "wikirevise", "group" => "wiki", "display_name" => "edit wiki page"],
        'wikipermlevel'             => ["name" => "wikipermlevel", "group" => "wiki", "display_name" => "change wiki page access"],

        // COMMUNITY
        'editflair'                 => ["name" => "editflair", "group" => "community", "display_name" => "edit user or post flair"],
        'create_award'              => ["name" => "create_award", "group" => "community", "display_name" => "create award"],
        'disable_award'             => ["name" => "disable_award", "group" => "community", "display_name" => "disable award"],
        'delete_award'             => ["name" => "delete_award", "group" => "community", "display_name" => "delete award"],
        'mod_award_given'           => ["name" => "mod_award_given", "group" => "community", "display_name" => "mod award given"],
        'collections'               => ["name" => "collections", "group" => "community", "display_name" => "collections"],
        'add_community_topics'      => ["name" => "add_community_topics", "group" => "community", "display_name" => "add community topics"],
        'remove_community_topics'   => ["name" => "remove_community_topics", "group" => "community", "display_name" => "remove community topics"],
        'events'                    => ["name" => "events", "group" => "community", "display_name" => "events"],
    ];

    const EMPTY_SEARCH_TEMPLATE = [
        'listing'  => [],
        'datainfo' => [
            'amount'    => 0,
            'before'    => null,
            'after'     => null,
        ],
        'sqldebug' => []
    ];

    public static function getRowTemplate() {
        static $_cache;
        return isset($_cache) ? $_cache : $_cache = array_fill_keys(self::getActionTypes(), 0);
    }

    public static function getActionTypes() {
        static $_cache;
        return isset($_cache) ? $_cache : $_cache = array_keys(self::MOD_ACTIONS_TEMPLATE);
    }

    public static function getActionTypesByName() {
        return self::MOD_ACTIONS_TEMPLATE;
    }

    public static function getActionTypesByGroup() {
        static $_cache;
        if (isset($_cache)) {
            return $_cache;
        }

        $res = [];

        foreach (self::MOD_ACTIONS_TEMPLATE as $name => $properties) {
            $group = $properties['group'];

            if (!isset($res[$group])) {
                $res[$group] = [];
            }

            $res[$group][$name] = $properties;
        }

        return $_cache = $res;
    }

}