<?php
namespace app\lib\logs;

class Logs {
    const SECONDS_IN_HOUR   = 3600;
    const SECONDS_IN_DAY    = 86400;
    const SECONDS_IN_WEEK   = 604800;
    const SECONDS_IN_MONTH  = 2628000;
    const SECONDS_IN_YEAR   = 31540000;

    const ONLY_NAMES = 0;
    const BY_GROUP = 1;
    const BY_NAME = 2;

    public static function get_action_types($get = self::ONLY_NAMES) {
        switch ($get) {
            case self::BY_GROUP:
                return Common::getActionTypesByGroup();
            case self::BY_NAME:
                return Common::getActionTypesByName();
            case self::ONLY_NAMES:
            default:
                return Common::getActionTypes();
        }
    }

    public static function get_action_display_name($name): string {
        return Common::MOD_ACTIONS_TEMPLATE[$name]['display_name'];
    }

    public static function stats_by_action($lowerbound, $upperbound = null, $subreddits = null) {
        // if 'subreddits' not provided, use the current user's subreddits
        if (!isset($subreddits) && user()->logged_in()) {
            $subreddits = user()->modded('all:logs')->to_array();
        }

        $subreddits = (array) $subreddits;

        if (empty($subreddits)) {
            return Common::getRowTemplate();
        }

        if (!isset($upperbound)) {
            $upperbound = time();
        } else if (valid_int($upperbound)) {
            $upperbound = intval($upperbound);
        }

        if (!isset($lowerbound)) {
            $lowerbound = -1 * self::SECONDS_IN_HOUR;
        } else if (valid_int($lowerbound)) {
            $lowerbound = intval($lowerbound);
        }

        if (is_string($lowerbound)) {
            switch (strtolower($lowerbound)) {
                case 'alltime':
                case 'all_time':
                    $lowerbound = 0;
                    break;
                case 'year':
                    $lowerbound = -1 * self::SECONDS_IN_YEAR;
                    break;
                case 'month':
                    $lowerbound = -1 * self::SECONDS_IN_MONTH;
                    break;
                case 'week':
                    $lowerbound = -1 * self::SECONDS_IN_WEEK;
                    break;
                case 'day':
                    $lowerbound = -1 * self::SECONDS_IN_DAY;
                    break;
                case 'hour':
                default:
                    $lowerbound = -1 * self::SECONDS_IN_HOUR;
                    break;
            }
        }

        // if lowerbound is negative, then set it to the distance from upperbound using this value
        if ($lowerbound < 0) {
            $lowerbound = $upperbound + $lowerbound; // addition b/c lowerbound is negative
        }

        $result = db('Zion')->query(
        "SELECT
          action::text,
          COUNT(1) AS count
        FROM
          modlog_view2
        WHERE
          subreddit IN %ls_subreddits
        AND
          action_utc < to_timestamp(%i_upperbound)
        AND
          action_utc > to_timestamp(%i_lowerbound)
        GROUP BY
          action::text",
        [
            'subreddits' => $subreddits,
            'upperbound' => $upperbound,
            'lowerbound' => $lowerbound,
        ]);

        $action_map = Common::getRowTemplate();

        if (empty($result)) {
            return $action_map;
        }

        foreach ($result as $row) {
            $action_map[$row['action']] = $row['count'];
        }

        return $action_map;
    }

    // Gets the oldest record out of the given subreddits
    public static function get_oldest_record($subreddits) {
        if (empty($subreddits)) {
            return null;
        }

        if (is_array($subreddits) && count($subreddits) == 1) {
            $subreddits = $subreddits[0];
        }

        $where_component =
            'WHERE m2.subreddit_id IN (SELECT id FROM subreddit WHERE subreddit_name IN %ls_srlist)';

        if (is_string($subreddits) && $subreddits == 'all' || empty($subreddits)) {
            if (user()->admin()) {
                $where_component = '';
            } else {
                $subreddits = user()->modded('all:logs')->to_array();
            }
        }

        $subreddits = to_lower_array($subreddits);

        return db('Zion')->queryFirstRow(
            "SELECT
                id,
                thing_id,
                mod::text,
                action::text,
                actionreason::text,
                modactionid,
                EXTRACT(EPOCH FROM action_utc) as action_utc,
                subreddit_id,
                author_name as subject_user,
                description
             FROM modlog m1
             WHERE action_utc=
                (SELECT MIN(action_utc) FROM modlog m2 {$where_component} LIMIT %i_srlen)
            ",
            [
                'srlist' => $subreddits,
                'srlen' => count($subreddits)
            ]);
    }

    public static function search() {
        return call_user_func_array('\app\lib\logs\Search::query', func_get_args());
    }
    public static function prepare_query() {
        return call_user_func_array('\app\lib\logs\Search::prepare', func_get_args());
    }

}