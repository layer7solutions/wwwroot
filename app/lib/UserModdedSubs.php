<?php
namespace app\lib;

use app\lib\Subreddit;
use app\lib\Common;
use app\lib\services\WebsyncProcess;
use app\lib\UserModule;
use app\lib\providers\SubredditSourceProvider;
use app\lib\providers\UserModdedProvider;

class UserModdedSubs extends Subreddit {
    use UserModule;

    /**
     * @var bool
     */
    private $is_all;

    public function __construct(User $user, $srIn, SubredditSourceProvider $provider) {
        parent::__construct($srIn, $provider);

        $this->is_all   = $srIn === 'all';
        $this->parent   = $user;
        $this->username = $user->get_username();
    }

    // GENERAL FUNCTIONS
    // ------------------------------------------------------------------------------------------
    public function history(int $limit = null): array {
        if (!isset($this->username)) {
            return [];
        }

        return db('application')->query(
            'SELECT * FROM websync_history WHERE moderator=%s ORDER BY "id" DESC ' .
            (isset($limit) ? ' LIMIT %i' : ''),
            $this->username, $limit);
    }

    public function has_category($category): bool {
        return $this->provider->has_category($category);
    }

    public function has($subreddits): bool {
        if (!isset($this->username)) {
            return false;
        }
        if ($this->is_all) {
            return $this->parent->admin() || $this->provider->has($subreddits);
        } else {
            return parent::has($subreddits);
        }
    }

    /**
     * Check if the User mods at least one of the given subreddits.
     *
     * @param string|string[] $subreddits
     * @return bool
     */
    public function has_any($subreddits): bool {
        if (!isset($this->username)) {
            return false;
        }

        if ($this->parent->admin()) {
            return true;
        }

        if ($this->is_all) {
            foreach ((array) $subreddits as $subreddit) {
                if ($this->provider->has($subreddit)) {
                    return true;
                }
            }
            return false;
        } else {
            return parent::has_any($subreddits);
        }
    }

    // PUBLIC HELPER FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Get the User's permissions on the specified subreddit.
     *
     * @param string        $for_display=false a string will be returned if true, an array otherwise
     * @return string|array an array of permissions or `['all']` if full permissions, or a string
     *                      if $for_display is true
     */
    public function get_perms(string $subreddit, bool $for_display = false) {
        $query = 'SELECT mod_permissions FROM user_modded WHERE username=%s AND subreddit=%s';
        $perms = db('application')->queryFirstField($query, $this->username, strtolower($subreddit));
        $perms = explode(',', $perms);
        return $for_display ? Common::format_reddit_perms($perms) : $perms;
    }

    /**
     * Check if the User has the specified permission on the specified subreddit.
     *
     * @return bool true if User has the permission or has full permissions, false otherwise
     */
    public function has_perm(string $subreddit, string $perm): bool {
        $perms = $this->get_perms($subreddit);
        return $perms[0] === 'all' || in_array($perm, $perms);
    }

    /**
     * Check if the User has full permissions on the specified subreddit.
     *
     * @return bool
     */
    public function has_full_perms(string $subreddit): bool {
        return $this->get_perms($subreddit)[0] === 'all';
    }

    // UPDATE FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Update the user_modded for a specific subreddit for the current user.
     *
     * @return array|false record array if successful, false otherwise
     * @throws \OAuth2Client\Exception
     */
    public function update_specific(string $subreddit) {
        if (!isset($this->username)) {
            return false;
        }

        $subreddit = trim($subreddit);

        $res = $this->parent->client()->fetch("https://oauth.reddit.com/r/$subreddit/about.json");

        if (empty($res['result']['data']) || $res['result']['kind'] != 't5') {
            return false;
        }

        $record = $this->fetch_record($res['result']['data']);

        if (!$record['user_is_moderator']) {
            return false;
        }

        db('application')->delete('user_modded', 'username=%s AND subreddit=%s', $this->username, $record['subreddit']);
        db('application')->insert('user_modded', $record);

        $this->provider->add_subreddits([$record['subreddit']]);

        return $record;
    }

    /**
     * Update all user_modded records for the current user.
     *
     * @return array|false array of record arrays if successful, false otherwise
     * @throws \OAuth2Client\Exception
     */
    public function update_all() {
        if (!isset($this->username)) {
            return false;
        }

        $records = $this->fetch_modded_list();

        if (!empty($records)) {
            db('application')->delete('user_modded', 'username=%s', $this->username);
            db('application')->insert('user_modded', $records);
        }

        // clear websync_flags, since this function updates everything
        WebsyncProcess::clear_flags($this->username);
        $this->provider->reset_subreddits();
        $this->parent->property('last_modded_resync', time());

        return $records;
    }

    // REDDIT API-FETCH FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * Get the moderator list for the given subreddit using this User's client.
     *
     * @param string $subreddit
     * @return array    associative array of moderators (moderator => details), in order from
     *                  highest to lowest
     * @throws \OAuth2Client\Exception
     */
    protected function fetch_mod_list(string $subreddit): array {
        if (!isset($this->username)) {
            return [];
        }

        $res = $this->parent->client()->fetch(
                    "https://oauth.reddit.com/r/{$subreddit}/about/moderators.json");

        if (!isset($res['result']['data']['children'])) {
            return [];
        }

        $map = [];

        foreach ($res['result']['data']['children'] as $mod) {
            $map[strtolower($mod['name'])] = [
                'mod_permissions'   => $mod['mod_permissions'],
                'date'              => $mod['date'],
                'id'                => $mod['id'],
                'name'              => $mod['name'],
            ];
        }

        return $map;
    }

    /**
     * Create a user_modded record from some subreddit data.
     *
     * @param array $data the subreddit data from reddit
     * @return array    user_modded record
     * @throws \OAuth2Client\Exception
     */
    protected function fetch_record(array $data): array {
        if (!isset($data['mod_permissions'])) {
            $data['mod_permissions'] =
                from(
                    from(
                        $this->fetch_mod_list($data['display_name']),
                        strtolower($this->username)
                    ),
                    'mod_permissions'
                );
        }
        $data = SubredditMethods::data_pipe($data, true);
        $data['username'] = $this->username;
        $data['subreddit'] = strtolower($data['name']);
        $data['record_last_updated'] = time();
        return $data;
    }

    /**
     * Gets subreddit data for all subreddits that the user moderates.
     *
     * @return array
     * @throws \OAuth2Client\Exception
     */
    public function fetch_modded_list(): array {
        if (!isset($this->username)) {
            return [];
        }

        $subs = [];

        $limit = 100;
        $requests = 0;
        for ($last_id = null;;) {
            // if over 3 requests (300 subreddits), sleep for 1 second between each request
            if ($requests > 3) {
                sleep(1);
            }

            if (isset($last_id)) {
                $response = $this->parent->client()->fetch(
                    "https://oauth.reddit.com/reddits/mine/moderator.json?limit={$limit}&after={$last_id}");
                $requests += 1;
            } else {
                $response = $this->parent->client()->fetch(
                    "https://oauth.reddit.com/reddits/mine/moderator.json?limit={$limit}");
                $requests += 1;
            }

            if (empty($response['result']['data']['children'])) { // no data left, break out of loop
                break;
            }

            $amount = 0;
            foreach ($response['result']['data']['children'] as $sub_obj) {
                $amount++;
                $subs[] = $this->fetch_record($sub_obj['data']);
                $last_id = $sub_obj['data']['name']; // get the t5_<whatever> id to use for the "after" parameter
            }

            // if amount of subs returned is less than the limit, then there's no need
            // to make another request with the after param, so break out of loop
            if ($amount < $limit) {
                break;
            }
        }

        return $subs;
    }

}