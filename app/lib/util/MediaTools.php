<?php
namespace app\lib\util;

use app\lib\Common;
use app\lib\RT;

class MediaTools {
    const CACHED_ENABLED        = true;
    const CACHED_TIME           = 60 * 60 * 24 * 7; // 1 week
    const PLATFORM_UNKNOWN      = 'unknown';        // 0
    const PLATFORM_YOUTUBE      = 'youtube';        // 1
    const PLATFORM_VIMEO        = 'vimeo';          // 2
    const PLATFORM_VIDME        = 'vidme';          // 3
    const PLATFORM_DAILYMOTION  = 'dailymotion';    // 4
    const PLATFORM_INSTAGRAM    = 'instagram';      // 5
    const PLATFORM_SOUNDCLOUD   = 'soundcloud';     // 6
    const PLATFORM_STREAMABLE   = 'streamable';     // 7
    const PLATFORM_TWITCH       = 'twitch';         // 8
    const PLATFORM_IMGUR        = 'imgur';          // 9
    const PLATFORM_TWITTER      = 'twitter';        // 10
    const PLATFORM_FACEBOOK     = 'facebook';       // 11
    const PLATFORM_ETSY         = 'etsy';           // 12
    const PLATFORM_REDDIT       = 'reddit';         // no id

    // ALL IN ONE
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function platform($media_url) {
        if (empty($media_url))
            return self::PLATFORM_UNKNOWN;

        $regex =
            '/^(?:(?:https?:\/\/)?(?:(?:.*?)|www\.)?)?(?:'.
            '(youtube)(?:\.com)|(youtu\.be)|(youtube)(?:\.[a-zA-Z]{2}\.[a-zA-Z]{2})|'.
            '(twitter)(?:\.com)|(vimeo)(?:\.com)|(vid\.me)|(dailymotion)(?:\.com)|(dai\.ly)|'.
            '(soundcloud)(?:\.com)|(twitch\.tv)|(instagram)(?:\.com)|(imgur)(?:\.com)|'.
            '(streamable)(?:\.com)|(facebook)(?:\.com)|(etsy)(?:\.com)|(reddit)(?:\.com)|(redd\.it)'.
            ')\/(?:.*?)$/i';
        $matches = Common::matcher($media_url, $regex);

        if (count($matches) >= 1) {
            switch (strtolower($matches[0])) {
                // #1
                case 'youtube':
                case 'youtu.be':
                    return self::PLATFORM_YOUTUBE;
                // #2
                case 'vimeo':
                    return self::PLATFORM_VIMEO;
                // #3
                case 'vid.me':
                    return self::PLATFORM_VIDME;
                // #4
                case 'dailymotion':
                case 'dai.ly':
                    return self::PLATFORM_DAILYMOTION;
                // #5
                case 'instagram':
                    return self::PLATFORM_INSTAGRAM;
                // #6
                case 'soundcloud':
                    return self::PLATFORM_SOUNDCLOUD;
                // #7
                case 'streamable':
                    return self::PLATFORM_STREAMABLE;
                // #8
                case 'twitch':
                case 'twitch.tv':
                    return self::PLATFORM_TWITCH;
                // #9
                case 'imgur':
                    return self::PLATFORM_IMGUR;
                // #10
                case 'twitter':
                case 't.co':
                    return self::PLATFORM_TWITTER;
                // #11
                case 'facebook':
                    return self::PLATFORM_FACEBOOK;
                // #12
                case 'etsy':
                    return self::PLATFORM_ETSY;
                // no id
                case 'reddit':
                case 'redd.it':
                    return self::PLATFORM_REDDIT;
            }
        }

        return self::PLATFORM_UNKNOWN;
    }

    public static function platform_id($platform_name) {
        static $_cache = [];

        $platform_name = strtolower($platform_name);

        if (isset($_cache[$platform_name])) {
            return $_cache[$platform_name];
        }

        return $_cache[$platform_name] = db('Zion')->queryFirstField(
            'SELECT id FROM media_platform WHERE platform_name=%s', $platform_name);
    }

    // Get media platform and author ID & Name from a media url
    //
    // @returns
    //   [platform=>..., id=>..., author=>...] -> if valid
    //   false -> if valid platform but invalid URL
    //   null -> if invalid platform
    public static function info($media_url) {
        $platform = self::platform($media_url);

        $cached_key = 'mediainfo_'.remove_prefix(
            trim(trim(preg_replace("(^https?://)", '', $media_url), '/ ')), 'www.');

        if (self::CACHED_ENABLED && $cached_info = cached($cached_key)) {
            return $cached_info;
        }

        switch ($platform) {
            case self::PLATFORM_YOUTUBE:
                if (($raw = self::info_yt($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_VIMEO:
                if (($raw = self::info_vimeo($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_VIDME:
                if (($raw = self::info_vidme($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_DAILYMOTION:
                if (($raw = self::info_dailymotion($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_INSTAGRAM:
                if (($raw = self::info_instagram($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_SOUNDCLOUD:
                if (($raw = self::info_soundcloud($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_STREAMABLE:
                if (($raw = self::info_streamable($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_TWITCH:
                if (($raw = self::info_twitch($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_IMGUR:
                if (($raw = self::info_imgur($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_TWITTER:
                if (($raw = self::info_twitter($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_FACEBOOK:
                if (($raw = self::info_facebook($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            case self::PLATFORM_ETSY:
                if (($raw = self::info_etsy($media_url)) !== false) {
                    list($id, $author, $url, $response) = $raw;
                }
                break;
            /** @noinspection PhpMissingBreakStatementInspection */
            case self::PLATFORM_REDDIT:
                $thing_id = RT::id_from_link($media_url);
                $lookup   = RT::get_media_info($thing_id);

                if (empty($lookup)) {
                    $data = RT::fetch_submission_data($thing_id);
                    if ($data['is_self'] != true) {
                        $new_url = from($data, 'url');
                    }
                } else {
                    $lookup = reset($lookup);
                    $new_url = from($lookup, 'url');
                }

                if (isset($new_url)) {
                    return self::info($new_url);
                }
                // fall-through if $new_url not set
            case self::PLATFORM_UNKNOWN: default:
                $platform = self::PLATFORM_UNKNOWN;
                break;
        }

        if (!isset($id)) {
            $id = null;
            $author = null;
            $response = null;
            $url = null;
        }

        if (!isset($author)) $author = null;
        if (!isset($response)) $response = null;
        if (!isset($url)) $url = null;

        if (isset($cached_key)) {
            cached($cached_key, array(
                'platform'  => $platform,
                'id'        => (string) $id,
                'author'    => (string) $author,
                'url'       => add_protocol((string) $url),
                'response'  => 'from_cache',
            ), self::CACHED_TIME);
        }

        return [
            'platform'  => $platform,
            'id'        => (string) $id,
            'author'    => (string) $author,
            'url'       => add_protocol((string) $url),
            'response'  => $response,
        ];
    }

    // YOUTUBE #1
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private static function call_yt_api($type, $id) {
        $rooturl = 'https://www.googleapis.com/youtube/v3/';
        $jsonurl = $rooturl . [
            'username'  => 'channels?part=snippet&forUsername='.$id.'&fields=items(id%2Csnippet%2Ftitle)&key='.GAPI_KEY,
            'channel'   => 'channels?part=snippet&id='.$id.'&fields=items(id%2Csnippet%2Ftitle)&key='.GAPI_KEY,
            'video'     => 'videos?part=snippet&id='.$id.'&fields=items(snippet(channelId%2CchannelTitle))&key='.GAPI_KEY,
            'playlist'  => 'playlists?part=snippet&id='.$id.'&fields=items(snippet(channelId%2CchannelTitle))&key='.GAPI_KEY,
            'plvideos'  => 'playlistItems?part=snippet&maxResults=50&playlistId='.$id.'&fields=items(contentDetails%2FvideoId%2Csnippet(channelId%2CchannelTitle))&key='.GAPI_KEY,
        ][$type];
        return self::call_json($jsonurl);
    }

    private static function get_yt_channel_id_unknown($url) {
        $html = self::call(add_protocol($url), [
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0',
            'dnt' => 1,
        ]);

        if ($html === false) {
            return false;
        }

        $a = strpos($html, 'itemprop="channelId"');
        if ($a !== false) {
            $i = strpos($html, 'content="', $a) + strlen('content="');
            if ($i !== false) {
                $j = strpos($html, '"', $i);
                if ($j !== false) {
                    return trim(substr($html, $i, $j - $i), "'\", ");
                };
            }
        }

        $b = strpos($html, 'window["ytInitialData"]');
        if ($b !== false) {
            $i = strpos($html, '"channelId":', $b) + strlen('"channelId":');
            if ($i !== false) {
                $j = strpos($html, ',', $i);
                if ($j !== false) {
                    return trim(substr($html, $i, $j - $i), "'\", ");
                }
            }
        }

        return false;
    }

    // Get the channel ID and author name from the given YouTube URL
    // Returns false if invalid.
    public static function info_yt($media_url) {
        $parts = explode('?', $media_url, 2);
        if (count($parts) == 2) {
            if (endsWith(strtolower($parts[0]), 'attribution_link')) {
                $media_url = 'youtube.com'.rawurldecode(from(proper_parse_str($parts[1]), 'u'));
            }
        }
        // (?:                                     # domain group
        //     youtube\.                             # match domain name
        //     (?:com|[a-zA-Z]{2}\.[a-zA-Z]{2})      # match tld: .com, .co, and **.** (for .co.jp, etc.)
        // )\/(?:                                  # BEGIN primary group
        //     (channel|user)\/(.*?)(?:#|\/|\?|$)    # channels and users
        //     |                                     # OR
        //     (?:                                   # playlists & videos
        //         (?:                                 # IF NOT "watch"
        //             (?:                               # IF playlist or embed playlist
        //                 (?:embed|v|playlist)            # playlist links OR embed links for playlists
        //                 (?:\/(?:videoseries)?)?         # optional "/" and "videoseries"
        //                 \?(?:(list)=|(?:.*?)&(list)=)   # skip past everything up to right after "list="
        //             )|                                # ELSE
        //             (embed)\/|(v)\/                     # regular embed link
        //         )|(watch)                           # IF "watch"
        //             (?:.*?)(?:\?|&)(?:v=)             # skip past everything up to right after "v="
        //     )([a-zA-Z0-9_\-]+)                      # match playlist and video IDs (base64)
        // )                                       # END primary group
        // |                                       # OR
        // (youtu.be)\/([a-zA-Z0-9_\-]+)           # match youtu.be links
        $regex = '/(?:youtube\.(?:com?|[a-zA-Z]{2}\.[a-zA-Z]{2}))\/(?:(channel|user|c)\/(.*?)(?:#|\/|\?|$)|(?:(?:(?:(?:embed|v|playlist)(?:\/(?:videoseries)?)?\?(?:(list)=|(?:.*?)&(list)=))|(embed)\/|(v)\/)|(watch)(?:.*?)(?:\?|&)(?:v=))([a-zA-Z0-9_\-]+))|(youtu.be)\/([a-zA-Z0-9_\-]+)/i';
        $matches = Common::matcher($media_url, $regex);
        $json = null;

        if (count($matches) === 2) {
            $id = $matches[1];
            switch ($matches[0]) {
                case 'watch':
                case 'v':
                case 'embed':
                case 'youtu.be':
                    $json = self::call_yt_api('video', $id);

                    if (isset($json['items'][0]['snippet'])) {
                        $channel_id     = $json['items'][0]['snippet']['channelId'];
                        $channel_author = $json['items'][0]['snippet']['channelTitle'];
                    }
                    break;
                case 'list':
                    $json = self::call_yt_api('playlist', $id);

                    if (isset($json['items'][0]['snippet'])) {
                        $channel_id     = $json['items'][0]['snippet']['channelId'];
                        $channel_author = $json['items'][0]['snippet']['channelTitle'];
                    }
                    break;
                case 'channel':
                    $json = self::call_yt_api('channel', $id);

                    if (isset($json['items'][0])) {
                        $channel_id     = $json['items'][0]['id'];
                        $channel_author = $json['items'][0]['snippet']['title'];
                    }
                    break;
                case 'user':
                    $json = self::call_yt_api('username', $id);

                    if (isset($json['items'][0])) {
                        $channel_id     = $json['items'][0]['id'];
                        $channel_author = $json['items'][0]['snippet']['title'];
                    }
                    break;
                case 'c':
                    // vanity URLs
                    $id = self::get_yt_channel_id_unknown($media_url);
                    if ($id === false) {
                        return false;
                    }

                    return self::info_yt('https://youtube.com/channel/'.$id);
            }
        }

        if (isset($channel_id) && isset($channel_author)) {
            $channel_url = 'https://www.youtube.com/channel/'.$channel_id;
            return [$channel_id, $channel_author, $channel_url, $json];
        }

        $real_pages = [
            'user','v','watch','channel','feed','playlist','reporthistory','account','red','yt','t',
            'testtube','upload','webcam','dashboard','logout','embed','results','create_channel',
            'my_videos', 'view_all_playlists','live_dashboard','my_live_events','webcam','comments',
            'messages','subscribers','comment_management','credits','features','upload_defaults',
            'branding','advanced_settings','analytics','timedtext_cs_queue','timedtext_cs_panel',
            'audiolibrary','music_policies'];

        $parts2 = explode('//', $parts[0], 2); // split protocol
        $parts3 = explode('/', $parts2[1] ?? $parts2[0]);
        array_shift($parts3); // remove domain

        // check against $real_pages -> don't do anything for pages we know aren't vanity URLs
        if (count($parts3) && !in_array(strtolower($parts3[0]), $real_pages) || startsWith($parts3[0],'account')) {
            $id = self::get_yt_channel_id_unknown($media_url);
            if ($id === false) {
                return false;
            }
            return self::info_yt('https://youtube.com/channel/'.$id);
        }

        return false;
    }

    // VIMEO #2
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_vimeo($media_url) {
        $media_url = strtok(get_final_url($media_url), '?');
        $parts = Common::matcher($media_url, '/(?:vimeo\.com\/)([^\/]*)(?:(?:\/)([^\/]*))?(?:(?:\/)([^\/]*))?/i');

        if (empty($parts)) {
            return false;
        }

        for ($i = 0; $i < count($parts); $i++) {
            $part = $parts[$i];

            if ($i == 1 && (strtolower($parts[0]) == 'channels' || strtolower($parts[0]) == 'groups')) {
                $call_type = 'playlist';
                $call_subject = $part;

                if (isset($parts[2]) && valid_int($parts[2])) {
                    $call_type = 'video';
                    $call_subject = $parts[2];
                }

                break;
            }

            if (valid_int($part)) {
                $call_subject = $part;

                if ($i == 0) {
                    $call_type = 'video';
                } else if ($i == 1) {
                    switch (strtolower($parts[0])) {
                        case 'video':
                            $call_type = 'video';
                            break;
                    }
                }

                break;
            }
        }

        if (!isset($call_subject)) {
            $call_type = 'user';
            $call_subject = $parts[0];
        }

        if (!isset($call_type) || !isset($call_subject)) {
            return false;
        }

        $json = self::call_json(sprintf([
            'user'      => 'https://api.vimeo.com/users/%s?fields=uri,name,link',
            'playlist'  => 'https://api.vimeo.com/channels/%s?fields=user.uri,user.name,user.link',
            'video'     => 'https://api.vimeo.com/videos/%s?fields=user.name,user.uri,user.link'
        ][$call_type], $call_subject), 'Authorization: Bearer '.VIMEO_KEY);

        if (isset($json['user'])) {
            $json = $json['user'];
        }

        $channel_id     = from($json, 'uri');
        $channel_author = from($json, 'name');
        $channel_url    = from($json, 'link');

        if (isset($channel_id) && isset($channel_author)) {
            return [$channel_id, $channel_author, $channel_url, $json];
        }

        return false;
    }

    // VIDME #3
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_vidme($media_url) {
        return false; // vid.me has shut down
        /*
        $parts = Common::matcher($media_url, '/vid\.me\/(.*)/i');

        if (empty($parts)) {
            return false;
        }

        // Try video first
        $json = self::call_json('https://api.vid.me/videoByUrl?url='.add_protocol($media_url));
        $user = null;

        if (isset($json['video']['user'])) {
            $user = $json['video']['user'];
        } else {
            // If not video, try user
            $json = self::call_json('https://api.vid.me/userByUsername/'.$parts[0]);
            if (isset($json['user'])) {
                $user = $json['user'];
            }
        }

        // If nothing worked, bail out
        if (empty($user)) {
            return false;
        }

        $channel_id     = from($user, 'user_id');
        $channel_author = from($user, 'username');
        $channel_url    = from($user, 'full_url');

        if (isset($channel_id) && isset($channel_author)) {
            return [$channel_id, $channel_author, $channel_url, $json];
        }

        return false;*/
    }

    // DAILYMOTION #4
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_dailymotion($media_url) {
        $regex = '/dailymotion\.com\/(playlist|video)\/([^\/_]*)|(dai\.ly)\/([^\/_]*)|(dailymotion\.com)\/([^\/]*)\/?$/i';
        $matches = Common::matcher($media_url, $regex);

        if (count($matches) !== 2) {
            return false;
        }

        $subject = $matches[1];
        $call_type = strtolower($matches[0]);

        switch ($call_type) {
            // playlist
            case 'playlist':
                $json = self::call_json("https://api.dailymotion.com/playlist/{$subject}?fields=owner,owner.screenname,owner.url");
                if (isset($json['owner'])) {
                    $channel_id = $json['owner'];
                    $channel_author = $json['owner.screenname'];
                    $channel_url = $json['owner.url'];
                }
                break;
            // video
            case 'video':
            case 'dai.ly':
                $json = self::call_json("https://api.dailymotion.com/video/{$subject}?fields=owner,owner.screenname,owner.url");
                if (isset($json['owner'])) {
                    $channel_id = $json['owner'];
                    $channel_author = $json['owner.screenname'];
                    $channel_url = $json['owner.url'];
                }
                break;
            // user
            case 'dailymotion.com':
                $json = self::call_json("https://api.dailymotion.com/user/{$subject}?fields=id,screenname,url");
                if (isset($json['id'])) {
                    $channel_id = $json['id'];
                    $channel_author = $json['screenname'];
                    $channel_url = $json['url'];
                }
                break;
            default:
                $json = null;
                break;
        }

        if (isset($channel_id) && isset($channel_author) && isset($channel_url)) {
            return [$channel_id, $channel_author, $channel_url, $json];
        }

        return false;
    }

    // INSTAGRAM #5
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_instagram($media_url) {
        // Not currently implemented by TSB
        // INSTAGRAM_KEY

        if (isset($channel_id) && isset($channel_author)) {
            return [$channel_id, $channel_author, $media_url, null];
        }

        return false;
    }

    // SOUNDCLOUD #6
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_soundcloud($media_url) {
        $parts = explode('/', trim(Common::matcher($media_url, '/soundcloud\.com\/(.*)/i')[0], '/'));

        if (empty($parts)) {
            return false;
        }

        $json = self::call_json(
            sprintf('https://api.soundcloud.com/resolve?url=%s&client_id=%s',
                'https://soundcloud.com/'.$parts[0], SOUNDCLOUD_KEY));

        if (isset($json['kind']) && $json['kind'] == 'user') {
            $channel_id = $json['id'];
            $channel_author = $json['username'];
            $channel_url = $json['permalink_url'];
        }

        if (isset($channel_id) && isset($channel_author) && isset($channel_url)) {
            return [$channel_id, $channel_author, $channel_url, $json];
        }

        return false;
    }

    // STREAMABLE #7
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_streamable($media_url) {
        // Not currently implemented by TSB

        if (isset($channel_id) && isset($channel_author)) {
            return [$channel_id, $channel_author, $media_url, null];
        }

        return false;
    }

    // TWITCH #8
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_twitch($media_url) {
        $parts = explode('/', trim(Common::matcher($media_url, '/twitch\.tv\/(.*)/i')[0], '/'));

        if (empty($parts)) {
            return false;
        }

        if ($parts[0] == 'videos' && count($parts) >= 2) {
            $call_type = 'video';
            $call_subject = $parts[1];
        } else {
            $call_type = 'user';
            $call_subject = $parts[0];
        }

        $json = self::call_json(sprintf([
            'user' => 'https://api.twitch.tv/kraken/users/?login=%s',
            'video' => 'https://api.twitch.tv/kraken/videos/%s',
        ][$call_type], $call_subject), [
            'Accept: application/vnd.twitchtv.v5+json',
            'Client-ID: '.TWITCH_KEY
        ]);

        $url_prefix = 'https://www.twitch.tv/';

        switch ($call_type) {
            case 'user':
                if (isset($json['users'][0]['_id'])) {
                    $channel_id = $json['users'][0]['_id'];
                    $channel_author = $json['users'][0]['display_name'];
                    $channel_url = $url_prefix . $json['users'][0]['name'];
                }
                break;
            case 'video':
                if (isset($json['channel']['_id'])) {
                    $channel_id = $json['channel']['_id'];
                    $channel_author = $json['channel']['display_name'];
                    $channel_url = $url_prefix . $json['channel']['name'];
                }
                break;
        }

        if (isset($channel_id) && isset($channel_author) && isset($channel_url)) {
            return [$channel_id, $channel_author, $channel_url, $json];
        }

        return false;
    }

    // IMGUR #9
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_imgur($media_url) {
        // Not currently implemented by TSB

        if (isset($channel_id) && isset($channel_author)) {
            return [$channel_id, $channel_author, $media_url, null];
        }

        return false;
    }

    // TWITTER #10
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_twitter($media_url) {
        $screen_name = strtok(Common::matcher($media_url, '/twitter\.com\/(.*)/i')[0], '/');

        $settings = array(
            'oauth_access_token'        => TWITTER_ACCESS_TOKEN,
            'oauth_access_token_secret' => TWITTER_ACCESS_TOKEN_SECRET,
            'consumer_key'              => TWITTER_CONSUMER_KEY,
            'consumer_secret'           => TWITTER_CONSUMER_SECRET
        );
        $url = 'https://api.twitter.com/1.1/users/lookup.json';
        $getfield = "?screen_name={$screen_name}";
        $requestMethod = 'GET';

        $twitter = new \TwitterAPIExchange($settings);
        $json = json_decode($twitter->setGetfield($getfield)
            ->buildOauth($url, $requestMethod)
            ->performRequest(true, [
                CURLOPT_SSL_VERIFYHOST => 2,
                CURLOPT_SSL_VERIFYPEER => 1,
                CURLOPT_CAINFO => CACERT_FILE,
            ]), true);

        if (empty($json) || empty($json[0])) {
            return false;
        }

        $channel_id = isset($json[0]['id']) ? $json[0]['id'] : null;
        $channel_author = isset($json[0]['screen_name']) ? $json[0]['screen_name'] : null;
        $channel_url = 'https://twitter.com/'.$json[0]['screen_name'];

        if (isset($channel_id) && isset($channel_author)) {
            return [$channel_id, $channel_author, $channel_url, $json];
        }

        return false;
    }

    // FACEBOOK #11
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_facebook($media_url) {
        static $fb = null;

        if (!isset($fb)) {
            $fb = new \Facebook\Facebook([
                'app_id'                    => FB_ID,
                'app_secret'                => FB_SECRET,
                'default_graph_version'     => 'v2.9',
                'default_access_token'      => FB_ACCESS_TOKEN,
            ]);
        }

        $parts = explode('/', trim(strtok(Common::matcher($media_url, '/facebook\.com\/(.*)/i')[0], '?'), '/'));

        if (empty($parts)) {
            return false;
        }

        if (strtolower($parts[0]) == 'pg') {
            array_shift($parts);
        }

        $call_type = 'user';
        $call_subject = $parts[0];

        $page_parts = explode('-', $call_subject);
        if (count($page_parts) > 1) {
            $last_page_part = array_pop($page_parts);
            if (is_numeric($last_page_part)) {
                $call_subject = $last_page_part;
                $call_type = 'page';
            }
        }

        /* if (count($parts) >= 3 && $parts[1] == 'videos') {
            $call_type = 'video';
            $call_subject = $parts[2];
        } else {
            $call_type = 'user';
            $call_subject = $parts[0];
        } */

        try {
            $json = $fb->get(sprintf([
                'page' => '/%s?fields=name,id,link',
                'user' => '/%s?fields=name,id,link',
                'video' => '/%s?fields=from{id,link,name}',
            ][$call_type], $call_subject))->getDecodedBody();

            $user = ($call_type == 'video') ? from($json, 'from') : $json;

            $channel_id     = from($user, 'id');
            $channel_author = from($user, 'name');
            $channel_url    = from($user, 'link');

            if (isset($channel_id) && isset($channel_author)) {
                return [$channel_id, $channel_author, $channel_url, $json];
            }
        } catch(\Exception $e) {
            return false;
        }
        return false;
    }

    // ETSY #12
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public static function info_etsy($media_url) {
        $matches = Common::matcher(strtok($media_url, '?'),
            '/etsy\.com\/(people|shop|listing)\/([^\/]*)|([a-zA-Z\-]{4,})\.etsy\.com/i');

        if (count($matches) == 1) {
            $call_type      = 'shop';
            $call_subject   = $matches[0];
        } else if (count($matches) == 2) {
            $call_type      = strtolower($matches[0]);
            $call_subject   = $matches[1];
        } else {
            return false;
        }

        $json = self::call_json(sprintf([
            'people'    => 'https://openapi.etsy.com/v2/users/%s?includes=User&api_key=%s',
            'listing'   => 'https://openapi.etsy.com/v2/listings/%s?includes=User&api_key=%s',
            'shop'      => 'https://openapi.etsy.com/v2/shops/%s?includes=User&api_key=%s',
        ][$call_type], $call_subject, ETSY_KEY));

        $user = from($json, 'results');
        if (empty($user)) return false;
        $user = $user[0];

        if ($call_type != 'people') {
            $user = from($user, 'User');
        }

        $channel_id     = from($user, 'user_id');
        $channel_author = from($user, 'login_name');
        $channel_url    = 'https://www.etsy.com/people/'.from($user, 'login_name');

        if (isset($channel_id) && isset($channel_author)) {
            return [$channel_id, $channel_author, $channel_url, $json];
        }

        return false;
    }

    // UTIL
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private static function call_json($url, $http_header = null, $http_method = 'GET') {
        return json_decode(self::call($url, $http_header, $http_method), true);
    }

    private static function call($url, $http_header = null, $http_method = 'GET') {
        try {
            return Common::call_url($url, $http_method, $http_header);
        } catch (\Exception $e) {
            return false;
        }
    }
}