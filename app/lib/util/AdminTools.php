<?php
namespace app\lib\util;

// administrative functions
// note that none of these automatically check if current user is an admin
class AdminTools {

    public static function get_users(int $limit = null) {
        $limit = isset($limit) ? 'LIMIT ' . $limit : '';
        return db('application')->query("SELECT * FROM user_data {$limit} ORDER BY username");
    }

    public static function oauthdata_getAll() {
        return db('TheTraveler')->query("SELECT username,app_id,agent_of FROM oauth_data");
    }

    public static function oauthdata_get($app_id) {
        return db('TheTraveler')->queryFirstField(
                "SELECT username,app_id,agent_of FROM oauth_data WHERE app_id=%s", $app_id);
    }

    public static function oauthdata_getPassword($app_id) {
        return db('TheTraveler')->queryFirstField(
                "SELECT password FROM oauth_data WHERE app_id=%s", $app_id);
    }

    public static function oauthdata_getSecret($app_id) {
        return db('TheTraveler')->queryFirstField(
                "SELECT app_secret FROM oauth_data WHERE app_id=%s", $app_id);
    }

    public static function oauthdata_create($username, $password, $app_id, $app_secret, $agent_of='tsb') {
        db('TheTraveler')->insert('oauth_data', array(
            'username'      => $username,
            'password'      => $password,
            'app_id'        => $app_id,
            'app_secret'    => $app_secret,
            'agent_of'      => $agent_of,
        ));
    }

}