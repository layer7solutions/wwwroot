<?php
namespace app\lib\util;

/**
 * CachedMap: easy utility for caching and pulling a bunch of stuff at once. Made easy through the
 * `CachedMap->cached($keys, $provider)` function. Any keys already cached will be pulled, the other
 * keys will be passed to the provider which will return the values for those keys and become
 * cached.
 * And the `cached` method will finally return KV-pairs as an associative array in the same order as
 * the $keys parameter passed in.
 */
class CachedMap {

    const DEFAULT_CACHE_TIME = 60*60*24; // 1 day

    /**
     * @var array
     */
    private $internal = [];

    /**
     * @var string
     */
    private $cache_prefix = null;

    /**
     * @var int
     */
    private $cache_time = self::DEFAULT_CACHE_TIME;

    /**
     * CachedMap.
     *
     * @param string    $cache_prefix (optional) the prefix to use for cache, if not specific then
     *                  no cache will be used, only the PHP runtime memory
     * @param int|null  $cache_time (optional) the time in seconds to cache for, if set to null
     *                  then there will be no cache expiry. If not specified then 1 day will be used
     */
    public function __construct(string $cache_prefix=null, ?int $cache_time = self::DEFAULT_CACHE_TIME) {
        $this->cache_prefix = $cache_prefix;
        $this->cache_time = $cache_time;
    }

    public function has($k, bool $mapping = false) {
        if (isset($this->cache_prefix)) {
            if (is_array($k)) {
                $cPrefix = $this->cache_prefix;

                $res = cached_exists(array_map(function($item) use ($cPrefix) {
                    return $cPrefix . $item;
                }, $k));

                $map = [];

                foreach ($res as $key => $state) {
                    if ($mapping) {
                        $map[remove_prefix($key, $this->cache_prefix)] = $state;
                    } else if (!$state) {
                        return false;
                    }
                }

                return $mapping ? $map : true;
            } else {
                return cached_exists($this->cache_prefix . $k);
            }
        } else {
            if (is_array($k)) {
                $map = [];

                foreach ($k as $key) {
                    if ($mapping) {
                        $map[$key] = array_key_exists($key, $this->internal);
                    } else if (!array_key_exists($key, $this->internal)) {
                        return false;
                    }
                }

                return $mapping ? $map : true;
            } else {
                return array_key_exists($k, $this->internal);
            }
        }
    }

    public function get($k) {
        if (isset($this->cache_prefix)) {
            if (is_array($k)) {
                $cPrefix = $this->cache_prefix;

                $res = cached(array_map(function($item) use ($cPrefix) {
                    return $cPrefix . $item;
                }, $k));

                $map = [];

                foreach ($res as $key => $value) {
                    $map[remove_prefix($key, $this->cache_prefix)] = $value;
                }

                return $map;
            } else {
                return cached($this->cache_prefix . $k);
            }
        } else {
            if (is_array($k)) {
                $ret = [];
                foreach ($k as $key) {
                    $ret[$key] = $this->internal[$key] ?? null;
                }
                return $ret;
            } else {
                return $this->internal[$k] ?? null;
            }
        }
    }

    public function clear($k) {
        return $this->put($k, null, true);
    }

    public function put($k, $v = null, $_delete = false) {
        if (func_num_args() === 1 && is_array($k) && array_keys($k) !== range(0, count($k) - 1)) {
            // One parameter, associative array
            if (isset($this->cache_prefix)) {
                $tmp = [];
                foreach ($k as $key => $value) {
                    $tmp[$this->cache_prefix . $key] = $value;
                }
                return cached($tmp);
            } else {
                foreach ($k as $key => $value) {
                    if (!$this->put($key, $value, $_delete)) {
                        return false;
                    }
                }
                return true;
            }
        }

        if (func_num_args() === 2 && is_string($k)) {
            if (isset($this->cache_prefix)) {
                if ($_delete) {
                    return cached_delete($this->cache_prefix . $k);
                } else {
                    return cached($this->cache_prefix . $k, $v, $this->cache_time);
                }
            } else {
                if ($_delete) {
                    unset($this->internal[$k]);
                } else {
                    $this->internal[$k] = $v;
                }
                return true;
            }
        }

        return false;
    }

    /**
     * Get cached values. Will call `$provider` for any keys that are not within the CachedMap
     * object.
     *
     * @param array     $keys array of keys
     * @param callable  $provider passed a list of keys, should return values - either as an
     *                  assocative array or sequential array in the same order and length as the
     *                  passed in keys
     * @return array    associative array of keys to value, will be in the same order as `$keys`
     */
    public function cached($keys, callable $provider = null): array {
        if (empty($keys)) {
            return [];
        }

        // ----- get existing stuff in cache, not guaranteed to have all the keys or be in any order
        $initial = $this->get((array) $keys);

        // -----  check if keys exist, key => true/false pair, will be same order as $keys parameter
        $has_map = $this->has((array) $keys, true);

        // ----- stuff we need to get from the provider
        $uncached = [];

        // ----- results from the provider
        $put_map = [];

        // ----- populate $uncached
        foreach ($has_map as $key => $state) {
            if (!$state) {
                $uncached[] = $key;
            }
        }

        // ----- process $uncached
        if (!empty($uncached) && isset($provider)) {
            $res = [];

            if (!empty($tmp = $provider($uncached, $res))) {
                $res = $tmp;
            }

            if (array_keys($res) !== range(0, count($res) - 1)) {
                // associative
                foreach ($uncached as $key) {
                    if (!array_key_exists($key, $res)) {
                        continue;
                    }
                    $put_map[$key] = $res[$key];
                    $has_map[$key] = true;
                }
            } else {
                // sequential
                for ($i = 0, $size = count($res); $i < $size; $i++) {
                    $key = $uncached[$i];

                    $put_map[$key] = $res[$i];
                    $has_map[$key] = true;
                }
            }
        }

        // ----- store $put_map
        if (!empty($put_map)) {
            $this->put($put_map);
        }

        // ----- compile results
        $result = [];

        // use $has_map for ordering and don't include keys that we could not obtain
        foreach ($has_map as $key => $state) {
            if ($state) {
                $result[$key] = $put_map[$key] ?? $initial[$key];
            }
        }

        return $result;
    }

    public function cachedValues($keys, callable $provider = null) {
        return array_values($this->cached($keys, $provider));
    }

}