<?php
namespace app\lib\services;

class WebsyncProcess {

    private static function check_about_moderators($subreddit, $moderator) {
        $query = "SELECT count(1) FROM about_moderators WHERE subreddit=%s AND moderator=%s";
        $result = db('application')->queryFirstField($query, trim(strtolower($subreddit)), trim($moderator));

        return $result === 1;
    }

    public static function clear_flags($moderator) {
        db('application')->update('user_data', array(
            'websync_flags' => null,
        ), 'username=%s', $moderator);
    }

    public static function process_flags($moderator) {
        $flags = db('application')->queryFirstField(
            'SELECT websync_flags FROM user_data WHERE username=%s', $moderator);

        // if empty, nothing we need to sync
        if (empty($flags)) {
            return false;
        }

        // prevent duplicate processing in case of duplicate flags
        $seen = [];

        // update modded sub record for each subreddit
        foreach (explode(',', $flags) as $subreddit) {
            $subreddit = trim($subreddit);

            if (empty($subreddit) || isset($seen[$subreddit])) {
                continue;
            }

            user($moderator)->modded->update_specific($subreddit);
            $seen[$subreddit] = true;
        }
        self::clear_flags($moderator);
        return true;
    }

    public static function add_flag($moderator, $flag) {
        return self::set_flag($moderator, $flag, true);
    }

    public static function remove_flag($moderator, $flag) {
        return self::set_flag($moderator, $flag, false);
    }

    /**
     * Set a websync flag on the given moderator user.
     *
     * @param string    $moderator
     * @param string    $flag
     * @param string    $addRemoveN true to add, false to remove
     * @return bool     true if the operation succeeded, false if a user the username $moderator
     *                  does not exist, or if the flag already exists and $addRemoveN=1, or if the
     *                  flag does not exist and $addRemoveN=0
     */
    public static function set_flag(string $moderator, string $flag, bool $addRemoveN) {
        $moderator = trim($moderator);
        $flag      = trim($flag);

        $dataQuery = "SELECT username, websync_flags FROM user_data WHERE username=%s";
        $data = db('application')->queryFirstRow($dataQuery, $moderator);

        if (empty($data['username'])) {
            return false;
        }

        $flags = empty($data['websync_flags']) ? [] : explode(',', $data['websync_flags']);

        if (in_array($flag, $flags)) {
            if ($addRemoveN)
                return false;
            array_remove_value($flags, $flag);
        } else {
            if (!$addRemoveN)
                return false;
            array_push($flags, $flag);
        }

        db('application')->update('user_data', array(
            'websync_flags' => implode(',', $flags),
        ), 'username=%s', $moderator);

        return true;
    }

    // TSBACCEPT - Action Required
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //  - Create user_modded rows for all moderators of the given subreddit
    //    who have also logged into this site
    public static function websync_tsbaccept($subreddit, $moderators, $tmp_flag=false) {
        if ($tmp_flag == false) {
            $mod = null;
            foreach ($moderators as $mod) {
                self::add_flag($mod, $subreddit);
                $mod = null;
            }
        }

        self::websync_modupdate($subreddit, $moderators);
    }

    // MODUPDATE
    public static function websync_modupdate($subreddit, $moderators) {
        $insert_data = [];

        $mod = null;
        foreach ($moderators as $mod) {
            $insert_data[] = [
                'subreddit' => strtolower($subreddit),
                'moderator' => $mod,
            ];
            $mod = null;
        }

        db('application')->delete('about_moderators',
            'subreddit=%s', strtolower($subreddit));

        db('application')->insert('about_moderators', $insert_data);
    }

    // MODADD - Action Required
    // ~~~~~~~~~~~~~~~~~~~~~~~~
    //  - Create user_modded rows for the given (subreddit,moderator) pair
    //     - We can only do this through the moderator's account b/c oauth
    //       so we have to enqueue and perform this action on the moderator's
    //       next session on our site
    //     - if the moderator does not have an account here, then there's
    //       no need to do anything
    public static function websync_modadd($subreddit, $moderator) {
        self::add_flag($moderator, $subreddit);

        if (!self::check_about_moderators($subreddit, $moderator)) {
            db('application')->delete('about_moderators',
                'moderator=%s AND subreddit=%s',
                $moderator, strtolower($subreddit));

            db('application')->insert('about_moderators', [
                'subreddit' => strtolower($subreddit),
                'moderator' => $moderator,
            ]);
        }
    }

    // MODREMOVE - Action Required
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //  - Remove user_modded rows for the given (subreddit,moderator) pair
    public static function websync_modremove($subreddit, $moderator) {
        self::remove_flag($moderator, $subreddit);

        db('application')->delete('user_modded',
            'username=%s AND subreddit=%s',
            $moderator, strtolower($subreddit));

        if (self::check_about_moderators($subreddit, $moderator)) {
            db('application')->delete('about_moderators',
                'moderator=%s AND subreddit=%s',
                $moderator, strtolower($subreddit));
        }
    }

    // MODPERMS - Action Required
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~
    //  - Update 'mod_permissions' on user_modded rows for the given (subreddit,moderator) pair
    public static function websync_modperms($subreddit, $moderator, $new_state) {
        $perms = is_string($new_state) ? strtolower($new_state) : $new_state;

        if (is_array($perms)) {
            $perms = strtolower(implode(',', $perms));
        }

        if (empty($perms) || $perms == 'none' || $perms == 'no permissions') {
            $perms = '';
        } else if ($perms == 'full' || $perms == 'full permissions') {
            $perms = 'all';
        }

        db('application')->update('user_modded', [
            'mod_permissions' => $perms,
        ], 'username=%s AND subreddit=%s', $moderator, strtolower($subreddit));
    }

}