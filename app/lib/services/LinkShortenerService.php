<?php
namespace app\lib\services;

class LinkShortenerService {
    const CHARSET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const SHORT_ID_LENGTH = 6;
    const PAGE_SIZE = 50;
    const MAX_PAGE_SIZE = 100;
    const MIN_PAGE_SIZE = 1;

    public static function get_creator(string $short_id) {
        return db('application')->queryFirstField(
            "SELECT created_by FROM link_shortener WHERE short_id=%s", $short_id);
    }

    public static function get_hits(string $short_id) {
        return db('application')->queryFirstField(
            "SELECT hits FROM link_shortener WHERE short_id=%s", $short_id);
    }

    /**
     * Get info about shortened links by short link creator.
     *
     * @param string $username the creator of the links to retrieve
     * @return array link info
     */
    public static function get_links(string $username, int $page=1, int $limit=null) {
        if ($limit === null || $limit <= 0) $limit = self::PAGE_SIZE;
        if ($limit < self::MIN_PAGE_SIZE) $limit = self::MIN_PAGE_SIZE;
        if ($limit > self::MAX_PAGE_SIZE) $limit = self::MAX_PAGE_SIZE;

        return db('application')->query(
            "SELECT * FROM link_shortener WHERE created_by=%s AND is_deleted=false
            ORDER BY created_utc DESC
            LIMIT %i OFFSET %i", $username, $limit, $limit*($page-1));
    }

    public static function get_total(string $username, int $limit=null) {
        if ($limit === null || $limit <= 0) $limit = self::PAGE_SIZE;
        if ($limit < self::MIN_PAGE_SIZE) $limit = self::MIN_PAGE_SIZE;
        if ($limit > self::MAX_PAGE_SIZE) $limit = self::MAX_PAGE_SIZE;

        $amount = db('application')->queryFirstField(
            "SELECT COUNT(*) FROM link_shortener WHERE created_by=%s AND is_deleted=false",
            $username);

        return [
            'total'         => $amount,
            'page_size'     => $limit,
            'page_count'    => ceil($amount / $limit),
        ];
    }

    /**
     * Delete link(s) by id.
     *
     * @param string[]|string $short_id
     * @return bool true if at least one link was deleted, false otherwise
     */
    public static function delete_link($short_id) {
        $where_clause = is_array($short_id) ? 'short_id IN %ls' : 'short_id=%s';

        db('application')->update('link_shortener', [
            'is_deleted' => true,
        ], $where_clause, $short_id);

        return db('application')->affectedRows() >= 1;
    }

    public static function is_deleted(string $short_id) {
        $res =  db('application')->queryFirstField(
            "SELECT is_deleted FROM link_shortener WHERE short_id=%s", $short_id);

        return $res === null || $res == true;
    }

    public static function increment_hits(string $short_id, int $amount=1) {
        $hits = self::get_hits($short_id);

        db('application')->update('link_shortener', [
            'hits' => $hits + $amount,
        ], 'short_id=%s', $short_id);
    }

    /**
     * Update the original url of a short link by its short id.
     *
     * @param string $short_id
     * @param string $original_url
     * @return bool true if a link was updated, false otherwise
     */
    public static function update_link(string $short_id, string $original_url) {
        db('application')->update('link_shortener', [
            'original_url' => $original_url,
        ], 'short_id=%s', $short_id);
        return db('application')->affectedRows() >= 1;
    }

    /**
     * Get link or link data.
     *
     * @param string $short_id the short id of the short link
     * @param bool $get_data if false, returns the original URL as a string, if true returns meta
     * data and info about the short link
     * @return string|array original URL or short link data
     */
    public static function get_link(string $short_id, bool $get_data=false) {
        $select_clause = $get_data ? '*' : 'original_url';
        $query_func = $get_data ? 'queryFirstRow' : 'queryFirstField';

        return db('application')->{$query_func}(
            "SELECT $select_clause FROM link_shortener WHERE short_id=%s",
            $short_id);
    }

    /**
     * Create a new short link.
     *
     * @param string $username creator of the short link
     * @param string $original_url the URL the short link should point to
     * @return array the data of the inserted row
     */
    public static function create_link(string $username, string $original_url) {
        $short_id = null;

        do {
            $short_id = self::create_short_id(self::SHORT_ID_LENGTH);
        } while (db('application')->queryFirstField('SELECT COUNT(1) FROM link_shortener WHERE short_id=%s', $short_id));

        db('application')->insert('link_shortener', $ret = [
            'short_id'      => $short_id,
            'original_url'  => $original_url,
            'created_utc'   => time(),
            'created_by'    => $username,
            'is_deleted'    => false,
            'hits'          => 0,
        ]);

        return $ret;
    }

    private static function create_short_id($length) {
        $max_index = strlen(self::CHARSET) - 1;
        $str = '';

        for ($i = 0; $i < $length; $i++) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $str .= self::CHARSET[random_int(0, $max_index)];
        }

        return $str;
    }
}