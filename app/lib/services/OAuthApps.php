<?php
namespace app\lib\services;

use app\lib\User;

class OAuthApps {

    // DEVELOPED APPS

    public static function create_new_app($name, $desc, $about_url, $redirect_uri) {
        $client_id = safe_token(8);
        $client_secret = safe_token(16);
        $username = user()->get_username();

        if (!isset($username)) {
            return false;
        }

        db('oauth2')->insert('oauth_clients', array(
            'client_id'     => $client_id,
            'client_secret' => $client_secret,
            'redirect_uri'  => $redirect_uri,
            'user_id'       => $username,
            'app_name'      => $name,
            'app_about'     => $desc,
            'app_url'       => $about_url,
        ));
        return $client_id;
    }

    public static function delete_app($client_id, $developer = null) {
        if (isset($developer) && self::get_developer($client_id) != $developer) {
            return false;
        }

        db('oauth2')->delete('oauth_authorization_codes', 'client_id=%s', $client_id);
        db('oauth2')->delete('oauth_refresh_tokens', 'client_id=%s', $client_id);
        db('oauth2')->delete('oauth_access_tokens', 'client_id=%s', $client_id);
        db('oauth2')->delete('oauth_jwt', 'client_id=%s', $client_id);
        db('oauth2')->delete('oauth_clients', 'client_id=%s', $client_id);

        return true;
    }

    public static function modify_app($client_id, $name = null, $desc = null,
            $about_url = null, $redirect_uri = null, $developer = null) {
        if (isset($developer) && self::get_developer($client_id) != $developer) {
            return false;
        }

        $update = array();

        if (isset($name)) {
            $update['app_name'] = $name;
        }
        if (isset($desc)) {
            $update['app_about'] = $desc;
        }
        if (isset($about_url)) {
            $update['app_url'] = $about_url;
        }
        if (isset($redirect_uri)) {
            $update['redirect_uri'] = $redirect_uri;
        }

        if (empty($update)) {
            return true;
        }

        db('oauth2')->update('oauth_clients', $update, "client_id=%s", $client_id);
        return true;
    }

    public static function app_exists($client_id) {
        return 1 == db('oauth2')->queryFirstField('SELECT COUNT(*) FROM oauth_clients WHERE client_id=%s', $client_id);
    }

    public static function get_developer($client_id) {
        return db('oauth2')->queryFirstField('SELECT user_id FROM oauth_clients WHERE client_id=%s', $client_id);
    }

    public static function get_app($client_id) {
        return db('oauth2')->queryFirstRow('SELECT * FROM oauth_clients WHERE client_id=%s', $client_id);
    }

    // AUTHORIZED APPS

    public static function revoke_access($client_id, $username) {
        db('oauth2')->delete('oauth_authorization_codes', 'client_id=%s AND user_id=%s', $client_id, $username);
        db('oauth2')->delete('oauth_refresh_tokens', 'client_id=%s AND user_id=%s', $client_id, $username);
        db('oauth2')->delete('oauth_access_tokens', 'client_id=%s AND user_id=%s', $client_id, $username);
        return true;
    }

    // GET

    public static function get_authorized_apps($username) {
        return db('oauth2')->query('SELECT'.
                ' DISTINCT ON (c.client_id) c.client_id, c.app_name, c.app_about, c.app_url, c.redirect_uri,'.
                ' c.app_is_official, c.grant_types, a.scope, a.user_id, c.user_id as developer'.
            ' FROM oauth_clients c INNER JOIN oauth_authorization_codes a'.
            ' ON (c.client_id = a.client_id AND a.user_id=%s)', $username);
    }

    public static function get_developed_apps($username) {
        return db('oauth2')->query('SELECT * FROM oauth_clients WHERE user_id=%s', $username);
    }


}