<?php
namespace app\lib\services;

use app\lib\Common;
use app\lib\User;

class GlobalQueue {
    const STATUS_PENDING    = 0;
    const STATUS_APPROVED   = 1;
    const STATUS_DENIED     = 2;

    public static $send_enqueue_notification_enabled = true;

    public static function get(User $user_obj) {
        // if admin mode or feature=admin and user is admin
        if (api_prefer('admin') && $user_obj->is_globalist()) {
            $pending    = db('application')->query('SELECT * FROM webqueue_globalblacklist WHERE approval_status=0');
            $approved   = db('application')->query('SELECT * FROM webqueue_globalblacklist WHERE approval_status=1 ORDER BY enqueued_time DESC LIMIT 50');
            $denied     = db('application')->query('SELECT * FROM webqueue_globalblacklist WHERE approval_status=2 ORDER BY enqueued_time DESC LIMIT 50');
        } else {
            $username   = $user_obj->get_username();
            $pending    = db('application')->query('SELECT * FROM webqueue_globalblacklist WHERE approval_status=0 AND enqueued_by=%s', $username);
            $approved   = db('application')->query('SELECT * FROM webqueue_globalblacklist WHERE approval_status=1 AND enqueued_by=%s ORDER BY enqueued_time DESC LIMIT 25', $username);
            $denied     = db('application')->query('SELECT * FROM webqueue_globalblacklist WHERE approval_status=2 AND enqueued_by=%s ORDER BY enqueued_time DESC LIMIT 50', $username);
        }

        return [
            'pending'   => $pending,
            'approved'  => $approved,
            'denied'    => $denied,
        ];
    }

    private static function send_enqueue_notification($enqueued) {
        if (!self::$send_enqueue_notification_enabled) {
            return;
        }

        $webhook_url = LAYER7HOOK_GLOBALQUEUE;
        $is_discord  = false;
        $ch_bold     = '*';
        $ch_italic   = '_';

        if (full_domain($webhook_url) == 'discordapp.com') {
            $is_discord   = true;
            $webhook_url .= '/slack'; // set to slack-compatible discord hook
            $ch_bold      = '**';
        }

        $add_line = function(&$message, $label, $data) use ($ch_bold) {
            $message .= $ch_bold . $label . ':' . $ch_bold . ' ' . $data . "\n";
        };

        foreach ($enqueued as $item) {
            $message = '';
            $add_line($message, 'Channel Name',     $item['media_channel_author']);
            $add_line($message, 'Channel ID',       $item['media_channel_id']);
            $add_line($message, 'Media Platform',   $item['media_platform']);
            $add_line($message, 'Submitted by',     $item['enqueued_by']);
            $add_line($message, 'Media URL',        $item['media_url']);
            $add_line($message, 'Reason',
                isset($item['reason']) ?
                    ("\n" . $item['reason']) :
                    ($ch_italic . 'no reason' . $ch_italic)
            );

            $payload = array(
                "channel"   => "#globalqueue",
                "text"      => $message,
                "username"  => "Site-Notifications",
            );
            if ($is_discord) unset($payload['channel']);

            Common::call_url($webhook_url, 'POST', null, "payload=" . json_encode($payload));
        }
    }

    public static function enqueue($media_url, $reason = null) {
        $data = subreddit('TheSentinelBot')->mediaban->check($media_url);

        $results = [
            'in_queue'      => [],
            'in_blacklist'  => [],
            'enqueued'      => [],
        ];

        $tmp = [];

        // already in blacklist
        foreach ($data['present'] as $item) {
            $itemdata = $tmp[$item['channel_id']] = $data['data'][$item['channel_id']];

            $results['in_blacklist'][] = [
                'timestamp'             => $item['timestamp'],
                'moderator'             => $item['moderator'],
                'media_url'             => add_protocol($itemdata['media_url']),
                'media_platform'        => $itemdata['media_platform'],
                'media_channel_id'      => $itemdata['channel_id'],
                'media_channel_author'  => $itemdata['channel_author'],
            ];
        }

        // not in blacklist
        $leftovers = [];
        foreach ($data['data'] as $channel_id => $itemdata) {
            if (!isset($tmp[$channel_id])) {
                $leftovers[] = $itemdata;
            }
        }

        foreach ($leftovers as $itemdata) {
            $media_url      = add_protocol($itemdata['media_url']);
            $platform       = $itemdata['media_platform'];
            $channel_id     = $itemdata['channel_id'];
            $channel_author = $itemdata['channel_author'];
            $enqueued_by    = user()->get_username();
            $enqueued_time  = time();

            $in_queue = db('application')->queryFirstRow(
                'SELECT id, approval_status, enqueued_time, enqueued_by, reason, reviewer'.
                ' FROM webqueue_globalblacklist'.
                ' WHERE media_channel_id=%s AND media_platform=%s AND enqueued_by=%s AND approval_status=0',
                $channel_id, $platform, $enqueued_by);

            if (isset($in_queue)) {
                // currently in queue

                $results['in_queue'][] = [
                    'id'                    => $in_queue['id'],
                    'approval_status'       => $in_queue['approval_status'],
                    'media_url'             => $media_url,
                    'media_platform'        => $platform,
                    'media_channel_id'      => $channel_id,
                    'media_channel_author'  => $channel_author,
                    'enqueued_by'           => $in_queue['enqueued_by'],
                    'enqueued_time'         => $in_queue['enqueued_time'],
                    'reason'                => $in_queue['reason'],
                    'reviewer'              => $in_queue['reviewer'],
                ];
            } else {
                // not in queue, add to queue
                db('application')->insert('webqueue_globalblacklist', array(
                    'media_url'             => $media_url,
                    'media_platform'        => $platform,
                    'media_channel_id'      => $channel_id,
                    'media_channel_author'  => $channel_author,
                    'enqueued_by'           => $enqueued_by,
                    'enqueued_time'         => $enqueued_time,
                    'reason'                => $reason,
                ));

                $results['enqueued'][] = [
                    'id'                    => db('application')->insertId(),
                    'approval_status'       => 0,
                    'media_url'             => $media_url,
                    'media_platform'        => $platform,
                    'media_channel_id'      => $channel_id,
                    'media_channel_author'  => $channel_author,
                    'enqueued_by'           => $enqueued_by,
                    'enqueued_time'         => $enqueued_time,
                    'reason'                => $reason,
                ];
            }
        }

        self::send_enqueue_notification($results['enqueued']);

        return $results;
    }


    public static function approve($id) {
        // update approval status to 1 (approved)
        db('application')->update('webqueue_globalblacklist', array(
            'approval_status' => 1,
            'reviewer' => user()->get_username(),
        ), "id=%i", $id);

        // add to global blacklist
        $url  = db('application')->queryFirstField("SELECT media_url FROM webqueue_globalblacklist WHERE id=%i", $id);
        $data = subreddit('TheSentinelBot')->mediaban->add($url);

        return (isset($data['processed']) && count($data['processed']) === 1);
    }

    public static function deny($id) {
        // update approval status to 2 (denied)
        db('application')->update('webqueue_globalblacklist', array(
            'approval_status' => 2,
            'reviewer' => user()->get_username(),
        ), "id=%i", $id);

        // affectedRows should always be either 1 or 0.
        // Anything else means we got bigger problems since 'id' is the primary key...
        return db('application')->affectedRows() === 1;
    }
}