<?php
namespace app\lib;

/**
 * This file is for miscellaneous utilities and helper functions.
 */
class Common {

    public static function matcher($data, $regex) {
        $matches = [];

        if (preg_match_all($regex, $data, $prematches)) {
            array_shift($prematches); // remove full match

            // remove empty matches
            foreach ($prematches as $prematch) {
                if (is_array($prematch)) $prematch = $prematch[0];
                if (!empty($prematch)) $matches[] = $prematch;
            }
        }

        return $matches;
    }

    public static function parse_multi_str(string $arg0, callable $sourceFn, ?string $sourceName='all', callable $sanitizeFn=null, string $itemMask='a-zA-Z0-9_', string $itemDelRegex='\\+,\s') {
        if (empty($arg0)) {
            return $sourceName ? [[], 'None'] : [];
        }

        $multi_validate_regex =
            "/^([aA][lL]{2}(:[a-zA-Z]+)?(\-[$itemMask\-,]+)?|(?![aA][lL]{2}[^$itemMask])[$itemMask$itemDelRegex]+)$/S";

        if (!preg_match($multi_validate_regex, $arg0)) {
            return $sourceName ? [false, 'failed validation regex'] : false;
        }

        $arg0_lower = strtolower($arg0);
        $arg0_tmp = strtolower(substr($arg0, 0, 4));

        if ($arg0_lower === 'all' || $arg0_tmp === 'all:' || $arg0_tmp === 'all-') {
            if ($arg0_lower === 'all') {
                // 'all' without category, without exclude
                $include = call_user_func_array($sourceFn, ['all']);
                $exclude = null;
            } else if ($arg0_lower === 'all-') {
                // 'all' without category, with exclude
                $exclude = array_filter(preg_split("/[\-,]/", $arg0));
                array_shift($exclude);

                $include = call_user_func_array($sourceFn, ['all']);
                $exclude = array_values(array_iunique($exclude));
            } else {
                // all with category, maybe exclude
                $exclude = array_filter(preg_split("/[\-,]/", $arg0));
                $allStmt = array_shift($exclude);

                $include = call_user_func_array($sourceFn, [substr($allStmt, 4) ?: 'all']);
                $exclude = array_values(array_iunique($exclude));
            }

            $title = $sourceName;

            if (!is_array($include) && is_object($include) && method_exists($include, 'to_array')) {
                $include = $include->to_array();
            }

            // no need to call `sanitizeFn` the include is from the sourceFn, so we should assume
            // it's already good
            $result = $include;

            if (!empty($exclude)) {
                $excludeRes = !empty($sanitizeFn) ?
                    call_user_func_array($sanitizeFn, [$exclude, 'exclude']) : $exclude;

                if (count($excludeRes) == 2 && $excludeRes[0] === false) {
                    return $sourceName ? [false, $excludeRes[1]] : false;
                } else if (!empty($excludeRes) && $excludeRes[0] === false) {
                    return $sourceName ? [false, 'failed `exclude` sanitization'] : false;
                } else if (count($exclude) !== count($excludeRes)) {
                    return $sourceName ? [false, 'sanitization refused input(s)'] : false;
                }

                if (!empty($sourceName))
                    $title .= ' except ' . implode(', ', $exclude);
                $result = array_values(array_diff($result, $exclude));
            }
        } else {
            $include = array_values(array_iunique(array_filter(preg_split("/[$itemDelRegex]/", $arg0))));

            $result = !empty($sanitizeFn) ?
                call_user_func_array($sanitizeFn, [$include, 'include']) : $include;

            if (count($result) === 2 && $result[0] === false) {
                return $sourceName ? [false, $result[1]] : false;
            } else if (!empty($result) && $result[0] === false) {
                return $sourceName ? [false, 'failed `include` sanitization'] : false;
            } else if (count($include) !== count($result)) {
                return $sourceName ? [false, 'sanitization refused input(s)'] : false;
            }
        }

        if (!empty($sourceName) && !isset($title)) {
            $title = implode(', ', $result);
        }

        return $sourceName ? [$result, $title] : $result;
    }

    public static function call_json($url, $http_method = 'GET', $http_header = null, $http_contentbody = null) {
        return json_decode(self::call_url($url, $http_method, $http_header, $http_contentbody), true);
    }

    /**
     * Make a request to a URL with proper SSL configurations.
     */
    public static function call_url($url, $http_method = 'GET', $http_header = null, $http_contentbody = null) {
        $context = [
            'http' => [
                'method' => strtoupper($http_method),
            ],
            'ssl' => [
                'peer_name'             => parse_url($url, PHP_URL_HOST),
                'verify_peer'           => true,
                'verify_peer_name'      => true,
                'verify_depth'          => 5,
                'cafile'                => CACERT_FILE,
                'ciphers'               => 'HIGH:!SSLv2:!SSLv3',
                'disable_compression'   => true,
            ],
        ];

        if (isset($http_header)) {
            if (is_string($http_header)) {
                $context['http']['header'] = $http_header;
            } else if (is_array($http_header)) {
                $context['http']['header'] = implode("\r\n", $http_header);
            }
        }

        // TODO: make this better
        if (isset($http_contentbody)) {
            if (!empty($context['http']['header'])) {
                $context['http']['header'] = trim($context['http']['header']) . "\r\n";
            } else {
                $context['http']['header'] = '';
            }

            if (!str_contains($context['http']['header'], 'Content-Type:')) {
                $context['http']['header'] .= 'Content-Type: application/x-www-form-urlencoded'."\r\n";
            }
            if (!str_contains($context['http']['header'], 'Content-Length:')) {
                $context['http']['header'] .= 'Content-Length: '.strlen($http_contentbody)."\r\n";
            }
            $context['http']['content'] = $http_contentbody;
        }

        return file_get_contents($url, false, stream_context_create($context));
    }

    public static function categories(bool $as_enabled_map = false) {
        static $_categories = [
            'sentinel_enabled'          => 'sentinel',
            'dirtbag_enabled'           => 'dirtbag',
            'modlog_enabled'            => 'modlog',
            'flairbot_enabled'          => 'flairbot',
            'modmail_enabled'           => 'modmail',
            'botban_enabled'            => 'botban',
            'domainblacklist_enabled'   => 'domainblacklist',
            'automute_enabled'          => 'automute',
            'replymute_enabled'         => 'replymute',
            'userlog_enabled'           => 'userlog'
        ];

        if ($as_enabled_map) {
            return $_categories;
        }

        return array_values($_categories);
    }

    public static function category($cat) {
        switch (strtolower($cat)) {
            case null:
            case '':
            case 'any':
            case 'all':
                return 'all';
            case 'modlogs':
            case 'modlog':
            case 'logs':
            case 'log':
                return 'modlog';
            case 'dirtbag':
                return 'dirtbag';
            case 'thesentinelbot':
            case 'thesentinel':
            case 'sentinel':
            case 'tsb':
            case 'media':
            case 'mediaban':
            case 'mediablacklist':
                return 'sentinel';
            case 'flairbot':
                return 'flairbot';
            case 'botban':
            case 'shadowban':
            case 'userban':
                return 'botban';
            case 'domain':
            case 'domains':
            case 'domainban':
            case 'domainblock':
            case 'domainblacklist':
                return 'domainblacklist';
            case 'modmail':
            case 'modmaillog':
            case 'modmaillogs':
            case 'modmaillogging':
                return 'modmail';
            case 'automute':
                return 'automute';
            case 'replymute':
                return 'replymute';
            case 'userlog':
            case 'userlogs':
            case 'userlogging':
            case 'newusers':
            case 'newuserlog':
            case 'newuserlogging':
                return 'userlog';
            default:
                return null;
        }
    }

    public static function get_tsb_subreddits() {
        return subreddit_provider_global()->get();
    }

    public static function is_tsb_subreddit($subreddit) {
        return subreddit_provider_global()->has($subreddit);
    }

    public static function tsb_agents($map_to_subscribers = false) {
        static $_map = null;

        if (isset($_map)) {
            if ($map_to_subscribers) {
                return $_map;
            } else {
                return array_keys($_map);
            }
        }

        $res = db('Zion')->query("
            SELECT
              name,
              subscribers as sum
            FROM
              sentinel_agents
            ORDER BY
              cast(NULLIF(regexp_replace(split_part(name, '_', 2), '\D', '', 'g'), '') as integer)
        ");

        $_map = [];
        foreach ($res as $row) {
            if (strtolower($row['name']) == 'thesentinel_3') {
                $row['sum'] = false;
            }
            $_map[$row['name']] = $row['sum'];
        }

        return self::tsb_agents($map_to_subscribers);
    }

    /**
     * Returns the reddit username from a username or user link.
     *
     * @param string $username_or_userlink
     * @return string
     */
    public static function reddit_username($username_or_userlink) {
        foreach (['user', 'u'] as $upart) {
            if (str_contains($username_or_userlink, $upart.'/')) {
                $username_or_userlink = explode($upart.'/', $username_or_userlink, 2)[1];
                return explode('/', $username_or_userlink)[0];
            }
        }

        $parts = explode('/', $username_or_userlink);
        $last_part = $parts[count($parts) - 1];
        if (strlen($last_part) == 0) { // if there's a trailing slash
            $last_part = $parts[count($parts) - 2];
        }
        return $last_part;
    }

    /**
     * Returns the subreddit name from a subreddit or subreddit link.
     *
     * @param string $subreddit_or_sublink
     * @return string
     */
    public static function reddit_subreddit($subreddit_or_sublink) {
        if (str_contains($subreddit_or_sublink, 'r/')) {
            $subreddit_or_sublink = explode('r/', $subreddit_or_sublink)[1];
        } else if (str_contains($subreddit_or_sublink, 'R/')) {
            $subreddit_or_sublink = explode('R/', $subreddit_or_sublink)[1];
        }
        return explode('/', $subreddit_or_sublink)[0];
    }

    /**
     * Format moderator permissions for display.
     *
     * @param string|string[] $perms
     * @param string $sep=','
     * @return string
     */
    public static function format_reddit_perms($perms, string $sep = ','): string {
        if (empty($perms))
            $perms = 'no permissions';

        if (is_array($perms)) {
            $perms = implode($sep, $perms);
        } else if ($sep != ',') {
            $perms = implode($sep, explode(',', $perms));
        }

        if ($perms === 'all') {
            return 'full permissions';
        }

        return $perms;
    }

}