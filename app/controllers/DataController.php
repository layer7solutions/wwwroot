<?php
namespace app\controllers;

use app\lib\SubredditMethods;
use app\lib\mixins\UsersModule;
use app\lib\logs\Logs;
use app\lib\SubredditException;

class DataController extends AuthController {

    protected function default_action(array $params) {
        redirect('/data/explore');
    }

    protected function users_POST(array $params) {
        if (!session_csrf_token_validity())
            redirect("/data/users?error=Bad request");
        $a = intval($_REQUEST['a'] ?: UsersModule::DEFAULT_SEARCH_RESULTS);
        $p = intval($_REQUEST['p'] ?? 0);
        $s = intval($_REQUEST['s'] ?? 0);
        $sr = from($params, 'subreddit');
        redirect("/data/users/{$sr}?s={$s}&p={$p}&a={$a}");
    }

    protected function users(array $params) {
        if (!isset($params['subreddit'])) {
            return view('net.applayout', 'data.users-srselect')
                ->meta('Users', 'page--data page--data_users')
                ->use('srlist', alphasort(user()->modded('all:userlog')->to_array()))
                ->stylesheets('ext/data/users');
        }

        try {
            $subreddit = user()->modded(from($params, 'subreddit'));
        } catch (SubredditException $e) {
            return $this->error(403);
        }

        $offset = from($_REQUEST, 'p', 'intval') ?: 0;
        $limit  = from($_REQUEST, 'a', 'intval') ?: UsersModule::DEFAULT_SEARCH_RESULTS;
        $order  = from($_REQUEST, 's', 'intval') ?? UsersModule::ORDER_THING_CREATED_UTC;

        return view('net.applayout', 'data.users')
            ->meta('Users', 'page--data page--data_users')
            ->use('new_users_table', $subreddit->users->get_new_users($order, $offset, $limit))
            ->use('sr_title', (string) $subreddit)
            ->using([
                'offset' => $offset,
                'limit'  => $limit,
                'order'  => $order,
                'error'  => from($_REQUEST, 'error'),
                'srIn'   => from($params, 'subreddit'),
            ])
            ->stylesheets('ext/data/users');
    }

    protected function explore(array $params) {
        $srlist = user()->modded->to_array();

        return view('net.applayout', 'data.explore')
            ->meta('Posts & Comments', 'page--data page--data_explore')
            ->stylesheets('ext/data/explore')
            ->usinglib('snudown')
            ->scripts([
                'ext/data/explore',
            ])
            ->using([
                'subreddits' => $srlist,
            ])
            ->addInitOpt('DataExplore', 'mod_list', $srlist);
    }
}