<?php
namespace app\controllers;

class OverviewController extends AuthController {

    protected function default_action(array $params) {
        $has_tsb = user()->modded->has_category('tsb');
        $has_logs = user()->modded->has_category('logs');

        $tsb_recent = $has_tsb ? from(user()->modded('all:tsb')->mediaban->list(), 'listing') : null;

        return view('net.applayout', 'overview.index')
            ->meta('Overview', 'page--overview')
            ->stylesheets('ext/landing/overview')
            ->scripts('ext/landing/overview')
            ->using([
                'tsb_recent'    => $tsb_recent,
                'has_tsb'       => $has_tsb,
                'has_logs'      => $has_logs,
            ]);
    }

}