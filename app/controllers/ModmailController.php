<?php
namespace app\controllers;

class ModmailController extends AuthController {

    protected function searchpage(array $params) {
        return view('net.applayout', 'modmail.search')
            ->meta('Modmail Search', 'page--modmail page--modmail_search')
            ->usinglib(['asi', 'pikaday', 'snudown'])
            ->stylesheets(['ext/modmail/common', 'ext/modmail/search'])
            ->scripts([
                'ext/modmail/common',
                'ext/modmail/search',
                'ext/modmail/searchconf',
            ])
            ->addInitOpt('ModmailSearch', 'subreddits', alphasort(user()->modded('all:modmail')->to_array()));
    }

    protected function lookuppage(array $params) {

    }
}