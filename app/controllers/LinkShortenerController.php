<?php
namespace app\controllers;

use app\lib\services\LinkShortenerService;

class LinkShortenerController extends AuthController {

    protected function default_action(array $params) {
        return view('net.applayout', 'linkshortener.index')
            ->meta('URL Shortener | Layer 7 Solutions', 'page--shortlink')
            ->use('linkdata', LinkShortenerService::get_links(user()->get_username()))
            ->stylesheets('ext/linkshortener/index')
            ->use('app-no-gutter', true)
            ->scripts('ext/linkshortener/linkshortener');
    }

}