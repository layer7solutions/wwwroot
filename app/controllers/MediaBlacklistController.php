<?php
namespace app\controllers;

class MediaBlacklistController extends AuthController {

    protected function edit_page(array $params) {
        return view('net.applayout', 'blacklist.mediablacklist')
            ->meta('Blacklist | Layer 7 Solutions', 'page--tsb page--tsb_blacklist')
            ->scripts('ext/blacklist/blacklist_edit')
            ->stylesheets([
                'ext/blacklist/blacklist_edit',
            ])
            ->using([
                'is_admin'        => user()->is_admin(),
                'subreddits'      => alphasort(user()->modded('all:tsb')->to_array()),
            ]);
    }

    protected function view_page(array $params) {
        return view('net.applayout', 'blacklist.mediaview')
            ->meta('View Blacklist | Layer 7 Solutions', 'page--tsb page--tsb_view')
            ->usinglib(['selectr-css', 'selectr-js'])
            ->scripts('ext/blacklist/blacklist_view')
            ->stylesheets([
                'ext/blacklist/blacklist_view',
            ])
            ->using([
                'is_admin'    => user()->is_admin(),
                'subreddits'  => alphasort(user()->modded('all:tsb')->to_array()),
            ]);
    }

    protected function reports_page(array $params) {
        return view('net.applayout', 'blacklist.mediareports')
            ->meta('Media Reports | Layer 7 Solutions', 'page--tsb page--tsb_reports')
            ->using([
                'search_options' => [
                    [
                        'type'          => 'channel',
                        'title'         => 'Search by channel URL',
                        'placeholder'   => 'media URL or channel ID',
                    ],
                    [
                        'type'          => 'user',
                        'title'         => 'Search by reddit user',
                        'placeholder'   => 'username or userpage link',
                    ],
                    [
                        'type'          => 'channel_name',
                        'title'         => 'Search by channel name',
                        'placeholder'   => 'channel name / keyword(s)'
                    ]
                ]
            ])
            ->usinglib('snudown')
            ->scripts([
                'ext/blacklist/ReportComponent',
                'ext/blacklist/blacklist_reports',
            ])
            ->stylesheets([
                'ext/blacklist/blacklist_reports',
            ]);
    }

    protected function globalblacklist_page(array $params) {
        $admin = from($params, 'admin') ?? false;

        return view('net.applayout', 'blacklist.globalqueue')
            ->meta('Global Blacklist Queue',
                $admin ? 'page--admin page--admin_globalqueue' : 'page--tsb page--tsb_global')
            ->scripts('ext/blacklist/blacklist_global')
            ->stylesheets([
                'ext/blacklist/globalqueue'
            ])
            ->use('is_admin', $admin)
            ->addInitOpt('BlacklistGlobal', 'is_admin', $admin);
    }
}