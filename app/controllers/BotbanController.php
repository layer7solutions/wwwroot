<?php

namespace app\controllers;

class BotbanController extends AuthController {

    protected function default_action(array $params) {
        return view('net.applayout', 'blacklist.simpleblacklist')
            ->meta('Botban', 'page--botban')
            ->addInitOpt('SimpleBlacklist', 'sbtconf', subreddit()->botban->jsconf())
            ->using([
                'blacklist_title' => 'Bot bans',
                'blacklist_desc' => 'Any posts/comments made by users you ban on this page will ' .
                    'be silently removed by TheSentinelBot.',
            ]);
    }

}