<?php

namespace app\controllers;

class LandingController extends \Controller {

    protected function default_action(array $params) {
        if (\App::request_uri() != '/homepage' && user()->logged_in()) {
            redirect('/overview');
        }

        return view('landing.home')
            ->meta('Layer 7 Solutions', 'page--landing page--home')
            ->stylesheets('ext/landing/common');
    }

    protected function about(array $params) {
        return view('landing.about')
            ->meta('About | Layer 7 Solutions', 'page--landing page--about')
            ->stylesheets('ext/landing/common');
    }

    protected function ntpls(array $params) {
        return view('landing.ntpls')
            ->useLayout(false)
            ->meta('nt pls', 'page--landing page--ntpls');
    }

}