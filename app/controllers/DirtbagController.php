<?php

namespace app\controllers;

class DirtbagController extends AuthController {

    protected function selection_page(array $params) {
        return view('net.applayout', 'dirtbag.settings-srselect')
            ->meta('Dirtbag Settings - Choose Subreddit', 'page--dirtbag')
            ->use('dirtbag_srlist', user()->modded('all:dirtbag')->to_array());
    }

    protected function settings_page(array $params) {
        return view('net.applayout', 'dirtbag.settings')
            ->meta('Dirtbag Settings', 'page--dirtbag page--dirtbag_settings')
            ->use('subreddit', $params['subreddit'])
            ->addInitOpt('DirtbagSettings', 'subreddit', $params['subreddit'])
            ->stylesheets('ext/dirtbag/settings')
            ->scripts([
                'ext/dirtbag/settings',
                'ext/dirtbag/settings_config',
                'ext/dirtbag/settings_handle',
            ]);
    }

}