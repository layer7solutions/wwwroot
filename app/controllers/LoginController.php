<?php

namespace app\controllers;

use app\lib\UserSession;

class LoginController extends \Controller {

    protected function login(array $params) {
        if (user()->logged_in()) {
            redirect_cont();
        }
        UserSession::authorize(); // also handles redirect
    }

    protected function logout(array $params) {
        // If there's no user to logout, then redirect
        if (!user()->logged_in()) {
            redirect_cont();
        }

        // Validate CSRF token
        if (!session_csrf_token_validity()) {
            return $this->error(403);
        }

        // Try session exit and redirect, otherwise die
        if (user()->session->exit()) {
            redirect_cont();
        } else {
            die('Failed to logout. Click the back button in your browser and try again later.');
        }
    }

    protected function interstitial(array $params) {
        if (isset($_REQUEST['error'])) {
            UserSession::dispatch_auth_fail_page($_REQUEST['error']);
        }

        if ( (empty($_REQUEST['code']) || empty($_REQUEST['state'])) && !test_feature_enabled('test') ) {
            if (user()->logged_in()) {
                redirect_cont();
            }
            die('Invalid request.');
        }

        return view('net.logininterstitial')
            ->meta('Logging you in', 'page--login page--login_interstitial')
            ->stylesheets('ext/logininterstitial')
            ->scripts('ext/logininterstitial')
            ->addInitOpt('LoginInterstitial', 'test', test_feature_enabled('test'));
    }

}