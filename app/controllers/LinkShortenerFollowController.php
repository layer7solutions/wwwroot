<?php
namespace app\controllers;

use app\lib\services\LinkShortenerService;

// Separate controller because LinkShortenerController extends AuthController
class LinkShortenerFollowController extends \Controller {

    protected function default_action(array $params) {
        $short_id = from($params, 'short_id');

        if (empty($short_id)) {
            return false;
        }

        $original_url = LinkShortenerService::get_link($short_id);

        if (empty($original_url)) {
            return false;
        }

        if (LinkShortenerService::is_deleted($short_id)) {
            return view('linkshortener.deleted')->useLayout(false);
        }

        LinkShortenerService::increment_hits($short_id);

        redirect_ext($original_url);
        exit;
    }

}