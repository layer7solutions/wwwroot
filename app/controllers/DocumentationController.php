<?php

namespace app\controllers;

class DocumentationController extends \Controller {

    private function redirectIfNecessary(string $name): void {
        switch ($name) {
            case 'about':
                redirect('/about');
                exit;
            case 'index':
            case '':
                redirect('/docs/get-started');
                exit;
        }
    }

    private function guardAgainstNotFound(string $name): array {
        return template_exists('docs.'.$name) ? [$name, null] : ['404', '404 | Documentation'];
    }

    private function getViewDocName(array $params): string {
        if (isset($params['name']))
            return $params['name'];

        $parts = [];
        for ($i = 0; isset($params['p'.$i]); $i++) {
            $parts[] = $params['p'.$i];
        }

        return implode('_', $parts);
    }

    private function getLibs(string $name): array {
        $libs = ['lato_font', 'docs'];
        if (startsWith($name, 'apidocs_') || $name === 'apidocs' || startsWith($name, 'internal_')) {
            $libs[] = 'code_prettify';
        }
        return $libs;
    }

    protected function default_action(array $params) {
        $name = $this->getViewDocName($params);

        $this->redirectIfNecessary($name);

        list($name, $pagetitle) = $this->guardAgainstNotFound($name);

        return view('docs.'.$name)
            ->meta($pagetitle ?? 'Documentation', 'full-nav page--docs page--docs_'.$name)
            ->usinglib($this->getLibs($name));
    }

    protected function legal(array $params) {
        $view = $this->default_action($params);
        $view->stylesheets('ext/docs/legaldocs');
        return $view;
    }

    protected function internal(array $params) {
        if (!user()->logged_in() || !user()->is_admin()) {
            return $this->error(403);
        }
        return $this->default_action($params);
    }

}