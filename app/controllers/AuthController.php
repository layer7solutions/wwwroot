<?php
namespace app\controllers;

abstract class AuthController extends \Controller {

    // Users can acess the test site if they mod any of these subs
    const APPROVED_TEST_MODDED = ['layer7', 'TheSentinelBot'];

    // Users that can access the test site that do not mod r/Layer7 or r/TheSentinelBot
    const APPROVED_TEST_USERS = ['kwwxis', 'kwwxis2', 'kwwxis3'];

    public function __construct() {
        $this->middleware(function($params) {
            if (!user()->logged_in() || test_feature_enabled('no_auth')) {
                return view('net.loginrequired')
                    ->stylesheets('ext/loginrequired')
                    ->meta('You must be logged in to access this page', 'page--loginrequired page--no_auth');
            }

            if (SITE_IS_STAGING &&
                    !user()->modded->has_any(self::APPROVED_TEST_MODDED) &&
                    !in_array(user()->get_username(), self::APPROVED_TEST_USERS)) {
                return view('net.loginprodsite')
                    ->meta('You should use the main site!', 'page--use_prod_site');
            }
        });
    }

}
