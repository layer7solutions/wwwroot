<?php
namespace app\controllers;

use app\lib\util\AdminTools;

class AdminController extends \Controller {
    const ALLOWED_PAGES_FOR_GLOBALISTS = ['/globalqueue'];

    public function __construct() {
        $this->middleware(function($params) {
            $page = \App::request_uri(false, '/admin');

            if ($page == 'websync') {
                // pass
            } else if (!user()->logged_in()) {
                return $this->error(403);
            } else if (in_array($page, self::ALLOWED_PAGES_FOR_GLOBALISTS)) {
                if (!user()->is_globalist())
                    return $this->error(403);
            } else {
                if (!user()->is_admin())
                    return $this->error(403);
            }
        });
    }

    protected function default_action(array $params) {
        return false;
    }

    protected function websync(array $params) {
        dispatch_json(api_directCall('Websync', null, null, $_REQUEST));
    }

    protected function adminer(array $params) {
        redirect(SITE_URL.'admin/adminer.php');
    }

    protected function globalqueue(array $params) {
        \App::runController('MediaBlacklist:globalblacklist_page', ['admin' => true]);
    }

    protected function userlist(array $params) {
        return view('net.applayout', 'admin.userlist')
            ->meta('User List | Admin', 'page--admin page--admin_userlist')
            ->use('userlist', AdminTools::get_users());
    }

    protected function oauth_data(array $params) {
        return view('net.applayout', 'admin.oauth_data')
            ->meta('TSB OAuth Data | Admin', 'page--admin page--admin_oauth_data')
            ->scripts('ext/admin/oauth_data')
            ->use('oauth_data', AdminTools::oauthdata_getAll());
    }

    protected function page500(array $params) {
        invoke_500_error_page();
    }

    protected function enable(array $params) {
        user()->admin(true);
        redirect_cont();
    }

    protected function disable(array $params) {
        user()->admin(false);
        redirect_cont();
    }

    protected function phpinfo(array $params) {
        phpinfo();
        die;
    }

}