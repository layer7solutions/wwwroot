<?php
namespace app\controllers;

use app\lib\SubredditMethods;
use app\lib\logs\Logs;
use app\lib\SubredditException;
use app\lib\InvalidSubredditException;

class LogsController extends AuthController {

    protected function view_page(array $params) {
        return view('net.applayout', 'logs.view')
            ->meta('Moderation Logs', 'page--logs page--logs_view')
            ->usinglib(['asi', 'pikaday', 'snudown'])
            ->stylesheets([
                'ext/logs/view',
                'ext/logs/modactions',
            ])
            ->scripts([
                'ext/logs/logsview',
                'ext/logs/logsviewconf',
            ])
            ->addInitOpt('LogsView', 'subreddits',    alphasort(user()->modded('all:logs')->to_array()))
            ->addInitOpt('LogsView', 'action_types',  Logs::get_action_types());
    }

    protected function matrix_selection_page(array $params) {
        return view('net.applayout', 'logs.matrixselection')
            ->meta('Choose matrix subreddits', 'page--logs page--logs_matrix')
            ->stylesheets('ext/logs/matrixpage')
            ->scripts('ext/logs/matrix')
            ->using([
                'all_available_subreddits'  => alphasort(user()->modded('all:logs')->to_array()),
                'subreddit'                 => from($params, 'subreddit'),
                'is_invalid_selection'      => $params['is_invalid_selection'] ?? false,
            ]);
    }

    protected function matrix_page(array $params) {
        try {
            $subreddit = user()->modded(from($params, 'subreddit'));

            if (empty($subreddit->all_moderators)) {
                throw new InvalidSubredditException($subreddit);
            }
        } catch (SubredditException $e) {
            return $this->matrix_selection_page([
                'subreddit' => $subreddit ?? null,
                'is_invalid_selection' => true,
            ]);
        }

        $data = [
            'body_class'                => 'page--logs page--logs_matrix',
            'subreddit'                 => $subreddit,
            'action_types'              => Logs::get_action_types(Logs::BY_GROUP),
            'earliest_action_utc'       => Logs::get_oldest_record($subreddit->to_array())['action_utc'],
            'moderators'                => $subreddit->all_moderators,
            'current_moderators'        => $subreddit->moderators,
            'past_moderators'           => $subreddit->past_moderators,
            'full_screen'               => false,
        ];

        // direct matrix
        if (isset($_REQUEST['from']) || isset($_REQUEST['to'])) {
            if (isset($_REQUEST['fullscreen']) && bool_unstr($_REQUEST['fullscreen'])) {
                $data['body_class'] .= ' page--logs_matrix--FullScreen';
                $data['full_screen'] = true;
            }
            $data['direct_matrix'] = $this->create_direct_matrix($data, $_REQUEST);
        }

        return view('net.applayout', 'logs.matrix')
            ->meta('Mod matrix for ' . ((string) $subreddit), $data['body_class'])
            ->usinglib('pikaday')
            ->stylesheets([
                'ext/logs/matrixpage',
                'ext/logs/matrixcomponent',
                'ext/logs/modactions',
            ])
            ->scripts([
                'lib/hammer',
                'ext/logs/matrix',
                'ext/logs/matrixscroll',
            ])
            ->using($data)
            ->addInitOpt('LogsMatrix', 'subreddits', alphasort($data['subreddit']->to_array()))
            ->addInitOpt('LogsMatrix', 'past_moderators', $data['past_moderators'])
            ->addInitOpt('LogsMatrix', 'has_direct_matrix', isset($data['direct_matrix']));
    }

    protected function create_direct_matrix(array $data, array $params) {
        return api_directCall('LogsMatrix', 'GET', ['subreddit' => $data['subreddit']], [
            'noemptycolumns'    => $params['noemptycolumns'] ?? false,
            'sort'              => $params['sort'] ?? null,
            'moderators'        => $params['moderators'] ?? 'all',
            'actions'           => $params['actions'] ?? 'all',
            'lowerbound'        => valid_int($lowerbound = $params['from'] ?? $data['earliest_action_utc'])
                                    ? intval($lowerbound) : strtotime($lowerbound),
            'upperbound'        => valid_int($upperbound = (isset($params['to']) && $params['to'] != 'now') ? $params['to'] : time())
                                    ? intval($upperbound) : strtotime($upperbound),
            'component'         => true,
        ]);
    }

}