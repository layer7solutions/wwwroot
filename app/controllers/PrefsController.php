<?php
namespace app\controllers;

use app\lib\Common;
use app\lib\services\OAuthApps;

class PrefsController extends AuthController {
    const SESSION_LIFETIME_OPTIONS = [
        0        => ['When I close my browser' .
            "<span class='icon icon--help'
                data-tippy='If you have \"Continue where you left off\" enabled in Chrome or
                \"Show your windows and tabs from last time\" enabled in Firefox, then your session
                may be preserved regardless of this setting.'>
                <i class='zmdi zmdi-help'></i>
            </span>", false],
        86400    => ['In 24 hours', false],
        604800   => ['In 1 week', false],
        2678400  => ['In 1 month (default)', false],
        31536000 => ['In 1 year (max)', false],
    ];

    protected function default_action(array $params) {
        $custom_lifetime = user()->property('session_lifetime');

        // set custom_lifetime to -1 if session_lifetime is one of the default options to avoid
        // duplicate
        if (in_array($custom_lifetime, array_keys(self::SESSION_LIFETIME_OPTIONS))) {
            $custom_lifetime = -1;
        }

        return view('net.applayout', 'prefs.layout', 'prefs.userprefs')
            ->meta('User Preferences', 'page--prefs_user')
            ->stylesheets('ext/prefs/settings')
            ->use('prefs', user()->prefs->get_all())
            ->use('session_lifetime_options',
                array_merge(array_map_to_seq(self::SESSION_LIFETIME_OPTIONS), [
                    [$custom_lifetime, 'Custom lifetime (in seconds)', true]
                ])
            );
    }

    protected function account_page(array $params) {
        $page = $params['page'];

        $title = $page == 'activity' ? 'Account Activity' : 'User Data';

        return view('net.applayout', 'prefs.layout', 'prefs.account')
            ->meta($title, "page--prefs_account-{$page} page--prefs_fullWidth")
            ->stylesheets('ext/prefs/settings')
            ->use('page', $page)
            ->using([
                'sessions' => session_get_handler()->get_sessions(user()->get_username()),
                'current_session_id' => user()->session->id(),
            ])
            ->usingIf($page === 'data', function() { return [
                'user_props' => user()->properties(),
                'user_is_blacklisted' => user()->session->isBlacklisted(),
                'user_props_comments' => [
                    'username'      => 'User\'s Reddit username.',
                    'is_moderator'  => '<code>true</code> if user mods at least 1 subreddit.',
                    'last_access'   => 'The most recent time the user accessed this website.',
                    'last_login'    => 'The most recent time the user logged in to this website.',
                    'first_login'   => 'The first time the user logged in to this website.',
                    'websync_flags' => 'This property is used to keep the user\'s modded subreddit list up-to-date.',
                    'refresh_token' => 'Reddit OAuth2 API refresh token. Deleted upon logout. This property is sensitive, do not share it with ' .
                                        'anyone. Logout immediately if it is compromised.',
                    'has_seen_tos'  => '<code>true</code> if user has agreed to the Layer 7 Terms of Service. Upon first login to this site, the ' .
                                        'user agrees to the ToS. The user will be notified if any significant modifications are ' .
                                        'made to the ToS after the user\'s first login.',
                    'access_token'  => 'Reddit OAuth2 API access token. Deleted upon logout or if expired. This property is sensitive, do not share it with ' .
                                        'anyone. Logout immediately if it is compromised.',
                    'access_token_expiry' => 'Time at which the currently stored <code>access_token</code> (if any) expires. Reddit expires all API access ' .
                                        'tokens an hour after they\'re generated. But we set the access token time expiry in our database to be 55 minutes ' .
                                        'instead of 60 in case of slow internet connections.',
                    'session_id'    => 'This property is very sensitive, do not share it with ' .
                                        'anyone. Logout immediately if it is compromised. If the user has multiple active sessions, ' .
                                        'this will contain the id of the most recently accessed session.',
                    'no_ipstore'    => 'If this property is true, this site will refrain from storing the client\'s IP address or sniffing the ' .
                                        'client user-agent whenever possible. IP addresses are only used for extra non-essential security features.',
                    'last_modded_resync' => 'Time at which user\'s TSB modded subs last underwent a complete re-sync',
                    'session_lifetime' => 'Preferred session lifetime for this user',
                    'user_timezone' => 'The user\'s timezone for which datetimes will be displayed in'
                ],
                'user_props_dtlist' => [
                    'last_access',
                    'last_login',
                    'first_login',
                    'access_token_expiry',
                ],
                'user_props_sensitive' => [
                    'refresh_token',
                    'access_token',
                    'session_id',
                ],
                'user_websync_history' => user()->modded->history(),
                'user_real_modded' => user()->real_modded(),
            ];});
    }

    protected function nightmode_disable(array $params) {
        user()->property('nightmode_enabled', false);
        redirect_cont();
    }

    protected function nightmode_enable(array $params) {
        user()->property('nightmode_enabled', true);
        redirect_cont();
    }

    protected function apps_page(array $params) {
        return view('net.applayout', 'prefs.layout', 'prefs.apps')
            ->meta('Applications', 'page--prefs_apps')
            ->scripts('ext/prefs/apps')
            ->stylesheets('ext/prefs/apps')->using([
                'authorized_apps' => OAuthApps::get_authorized_apps(user()->get_username()),
                'developed_apps'  => OAuthApps::get_developed_apps(user()->get_username()),
            ]);
    }

    protected function apps_dispatchError(string $error_msg) {
        redirect('/prefs/apps?error='.rawurlencode('<b>Error:</b> '. $error_msg).'#');
    }

    protected function apps_postAction(array $params) {
        if (empty($_POST['action']))
            $this->apps_dispatchError('invalid request');
        if (!session_csrf_token_validity())
            $this->apps_dispatchError('not authorized');

        switch ($_POST['action']) {
            case 'app_revoke':
                $client_id = from($_POST, 'client_id');

                if (!OAuthApps::app_exists($client_id)) {
                    $this->apps_dispatchError('application does not exist');
                }

                $app_name = OAuthApps::get_app($client_id)['app_name'];

                OAuthApps::revoke_access($client_id, user()->get_username());

                redirect('/prefs/apps?status='.
                    rawurlencode('Succesfully revoked access to <b>' . $app_name . '</b>').'#');
                exit;
            case 'app_modify':
                $client_id      = from($_POST, 'client_id');
                $name           = from($_POST, 'app_name');
                $about          = from($_POST, 'app_desc');
                $about_url      = from($_POST, 'app_about_url');
                $redirect_uri   = from($_POST, 'app_redirect_uri');

                if (!OAuthApps::app_exists($client_id)) {
                    $this->apps_dispatchError('application does not exist');
                }

                OAuthApps::modify_app($client_id, $name, $about, $about_url, $redirect_uri, user()->get_username());

                redirect('/prefs/apps?modified='.$client_id . '#dev_'.$client_id);
                exit;
            case 'app_create':
                $name           = from($_POST, 'app_name');
                $about          = from($_POST, 'app_desc');
                $about_url      = from($_POST, 'app_about_url');
                $redirect_uri   = from($_POST, 'app_redirect_uri');

                $client_id = OAuthApps::create_new_app($name, $about, $about_url, $redirect_uri);

                redirect('/prefs/apps?created='.$client_id . '#dev_'.$client_id);
                exit;
            case 'app_delete':
                $client_id = from($_POST, 'client_id');

                if (!OAuthApps::app_exists($client_id)) {
                    $this->apps_dispatchError('application does not exist');
                }

                $data = OAuthApps::get_app($client_id);
                OAuthApps::delete_app($client_id, user()->get_username());

                redirect('/prefs/apps?status='.rawurlencode('Succesfully deleted app: <b>' . $data['app_name'] . '</b>').'#');
                exit;
        }
    }

    protected function subreddit_page(array $params) {
        return view('net.applayout', 'prefs.layout', 'prefs.subsettings')
            ->meta('Subreddit Settings', 'page--prefs_subsettings page--prefs_fullWidth')
            ->scripts('ext/prefs/subsettings')
            ->stylesheets('ext/prefs/subsettings')
            ->useIf('srlist', !test_feature_enabled('no_subreddits'), function() {
                return alphasort(user()->modded->to_array());
            })
            ->usingIf(isset($params['subreddit']) && user()->modded->has($params['subreddit']),
                function() use ($params) {
                    return [
                        'srcomp_name' =>
                            strtolower($params['subreddit']),
                        'srcomp_html' =>
                            user()->modded($params['subreddit'])
                                ->api_call('SubredditSettings', 'GET', [
                                    'action'    => 'get_sr_component',
                                ])
                    ];
                });
    }

}