<?php

namespace app\controllers;

class DomainsController extends AuthController {

    protected function default_action(array $params) {
        return view('net.applayout', 'blacklist.simpleblacklist')
            ->meta('Domain Blacklist', 'page--domainblacklist')
            ->addInitOpt('SimpleBlacklist', 'sbtconf', subreddit()->domainban->jsconf())
            ->using([
                'blacklist_title' => 'Domain bans',
            ]);
    }

}