<?php
autoload_oauth2_server();

$storage = new \OAuth2\Storage\Pdo(array(
    'dsn'       => 'pgsql:host='.PGSQL_SERVER.';dbname=oauth2;',
    'username'  => OAUTH2DB_USERNAME,
    'password'  => OAUTH2DB_PASSWORD
));

$server = new \OAuth2\Server($storage);

$scopeMemory = new \OAuth2\Storage\Memory(array(
    'default_scope'     => api_default_scope(),
    'supported_scopes'  => array_keys(api_scopes()),
));
$scopeUtil = new \OAuth2\Scope($scopeMemory);

$server->setScopeUtil($scopeUtil);

$server->addGrantType(new \OAuth2\GrantType\ClientCredentials($storage));
$server->addGrantType(new \OAuth2\GrantType\AuthorizationCode($storage));
$server->addGrantType(new \OAuth2\GrantType\RefreshToken($storage));

return $server;