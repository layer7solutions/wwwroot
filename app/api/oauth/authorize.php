<?php
$request = \OAuth2\Request::createFromGlobals();
$response = new \OAuth2\Response();

// validate the authorize request
if (!$server->validateAuthorizeRequest($request, $response)) {
    $p = $response->getParameters();

    api_dispatchError($p['error'], $p['error_description'], null, $response->getStatusCode());
    die();
}

if (empty($_POST)) {
    view('net.oauth_authorize')->title('Authorize')->using([
        'logged_in'     => user()->logged_in(),
        'username'      => user()->get_username(),
        'app_name'      => db('oauth2')->queryFirstField('SELECT app_name FROM oauth_clients WHERE client_id=%s',
                                from($_REQUEST, 'client_id')),
        'app_about'     => db('oauth2')->queryFirstField('SELECT app_about FROM oauth_clients WHERE client_id=%s',
                                from($_REQUEST, 'client_id')),
        'app_url'       => db('oauth2')->queryFirstField('SELECT app_url FROM oauth_clients WHERE client_id=%s',
                                from($_REQUEST, 'client_id')),
    ])->dispatch();
} else {
    if (!user()->logged_in()) {
        die('Must be logged in to connect an application with your account!');
    }

    // print the authorization code if the user has authorized your client
    $is_authorized = ($_POST['authorized'] === 'yes');
    $server->handleAuthorizeRequest($request, $response, $is_authorized, user()->get_username());
    if ($is_authorized && test_feature_enabled('print_auth_code')) {
        // this is only here so that you get to see your code in the cURL request. Otherwise, we'd redirect back to the client
        $code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
        exit("SUCCESS! Authorization Code: $code");
    }
    $response->send();
}
