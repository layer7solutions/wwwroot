<?php
namespace app\api\resource;

use app\lib\services\GlobalQueue;

class BlacklistGlobal extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'tsbread';
    }

    // Cancel - remove from database if admin or matching enqueued_by user
    public function action_cancel($id) {
        $enqueued_by = db('application')->queryFirstField(
                "SELECT enqueued_by FROM webqueue_globalblacklist WHERE id=%i", $id);

        if (user()->is_globalist() || $enqueued_by == user()->get_username()) {
            db('application')->delete('webqueue_globalblacklist', "id=%i", $id);
            return true;
        } else {
            api_notAuthorized();
            exit;
        }
    }

    public function execute(string $method, array $path, array $query): void {
        if (startsWith($path['action'], 'admin')) {
            if (!user()->is_globalist()) {
                api_notAuthorized();
            }
        }

        switch ($path['action']) {
            case 'get':
                $this->set_json(GlobalQueue::get(user()));
                return;
            case 'enqueue':
                $this->set_json(GlobalQueue::enqueue(
                    api_requireParameter('media_url', $query),
                    api_optionalParameter('reason', $query)
                ));
                return;
            case 'admin_approve':
                $this->set_json([
                    'success' => GlobalQueue::approve(api_requireParameter('id', $query, 'valid_int'))
                ]);
                return;
            case 'admin_deny':
                $this->set_json([
                    'success' => GlobalQueue::deny(api_requireParameter('id', $query, 'valid_int'))
                ]);
                return;
            case 'cancel':
                $this->set_json([
                    'success' =>$this->action_cancel(api_requireParameter('id', $query, 'valid_int'))
                ]);
                return;
            default:
                api_unknownField('action');
        }
    }

}