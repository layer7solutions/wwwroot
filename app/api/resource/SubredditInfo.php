<?php
namespace app\api\resource;

use app\lib\Common;

class SubredditInfo extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'srprefread';
    }

    public function execute(string $method, array $path, array $query): void {
        $this->field('subreddit', function() {
            return $this->subreddit->to_array();
        });

        $this->field('srIn', function() {
            return (string) $this->subreddit->getSrIn();
        });

        $this->field('title', function() {
            return (string) $this->subreddit;
        });

        $this->field('config', function() {
            $config = [];

            foreach ($this->subreddit->to_array() as $sr) {
                $config[$sr] = subreddit_explicit($sr)->config->get();
            }

            return $config;
        });

        $this->field('moderators.real', function() {
            return $this->subreddit->moderators(true);
        });

        $this->field('moderators.logged', function() {
            if (count($this->subreddit) === 1) {
                // This should be faster for one subreddit
                return [ "{$this->subreddit[0]}" => $this->subreddit->logged_moderators() ];
            } else {
                return $this->subreddit->logged_moderators(true);
            }
        });

        $this->field('id.split', function() {
            return $this->subreddit->id(true);
        });

        $this->field('id.combined', function() {
            return $this->subreddit->id;
        });
    }
}