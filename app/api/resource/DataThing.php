<?php
namespace app\api\resource;

use app\lib\data\GeneralQueryParser;
use app\lib\data\InvalidQueryException;
use app\lib\data\ThingQueryPlanner;
use app\lib\data\QueryExecutor;

class DataThing extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'thingread';
    }

    public function execute(string $method, array $path, array $query): void {
        $parser = new GeneralQueryParser();
        $planner = new ThingQueryPlanner();
        $executor = new QueryExecutor();

        $components = [];
        foreach ($query as $k => $v) {
            switch (strtolower($k)) {
                case 'feature':
                    break;
                case 'limit':
                    if (!valid_int($v)) {
                        api_dispatchError('INVALID_QUERY', "value of 'limit' must be an integer, got $k", $k, 400);
                    }
                    $components[$k] = intval($k);
                    break;
                case 'fields':
                case 'where':
                    try {
                        $components[$k] = $parser->parse($v);
                    } catch (InvalidQueryException $e) {
                        api_dispatchError('INVALID_QUERY', $e->getMessage());
                    }
                    break;
                case 'order':
                    $components[$k] = $v;
                    break;
                default:
                    api_dispatchError('INVALID_QUERY', "unknown property - $k", $k, 400);
            }
        }

        if (test_feature_enabled('parse_only')) {
            $this->set_json($components);
            return;
        }

        try {
            $prepared_statement = $planner->build($components);
        } catch (InvalidQueryException $e) {
            api_dispatchError('INVALID_QUERY', $e->getMessage());
            exit;
        }

        if (test_feature_enabled('build_only')) {
            $this->set_json($prepared_statement);
            return;
        }

        $result = $executor->execute($prepared_statement['query'], $prepared_statement['params']);

        if (isset($result->error)) {
            api_dispatchError('QUERY_ERROR', $result->error->getMessage());
        }

        $this->set_json([
            'listing' => $result->result,
            'cont' => null,
        ]);
    }
}