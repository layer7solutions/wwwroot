<?php
namespace app\api\resource;

use app\lib\mixins\ModmailModule;

class ModmailSearch extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'modmailread';
    }

    public function execute(string $method, array $path, array $query_params): void {
        $query  = api_requireParameter('query',  $query_params);
        $amount = api_optionalParameter('amount', $query_params, 'valid_int');

        switch (api_optionalParameter('order', $query_params)) {
            case 'relevance':
                $order = ModmailModule::MOST_RELEVANT;
                break;
            case 'old':
                $order = ModmailModule::MOST_AGED;
                break;
            case 'new':
            default:
                $order = ModmailModule::MOST_RECENT;
                break;
        }

        $search_results = $this->subreddit->modmail->search($query, $amount, $order);

        $this->set_json([
            'query'     => $query,
            'listing'   => $search_results,
            'order'     => $order,
            'amount'    => count($search_results),
        ]);
    }

}