<?php
namespace app\api\resource;

use app\lib\services\WebsyncProcess;

class Websync extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return false;
    }

    public function add_history() {
        $type      = from($_REQUEST, 'type');
        $subreddit = from($_REQUEST, 'subreddit');
        $moderator = from($_REQUEST, 'moderator');
        $new_state = from($_REQUEST, 'new_state');
        $wsync_utc = time();

        $insert_data = [];

        if (isset($type)) $insert_data['type'] = $type;
        if (isset($subreddit)) $insert_data['subreddit'] = $subreddit;
        if (isset($moderator)) $insert_data['moderator'] = $moderator;
        if (isset($new_state)) $insert_data['new_state'] = $new_state;
        if (isset($wsync_utc)) $insert_data['history_utc'] = $wsync_utc;

        db('application')->insert('websync_history', $insert_data);
    }

    public function execute(string $method, array $path, array $query): void {
        $secret = from($_SERVER, 'HTTP_WEBSYNC_SECRET_KEY');
        if ($secret !== WEBSYNC_SECRET_KEY && !user()->is_admin()) {
            die('Not authorized');
        }

        $subreddit = from($query, 'subreddit');
        $moderator = from($query, 'moderator');
        $new_state = from($query, 'new_state');

        switch (from($query, 'type')) {
            case 'tsbaccept':
                if (empty($subreddit) || empty($new_state)) {
                    $this->set_json(['state' => false]);
                    return;
                }

                $this->add_history();
                $moderators = array_map('trim', explode(',', $new_state));

                $tmp_flag = to_bool(from($query, 'tmp') ?: false);

                if (!empty($moderators)) {
                    WebsyncProcess::websync_tsbaccept($subreddit, $moderators, $tmp_flag);
                }
                break;
            case 'addmoderator':
            case 'acceptmoderatorinvite':
                if (empty($subreddit) || empty($moderator)) {
                    $this->set_json(['state' => false]);
                    return;
                }

                $this->add_history();
                WebsyncProcess::websync_modadd($subreddit, $moderator);

                break;
            case 'removemoderator':
                if (empty($subreddit) || empty($moderator)) {
                    $this->set_json(['state' => false]);
                    return;
                }

                $this->add_history();
                WebsyncProcess::websync_modremove($subreddit, $moderator);

                break;
            case 'setpermissions':
                if (empty($subreddit) || empty($moderator) || empty($new_state)) {
                    $this->set_json(['state' => false]);
                    return;
                }

                $this->add_history();
                WebsyncProcess::websync_modperms($subreddit, $moderator, $new_state);

                break;
            default:
                $this->set_json(['state' => false]);
                return;
        }

        $this->set_json(['state' => true]);
    }

}