<?php
namespace app\api\resource;

use app\lib\util\AdminTools;

class OAuthData extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return false;
    }

    public function execute(string $method, array $path, array $query): void {
        if (!user()->is_admin()) {
            die('403');
        }

        if ($method == 'PUT') {
            $username    = api_requireParameter('username', $query);
            $password    = api_requireParameter('password', $query);
            $app_id      = api_requireParameter('app_id', $query);
            $app_secret  = api_requireParameter('app_secret', $query);
            $agent_of    = api_requireParameter('agent_of', $query);

            AdminTools::oauthdata_create($username, $password, $app_id, $app_secret, $agent_of);
        } else {
            if (isset($query['for'])) {
                if (isset($query['password'])) {
                    $this->set_json(['item' => AdminTools::oauthdata_getPassword($query['for']) ]);
                } else if (isset($query['app_secret'])) {
                    $this->set_json(['item' => AdminTools::oauthdata_getSecret($query['for']) ]);
                } else {
                    $this->set_json(AdminTools::oauthdata_get($query['for']));
                }
            } else {
                $this->set_json(AdminTools::oauthdata_getAll());
            }
        }
    }

}