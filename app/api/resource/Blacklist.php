<?php
namespace app\api\resource;

class Blacklist extends APIResource {
    private static $subject_types = ['media_url', 'user', 'domain', 'list'];

    public function scope(string $method, array $path, array $query) {
        if ($method == 'GET') {
            return 'tsbread';
        } else {
            return 'tsbedit';
        }
    }

    public function execute(string $method, array $path, array $query): void {
        $subject_type   = null;
        $subject_value  = null;

        foreach (self::$subject_types as $type) {
            if (!empty($query[$subject_type = $type])) {
                $subject_value = array_filter(explode(',', trim($query[$subject_type], ', ') ));
                break;
            }
        }

        if (empty($subject_value)) {
            api_badParameter(self::$subject_types, true);
        }

        switch ($subject_type) {
            case 'media_url':
                if ($method == 'GET') {
                    $this->set_json($this->subreddit->mediaban->check($subject_value));
                } else if ($method == 'PUT') {
                    $this->set_json($this->subreddit->mediaban->add($subject_value));
                } else if ($method == 'DELETE') {
                    $this->set_json($this->subreddit->mediaban->remove($subject_value));
                }
                break;
            case 'user':
                if ($method == 'GET') {
                    $this->set_json($this->subreddit->botban->check($subject_value));
                } else if ($method == 'PUT') {
                    $this->set_json($this->subreddit->botban->add($subject_value));
                } else if ($method == 'DELETE') {
                    $this->set_json($this->subreddit->botban->remove($subject_value));
                }
                break;
            case 'domain':
                if ($method == 'GET') {
                    $this->set_json($this->subreddit->domainban->check($subject_value));
                } else if ($method == 'PUT') {
                    $this->set_json($this->subreddit->domainban->add($subject_value));
                } else if ($method == 'DELETE') {
                    $this->set_json($this->subreddit->domainban->remove($subject_value));
                }
                break;
            case 'list':
                $a = $query['amount'] ?? 25;

                if (isset($query['page'])) {
                    $b = $query[$listing_type = 'page'] ?? 1;
                } else {
                    $b = $query[$listing_type = 'offset'] ?? 0;
                }

                $subjects = api_optionalParameter('subject', $query);
                if (empty($subjects) || $subjects == 'all') {
                    $subjects = null;
                } else {
                    $subjects = explode(',', $subjects);
                }

                switch ($subject_value[0]) {
                    case 'media_url':
                        $this->set_json($this->subreddit->mediaban->list($a, $b, $listing_type, $subjects));
                        break;
                    case 'user':
                        $this->set_json($this->subreddit->botban->list($a, $b, $listing_type, $subjects));
                        break;
                    case 'domain':
                        $this->set_json($this->subreddit->domainban->list($a, $b, $listing_type, $subjects));
                        break;
                }
                break;
        }
    }

}