<?php
namespace app\api\resource;

use app\lib\logs\Logs;
use app\lib\logs\SearchException;

class LogsView extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'logsread';
    }

    public function execute(string $method, array $path, array $query_params): void {
        $query  = api_optionalParameter('query',  $query_params) ?? '';
        $amount = api_optionalParameter('amount', $query_params, 'valid_int');
        $before = api_optionalParameter('before', $query_params, 'valid_int');
        $after  = api_optionalParameter('after',  $query_params, 'valid_int');

        if (test_feature_enabled('only_query')) {
            $this->set_json([
                'sqldebug' => Logs::prepare_query($query, $amount, $before, $after)
            ]);
        }

        try {
            $ret = Logs::search($query, $amount, $before, $after);
        } catch(SearchException $e) {
            api_dispatchError('QUERY_ERROR', $e->getMessage());
            exit;
        }

        if ($ret === false) {
            api_dispatchError('INVALID_QUERY');
        }

        $this->set_json($ret);
    }

}