<?php
namespace app\api\resource;

use app\lib\SubredditConfig;

class SubredditSettings extends APIResource {

    public function scope(string $method, array $path, array $query) {
        switch (api_optionalParameter('action', $query)) {
            case 'set':
                return 'srprefedit';
            case 'get_sr_component':
            case 'get':
            default:
                return 'srprefread';
        }
    }

    public function execute(string $method, array $path, array $query): void {
        switch (api_optionalParameter('action', $query)) {
            case 'get_sr_component':
                $this->set_html(render_to_string('prefs.subsettings-srcomp', [
                    'srname'    => $this->subreddit->name,
                    'srdata'    => $this->subreddit->row(),
                    'srinfo'    => $this->subreddit->about(),
                    'settings'  => SubredditConfig::SETTINGS,
                    'changelog' => $this->subreddit->config->log(),
                ]));
                return;
            case 'set':
                if (!user()->modded->has_full_perms($this->subreddit[0]) && !user()->admin()) {
                    api_notAuthorized('user must have full permissions');
                }

                $res = $this->subreddit->config->set(
                    api_requireParameter('property', $query),
                    api_requireParameter('value', $query));

                if (!$res['success']) {
                    api_unknownField('property');
                }

                $this->set_json([
                    'property_key'          => $res['property'],
                    'property_new_value'    => $res['new'],
                    'property_old_value'    => $res['old'],
                ]);
                return;
            case 'get':
                $prop = api_requireParameter('property', $query);
                $res = $this->subreddit->config->get($prop);
                if (!isset($res)) {
                    api_unknownField('property');
                }
                $this->set_json([
                    'property_key' => $prop,
                    'property_value' => $res,
                ]);
                return;
            case 'log':
                $this->set_json($this->subreddit->config->log());
                return;
            default:
                $this->set_json($this->subreddit->config->get());
                return;
        }
    }

}