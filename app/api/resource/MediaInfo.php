<?php
namespace app\api\resource;

use app\lib\util\MediaTools;

class MediaInfo extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'tsbread';
    }

    public function execute(string $method, array $path, array $query): void {
        api_requireParameter('url', $query);

        $info = MediaTools::info($query['url']);

        if (!test_feature_enabled('show_response')) {
            unset($info['response']);
        }

        $this->set_json($info);
    }

}