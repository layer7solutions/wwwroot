<?php
namespace app\api\resource;

class DiscoNotify extends APIResource {

    public function scope(string $method, array $path, array $query) {
        switch ($method) {
            case 'GET':
            case 'PUT':
            case 'POST':
                return 'disconotify';
            default:
                return false;
        }
    }

    public function execute(string $method, array $path, array $query): void {
        if ($method == 'PUT') {
            $body_json = $this->get_body();
            $DiscordGuildID = api_requireParameter('guild_id', $body_json);
            $DiscordBotName = api_requireParameter('bot_username', $body_json);
            $TwitterLink    = api_requireParameter('twitter_link', $body_json);

            switch ($DiscordGuildID) {
                case "305133671776649216": // Sweeper Bot Dev - dev-d0cr3d-test
                    $Webhook = DISCORD_SWEEPERBOT_DEV;
                    break;
                case "872128969938452540": // Den Of Wolves - social-feed
                    $Webhook = DISCORD_DENOFWOLVES;
                    break;
                case "408196129470152705": // GTFO - social-feed
                    $Webhook = DISCORD_GTFO;
                    break;
                default:
                    api_dispatchError(
                        'DISCORD_GUILD_NOT_CONFIGURED',
                        'This Discord server is not setup to handle this message relay.',
                        'Contact Mike Wohlrab aka d0cr3d for support.',
                        418);
                    return;
            }

            $headers = [ 'Content-Type: application/json; charset=utf-8' ];
            $discord_data = [ 'username' => $DiscordBotName, 'content' => $TwitterLink ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $Webhook);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($discord_data));
            $response   = curl_exec($ch);

            $this->set_json([
                'response' => 'Successfully posted Webhook request to Discord'
            ]);
        }
    }

    private function get_body()
    {
        $data = file_get_contents('php://input');
        if ($data) {
            return json_decode($data, true);
        }
        return array();
    }

}