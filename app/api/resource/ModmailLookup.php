<?php
namespace app\api\resource;

class ModmailLookup extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'modmailread';
    }

    public function execute(string $method, array $path, array $query_params): void {
        $modmail_id = api_optionalParameter('id', $path);
        $lookup_amount  = api_optionalParameter('amount',  $query_params);

        $search_results = [];

        $this->set_json([
            'listing' => $search_results,
            'amount'  => count($search_results),
        ]);
    }

}