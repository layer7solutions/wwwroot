<?php
namespace app\api\resource;

use app\lib\Common;

class Identity extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'identity';
    }

    public function execute(string $method, array $path, array $query): void {
        if (empty($path['field'])) {
            $this->field('username', function() {
                return user()->get_username();
            });
            $this->field('first_login', function() {
                return user()->property('first_login');
            });
            $this->field('last_login', function() {
                return user()->property('last_login');
            });
            $this->field('last_access', function() {
                return user()->property('last_access');
            });
            $this->field('user_tz.php_timezone', function() {
                return user()->prefs->get_php_name_timezone();
            });
            $this->field('user_tz.display_timezone', function() {
                return user()->prefs->get_display_timezone();
            });
            $this->field('user_tz.gmt_timezone_offset', function() {
                return user()->prefs->get_gmt_timezone_offset();
            });
            $this->field('user_tz.abrv_timezone', function() {
                return user()->prefs->get_abrv_timezone();
            });
            $this->field('prefs', function() {
                return user()->prefs->get_all();
            });

            return;
        }

        switch (from($path, 'field')) {
            case 'prefs':
                $this->set_json([
                    'success' => user()->prefs->setFromJSON(
                        api_requireParameter('property', $query),
                        api_requireParameter('value', $query, null, true)
                    ),
                ]);
                return;
            case 'subreddits':
                if (isset($path['field2'])) {
                    $category = Common::category($path['field2']);
                    if ($category == null) {
                        api_unknownField('category');
                    }
                    $this->set_json(user()->modded('all:'.$category)->to_array());
                    return;
                }
                $this->set_json(
                    user()->modded->to_array()
                );
                return;
            case 'websync_history':
                $this->set_json(
                    user()->modded->history(api_optionalParameter('limit', $query, 'valid_int'))
                );
                return;
            case 'resync_specific_sub':
                $sr = Common::reddit_subreddit(api_requireParameter('subreddit', $query));

                $res = user()->modded->update_specific($sr);

                $this->set_json([
                    'success' => ($res !== false),
                    'record'  => $res ?: [],
                    'sr' => $sr,
                ]);
                return;
            case 'resync_modded_subs':
                api_checkCooldown(user()->property('last_modded_resync'), 5 * 60);

                // update_all() will also update the user's `last_modded_resync` to the current time
                // if succesful

                $res = user()->modded->update_all();

                $this->set_json([
                    'success' => ($res !== false),
                    'records' => $res ?: [],
                ]);
                return;
            case 'mark_terms_seen':
                if (user()->property('has_seen_tos', true)) {
                    $this->set_json( ['success' => true] );
                } else {
                    api_internalError();
                }
                return;
            case 'force_logout':
                $delete_token   = api_optionalParameter('field2', $path);
                $username       = user()->get_username();
                $success        = false;

                if (empty($delete_token)) {
                    $sess_id = user()->session->id();

                    $success = session_get_handler()->safe_delete_all_others($username, $sess_id);

                    if ($success) {
                        $delete_token = session_get_handler()->get_delete_token($sess_id);
                    }
                } else {
                    $success = session_get_handler()->safe_delete($username, $delete_token);
                }

                $this->set_json([
                    'success'       => $success,
                    'delete_token'  => $delete_token,
                ]);

                return;
            default:
                api_unknownField('field');
        }
    }
}