<?php
namespace app\api\resource;

use app\lib\services\LinkShortenerService;

class LinkShorten extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'linkshorten';
    }

    public function execute(string $method, array $path, array $query): void {
        $username = user()->get_username();
        $is_admin = user()->admin();

        switch ($method) {
            case 'GET':
                $_input = api_optionalParameter('short_id', $path);
                $page   = max(api_optionalParameter('page', $query, 'valid_int') ?? 1, 1);
                $amount = api_optionalParameter('amount', $query, 'valid_int');

                if (empty($_input)) {
                    $listing = LinkShortenerService::get_links($username, $page, $amount);
                    $this->set_json([
                        'listing'   => $listing,
                        'page'      => $page,
                        'amount'    => count($listing),
                    ] + LinkShortenerService::get_total($username, $amount));
                } else {
                    $listing = [];

                    foreach (explode(',', $_input) as $short_id) {
                        $visitor_is_owner = $is_admin || LinkShortenerService::get_creator($short_id) === $username;

                        $listing[] =
                            ($visitor_is_owner) ? LinkShortenerService::get_link($short_id, true)
                            : [
                                'original_url' => LinkShortenerService::get_link($short_id, false),
                            ];
                    }

                    $this->set_json([
                        'listing' => $listing,
                        'amount'  => count($listing),
                    ]);
                }
                return;
            case 'POST':
                $original_urls = (array) api_requireParameter('original_url', $query);
                $listing = [];

                foreach ($original_urls as $original_url) {
                    $listing[]  = LinkShortenerService::create_link($username, $original_url);
                }

                if (api_prefer('html')) {
                    $this->set_html(render_to_string('linkshortener.items', [
                        'listing'    => $listing,
                        'flashitems' => api_prefer('flashitems')
                    ]));
                } else {
                    $this->set_json([
                        'listing' => $listing
                    ]);
                }
                return;
            case 'PATCH':
                $short_id = api_requireParameter('short_id', $path);
                $original_url = api_requireParameter('original_url', $query);

                if ($username !== LinkShortenerService::get_creator($short_id) && !$is_admin) {
                    api_notAuthorized();
                }

                $this->set_json([
                    'success' => LinkShortenerService::update_link($short_id, $original_url),
                ]);
                return;
            case 'DELETE':
                $queue = array_filter(explode(',', api_requireParameter('short_id', $path)),
                    function($short_id) use ($is_admin, $username) {
                        return $is_admin || LinkShortenerService::get_creator($short_id) === $username;
                    });

                $this->set_json([
                    'success' => LinkShortenerService::delete_link($queue),
                ]);
                return;
        }
    }

}