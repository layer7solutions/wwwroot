<?php
namespace app\api\resource;

use app\lib\Subreddit;
use app\lib\util\AdminTools;

class BlacklistReports extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'tsbread';
    }

    public function execute(string $method, array $path, array $query): void {
        if (api_prefer('no_sr_guard') && user()->is_globalist()) {
            $this->subreddit = subreddit($this->subreddit);
        }

        $this->set_json($this->subreddit->mediaban->reports(
            api_requireParameter('type', $query),
            api_requireParameter('subject', $query),
            api_optionalParameter('offset', $query),
            api_optionalParameter('amount', $query)
        ));
    }

}