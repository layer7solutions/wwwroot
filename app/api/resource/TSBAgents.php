<?php
namespace app\api\resource;

use app\lib\Common;

class TSBAgents extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return null;
    }

    public function execute(string $method, array $path, array $query): void {
        $this->set_json(Common::tsb_agents(true));
    }

}