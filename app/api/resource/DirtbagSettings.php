<?php
namespace app\api\resource;

class DirtbagSettings extends APIResource {

    public function scope(string $method, array $path, array $query) {
        switch ($method) {
            case 'GET':
                return 'dirtbagprefread';
            case 'PUT':
            case 'DELETE':
                return 'dirtbagprefedit';
            default:
                return false;
        }
    }

    public function execute(string $method, array $path, array $query): void {
        switch ($method) {
            case 'GET':
                $this->set_json($this->subreddit->dirtbag->settings());
                return;
            case 'PUT':
                api_requireParameter('settings', $query);
                $this->set_json([
                    'success' => $this->subreddit->dirtbag->settings($query['settings'])
                ]);
                return;
            case 'DELETE':
                $this->subreddit->dirtbag->purgeSettings();
                $this->set_json([
                    'success' => true
                ]);
                return;
        }
    }

}