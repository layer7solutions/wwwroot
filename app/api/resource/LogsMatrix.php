<?php
namespace app\api\resource;

use app\lib\Common;
use app\lib\logs\Common as LogsCommon;
use app\lib\logs\Matrix;

class LogsMatrix extends APIResource {

    public function scope(string $method, array $path, array $query) {
        return 'logsread';
    }

    public function execute(string $method, array $path, array $query): void {
        $lowerbound = api_requireParameter('lowerbound', $query, 'valid_int');
        $upperbound = api_requireParameter('upperbound', $query, 'valid_int');
        $moderators = api_optionalParameter('moderators', $query) ?? 'all';
        $actions    = api_optionalParameter('actions', $query) ?? 'all';

        if ($lowerbound < 0 || $lowerbound > time()) {
            api_badParameter('lowerbound');
        } else if ($upperbound < 0) {
            api_badParameter('upperbound');
        } else if ($lowerbound >= $upperbound) {
            api_dispatchError('BAD_DATE_RANGE', 'lowerbound must be lower than upperbound');
        }

        $moderator_list = Common::parse_multi_str($moderators, function($categ) use ($path) {
            if ($categ === 'logged') {
                return $this->subreddit->all_moderators;
            } else {
                return $this->subreddit->moderators;
            }
        }, null, null, 'a-zA-Z0-9 _\\-', '\\+,');

        $action_list = Common::parse_multi_str($actions, function() {
            return LogsCommon::getActionTypes();
        }, null, null);

        $result = $this->subreddit->logs->matrix(
            $lowerbound,
            $upperbound,
            $moderator_list,
            $action_list
        );

        if (api_optionalParameter('component', $query, 'valid_bool') && !empty($result)) {
            $action_map = Matrix::getRowTemplate();

            foreach ($action_list as $action_type) {
                $action_map[strtolower($action_type)] = 1;
            }

            $component_sort = api_optionalParameter('sort', $query) ?? 'no-sort';
            $noemptycolumns = api_optionalParameter('noemptycolumns', $query, 'valid_bool');

            $this->set_html(render_to_string('logs.matrixcomponent', [
                'modlog_matrix'     => $result,
                'modlog_upperbound' => $upperbound,
                'modlog_lowerbound' => $lowerbound,
                'modlog_action_map' => $action_map,
                'modlog_moderators' => implode(',', $moderator_list),
                'modlog_moderators2'=> $moderators,
                'modlog_actions'    => implode(',', $action_list),
                'modlog_actions2'   => $actions,
                'modlog_noemptycol' => $noemptycolumns,
                'modlog_sort'       => $component_sort,
                'modlog_has_sort'   => isset($query['sort']),
            ]));
        } else {
            $this->set_json($result);
        }
    }

}