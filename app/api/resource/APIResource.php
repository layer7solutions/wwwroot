<?php
namespace app\api\resource;

use app\lib\Subreddit;
use app\lib\data\FieldSelectionParser;
use app\lib\data\InvalidQueryException;
use Closure;

/**
 * Abstract class for standard API resources.
 */
abstract class APIResource {
    const RESULT_TYPE_JSON = 0;
    const RESULT_TYPE_HTML = 1;

    /**
     * @var int `RESULT_TYPE_JSON` or `RESULT_TYPE_HTML`
     */
    protected $result_type = self::RESULT_TYPE_JSON;

    /**
     * @var array
     */
    protected $result_json = [];

    /**
     * @var string
     */
    protected $result_html = '';

    /**
     * @var Subreddit
     */
    protected $subreddit;

    /**
     * @var FieldSelectionParser|null
     */
    protected $field_parser;

    /**
     * @var array|true
     */
    protected $field_model = true;

    /**
     * @var bool
     */
    protected $field_trim_required = false;

    /**
     * This method is called for every instantiated resource. It is called before oauth
     * authentication and scope checking takes place.
     *
     * @param string                $method the HTTP method, always all uppercase
     * @param string                $path the path parameters (associative array)
     * @param string                $query the query parameters (associative array)
     */
    public function init(string $method, array $path, array $query): void {
        if (isset($path['subreddit'])) {
            $this->subreddit = $path['subreddit'];
        } else if (isset($query['subreddit'])) {
            $this->subreddit = $query['subreddit'];
        }

        if (isset($query['fields'])) {
            $this->field_parser = new FieldSelectionParser();
            try {
                $this->field_model = $this->field_parser->parse($query['fields']);
            } catch (InvalidQueryException $ex) {
                api_dispatchError('BAD_FIELDS', "The 'fields' parameter is invalid");
            }
        }
    }

    /**
     * Returns the scope for this API resource, should be deterministic depending on the function
     * parameters. May be different for different combinations of parameters.
     *
     * @param string                $method the HTTP method, always all uppercase
     * @param string                $path the path parameters (associative array)
     * @param string                $query the query parameters (associative array)
     * @return string|false|null    string containing required scope, false to throw not authorized,
     *                              or null for no scope required
     */
    public abstract function scope(string $method, array $path, array $query);

    /**
     * Execute the resource. The result can be retrieved using `get_result()` and the result type
     * via `get_type()`.
     *
     * @param string                $method the HTTP method, always all uppercase
     * @param string                $path the path parameters (associative array)
     * @param string                $query the query parameters (associative array)
     */
    public abstract function execute(string $method, array $path, array $query): void;

    /**
     * Get resource result type.
     *
     * @return int one of `self::RESULT_TYPE_*`
     */
    public function get_type(): int {
        return $this->result_type;
    }

    /**
     * Get resource result.
     *
     * @return mixed depending on the result type
     */
    public function get_result() {
        switch ($this->result_type) {
            case self::RESULT_TYPE_JSON:
                if ($this->field_trim_required && isset($this->field_parser)) {
                    return $this->field_parser->normalize($this->field_model, $this->result_json);
                }
                return $this->result_json;
            case self::RESULT_TYPE_HTML:
                return $this->result_html;
        }
        return null;
    }

    /**
     * Set a JSON response.
     *
     * Check API `fields` parameter for $field and store result of $value_provider into
     * `$this->result_json with $field` if $field exists in `fields` parameter or `fields` parameter
     * does not exist.
     *
     * @param string        $field
     * @param Closure       $value_provider
     * @param Closure       $else
     * @return bool         whether or not the value was added or not
     */
    public function field(string $field, Closure $value_provider): bool {
        $this->result_type = self::RESULT_TYPE_JSON;

        if ($this->field_model === true) {
            $do_set = true;
        } else {
            $do_set = $this->field_parser->check_field($this->field_model, $field, true);
        }

        if ($do_set) {
            $value_provider->bindTo($this, $this);
            to($this->result_json, $field, call_user_func($value_provider));
            if ($do_set === 2) {
                $this->field_trim_required = true;
            }
        }

        return $do_set;
    }

    /**
     * Set a JSON response directly. Using `field()` is preferred if possible.
     *
     * @param array $json the JSON array
     */
    public function set_json(array $json): void {
        $this->result_type = self::RESULT_TYPE_JSON;
        $this->result_json = $json;
        $this->field_trim_required = true;
    }

    /**
     * Set an HTML response.
     *
     * @param string $html the HTML string
     */
    public function set_html(string $html): void {
        $this->result_type = self::RESULT_TYPE_HTML;
        $this->result_html = $html;
    }

}