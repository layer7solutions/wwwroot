<?php

/**
 * Application API service.
 *
 * Use api_start() to handle an API request;
 *   expects REQUEST_URI to be in the format, '/api/{VERSION/{URI}'
 * Use api_directCall() to directly invoke an API resource class.
 *
 * Functions starting with '__' (double underscore) are internal to this
 * file and are not intended to be used outside.
 */

use app\lib\User;
use app\api\resource\APIResource;
use app\api\AuthenticationHandle;

// ================================================================================
// API STANDARD ACCESS FUNCTIONS
// ================================================================================

/**
 * Execute and dispatch a request to the API.
 */
function api_start(): void {
    // URL Format: /api/{VERSION}/{URI}
    // 5 -> length of '/api/'
    // 2 -> only want to split on '/' once (2 parts)
    $url_parts = explode('/', substr(App::request_uri(), 5), 2);
    $version   = array_shift($url_parts);
    $uri       = '/' . trim(array_shift($url_parts), '/');

    if (API_ENABLED) {
        session_init();
    } else {
        die('API not available.');
    }

    ini_set('xdebug.overload_var_dump', 'off');

    set_exception_handler(function($exception) {
        exceptionHandler($exception);
        api_internalError('A server error occurred.');
    });

    if (empty($version)) {
        api_dispatchError('NO_VERSION_SPECIFIED');
    } else if (strlen($uri) <= 1) { // 1 -> includes length of '/' prefix
        api_dispatchError('NO_RESOURCE_SPECIFIED');
    }

    switch ($uri) {
        case '/authorize':
            $server = require(APP_ROOT . 'api/oauth/server.php');
            require(APP_ROOT . 'api/oauth/authorize.php');
            die;
        case '/access_token':
            $server = require(APP_ROOT . 'api/oauth/server.php');
            require(APP_ROOT . 'api/oauth/token.php');
            die;
    }

    __api_executeResource($version, $uri);
}

/**
 * Directly call an API resource.
 *
 * @param string        $resource_name the class name of the resource located in '\app\api\resource'
 * @param string        $method the HTTP method
 * @param array         $path parameter map on the API request URI path
 * @param array         $query parameter map on the API request URI query string
 * @param string        $test_feature (optional) test feature
 * @return mixed        API resource response
 */
function api_directCall(string $resource_name, ?string $method, ?array $path, ?array $query) {
    $call_args = [strtoupper($method ?? 'GET'), $path ?? [], $query ?? []];

    $resource_class = "\\app\\api\\resource\\{$resource_name}";
    $resource = new $resource_class();

    $resource->init(... $call_args);
    $resource->execute(... $call_args);

    return $resource->get_result();
}

// ================================================================================
// API SCOPE DECLARATION FUNCTIONS
// ================================================================================

/**
 * Get or set API OAuth scopes.
 *
 * @param array         $new_scopes (optional) associative array, scope_name => scope_desc
 * @return array        Current API scopes if $new_scopes not provided, old scopes otherwise
 */
function api_scopes(array $new_scopes = null): array {
    static $_scopes = [];

    if (isset($new_scopes)) {
        $old_scopes = $_scopes;
        $_scopes = $new_scopes;
        return $old_scopes;
    } else {
        return $_scopes;
    }
}

/**
 * Get the description for an API scope.
 *
 * @param string        $scope the scope name
 * @return string       the scope description for the given scope name, or null if not found.
 */
function api_scope_desc(string $scope): ?string {
    return from(api_scopes(), $scope);
}

/**
 * Get or set the default API OAuth scope.
 *
 * @param string $new_scope the new default scope
 * @return string default scope string if get operation, the passed in parameter if set operation
 */
function api_default_scope(string $new_scope = null): string {
    static $_defaultScope = null;
    if (isset($new_scope)) {
        return $_defaultScope = $new_scope;
    } else {
        return $_defaultScope;
    }
}

// ================================================================================
// API REQUEST PARAMETER UTILITIES
// ================================================================================

/**
 * Check API request preferences.
 *
 * i.e. `api_prefer('featurename;)` will return true for `/?prefer=featurename`;
 * multiple features can be separated by `|`.
 *
 * @param string $preference_name
 * @return bool
 */
function api_prefer(string $preference_name): bool {
    return test_feature_enabled($preference_name, 'prefer');
}

/**
 * Validate a parameter with the given validator function.
 * If the validator returns false, api_badParameter() will be called which dispatches the
 * appropriate 400 error code and stops the script execution.
 *
 * @param string        $name parameter name
 * @param mixed         $value parameter value
 * @param callable      $validator the validator function
 * @return mixed        new parameter value (may or may not be changed)
 */
function __api_validateParameter(string $name, $value, callable $validator = null) {
    if (!isset($validator)) {
        return $value;
    }

    if ($validator($value) === false) {
        api_badParameter($name);
    }

    if (is_string($validator)) {
        switch ($validator) {
            case 'valid_int':
                $value = intval($value);
                break;
            case 'valid_bool':
            case 'valid_boolean':
                $value = bool_unstr($value);
                break;
        }
    }

    return $value;
}

/**
 * Get an optional parameter from the given source.
 *
 * @param string        $name parameter name
 * @param array         $source the source
 * @param callable      $validator (optional) validates the value, must return true
 * @return mixed        the parameter value, or `null` if parameter does not exist
 */
function api_optionalParameter(string $name, array $source, callable $validator = null) {
    return empty($source[$name]) ? null : __api_validateParameter($name, $source[$name], $validator);
}

/**
 * Get a required parameter from the given source. If the parameter doesn't exist,
 * the BAD_PARAMETER error response will be dispatched.
 *
 * @param string        $name parameter name
 * @param array         $source the source
 * @param callable      $validator (optional) validates the value, must return true
 * @return mixed        the parameter value, or `null` if parameter does not exist
 */
function api_requireParameter(string $name, array $source, callable $validator = null, bool $use_isset = false) {
    if ($use_isset ? !isset($source[$name]) : empty($source[$name])) {
        api_badParameter($name);
    }
    return __api_validateParameter($name, $source[$name], $validator);
}

// ================================================================================
// API ERROR RESPONSE DISPATCH FUNCTIONS
// ================================================================================

/**
 * Dispatches an UNKNOWN_FIELD api error response.
 *
 * @param string $fieldtype the parameter that received an unknown field.
 */
function api_unknownField(string $fieldtype = 'action'): void {
    api_dispatchError('UNKNOWN_FIELD', 'Unknown ' . $fieldtype, $fieldtype, 404);
}

/**
 * Dispatches a NOT_AUTHORIZED api error response.
 *
 * @param string $errmsg='Not authorized' (optional) custom error message
 */
function api_notAuthorized(string $errmsg = 'Not authorized', $detail=null): void {
    api_dispatchError('NOT_AUTHORIZED', $errmsg, $detail, 403);
}

/**
 * Dispatches an INTERNAL_ERROR api error response.
 *
 * @param string $errmsg='An internal error occurred' (optional) custom error message
 */
function api_internalError(string $errmsg = 'An internal error occurred'): void {
    api_dispatchError('INTERNAL_ERROR', $errmsg, null, 500);
}

/**
 * Dispatches a BAD_PARAMETER api error response.
 *
 * @param string|string[]   $parameters the parameter(s) that are missing or received a bad value
 * @param bool              $only_one if true, signifies one but not all of the specified parameters
 *                          are required
 */
function api_badParameter($parameters, bool $only_one = false): void {
    $msg = $only_one
        ? 'One of the following parameters is required: '
        : 'The following parameter(s) are invalid or missing: ';

    api_dispatchError('BAD_PARAMETER',
        $msg . implode(', ', (array) $parameters),
        (array) $parameters, 400);
}

/**
 * Dispatches a COOLDOWN api error response.
 */
function api_cooldown(int $time_left): void {
    api_dispatchError('COOLDOWN', 'Wait ' . human_timing(time() + $time_left, '') .
        ' before doing this again.', $time_left, 400);
}

function api_checkCooldown(?int $property, int $cooldown_time) {
    if ($property !== null && ($property + $cooldown_time) >= time()) {
        api_cooldown(($property + $cooldown_time) - time());
    }
}

// ================================================================================
// API NORMAL RESPONSE DISPATCH FUNCTIONS
// ================================================================================

/**
 * Default HTTP headers sent with every API response.
 * @return string[] list of headers
 */
function __api_getAdditionalHeaders(): array {
    return [
        "Access-Control-Allow-Origin: *",
        "Access-Control-Allow-Headers: Origin, Content-Type, Accept, User-Agent, X-CSRF-Token, X-Requested-With, Authorization",
        "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, HEAD, OPTIONS",
        "Access-Control-Allow-Credentials: true",
    ];
}

/**
 * Dispatches the default response to an OPTIONS request.
 *
 * @param string[] $allowed_methods the HTTP methods that are allowed for this request.
 */
function __api_respondOPTIONS(array $allowed_methods): void {
    http_response_code(204);
    header('Content-Length: 0', false);
    header('Allow: OPTIONS, ' . implode(', ', $allowed_methods), false);

    foreach (__api_getAdditionalHeaders() as $header) {
        header($header, false);
    }
    die;
}

/**
 * Dispatch an API error response.
 *
 * @param string        $errcode the error code
 * @param string        $errmsg (optional) the error message
 * @param mixed         $detail (optional) purpose of this parameter depends on the errcode
 * @param int           $response_code=400 (optional) the HTTP response code
 */
function api_dispatchError(string $errcode, string $errmsg = null, $detail = null, int $response_code = 400) {
    __api_sendResponse([
        'error' => $errcode,
        'error_description'  => $errmsg,
        'error_detail' => $detail,
    ], $response_code);
}

/**
 * Send a standard API response.
 *
 * @param APIResource|array|string  $result the result returned by the API resource
 * @param int                       $response_code=200 (optional) the HTTP response code
 */
function __api_sendResponse($result, int $response_code = 200): void {
    if (defined('BOOTSTRAP_END_TIME')) {
        define('API_TIME_TAKEN', (microtime(true) * 1000) - BOOTSTRAP_END_TIME);
    } else {
        define('API_TIME_TAKEN', 0);
    }

    $headers = __api_getAdditionalHeaders();

    if (api_version() === 'ajax' && session_csrf_token_validity(true)) {
        $headers[] = 'X-CSRF-Token: ' . session_csrf_token();
    }

    if ($result instanceof APIResource) {
        $result = $result->get_result();
    }

    if (is_string($result)) {
        dispatch_html($result, $response_code, $headers);
    } else if (is_array($result)) {
        if (test_feature_enabled('debug')) {
            $result['_runtime_millis'] = [
                'startup'   => BOOTSTRAP_TIME_TAKEN,
                'function'  => API_TIME_TAKEN,
                'total'     => API_TIME_TAKEN + BOOTSTRAP_TIME_TAKEN,
            ];
        }
        dispatch_json($result, $response_code, $headers, test_feature_enabled('prettify'));
    } else {
        die;
    }
}

// ================================================================================
// API RESOURCE ROUTING AND EXECUTION WITH AUTHORIZATION VERIFICATION
// ================================================================================

/**
 * Get API version of the currently executing resource. (Only for non-directcalls).
 * @return string
 */
function api_version(string $_newVersion = null): ?string {
    static $_version = null;
    return isset($_newVersion) ? ($_version = $_newVersion) : $_version;
}

/**
 * Finds, executes, and dispatches an API resource.
 *
 * @param string $version API version
 * @param string $uri request URI relative to '/api/{version}'
 *
 * Dispatches these errors:
 *   - NO_SUCH_VERSION :    if unknown API version
 *   - NO_SUCH_VERSION :    if unknown API version
 *   - NO_SUCH_RESOURCE :   if resource not found
 *   - METHOD_NOT_ALLOWED : if the resource does not allow the HTTP request method
 *   - INTERNAL_ERROR :     if a resource was declared in the API routes but the Class doesn't exist
 *   - SR_FORBIDDEN :       if the request operates on subreddit(s) the requesting user does not mod
 *   - SR_INVALID :         if a subreddit string is invalid
 *   - SR_UNKNOWN :         if a subreddit string has an unknown subreddit
 *   - Any other errors depending on the specific API response.
 */
function __api_executeResource(string $version, string $uri): void {
    switch (api_version($version)) {
        case 'ajax':
        case 'v1':
            $api_routes_file = 'routes/api_v1.php';
            break;
        default:
            $available_versions = ['ajax', 'v1'];
            api_dispatchError('NO_SUCH_VERSION', "The version '$version' does not exist",
                $available_versions);
            exit;
    }

    $routeInfo = App::dispatch($api_routes_file, $uri, true);

    if ($routeInfo instanceof \RouteFail) {
        switch ($routeInfo->status) {
            case 404: // (NOT FOUND)
                api_dispatchError('NO_SUCH_RESOURCE', 'Resource does not exist', $uri, 404);
                exit;
            case 405: // (METHOD NOT ALLOWED)
                if (App::request_method() === 'OPTIONS') {
                    __api_respondOPTIONS($routeInfo->detail); // respond to CORS preflight
                }
                // For status=405, 'detail' is a list of valid HTTP request methods for the request
                api_dispatchError('METHOD_NOT_ALLOWED', implode(',', $routeInfo->detail),
                    $routeInfo->detail, 405);
                exit;
            default: // (MISC ERROR)
                api_dispatchError($routeInfo->status, $routeInfo->detail, $routeInfo->detail,
                    $routeInfo->status);
                exit;
        }
    } else if (is_string($routeInfo)) {
        $call_args = [App::request_method(), App::router()->params(), $_REQUEST];
        $resource = __api_verifyResource($routeInfo, $call_args);
        $resource->execute(... $call_args);
        __api_sendResponse($resource);
    } else {
        api_internalError('unexpected routing callback');
    }
}

function __api_verifyResource(string $resource_name, array $call_args) {
    $resource_class = "\\app\\api\\resource\\{$resource_name}";
    $resource = new $resource_class();

    if (api_version() === 'ajax') {
        if (SITE_IS_STAGING && from($_SERVER, 'HTTP_X_REQUESTED_WITH') !== 'XMLHttpRequest') {
            $auth_type = new AuthenticationHandle(AuthenticationHandle::WEB);
        } else {
            $auth_type = new AuthenticationHandle(AuthenticationHandle::AJAX);
        }
    } else {
        $auth_type = new AuthenticationHandle(AuthenticationHandle::OAUTH, [
            'server' => require(APP_ROOT . 'api/oauth/server.php'),
        ]);
    }

    $resource->init(... $call_args);

    if ($auth_type->verify($resource->scope(... $call_args))) {
        User::set_current_user($auth_type->get_verified_user());
        return $resource;
    }

    api_notAuthorized("Authentication failed", [
        "meta" => [
            "api_version" => api_version(),
            "auth_type" => $auth_type->get_type(),
            'auth_user' => ((string) $auth_type->get_verified_user()),
            'X-Requested-With' => from($_SERVER, 'HTTP_X_REQUESTED_WITH'),
            'token_validity' => session_csrf_token_validity(true),
        ],
        "invocation" => [
            "resource_name" => $resource_name,
            "call_args" => $call_args,
        ],
        "moreinfo" => session_csrf_token_rejected_reason(),
    ]);
}