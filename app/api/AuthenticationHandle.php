<?php
namespace app\api;

use app\lib\User;
use OAuth2\Server;

/**
 * Represents the method of authentication used for accessing the API.
 */
final class AuthenticationHandle {
    // types
    const AJAX = 'ajax';
    const OAUTH = 'oauth';
    const WEB = 'web';

    // internal variables
    private $type = null;
    private $options = null;
    private $user = null;

    /**
     * Construct a new instance.
     *
     * @param string    $type one of these: AuthenticationHandle::{AJAX,OAUTH,WEB}
     * @param array     $options (optional) options array
     */
    public function __construct(string $type, array $options=[]) {
        $this->type = $type;
        $this->options = $options;
    }

    /**
     * Retrieve the User that sent the request. Will not be available until
     * verify() is called.
     *
     * @return User|null    the User instance, null if User could not be obtained
     *                      or if the authentication failed
     */
    public function get_verified_user(): ?User {
        return $this->user;
    }

    /**
     * Get the authentication type.
     *
     * @return string
     */
    public function get_type(): string {
        return $this->type;
    }

    /**
     * Verify the authentication request.
     *
     * @param string    $scope (optional) the required OAuth scope for the resource
     * @return bool     true if the authentication is succesful, false otherwise
     */
    public function verify(string $scope = null): bool {
        switch ($this->type) {
            case self::AJAX:
                if (session_csrf_token_validity(true)) {
                    $this->user = User::user(session_csrf_token_data('user_for'));
                }
                break;
            case self::OAUTH:
                if ($scope === false) {
                    return false;
                }

                $server   = $this->options['server'];
                $request  = \OAuth2\Request::createFromGlobals();
                $response = new \OAuth2\Response();

                if ($server->verifyResourceRequest($request, $response, $scope)) {
                    $token = $server->getAccessTokenData(\OAuth2\Request::createFromGlobals());
                    $this->user = User::user($token['user_id']);
                }
                break;
            case self::WEB:
                if (User::current_user()->logged_in()) {
                    $this->user = User::current_user();
                }
                break;
            default:
                $this->user = null;
                break;
        }

        if (!isset($this->user)) {
            return false;
        }

        // If user is blacklisted from the API, don't allow them to use any types other than AJAX
        if (isset($this->user) && $this->user->session->isBlacklisted() && $this->type != self::AJAX) {
            $this->user = null;
            return false;
        }

        return true;
    }

}