<?php
if (!b_stash('has_tsb') || test_feature_enabled('ntsb')):
    if (count(user()->modded) && !test_feature_enabled('nmod')):
        render_template('overview.welcome-card', ['is_mod' => true]);
    else:
        render_template('overview.welcome-card', ['is_mod' => false]);
    endif;
else: ?>
<div id="dashboard">
    <div class="card">
        <div data-dashboard-module="welcome_text" class="header DashboardWelcome">
            <div>
                <span class="DashboardWelcome__greeting">Welcome back</span>
                <span class="DashboardWelcome__commaSep">, </span>
                <span class="DashboardWelcome__username"><?php xecho(user()->get_username()) ?></span>
            </div>
        </div>
        <div data-dashboard-module="quick_links" class="QuickLinks row">
            <h3 class="QuickLinks__title">Quick Links</h3>
            <!--
            7a84e6 purple
            f19352 orange
            24b47e green
            3daaf5 blue
            -->
            <div class="QuickLinks__inner">
                <?php if (b_stash('has_tsb')): ?>
                <div class="QuickLinksColumn">
                    <span class="QuickLinksColumn__title" style="color:#7a84e6">Media Blacklist</span>
                    <a href="<?php echo SITE_URL ?>blacklist/edit" class="QuickLinksItem">
                        <span>Edit blacklist</span>
                    </a>
                    <a href="<?php echo SITE_URL ?>blacklist/view" class="QuickLinksItem">
                        <span>View blacklist</span>
                    </a>
                </div>
                <?php endif; ?>
                <?php if (b_stash('has_logs')): ?>
                <div class="QuickLinksColumn">
                    <span class="QuickLinksColumn__title" style="color:#3daaf5">Action Logs</span>
                    <a href="<?php echo SITE_URL ?>logs/view" class="QuickLinksItem">
                        <span>View mod logs</span>
                    </a>
                    <a href="<?php echo SITE_URL ?>logs/matrix" class="QuickLinksItem">
                        <span>Build mod matrix</span>
                    </a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php if (!empty(a_stash('tsb_recent'))): ?>
    <div data-dashboard-module="tsb_recent" class="card no-pad">
        <div class="header">
            <span>Recently Blacklisted</span>
            <a href="<?php echo SITE_URL ?>blacklist/view">
                <button class="tertiary">See more</button>
            </a>
        </div>
        <div class="listing listing--TSBRecent">
            <?php foreach (xa_stash('tsb_recent') as $item): ?>
                <div class="item" data-reqid="<?php xecho($item['requestid']) ?>">
                    <span class="TSBRecent__subject">
                        <span class="TSBRecent__subjectAuthor"><?php xecho($item['media_author']) ?></span>
                        <input type="text" class="TSBRecent__subjectID code" value="<?php xecho($item['media_channel_id']) ?>" />
                    </span>
                    <div class="TSBRecent__line">
                        <span class="TSBRecent__sr"><?php xecho(
                            strtolower($item['subreddit']) == 'thesentinelbot' ?
                                'Global Blacklist' : $item['subreddit']
                        ) ?></span>
                        <span class="TSBRecent__mod"><?php xecho($item['moderator']) ?></span>
                        <span class="TSBRecent__ts"><?php echo timeConvert($item['timestamp']) ?></span>
                        <a href="<?php xecho($item['media_channel_url']) ?>"
                            target="_blank" rel="noopener noreferrer"
                            data-media-icon-platform="<?php xecho($item['media_platform']) ?>" class="media-link"></a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php endif; ?>
</div>
<?php endif; ?>