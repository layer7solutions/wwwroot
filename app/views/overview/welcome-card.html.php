<div data-dashboard-module="welcome_card" class="WelcomeCard">
    <div class="WelcomePanel active" data-name="start">
        <div class="WelcomeCardIntro">
            <h3>Welcome to Layer7</h3>
            <p>We provide services to Reddit moderators via TheSentinelBot (TSB).</p>
            <?php if (b_stash('is_mod')): ?>
                <p>It looks like you don't mod any subreddits using TheSentinelBot,
                check out <a href="<?php echo SITE_URL ?>docs/get-started/">this page</a>
                on how you can get your subreddit set up with TSB.</p>

                <div class="WelcomeButtons">
                    <a href="<?php echo SITE_URL ?>docs/get-started/">
                        <button class="primary">Get started with TSB</button>
                    </a>
                </div>
            <?php else: ?>
                <p>It looks like you don't mod any subreddits, so this website
                won't be very useful to you.</p>
                <p>But hey, thanks for stopping by :)</p>
            <?php endif; ?>
        </div>
        <div class="WelcomeItemGroup">
            <section class="WelcomeItem">
                <div class="WelcomeItem__img">
                    <img src="<?php img_src('landing/intro/fight-spam.svg') ?>" />
                </div>
                <div class="WelcomeItem__desc">
                    <h3>Fight Spam</h3>
                    <p>Auto-remove posts &amp; comments with links to spam channels</p>
                </div>
            </section>
            <section class="WelcomeItem">
                <div class="WelcomeItem__img">
                    <img src="<?php img_src('landing/intro/explore-data.svg') ?>" />
                </div>
                <div class="WelcomeItem__desc">
                    <h3>Control Your Data</h3>
                    <p>Search your mod logs, create instant mod matrices, and more.</p>
                </div>
            </section>
            <section class="WelcomeItem">
                <div class="WelcomeItem__img">
                    <img src="<?php img_src('landing/intro/and-more.svg') ?>" />
                </div>
                <div class="WelcomeItem__desc">
                    <h3>And more!</h3>
                    <p>Choose from the many tools we offer to help manage your subreddit.</p>
                </div>
            </section>
        </div>
    </div>
    <div class="WelcomePanel" data-name="0">
        <div class="WelcomePanel__left">
            <h3>Fight Spam</h3>
            <div class="WelcomeButtons">
                <button data-target="start" class="primary">Previous</button>
                <button data-target="1" class="primary">Next</button>
            </div>
        </div>
    </div>
    <div class="WelcomePanel" data-name="1">
        <div class="WelcomePanel__left">
            <h3>Control Your Data</h3>
            <div class="WelcomeButtons">
                <button data-target="0" class="primary">Previous</button>
                <button data-target="2" class="primary">Next</button>
            </div>
        </div>
    </div>
    <div class="WelcomePanel" data-name="2">
        <div class="WelcomePanel__left">
            <h3>And more!</h3>
            <div class="WelcomeButtons">
                <button data-target="1" class="primary">Previous</button>
                <button data-target="start" class="primary">Done</button>
            </div>
        </div>
    </div>
</div>