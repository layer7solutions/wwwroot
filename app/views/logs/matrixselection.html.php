<div class="talign">
    <?php if (!empty(a_stash('all_available_subreddits'))): ?>
    <div class="app-aside-outer" style="top:105px">
        <div class="app-aside-float">
            <aside class="app-aside">
                <button id="ModlogButton__SelectNext" class="primary" disabled>Next</button>
                <hr/>
                <p>Select at least 1 subreddit to create a mod action matrix for, then click next.</p>
            </aside>
        </div>
    </div>
    <?php endif; ?>
    <div>
        <div id="modlog-module-header" class="valign spacer-bottom">
            <?php if (b_stash('is_invalid_selection')): ?>
                <a href="<?php echo SITE_URL ?>logs/matrix" title="Back to subreddit selection">
                    <button class="tertiary">
                        <i class="icon zmdi zmdi-chevron-left"></i>
                        <span>Back</span>
                    </button>
                </a>
                <h4 class="spacer-left">Mod Matrix for <?php xecho((string) u_stash('subreddit')) ?></h4>
            <?php else: ?>
                <h4>Mod Matrix &mdash; Select Subreddit(s)</h4>
            <?php endif; ?>
        </div>
        <?php if (b_stash('is_invalid_selection')): ?>
            <div class="error-notice spacer10-bottom">
                <strong>Not available:</strong> the subreddit(s) you've selected either: do not
                exist, do not have TSB, do not have Mod Action Logging enabled in their
                <a href="<?php echo SITE_URL ?>prefs/subreddit">TSB settings</a>; or you are not a
                mod of all of them.
            </div>
            <h4 class="spacer20-top spacer5-bottom">Try choosing another selection:</h4>
        <?php endif; ?>
        <?php if (empty(a_stash('all_available_subreddits'))): ?>
            <div class="output-status">No subreddits available</div>
        <?php else: ?>
            <div id="ModlogField__subreddits" class="Chooser wSplit4 Chooser--MatrixForm">
                <div class="ChooserTop">
                    <label class="ChooserOption ChooserOption--all" style="width:auto">
                        <input type="checkbox" />
                        <span>All Modded Subreddits</span>
                    </label>
                </div>
                <?php foreach (xa_stash('all_available_subreddits') as $sr): ?>
                    <label class="ChooserOption">
                        <input type="checkbox" data-value="<?php echo $sr ?>" />
                        <span><?php echo $sr ?></span>
                    </label>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>