<div class="LogsView_Wrap">
    <div id="LogsView__Header">
        <h2>Search your logs</h2>
        <div id="LogsView__Header__PageOpts">
            <span>Items per page:</span>
            <div id="LogsView__PageAmountChooser" class="option-group">
                <span data-value="1" class="option-group-item">1</span>
                <span data-value="5" class="option-group-item">5</span>
                <span data-value="10" class="option-group-item">10</span>
                <span data-value="25" class="option-group-item selected">25</span>
                <span data-value="50" class="option-group-item">50</span>
                <span data-value="100" class="option-group-item">100</span>
                <span data-value="200" class="option-group-item">200</span>
            </div>
            <i id="LogView__Settings" class="icon zmdi zmdi-settings"></i>
        </div>
    </div>
    <div id="LogsView__Search"></div>
    <div id="LogsView__Result" class="card hide"></div>
</div>