<?php /** @noinspection ALL */

// Implementation of the function:
// f_a(x) = {-1<x<0: -((-x)^a)/((-x)^a + (1+x)^a), 0<x<1: (x^a)/(x^a+(1-x)^a) }
//
// \ f_a\left(x\right)=\left\{
//    -1<x<0:\ \frac{-\left(-x\right)^a}{\left(-x\right)^a+\left(1+x\right)^a},
//    0<x<1:\frac{x^a}{x^a+\left(1-x\right)^a}\right\}
function matrix_intensity_ease($x, $a=2) {
    if ($x >= 0) {
        $pXA = pow($x,$a);
        return $pXA/($pXA + pow(1-$x,$a));
    } else if ($x < 0) {
        $npXA = pow(-$x,$a);
        return -($npXA/($npXA + pow(1+$x,$a)));
    }
}

function matrix_intensity($count, $max) {
    if ($count == 0) {
        return 'color:rgba(0,0,0,0.2)';
    }

    $opacity = $count / $max;
    if ($opacity > 1)
        $opacity = 1;
    $opacity = matrix_intensity_ease($opacity, 0.5);

    return 'background:hsla(215, 60%, 50%, '.$opacity.');' .
        'color: hsla(215, 60%, 20%, 1)';
}
?>

<?php extract(u_stash('modlog_matrix'), EXTR_OVERWRITE);

$mod_ordering = [];
$mod_ordering_tmp = $matrix_rows;
$mod_ordering_i = 0;
$mod_ordering_count = count($matrix_rows);
asort($mod_ordering_tmp);
foreach ($mod_ordering_tmp as $mod => $sum) {
    $mod_ordering[$mod] = ['asc' => $mod_ordering_i, 'desc' => ($mod_ordering_count-$mod_ordering_i)];
    $mod_ordering_i++;
}
?>
<div id="modlog-matrix" class="matrix<?php

if (b_stash('modlog_noemptycol')) echo " no-empty-columns";
if (!b_stash('modlog_has_sort')) echo " no-direct-sort";

?>"
        data-subreddits="<?php echo implode(',', $matrix_subreddits); ?>"
        data-current-sort="<?php echo x_stash('modlog_sort') ?>"
        data-lowerbound="<?php echo x_stash('modlog_lowerbound') ?>"
        data-lowerbound2="<?php xecho(date("Y/m/d", i_stash('modlog_lowerbound'))) ?>"
        data-upperbound="<?php echo x_stash('modlog_upperbound') ?>"
        data-upperbound2="<?php xecho(date("Y/m/d", i_stash('modlog_upperbound'))) ?>"
        data-moderators="<?php echo x_stash('modlog_moderators') ?>"
        data-moderators2="<?php echo x_stash('modlog_moderators2') ?>"
        data-actions="<?php echo x_stash('modlog_actions') ?>"
        data-actions2="<?php echo x_stash('modlog_actions2') ?>">
    <div class="matrix-main">
        <div class="matrix-pending pending-overlay hide">
            <?php if (count($matrix_rows) >= 4): ?>
            <div class="loading loading--double"></div>
            <?php endif; ?>
        </div>
        <div class="matrix-head">
            <div class="matrix-item headcol matrix-header-col matrix-moderator"
                title="moderator">moderator</div>
            <?php foreach ($matrix as $mod => $data): ?>
                <div class="matrix-item headcol matrix-moderator" data-mod="<?php xecho($mod) ?>"
                    style="cursor:pointer;"
                    data-desc="<?php xecho($mod_ordering[$mod]['desc']) ?>"
                    data-asc="<?php xecho($mod_ordering[$mod]['asc']) ?>"><?php xecho($mod) ?></div>
            <?php endforeach; ?>
            <div class="matrix-item headcol matrix-total-label matrix-moderator"><b>Total</b></div>
        </div>
        <div class="matrix-table">
            <div class="matrix-datawrap">
                <div class="matrix-header clearfix">
                    <?php foreach($matrix_columns as $action_type => $column_count):
                        if (u_stash('modlog_action_map')[$action_type] === 0)
                            continue;
                        $real_name = \app\lib\logs\Logs::get_action_display_name($action_type);
                        ?><div data-column-count="<?php xecho($column_count) ?>"
                            class="matrix-item matrix-header-col posRel modactions modactions-<?php xecho($action_type) ?>">
                            <div class="full-cover matrix-header-item-label"
                                data-tippy-content="<?php xecho($real_name)?>"
                                data-name="<?php xecho($real_name)?>"></div>
                        </div>
                    <?php endforeach; ?>
                    <div title="Total" class="matrix-item matrix-header-col matrix-item--total">Total</div>
                    <div title="Percent" class="matrix-item matrix-header-col matrix-item--percent">Percent</div>
                </div>
                <div class="matrix-data">
                    <?php
                    if (!empty($matrix_rows)):

                    $row_sum_max = max(array_values($matrix_rows));
                    foreach ($matrix as $mod => $data): ?>
                        <div class="matrix-row clearfix" data-mod="<?php xecho($mod) ?>"
                                data-desc="<?php xecho($mod_ordering[$mod]['desc']) ?>"
                                data-asc="<?php xecho($mod_ordering[$mod]['asc']) ?>">
                            <?php foreach ($data as $action => $count):
                                if (u_stash('modlog_action_map')[$action] === 0) continue;
                                ?><div data-column-name="<?php xecho($action) ?>" data-column-count="<?php xecho($matrix_columns[$action]) ?>" class="matrix-item" style="<?php echo matrix_intensity($count, $matrix_trimmed_maxval) ?>"><?php xecho($count) ?></div><?php
                            endforeach;
                            $row_sum = $matrix_rows[$mod];
                            ?><div class="matrix-item matrix-item--total" style="<?php echo matrix_intensity($row_sum, $row_sum_max) ?>"><?php xecho($row_sum) ?></div><?php
                            $row_percent = $matrix_sum == 0 ? 0 : number_format((float) ($row_sum / $matrix_sum * 100.0), 1, '.', '');
                            ?><div class="matrix-item matrix-item--percent" style="<?php echo matrix_intensity($row_percent, 100) ?>"><?php xecho($row_percent) ?>%</div>
                        </div>
                    <?php endforeach;

                    endif; ?>
                    <div class="matrix-row matrix-total-row clearfix" data-mod="total"
                            data-desc="<?php xecho(count($matrix)+1) ?>"
                            data-asc="<?php xecho(count($matrix)+1) ?>">
                        <?php foreach($matrix_columns as $action_type => $column_count):
                            if (u_stash('modlog_action_map')[$action_type] === 0) continue;
                            ?><div data-column-count="<?php xecho($column_count) ?>" title="<?php xecho($action_type) ?>" class="matrix-item"><?php xecho($column_count) ?></div><?php
                        endforeach;
                        ?><div class="matrix-item matrix-item--total"><?php xecho($matrix_sum) ?></div>
                    </div>
                </div>
            </div>
            <div class="matrix-xscroll">
                <div class="matrix-xscroll__float">
                    <div class="matrix-xscroll__outerTrack">
                        <div class="matrix-xscroll__track">
                            <div class="matrix-xscroll__thumb">
                                <div class="matrix-xscroll__thumbInner">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="matrix-runtime-info">
        Query completed in <span class="matrix-querytime"><?php xecho($query_runtime); ?></span> ms.
        Total matrix built time: <span class="matrix-runtime"><?php xecho($matrix_runtime); ?></span> ms.
        Time range from <span class="matrix-lowerbound"><?php echo x_stash('modlog_lowerbound') ?></span>
        to <span class="matrix-upperbound"><?php echo x_stash('modlog_upperbound') ?></span>.
    </div>
</div>