<div id="modlog-module-header" class="valign spacer-bottom<?php
        if (b_stash('full_screen')): ?> hide<?php endif; ?>">
    <a href="<?php echo SITE_URL ?>logs/matrix" title="Back to subreddit selection">
        <button class="tertiary">
            <i class="icon zmdi zmdi-chevron-left"></i>
            <span>Back</span>
        </button>
    </a>
    <h4 class="spacer-left">Mod Matrix for <?php xecho((string) u_stash('subreddit')) ?></h4>
    <div class="grow"></div>
    <i id="modlog-form-settings" class="icon icon28 zmdi zmdi-settings"></i>
</div>
<div id="modlog-module">
    <div id="modlog-form-output-wrapper" class="card matrix-wrapper <?php if (!is_stashed('direct_matrix')): ?>hide<?php endif; ?>">
        <div class="header">
            <span>Mod log matrix</span>
        </div>
        <div class="valign content" style="padding-top:0">
            <button class="tertiary matrix-toggleEmptyColumns spacer5-right">
                <i style="min-width:12px"
                    class="icon fa fa-square-o"
                    data-fstate="icon fa fa-square-o"
                    data-tstate="icon fa fa-check-square"></i>
                <span>Hide empty columns</span>
            </button>
            <button class="tertiary matrix-toggleFullScreen spacer5-right">
                <i style="min-width:12px"
                    class="icon fa fa-square-o"
                    data-fstate="icon fa fa-square-o"
                    data-tstate="icon fa fa-check-square"></i>
                <span>Enter full screen</span>
            </button>
            <button class="tertiary matrix-toggleSort spacer5-right">
                <i class="icon fa fa-sort"></i>
                <span>No Sorting</span>
            </button>
            <button class="tertiary matrix-exportToCSV spacer5-right">
                <i class="icon fa fa-table"></i>
                <span>Export to CSV</span>
            </button>
            <button class="tertiary matrix-getDirectLink spacer5-right">
                <i class="icon fa fa-link"></i>
                <span>Get direct link</span>
            </button>
        </div>
        <div class="posRel">
            <?php if (is_stashed('direct_matrix')): ?>
                <div class="pending-overlay" style="background:#f7f9fb;"></div>
            <?php endif; ?>
            <div id="modlog-form-output" class="<?php if (is_stashed('direct_matrix')): ?>visHide<?php endif; ?>">
                <?php if (is_stashed('direct_matrix')):
                    echo u_stash('direct_matrix');
                endif; ?>
            </div>
        </div>
    </div>
    <form class="card" id="ModlogForm">
        <div class="content dispFlex" style="min-height: 80px;">
            <section class="spacer20-right">
                <h4 class="spacer5-bottom visHide">Go</h4>
                <button id="ModlogForm_SubmitButton" type="submit" class="primary submit-button"
                    style="margin:0">Generate Matrix</button>
                <div class="submit-button-status spacer5-top">Please wait..</div>
            </section>
            <section class="spacer5-right">
                <h4 class="spacer5-bottom">From</h4>
                <label class="dispBlock">
                    <input type="text" id="ModlogField__lowerbound" autocomplete="off" style="min-width:188px;">
                </label>
                <div class="valign spacer5-top">
                    <h5>or</h5>
                    <input type="hidden" id="ModLogField__lowerboundOptionValue--EarliestUTC"
                        value="<?php echo i_stash('earliest_action_utc') ?>" />
                    <label class="valign" id="ModlogField__lowerboundOptionLabel--AllTime">
                        <input type="checkbox" id="ModlogField__lowerboundOptionValue--AllTime" />
                        <span class="valign no-select spacer5-left">
                            <span>Since <?php echo timeConvert(i_stash('earliest_action_utc'), true) ?></span>
                            <span class="icon icon--help"
                                data-tippy="
                                    <?php echo timeConvert(i_stash('earliest_action_utc'), true) ?>
                                    is the earliest date we have mod actions on file for
                                    the selected subreddit(s).">
                                <i class="zmdi zmdi-help"></i>
                            </span>
                        </span>
                    </label>
                </div>
            </section>
            <section>
                <h4 class="spacer5-bottom">To</h4>
                <label class="dispBlock">
                    <input type="text" id="ModlogField__upperbound" autocomplete="off" style="min-width:188px;">
                </label>
                <div class="valign spacer5-top">
                    <h5>or</h5>
                    <label class="valign" id="ModlogField__upperboundOptionLabel--DateNow">
                        <input type="checkbox" id="ModlogField__upperboundOptionValue--DateNow" />
                        <span class="no-select spacer5-left">Today</span>
                    </label>
                </div>
            </section>
        </div>
        <div id="ModlogField__moderators" class="Chooser Chooser--MatrixForm">
            <header class="ChooserTop">
                <label class="ChooserOption ChooserOption--all">
                    <input type="checkbox" checked />
                    <span>All Moderators</span>
                </label>
                <label class="ChooserOption">
                    <input id="ModlogField__moderators--CurrentModsOpt" type="checkbox"
                        checked data-value="current_mods_only" />
                    <span>Current Mods Only
                        <b class="icon icon--help"
                            style="font-weight:normal;margin-left:3px"
                            data-tippy="
                                Unchecking this will show you any past moderators, Reddit admins,
                                and Reddit Trust &amp; Safety who have had at least one action.">
                            <i style="font-size:14px" class="zmdi zmdi-help"></i>
                        </b>
                    </span>
                </label>
            </header>
            <section class="wSplit5 dispFlex flexWrap">
            <?php foreach (xa_stash('moderators') as $mod):
                $is_current_mod = in_array($mod, a_stash('current_moderators'));
                ?>
                <label class="ChooserOption<?php if ($is_current_mod): ?> is--current_mod<?php else: ?> is--past_mod hide<?php endif; ?>">
                    <input type="checkbox" checked data-value="<?php echo $mod ?>" />
                    <span><?php echo $mod ?></span>
                </label>
            <?php endforeach; ?>
            </section>
        </div>
        <div id="ModlogField__columnsFilter" class="Chooser wSplit5 Chooser--MatrixForm">
            <header class="ChooserTop">
                <label class="ChooserOption ChooserOption--all">
                    <input type="checkbox" checked />
                    <span>All Actions</span>
                </label>
            </header>
            <?php foreach (xa_stash('action_types') as $group => $action_types): ?>
            <section>
                <h3><?php echo $group ?></h3>
                <div class="content">
                <?php foreach ($action_types as $action_type): ?>
                    <label class="ChooserOption no-select">
                        <input type="checkbox" checked data-value="<?php echo $action_type['name'] ?>" />
                        <span class="valign">
                            <i class="modactions modactions-<?php echo $action_type['name'] ?> spacer5-right"></i>
                            <?php echo $action_type['display_name'] ?>
                        </span>
                    </label>
                <?php endforeach; ?>
                </div>
            </section>
            <?php endforeach; ?>
        </div>
    </form>
</div>