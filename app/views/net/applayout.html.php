<div id="app">
<noscript>
    <p class="error-notice js-error-notice"><strong>This website does not work without JavaScript.</strong></p>
</noscript>
<!--[if IE]>
<p id="ie-error-notice" class="error-notice js-error-notice">
    <strong>This website may not work properly on Internet Explorer. Try Chrome or Firefox.</strong>
</p>
<![endif]-->
<p id="ieLte10-error-notice" style="display:none" class="error-notice js-error-notice">
    <strong>This website may not work properly on Internet Explorer. Try Chrome or Firefox.</strong>
</p>
<p id="ie11-error-notice" style="display:none" class="error-notice js-error-notice">
    <strong>This website may not work properly on Internet Explorer. Try Chrome or Firefox.</strong>
</p>
<?php if (!user()->property('has_seen_tos')): ?>
<div class="TermsOfServiceNotice">
    <p>We've updated our <a href="<?php echo SITE_URL?>terms">Terms of Service</a>
    and <a href="<?php echo SITE_URL?>privacy">Privacy Policy</a>.</p>
    <button class="TermsOfServiceNotice__confirm primary primary--2">Got it</button>
</div>
<?php endif; ?>
<app-side v-cloak site-name="Layer 7 Solutions">
    <div v-cloak slot="logo"><?php include IMAGES_ROOT . x_stash('logo_url'); ?></div>

    <app-side-group sel="overview" :top="true">
        <app-side-item class="big-item" href="<?php echo SITE_URL ?>overview/" sel="overview">
            <i class="icon zmdi zmdi-view-web"></i>
            <span>Overview</span>
        </app-side-item>
        <app-side-item sel="prefs_subsettings" href="<?php echo SITE_URL ?>prefs/subreddit">
            <i class="icon fr zmdi zmdi-settings"></i>
            <span>Subreddit Settings</span>
        </app-side-item>
    </app-side-group>

    <?php if (user()->modded->has_category('mediablacklist')): ?>
    <app-side-group name="MediaBlacklist" sel="tsb">
        <div slot="header">Media Blacklist</div>

        <app-side-item sel="tsb_blacklist" href="<?php echo SITE_URL ?>blacklist/">
            <i class="icon zmdi zmdi-block"></i>
            <span>Edit Blacklist</span>
        </app-side-item>
        <app-side-item sel="tsb_view" href="<?php echo SITE_URL ?>blacklist/view/">
            <i class="icon zmdi zmdi-view-headline"></i>
            <span>View Blacklist</span>
        </app-side-item>
        <app-side-item sel="tsb_reports" href="<?php echo SITE_URL ?>blacklist/reports/">
            <i class="icon zmdi zmdi-comment-outline"></i>
            <span>View Reports</span>
        </app-side-item>
        <app-side-item sel="tsb_global" href="<?php echo SITE_URL ?>blacklist/global/">
            <i class="icon zmdi zmdi-globe"></i>
            <span>Global Blacklist</span>
        </app-side-item>
    </app-side-group>
    <?php endif; ?>

    <?php if (user()->modded->has_category(['logs','userlog'])): ?>
    <app-side-group name="LogsAndData" sel="logs,userlog,data">
        <div slot="header">Logs &amp; Data</div>

        <app-side-item sel="logs_view" href="<?php echo SITE_URL ?>logs/view/">
            <i class="icon zmdi zmdi-search"></i>
            <span>Search Mod Logs</span>
        </app-side-item>
        <app-side-item sel="logs_matrix" href="<?php echo SITE_URL ?>logs/matrix/">
            <i class="icon zmdi zmdi-grid"></i>
            <span>Mod Matrix</span>
        </app-side-item>

        <?php if (user()->modded->has_category('userlog')): ?>
        <app-side-item sel="data_users" href="<?php echo SITE_URL ?>data/users/">
            <i class="icon zmdi zmdi-accounts"></i>
            <span>Users</span>
        </app-side-item>
        <?php endif; ?>
        <!--<app-side-item sel="data_explore" href="<?php echo SITE_URL ?>data/explore">
            <i class="icon zmdi zmdi-comments"></i>
            <span>Posts &amp; Comments</span>
        </app-side-item>-->
    </app-side-group>
    <?php endif; ?>

    <app-side-group name="OtherTools" sel="botban,domainblacklist,dirtbag,shortlink">
        <div slot="header">Other Tools</span></div>

        <?php if (user()->modded->has_category('botban')): ?>
        <app-side-item sel="botban" href="<?php echo SITE_URL ?>botban/">
            <i class="icon zmdi zmdi-close-circle-o"></i>
            <span>User Blacklist (Bot ban)</span>
        </app-side-item>
        <?php endif; ?>

        <?php if (user()->modded->has_category('domainblacklist')): ?>
        <!--<app-side-item sel="domainblacklist" href="<?php echo SITE_URL ?>domainban/">
            <i class="icon zmdi zmdi-http"></i>
            <span>Domain Blacklist</span>
        </app-side-item>-->
        <?php endif; ?>

        <?php if (user()->modded->has_category('dirtbag')): ?>
        <app-side-item sel="dirtbag" href="<?php echo SITE_URL ?>dirtbag">
            <i class="icon zmdi zmdi-settings"></i>
            <span>Dirtbag Settings</span>
        </app-side-item>
        <?php endif; ?>

        <app-side-item sel="shortlink" href="<?php echo SITE_URL ?>shortlink">
            <i class="icon fa fa-link"></i>
            <span>Link Shortener</span>
        </app-side-item>
    </app-side-group>

    <?php if (user()->is_admin() || user()->is_globalist()): ?>
    <app-side-group name="AdminTools" sel="admin">
        <div slot="header">Admin Tools</span></div>

        <?php if (user()->is_admin()): ?>
            <app-side-item class="add-hash"
                href="<?php echo SITE_URL ?>admin/<?php
                    echo user()->admin() ? 'disable' : 'enable'
                ?>/?cont=<?php echo safe_current_url() ?>">
                <?php if (user()->admin()): ?>
                    <span>Disable Admin Mode</span>
                <?php else: ?>
                    <span>Enable Admin Mode</span>
                <?php endif; ?>
            </app-side-item>
        <?php endif; ?>
        <app-side-item sel="admin_globalqueue" href="<?php echo SITE_URL ?>admin/globalqueue/">Global Blacklist Queue</app-side-item>
        <?php if (user()->is_admin()): ?>
            <app-side-item sel="admin_userlist" href="<?php echo SITE_URL ?>admin/userlist/">User List</app-side-item>
            <app-side-item sel="admin_oauth_data" href="<?php echo SITE_URL ?>admin/oauth_data/">OAuth Data</app-side-item>
            <app-side-item href="<?php echo SITE_URL ?>admin/adminer/">Database Admin</app-side-item>
        <?php endif; ?>
    </app-side-group>
    <?php endif; ?>

    <app-side-group class="app-side-footer" :bottom="true">
        <h5>&copy; LAYER 7 SOLUTIONS, LLC</h5>
        <p>
            <a href="<?php echo SITE_URL ?>terms">Terms</a>
            <span>&bull;</span>
            <a href="<?php echo SITE_URL ?>privacy">Privacy</a>
        </p>
    </app-side-group>
</app-side>
<div id="app-content" class="<?php echo_if('no-gutter', b_stash('app-no-gutter')) ?>"><?php
    render_content_id()
?></div>
</div>