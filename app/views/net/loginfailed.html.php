<div id="inlogin" class="wrapper halign">
    <div id="inlogin__Inner" class="wrapper halign">
        <div id="inlogin__Failed">
            <h2 id="inlogin__Title">Login Failed</h2>
            <div id="inlogin__Notice">
                <p>Failed to login: <?php echo x_stash('error') ?>.</p>
                <p><a href="<?php echo SITE_URL ?>">Back to home</a></p>
            </div>
            <a class="inlogin__Button" href="<?php echo SITE_URL ?>login">Try again?</a>
        </div>
    </div>
</div>