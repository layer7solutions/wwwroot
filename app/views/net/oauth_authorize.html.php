<component is="style">
body {
    background: #fafafa
}

#oauth_authorize_form {
    max-width: 700px;
}

h2 {
    font-size: 12px;
    text-transform: uppercase;
    font-weight: bold
}

#oauth_authorize_form__box {
    padding: 10px;
    margin: 10px 0;
    box-shadow: 0 -1px 0 #efefef,0 0 2px rgba(0,0,0,0.12),0 1.5px 4px rgba(0,0,0,.14);
    background: white;
}

#oauth_authorize_form__box p {
    font-size: 15px;
}
</component>
<?php if (!b_stash('logged_in')): ?>
<component is="style">#site-nav .fr {display:none;}</component>
<?php endif; ?>
<div id="oauth_authorize_form" class="wrapper spacer">
    <h2 style="font-family:'Open Sans',sans-serif;">Request for Permission</h2>
    <?php if (b_stash('logged_in')): ?>
    <form id="oauth_authorize_form__box" method="POST">
        <input type="hidden" name="feature" value="<?php xecho(from($_REQUEST, 'feature')) ?>" />
        <label>Hey <?php echo x_stash('username') ?>,
        <b><?php echo x_stash('app_name') ?></b> would like to connect with your Layer7 Solutions account.</label>
        <div style="background:#f7f7f7;padding:10px;margin:10px 0 2px">
            <p>Allow <?php echo x_stash('app_name') ?> to:</p>
            <ul style="padding-left:30px;font-size:15px;color:#555">
                <?php if (!isset($_REQUEST['scope'])): ?>
                <li><?php echo api_scope_desc('identity') ?></li>
                <?php else: foreach (explode(' ',$_REQUEST['scope']) as $scope): ?>
                <li><?php echo api_scope_desc($scope) ?></li>
                <?php endforeach; endif; ?>
            </ul>
        </div>
        <div style="margin-top:5px;">
            <button class="primary yes" type="submit" name="authorized" value="yes">Allow</button>
            <button class="primary no" type="submit" name="authorized" value="no">Decline</button>
        </div>
    </form>
    <?php else: ?>
    <div id="oauth_authorize_form__box">
        <p>Login first using Reddit to connect <?php echo x_stash('app_name') ?> to your Layer7 Solutions account.</p>
        <div class="spacer"></div>
        <div>
            <a class="login-button" href="<?php echo SITE_URL ?>login?cont=<?php
                    echo safe_current_url() ?>">
                <i class="icon icon-reddit zmdi zmdi-reddit"></i>
                <span>Login with Reddit</span>
            </a>
        </div>
    </div>
    <?php endif; ?>
</div>