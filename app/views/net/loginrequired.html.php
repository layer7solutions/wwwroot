<div class="LoginRequired fsplit">
    <div class="LoginRequired__imgArea">
        <img src="<?php img_src('landing/intro/explore-data.svg') ?>" />
    </div>
    <div class="LoginRequired__textArea">
        <div class="LoginRequired__textWrap">
            <div><p class="LoginRequired__header">Login Required</p></div>
            <div class="LoginRequired__description">You must be logged in to access this page.</div>
            <div class="LoginRequired__retry">
                <a href="<?php echo SITE_URL . 'login?cont=' . safe_current_url(); ?>">
                    <button class="primary">Login and continue</button>
                </a>
            </div>
        </div>
    </div>
</div>