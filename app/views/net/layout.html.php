<?php if (SITE_IS_STAGING): ?>
<script>
Vue.config.devtools = true;
</script>
<?php endif; ?>
<div id="website" v-cloak>
    <nav id="site-nav">
        <div class="wrapper">
            <div class="fl">
                <i class="icon zmdi zmdi-menu mobi-trigger"></i>
                <a class="nav-logo-a" href="<?php echo SITE_URL ?>">
                    <div class="nav-logo">
                        <?php include IMAGES_ROOT . x_stash('logo_url'); ?>
                    </div>
                    <span>Layer 7 Solutions</span>
                </a>
                <a href="https://bitbucket.org/layer7solutions/">Bitbucket</a>
                <a href="https://www.reddit.com/r/layer7">Subreddit</a>
                <a href="<?php echo SITE_URL ?>docs">Docs</a>
                <a href="<?php echo SITE_URL ?>discord">Discord</a>
                <a href="https://www.patreon.com/Layer7">Patreon</a>
            </div>
            <div class="fr">
                <?php if (user()->logged_in()): ?>
                <div class="nav-user-menu spacer5-right">
                    <?php if (!b_stash('user_has_timezone_set') && !body_class_exists('page--prefs_user')): ?>
                    <div class="user-button user-tz-button dropdown--trigger">
                        <div class="user-label">
                            <span>Set timezone</span>
                        </div>
                        <i class="icon zmdi zmdi-time"></i>
                        <div class="user-dropdown dropdown dropdown--panel">
                            <div class="content">
                            <p class="spacer-bottom">
                                You don't have a time zone currently set in your <strong>User Preferences</strong>, so
                            dates/times are currently displayed in "Greenwich Mean Time". It is
                            recommended you select your preferred timezone.</p>
                            <a href="<?php echo SITE_URL ?>prefs#choose-timezone">
                                <button class="primary primary--2">Choose timezone</button>
                            </a>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="user-button dropdown--trigger">
                        <div class="user-label">
                            <span><?php echo user()->get_username(); ?></span>
                        </div>
                        <i class="icon zmdi zmdi-chevron-down"></i>
                        <div class="user-dropdown dropdown">
                            <a class="add-hash" href="<?php echo SITE_URL ?>prefs/nightmode-<?php
                                    echo user()->property('nightmode_enabled') ? 'disable' : 'enable'
                                    ?>?cont=<?php echo safe_current_url() ?>">
                                <?php if (user()->property('nightmode_enabled')): ?>
                                <span>Disable nightmode<i class="icon fr zmdi zmdi-brightness-5"></i></span>
                                <?php else: ?>
                                <span>Enable nightmode<i class="icon fr zmdi zmdi-brightness-2"></i></span>
                                <?php endif; ?>
                                <small>(reloads page)</small>
                            </a>
                            <a href="<?php echo SITE_URL ?>prefs">
                                <span>Preferences<i class="icon fr zmdi zmdi-settings"></i></span>
                            </a>
                            <a href="<?php echo SITE_URL ?>prefs/subreddit">
                                <span>Subreddit Settings<i class="icon fr zmdi zmdi-settings"></i></span>
                            </a>
                            <hr>
                            <form method="POST" action="<?php echo SITE_URL ?>logout">
                                <?php session_csrf_token_field() ?>
                                <button type="submit">Logout</button>
                            </form>
                        </div>
                    </div>
                </div>
                <?php else: ?>
                <a class="login-button add-hash"
                        href="<?php echo SITE_URL ?>login?cont=<?php echo safe_current_url() ?>">
                    <i class="icon icon-reddit zmdi zmdi-reddit"></i> Login with reddit</a>
                <?php endif; ?>
            </div>
        </div>
    </nav>
    <?php if (!body_class_exists('application')): ?>
    <!--[if IE]>
    <p id="ie-error-notice" class="error-notice js-error-notice">
        <strong>This website may not work properly on Internet Explorer. Try Chrome or Firefox.</strong>
    </p>
    <![endif]-->
    <p id="ieLte10-error-notice" style="display:none" class="error-notice js-error-notice">
        <strong>This website may not work properly on Internet Explorer. Try Chrome or Firefox.</strong>
    </p>
    <p id="ie11-error-notice" style="display:none" class="error-notice js-error-notice">
        <strong>This website may not work properly on Internet Explorer. Try Chrome or Firefox.</strong>
    </p>
    <?php endif; ?>
    <div id="shutdown" style="padding:10px 0;border-top:1px solid #ccc;border-bottom:1px solid #ccc;background:#fff;">
        <div class="wrapper" style="
        line-height: 26px;
        font-size: 15px;
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';
        max-width: 900px;
        width: 100%;
        margin-left: auto;
        margin-right: auto;
        padding: 0 40px;
        box-sizing: border-box;">
            <p><strong>TheSentinelBot (TSB) is permanently shutting down.</strong> Unfortunately we are no longer able
            to maintain the TSB project mainly due to Google restricting our API usage and also due to a lack of developer
            and system resources. We've submitted requests to Google for raising the rate limit on our API key, however those have been denied. As such, we will soon be shutting
            down all our TSB bots indefinitely. If you'd like to export any of your data, please reach out
            to us in our <strong><a href="<?php echo SITE_URL ?>discord">Discord</a></strong>. Thank you for using TSB up until now.</p>
        </div>
    </div>
    <main><?php render_content_id(); ?></main>
    <?php if (!b_stash('no_footer')): ?>
    <footer id="footer-parent">
        <div id="footer">
            <div id="copy" class="col grow">
                <p>&copy; Layer 7 Solutions, LLC</p>
            </div>
            <div class="col">
                <h3>Info</h3>
                <ul>
                    <li><a href="<?php echo SITE_URL ?>getting-started">Getting Started</a></li>
                    <li><a href="<?php echo SITE_URL ?>docs/about/media-blacklist">Media Blacklist</a></li>
                    <li><a href="<?php echo SITE_URL ?>docs/about/mod-logs">Mod Logs</a></li>
                    <li class="fontItalic">More coming soon!</li>
                </ul>
            </div>
            <div class="col">
                <h3>About</h3>
                <ul>
                    <li><a href="<?php echo SITE_URL ?>about">About Us</a></li>
                    <li><a href="<?php echo SITE_URL ?>faq">FAQ</a></li>
                    <li><a href="https://www.reddit.com/r/Layer7">Our Subreddit</a></li>
                    <li><a href="<?php echo SITE_URL ?>discord">Our Discord</a></li>
                    <li><a href="https://bitbucket.org/layer7solutions/">Source Code</a></li>
                </ul>
            </div>
            <!--<div class="col">
                <h3>Developers</h3>
                <ul>
                    <li><a href="<?php echo SITE_URL ?>developers">API Reference</a></li>
                    <li><a href="<?php echo SITE_URL ?>prefs/apps">My Apps</a></li>
                </ul>
            </div>-->
            <div class="col">
                <h3>Resources</h3>
                <ul>
                    <li><a href="https://www.reddit.com/r/layer7/submit?selftext=true&amp;title=%5BSupport%5D%20%20%20...">Need Help?</a></li>
                    <li><a href="<?php echo SITE_URL ?>contact">Contact</a></li>
                    <li><a href="<?php echo SITE_URL ?>privacy">Privacy</a></li>
                    <li><a href="<?php echo SITE_URL ?>terms">Terms</a></li>
                </ul>
            </div>
        </div>
    </footer>
    <?php endif; ?>
</div>