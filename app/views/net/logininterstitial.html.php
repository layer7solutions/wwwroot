<div id="inlogin" class="wrapper halign">
    <div id="inlogin__Inner" class="wrapper halign">
        <div id="inlogin__Icon">
            <svg class="loader" viewBox="0 0 120 120">
                <circle class="loader--internal-circle" cx="60" cy="60" r="30"></circle>
                <circle class="loader--external-circle" cx="60" cy="60" r="50"></circle>
            </svg>
        </div>
        <div id="inlogin__Bottom">
            <h2 id="inlogin__Title">We're logging you in...</h2>
            <div id="inlogin__Notice">
                <p>This may take a bit if you mod more than 300 subreddits.</p>
                <p>Do not refresh this page.</p>
            </div>
        </div>
    </div>
</div>
<form method="POST" action="<?php echo SITE_URL ?>login" name="LoginCallback">
    <?php if (isset($_REQUEST['cont'])): ?>
        <input name="cont" type="hidden" value="<?php echo $_REQUEST['cont'] ?>" />
    <?php endif; ?>
    <input name="code" type="hidden" value="<?php echo $_REQUEST['code'] ?>" />
    <input name="state" type="hidden" value="<?php echo $_REQUEST['state'] ?>" />
</form>