<div id="forbidden">
    <p class="tagline">Go to <a href="https://layer7.solutions/">https://layer7.solutions</a></p>
    <p class="description" style="
        text-align: left;
        margin: 0 40px;
    ">You're on the beta site! The beta site is where we test new features
    and is limited access to Layer7 team members.</p>
    <p class="description" style="
        text-align: left;
        margin: 10px 40px;
    ">Head on over to
    <a href="https://layer7.solutions/">https://layer7.solutions/</a> instead. Thank you!</p>
</div>
<style>
#forbidden {
    width: 500px;
    text-align: center;
    margin: 50px auto 0;
}
#forbidden .tagline {
    font-size: 17px;
    font-weight: 700;
    text-transform: uppercase;
    color: rgba(0,0,0,0.5);
    flex-grow: 1;
}
#forbidden .description {
    font-size: 15px;
}
#forbidden .retry {
    margin-top: 10px;
}
</style>