<h3 class="spacer-bottom">Dirtbag Settings</h3>

<p>Choose Subreddit:</p>

<div class="sr-listing spacer">
    <?php foreach(xa_stash('dirtbag_srlist') as $sr): ?>
        <a href="<?php echo SITE_URL ?>dirtbag/<?php echo $sr ?>" class="item">
            <button class="primary spacer5-bottom"><?php xecho($sr) ?></button>
        </a>
    <?php endforeach; ?>
</div>