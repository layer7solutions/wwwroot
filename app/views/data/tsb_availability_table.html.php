<?php $tsbsubs = \app\lib\Common::tsb_agents(true) ?>
<table>
    <thead>
        <tr>
            <th>Bot</th>
            <th>Current Subscribers</th>
            <th>Max Subscribers</th>
            <th>Availability</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($tsbsubs as $bot => $subs): ?>
        <tr>
            <td><a href="https://www.reddit.com/u/<?php xecho($bot) ?>">/u/<?php xecho($bot) ?></a></td>
            <td><?php echo ($subs === false ? 'Reserved' : number_format($subs)) ?></td>
            <td>20,000,000</td>
            <?php if ($subs < 20000000 && $subs !== false): ?>
                <td style="background:rgba(83, 191, 107, 0.2) !important">Available</td>
            <?php else: ?>
                <td style="background: rgba(230, 69, 72, 0.2) !important">At Capacity</td>
            <?php endif; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>