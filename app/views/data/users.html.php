<h3 class="spacer-bottom">
    <span>Users</span>
    <i class="icon fa fa-angle-right spacer3-horiz"></i>
    <span><?php echo x_stash('sr_title') ?></span>
</h3>

<section>
<h3>New Users <span class="label spacer5-left">BETA</span></h3>
<form action="<?php echo SITE_URL ?>data/users/<?php echo x_stash('srIn') ?>" class="spacer10 box"
        method="POST" style="max-width: 700px;padding-right: 0;">
    <?php session_csrf_token_field() ?>
    <?php if (is_stashed('error')): ?>
    <p class="error-notice"><?php echo x_stash('error') ?></p>
    <?php endif; ?>
    <label class="spacer10-right">
        <span class="dispInlineBlock spacer5-right">Sort</span>
        <select name="s">
            <option value="0" <?php echo_if('selected', u_stash('order') == 0) ?>>First Post</option>
            <option value="1" <?php echo_if('selected', u_stash('order') == 1) ?>>Cakeday</option>
        </select>
    </label>
    <label class="spacer10-right">
        <span class="dispInlineBlock spacer5-right">Amount</span>
        <input name="a" type="number" value="<?php echo i_stash('limit') ?>" min="1" step="1" />
    </label>
    <label class="spacer10-right">
        <span class="dispInlineBlock spacer5-right">Offset</span>
        <input name="p" type="number" value="<?php echo i_stash('offset') ?>" min="0" />
    </label>
    <button type="submit" class="primary small">Go</button>
</form>
<table class="primary">
    <thead>
        <tr>
            <th>Author</th>
            <th>Cakeday</th>
            <th>First Post in Subreddit</th>
            <th>Author ID</th>
            <th>Is Content Creator</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach (xa_stash('new_users_table') as $entry): ?>
        <tr>
            <td><a href="https://www.reddit.com/u/<?php echo $entry['author'] ?>"><?php echo $entry['author'] ?></a></td>
            <td><?php echo timeConvert($entry['authorcreated_utc']) ?></td>
            <td>
                <span><?php echo timeConvert($entry['current_utc']) ?></span>
                <span>&nbsp;</span>
                <a href="<?php echo $entry['permalink'] ?>">(link)</a>
            </td>
            <td><code><?php echo $entry['authorid'] ?></code></td>
            <td><?php if ($entry['iscontentcreator']): ?><i class="zmdi zmdi-check-circle green"></i><?php endif; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</section>