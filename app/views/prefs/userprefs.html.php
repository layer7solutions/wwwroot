<h3 class="spacer-bottom">
    <span>Preferences</span>
    <i class="icon fa fa-angle-right spacer3-horiz"></i>
    <span>User Options</span>
</h3>
<p>Changes are automatically saved. <strong>Please read the text</strong> accompanying any options
carefully.</p>

<section id="general-settings">
    <h3>General Settings</h3>
    <fieldset>
        <h4>Interface Theme</h4>
        <label>
            <input type="radio" class="UserPrefItem" name="nightmode_enabled" data-value1="false"
                <?php echo_if('checked', !u_stash('prefs')['nightmode_enabled'] ) ?> />
            <span>Light</span>
        </label>
        <label>
            <input type="radio" class="UserPrefItem" name="nightmode_enabled" data-value1="true"
                <?php echo_if('checked', u_stash('prefs')['nightmode_enabled'] ) ?> />
            <span>Dark (beta)</span>
        </label>
    </fieldset>
    <fieldset id="choose-timezone">
        <h4>Timezone</h4>
        <p class="spacer10-bottom">If your country observes Daylight Savings Time, choose your
        timezone for the standard-time GMT offset. DST will be detected automatically and date/time
        displays across the site will be adjusted appropriately.</p>
        <label>
            <select class="UserPrefItem" name="user_timezone">
                <option value="" disabled selected hidden>Please Choose...</option>
                <?php foreach (get_timezones_by_region() as $region => $timezones): ?>
                <optgroup label="<?php xecho($region) ?>">
                    <?php foreach ($timezones as $display_name => $php_name):
                        $composite_name = tz_create_composite($php_name, $display_name);
                        ?>
                        <option value="<?php echo json_encode_for_html($composite_name) ?>"
                                <?php echo_if('selected', u_stash('prefs')['user_timezone'] === $composite_name) ?>>
                            <?php xecho($display_name) ?>
                        </option>
                    <?php endforeach; ?>
                </optgroup>
                <?php endforeach; ?>
            </select>
        </label>
    </fieldset>
    <fieldset id="session-expiry">
        <h4>Session Auto-Expiry</h4>
        <?php foreach (u_stash('session_lifetime_options') as list($value, $label_html, $is_custom)): ?>
            <label>
                <?php if ($is_custom): ?>
                    <input
                        id="CustomSessLifetimeRadio"
                        type="radio"
                        name="session_lifetime"
                        data-for-input="CustomSessLifetimeInput"
                        <?php echo_if('checked', u_stash('prefs')['session_lifetime'] === $value ) ?>
                        />
                    <div class="halign">
                        <span><?php echo $label_html ?></span>
                        <input
                            id="CustomSessLifetimeInput"
                            type="number"
                            class="UserPrefItem w100p"
                            name="session_lifetime"
                            data-for-radio="CustomSessLifetimeRadio"
                            value="<?php echo $value === -1 ? '' : xssafe($value) ?>"
                            step="1"
                            min="0"
                            max="<?php echo SESSION_LIFETIME_MAXIMUM ?>"
                            />
                    </div>
                <?php else: ?>
                    <input type="radio" class="UserPrefItem" name="session_lifetime"
                        data-value1="<?php echo xssafe($value) ?>"
                        <?php echo_if('checked',
                                u_stash('prefs')['session_lifetime'] === $value
                                || ( is_null(u_stash('prefs')['session_lifetime'])
                                        && $value === SESSION_LIFETIME_DEFAULT) )
                        ?> />
                    <span><?php echo $label_html ?></span>
                <?php endif; ?>
            </label>
        <?php endforeach; ?>
    </fieldset>
</section>
<section id="modded-subs">
    <h3>Subreddits you mod</h3>
    <p>You can find all synced subreddits that you mod
    <strong><a href="<?php echo SITE_URL ?>prefs/subreddit">here</a></strong>.
    If there are any missing, you can click the button below. Note that depending on how many
    subreddits you mod, <strong>this operation might take a bit</strong>.
    </p>
    <div class="UserModdedReSyncForm">
        <button class="primary UserModdedReSyncButton">re-sync modded subreddits</button>
    </div>
    <p class="spacer">
    The above button will update everything and might take some time. If there is a specific subreddit
    that you believe should be in your list but isn't, you can try manually adding it below. If our
    systems detect that you mod that subreddit, then it'll be added to your modded subreddits list.</p>
    <div class="UserModdedManualAddForm valign">
        <input type="text" class="UserModdedManualAddInput" placeholder="Enter a subreddit" />
        <button class="primary primary--2 small spacer5-left UserModdedManualAddButton">add</button>
    </div>
</section>
<section id="privacy_settings">
    <h3>Privacy Settings</h3>
    <p>We temporarily store IP addresses to offer you better security. For example, IP addresses
    wil be shown on the <a href="http://layer7.localhost/prefs/account-activity">Account Activity</a>
    page. Also, IP addresses will also be used to verify AJAX requests (AJAX is a way to send requests
    to a website without you needing to refresh the page); if the IP address you originally visited
    the page with does not match the IP address of the AJAX request, then the AJAX request will be
    blocked. However, these security features are not essential, enabling the setting below will not
    significantly impact your Layer 7 account's security. Only the most recent IP address you used
    to access your account will be stored. Any IP addresses linked to your account will be deleted
    from our database once you log out.</p>
    <label class="valign">
        <input type="checkbox" class="UserPrefItem" name="no_ipstore"
            data-value1="true" data-value0="false"
            <?php echo_if('checked', u_stash('prefs')['no_ipstore'] ) ?> />
        <span>Do not store IP addresses or browser info.</span>
    </label>
</section>
<section id="data_dump">
    <h3>User Data Dump</h3>
    <p>By clicking the button below you will be accessing a page that may contain some sensitive
    information. You should only visit this page <strong>over a secure network</strong> - if you're
    connected to a Wi-Fi network, it should be one that is secured rather than a public network like
    Starbucks free Wi-Fi.</p>
    <a href="<?php echo SITE_URL ?>prefs/account-data">
        <button class="primary danger">User Data Dump</button>
    </a>
</section>