<h3 class="spacer-bottom">
    <span>Preferences</span>
    <i class="icon fa fa-angle-right spacer3-horiz"></i>
    <span><?php xecho(stash('VIEW')->title()) ?></span>
</h3>
<?php if (u_stash('page') === 'data'): ?>
<p>In the spirit of transparency and stuff, listed on this page is information related to your account.
Please be aware that some of the information on this page can be sensitive and if compromised can
be used to hijack your account. Sensitive information is marked with a red asterisk (<b class="red">*</b>).
If you believe your session has been compromised, click the
"clear all sessions" button below.</p>
<?php endif; ?>
<section class="SessionList">
    <h3>Recent activity on your account</h3>
    <p>This section shows a history of recent activity on your Layer 7 account. If you notice
    unusual activity, you should click the "clear all sessions" button immediately.</p>
    <table class="SessionList__table w100p primary">
        <thead>
            <tr>
                <?php if (u_stash('page') === 'data'): ?>
                    <th style="width: 180px;">session id &amp; expires</th>
                <?php else: ?>
                    <th style="width: 160px;">session expires</th>
                <?php endif; ?>
                <th>ip address</th>
                <th>platform</th>
                <th>browser</th>
                <?php if (u_stash('page') === 'data'): ?>
                <th>session data</th>
                <?php endif; ?>
                <th>last visit</th>
                <th style="width:120px">force logout
                    <b data-tippy="You can remotely logout of a specific session using the buttons in this column."
                        class="icon icon--help icon--forceLogoutInfo spacer5-left"
                        style="font-weight: normal;text-transform:initial">
                        <i class="zmdi zmdi-help"></i>
                    </b>
                </th>
            </tr>
        </thead>
        <tbody>
        <?php foreach (xa_stash('sessions') as $session): ?>
            <tr class="SessionList__row" data-token="<?php echo $session['delete_token'] ?>">
                <td style="font-size:13px;line-height:17px;">
                    <?php if (u_stash('page') === 'data'): ?>
                        <div style="font-size:10px">
                            <code><?php echo $session['session_id'] ?></code>
                            <b class="red">*</b>
                            <div style="font-size:12px;"><?php echo timeConvert($session['session_expires']) ?></div>
                        </div>
                    <?php else: ?>
                        <span><?php echo timeConvert($session['session_expires']) ?></span>
                    <?php endif; ?>
                </td>
                <td style="font-size:12px"><code><?php echo $session['session_ipaddr'] ?: 'n/a' ?></code></td>
                <td><?php echo $session['session_platform'] ?: 'n/a' ?></td>
                <td><?php echo $session['session_browser'] ?: 'n/a' ?></td>
                <?php if (u_stash('page') === 'data'): ?>
                    <td><textarea class="code" readonly><?php echo $session['session_data'] ?></textarea></td>
                <?php endif; ?>
                <td title="<?php echo timeConvert($session['session_lastutc']) ?>">
                    <?php echo human_timing($session['session_lastutc']) ?>
                    <?php if ($session['session_id'] === u_stash('current_session_id')): ?>
                        <b data-tippy="This is the session you are currently using to view this page"
                            class="icon icon--currentSessionIndicator icon--help spacer5-left"
                            style="font-weight: normal;">
                            <i class="zmdi zmdi-help"></i>
                        </b>
                    <?php endif; ?>
                </td>
                <td>
                    <div class="valign">
                        <?php if ($session['session_id'] === u_stash('current_session_id')): ?>
                            <form method="POST" action="<?php echo SITE_URL ?>logout">
                                <?php session_csrf_token_field() ?>
                                <button type="submit" class="tertiary spacer5">logout</button>
                            </form>
                        <?php else: ?>
                            <?php if (empty($session['delete_token'])): ?>
                                <button class="SessionList__deleteButton tertiary spacer5" disabled>logout</button>
                            <?php else: ?>
                                <button class="SessionList__deleteButton tertiary spacer5" data-token="<?php echo $session['delete_token'] ?>">logout</button>
                            <?php endif; ?>
                            <span class="loading small spacer10-left hide"></span>
                        <?php endif; ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="valign spacer10-top">
        <button class="SessionList__deleteButton SessionList__deleteButtonAll tertiary spacer5-right">clear all sessions</button>
        <span class="loading small spacer10-left hide"></span>
    </div>
</section>
<?php if (u_stash('page') === 'data'): ?>
<section class="UserProperties">
    <h3>User Properties</h3>
    <p>This is your entry in our users section of database.</p>

    <table class="UserProperties__table primary">
        <thead>
            <tr>
                <th>Property</th>
                <th>Data</th>
                <th style="width: 350px;">Comment</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach (xa_stash('user_props') as $key => $value): ?>
                <tr>
                    <td>
                        <strong><?php echo $key ?></strong>
                        <?php if (in_array($key, u_stash('user_props_sensitive'))): ?>
                            <b class="red">*</b>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php
                        if (is_bool($value))
                            echo '<i>'.bool_str($value).'</i>';
                        else if ($value === null)
                            echo "<i>null</i>";
                        else if (empty($value) && is_string($value))
                            echo "<i>&lt;empty string&gt;</i>";
                        else if (in_array($key, a_stash('user_props_dtlist')))
                            echo timeConvert($value) . " <small><i>(<code>{$value}</code>)</i></small>";
                        else
                            echo '<code>'.$value.'</code>';
                        ?>
                    </td>
                    <td><?php
                    if (isset(u_stash('user_props_comments')[$key])) {
                        echo u_stash('user_props_comments')[$key];
                    } ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</section>
<section class="UserModded">
    <h3>User Modded Subreddits</h3>
    <p>This is a list of all synced subreddits you mod. Items in this list might be outdated, you
    can see the last time an item was updated by viewing the rightmost "RECORD_LAST_UPDATED" column.
    We do not store the sidebars for any of your subreddits without TSB, only the information you
    see here. For subreddits that do have TSB, we also store the list of moderators and other
    information used to provide our services. If you see any subreddits missing, you can re-sync
    using the "re-sync modded subreddits" button in your <a href="<?php echo SITE_URL ?>prefs">preferences</a>.</p>
    <p>Total: <?php xecho(count(u_stash('user_real_modded'))) ?></p>
    <table class="UserModdedHistory__table primary">
        <thead>
            <tr>
                <th>meta</th>
                <th>description</th>
                <th style="width:160px">created<br/>
                created_utc</th>
                <th>user_is_moderator<br/>
                user_is_subscriber<br/>
                user_is_banned</th>
                <th style="width:120px;">mod_permissions</th>
                <th style="width:160px;">record_last_updated</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach (xa_stash('user_real_modded') as $row): ?>
            <tr>
                <td style="line-height:18px">
                    <div><b>name:</b> <?php echo $row['name'] ?></div>
                    <div><b>subreddit:</b> <?php echo $row['subreddit'] ?></div>
                    <div><b>type:</b> <?php echo $row['type'] ?></div>
                    <div><b>submit_type:</b> <?php echo $row['submit_type'] ?></div>
                    <div><b>title:</b> <?php echo $row['title'] ?></div>
                    <div><b>id:</b> <?php echo $row['id'] ?></div>
                    <div><b>quarantine:</b> <?php echo $row['quarantine'] ? 'yes' : 'no' ?></div>
                    <div><b>over18:</b> <?php echo $row['over18'] ? 'yes' : 'no' ?></div>
                </td>
                <td><p><?php echo $row['description'] ?></p></td>
                <td><?php echo timeConvert($row['created']) ?><br/>
                <?php echo timeConvert($row['created_utc']) ?></td>
                <td><?php echo $row['user_is_moderator'] ? 'yes' : 'no' ?><span>&nbsp;</span>
                <?php echo $row['user_is_subscriber'] ? 'yes' : 'no' ?><span>&nbsp;</span>
                <?php echo $row['user_is_banned'] ? 'yes' : 'no' ?></td>
                <td><?php echo implode(',<br/>',explode(',',$row['mod_permissions'])) ?></td>
                <td><?php echo timeConvert($row['record_last_updated']) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</section>
<section class="UserModdedHistory">
    <h3>User Modded Subreddits History</h3>
    <p>A history of your moderator status on subreddits with TSB.</p>
    <table class="UserModdedHistory__table primary">
        <thead>
            <tr>
                <th>type</th>
                <th>subreddit</th>
                <th>moderator</th>
                <th>new_state</th>
                <th>history_utc</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach (xa_stash('user_websync_history') as $row): ?>
            <tr>
                <td><?php echo $row['type'] ?></td>
                <td><?php echo $row['subreddit'] ?></td>
                <td><?php echo $row['moderator'] ?></td>
                <td><?php echo $row['new_state'] ?></td>
                <td><?php echo timeConvert($row['history_utc']) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</section>
<?php endif; ?>