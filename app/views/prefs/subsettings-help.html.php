<div class="box spacer" style="line-height: 21px;font-size: 15.5px;">
    <h3>Did we mess up?</h3>
    <p class="spacer10">TheSentinelBot (TSB) is programmed to automatically update our
    database of who mods what subreddits with TSB. If TSB is added to a subreddit you mod
    or if you're added to a subreddit with TSB, then that subreddit should appear on this
    page (it'll take a couple minutes sometimes so give some time).</p>
    <p class="spacer10">Sometimes it might mess up though, if you don't see your subreddit
    here with several minutes of being added, go to your
    <strong><a href="<?php echo SITE_URL ?>prefs/#modded-subs">preferences page</a></strong>
    and click the <strong>"re-sync modded subreddits"</strong> button or try the manual
    input and click "add".</p>
</div>