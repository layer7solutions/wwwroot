<h3 class="spacer-bottom">
    <span>Preferences</span>
    <i class="icon fa fa-angle-right spacer3-horiz"></i>
    <span>Subreddits</span>
</h3>
<div id="SubredditSettings" class="settings">
    <div class="settings__side">
        <div class="settings__sideTop">
            <i class="icon zmdi zmdi-search"></i>
            <input id="SubredditSettings__filter" type="text" />
        </div>
        <div class="settings__sideListing" id="SubredditSettings__SubredditList">
            <?php if (!is_stashed('srlist') || empty(a_stash('srlist'))): ?>
                <div class="settings__sideItem">No subreddits</div>
            <?php else: ?>
                <div class="settings__sideItem settings__sideItemHelp">Don't see your subreddit here?</div>
                <?php foreach (xa_stash('srlist') as $sr): ?>
                <div data-uri="<?php echo SITE_URL ?>prefs/r/<?php echo $sr ?>"
                    data-subreddit="<?php echo $sr ?>"
                    data-lcsubreddit="<?php echo strtolower($sr) ?>"
                    class="settings__sideItem SubredditSettings__SubredditListItem
                    SubredditSettings__SubredditListItem--<?php echo $sr ?>"><?php echo $sr ?></div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="settings__panel">
        <?php if (!is_stashed('srlist') || empty(a_stash('srlist'))): ?>
        <div class="settings__comp">
            <h2>None of the subreddits you mod are enabled for TheSentinelBot yet.</h2>
            <p class="spacer">To add TheSentinelBot to one of your subreddits, add as moderator one
            of the bots listed in the table below that are not at capacity.</p>
            <?php render_template('prefs.subsettings-help'); ?>
            <?php render_template('data.tsb_availability_table'); ?>
        </div>
        <?php else: ?>
            <?php if (is_stashed('srcomp_html')): ?>
            <div class="settings__comp" data-for="<?php echo x_stash('srcomp_name') ?>"><?php
                echo u_stash('srcomp_html');
            ?></div>
            <?php endif; ?>
            <div class="settings__comp settings__compHelp hide">
                <?php render_template('prefs.subsettings-help'); ?>
            </div>
        <?php endif; ?>
    </div>
</div>