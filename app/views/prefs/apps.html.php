<div class="wrapper">
<?php
$areyousure = <<<EOD
<span class="are-you-sure hide">
    <span>&nbsp;—&nbsp;</span>
    <span class="are-you-sure__label">are you sure?&nbsp;</span>
    <span class="are-you-sure__divider">(</span>
    <button class="are-you-sure__yes" type="submit"><a>yes</a></button>
    <span class="are-you-sure__divider">/</span>
    <a class="are-you-sure__no">no</a>
    <span class="are-you-sure__divider">)</span>
</span>
EOD;
?>
<h3 class="spacer-bottom">
    <span>Preferences</span>
    <i class="icon fa fa-angle-right spacer3-horiz"></i>
    <span>Apps</span>
</h3>
<div id="oauth2apps_form">
    <div id="oauth2apps_form__wrap">
        <?php if (isset($_REQUEST['error'])): ?>
            <div class="error-notice"><?php echo($_REQUEST['error']) ?></div>
        <?php endif; ?>
        <?php if (isset($_REQUEST['status'])): ?>
            <div class="notice" style="margin:0"><?php echo($_REQUEST['status']) ?></div>
        <?php endif; ?>
        <h2 style="margin-top:0">Authorized Applications</h2>
        <div class="oapplist--authorized" class="oauth2apps_form__boxcontent">
            <?php if (!empty(a_stash('authorized_apps'))): ?>
                <?php foreach (a_stash('authorized_apps') as $u_app): ?>
                    <form id="app_<?php xecho($u_app['client_id']) ?>" class="oapp oauth2apps_formbox" method="POST">
                        <?php session_csrf_token_field() ?>
                        <input type="hidden" name="action" value="app_revoke" />
                        <input type="hidden" name="client_id" value="<?php xecho($u_app['client_id']) ?>" />
                        <div>
                            <h3 class="oapp__name"><?php xecho($u_app['app_name']);
                            if (isset($u_app['app_is_official']) && $u_app['app_is_official'] === true): ?>
                                <strong class="oapp__IsOfficial">
                                    <i class="icon fa fa-check"></i>
                                    <span>Official Layer7 App</span>
                                </strong>
                            <?php endif; ?></h3>
                            <div class="oapp__developer">
                                <span>Developer:</span>
                                <a href="https://www.reddit.com/user/<?php xecho($u_app['developer']); ?>"
                                        target="_blank" rel="noopener noreferrer">
                                    <span><?php xecho($u_app['developer']); ?></span>
                                </a>
                                <?php if (!empty($u_app['app_url'])): ?>
                                <span>&nbsp;-&nbsp;</span>
                                <a target="_blank" rel="noopener noreferrer" href="<?php xecho($u_app['app_url']) ?>">about this app</a>
                                <?php endif; ?>
                            </div>
                            <?php if (isset($u_app['app_about'])): ?>
                            <div class="oapp__about"><?php echo(markdown($u_app['app_about'])) ?></div>
                            <?php endif; ?>
                            <?php if (!empty(trim($u_app['scope']))): ?>
                                <ul class="oapp__ScopeList scope-list">
                                    <?php foreach (explode(' ', $u_app['scope']) as $u_scope_item): ?>
                                    <li><?php xecho(api_scope_desc($u_scope_item)) ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                            <div class="oapp__RevokeAccessWrap">
                                <a class="oapp__RevokeAccess">Revoke access</a>
                                <?php echo $areyousure ?>
                            </div>
                        </div>
                    </form>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="oauth2apps_form__boxcontentstatus">none</div>
            <?php endif;?>
        </div>
        <h2>Developed Applications</h2>
        <div class="oapplist--developed oauth2apps_form__boxcontent">
            <?php if (!empty(a_stash('developed_apps'))): ?>
                <?php foreach (a_stash('developed_apps') as $u_app): ?>
                    <div id="dev_<?php xecho($u_app['client_id']); ?>" class="oapp oauth2apps_formbox<?php
                            if (from($_REQUEST, 'created') === $u_app['client_id']) echo ' highlighted';
                            if (from($_REQUEST, 'modified') === $u_app['client_id']) echo ' highlighted'; ?>">
                        <div>
                            <h3 class="oapp__name"><?php xecho($u_app['app_name']);
                            if (isset($u_app['app_is_official']) && $u_app['app_is_official'] === true): ?>
                                <strong class="oapp__IsOfficial">
                                    <i class="icon fa fa-check"></i>
                                    <span>Official Layer7 App</span>
                                </strong>
                            <?php endif; ?></h3>
                            <div class="oapp__cid">consumer_id: <?php xecho($u_app['client_id']); ?></div>
                            <div class="oapp__ModifyBtnGroup">
                                <a class="oapp__ModifyBtnGroup__editButton<?php
                                    if (from($_REQUEST, 'created') === $u_app['client_id'] ||
                                        from($_REQUEST, 'modified') === $u_app['client_id']) echo ' active';
                                    ?>"><?php
                                    if (from($_REQUEST, 'created') === $u_app['client_id'] ||
                                        from($_REQUEST, 'modified') === $u_app['client_id']) echo 'cancel ';
                                    ?>edit</a>
                                <span>&bull;</span>
                                <form method="POST" style="display:inline-block;">
                                    <?php session_csrf_token_field() ?>
                                    <input type="hidden" name="action" value="app_delete" />
                                    <input type="hidden" name="client_id" value="<?php xecho($u_app['client_id']) ?>" />
                                    <a class="oapp__ModifyBtnGroup__deleteButton">delete app</a>
                                    <?php echo $areyousure ?>
                                </form>
                            </div>
                        </div>
                        <form method="POST" class="oapp__edit more<?php
                                if (from($_REQUEST, 'created') !== $u_app['client_id'] &&
                                from($_REQUEST, 'modified') !== $u_app['client_id']) echo ' hide';?>">
                            <?php session_csrf_token_field() ?>
                            <input type="hidden" name="action" value="app_modify" />
                            <input type="hidden" name="client_id" value="<?php xecho($u_app['client_id']) ?>" />
                            <div>
                                <label>
                                    <span>consumer secret</span>
                                    <input type="text" disabled value="<?php xecho($u_app['client_secret']) ?>"/>
                                </label>
                            </div>
                            <div>
                                <label for="app_name">
                                    <span>Name<b class="red spacer5-left">*</b></span>
                                    <input required type="text" name="app_name" value="<?php xecho($u_app['app_name']) ?>"/>
                                </label>
                            </div>
                            <div>
                                <label for="app_desc">
                                    <span>Description</span>
                                    <textarea style="min-height:50px" name="app_desc"><?php
                                        xecho($u_app['app_about']) ?></textarea>
                                </label>
                            </div>
                            <div>
                                <label for="app_about_url">
                                    <span>About URL</span>
                                    <input type="url" name="app_about_url" value="<?php xecho($u_app['app_url']) ?>"/>
                                </label>
                            </div>
                            <div>
                                <label for="app_redirect_uri">
                                    <span>Redirect URI<b class="red spacer5-left">*</b></span>
                                    <input required type="url" name="app_redirect_uri" value="<?php xecho($u_app['redirect_uri']) ?>"/>
                                </label>
                            </div>
                            <div>
                                <label for="app_modify_button" style="padding-top:10px;">
                                    <span></span>
                                    <button type="submit" id="app_modify_button" class="primary">save changes</button>
                                </label>
                            </div>
                        </form>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="oauth2apps_form__boxcontentstatus">none</div>
            <?php endif; ?>
        </div>
        <div class="oapp__CreateAppArea oauth2apps_form__boxcontent">
            <button id="toggle_create_app_form" class="secondary">
                <span>Create new application</span>
            </button>
            <form id="create_app_form" class="oauth2apps_formbox hide" method="POST" >
                <?php session_csrf_token_field() ?>
                <input type="hidden" name="action" value="app_create" />
                <div>
                    <label for="app_name">
                        <span>Name<b class="red spacer5-left">*</b></span>
                        <input required type="text" name="app_name" />
                    </label>
                </div>
                <div>
                    <label for="app_desc">
                        <span>Description</span>
                        <textarea style="min-height:50px" name="app_desc"></textarea>
                    </label>
                </div>
                <div>
                    <label for="app_about_url">
                        <span>About URL</span>
                        <input type="url" name="app_about_url"/>
                    </label>
                </div>
                <div>
                    <label for="app_redirect_uri">
                        <span>Redirect URI<b class="red spacer5-left">*</b></span>
                        <input required type="url" name="app_redirect_uri"/>
                    </label>
                </div>
                <div>
                    <label for="app_create_btn" style="padding-top:10px;">
                        <span></span>
                        <button type="submit" id="app_create_btn" class="primary">create app</button>
                        <button id="app_create_cancel_btn" class="primary cancel">
                            <span>cancel</span>
                        </button>
                        <span class="grow"></span>
                    </label>
                </div>
            </form>
            <a target="_blank" rel="noopener noreferrer" href="<?php echo SITE_URL ?>api">See API Documentation</a>
        </div>
    </div>
</div>
</div>