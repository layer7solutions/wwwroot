<div class="prefs__header">
    <div class="prefs__tabs valign">
        <a class="prefs__tabItem prefs__tabItem--userprefs <?php echo_if('selected', body_class_exists('page--prefs_user')) ?>"
            href="<?php echo SITE_URL ?>prefs">User Options</a>
        <a class="prefs__tabItem prefs__tabItem--subsettings <?php echo_if('selected', body_class_exists('page--prefs_subsettings')) ?>"
            href="<?php echo SITE_URL ?>prefs/subreddit">Subreddit Settings</a>
        <a class="prefs__tabItem prefs__tabItem--apps <?php echo_if('selected', body_class_exists('page--prefs_apps')) ?>"
            href="<?php echo SITE_URL ?>prefs/apps">Apps</a>
        <a class="prefs__tabItem prefs__tabItem--account-activity <?php echo_if('selected', body_class_exists('page--prefs_account-activity')) ?>"
            href="<?php echo SITE_URL ?>prefs/account-activity">Account Activity</a>
        <?php if (body_class_exists('page--prefs_account-data')): ?>
        <a class="prefs__tabItem prefs__tabItem--account-data <?php echo_if('selected', body_class_exists('page--prefs_account-data')) ?>"
            href="<?php echo SITE_URL ?>prefs/account-data">User Data</a>
        <?php endif; ?>
    </div>
</div>
<div class="prefs__body"><?php render_content_id() ?></div>