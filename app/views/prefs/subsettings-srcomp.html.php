<?php
$perms = from(a_stash('srinfo'), 'mod_permissions');
$bot_perms = from(a_stash('srdata'), 'redditbot_permissions');
?>
<?php if ($perms != 'all' && !user()->admin()): ?>
<div class="notice" style="margin-top:0;">
    <p><b>Viewing in Read Only</b> You must have full permissions in order to change these settings.</p>
</div>
<?php endif; ?>
<div class="settings__compHeader">
    <h2>
        <span>Layer7 Settings for:</span>
        <a href="https://www.reddit.com/r/<?php echo x_stash('srname') ?>"
            target="_blank" rel="noopener noreferrer"><?php echo x_stash('srname') ?></a>
    </h2>
    <p class="spacer5">You may want to talk with your team first before changing stuff here.</p>
</div>
<div class="settings__compSection">
    <h3>TSB Settings</h3>
    <?php foreach (u_stash('settings') as $u_item): ?>
        <div class="settingsProp">
            <div class="settingsProp__label">
                <?php if (is_array($u_item['label'])): ?>
                    <div class="settingsProp__label--main"><?php xecho($u_item['label'][0]) ?></div>
                    <div class="settingsProp__label--sub">
                        Requires <code><?php xecho($u_item['label'][1]) ?></code> permission on TheSentinelBot
                    </div>
                <?php else: ?>
                    <?php xecho($u_item['label']); ?>
                <?php endif; ?>
            </div>
            <div class="settingsProp__value">
                <div class="ui-switch">
                    <label>
                        <input type="checkbox"
                            <?php
                                xecho_if('checked',           from(u_stash('srdata'), $u_item['property']));
                                echo " ";
                                xecho_if('disabled readonly', $perms != 'all' && !user()->admin());
                            ?>
                            data-property="<?php echo $u_item['property'] ?>"
                            data-subreddit="<?php echo x_stash('srname') ?>" />
                        <span class="knob"></span>
                    </label>
                </div>
                <i class="icon zmdi zmdi-check-circle save-marker"></i>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div class="settings__compSection">
    <h3>Info</h3>
    <div class="settingsProp">
        <div class="settingsProp__label">Reddit Bot</div>
        <div class="settingsProp__value">
            <span><?php xecho(from(u_stash('srdata'), 'redditbot_name')) ?></span>
            <span class="settingsProp__label--perm">
                perms: <?php xecho(\app\lib\Common::format_reddit_perms($bot_perms)); ?>
            </span>
        </div>
    </div>
    <div class="settingsProp">
        <div class="settingsProp__label">Subreddit Moderators</div>
        <div class="settingsProp__value">
            <a href="https://www.reddit.com/r/<?php echo x_stash('srname') ?>/about/moderators"
                    target="_blank" rel="noopener noreferrer">
                <span>about/moderators <i class="icon zmdi zmdi-open-in-new"></i></span>
            </a>
        </div>
    </div>
    <div class="settingsProp">
        <div class="settingsProp__label">Subreddit Date Created</div>
        <div class="settingsProp__value"><?php
            xecho(timeConvert(from(u_stash('srinfo'), 'created_utc') ?? 'not available', false, true)) ?>
            (UTC)</div>
    </div>
    <div class="settingsProp">
        <div class="settingsProp__label">Your Permissions Here</div>
        <div class="settingsProp__value"><?php xecho(\app\lib\Common::format_reddit_perms($perms)) ?></div>
    </div>
</div>
<div class="settings__compSection">
    <h3>Changelog</h3>
    <?php if (empty(a_stash('changelog'))): ?>
        <p class="spacer10-left">Nothing here.</p>
    <?php else: ?>
    <table class="settingsChangelog primary">
        <thead>
            <tr class="settingsChangelogEntry">
                <th class="settingsChangelogEntry__property">Property</th>
                <th class="settingsChangelogEntry__newValue">New Value</th>
                <th class="settingsChangelogEntry__oldValue">Old Value</th>
                <th class="settingsChangelogEntry__changedUTC">Changed Time (UTC)</th>
                <th class="settingsChangelogEntry__changedBy">Changed By</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach (xa_stash('changelog') as $changelog_entry): ?>
            <tr class="settingsChangelogEntry">
                <td class="settingsChangelogEntry__property">
                    <?php
                    echo ucwords(implode(' ', explode('_', $changelog_entry['setting'])));
                    ?>
                </td>
                <td class="settingsChangelogEntry__newValue">
                    <?php
                    echo bool_unstr($changelog_entry['new_value']) ? '<b>On</b>' : 'Off';
                    ?>
                </td>
                <td class="settingsChangelogEntry__oldValue">
                    <?php
                    echo bool_unstr($changelog_entry['old_value']) ? '<b>On</b>' : 'Off';
                    ?>
                </td>
                <td class="settingsChangelogEntry__changedUTC">
                    <?php
                    echo timeConvert($changelog_entry['changed_utc'], false, true)
                    ?>
                </td>
                <td class="settingsChangelogEntry__changedBy">
                    <?php
                    echo '/u/'.$changelog_entry['changed_by']
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>