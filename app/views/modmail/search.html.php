<div class="ModmailSearch_Wrap">
    <div id="ModmailSearch__Header">
        <h2>Modmail Search <span class="label spacer5-left">BETA</span></h2>
    </div>
    <div id="ModmailSearch__Search"></div>
    <div id="ModmailSearch__Result" class="card hide"></div>
</div>