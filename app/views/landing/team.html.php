<section class="profiles">
    <div class="row first">
        <section class="profile">
            <i class="icon icon-profile zmdi zmdi-account-circle"></i>
            <h5>D0cR3d</h5>
            <h6>Co-founder</h6>
            <p>Founder, Software Developer, creator of Benedict 99-40.</p>
        </section>
    </div>
    <div class="row">
        <section class="profile">
            <i class="icon icon-profile zmdi zmdi-account-circle"></i>
            <h5>kwwxis</h5>
            <h6>Web Developer</h6>
            <p>Full-Stack Web Developer and I should probably put something here.</p>
        </section>
    </div>
</section>