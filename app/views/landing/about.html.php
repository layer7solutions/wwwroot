<section id="logo-section">
    <h1>
        <img src="<?php img_src('brand/logo-white-200x178.png') ?>" />
        <span>Layer 7 Solutions</span>
    </h1>
</section>
<section id="about-us">
    <div class="wrapper">
        <p>Layer 7 is a small, unpaid open source group of moderators with a big vision: making moderating easier. We create
        moderation tools, for moderators, by moderators. We have many different projects going on, all with
        the single purpose of accomplishing the goal of making moderating easy.</p>
    </div>
</section>
<section id="about-subreddits">
    <h2 style="">Serving communities of all sizes.</h2>
    <p>like...</p>
    <div id="tsb-shortlist">
        <span style="border-radius:2px 0 0 0">GTFO</span>
        <span>DestinyReddit</span>
    </div>
    <p>and more!</p>
</section>
<section id="about-team">
    <div class="wrapper no-pad">
        <header>
            <h3 class="landing-header"><span>Meet the Team</span></h3>
            <div class="tagline">
                <p class="first">We're a passionate group of moderators passionate about passionately creating
                mod tools for you to passionately moderate your community, passionately!</p>
            </div>
        </header>
        <?php render_template('landing.team') ?>
    </div>
</section>
<section id="contact-us">
    <div class="wrapper">
        <a id="contact-us-button" href="https://layer7.xyz/benedict">
            <button class="primary big">Contact Us</button>
        </a>
    </div>
</section>