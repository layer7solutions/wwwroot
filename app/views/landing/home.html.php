<header id="header">
    <div class="wrapper cf">
        <div id="header-main-box" class="fl">
            <div id="header--intro">
                <h1><span>Layer 7 Solutions</span></h1>
                <div id="header--description">
                    <p>We're a small unpaid group of developers working on an open source project
                    with the goal of making moderation easier for Redditors.</p>
                </div>
            </div>
            <div id="header--action-box">
                <a class="button" href="<?php echo SITE_URL ?>get-started">Get Started</a>
                <p>Or scroll down to learn more.</p>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div class="widget landing-widget product-promotion-widget">
            <div class="widget-webview">
                <div class="widget-webview-top"><span></span></div>
                <div class="widget-webview-viewport"
                    style="background:url(<?php img_src('landing/home/sample_matrix.png') ?>) no-repeat 0 -2px / 100% auto">
                </div>
            </div>
        </div>
    </div>
</header>
<section id="product-list" class="product-promotion-widget--after">
    <div class="wrapper">
        <div class="product-list-parent">
            <h3 class="landing-header product-list-header"><span>Our Tools</span></h3>
            <div class="product-list">
                <section class="product-card">
                    <div class="product-icon i-bots">
                        <i class="icon zmdi zmdi-settings" style="
                            color: white;
                            margin-left: 90px;
                            font-size: 56px;
                            margin-top: 30px;
                        "></i>
                    </div>
                    <div class="product-info">
                        <p class="title">A Robot Army</p>
                        <p class="description">
                            From The_Vanguard to YT_Killer, Layer 7 has the best
                            breed of Reddit bots for managing your community.
                        </p>
                        <a class="learn-more"></a>
                    </div>
                </section>
                <section class="product-card">
                    <div class="product-icon i-mod-logs">
                        <div class="i-particles">
                            <span class="i-p0"></span>
                            <span class="i-p1"></span>
                            <span class="i-p2"></span>
                            <span class="i-p3"></span>
                            <span class="i-p4"></span>
                        </div>
                        <div class="i-monitor">
                            <div class="i-screen">
                                <div class="i-graph">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                            <div class="i-base">
                                <div class="i-base0"></div>
                                <div class="i-base1"></div>
                                <div class="i-base2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-info">
                        <p class="title">Advanced Analytics</p>
                        <p class="description">
                            Moderation Log falls short, but Layer 7 has the
                            tools to harness your mod data.
                        </p>
                        <a class="learn-more"></a>
                    </div>
                </section>
                <section class="product-card">
                    <div class="product-icon i-mobimod">
                        <div class="i-particles">
                            <span class="i-p0"></span>
                            <span class="i-p1"></span>
                            <span class="i-p2"></span>
                            <span class="i-p3"></span>
                            <span class="i-p4"></span>
                        </div>
                        <div class="i-phone">
                            <div class="i-screen">
                                <div class="i-hammer">
                                    <span class="i-hammer0"></span>
                                    <span class="i-hammer1"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<section id="highlights">
    <div class="wrapper">
        <div class="tagline">Highlights</div>
        <div id="stuff">
            <section>
                <div class="left">
                    <h3>Mod log searching</h3>
                    <p>You can search your moderation logs quickly and precisely: such as inputing a
                    <code>thing id</code> and seeing all actions related to that post or comment.</p>
                </div>
                <div class="right"
                    style="background: url(<?php img_src('landing/home/sample_search.png') ?>) no-repeat bottom right / contain">
                </div>
            </section>
            <section>
                <div class="left posRel">
                    <i class="icon media-icon media-icon--youtube fa fa-youtube"></i>
                    <i class="icon media-icon media-icon--vimeo fa fa-vimeo"></i>
                    <img class="icon media-icon media-icon--vidme" src="<?php img_src('media-host/vidme_icon.svg') ?>" />
                    <img class="icon media-icon media-icon--dailymotion" src="<?php img_src('media-host/dailymotion_icon.svg') ?>" />
                    <i class="icon media-icon media-icon--soundcloud fa fa-soundcloud"></i>
                    <i class="icon media-icon media-icon--twitch fa fa-twitch"></i>
                    <i class="icon media-icon media-icon--twitter fa fa-twitter"></i>
                </div>
                <div class="right">
                    <h3>Blacklist media spam</h3>
                    <p>Prevent known spam channels from posting on your subreddit by adding their
                    channel to a blacklist; any post or comment containing a video or link to that
                    channel will be automatically removed regardless of the Reddit poster.</p>
                </div>
            </section>
            <section>
                <div class="left">
                    <h3>Generate mod matrixes instantaneously</h3>
                    <p>You can build mod action matrixes in an instant - a matrix
                    for the past 2 years of /r/DestinyTheGame takes just 1.5 seconds.</p>
                </div>
                <div class="right"
                    style="background: url(<?php img_src('landing/home/sample_matrixform.png') ?>) no-repeat bottom right / contain">
                </div>
            </section>
        </div>
    </div>
</section>
<section id="team">
    <div class="wrapper no-pad">
        <header>
            <h3 class="landing-header"><span>Our Team</span></h3>
            <div class="tagline">
                <p class="first">We're a small unpaid group of Redditors with one goal: making moderating easier</p>
                <p class="second">With every team member being a moderator,<br/>that goal is very
                near and dear to us.</p>
            </div>
        </header>
        <?php render_template('landing.team') ?>
    </div>
</section>
<section id="join-us">
    <div class="wrapper">
        <h3 class="landing-header"><span>Interested in helping out?</span></h3>
        <section style="margin-top:20px">
            <a href="https://bitbucket.org/layer7solutions/">
                <button class="primary big">Our Bitbucket</button>
            </a>
            <a href="https://www.patreon.com/Layer7">
                <button class="primary big">Our Patreon*</button>
            </a>
        </section>
        <p style="font-size: 14px;margin-top: 10px;">* - all donations go to our server hosting costs firstmost</p>
    </div>
</section>
<component is="style">
.left .media-icon.media-icon--youtube {
    position: absolute;
    font-size: 40px;
    left: 170px;
    top: 90px;
}
.left .media-icon.media-icon--vimeo {
    position: absolute;
    font-size: 40px;
    left: 70px;
    top: 90px;
}

.left .media-icon.media-icon--vidme {
    position: absolute;
    left: 218px;
    top: 150px;
    width: 35px;
    height: 35px;
}

.left .media-icon.media-icon--dailymotion {
    position: absolute;
    font-size: 40px;
    left: 270px;
    top: 84px;
    width: 35px;
    height: 35px;
}

.left .media-icon.media-icon--soundcloud {
    position: absolute;
    font-size: 40px;
    left: 210px;
    top: 30px;
}

.left .media-icon.media-icon--twitch {
    position: absolute;
    font-size: 40px;
    left: 118px;
    top: 160px;
}

.left .media-icon.media-icon--twitter {
    position: absolute;
    font-size: 40px;
    left: 120px;
}
</component>