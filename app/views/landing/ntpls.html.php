<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="shortcut icon" href="/favicon.ico?v=2.4" type="image/x-icon">
    <title>nt pls</title>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open%20Sans:300italic,400italic,600italic,700italic,300,400,600,700|Source%20Sans%20Pro:300,400,600,700,300italic,400italic,600italic,700italic&ver=4.0-alpha" crossorigin="anonymous" />
<style>
* {
    padding: 0;
    margin: 0;
}
body {
    font-family: 'Open Sans', Arial, sans-serif;
    color: hsla(232,20%,20%,0.95);
}

#ntpls-0, #ntpls-1 {
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -ms-backface-visibility: hidden;
    backface-visibility: hidden;
    animation: ntpls 20s linear infinite reverse, ntopacity 200ms ease-out forwards;
    animation-delay: 2s;
    opacity: 0;
}

#ntpls-1 {
    margin-top: 34053px;
    position: absolute;
}

@-webkit-keyframes ntpls {
    100% {
        transform: translateY(-100%);
    }
}
@-moz-keyframes ntpls {
    100% {
        transform: translateY(-100%);
    }
}
@-ms-keyframes ntpls {
    100% {
        transform: translateY(-100%);
    }
}
@keyframes ntpls {
    100% {
        transform: translateY(-100%);
    }
}


@-webkit-keyframes ntopacity {
    100% {
        opacity: 1;
    }
}
@-moz-keyframes ntopacity {
    100% {
        opacity: 1;
    }
}
@-ms-keyframes ntopacity {
    100% {
        opacity: 1;
    }
}
@keyframes ntopacity {
    100% {
        opacity: 1;
    }
}

#nt337 {
    position: absolute;
    z-index: 5;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 200px;
    height: 150px;
    margin: auto;
    transform: translateY(-25%);
}
#nt337 h1 {
    letter-spacing: 5px;
    color: white;
    text-align: center;
    text-shadow:
    -2px -2px 0 #000,
    -2px -2px 0 #000,
     1px -2px 0 #000,
     1px -2px 0 #000,
    -1px 1px 0 #000,
     1px 1px 0 #000, 0 1px 5px rgba(0,0,0,0.9);
    transform: scale(0);
    animation: nt337_box 200ms ease-out forwards;
    animation-delay: 2.1s;
    font-style: italic;
}

#nt337-box {
    width: 140px;
    height: 140px;
    position: relative;
    background: #fff;
    border: 2px solid #000;
    box-shadow: 0 1px 20px rgba(0,0,0,0.25), inset 0 0px 5px rgba(0,0,0,0.25);
    margin: 0 auto;
    transform: scale(0);
    animation: nt337_box 300ms ease-out forwards;
    animation-delay: 2s;
}
#nt337-box img {
    width: 100px;
    height: 100px;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    margin: auto;
}

@-webkit-keyframes nt337_box {
    90% {
        transform: scale(1.05);
    }
    100% {
        transform: scale(1);
    }
}
@-moz-keyframes nt337_box {
    90% {
        transform: scale(1.05);
    }
    100% {
        transform: scale(1);
    }
}
@-ms-keyframes nt337_box {
    90% {
        transform: scale(1.05);
    }
    100% {
        transform: scale(1);
    }
}
@keyframes nt337_box {
    90% {
        transform: scale(1.05);
    }
    100% {
        transform: scale(1);
    }
}

footer {
    position: absolute;
    z-index: 2;
    top: 5px;
    right: 5px;
    bottom: 5px;
    left: 5px;
}

footer .box {
    border: 2px solid black;
    position: absolute;
    display: block;
    width: 0%;
    height: 0%;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    margin: auto;
    overflow: hidden;
    animation: footer_box 900ms ease-out forwards;
}
@-webkit-keyframes footer_box {
    50% {
        height: 100%;
        width: 0%;
    }
    100% {
        height: 100%;
        width: 100%;
    }
}
@-moz-keyframes footer_box {
    50% {
        height: 100%;
        width: 0%;
    }
    100% {
        height: 100%;
        width: 100%;
    }
}
@-ms-keyframes footer_box {
    50% {
        height: 100%;
        width: 0%;
    }
    100% {
        height: 100%;
        width: 100%;
    }
}
@keyframes footer_box {
    50% {
        height: 100%;
        width: 0%;
    }
    100% {
        height: 100%;
        width: 100%;
    }
}
    </style>
</head>
<body>
    <header id="nt337">
        <div id="nt337-box">
            <img src="<?php img_src('web/misc/nt337.jpg') ?>" />
        </div>
        <h1>nt337</h1>
    </header>
<div style="position:absolute;height:100vh;width:100vw;overflow:hidden">
    <?php for ($j = 0; $j < 2; $j++): ?>
    <div id="ntpls-<?php echo $j ?>" style="width:20000px;height:34053px">
        <?php
        $fsize = 1;
        $fsizedir = 1;
        for ($i = 1; $i <= 1000; $i++):
            $color = 'hsl(' . ((($i % 250) / 250) * 255) . ', 50%, 50%)';
        ?>
        <div class="ntpls-<?php echo $j ?>-<?php echo $i ?>"
                style="font-size:<?php echo $fsize; ?>px;display:-webkit-flex;display:flex;color:<?php echo $color ?>">
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
            <span>nt pls&nbsp;</span>
        </div>
        <?php
            if ($fsize === 50) {
                $fsizedir = -1;
            }
            if ($fsize === 0) {
                $fsizedir = 1;
            }
            $fsize += $fsizedir;
        endfor; ?>
    </div>
    <?php endfor; ?>
</div>
    <footer>
        <div class="box"></div>
    </footer>
</body>
</html>