<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="shortcut icon" href="/favicon.ico?v=2.4" type="image/x-icon">
    <title>404 Not found</title>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open%20Sans:300italic,400italic,600italic,700italic,300,400,600,700|Source%20Sans%20Pro:300,400,600,700,300italic,400italic,600italic,700italic&ver=4.0-alpha" crossorigin="anonymous" />
    <style>
    * {
        padding: 0;
        margin: 0;
    }

    .wrapper {
        max-width: 400px;
        box-sizing: border-box;
        padding: 10px;
        margin: 0 auto;
    }
    #http-error {
        margin-top: 100px;
    }
    #tagline {
        font-family: 'Open Sans', Arial, sans-serif;
        margin-top: 20px;
        margin-bottom: 10px;
        color: hsla(232,20%,10%,0.7);
    }
    #tagline strong {
        font-weight: 600;
        color: hsla(232,20%,30%,1);
    }
    #description {
        font-size: 14px;
        font-family: 'Source Sans Pro', 'Open Sans', Arial, sans-serif;
        color: hsla(232,20%,10%,0.6);
    }
    </style>
</head>
<body>
    <div id="http-error" class="wrapper">
        <p id="tagline"><strong>404.</strong> Not found.</p>
        <p id="description">This <strong>short link</strong> has been deleted by its creator.</p>
    </div>
</body>
</html>