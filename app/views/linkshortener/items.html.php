<?php foreach (xa_stash('listing') as $item):
    $short_link = SITE_XYZ . $item['short_id'];
    ?>
    <tr class="ShortLinkRow<?php echo_if(' flash', b_stash('flashitems')) ?>"
            data-short-id="<?php echo $item['short_id'] ?>"
            data-short-link="<?php echo $short_link ?>"
            data-value="<?php echo $item['original_url'] ?>">
        <td class="ShortLinkRow__originalLink">
            <div class="valign ShortLinkRowButtons ShortLinkRowButtons--normal">
                <span class="posRel grow valign">
                    <a href="<?php echo $item['original_url'] ?>"
                        title="<?php echo $item['original_url'] ?>"><?php echo $item['original_url'] ?></a>
                </span>
                <button class="ShortLinkRow__editLinkButton tertiary small">edit</button>
            </div>
            <div class="valign ShortLinkRowButtons ShortLinkRowButtons--inEdit">
                <span class="posRel grow">
                    <input type="text" value="<?php echo $item['original_url'] ?>" />
                </span>
                <button class="ShortLinkRow__saveEditButton tertiary small">save</button>
                <button class="ShortLinkRow__cancelEditButton tertiary small">cancel</button>
                <div class="loading small spacer5-left hide"></div>
            </div>
        </td>
        <td class="ShortLinkRow_createdUTC" title="<?php echo timeConvert($item['created_utc']) ?>"
                style="padding-right:0">
            <span><?php echo human_timing($item['created_utc']) ?></span>
        </td>
        <td class="ShortLinkRow__link">
            <div class="valign">
                <span class="posRel grow">
                    <a href="<?php echo $short_link ?>"><?php echo $short_link ?></a>
                </span>
                <button class="ShortLinkRow__copyLinkButton tertiary small">copy</button>
            </div>
        </td>
        <td class="ShortLinkRow__linkHits">
            <span><?php echo number_format($item['hits']); ?></span>
        </td>
        <td class="ShortLinkRow__more">
            <div class="dropdown--trigger">
                <i class="zmdi zmdi-more-vert"></i>
                <div class="dropdown" style="min-width: 80px">
                    <a class="ShortLinkRow__deleteButton">
                        <i class="icon zmdi zmdi-delete"></i>
                        <div class="loading xsmall spacer5-right hide"></div>
                        <span>Delete</span>
                    </a>
                </div>
            </div>
        </td>
    </tr>
<?php endforeach; ?>