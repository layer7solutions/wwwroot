<div class="LinkShortener">
    <div class="LinkShortener__NewBox">
        <div class="wrapper">
            <h1>URL Shortener<small>beta</small></h1>
            <div class="LinkShortenForm">
                <input class="LinkShortenForm__originalURL" type="url" placeholder="Your original URL here" />
                <button class="LinkShortenForm__submitButton primary primary--2" type="submit">SHORTEN URL</button>
                <span class="LinkShortenForm__pendingIcon loading loading--white small spacer10-left hide"></span>
            </div>
        </div>
        <div class="LinkShortenerFormErrorList">
            <div class="wrapper">
                <p class="LinkShortenerFormError LinkShortenerFormError--emptyURL hide">
                    You need to enter a URL.
                </p>
                <p class="LinkShortenerFormError LinkShortenerFormError--invalidURL hide">
                    Not a valid URL, make sure it begins with "https://" or "http://".
                </p>
            </div>
        </div>
    </div>
    <div class="LinkShortener__CurrentBox">
        <div class="wrapper">
            <div class="card">
                <div>
                    <table class="ShortenedLinkTable primary">
                        <thead class="ShortenedLinkTable__head">
                            <tr>
                                <th>Original URL</th>
                                <th style="width:110px;padding-right:0">Created</th>
                                <th style="width:250px">Short URL</th>
                                <th style="width:100px">All Clicks</th>
                                <th style="padding-left:0">More</th>
                            </tr>
                        </thead>
                        <tbody class="ShortenedLinkTable__body"><?php
                            render_template('linkshortener.items', [
                                'listing' => stash('linkdata')
                            ]);
                        ?></tbody>
                    </table>
                    <?php if (empty(u_stash('linkdata'))): ?>
                        <div class="NoShortenedLinks output-status">No links yet</div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<component is="style">
.LinkShortener__NewBox:before {
    background: url(<?php img_src('web/bg/geometry.png') ?>);
}
</component>