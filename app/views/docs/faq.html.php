<article>
    <header>
        <h1 data-doctitle="FAQ | Layer7">Frequently Asked Questions</h1>
        <p>Note: this FAQ is a work in progress.</p>
    </header>
    
    <!--
    <div id="toc" class="no-fixed split">
        <section data-title="In this Article">
            <a href="#general">General Questions</a>
            <section>
                <a href="#are-your-services-free">Are your services free?</a>
            </section>
        </section>
    </div>-->
    <!--
    <hr class="large-separator" />
    <div class="doc-header cf"><h1>General Questions</h1></div>
    -->
    
    <section>
        <h3>Are your services free?</h3>
        <p>Yup, all our services have always been free and always will be.</p>
        <p>But we'd love any donations, all of which will go to keeping TheSentinelBot
        and this website alive (server cost are expensive!).<br/>
        <a href="https://www.paypal.me/D0cR3d">Click here to donate.</a>
    </section>
    
    <section>
        <h3>What information do you collect from my
        Reddit account and the subreddits I mod?</h3>
        
        <b>Your Reddit Account</b>
        
        <section class="function">
            <p>The only information we collect from your Reddit account is:
            your username, the subs you mod, and what permissions you have on those subreddits.</p>
        </section>
        
        <b>The Subreddits you mod</b>
        <section class="function">
            <p>As for information we collect on your subreddits: we store basic information about them such as:
            the date it was created, the sidebar text, description, subreddit name, number of subscribers.</p>
            <p>We also store extra information depending on what features you have enabled. So if your subreddit
            has mod logging enabled then we also store all your mod logs. If you have modmail logging enabled, we
            store your modmail.</p>
        </section>
        
        <b>Why</b>
        
        <section class="function">
            <p>We only use this information to provide, improve, and protect our services. We do not
            hand out or sell any of this information to anyone.</p>
        </section>
        
        <a href="<?php echo SITE_URL ?>docs/privacy">See our full privacy policy for more information</a>
    </section>
    
    <section>
        <h3>Can all moderators in my team change the Layer7 subreddit settings?</h3>
        
        <p>Only moderators with full permissions can change the settings to enable/disable
        Layer7 features such as mod logging.</p>
    </section>
    
    <section>
        <h3>How does global blacklisting work? Who manages the global blacklist?</h3>
        <p>We only add channels to the global blacklist when the channel is being spammed by user(s) who are in
        major violation of the 9:1 policy (more than 10%, dedicated to self promo) which we usually look at
        around 50% or more. In addition, it has to be spam that is happening in multiple subreddits, and not
        just one that could add to their local blacklist. We look for patterns to see if a channel is connected
        to multiple reddit accounts that aren't just casually sharing it, but making an effort to
        have a personal gain in that channel. In addition there are other factors that could weigh in such as very
        little subscribers, channel names that sound like spam, videos that are low quality and clickbait,
        stolen/rehosted content, as well as monetized all play a factor. If in doubt we err on the side of caution</p>
        <p>The global blacklist is managed by the moderators of /r/Layer7 and /r/TheSentinelBot</p>
</article>