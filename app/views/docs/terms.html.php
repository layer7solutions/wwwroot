<article>
    <header>
        <h1 data-doctitle="Terms and Conditions | Layer7">Layer 7 Solutions - Terms and Conditions</h1>
        <p>Modified: March 5th, 2022.<br/>
        Effective: March 5th, 2022.</p>
    </header>
    <div id="toc" class="no-fixed split">
        <section data-title="Terms and Conditions">
            <a href="#responsibilities">Responsibilities</a>
            <section>
                <a href="#acceptable_use_policy">Acceptable Use Policy</a>
                <a href="#note_on_privacy">Note on Privacy</a>
                <a href="#api_usage_rules">API Usage Rules</a>
            </section>
            <a href="#third_party_links">Third Party Links</a>
            <a href="#usage_of_bots">Usage of our Bots</a>
            <a href="#termination">Termination</a>
            <a href="#beta_services">Beta Services</a>
            <a href="#some_legalities">Some Legalities</a>
            <section>
                <a href="#warranty_and_condition_disclaimer">Warranty and Condition Disclaimer</a>
                <a href="#limitation_of_liability">Limitation of Liability</a>
                <a href="#indemnity">Indemnity</a>
            </section>
            <a href="#ability_to_accept_terms_of_service">Ability to Accept Terms of Service</a>
            <a href="#more_legalities">More Legalities</a>
            <section>
                <a href="#governing_law">Governing Law</a>
                <a href="#severability_and_enforcement">Severability and Enforcement</a>
                <a href="#assignment">Assignment</a>
                <a href="#modifications">Modifications</a>
                <a href="#entire_agreement">Entire Agreement</a>
            </section>
            <a href="#contact_us">Contact Us</a>
        </section>
        <?php render_template('docs.templates.terms_versions'); ?>
        <?php render_template('docs.templates.legaldocs_list'); ?>
    </div>
    <section style="padding-bottom: 0;">
        <p>Layer 7 Solutions, LLC ("Layer 7 Solutions", "Layer7 Solutions", "us", "we", or "our")
        operates our website, software, Discord & Reddit bots, mobile applications, and services ("Service", "Services").</p>

        <p>By using any of our bots, logging into our website, using our mobile applications,
        using any other products or software developed or owned by us, or accessing
        any content or material that is made available by us, you and anyone you've made our services
        available to such as by enabling the bot in your Discord Server who engage in our "Service"
        agree to be bound by the following terms and conditions ("Terms of Service", "Terms of Use", "Terms"),
        including those additional terms and conditions and policies referenced herein and/or available by
        hyperlink. These Term of Service apply to all users who engage in our "Service".</p>

        <p><strong>Please read these Terms of Service carefully</strong> before accessing or using our
        Services. By using our Services, you agree to be bound by these Terms of Service. If you do
        not agree to all the terms and conditions of this agreement, then you should not use any of
        our Services.</p>

        <div class="box">This is an example of a summary.
        These summaries are not legally binding, but they give you a short version of the terms.
        Summaries may appear highlighted throughout our Terms of service.</div>
    </section>
    <section class="function">
        <h2 id="responsibilities">Responsibilities</h2>

        <div class="box">Don't do bad stuff. Be responsible and use common sense.</div>

        <p>You're responsible for your conduct; we take no responsibility for, we do not expressly or
        implicitly endorse, and we do not assume any liability for any behavior or actions of any
        user of our Services or the misusage of any of our Services. We may review your conduct
        for compliance with our Terms of Service. With that said, we have no obligation to do so. </p>

        <p>In your use of our website and Services, you must comply with the terms and conditions of
        these Terms of Service, and all applicable local, national, and international laws and regulations.</p>

        <h3 id="acceptable_use_policy">Acceptable Use Policy</h3>

        <p>You agree not to misuse our Services or help anyone else to do so. For example, you must not
        even try to do any of the following in connection with our Services:</p>

        <ul>
            <li>attempt to or do anything that breaks or is contrary to the platform ("Discord", "Reddit")
            our services use Terms of Service, Community Guidelines, etc.</li>
            <li>using our Services to attempt to interfere with the serving of that platform, make it
            difficult for anyone else to use the platform due to your use of our Services, or otherwise
            attempt to misuse or help anyone misuse the platform in any way.</li>
            <li>attempt to interfere with the serving of our Services, introduce malicious code into
            our Services, make it difficult for anyone else to use our Services due to your actions,
            our otherwise attempt to misuse or help anyone misuse our Services in any way.</li>
            <li>access, tamper with, or use non-public areas or parts of our Services</li>
            <li>probe, scan, or test the vulnerability of any system or network of our Services</li>
            <li>attempt to sell our Services unless specifically authorized to do so</li>
            <li>advocate bigotry or hatred against any person or group of people based on their race,
            religion, ethnicity, sex, gender identity, sexual preference, disability, or impairment</li>
            <li>publicly publish any posts, comments, or user content that has been deleted by the
            author thereof without their explicit permission</li>
            <li>violate or threaten to violate the privacy or infringe the rights of others</li>
            <li>violate the law in any way</li>
        </ul>

        <h3 id="note_on_privacy">Note on Privacy</h3>
        <p>You may not violate the privacy of another user or post personally identifying information (PII)
        on another user without their explicit permission.</p>

        <h3 id="api_usage_rules">API Usage</h3>
        <p>We provide a public API for some of our services that you may use provided that you abide by the following:</p>
        <ul>
            <li>you agree to follow our Terms of Service</li>
            <li>you agree to follow our Acceptable Use Policy</li>
            <li>you agree to follow any provided API Usage Rules, and if no rules or ratelimits are defined, you agree to use your best reasonable discretion</li>
            <li>you agree to not spam, DDOS, or otherwise overload our systems</li>
            <li>violate the law in any way</li>
        </ul>
    </section>
    <section class="function">
        <h2 id="third_party_links">Third Party Links</h2>
        <p>Our services may contain links to third party websites that are not owned or
        controlled by Layer7 Solutions. Layer7 Solutions has no control over, and assumes no responsibility
        for, the content, privacy policies, or practices of any third party websites. In addition,
        Layer7 Solutions will not and cannot censor or edit the content of any third-party site. By using
        our services, you expressly relieve Layer7 Solutions from any and all liability arising from your
        use of any third-party website.</p>
    </section>
    <section class="function">
        <h2 id="usage_of_bots">Usage of our Bots</h2>
        <div class="box">If you or any other moderator added any of our bots accidentally or without
        consulting the rest of your staff team, then that's your problem.</div>
        <p>By employing any of our bot(s) in your community, that is to add the bot as a moderator to
        that subreddit's moderator team or Discord server, you agree that you have the authority within your moderator
        team (based on the consent of your co-moderators) to enter your entire moderator team into
        binding contract with us through these Terms of Service. By the "Warranty and Condition Disclaimer"
        and "Limitation of Liability" sections of these Terms, we accept no responsibility or liability,
        and we provide no warranty, should you not have had the necessary authority within your staff team
        or should the bot have been accidentally added.</p>
    </section>
    <section class="function">
        <h2 id="termination">Termination</h2>
        <div class="box">We can restrict, suspend or terminate your ability to use any of our services at our discretion, without need for warning.</div>

        <p>Layer7 Solutions reserves the right to restrict, suspend or terminate any or all Services,
        or any aspect of a Service, for any or no reason, and at any time without prior notice or liability;
        giving an advance notice or warning does not constitute a waiver of this right. Users may be
        given but are not guaranteed any sort of prior notice or reasonable explanation.</p>

        <p>Layer7 Solutions reserves the right to discontinue any aspect of our Website or Services
        at any time with or without advance notice.</p>

        <p>All provisions of our Terms of Service which by their nature should survive termination shall survive
        termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity
        and limitations of liability.</p>
    </section>
    <section class="function">
        <h2 id="beta_services">Beta Services</h2>

        <div class="box">Beta Services might break, so you should keep that in mind</div>

        <p>We may sometimes release products, modules or features that we are still testing and evaluating.
        Those Services have been marked beta, preview, early access, or evaluation (or with words or phrases
        with similar meanings) and may not be as reliable as our other services, so please keep that in mind.</p>
    </section>
    <section class="function">
        <h2 id="some_legalities">Some Legalities</h2>

        <h3 id="warranty_and_condition_disclaimer">Warranty and Condition Disclaimer</h3>
        <div class="box">We do our best to make sure our Services work as expected, but stuff happens.</div>
        <p style="font-size: 14px;line-height: 20px;">YOU AGREE THAT YOUR USE OF Layer7 Solutions
        SERVICES SHALL BE AT YOUR SOLE RISK. Layer7 Solutions AND ITS SERVICES ARE PROVIDED "AS IS" AND
        WITHOUT WARRANTY OF ANY KIND. TO THE FULLEST EXTENT PERMITTED BY LAW, Layer7 Solutions, ITS STAFF,
        OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, VOLUNTEERS, INTERNS AND THIRD PARTY SERVICE PROVIDERS
        DISCLAIM ALL WARRANTIES AND CONDITIONS, WHETHER WRITTEN, ORAL, EXPRESS, IMPLIED, LEGAL, STATUTORY,
        CONTRACTUAL, EXTRA-CONTRACTUAL, DELICTUAL OR IN TORT, AND WHETHER ARISING BY LAW, STATUTE, USAGE
        OF TRADE, CUSTOM, COURSE OF DEALING OR PERFORMANCE, OR THE PARTIES' CONDUCT OR COMMUNICATION WITH
        ONE ANOTHER, OR AS A RESULT OF THE NATURE OF THESE TERMS AND CONDITIONS, THE SERVICES AND YOUR USE
        THEREOF OR IN CONFORMITY WITH USAGE, EQUITY OR LAW OR OTHERWISE.<br/><br/>

        Layer7 Solutions MAKES NO WARRANTIES, CONDITIONS OR REPRESENTATIONS ABOUT THE ACCURACY OR
        COMPLETENESS OF THIS SITE'S CONTENT OR THE CONTENT OF ANY SITES LINKED TO THIS SITE AND ASSUMES
        NO LIABILITY OR RESPONSIBILITY FOR ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT,
        (II) PERSONAL INJURY, PROPERTY, BODILY, MORAL OR MATERIAL DAMAGE OF ANY NATURE WHATSOEVER,
        RESULTING FROM YOUR ACCESS TO AND USE OF OUR SERVICES, (III) ANY UNAUTHORIZED ACCESS TO OR USE
        OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION
        STORED THEREIN, (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR SERVICES,
        (IV) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH
        OUR SERVICES BY ANY THIRD PARTY, AND/OR (V) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR
        ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED,
        TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA Layer7 Solutions SERVICES.<br/><br/>

        Layer7 Solutions EXPRESSLY DISCLAIMS ANY WARRANTY OR CONDITION OF ANY KIND OF FITNESS FOR A
        PARTICULAR OR GENERAL PURPOSE, QUALITY, MERCHANTABILITY, WORKMANSHIP, NON-INFRINGEMENT, OR
        TITLE AND OWNERSHIP. Layer7 Solutions DOES NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME
        RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH
        Layer7 Solutions SERVICES OR ANY HYPERLINKED WEBSITE OR FEATURED IN ANY BANNER OR OTHER
        ADVERTISING, AND Layer7 Solutions WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR
        MONITORING ANY TRANSACTION BETWEEN YOU AND THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES.
        AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU
        SHOULD USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE. TO THE MAXIMUM EXTENT
        PERMITTED BY LAW, THE PROVISIONS OF THE UNITED NATIONS CONVENTION ON CONTRACTS FOR THE
        INTERNATIONAL SALE OF GOODS ARE HEREBY DISCLAIMED.</p>

        <h3 id="limitation_of_liability">Limitation of Liability</h3>
        <div class="box">We're not liable if anything goes wrong or if any of our Services break.</div>
        <p style="font-size: 14px;line-height: 20px;">IN NO EVENT SHALL Layer7 Solutions, ITS STAFF,
        OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, VOLUNTEERS, INTERNS OR THIRD PARTY SERVICE PROVIDERS BE LIABLE
        TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES
        WHATSOEVER RESULTING FROM ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, (II) PERSONAL
        INJURY, PROPERTY, BODILY, MORAL OR MATERIAL DAMAGE OF ANY NATURE WHATSOEVER, RESULTING FROM
        YOUR ACCESS TO AND USE OF OUR SERVICES, (III) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE
        SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN,
        (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR SERVICES, (IV) ANY BUGS,
        VIRUSES, TROJAN HORSES, OR THE LIKE, WHICH MAY BE TRANSMITTED TO OR THROUGH OUR SERVICES BY
        ANY THIRD PARTY, AND/OR (V) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE
        OF ANY KIND INCURRED AS A RESULT OF YOUR USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR
        OTHERWISE MADE AVAILABLE VIA Layer7 Solutions SERVICES, WHETHER BASED ON WARRANTY, CONTRACT,
        TORT, OR ANY OTHER LEGAL THEORY, AND WHETHER ARISING BY LAW, STATUTE, USAGE OF TRADE, CUSTOM,
        COURSE OF DEALING OR PERFORMANCE, OR THE PARTIES' CONDUCT OR COMMUNICATION WITH ONE ANOTHER,
        OR AS A RESULT OF THE NATURE OF THESE TERMS AND CONDITIONS OR IN CONFORMITY WITH USAGE, EQUITY
        OR LAW OR OTHERWISE, AND WHETHER OR NOT THE COMPANY IS ADVISED OF THE POSSIBILITY OF SUCH
        DAMAGES. THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY
        LAW IN THE APPLICABLE JURISDICTION. YOU SPECIFICALLY ACKNOWLEDGE THAT Layer7 Solutions SHALL NOT
        BE LIABLE FOR USER SUBMISSIONS OR THE DEFAMATORY, OFFENSIVE, OR ILLEGAL CONDUCT OF ANY THIRD
        PARTY AND THAT THE RISK OF HARM OR DAMAGE FROM THE FOREGOING RESTS ENTIRELY WITH YOU.
        The Services are operated by Layer7 Solutions from its facilities in the United States of America.
        Layer7 Solutions makes no representations that the Layer7 Solutions Services is appropriate or
        available for use in other locations. Those who access or use the Layer7 Solutions Services
        from other jurisdictions do so at their own volition and are responsible for compliance
        with local law.</p>

        <h3 id="indemnity">Indemnity</h3>
        <div class="box">You will not hold us legally liable for any of your content or actions that
        infringe the law or the rights of a third party or person in any way.</div>
        <p>To the fullest extent permitted by applicable law, you agree to defend, indemnify and hold
        harmless Layer7 Solutions, its staff, officers, directors, employees, agents, volunteers,
        interns, and third party service providers, from and against any and all claims, demands,
        causes of action, damages, obligations, losses, liabilities, costs or debt, and expenses,
        (including but not limited to reasonable legal fees) arising from: (i) your use of and access to
        Layer7 Solutions Services; (ii) your violation of any term of these Terms of Service; (iii) your
        violation of any third party right, including without limitation any copyright, intellectual property,
        or privacy right; or (iv) any claim that your usage of our Services caused damage to a third party.
        This defense and indemnification obligation will survive these Terms of Service and your use
        of Layer7 Solutions Services.</p>
    </section>
    <section>
        <h2 id="ability_to_accept_terms_of_service">Ability to Accept Terms of Service</h2>
        <div class="box">You must be at least 13 years old to use our services.</div>
        <p>You affirm that you are either more than the age of majority in your jurisdiction of residence,
        or an emancipated minor, or possess legal parental or guardian consent, and are fully able and
        competent to enter into the terms, conditions, obligations, affirmations, representations, and
        warranties set forth in these Terms of Service, and to abide by and comply with these Terms of
        Service. You affirm that you are over the age of 13, as Layer7 Solutions and its
        Services are not directed at people under the age of 13.</p>
    </section>
    <section class="function">
        <h2 id="more_legalities">More Legalities</h2>

        <h3 id="governing_law">Governing Law</h3>
        <div class="box">Any disputes with us must be resolved in Austin under Texas law.</div>
        <p>You agree that: (i) the Layer7 Solutions Services shall be deemed solely based in Texas; and
        (ii) the Layer7 Solutions Website shall be deemed a passive website that does not give rise to
        personal jurisdiction over Layer7 Solutions, either specific or general, in jurisdictions other
        than Texas. (iii) Unless prohibited by local law, these Terms of Service shall be governed by the
        internal substantive laws of the State of Texas, without respect to its conflict of laws principles.
        Unless prohibited by local law, any claim or dispute between you and Layer7 Solutions that arises
        in whole or in part from Layer7 Solutions Services shall be decided exclusively by a court of
        competent jurisdiction located in Austin, Texas, and you agree to waive any objection to
        the laying of venue there.</p>

        <h3 id="severability_and_enforcement">Severability and Enforcement</h3>
        <div class="box">If we do not enforce any right or provision in this user agreement,
        that is not to be deemed a waiver of our right to do so in the future.</div>
        <p>If any provision of these Terms of Service is deemed invalid by a court of competent jurisdiction,
        the invalidity of such provision shall not affect the validity of the remaining provisions of these
        Terms of Service, which shall remain in full force and effect. No waiver of any term of this these
        Terms of Service shall be deemed a further or continuing waiver of such term or any other term, and
        Layer7 Solutions's failure to assert any right or provision under these Terms of Service shall not
        constitute a waiver of such right or provision. TO THE MAXIMUM EXTENT PERMITTED BY LOCAL LAW, YOU
        AND Layer7 Solutions AGREE THAT ANY CAUSE OF ACTION ARISING OUT OF OR RELATED TO Layer7 Solutions
        SERVICES MUST COMMENCE WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES. OTHERWISE, SUCH CAUSE OF
        ACTION IS PERMANENTLY BARRED.</p>

        <h3 id="assignment">Assignment</h3>
        <p>These Terms of Service, and any rights and licenses granted hereunder, may not be transferred
        or assigned by you, but may be assigned by Layer7 Solutions without restriction.</p>

        <h3 id="modifications">Modifications</h3>
        <div class="box">We can change our Terms of Service at any time, but we'll tell you if there any
        changes you'd be worried about.</div>
        <p>Layer7 Solutions reserves the right to amend these Terms of Service at any time. If we make
        changes to this agreement that materially affect your rights, we will provide advance notice and
        keep this edition available as an archive on the Layer7 Solutions website. Your use of any
        Layer7 Solutions Services following any amendment or change to these Terms of Service will signify your
        assent to and acceptance of its revised terms.</p>

        <h3 id="entire_agreement">Entire Agreement</h3>
        <div class="box">These terms are the final word on our policies.</div>
        <p>These Terms of Service, together with the Privacy Notice at <a href="<?php echo SITE_URL ?>privacy"><?php echo SITE_URL ?>privacy</a> and
        any other legal documents and notices found in the sidebar section "LEGAL DOCUMENTS AND NOTICES" of
        this page, shall constitute the entire agreement between you and Layer7 Solutions concerning
        Layer7 Solutions Services. It supersedes all prior or contemporaneous agreements between you and us.<br/><br/>

        Any ambiguities in the interpretation of these Terms of Service shall not be construed against
        the drafting party.</p>
    </section>
    <section>
        <h2 id="contact_us">Contact Us</h2>
        <div class="box">If you have any questions about these Terms, please contact us.</div>
    </section>
</article>