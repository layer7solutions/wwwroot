<div class="doc-header">
    <h1>Developers //</h1>
    <a href="<?php echo SITE_URL ?>prefs/apps">create an app</a>
    <span>&nbsp;|&nbsp;</span>
    <a href="<?php echo SITE_URL ?>developers">api documentation</a>
</div>
<article>
    <header>
        <h1 id="reference">API Reference</h1>

        <p class="notice">This page is under construction.</p>

        <p>Welcome to the Layer7 Solutions API reference. We provide an API for you to use for your
        own subreddit-specific applications or anything else you wish. Using our API you can manage
        your TheSentinelBot blacklist, fetch mod matrices, mod logs, and more.</p>
        <p>Have a question or need help? Try asking in our subreddit at <a href="https://www.reddit.com/r/Layer7">/r/Layer7</a></p>
    </header>
    <div id="toc" class="no-fixed split">
        <section data-title="In this Article">
            <a href="#reference">API Reference</a>
            <section>
                <a href="#overview">Overview</a>
                <a href="#authentication">Authenticating</a>
                <a href="#rules">API Usage Rules</a>
                <a href="#python_integration">Python Integration</a>
                <a href="#subreddit_notation">Subreddit Notation</a>
            </section>
        </section>
        <section data-title="API Methods by Scope">
            <span>identity</span>
            <section>
                <a href="#identity">/identity</a>
                <a href="#identity_subreddits">/identity/subreddits</a>
                <a href="#identity_websync_history">/identity/websync_history</a>
            </section>
            <span>logsread</span>
            <section>
                <a href="#logs_view">/logs/view</a>
            </section>
            <span>srprefread/srprefedit</span>
            <section>
                <a href="#subreddit_settings">/r/{srname}/settings</a>
            </section>
        </section>
    </div>
    <section class="function">
        <h2 id="overview">Overview</h2>

        <p>The web API takes the form <code><?php echo SITE_URL ?>api/v1/METHOD</code>.<br/>
        For example, <code><?php echo SITE_URL ?>api/v1/identity</code>.</p>

        <p>All methods must be called using HTTPS. The response will always be in JSON format.
        The properties within the response are defined in the documentation for the relevant method.</p>

        <h3>Errors</h3>
        <p>If an error occurred, there will be two properties: <code>error</code> and
        <code>error_description</code>. The error code will remain constant for the given error type,
        whereas the error_description is a human-readable description of the error and may change
        at any time.</p>
        <pre class="prettyprint">
        {
            "error": "ERROR_CODE",
            "error_description": "Something happened",
        }
        </pre>
        <p>The HTTP status code will also be different from the usual 200. For example, if your
        request had invalid or missing parameters then the status code will be 400 Bad request, and
        if you attempt to make a request on a subreddit you don't mod then it'll be 403 Forbidden.</p>
    </section>
    <section class="function">
        <h2 id="authentication">Authenticating</h2>

        <p>The api uses OAuth 2. If you are unsure of how to use OAuth 2, search for OAuth 2 tutorials.</p>

        <p>Our OAuth2 server currently only enables these grant types: client_credentials, authorization_code,
        refresh_token</p>

        <table class="flex-table flex-table-2">
            <thead>
                <tr>
                    <th colspan="2">OAuth URLs</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Authorize URL</td>
                    <td><?php echo SITE_URL ?>api/v1/authorize</td>
                </tr>
                <tr>
                    <td>Access Token URL</td>
                    <td><?php echo SITE_URL ?>api/v1/access_token</td>
                </tr>
            </tbody>
        </table>

        <p class="box">To register an application click <a href="<?php echo SITE_URL ?>prefs/apps">here</a>.</p>
    </section>
    <section class="function">
        <h2 id="rules">API Usage Rules</h2>

        <p>Overall, our API rules aren't too strict. But if there is precedent we may make them more strict overtime.
        Any changes to this API will be announced on <a href="https://www.reddit.com/r/Layer7">our subreddit</a>.</p>
        <ul>
            <li><b>Authentication</b> API clients must authenticate using OAuth 2</li>
            <li><b>Rate Limit:</b> currently we do not have a rate limit, this is subject to change if it becomes a problem.
            So please use our API responsibly. You shouldn't really be going over 60 requests per minute.</li>
            <li>
                <span><b>User-Agent:</b> the client's user-agent should be something unique and descriptive that identifies
                your application.</span>
                <ul>
                    <li><b>Do not lie about your user-agent.</b> Such as masquerading as other bots or applications.</li>
                </ul>
            </li>
            <li><b>Do not use our API for malicious purposes.</b> We do reserve the right to refuse service
            to any user or subreddit mod team.</li>
        </ul>
    </section>
    <section class="function">
        <h2 id="python_integration">Quick guide on how to integrate with
        your PRAW bot or other Python applications</h2>

        <p>With Python 3, <code>requests</code> library, and the <code>client_credentials</code>
        grant type.<br/>
        Or you could use a OAuth2 python library.</p>

        <hr/>

        <p>First <a href="<?php echo SITE_URL ?>prefs/apps">click this link</a> and register
        your OAuth2 application if you haven't done so already. Take note of the generated
        Consumer ID and Consumer Secret.</p>

        <p>Then request an access token:</p>

        <pre class="prettyprint">
        >>> import requests
        >>> import json

        >>> r = requests.post('https://layer7.solutions/api/v1/access_token',
                data = {
                    'grant_type': 'client_credentials',
                    'scope': 'identity tsbread' # space-delimited scope list
                },
                auth = (
                    'e0aaV-pQQ_vPpu', # Consumer ID
                    'f4fJPcr6UbpnBolBn_TI4HZZPKwQkYLvtMgvFiZ7oDEA0n5kDs' # Consumer Secret
                ))
        >>> r.status_code
        200
        >>> print(json.dumps(r.json(), indent=2))
        {
          'access_token': 'fc92463c2a1753ed1be23c84b0fd42496bbaf45f',
          'expires_in': 3600,
          'token_type': 'Bearer',
          'scope': 'identity tsbread'
        }
        </pre>
        <p>And then add the <code>Authorization: Bearer &lt;ACCESS TOKEN HERE&gt;</code> header
        to your requests:</p>
        <pre class="prettyprint">
        >>> r2 = requests.get('https://layer7.solutions/api/v1/videos/blacklist/reports',
                params = {
                    'type': 'channel_name',
                    'subject': 'funny videos',
                    'amount': 5
                },
                headers = {
                    'Authorization': r.json()['token_type'] + ' ' + r.json()['access_token']
                })
        >>> r2.status_code
        200
        >>> print(json.dumps(r2.json(), indent=2))
        {
          "matter": [
            {
              "subject": "funny videos",
              "type": "channel_name",
              "matter_id": "channel_name;funny videos"
            }
          ],
          "listing": [
            {
              "id": 64925062,
              "removed": false,
              "action_utc": 1513598213.2133,
              "subreddit": "videos",
              "author": "ngochaivp91",
              "thing_id": "t3_7kkrmd",
              "thing_title": "Funny videos 2017!Secret of doing stupid actions - HD sex Dino 2017 #3",
              "thing_data": "",
              "thing_created": 1513598163,
              "permalink": "http://reddit.com/7kkrmd",
              "media_author": "Funny Videos",
              "media_channel_id": "UCh6EkQQCkxOX_aEIJs0Ftew",
              "media_link": "https://www.youtube.com/attribution_link?a=vQhiRShsXZc&u=%2Fwatch%3Fv%3DvdnT4kqmP0s%26feature%3Dshare",
              "media_platform_id": 1,
              "media_channel_url": null,
              "media_platform": "YouTube",
            },
            {
              "id": 64526546,
              "removed": false,
              "action_utc": 1513412690.6086,
              "subreddit": "videos",
              "author": "Isadoscomedy",
              "thing_id": "t1_drbtfws",
              "thing_title": null,
              "thing_data": "Most funniest video https://youtu.be/PCnw7y_7x2k",
              "thing_created": 1513412664,
              "permalink": "http://reddit.com/comments/7k3eeu/-/drbtfws",
              "media_author": "Most Funny Videos Latest",
              "media_channel_id": "UCYytxrvf1C5TIN0Macafw4g",
              "media_link": "https://youtu.be/PCnw7y_7x2k",
              "media_platform_id": 1,
              "media_channel_url": null,
              "media_platform": "YouTube",
            },
            {
              "id": 64525973,
              "removed": false,
              "action_utc": 1513412324.8388,
              "subreddit": "videos",
              "author": "Isadoscomedy",
              "thing_id": "t1_drbtb1e",
              "thing_title": null,
              "thing_data": "Are you interested in laughing out your sorrow? Then click here https://youtu.be/S6e1XJcRWVM",
              "thing_created": 1513412308,
              "permalink": "http://reddit.com/comments/7k1qia/-/drbtb1e",
              "media_author": "Most Funny Videos Latest",
              "media_channel_id": "UCYytxrvf1C5TIN0Macafw4g",
              "media_link": "https://youtu.be/S6e1XJcRWVM",
              "media_platform_id": 1,
              "media_channel_url": null,
              "media_platform": "YouTube",
            },
            {
              "id": 64525902,
              "removed": false,
              "action_utc": 1513412272.9849,
              "subreddit": "videos",
              "author": "Isadoscomedy",
              "thing_id": "t1_drbta4n",
              "thing_title": null,
              "thing_data": "Are you interested in laughing out your sorrow? Then click here https://youtu.be/S6e1XJcRWVM",
              "thing_created": 1513412241,
              "permalink": "http://reddit.com/comments/7k3wwc/-/drbta4n",
              "media_author": "Most Funny Videos Latest",
              "media_channel_id": "UCYytxrvf1C5TIN0Macafw4g",
              "media_link": "https://youtu.be/S6e1XJcRWVM",
              "media_platform_id": 1,
              "media_channel_url": null,
              "media_platform": "YouTube",
            },
            {
              "id": 64525799,
              "removed": false,
              "action_utc": 1513412193.3,
              "subreddit": "videos",
              "author": "Isadoscomedy",
              "thing_id": "t1_drbt981",
              "thing_title": null,
              "thing_data": "Are you interested in laughing out your sorrow? Then click here https://youtu.be/S6e1XJcRWVM",
              "thing_created": 1513412174,
              "permalink": "http://reddit.com/comments/7k28dr/-/drbt981",
              "media_author": "Most Funny Videos Latest",
              "media_channel_id": "UCYytxrvf1C5TIN0Macafw4g",
              "media_link": "https://youtu.be/S6e1XJcRWVM",
              "media_platform_id": 1,
              "media_channel_url": null,
              "media_platform": "YouTube",
            }
          ],
          "amount": 5,
          "offset": 0,
          "total": 1297
        }
        >>>
        </pre>
        <p>And that's it! And don't forget about the <code>'expires_in': 3600</code> part of the
        <code>access_token</code> response. You'll have to fetch a new access token every hour.
        <p>Here's a list of all our scopes for ease of copying:</p>
        <textarea readonly style="width:100%;resize:vertical"><?php echo implode(' ', array_keys(api_scopes())) ?></textarea>
    </section>

    <section class="function">
        <h2 id="subreddit_notation">Subreddit Notation</h2>

        <p>Take note that wherever you see <code>{subreddit}</code>, whether as part of the path or
        as a query parameter value, you can use our
        <a href="<?php echo SITE_URL ?>developers/types#subreddit_notation">Subreddit Notation</a></p>

        <p>However, wherever you see <code>{srname}</code> allows only <b>one</b> subreddit.</p>
    </section>

    <div class="scope">
        <h2 class="scope-title">API Methods // identity</h2>
        <section class="function">
            <h2 id="identity">GET:/identity</h2>
            <p class="box">Returns the current user's username, a boolean value representing whether the
            user mods at least one subreddit, and some time information about the current user's activity
            on the layer7.solutions website.</p>

            <h3>Arguments</h3>
            <p>None</p>

            <h3>Example Response</h3>
            <pre class="prettyprint">
            {
                "username": "kwwxis",
                "first_login": 1477958400,
                "last_login": 1510016101,
                "last_access": 1513878367
            }
            </pre>
        </section>
        <section class="function">
            <h2 id="identity_subreddits">GET:/identity/subreddits[/{category}]</h2>
            <p class="box">Returns the subreddits the current user mods in no particular order.</p>

            <h3>Arguments</h3>
            <div class="param">
                <h4>category <span>(optional)</span></h4>
                <p>Returns subreddits the current users mods that are in the given category.
                Raises <code>UNKNOWN_FIELD</code> error if unknown category.
                </p>
            </div>
            <div class="param">
                <h4>detail <span>(optional)</span></h4>
                <p>If <code>true</code>, returns subreddit name to detail map rather
                than an array of just subreddit names.</p>
            </div>

            <h3>Example Response</h3>
            <pre class="prettyprint">
            [
                "pokemon",
                "CrucibleSherpa",
                "DestinySherpa",
                "worldnews",
                "LifeProTips",
                "aww",
                ...
            ]
            </pre>
        </section>
        <section class="function">
            <h2 id="identity_websync_history">GET:/identity/websync_history</h2>
            <p class="box">Gives a descending history of the current user's moderator history
            on subreddits that have TSB.</p>

            <h3>Arguments</h3>
            <div class="param">
                <h4>limit <span>(optional)</span></h4>
                <p>Limit results to a specific number. If this parameter is not present then
                the entire history will be returned.</p>
            </div>

            <h3>Example Response</h3>
            <p>The <code>moderator</code> property will always be the username of the
            current user. The moderator who performed the action on the current user
            is not available for this api method.</p>

            <pre class="prettyprint">
            [
                {
                    "id": 6120,
                    "type": "removemoderator",
                    "subreddit": "Scholar",
                    "moderator": "kwwxis",
                    "new_state": null,
                    "history_utc": 1512520179
                },
                {
                    "id": 5009,
                    "type": "setpermissions",
                    "subreddit": "Fireteams",
                    "moderator": "kwwxis",
                    "new_state": "config,flair",
                    "history_utc": 1506961057
                },
                {
                    "id": 4991,
                    "type": "setpermissions",
                    "subreddit": "DestinySherpa",
                    "moderator": "kwwxis",
                    "new_state": "config,flair",
                    "history_utc": 1506904167
                },
                {
                    "id": 4990,
                    "type": "setpermissions",
                    "subreddit": "CrucibleSherpa",
                    "moderator": "kwwxis",
                    "new_state": "config,flair",
                    "history_utc": 1506904108
                },
                {
                    "id": 4811,
                    "type": "acceptmoderatorinvite",
                    "subreddit": "aww",
                    "moderator": "kwwxis",
                    "new_state": null,
                    "history_utc": 1506199854
                },
                {
                    "id": 4809,
                    "type": "removemoderator",
                    "subreddit": "aww",
                    "moderator": "kwwxis",
                    "new_state": null,
                    "history_utc": 1506199854
                },
                ...
            ]
            </pre>
        </section>
    </div>

    <div class="scope">
        <h2 class="scope-title">API Methods // srprefread or srprefedit</h2>
        <section class="function">
            <h2 id="subreddit_settings">GET:/r/{srname}/settings</h2>

            <p class="box">Returns the current subreddit settings.</p>

            <h3>Arguments</h3>
            <div class="param">
                <h4>action (optional)</h4>
                <ul>
                    <li><code>get</code>: get the value for a specific property</li>
                    <li><code>set</code>: change a property's value. Requires <code>srprefedit</code> scope.</li>
                    <li>Not given: fetch the entire subreddit settings</li>
                </ul>
            </div>
            <div class="param">
                <h4>property (optional)</h4>
                <p>The property that is the subject of the request.
                Required only if the <code>action</code> parameter is set.</p>
                <p>Raises <code>UNKNOWN_FIELD</code> error if unknown property</p>
            </div>
            <div class="param">
                <h4>value (optional)</h4>
                <p>The new value. Required only if <code>action</code> is <code>set</code>.
                Must be one of these values if to be interpreted as true:
                <code>t</code>, <code>true</code>, <code>1</code>, <code>yes</code>.
                All other values will be interpreted as false.</p>
            </div>

            <h3>Example Responses</h3>
            <p>No parameters:</p>
            <pre class="prettyprint">
            {
                "id": 694,
                "subreddit": "aww",
                "redditbot_name": "TheSentinel_8",
                "redditbot_permissions": "posts,flair",
                "sentinel_enabled": true,
                "modlog_enabled": true,
                "modmail_enabled": false,
                "botban_enabled": true,
                "domainblacklist_enabled": true,
                "dirtbag_enabled": true
            }
            </pre>
            <p><code>?action=get&amp;property=sentinel_enabled</code>:</p>
            <pre>
            {
                "property_key": "sentinel_enabled",
                "property_value": true
            }
            </pre>
            <p><code>?action=set&amp;property=modmail_enabled&amp;value=f</code>:</p>
            <pre>
            {
                "property_key": "modmail_enabled",
                "property_new_value": false,
                "property_old_value": true
            }
            </pre>
        </section>
    </div>

    <div class="scope">
        <h2 class="scope-title">API Methods // logsread</h2>
        <section class="function">
            <h2 id="logs_view">GET:/logs/view</h2>

            <p class="box">Fetches a page from the user's subreddit moderation logs.</p>

            <h3>Arguments</h3>
            <div class="param">
                <h4>query <span>(required)</span></h4>
                <p>The filter query as a stringified JSON string. To grab a page with no filter, set this value
                to empty string or <code>{}</code>.</p>
                <p>The format of this JSON string follows:</p>
                <pre class="prettyprint">
                {
                    "tokens": [ // tokens (optional)
                        {
                            "label": "somelabel", // token label (required)
                            "modifier": null, // token modifier (optional, default:null)
                            "values": [ // token values (at least 1 value is required)
                                "somevalue"
                            ]
                        }
                    ]
                }
                </pre>
                <p>The query in the following example will return log entries for <code>sticky</code> and
                <code>removelink</code> actions in /r/DestinyTheGame and /r/videos.</p>
                <pre class="prettyprint">
                {
                "tokens": [
                    {
                        "label": "in",
                        "values": [
                            "destinythegame",
                            "videos"
                        ]
                    },
                    {
                        "label": "action",
                        "values":[
                            "removelink",
                            "sticky"
                        ]
                    }
                ]
                }
                </pre>
                <p>Here is a list of valid labels and modifiers:</p>
                <ul class="param-list">
                    <li>
                        <p><b>label:</b> in</p>
                        <p><b>Multiple values</b>: allowed</p>
                        <p>Use this token label to limit your query to specific subreddit(s)</p>
                    </li>
                    <li>
                        <p><b>label:</b> after</p>
                        <p><b>Multiple values</b>: no</p>
                        <p>Use this label to limit your query to actions preformed after this timestamp.
                        You can use this in conjunction with <code>before</code> if you want a specific range.</p>
                    </li>
                    <li>
                        <p><b>label:</b> before</p>
                        <p><b>Multiple values</b>: no</p>
                        <p>Use this label to limit your query to actions preformed before this timestamp.
                        You can use this in conjunction with <code>after</code> if you want a specific range.</p>
                    </li>
                    <li>
                        <p><b>label:</b> date</p>
                        <p><b>Multiple values</b>: no</p>
                        <p>Use this label to limit your query to actions between the given
                        timestamp and <code>timestamp + 86400</code></p>
                    </li>
                    <li>
                        <p><b>label:</b> mod</p>
                        <p><b>Multiple values</b>: allowed</p>
                        <p>Use this label to limit your query to a specific set of moderator(s).
                        Do not include the "/u/", just the moderator username.</p>
                    </li>
                    <li>
                        <p><b>label:</b> action</p>
                        <p><b>Multiple values</b>: allowed</p>
                        <p>Use this label to limit your query to a specific set of mod action types.
                        See <a href="<?php echo SITE_URL ?>developers/types#modactiontypes">this page</a>
                        for a list of valid action types</p>
                    </li>
                    <li>
                        <p><b>label:</b> reason</p>
                        <p><b>Multiple values</b>: no</p>
                        <p>Search for mod actions whose reason includes the given string in it's reason.
                        The modifier used if no modifier specified is <code>includes</code></p>
                        <ul>
                            <li>
                                <p><b>mod:</b> includes-word</p>
                                <p>Search for reasons containing the given word.</p>
                            </li>
                            <li>
                                <p><b>mod:</b> includes</p>
                                <p>Search for reasons containing the given string.</p>
                            </li>
                            <li>
                                <p><b>mod:</b> starts-with</p>
                                <p>Search for reasons starting with the given string.</p>
                            </li>
                            <li>
                                <p><b>mod:</b> ends-with</p>
                                <p>Search for reasons ending with the given string.</p>
                            </li>
                            <li>
                                <p><b>mod:</b> full-exact</p>
                                <p>Search for reasons that exactly match the given string.</p>
                            </li>
                            <li>
                                <p><b>mod:</b> full-text</p>
                                <p>Search for reasons that exactly match the given string,
                                ignoring leading and trailing whitespace.</p>
                            </li>
                            <li>
                                <p><b>mod:</b> regex</p>
                                <p>Search for reasons that match the given regex.</p>
                            </li>
                            <li>
                                <p><b>mod:</b> shorter-than</p>
                                <p>Search for reasons whose length is shorter than the
                                given number in characters.</p>
                            </li>
                            <li>
                                <p><b>mod:</b> longer-than</p>
                                <p>Search for reasons whose length is longer than the
                                given number in characters.</p>
                            </li>
                        </ul>
                        <p>You can only include one modifier per token item. But you can use
                        multiple token items. For example, this query will search for reasons
                        longer than 5 characters that also contain "flair":</p>
                        <pre class="prettyprint">
                        {
                        "tokens": [
                            {
                                "label": "reason",
                                "modifier": "longer-than",
                                "values": [ "5" ]
                            },
                            {
                                "label": "reason",
                                "values": [ "flair" ]
                            }
                        ]
                        }
                        </pre>
                    </li>
                    <li>
                        <p><b>label:</b> author</p>
                        <p><b>Multiple values</b>: allowed</p>
                        <p>Limit your query to actions whose subject user is in the given set of authors.
                        Do not include the "/u/", just the subject username.</p>
                    </li>
                    <li>
                        <p><b>label:</b> thingid</p>
                        <p><b>Multiple values</b>: allowed</p>
                        <p>Limit your query to actions whose thing ID is in the given set of thing IDs.
                        The thing id should include the <code>t#_</code> prefix.</p>
                    </li>
                </ul>
            </div>
            <div class="param">
                <h4>before <span>(optional)</span></h4>
                <p>Used for pagination. If this argument is present, fetching will start before the entry with the ID
                that matches the value of this argument. Not inclusive of the entry with the ID.
                Use <code>before</code> when going back a page.</p>
            </div>
            <div class="param">
                <h4>after <span>(optional)</span></h4>
                <p>Used for pagination. If this argument is present, fetching will start after the entry with the ID
                that matches the value of this argument. Not inclusive of the entry with the ID.
                Use <code>after</code> when going forward a page.</p>
                <p>If both <code>before</code> and <code>after</code> are present, <code>after</code> will be ignored
                and <code>before</code> will be used.</p>
            </div>
            <div class="param">
                <h4>amount <span>(optional)</span></h4>
                <p>The amount of items per page. Max is 100. Default is 25.</p>
            </div>

            <h3>Example Response</h3>
            <pre class="prettyprint">
            {
                "listing": {
                    {
                        "id": 1517333, // the id to use for <code>before</code> and <code>after</code>
                        "thingid": "t3_5pegdn",
                        "mod": "Destiny_Flair_Bot",
                        "author": "BladedAbyss2551",
                        "action": "removelink",
                        "reason": "remove",
                        "permalink": "/r/DestinyTheGame/comments/5pegdn/wrath_of_the_machine_hard_mode_question/",
                        "timestamp": 1485050543,
                        "human_timing": "3 minutes ago",
                        "subreddit": "DestinyTheGame",
                        "modactionid": "ModAction_d090c95c-e046-11e6-9a0a-0e5f443b9416",
                        "thing_title": "Wrath of The Machine Hard Mode Question..."
                    },
                    {
                        "id": 1517332,
                        "thingid": "t3_5peh16",
                        "mod": "CodeWord_Dirtbag",
                        "author": "SeekonlyJoy",
                        "action": "removelink",
                        "reason": "remove",
                        "permalink": "/r/videos/comments/5peh16/abrahamhicks_avoiding_your_thoughts_esther_hicks/",
                        "timestamp": 1485050489,
                        "human_timing": "4 minutes ago",
                        "subreddit": "videos",
                        "modactionid": "ModAction_b0499a0c-e046-11e6-98fc-0ece270f97e6",
                        "thing_title": "#AbrahamHicks ~ Avoiding your #Thoughts â™¥ Esther Hicks #LawofAttraction Best Daily Videos Workshop"
                    },

                    ... skipping to last item ...

                    {
                        "id": 1517267,
                        "thingid": "t3_5pecaj",
                        "mod": "AutoModerator",
                        "author": "turcois",
                        "action": "removelink",
                        "reason": "[C11.1] Political - Bernie",
                        "permalink": "/r/videos/comments/5pecaj/no_i_will_not_yield/",
                        "timestamp": 1485048882,
                        "human_timing": "30 minutes ago",
                        "subreddit": "videos",
                        "modactionid": "ModAction_f2ccc8c6-e042-11e6-8778-0e18eb2eb5c7",
                        "thing_title": "\"No, I will not yield!\""
                    }
                },
                "datainfo": { // contains some metadata about 'listing'
                    "sr_count": { // map of # of occurences for each subreddit in 'listing'
                        "DestinyTheGame": 3,
                        "videos": 22
                    },
                    "amount": 25, // same as 'amount' received
                    "before": null, // the id of the first item in 'listing' (will be null if at first page)
                    "after": 1517267, // the id of the last item in 'listing'
                }
            }
            </pre>
            <p>The <code>before</code> and <code>after</code> properties in <code>datainfo</code> are not
            copies of the parameters received by the request, but rather the <code>before</code> and
            <code>after</code> values you should use to traverse to the previous or next page respectively.
            If <code>before</code> is null, then that means there is no previous page.</p>

            <h3>Errors</h3>
            <p>INVALID_QUERY</p>
        </section>
    </div>
</article>