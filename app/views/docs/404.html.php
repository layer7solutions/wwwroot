<article>
    <header>
        <h2>Documentation page not found!</h2>
        
        <p>The page requested either does not exist or was moved.</p>
        <p><a href="<?php echo SITE_URL ?>docs/">return to index</a></p>
    </header>
</article>