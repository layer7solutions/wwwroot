<article>
    <header>
        <h1 data-doctitle="Privacy Policy | Layer7">Layer 7 Solutions, LLC - Privacy Policy</h1>
        <p>Modified: December 22nd, 2017.<br/>
        Effective: December 22nd, 2017.</p>

        <p class="notice">This is an old version of our Privacy Policy and is no longer legally
        significant or binding. It has been superseded by the latest version, which can be found at
        <strong><a href="<?php echo SITE_URL ?>privacy"><?php echo SITE_URL ?>privacy</a></strong>.</p>
    </header>
    <div id="toc" class="no-fixed split">
        <section data-title="Privacy Policy">
            <a href="#what_and_why">What and Why</a>
            <section>
                <a href="#what_and_why_p1">On subreddits employing our bots</a>
                <a href="#what_and_why_p2">On users of the Layer7.Solutions website</a>
            </section>
            <a href="#how">How</a>
            <section>
                <a href="#how_p1">Services</a>
                <a href="#how_p2">Retention</a>
            </section>
            <a href="#with_whom">With Whom</a>
            <section>
                <a href="#with_whom_p1">Essential Third-Party Services</a>
                <a href="#with_whom_p2">The Mod Team of a Subreddit</a>
                <a href="#with_whom_p3">Those You Consent To</a>
                <a href="#with_whom_p4">Our Staff</a>
                <a href="#with_whom_p5">Law &amp; Order</a>
                <a href="#with_whom_p6">And Nobody Else</a>
            </section>
            <a href="#information_security">Information Security</a>
            <a href="#your_choices">Your Choices</a>
            <a href="#changes">Changes</a>
            <a href="#contact">Contact</a>
        </section>
        <?php render_template('docs.templates.privacy_versions'); ?>
        <?php render_template('docs.templates.legaldocs_list'); ?>
    </div>
    <section style="padding-bottom: 0;">
        <p>Layer 7 Solutions, LLC ("Layer7.Solutions", "us", "we", or "our") operates our website, software,
        and services ("Service", "Services").
        This Privacy Policy informs you of our policies regarding how we collect, use,
        and handle your information</p>
    </section>
    <section class="function">
        <h2 id="what_and_why">What and Why</h2>

        <h4 id="what_and_why_p1">On subreddits employing our bots</h4>

        <p>As a baseline, when TheSentinelBot is added as moderator we will begin recording all posts and
        comments made to the subreddit. This includes body, title, author, time posted, the unique identifier
        of the post, a permalink to the post/comment, any flair attributed to the post, and the outgoing URL
        of link posts.</p>

        <p>TheSentinelBot has several modules that may enabled or disabled at will of the mod team through
        the settings pages of our website. Should features be enabled, additional data will be collected in
        other to provide the the service that was enabled. Specifically:</p>

        <ul>
            <li>"Sentinel" shall require the data collection of posts and comments,</li>
            <li>"Mod Action Logging" shall require the collection of the subreddit's mod action log,</li>
            <li>"Modmail Logging" shall require we record the subreddit's modmail,</li>
            <li>"Bot Ban" shall require the same information as "Sentinel",</li>
            <li>"Domain Blacklisting" shall require the same information as "Sentinel",</li>
            <li>"Dirtbag" shall require the same information as "Sentinel".</li>
        </ul>

        <p>The "Sentinel" module is the only module enabled by default, all other modules are disabled by
        default.  We will not opt the subreddit into any additional modules unless given the explicit
        permission by the mod team thereof. If a module is disabled, the pertinent information will no
        longer be collected but will be retained. If the module is re-enabled, we resume collecting the
        pertinent information.</p>

        <h4 id="what_and_why_p2">On users of the Layer7.Solutions website</h4>

        <p>By logging into our website using your Reddit.com account, you agree to give us access to
        information afforded to us by Reddit.com limited to these scopes: identity, mysubreddits, read.
        These scopes give us access to: posts and comments through your account; the list of subreddits
        you moderate, contribute to, and subscribe to; and your reddit username and signup date. However,
        our website specifically only stores and uses: your reddit username, the list of subreddits you mod,
        and the list of moderators and their permissions on each subreddit you mod. This information will
        be kept indefinitely unless manually revoked through user request.</p>

        <p>Layer7.Solutions may collect limited non-personally identifying information your browser makes
        available whenever you visit a website. This log information may include but not limited to your
        Internet Protocol address, browser type, browser language, the date and time of your query and
        one or more cookies that may uniquely identify your browser. We use this information to operate,
        develop and improve our Services. This information may be kept indefinitely.</p>

        <p>A "cookie" is a small file containing a string of characters that is sent to your computer when
        you visit a website. We use cookies to improve the quality of our service. You can reset your browser
        to refuse all cookies or to indicate when a cookie is being sent. However, some Layer7.Solutions
        features or Services, namely the ability to login to our website, may not function properly without
        cookies.</p>
    </section>
    <section class="function">
        <h2 id="how">How</h2>

        <p><b id="how_p1">Services</b>
        The information collected by all Services is used to provide, improve, and
        protect our Services. We do not hand out or sell any of this information to anyone.

        <p><b id="how_p2">Retention</b>
        If the mod team of a subreddit chooses to remove TheSentinelBot from their subreddit,
        no further information will be collected on that subreddit, but for security reasons, such as a rogue
        moderator, existing collected information shall be retained. To have all information and user content
        collected on your subreddit deleted, have the head moderator contact us through /r/Layer7 modmail, if
        that is not possible we would need to see a consensus by the mod team through a majority of active
        moderators individually contacting us at Layer7 modmail. Should we complete your deletion request,
        we will send a modmail to your subreddit confirming the deletion of all collected data.</p>
    </section>
    <section class="function">
        <h2 id="with_whom">With Whom</h2>

        <h4 id="with_whom_p1">Essential Third-Party Services</h4>
        <p>We may use third-party services to store, process, or transmit data, or perform other technical
        functions related to operating our Services. These services currently only include the Reddit.com API,
        our server host (Hivelocity Hosting), and our DNS Registrar (Google Domains).

        <h4 id="with_whom_p2">The Mod Team of a Subreddit</h4>
        <p>If the mod team of a subreddit asks for information we have regarding their subreddit, we may
        provide that information to them.</p>

        <h4 id="with_whom_p3">Those You Consent To</h4>
        <p>We may offer our information to any third party provided that you give your explicit consent.</p>
        <p>For example, a Layer7.Solutions website user can give third parties access to their information
        and Layer7 account through our OAuth API. Just remember that use of your information will be governed
        by their privacy policy and terms</p>

        <h4 id="with_whom_p4">Our Staff</h4>
        <p>Your information may be accessible to site staffs who need to
        access that information in order to operate, develop or improve our Services.</p>

        <h4 id="with_whom_p5">Law &amp; Order</h4>
        <p>We may preserve or disclose your information if we believe that it is reasonably necessary to comply with
        a law, regulation, legal process, or governmental request; or to protect the physical safety of any person.</p>

        <h4 id="with_whom_p6">And Nobody Else</h4>
        <p>We do not rent, sell, or give away your information to any third parties.
        The stewardship of your information and trust are critical to us and not something we would compromise.
        As Reddit moderators ourselves, we provide our Services solely to make moderators' lives easier.</p>
    </section>
    <section>
        <h2 id="information_security">Information Security</h2>

        <p>We take appropriate security measures to protect against unauthorized access to or
        unauthorized alteration, disclosure or destruction of data.</p>

        <p>If we terminate your service, we may retain enough information to prevent you
        from signing up for the service in the future.</p>
    </section>
    <section>
        <h2 id="your_choices">Your Choices</h2>

        <p>At Layer 7 Solutions, we respect user's rights to privacy and will delete any Reddit.com
        user's content from our server(s) and database(s) should that user content be already deleted
        from Reddit.com and should the user contact us. We do not have the technical capabilities to
        immediately delete any user content that has been deleted from Reddit.com.</p>

        <p>We do not allow any Reddit.com users to opt-out from having their user content collected
        on subreddits that employ our Services. If a subreddit uses TheSentinelBot or any other of our
        Services and you do not want your user content that you post to that subreddit collected by us,
        then we encourage you to not participate in that subreddit. In accordance with Reddit.com's
        User Agreement, by participating in a subreddit you authorize that subreddit to use your
        content and activity on that subreddit as the moderators thereof see fit so long as it is
        not contrary to the Reddit User Agreement or Reddit Content Policy.</p>
    </section>
    <section>
        <h2 id="changes">Changes</h2>

        <p>We may revise this Privacy Policy from time to time, the most recent version will always be at
        <a href="<?php echo SITE_URL ?>privacy"><?php echo SITE_URL ?>privacy</a>.
        If a revision meaningfully reduces your rights, we will notify you.</p>
    </section>
    <section>
        <h2 id="contact">Contact</h2>

        <p>Still have questions? Contact us by sending a modmail to
        <a href="https://www.reddit.com/r/Layer7">/r/Layer7</a></p>
    </section>
</article>