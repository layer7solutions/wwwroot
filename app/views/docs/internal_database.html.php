<article>
    <header>
        <h1 id="db_class">DB.class</h1>

        <p><code>db.php</code> is a standalone file, it does not require any other files in the application to run.</p>
    </header>
    <div id="toc" class="no-fixed">
        <a href="#db_class">DB</a>
        <section>
            <a href="#db_connect">connect</a>
            <a href="#adding-new-drivers">Adding new drivers</a>
            <a href="#error-handling">Error handling</a>
            <span>Query</span>
            <section>
                <a href="#db_query">query</a>
                <a href="#db_queryList">queryList</a>
                <a href="#db_queryFirstRow">queryFirstRow</a>
                <a href="#db_queryFirstList">queryFirstList</a>
                <a href="#db_queryFirstColumn">queryFirstColumn</a>
                <a href="#db_queryOneColumn">queryOneColumn</a>
                <a href="#db_queryFirstField">queryFirstField</a>
                <a href="#db_queryOneField">queryOneField</a>
                <a href="#db_queryRaw">queryRaw</a>
                <a href="#db_insert">insert</a>
                <a href="#db_insertIgnore">insertIgnore</a>
                <a href="#db_insertUpdate">insertUpdate</a>
                <a href="#db_insert">replace</a>
                <a href="#db_update">update</a>
                <a href="#db_delete">delete</a>
            </section>
            <span>Regarding Last Query</span>
            <section>
                <a href="#db_insertId">insertId</a>
                <a href="#db_count">count</a>
                <a href="#db_affectedRows">affectedRows</a>
            </section>
            <span>Other</span>
            <section>
                <a href="#db_transactions">Transactions</a>
                <a href="#db_tableList">tableList</a>
                <a href="#db_columnList">columnList</a>
                <a href="#db_disconnect">disconnect</a>
                <a href="#db_get">get</a>
            </section>
        </section>
    </div>
    <section class="function">
        <h2 id="db_connect">connect()</h2>

        <p>connect - construct a new DB object.</p>

        <div class="notice">Contrary to what the name of the function suggests,
        this function does not actually start a database connection.<br/>The connection is
        made on the first query to the database.</div>

        <h3>Description:</h3>
        <pre class="prettyprint">
        $db = \DB::connect(array $options);
        </pre>

        <h3>Parameters:</h3>

        <div class="param">
            <h4>options</h4>
            <p>An associative array of connection options. See the table below
            for the list of properties.</p>
            <table>
                <thead>
                    <tr>
                        <th style="width: 100px;">property name</th>
                        <th style="width: 70px;">is optional</th>
                        <th style="width: 100px;">driver specific</th>
                        <th>description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>driver</td>
                        <td>no</td>
                        <td>no</td>
                        <td>
                        <p>The driver type is the database system type (MySQL, PostgreSQL, etc.)</p>
                        <p>If there exists a constant named <code>DB_DEFAULT_DRIVER</code> and this property
                        is not passed, then the driver type will be taken from that constant. If this constant
                        exists and the driver property <i>is</i> passed, then the driver type will be taken
                        from the passed driver type.</p>
                        <p>Currently only the following driver types are supported:</p>
                        <ul>
                            <li><code>pgsql</code></li>
                        </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>host</td>
                        <td>yes</td>
                        <td>no</td>
                        <td>the database host; if not supplied, <code>localhost</code> will be used.</td>
                    </tr>
                    <tr>
                        <td>port</td>
                        <td>yes</td>
                        <td>no</td>
                        <td>database port</td>
                    </tr>
                    <tr>
                        <td>dbname</td>
                        <td>no</td>
                        <td>no</td>
                        <td>database name</td>
                    </tr>
                    <tr>
                        <td>user</td>
                        <td>no</td>
                        <td>no</td>
                        <td>databa user</td>
                    </tr>
                    <tr>
                        <td>password</td>
                        <td>no</td>
                        <td>no</td>
                        <td>password for the database user</td>
                    </tr>
                    <tr>
                        <td>options</td>
                        <td>yes</td>
                        <td>pgsql</td>
                        <td>This is the <code>options=''</code> part of the <code>pg_connect</code> connection string.<br/>
                        You can set options <code>--client_encoding=UTF8</code> to set the database encoding to UTF-8 for example.</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <h3>Return values</h3>
        <p>Returns a <code>DB</code> object.</p>

        <h3>Examples</h3>
        <pre class="prettyprint">
        $db = \$db->connect([
            'driver' => 'pgsql',
            'host' => 'localhost',
            'user' => 'admin',
            'password' => 'Hunter2',
            'dbname' => 'some_database',
        ]);
        </pre>
    </section>
    <section class="function">
        <h2 id="adding-new-drivers">Adding new drivers</h2>

        <p>The DB drivers should be named <code>driver_{drivername}.php</code> and be placed within the same folder as <code>db.php</code>.
        <code>{drivername}</code> is what should be passed to the "driver" property in <code>$db->connect()</code>.</p>

        <p>See <code>driver_interface.php</code> on how to create new database drivers if needed.</p>
    </section>
    <section class="function">
        <h2 id="error-handling">Error handling</h2>

        <h3>Properties</h3>

        <p>There are no getters/setters for these properties, just assign them directly.</p>

        <ul>
            <li>
                <code>(bool) $db->throw_exception_on_sql_error</code><b> (default: false)</b>
                <p>Whether or not to throw a DBException on a SQL error</p>
            </li>
            <li>
                <code>(bool) $db->throw_exception_on_nonsql_error</code><b> (default: false)</b>
                <p>Whether or not to throw a DBException on a non-SQL error</p>
            </li>
            <li>
                <code>(callable) $db->sql_error_handler</code><b> (default: db_sql_error_handler)</b> and<br/>
                <code>(callable) $db->nonsql_error_handler</code><b> (default: db_nonsql_error_handler)</b>:

                <p>
                Both handler functions are passed one argument: $args - an associative array.

                **Properties:**
                <ul>
                    <li>type: either 'sql' or 'nonsql', so you can set both handlers to the same function if you'd like</li>
                    <li>error: error message</li>
                    <li>query (type:sql only): the SQL query that caused the error</li>
                    <li>errno: SQLSTATE number</li>
                </ul>
                </p>
            </li>
        </ul>
    </section>
    <section class="function">
        <h2 id="db_query">query()</h2>

        <p>query - make a database query</p>

        <h3>Description</h3>
        <code>$db->query(string $query [, {array $args}|{mixed $value...} ])</code>

        <h3>Parameters</h3>

        <div class="param">
            <h4>query</h4>
            <p>The SQL query statement</p>
        </div>
        <div class="param">
            <h4>args</h4>
            <p>An associative array: array keys should coorespond to the name of a placeholder variable
            and the respective array value be the value for said placeholder variable</p>
            <p>Use <code>args</code> only if placeholder variables are named, otherwise use <code>value...</code></p>

            <h5>Example:</h5>
            <pre class="prettyprint">
            $db->query('SELECT * FROM accounts WHERE username=%s_user', array('user' => 'bob'))
            </pre>
        </div>
        <div class="param">
            <h4>value...</h4>
            <p>Values for each placeholder variable. An additional parameter must exist for each
            placeholder variable unless <code>args</code> is being used.</p>

            <h5>Example:</h5>
            <pre class="prettyprint">
            $db->query('SELECT * FROM accounts WHERE account_age>%i AND reputation>%i', 360, 5)
            </pre>
        </div>

        <p><b>Summary:</b></p>
        <p>The first parameter is a query string with placeholder variables. Following that, you must have
        an additional parameter for each placeholder variable, or alternatively a single associative
        array if the placeholder variables are named.</p>

        <h3>Placeholder variables</h3>
        <ul>
            <li>Unnamed: <code>%s</code></li>
            <li>Named: <code>%s_some_name_here</code></li>
        </ul>

        <table>
            <tr>
                <th colspan="2">Placeholder variable types</th>
            </tr>
            <tr><td><code>%s</code></td><td>string</td></tr>
            <tr><td><code>%i</code></td><td>integer</td></tr>
            <tr><td><code>%d</code></td><td>decimal/double</td></tr>
            <tr><td><code>%t</code></td><td>timestamp (can be instance of DateTime or string accepted by strtotime)</td></tr>
            <tr><td><code>%?</code></td><td>any data type (including arrays) -- will automatically do the right thing</td></tr>
            <tr><td><code>%ss</code></td><td>search string (string surrounded with % for use with LIKE)</td></tr>
            <tr><td><code>%b</code></td><td>identifiers (can be dangerous with user-supplied data -- BE CAREFUL)</td></tr>
            <tr><td><code>%l</code></td><td>literal (no escaping or parsing of any kind -- BE CAREFUL)</td></tr>
            <tr><td><code>%ls</code></td><td>list of strings (array)</td></tr>
            <tr><td><code>%li</code></td><td>list of integers</td></tr>
            <tr><td><code>%ld</code></td><td>list of decimals/doubles</td></tr>
            <tr><td><code>%lt</code></td><td>list of timestamps</td></tr>
            <tr><td><code>%lb</code></td><td>list of identifiers (can be dangerous with user-supplied data -- BE CAREFUL)</td></tr>
            <tr><td><code>%ll</code></td><td>list of literals (no escaping or parsing of any kind -- BE CAREFUL)</td></tr>
        </table>

        <h3>Return values:</h3>
        <p>An array of associative arrays. If your query produced no results, an empty array will be returned.</p>

        <h3>Return values for no results and erroring of ALL QUERY FUNCTIONS</h3>
        <div class="indent">
        <div class="spacer">
            <h4>If the query produced no results:</h4>
            <p>For <code>query()</code> and <code>queryList()</code> an empty array will be returned.<br/>
            For all other query functions except <code>queryRaw()</code>, <code>null</code> will be returned</p>
        </div>
        <div class="spacer">
            <h4>If the query resulted in an error:</h4>
            <p>For all query functions, <code>false</code> will be returned</p>
        </div>
        </div>
    </section>
    <section class="function">
        <h2 id="db_queryList">queryList()</h2>

        <p>Same as <a href="#db_query"><code>query()</code></a>, but returns results as numbered index (non-associative) arrays.</p>
    </section>
    <section class="function">
        <h2 id="db_queryFirstRow">queryFirstRow()</h2>

        <p>Returns the first row of results for this query as an associative array.</p>

        <pre class="prettyprint">
        // get information on the account with the username Joe
        $account = $db->queryFirstRow("SELECT * FROM accounts WHERE username=%s", 'Joe');
        echo "Username: " . $account['username'] . "\n"; // will be Joe, obviously
        echo "Password: " . $account['password'] . "\n";
        </pre>
    </section>
    <section class="function">
        <h2 id="db_queryFirstList">queryFirstList()</h2>

        <p>Retrieve the first row of results for the query, and return it as a numbered index (non-associative) array.</p>

        <pre class="prettyprint">
        // get information on the account with the username Joe
        list($username, $password) = $db->queryFirstList("SELECT username, password FROM accounts WHERE username=%s", 'Joe');
        echo "Username: " . $username . "\n"; // will be Joe, obviously
        echo "Password: " . $password . "\n";
        </pre>
    </section>
    <section class="function">
        <h2 id="db_queryFirstColumn">queryFirstColumn()</h2>

        <p>Retrieve the first column of results for the query, and return it as a regular array.</p>

        <pre class="prettyprint">
        // get a list of DISTINCT usernames in the accounts table (skip duplicates, if any)
        $usernames = $db->queryFirstColumn("SELECT DISTINCT username FROM accounts");
        foreach ($usernames as $username) {
            echo "Username: " . $username . "\n";
        }
        </pre>
    </section>
    <section class="function">
        <h2 id="db_queryOneColumn">queryOneColumn()</h2>

        <p>Retrieve the requested column of results from the query, and return it as a regular array.</p>

        <pre class="prettyprint">
        // get a list of ALL usernames in the accounts table
        $usernames = $db->queryOneColumn('username', "SELECT * FROM accounts");
        foreach ($usernames as $username) {
            echo "Username: " . $username . "\n";
        }
        </pre>
    </section>
    <section class="function">
        <h2 id="db_queryFirstField">queryFirstField()</h2>

        <p>Get the contents of the first field from the first row of results, and return that.</p>

        <pre class="prettyprint">
        // get Joe's password and print it out
        $joePassword = $db->queryFirstField("SELECT password FROM accounts WHERE username=%s", 'Joe');
        echo "Joe's password is: " . $joePassword . "\n";
        </pre>
    </section>
    <section class="function">
        <h2 id="db_queryOneField">queryOneField()</h2>

        <p>Get the contents of the requested field from the first row of results, and return that.</p>

        <pre class="prettyprint">
        // get Joe's password and print it out
        $joePassword = $db->queryOneField('password', "SELECT * FROM accounts WHERE username=%s", 'Joe');
        echo "Joe's password is: " . $joePassword . "\n";
        </pre>
    </section>
    <section class="function">
        <h2 id="db_queryRaw">queryRaw()</h2>

        <p>Like <code>query()</code> but returns the standard result object.</p>

        <pre class="prettyprint">
        $db->queryRaw("SELECT * FROM accounts WHERE username=%s", 'Joe');
        </pre>
    </section>
    <section class="function">
        <h2 id="db_insert">insert() / replace()</h2>

        <p>Either INSERT or REPLACE a row into a table. You can use $db->sqleval() to force something to be passed directly and not escaped.</p>

        <div class="notice">
            $db->sqleval() does nothing on its own, outside of the insert/replace/update/delete commands.
        </div>

        <pre class="prettyprint">
        // insert a new account
        $db->insert('accounts', array(
          'username' => 'Joe',
          'password' => 'hello'
        ));

        // change Joe's password (assuming username is a primary key)
        $db->replace('accounts', array(
          'username' => 'Joe',
          'password' => 'asd254890s'
        ));

        // use $db->sqleval() to pass things directly
        // sqleval() supports the same parameter structure as query()
        $db->insert('accounts', array(
          'username' => 'Joe',
          'password' => 'hello',
          'data' => $db->sqleval("REPEAT('blah', %i)", 4),
          'time' => $db->sqleval("NOW()")
        ));

        // insert two rows at once
        $rows = array();
        $rows[] = array(
          'username' => 'Frankie',
          'password' => 'abc'
        );
        $rows[] = array(
          'username' => 'Bob',
          'password' => 'def'
        );
        $db->insert('accounts', $rows);
        </pre>
    </section>
    <section class="function">
        <h2 id="db_insertIgnore">insertIgnore()</h2>

        <p>Like INSERT, except no error will be thrown
        if the primary key is already taken.</p>

        <pre class="prettyprint">
        // insert new account, don't throw an error if primary key id is already taken
        $db->insertIgnore('accounts', array(
          'id' => 5, //primary key
          'username' => 'Joe',
          'password' => 'hello'
        ));
        </pre>
    </section>
    <section class="function">
        <h2 id="db_insertUpdate">insertUpdate()</h2>

        <p>Like INSERT, but will update if the primary key is taken.</p>

        <h3>Description</h3>
        <pre class="prettyprint">
        $db->insertUpdate(string $tablename, array $insertData [, {string $where, mixed $value...}|{array $updateData}])
        </pre>

        <h3>Examples</h3>
        <pre class="prettyprint">
        // insert new account, if id 5 is already taken
        // then change Joe's password to goodbye instead
        $db->insertUpdate('accounts', array(
          'id' => 5, //primary key
          'username' => 'Joe',
          'password' => 'hello'
        ), 'password=%s', 'goodbye');

        // same as above
        $db->insertUpdate('accounts', array(
          'id' => 5, //primary key
          'username' => 'Joe',
          'password' => 'hello'
        ), array(
          'password' => 'goodbye'
        ));

        // insert new account, if id 5 is taken then the username and password fields
        // will be set to 'Joe' and 'hello' respectively and all other fields ignored
        // this is a bit like REPLACE INTO, except we leave any other columns in the table
        // untouched
        $db->insertUpdate('accounts', array(
          'id' => 5, //primary key
          'username' => 'Joe',
          'password' => 'hello'
        ));
        </pre>
    </section>
    <section class="function">
        <h2 id="db_update">update()</h2>

        <p>Run an UPDATE command by specifying an array of changes to make (<code>$updateData</code>),
        and a WHERE component (<code>$where</code>)</p>

        <h3>Description</h3>
        <pre class="prettyprint">
        $db->update(string $tablename, array $updateData, string $where [, mixed $where_value...])
        </pre>

        <h3>Examples</h3>
        <pre class="prettyprint">
        // change Joe's password
        $db->update('accounts', array(
          'password' => 'sdfdd'
        ), "username=%s", 'Joe');

        // set Joe's password to "joejoejoe"
        // WARNING: Passing user-submitted data to sqleval() will probably create a security flaw!!
        $db->update('accounts', array(
          'password' => $db->sqleval("REPEAT('joe', 3)")
        ), "username=%s", 'Joe');
        </pre>
    </section>
    <section class="function">
        <h2 id="db_delete">delete()</h2>

        <p>Run a DELETE command given a WHERE component.</p>

        <h3>Description</h3>
        <pre class="prettyprint">
        $db->delete(string $tablename, string $where [, mixed $where_value...])
        </pre>

        <h3>Examples</h3>
        <pre class="prettyprint">
        // delete Joe's account
        $db->delete('accounts', "username=%s", 'Joe');
        </pre>
    </section>
    <section class="function">
        <h2 id="db_insertId">insertId()</h2>

        <p>Returns the auto incrementing ID for the last insert statement.
        The insert could have been done through $db->insert() or $db->query().</p>

        <pre class="prettyprint">
        // insert a new account (presuming there is a primary key id column)
        $db->insert('accounts', array(
          'username' => 'Joe',
          'password' => 'hello'
        ));

        $joe_id = $db->insertId(); // what id did it choose?
        </pre>
    </section>
    <section class="function">
        <h2 id="db_count">count()</h2>

        <p>Counts the number of rows returned by the last query.</p>

        <div class="notice">
            <p>This returns the number of rows returned by the <i>actual</i> query,
            ignoring functions like <code>queryFirstRow</code> that return something specific from
            within the query result.</p>
            <p>So if you do <code>queryFirstRow</code> this function will return
            the actual number of rows retrieved rather than <code>1</code></p>
        </div>

        <pre class="prettyprint">
        $db->query("SELECT * FROM accounts WHERE password=%s", 'Hunter2');
        $counter = $db->count();
        echo $counter . " people are using Hunter2 as their password!!";
        </pre>
    </section>
    <section class="function">
        <h2 id="db_affectedRows">affectedRows()</h2>

        <p>Returns the number of rows changed by the last update statement.
        That statement could have been run through $db->update() or $db->query().</p>

        <pre class="prettyprint">
        // give a better password to everyone who is using hello as their password
        $db->query("UPDATE accounts SET password=%s WHERE password=%s", 'sdfwsert4rt', 'hello');
        $counter = $db->affectedRows();
        echo $counter . " people just got their password changed!!\n";
        </pre>
    </section>
    <section class="function">
        <h2 id="db_transactions">startTransaction() / commit() / rollback()</h2>

        <p>These are merely shortcuts for the three standard transaction commands:
        <code>START/BEGIN TRANSACTION</code>, <code>COMMIT</code>, and <code>ROLLBACK</code>.</p>

        <p>
        When <code>$db->nested_transactions</code> is enabled, these commands can be used to have multiple
        layered transactions. Otherwise, running $db->startTransaction() when a transaction is active will
        auto-commit that transaction and start a new one.
        </p>

        <pre class="prettyprint">
        // give a better password to everyone who is using hello as their password
        // but ONLY do this if there are more than 3 such people
        $db->startTransaction();
        $db->query("UPDATE accounts SET password=%s WHERE password=%s", 'sdfwsert4rt', 'hello');
        $counter = $db->affectedRows();
        if ($counter > 3) {
          echo $counter . " people just got their password changed!!\n";
          $db->commit();
        } else {
          echo "No one got their password changed!\n";
          $db->rollback();
        }
        </pre>
    </section>
    <section class="function">
        <h2 id="db_tableList">tableList()</h2>

        <p>Get an array of the tables in the current database.</p>

        <pre class="prettyprint">
        $db_tables = $db->tableList();

        foreach ($db_tables as $table) {
          echo "Table Name: $table\n";
        }
        </pre>
    </section>
    <section class="function">
        <h2 id="db_columnList">columnList()</h2>

        <p>Get an array of the columns in the requested table.</p>

        <h3>Description</h3>
        <pre class="prettyprint">
        $db->columnList(string $tablename [, bool $types = false])
        </pre>

        <h3>Parameters</h3>
        <div class="param">
            <h4>tablename</h4>
            <p>The tablename</p>
        </div>
        <div class="param">
            <h4>types</h4>
            <p>If true, the return result will be an associative array
            of <code>$column_name => $column_type</code></p>
            <p>If false (default), the return result will be a numbered array
            of column names.</p>
        </div>


        <h3>Examples</h3>
        <pre class="prettyprint">
        $columns = $db->columnList('accounts');

        foreach ($columns as $column) {
          echo "Column: $column\n";
        }

        $columns_with_types = $db->columnList('accounts');

        foreach ($columns_with_types as $name => $type) {
          echo "Column: $column, Type: $type\n";
        }
        </pre>
    </section>
    <section class="function">
        <h2 id="db_disconnect">disconnect()</h2>

        <p>Drop the database connection. If you run a query after this, it will automatically reconnect. </p>

        <pre class="prettyprint">
        $db->disconnect();
        </pre>
    </section>
    <section class="function">
        <h2 id="db_get">get()</h2>

        <p>Get the underlying database object</p>
    </section>
    <section class="function">
        <h2 id="db_getDriver">getDriver()</h2>

        <p>Get the underlying driver object. Note that <code>$db->get()</code> (above function) is an alias
        of <code>$db->getDriver()->get()</code></p>
    </section>
</article>