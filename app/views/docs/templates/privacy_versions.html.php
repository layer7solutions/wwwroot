<section data-title="Revisions of this Document">
    <a href="<?php echo SITE_URL ?>privacy">March 5th, 2022 (current)</a>
    <a href="<?php echo SITE_URL ?>privacy/2018.05.26">May 26th, 2018</a>
    <a href="<?php echo SITE_URL ?>privacy/2017.12.22">December 22nd, 2017</a>
</section>