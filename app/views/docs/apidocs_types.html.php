<div class="doc-header cf">
    <h1>Developers // Types</h1>
</div>
<article>
    <header>
        <h1 id="types">API Types</h1>
    </header>
    <div id="toc">
        <a href="#subreddit_notation">Subreddit Notation</a>
        <section>
            <a href="#subreddit_notation_syntax">Notation Syntax</a>
            <a href="#subreddit_categories">Subreddit Categories</a>
            <a href="#subreddit_valid_examples">Valid Examples</a>
            <a href="#subreddit_invalid_examples">Invalid Examples</a>
        </section>
        <a href="#modactiontypes">Mod Action Types</a>
    </div>
    <section class="function">
        <h2 id="subreddit_notation">Subreddit Notation</h2>

        <h3 id="subreddit_notation_syntax">Notation Syntax (in EBNF Grammar)</h3>
        <p>Where the final <code>subreddit</code> grammar is the final notation.</p>
        <pre class="prettyprint">
        digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" ;
        letter = "A" | "B" | "C" | "D" | "E" | "F" | "G"
            | "H" | "I" | "J" | "K" | "L" | "M" | "N"
            | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
            | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
            | "c" | "d" | "e" | "f" | "g" | "h" | "i"
            | "j" | "k" | "l" | "m" | "n" | "o" | "p"
            | "q" | "r" | "s" | "t" | "u" | "v" | "w"
            | "x" | "y" | "z" ;

        srchar = letter | digit | "_" ;
        srname = srchar, srchar, { srchar } ;

        negsep = "-" | "," ;
        possep = "+" | "," ;

        srneglist = srname, { negsep, srname } ;
        srposlist = srname, { possep, srname } ;

        subreddit = srposlist | 'all', [ ":", category ], [ '-', srneglist ] ;
        </pre>
        <p>What <code>category</code> can be equal to is listed in the next section</p>

        <h3 id="subreddit_categories">Subreddit Categories</h3>
        <div class="cf">
            <img src="<?php img_src('developers/srprefspage.png'); ?>" style="
                max-width: 400px;
                border: 2px solid #ddd;
                float: right;
                margin-left: 20px;
            ">
            <p><b>Subreddit Categories</b> are based what TSB modules a subreddit can have enabled.
            If a subreddit has a module enabled, then it is to be considered in that category. If
            it does not have a module enabled, then it is to be considered not in that category.</p>
            <p><b>"all":</b> Subreddit categories can be used to modify the <code>"all"</code> keyword, which, on
            its own, results in all the subreddits you mod that have TheSentinelBot.</p>
        </div>
        <hr/>
        <div>
            <p>For example, <code>all:modmail</code> results in all subreddits you mod that have
            "Modmail Logging" enabled.</p>
            <p>Another example, <code>all:tsb-aww</code> results in all subreddits you mod that have
            "Sentinel" enabled, except /r/aww.</p>
        </div>
        <hr/>
        <p>Here is a list of all the category types currently available:</p>
        <ul>
            <li><p><code>sentinel</code>;
            aliases: <code>tsb</code>, <code>thesentinel</code></p></li>

            <li><p><code>modlog</code>;
            aliases: <code>log</code>, <code>logs</code></p></li>

            <li><p><code>modmail</code>;
            aliases: none</p></li>

            <li><p><code>botban</code>;
            aliases: <code>botban</code>, <code>userban</code></p></li>

            <li><p><code>domainblacklist</code>;
            aliases: <code>domain</code>, <code>domains</code>, <code>domainban</code></p></li>

            <li><p><code>dirtbag</code>;
            aliases: none</li>
        </ul>

        <h3 id="subreddit_valid_examples">Valid Examples:</h3>

        <ul>
            <li><code>aww</code>: just /r/aww</li>
            <li><code>aww,DestinyTheGame,videos</code>: /r/aww, /r/DestinyTheGame, and /r/videos</li>
            <li><code>aww+DestinyTheGame+videos</code>: same as above</li>
            <li><code>aww+DestinyTheGame,videos</code>: same as above</li>
            <li><code>aww,DestinyTheGame+videos</code>: same as above</li>
            <li><code>all</code>: all subreddits you mod that have TSB (TSB subreddits)</li>
            <li><code>all-aww</code>: all subreddits you mod except for /r/aww</li>
            <li><code>all-aww,DestinyTheGame,videos</code>: all TSB subreddits
                you mod except for /r/aww, /r/DestinyTheGame, and /r/videos</li>
            <li><code>all-aww-DestinyTheGame-videos</code>: same as above</li>
            <li><code>all-aww-DestinyTheGame,videos</code>: same as above</li>
            <li><code>all-aww,DestinyTheGame-videos</code>: same as above</li>
            <li><code>all:modlog</code>: all TSB subreddits you mod that have
                mod action logging enabled</li>
            <li><code>all:modlog-aww</code>: all TSB subreddits you mod that have
                mod action logging enabled except for /r/aww</li>
            <li><code>all:modlog-aww,DestinyTheGame,videos</code>: all TSB subreddits you mod that have
                mod action logging enabled except for /r/aww, /r/DestinyTheGame, and /r/videos</li>
            <li><code>all:logs-aww-DestinyTheGame-videos</code>: same as above</li>
        </ul>

        <h3 id="subreddit_invalid_examples">Invalid Examples:</h3>

        <p>These examples are examples of invalid syntax that may cause an error (SR_INVALID) when used:</p>

        <ul>
            <li><code>aww-DestinyTheGame</code>: cannot subtract two subreddits.</li>
            <li><code>all+aww</code>: cannot use concatenation in conjunction with "all".</li>
            <li><code>all,aww</code>: same as above.</li>
            <li><code>all-aww+DestinyTheGame</code>: cannot use <code>+</code> in conjunction with <code>all</code>.</li>
            <li><code>all:modlog,aww</code>: cannot use concatenation in conjunction will categorized "all".</li>
            <li><code>all:modlog+aww</code>: same as above.</li>
        </ul>
    </section>
    <section>
        <h2 id="modactiontypes">Mod Action Types</h2>
        <?php
        $action_types = \app\lib\logs\Common::getActionTypes();
        $action_types_colsize = 3;
        $action_types_cols = array_chunk($action_types, ceil(count($action_types) / $action_types_colsize));
        ?>
        <div class="fsplit fsplit<?php echo $action_types_colsize; ?>">
        <?php
        foreach ($action_types_cols as $col) {
            echo '<div class=""><ul>';
            foreach ($col as $type) {
                echo "<li><code>";
                xecho($type);
                echo "</code></li>";
            }
            echo '</ul></div>';
        } ?>
        </div>
    </section>
</article>