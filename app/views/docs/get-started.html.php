<article>
    <header>
        <h1 data-doctitle="Getting Started with Layer7">Getting started with Layer 7</h1>

        <p>At the core of all our services is TheSentinelBot, which makes all the magic happen.
        Rather than have multiple bots, we have all our services under one bot. And these services
        can all be turned off or on through our website.</p>
    </header>
    <div id="toc" class="no-fixed">
        <a href="#adding-tsb">Adding TheSentinelBot</a>
        <a href="#logging-in">Logging in</a>
    </div>
    <section class="function">
        <h2 id="adding-tsb">Step 1: Add The Sentinel to your team</h2>

        <p>Due to Reddit's API rate limits, we have multiple instances (bot accounts) of TheSentinelBot.
        Follow these instructions to figure out which bot to add:</p>

        <ul>
            <li>Check the number of subscribers your subreddit has</li>
            <li>If the sum of a bot's <b>Current Subscribers</b> and your subscribers is less than
            <b>Max Subscribers</b>, then you can add that bot</li>
            <li>At a minimum the bot needs the <code>posts</code> permission.</li>
        </ul>

        <?php render_template('data.tsb_availability_table'); ?>
    </section>
    <section class="function">
        <h2 id="logging-in">Step 2: Login!</h2>

        <div class="box">
            <?php if (user()->logged_in()): ?>
                <span>You're already logged in. Thanks for joining us!</span>
            <?php else: ?>
                <a href="<?php echo SITE_URL ?>"><button class="primary">Click here to login</button></a>
            <?php endif; ?>
        </div>

        <p>Once you've logged in, you can go to <code><?php echo SITE_URL ?>prefs/r/YOUR_SUBREDDIT</code>
        to enable or disable features of TheSentinelBot.</p>

        <strong>Here's a quick overview of the features currently available:</strong>

        <h3>Media Blacklisting</h3>

        <p>One of the most powerful capabilities we have is media blacklisting.
        This allows a subreddit to blacklist an entire media channel (Youtube channels, Vimeo, Dailymotion, etc.),
        and know that regardless of which users make a link post, self post, or comment including a
        link to that media channel, it will be removed. In addition to a local subreddit
        blacklist that you manage, we also have a global blacklist controlled by the moderators of /r/Layer7
        and /r/TheSentinelBot  to ensure that global spam is met with swift action.</p>

        <h3>Advanced Mod logs</h3>

        <p>Overcome the limitations of the modlog and search in any way imaginable: such as
        inputting a thing id and seeing all actions related to that post or comment, searching
        by author, mod, title, etc. If you set an action_reason in AutoModerator, you can even
        filter by that enabling you to see how useful your AutoMod rules are. You can even build
        a mod matrix in less than a second allowing you to know who's the powermod in your group,
        or what the most common actions are.</p>

        <h3>Have any questions?</h3>

        <a href="<?php echo SITE_URL ?>docs/faq">Try checking out our FAQ.</a>
        If our FAQ page doesn't answer your questions, please feel free to make a post on
        <a href="https://www.reddit.com/r/Layer7">/r/Layer7</a> with your question(s).
    </section>
</article>