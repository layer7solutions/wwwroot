<article>
    <header>
        <h1 id="guide">Style guide and Standards</h1>
    </header>
    <section>
        <h2>General</h2>

        <h3>PHP tags</h3>

        <ul>
            <li>PHP tags MUST always be <code>&lt;?php</code> and <code>?&gt;</code>. Alternative PHP
            tags such as <code>&lt;?</code>, <code>&lt;%</code>, <code>&lt;%=</code>,
            <code>%&gt;</code> are NEVER allowed.</li>
            <li>The closing tag <code>?&gt;</code> MUST be omitted from files containing only PHP.</li>
        </ul>

        <h3>Lines</h3>

        <ul>
            <li>There is not a hard limit on line length, but avoid excessively long lines.</li>
            <li>There MUST NOT be trailing whitespace at the end of non-blank lines.</li>
            <li>Blank lines MAY be added to improve readability and to indicate related blocks of code.</li>
            <li>There MUST NOT be more than one statement per line.</li>
        </ul>

        <h3>Indenting</h3>

        <ul>
            <li>Code MUST use an indent of 4 spaces, and MUST NOT use tabs for indenting.</li>
        </ul>

        <h3>Keywords</h3>

        <ul>
            <li>PHP <a href="http://php.net/manual/en/reserved.keywords.php">keywords</a> MUST be in lower case.</li>
            <li><code>true</code>, <code>false</code>, and <code>null</code> MUST be in lower case.</li>
        </ul>

        <h2>Namespaces and Use Declarations</h2>

        <ul>
            <li>When present, there MUST be one blank line after the namespace declaration.</li>
            <li>When present, all <code>use</code> declarations MUST go after the namespace declaration.</li>
            <li>There MUST be one blank line after the <code>use</code> block.</li>
        </ul>

        <h2>Naming conventions</h2>

        <ul>
            <li>The following two rules are necessary for the SPL Autoload to work. If you have a namespace
            or class that is never intended to be SPL autoloaded, then these two rules may be ignored.
                <ul>
                    <li>Namespaces MUST match the folder path the file is in. For example,
                    <code>app/lib/Subreddit.php</code> should have the namespace of <code>app\lib</code>.</li>
                    <li>Class names MUST match the file name it is in. For example, the class inside
                    <code>app/lib/Subreddit.php</code> MUST be <code>Subreddit</code></li>
                </ul>
            </li>
            <li>Class names MUST be in upper camel case.</li>
            <li>PHP files that do not represent a class MUST start with a lower case letter</li>
            <li>All directories used by the application MUST be all lowercase. This does not
            include directories like <code>Documentation/</code> and third-party libraries
            like <code>bootstrap/oauth/oauth2-client-php/</code></li>
            <li>Variables and functions MUST start with a lower case letter.</li>
        </ul>

        <h2>Control Structures</h2>

        <ul>
            <li>The opening brace, <code>{</code>, MUST be on the same line as the keyword.</li>
            <li>There MUST be one space before the opening brace,.</li>
            <li>There MUST be one space after the control structure keyword.</li>
            <li>There MUST NOT be a space after the opening parenthesis.</li>
            <li>There MUST NOT be a space before the closing parenthesis.</li>
            <li>The structure body MUST be indented once.</li>
            <li>The closing brace MUST be on the next line after the body.</li>
            <li>The body of each structure MUST be enclosed by braces. This standardizes
            how control structures look and reduces the likeihood of introducing errors as news lines
            get added to the structure body.</li>
        </ul>
    </section>
</article>