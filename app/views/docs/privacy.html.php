<article>
    <header>
        <h1 data-doctitle="Privacy Policy | Layer7">Layer 7 Solutions - Privacy Policy</h1>
        <p>Modified: March 5th, 2022.<br/>
            Effective: March 5th, 2022.</p>
    </header>
    <div id="toc" class="no-fixed split">
        <section data-title="Privacy Policy">
            <a href="#infrastructure">Infrastructure</a>
            <a href="#what_and_why">What and Why</a>
            <section>
                <a href="#what_and_why_p1">On Discord Communities employing our bots</a>
                <a href="#what_and_why_p2">On Reddit Communities employing our bots</a>
                <a href="#what_and_why_p3">On users of our websites</a>
                <a href="#what_and_why_p4">Discord Bots for 10 Chambers AB</a>
            </section>
            <a href="#how">How</a>
            <section>
                <a href="#how_p1">Services</a>
                <a href="#how_p2">Retention</a>
            </section>
            <a href="#with_whom">With Whom</a>
            <section>
                <a href="#with_whom_p1">Essential Third-Party Services</a>
                <a href="#with_whom_p2">The Mod Team of a Subreddit</a>
                <a href="#with_whom_p3">Those You Consent To</a>
                <a href="#with_whom_p4">Our Staff</a>
                <a href="#with_whom_p5">Law &amp; Order</a>
                <a href="#with_whom_p6">And Nobody Else</a>
            </section>
            <a href="#information_security">Information Security</a>
            <a href="#your_choices">Your Choices</a>
            <a href="#changes">Changes</a>
            <a href="#contact">Contact</a>
        </section>
        <?php render_template('docs.templates.privacy_versions'); ?>
        <?php render_template('docs.templates.legaldocs_list'); ?>
    </div>
    <section style="padding-bottom: 0;">
        <p>Layer 7 Solutions, LLC ("Layer 7 Solutions", "Layer7.Solutions", "us", "we", or "our")
            operates our website, software, and services ("Service", "Services").
            This Privacy Policy informs you of our policies regarding how we collect, use,
            and handle your information</p>
        <div class="box">
            <h4 id="version_changes">Changes from the Previous Version (May 26th, 2018)</h4>
            <p>We did a complete review to make sure it reflects our current standing. We've added sections about usage
                of our Discord bots.</p>
        </div>
    </section>
    <section class="function">
        <h2 id="infrastructure">Infrastructure</h2>

        <p>Our infrastructure consists of two physical servers with multiple Virtual Machines that are co-location
            hosted in
            a <a href="https://glesys.com">GleSYS AB</a> datacenter in Sweden where they have many
            <a href="https://glesys.com/compliance-security">security practices</a> in place to ensure the physical and
            digital security
            of our infrastructure. In addition, we have a limited list of those who are allowed physical access to our
            servers, and we require
            SSH keys along with limiting the number of staff who can remotely access the servers. We even further limit
            the number
            of staff who can access the user data stored on our servers.</p>
    </section>
    <section class="function">
        <h2 id="what_and_why">What and Why</h2>

        <h4 id="what_and_why_p1">On Discord Communities employing our bots</h4>

        <p>Our Discord bots, unless otherwise specified, use the same codebase and fall under this same umbrella
            of information collected. This information is intended to be the minimum needed for the Discord server
            moderation teams to be able to moderate their communities. If any text content is saved, it is
            encrypted at rest.</p>

        <ul>
            <li>Note, Warn, Mute, Kick, Ban: We store the text of the content that a moderator specifically enters.
                This is so the moderators can recall that information for the purpose of moderation.
            </li>
            <li>Message ID, Channel ID, User, Server: No personal information is stored. This meta data helps
                us with various analytics and connects with other features.
            </li>
            <li>Mod Mail - Channel ID, User Channel ID, User, Message ID, Server ID: No personal information is stored.
                This meta data helps us with various analytics and connects with other features.
            </li>
            <li>Reminder - User, Remind Text: We have to and only store the text you specify so that we can
                remind you about it later on.
            </li>
            <li>Server Name: This is so we can show the friendly server name when you use our website.</li>
            <li>Tags - Tag Name, Tag Content: We only store the text you specify so that we can recall and show
                that information to users when they use that tag.
            </li>
            <li>Deleted & Edited Messages - Message Content: If you edit or delete a message, the content of that
                message is posted to a Discord channel, if the moderators chose to setup this feature. This is
                meant so moderators are better equipped at keeping their community safe. You can request that
                this data is removed my reaching out to that Discord communities staff.
            </li>
            <li>Username, Nickname, & Profile Pictures: If the Discord community staff chose to enable this feature,
                the bot will record in a Discord channel this content for purpose of moderation. You can request that
                this data is removed my reaching out to that Discord communities staff.
            </li>
        </ul>

        <p>The information is only shared with the intended, limited recipient. Meaning, information collected for
            the purpose of moderation is only shared with that Discord communities moderation team, which is
            determined based on the users' permission in the server. The content inputted while using the reminder
            command is only shared with the person the reminder is for. The content inputted for a Tag command
            is meant to be public and freely visible by anyone who can send messages in the server.</p>

        <p>We do not share any of this information with third parties, unless otherwise required by law. We do
            not share this information with users if they would normally not have access to it. If you'd like to request
            that your information be removed, please message "D0cR3d#0001 (82942340309716992)" on <a href="http://layer7.xyz/benedict">Discord.</a></p>

        <h4 id="what_and_why_p2">On subreddits employing our bots</h4>

        <p>As a baseline, when our Reddit bots are added as moderator we will begin recording all posts and
            comments made to the subreddit. This includes body, title, author, time posted, the unique identifier
            of the post, a permalink to the post/comment, any flair attributed to the post, and the outgoing URL
            of link posts.</p>

        <p>TheSentinelBot has several modules that may be enabled or disabled at will of the mod team through
            the settings pages of our website. Should features be enabled, additional data will be collected in
            order to provide the service that was enabled.</p>

        <p>The following information has been retained as part of previous service offerings, many of which
            have been disabled. We are in the process of identifying the information that is no longer being used
            so that we can permanently remove it from our storage.</p>

        <ul>
            <li>The collection of posts and comments title, message content, author name</li>
            <li>Subreddits using Repost Sentinel would have posts title, media link, author name, and hash of media</li>
        </ul>

        <h4 id="what_and_why_p3">On users of our websites</h4>

        <p>By logging into our website using your Reddit.com account, you agree to give us access to
            information afforded to us by Reddit.com limited to these scopes: identity, mysubreddits, read.
            These scopes give us access to: posts and comments through your account; the list of subreddits
            you moderate, contribute to, and subscribe to; and your reddit username and signup date. However,
            our website specifically only stores and uses: your reddit username, the list of subreddits you mod,
            and the list of moderators and their permissions on each subreddit you mod with The Sentinel Bot.
            This information will be kept indefinitely unless manually revoked through user request.</p>

        <p>By logging into our website using your Discord.com account, you agree to give us access to
            information afforded to us by Discord.com limited to these scopes: identity.
            These scopes give us access to: Username, Avatar, Banner.
            These are necessary so that we can grant you access to our Bots features on the website.
        </p>

        <p>We may collect limited non-personally identifying information your browser makes
            available whenever you visit a website. This log information may include but not limited to your
            Internet Protocol address, browser type, browser language, the date and time of your query and
            one or more cookies that may uniquely identify your browser. We use this information to operate,
            develop and improve our Services. This information may be kept indefinitely.</p>

        <p>A "cookie" is a small file containing a string of characters that is sent to your computer when
            you visit a website. We use cookies to improve the quality of our service. You can reset your browser
            to refuse all cookies or to indicate when a cookie is being sent. However, some website
            features or Services, namely the ability to login to our website, may not function properly without
            cookies. We only use cookies for session management purposes. There also may be third-party cookies
            set by Cloudflare, our Content Delivery Network provider.</p>

        <h4 id="what_and_why_p4">Discord Bots for 10 Chambers AB</h4>

        <p>Layer 7 Solutions creates, runs, and manages certain Discord bots for 10 Chambers AB.
            The following bots are also covered by <a href="http://10chambers.com/privacy/Privacy_Policy.pdf">10 Chambers AB Privacy Policy</a>,
            <a href="http://10chambers.com/toc/Terms_and_Conditions.pdf">10 Chambers AB Terms and Conditions</a>, or
            other agreements in addition to our own.</p>

        <ul>
            <li>"GTFO Bot#3773"</li>
            <ul>
                <li>Saves only the User ID, Channel ID, Message ID, and the message content from certain channels.
                    If a message is deleted from Discord, then it's also deleted from our Database. This message content
                    is anonymized
                    and uploaded to <a href="https://www.pulsarplatform.com">Pulsar.</a> This is to help with providing
                    a
                    better quality experience in GTFO.
                </li>
                <li>If you send a Direct Message / Private Message to the bot with your email, we will use that to look
                    up
                    and validate your subscription to our <a href="https://mailchimp.com">Mailchimp mailing list.</a>
                    This is to provide you with a role in our GTFO Discord server.
                </li>
            </ul>
            <li>The Warden#4195 (516697954405580821)</li>
            <ul>
                <li>If you send a Direct Message / Private Message to the bot with an audio file, your voice and that
                    file is securely saved on the Layer 7 Solutions infrastructure, and then securely transferred to the
                    10 Chambers AB infrastructure. This is outlined in the Participation Agreement.
                </li>
            </ul>
            <li>GTFO Mod Mail#4555 (553357041586929704)</li>
            <ul>
                <li>Please see the <a href="<?php echo SITE_URL ?>privacy#what_and_why_p1">following section.</a></li>
            </ul>
        </ul>
    </section>
    <section class="function">
        <h2 id="how">How</h2>

        <p><b id="how_p1">Services</b>
            The information collected by all Services is used to provide, improve, and
            protect our Services. We do not hand out or sell any of this information to anyone.

        <p><b id="how_p2">Retention</b>
            If the staff team chooses to remove one of our bots from their community, no further information will
            be collected on that community or the users of it, but for security reasons, such as a rogue
            moderator, and to continue the service should the staff choose to add the Bot back, existing collected
            information shall be retained. To request your information to be deleted, please reach out to
            "D0cR3d#0001 (82942340309716992)" on <a href="http://layer7.xyz/benedict">Discord.</a>
        </p>
    </section>
    <section class="function">
        <h2 id="with_whom">With Whom</h2>

        <h4 id="with_whom_p1">Essential Third-Party Services</h4>
        <p>We may use third-party services to store, process, or transmit data, or perform other technical
            functions related to operating our Services. These third-party services currently only include
            the Reddit.com API, Discord.com API, our server host (GleSYS AB), our Content Delivery Network
            (Cloudflare), and our DNS Registrar (Cloudflare Registrar).

        <h4 id="with_whom_p2">The Mod Team of a Community</h4>
        <p>If the mod team of a community asks for information we have regarding their community, we may
            provide that information to them.</p>

        <h4 id="with_whom_p3">Those You Consent To</h4>
        <p>We may offer our information to any third party provided that you give your explicit consent.</p>

        <h4 id="with_whom_p4">Our Staff</h4>
        <p>Your information may be accessible to any Layer 7 Solutions employee or authorized representative
            who needs to access that information in order to operate, develop or improve our Services.</p>

        <h4 id="with_whom_p5">Law &amp; Order</h4>
        <p>We may preserve or disclose your information if we believe that it is reasonably necessary to comply with
            a law, regulation, legal process, or governmental request; or to protect the physical safety of any
            person.</p>

        <h4 id="with_whom_p6">And Nobody Else</h4>
        <p>We do not rent, sell, or give away any information to any third parties.</p>
    </section>
    <section>
        <h2 id="information_security">Information Security</h2>

        <p>We take appropriate security measures to protect against unauthorized access to or
            unauthorized alteration, disclosure or destruction of data.</p>

        <p>If we terminate your service, we may retain enough information to prevent you
            from signing up for the service in the future.</p>

        <p>On the <strong><a href="https://www.reddit.com/account-activity">Reddit Account Activity</a></strong>
            page, our <a href="#what_and_why_p2">limited OAuth access</a> to your Reddit account will
            show up with the IP address "46.246.41.96/27" and the organization name, "GleSYS AB", which is our
            server's host</p>
    </section>
    <section>
        <h2 id="your_choices">Your Choices</h2>

        <p>We respect user's rights to privacy and will delete any user's content from our
            server(s) and database(s) should the user contact us.</p>

        <p>We do not offer any users to opt-out from having their user content collected
            on communities that employ our Services, due to the necessity for the purpose of moderation,
            and allowing community staff to provide a healthy community. If you wish to opt-out, we recommend
            not participating in that community.</p>
    </section>
    <section>
        <h2 id="changes">Changes</h2>

        <p>We may revise this Privacy Policy from time to time, the most recent version will always be at
            <a href="<?php echo SITE_URL ?>privacy"><?php echo SITE_URL ?>privacy</a>.
            If a revision meaningfully reduces your rights, we will notify you.</p>
    </section>
    <section>
        <h2 id="contact">Contact</h2>

        <p>Still have questions? Contact us by messaging "D0cR3d#0001 (82942340309716992)" on <a href="http://layer7.xyz/benedict">Discord.</a></p>
    </section>
</article>