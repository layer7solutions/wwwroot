<div class="valign">
    <h3>Global Blacklist <?php if (b_stash('is_admin')): ?>- Manage Queue<?php else: ?>Request<?php endif; ?></h2>
    <button id="GlobalQueue__RefreshButton" class="tertiary spacer-left">refresh</button>
</div>
<?php if (!b_stash('is_admin')): ?>
    <section class="GlobalQueueForm">
        <h4>New Request</h4>
        <div id="GlobalQueue__Enqueue">
            <div class="talign">
                <div class="halign">
                    <input id="GlobalQueue__EnqueueURL" type="url" placeholder="Enter media URL" />
                    <textarea id="GlobalQueue_EnqueueReason" placeholder="Reason (optional)"></textarea>
                </div>
                <div class="valign">
                    <button id="GlobalQueue__EnqueueButton" class="primary">Send Request</button>
                    <span id="GlobalQueue__EnqueueStatus" class="hide" style="margin-left:10px;">Please wait...</span>
                    <span id="GlobalQueue__EnqueueError" style="margin-left:10px;color:red"></span>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<section class="GlobalQueueResult">
    <h4>Pending Requests</h4>
    <div id="GlobalQueueResult--pending" class="GlobalQueueResult--output"></div>
</section>
<section class="GlobalQueueResult">
    <h4>Recently Approved Requests</h4>
    <div id="GlobalQueueResult--approved" class="GlobalQueueResult--output"></div>
</section>
<section class="GlobalQueueResult">
    <h4>Recently Denied Requests</h4>
    <div id="GlobalQueueResult--denied" class="GlobalQueueResult--output"></div>
</section>