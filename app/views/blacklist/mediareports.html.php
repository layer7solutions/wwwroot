<h3 class="spacer-bottom">Reports Page</h3>

<?php
$idx = 0;
foreach (xa_stash('search_options') as $option):
    $id = function($x, $cls = null) use ($option, $idx) {
        echo ' id="'.$x.'--'.$option['type'].'" ';
        echo ' data-type="'.$option['type'].'" ';
        echo ' data-form-index="'.$idx.'" ';
        echo ' class="'.$x;
        if (isset($cls)) {
            echo ' '.$cls;
        }
        echo '" ';
    };
?>
<section <?php $id('MediaReport', 'spacer-vert') ?>>
    <h4><?php echo $option['title'] ?></h4>
    <div <?php $id('MediaReport__Inner') ?>>
        <div <?php $id('MediaReportForm') ?>>
            <div <?php $id('MediaReportForm__SubjectInputArea') ?>>
                <input <?php $id('MediaReportForm__SubjectInput') ?> type="text"
                    placeholder="<?php echo $option['placeholder'] ?>" />
            </div>
            <div <?php $id('MediaReportForm__Error', 'error-notice hide') ?>></div>
            <div <?php $id('MediaReportForm__SearchButtonArea') ?>>
                <button <?php $id('MediaReportForm__SearchButton', 'primary primary--2') ?>>Search</button>
            </div>
        </div>
        <div <?php $id('MediaReportResult', 'card hide') ?>>
            <input type="hidden" value="0" name="CurrentAmount" />
            <input type="hidden" value="0" name="CurrentSubject" />
            <div class="header">
                <span>
                    <span>Results</span>
                    <span <?php $id('MediaReportOutput__AmountLabel', 'status spacer-left') ?>>n/a</span>
                </span>
                <div class="close small"></div>
            </div>
            <div <?php $id('MediaReportOutput__Tabs', 'card-tabs') ?>>
                <span <?php $id('MediaReportTab--name' , 'MediaReportTab selected') ?> name="list">List</span>
                <span <?php $id('MediaReportTab--group', 'MediaReportTab') ?> name="group">Group</span>
                <!-- <span <?php $id('MediaReportTab--graph', 'MediaReportTab') ?> name="graph">Network</span> -->
            </div>
            <div <?php $id('MediaReportOutput') ?>></div>
        </div>
    </div>
</section>
<?php $idx++; endforeach; ?>