<div class="talign">
    <div class="app-aside-outer">
        <div class="app-aside-float">
            <aside class="app-aside">
                <button class="primary TSBForm__SubmitButton TSBForm__SubmitButton--Add"
                    data-action-type="add">Add to blacklist</button>
                <hr/>
                <button class="secondary TSBForm__SubmitButton TSBForm__SubmitButton--Remove"
                    data-action-type="remove">Remove from blacklist</button>
                <button class="secondary TSBForm__SubmitButton TSBForm__SubmitButton--Check"
                    data-action-type="check">Check if blacklisted</button>
                <div class="TSBForm__SupportedPlatforms">
                    <span class="TSBForm__SupportedPlatformsLabel">Supported Platforms</span>
                    <ul class="TSBForm__SupportedPlatformsList">
                        <li><i title="YouTube" class="icon media-icon media-icon--youtube zmdi zmdi-youtube"></i></li>
                        <li><i title="Vimeo" class="icon media-icon media-icon--vimeo zmdi zmdi-vimeo"></i></li>
                        <li><img title="DailyMotion" class="icon media-icon media-icon--dailymotion" src="<?php echo SITE_URL ?>assets/images/media-host/dailymotion_icon.svg" /></li>
                        <li><i title="Soundcloud" class="icon media-icon media-icon--soundcloud zmdi zmdi-soundcloud"></i></li>
                        <li><i title="Twitch" class="icon media-icon media-icon--twitch zmdi zmdi-twitch"></i></li>
                        <li><i title="Twitter" class="icon media-icon media-icon--twitter zmdi zmdi-twitter"></i></li>
                        <li><i title="Facebook" class="icon media-icon media-icon--facebook zmdi zmdi-facebook-box"></i></li>
                        <li><i title="Etsy" class="icon media-icon media-icon--etsy fa fa-etsy"></i></li>
                    </ul>
                </div>
                <p class="TSBError TSBError--internal form-error hide spacer-horiz">An internal error occurred</p>
                <p class="TSBError TSBError--no_urls form-error hide spacer-horiz">No URLs entered</p>
                <p class="TSBError TSBError--no_subreddits form-error hide spacer-horiz">No subreddits selected</p>
                <p class="TSBError TSBError--invalid_urls form-error hide spacer-horiz">One or more URLs are invalid.</p>
            </aside>
        </div>
    </div>
    <div class="form form--tsb TSBForm">
        <div class="spacer-bottom">
            <div class="spacer-bottom valign">
                <h4 class="grow">Enter Media URLs</h4>
                <div>
                    <button class="secondary secondary--icon secondary--hover MediaTargetClearButton">Clear All</button>
                    <button class="secondary secondary--icon secondary--hover MediaTargetAddButton"><i class="zmdi zmdi-plus"></i></button>
                </div>
            </div>
            <div class="MediaTargetList">
                <div class="MediaTargetList__contents"></div>
            </div>
        </div>
        <div>
            <div class="spacer-bottom cf">
                <h4>Select Subreddits</span>
                <?php if (user()->admin()): ?>
                <small class="fr">(admin)</small>
                <?php endif; ?>
            </div>
            <div id="TSBForm__SRFilter" class="SubredditChooser SubredditChooser--TSBForm">
                <div class="SubredditChooserTop">
                    <label class="SubredditOption SubredditOption--all">
                        <input type="checkbox" />
                        <span>All Subreddits</span>
                    </label>
                    <?php if (user()->is_globalist()): ?>
                    <label class="SubredditOption">
                        <input type="checkbox" data-value="thesentinelbot" />
                        <span>Global Blacklist</span>
                    </label>
                    <?php endif; ?>
                </div>
                <?php foreach (xa_stash('subreddits') as $sr):
                if (user()->is_globalist() && strtolower($sr) == 'thesentinelbot') {
                    continue;
                } ?>
                <label class="SubredditOption">
                    <input type="checkbox" data-value="<?php echo strtolower($sr) ?>" />
                    <span><?php
                        if (strtolower($sr) == 'thesentinelbot') {
                            echo('<b>Global Backlist</b>');
                        } else {
                            echo $sr;
                        }
                    ?></span>
                </label>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>