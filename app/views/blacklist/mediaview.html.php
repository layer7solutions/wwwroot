<div class="valign spacer-bottom">
    <h4 class="spacer-right">View Blacklist</h4>
    <button class="secondary TSBViewForm__resetButton">Clear query</button>
</div>
<div>
    <div class="form form--tsb TSBViewForm">
        <div class="card">
            <div id="TSBForm__SRFilter" class="SubredditChooser SubredditChooser--TSBViewForm">
                <div class="header SubredditChooserTop">
                    <div class="spacer-right">Select Subreddits</div>
                    <div class="grow">
                        <label class="SubredditOption SubredditOption--all" style="width:initial">
                            <input type="checkbox" />
                            <span>All Subreddits</span>
                        </label>
                        <?php if (user()->is_globalist()): ?>
                        <label class="SubredditOption" style="width:initial">
                            <input type="checkbox" data-value="thesentinelbot" />
                            <span>Global Blacklist</span>
                        </label>
                        <?php endif; ?>
                    </div>
                    <button class="TSBForm__SRFilterCollapseButton secondary">Collapse</button>
                </div>
                <?php foreach (xa_stash('subreddits') as $sr):
                if (user()->is_globalist() && strtolower($sr) == 'thesentinelbot') {
                    continue;
                } ?>
                <label class="SubredditOption">
                    <input type="checkbox" data-value="<?php echo strtolower($sr) ?>" />
                    <span><?php
                        if (strtolower($sr) == 'thesentinelbot') {
                            echo('<b>Global Backlist</b>');
                        } else {
                            echo $sr;
                        }
                    ?></span>
                </label>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div id="TSBViewForm__outputWrapper" class="card posRel">
        <div class="header">
            <div id="TSBViewForm__ItemCountLabel" class="marker-label marker-label--toggle dropdown--trigger">
                <span><span id="TSBViewForm__ItemCountLabelValue" class="dropdown--label">25</span> per page</span>
                <i class="icon zmdi zmdi-chevron-down"></i>
                <div id="TSBViewForm__ItemCountDropdown" class="dropdown">
                    <span>10</span>
                    <span class="selected">25</span>
                    <span>50</span>
                    <span>100</span>
                    <span>250</span>
                    <span>500</span>
                </div>
            </div>
            <span class="grow"></span>
        </div>
        <div id="TSBViewForm__output" class="row hide"></div>
        <div id="TSBViewForm__status" class="output-status">No subreddits selected</div>
        <div id="TSBViewForm__pendingOverlay" class="pending-overlay hide">
            <div class="loading loading--double"></div>
            <div id="TSBViewForm__waitingText" class="">waiting</div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <span>Export Wizard</span>
        </div>
        <div class="content">
            <p>Make sure to select the subreddit(s) you want to export the media blacklist for and
            then click the <strong>Begin Export</strong> button.</p>

            <p style="margin-top:10px">Do not close the tab once it starts. The <strong>Download File</strong> button will
            be enabled once it's ready to be downloaded.</p>

            <p style="margin-top:10px">You may have to rename the downloaded file to add a <code>.json</code>
            file extension if it doesn't have one already.</p>

            <p style="margin-top:10px">You can only run the export tool once per page refresh, if you
            want to do another export, then refresh the page.</p>

            <div class="valign" style="margin-top:20px">
                <button id="TSBViewForm__beginExportButton" class="primary">Begin Export</button>
                <button id="TSBViewForm__downloadExportButton" class="primary" disabled>Download File</button>
                <span id="TSBViewForm__exportPendingIcon" class="loading hide"></span>
                <span id="TSBViewForm__exportReadyLabel" class="hide" style="margin-left:15px">Ready to download.</span>
            </div>
        </div>
    </div>
</div>