<h4 class="spacer-bottom"><?php echo x_stash('blacklist_title') ?></h4>
<?php if (is_stashed('blacklist_desc')): ?>
    <p class="spacer-bottom"><?php echo x_stash('blacklist_desc') ?></p>
<?php endif; ?>
<div class="card" id="SBTAdd__Parent">
    <div class="header">
        <span>Ban or Unban <span class="SBT__subjectPlural"></span></span>
    </div>
    <div class="content">
        <p>Separate multiple items by commas, spaces, new lines, pipes, or semi-colons. Invalid items will be auto-removed.</p>
        <div class="spacer10-top">
            <textarea id="SBTAction__subjectInput" class="SBT__subjectPluralPlaceholder" placeholder="enter "></textarea>
        </div>
        <div class="spacer5">
            <hr/>
        </div>
        <div class="spacer5">
            <div class="spacer10-bottom">
                <label class="ui-radio" for="SBTAction__subredditInputOption--OnlyThese">
                    <input name="SBTAction__subredditInputOption"
                        id="SBTAction__subredditInputOption--OnlyThese" type="radio" checked />
                    <span>From only the following subreddits</span>
                </label>
                <label class="ui-radio" for="SBTAction__subredditInputOption--AllExcept">
                    <input name="SBTAction__subredditInputOption"
                        id="SBTAction__subredditInputOption--AllExcept" type="radio" />
                    <span>From all subreddits except the following</span>
                </label>
            </div>
            <select id="SBTAction__subredditInput"></select>
        </div>
        <div class="spacer-top valign">
            <button id="SBTAdd__submitButton" class="primary no calign" style="min-width:98px;height:30px">
                <span id="SBTAdd__submitButtonText" class="valign calign"><i class="zmdi zmdi-block spacer10-right"></i> Ban</span>
                <span id="SBTAdd__submitButtonPending" class="loading small hide valign loading--white"></span>
            </button>
            <button id="SBTRemove__submitButton" class="primary yes calign" style="min-width:98px;height:30px">
                <span id="SBTRemove__submitButtonText" class="valign calign"><i class="zmdi zmdi-check spacer10-right"></i> Unban</span>
                <span id="SBTRemove__submitButtonPending" class="loading small hide valign loading--white"></span>
            </button>
        </div>
        <div class="spacer-top hide forward w100p" id="SBTAction__resultsContainer">
            <div class="forward_arrow--top" style="left:40px"></div>
            <div id="SBTAction__resultsInnerContainer">
                <div class="valign spacer10-top spacer5-horiz">
                    <h5 id="SBTAction_resultsTitle" class="grow" style="line-height: 16px;">RESULTS</h5>
                    <span class="close small opacity30p"></span>
                </div>
                <p id="SBTAction__results"></p>
            </div>
        </div>
    </div>
</div>
<div class="card" id="SBTView__Parent">
    <div class="header">
        <span>View Ban List</span>
    </div>
    <div class="content">
        <div class="grow dispFlex talign">
            <div class="spacer5-right" style="width:35%">
                <select id="SBTView__subredditInput"></select>
            </div>
            <div class="spacer5-right" style="width:30%">
                <input type="text" id="SBTView__subjectFilter" placeholder="filter "/>
            </div>
            <select id="SBTView__amountPerPage">
                <option value="5">5 per page</option>
                <option value="10">10 per page</option>
                <option value="15">15 per page</option>
                <option value="20">20 per page</option>
                <option value="25" selected>25 per page</option>
                <option value="50">50 per page</option>
                <option value="100">100 per page</option>
                <option value="200">200 per page</option>
            </select>
            <button id="SBTView__refreshButton" class="primary">Apply/Refresh</button>
        </div>
        <hr/>
        <div class="fsplit">
            <button class="SBTView__prevButton primary primary--2" disabled>Prev Page</button>
            <div class="grow textAlignCenter">
                <input type="text" disabled value="1" class="SBTView__currentPage" />
                <span>/</span>
                <span class="SBTView__maxPage">?</span>
            </div>
            <button class="SBTView__nextButton primary primary--2" disabled>Next Page</button>
        </div>
    </div>
    <div class="posRel" style="min-height:140px">
        <table id="SBTView__table">
            <thead>
                <tr>
                    <th>Subreddit</th>
                    <th class="SBTView__colHead--subject"></th>
                    <th class="SBTView__colHead--mod"></th>
                    <th class="SBTView__colHead--time"></th>
                    <th style="width: 122px;"></th>
                </tr>
            </thead>
            <tbody id="SBTView__tableBody"></tbody>
        </table>
        <div class="pending-overlay hide"  id="SBTView__loading">
            <span class="loading small"></span>
        </div>
    </div>
    <div class="content">
        <div class="fsplit">
            <button class="SBTView__prevButton primary primary--2" disabled>Prev Page</button>
            <div class="grow textAlignCenter">
                <input type="text" disabled value="1" class="SBTView__currentPage" />
                <span>/</span>
                <span class="SBTView__maxPage">?</span>
            </div>
            <button class="SBTView__nextButton primary primary--2" disabled>Next Page</button>
        </div>
    </div>
</div>
<div class="spacer" style="margin-bottom:70px"></div>