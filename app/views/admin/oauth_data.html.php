<?php ?>
<h3>OAuth Data</h3>
<div class="box spacer-bottom">
    <div class="fsplit">
        <div>
            <div class="field">
                <label>Username:</label>
                <input type="text" id="TSBAuth_InsertUsername" />
            </div>
            <div class="field">
                <label>Password:</label>
                <input type="text" id="TSBAuth_InsertPassword" />
            </div>
            <div class="field">
                <label>Agent Of:</label>
                <input type="text" id="TSBAuth_InsertAgentOf" />
            </div>
        </div>
        <div class="grow spacer-left">
            <div class="field">
                <label>App ID:</label>
                <input type="text" id="TSBAuth_InsertAppID" />
            </div>
            <div class="field">
                <label>App Secret:</label>
                <input type="text" id="TSBAuth_InsertAppSecret" />
            </div>
            <div class="field">
                <label>&nbsp;</label>
                <button class="TSBAuth_InsertSubmit primary primary--2">Add</button>
            </div>
        </div>
    </div>
</div>
<div>
    <table class="TSBAuth_Table">
        <thead>
            <tr>
                <th>Agent Of</th>
                <th>Username</th>
                <th>Password</th>
                <th>App ID</th>
                <th>App Secret</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $u_items = u_stash('oauth_data');
            $u_items[] = [
                'agent_of' => 'template',
                'username' => 'template',
                'app_id' => 'template',
            ];
            foreach ($u_items as $u_item): ?>
            <tr class="TSBAuth_Row <?php if ($u_item['agent_of'] == 'template') echo 'hide'; ?>"
                    data-for="<?php xecho($u_item['app_id']) ?>">
                <td class="TSBAuth_AgentOf"><?php xecho($u_item['agent_of']) ?></td>
                <td class="TSBAuth_Username"><?php xecho($u_item['username']) ?></td>
                <td><a class="TSBAuth_GetPassword" data-for="<?php xecho($u_item['app_id']) ?>">Click to request</a></td>
                <td class="TSBAuth_AppID"><?php xecho($u_item['app_id']) ?></td>
                <td><a class="TSBAuth_GetSecret" data-for="<?php xecho($u_item['app_id']) ?>">Click to request</a></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>