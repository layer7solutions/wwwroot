<div class="card no-pad">
    <div class="header">
        <span>User List</span>
        <span class="status spacer-left"><?php echo count(a_stash('userlist')) ?></span>
    </div>
    <table style="font-size:12px" class="secondary">
        <thead>
            <tr>
                <th>Username</th>
                <th>Last Access</th>
                <th>Last Login</th>
                <th>First Login</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $curr_time = time();
            foreach (a_stash('userlist') as $user): ?>
            <tr>
                <td>
                    <a href="https://www.reddit.com/user/<?php xecho($user['username']) ?>"
                        target="_blank" rel="noopener noreferrer"><?php
                        xecho($user['username'])
                    ?></a>
                </td>
                <td title="<?php echo timeConvert($user['last_access']) ?>">
                    <span style="<?php
                        if ($curr_time - $user['last_access'] < 86400)
                            echo 'font-weight:bold';
                    ?>"><?php echo human_timing($user['last_access']) ?></span>
                </td>
                <td title="<?php echo timeConvert($user['last_login']) ?>">
                    <span style="<?php
                        if ($curr_time - $user['last_login'] < 86400)
                            echo 'font-weight:bold';
                    ?>"><?php echo human_timing($user['last_login']) ?></span>
                </td>
                <td title="<?php echo timeConvert($user['first_login']) ?>">
                    <span style="<?php
                        if ($curr_time - $user['last_login'] < 86400)
                            echo 'font-weight:bold';
                    ?>"><?php echo human_timing($user['first_login']) ?></span>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>