<?php
// Example config, includes all the properties that need to be filled and their descriptions.
// Name the actual config file 'config.php'

// BASIC
// --------------------------------------------------------------------------------

// unique name for the site, used as the session name so that
// sessions across sub domains are separate
const SITE_NAME = 'beta';

// domain name of the website
const SITE_HOST = 'beta.layer7.solutions';

// site url, must include trailing slash
const SITE_URL = 'http://beta.layer7.solutions/';

// location of API base URI
const SITE_API = 'http://beta.layer7.solutions/api/ajax/';

// Name of the website, can be anything
const SITE_TITLE = 'Layer 7 Solutions';

// static domain
const SITE_STATIC = 'https://static.layer7.solutions/';

// layer7.xyz domain
const SITE_XYZ = 'https://layer7.xyz/';

// If true, indicates that this site is a staging or development site. If false, then production.
const SITE_IS_STAGING = true;

// ASSETS HTTP LOCATIONS
// --------------------------------------------------------------------------------

const SITE_ASSETS       = 'http://beta.layer7.solutions/assets/';
const SITE_JS           = 'http://beta.layer7.solutions/assets/js/';
const SITE_CSS          = 'http://beta.layer7.solutions/assets/css/';
const SITE_IMAGES       = 'http://beta.layer7.solutions/assets/images/';

// ASSETS FILE LOCATIONS
// --------------------------------------------------------------------------------
const ASSETS_ROOT       = '/var/www/beta.layer7.solutions/public/assets/';
const JS_ROOT           = '/var/www/beta.layer7.solutions/public/assets/js/';
const CSS_ROOT          = '/var/www/beta.layer7.solutions/public/assets/css/';
const IMAGES_ROOT       = '/var/www/beta.layer7.solutions/public/assets/images/';
const APP_ROOT          = '/var/www/beta.layer7.solutions/app/';
const ROOT_PATH         = '/var/www/beta.layer7.solutions/';
const BOOTSTRAP_ROOT    = '/var/www/beta.layer7.solutions/bootstrap/';

const AUTOLOADERS = [
    '/var/www/beta.layer7.solutions/vendor/autoload.php'
];

// DEFAULT VIEWS
// --------------------------------------------------------------------------------
// file location of the views root
const VIEWS_ROOT = '/var/www/beta.layer7.solutions/app/views/';

// file location of the 500 error page
const VIEWS_500 = '/var/www/beta.layer7.solutions/app/views/net/error/500.html';

// name of the default layout view
const VIEWS_LAYOUT = 'net.layout';

// SECURITY
// --------------------------------------------------------------------------------
// secret key for user authentication in the internal API by CSRF token
const USERAUTH_SECRET_KEY = '<REDACTED>';

// secret key for the websync module
const WEBSYNC_SECRET_KEY = '<REDACTED>';

// cacert.pem file location
const CACERT_FILE = '/var/www/cacert.pem';

// should the public API be enabled?
const API_ENABLED = true;

// API KEYS
// --------------------------------------------------------------------------------
const GAPI_KEY = '<REDACTED>';
const INSTAGRAM_KEY = '<REDACTED>';
const VIMEO_KEY = '<REDACTED>';
const SOUNDCLOUD_KEY = '<REDACTED>';
const TWITCH_KEY = '<REDACTED>';

const TWITTER_CONSUMER_KEY = '<REDACTED>';
const TWITTER_CONSUMER_SECRET = '<REDACTED>';
const TWITTER_ACCESS_TOKEN = '<REDACTED>';
const TWITTER_ACCESS_TOKEN_SECRET = '<REDACTED>';

const FB_ID = '<REDACTED>';
const FB_SECRET = '<REDACTED>';
const FB_ACCESS_TOKEN = '<REDACTED>';

const ETSY_KEY = '<REDACTED>';

const DIRTBAG_ID = '<REDACTED>';
const DIRTBAG_SECRET = '<REDACTED>';

// REDDIT OAUTH
// --------------------------------------------------------------------------------
// authorize and access token urls for reddit's oauth api
const OAUTH_AUTHORIZE_URL = 'https://ssl.reddit.com/api/v1/authorize';
const OAUTH_ACCESS_TOKEN_URL = 'https://ssl.reddit.com/api/v1/access_token';

// reddit oauth client id/secret/redirect_url
const OAUTH_CLIENT_ID = '<REDACTED>';
const OAUTH_CLIENT_SECRET = '<REDACTED>';
const OAUTH_USER_AGENT = '<REDACTED>';
const OAUTH_REDIRECT_URL = 'http://beta.layer7.solutions/login/interstitial';

// DATABASE
// --------------------------------------------------------------------------------
// standard database credentials
const PGSQL_SERVER = '<REDACTED>';
const PGSQL_USERNAME = '<REDACTED>';
const PGSQL_PASSWORD = '<REDACTED>';
const PGSQL_READONLY_USERNAME = '<REDACTED>';
const PGSQL_READONLY_PASSWORD = '<REDACTED>';
const DB_DEFAULT_DRIVER = 'pgsql';

// oauth database credentials
const OAUTH2DB_USERNAME = '<REDACTED>';
const OAUTH2DB_PASSWORD = '<REDACTED>';

// MEMCACHED
// --------------------------------------------------------------------------------
const MEMCACHED_HOST = '127.0.0.1';
const MEMCACHED_PORT = '11211';
const MEMCACHED_PREFIX = 'www_';

// WEBHOOKS
// --------------------------------------------------------------------------------
const LAYER7HOOK_GLOBALQUEUE = '<REDACTED>';

const DISCORD_SWEEPERBOT_DEV = '<REDACTED>';
const DISCORD_DENOFWOLVES = '<REDACTED>';
const DISCORD_GTFO = '<REDACTED>';