<?php /** @noinspection PhpUndefinedConstantInspection */

/**
 *  proxy.php: requires the constant PROXY_PREFIX
 */

set_include_path(__DIR__);

define('PROXY_URI', PROXY_PREFIX . '/' . trim($_SERVER['REQUEST_URI'], '/'));

require 'config.php';
require BOOTSTRAP_ROOT.'bootstrap.php';