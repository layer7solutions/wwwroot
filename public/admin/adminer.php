<?php
require './admin_check.inc.php';

function adminer_object() {

  class AdminerSoftware extends Adminer {

    function name() {
      return 'Layer7 DB';
    }

    function loginForm() {
        $my_host = 'database.layer7.solutions';
		?>
<input type="hidden" name="auth[driver]" value="pgsql" />
<!-- <input type="hidden" name="auth[server]" title="hostname[:port]" value="<?php echo $my_host ?>" /> -->
<table cellspacing="0">
<tr><th><?php echo lang('Server'); ?></th><td><select name="auth[server]">
  <option value="PROD: database.layer7.solutions">database.layer7.solutions</option>
  <option value="DEV: database.dev.layer7.solutions">database.dev.layer7.solutions</option>
</select></td></tr>
<tr><th><?php echo lang('Username'); ?><td><input name="auth[username]" id="username" value="<?php echo h($_GET["username"]); ?>" autocapitalize="off" style="width:100%;box-sizing:border-box">
<tr><th><?php echo lang('Password'); ?><td><input type="password" name="auth[password]" style="width:100%;box-sizing:border-box">
<tr><th><?php echo lang('Database'); ?><td><input name="auth[db]" value="<?php echo h(isset($_GET["db"]) ? $_GET["db"] : 'Zion'); ?>" autocapitalize="off" style="width:100%;box-sizing:border-box">
</table>
<?php
		  echo "<p><input type='submit' value='" . lang('Login') . "'>\n";
		  echo checkbox("auth[permanent]", 1, $_COOKIE["adminer_permanent"], lang('Permanent login')) . "\n";
	  }

  }

  return new AdminerSoftware;
}

include "./editor.inc.php";