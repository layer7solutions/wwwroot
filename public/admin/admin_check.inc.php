<?php
// admin_check.inc.php
// ~~~~~~~~~~~~~~~~~~~
// This file can be included from a standalone PHP file
//
// It'll check if the logged-in user is an admin:
//   if yes -> the request will go through
//   if no  -> the request will display 404 page

function admin_check_inc(): bool {
    require_once '../../config.php';
    $is_admin = false;

    // first grab the current session id
    if (empty($_COOKIE[SITE_NAME.'sessid'])) return false;
    $session_id = $_COOKIE[SITE_NAME.'sessid'];

    try {
        // ----- connect to database
        $conn_string =
            'host='    . PGSQL_SERVER.
            ' dbname=' . 'application'.
            ' user='   . PGSQL_USERNAME.
            ' password='.PGSQL_PASSWORD.
            ' sslmode=\'require\''
            ;
        $db = pg_connect($conn_string);

        // ----- Get username from session id
        pg_send_query($db, "SELECT username FROM user_session WHERE session_id='".pg_escape_string(strval($session_id))."'");
        $res = pg_get_result($db);
        $r_username = pg_field_is_null($res,0,0) ? null : pg_fetch_result($res,0,0);
        if (empty($r_username)) return false; // exit if empty username (meaning not logged in)

        // ----- Check if admin (user mods layer7 with full perms)
        pg_send_query($db, "SELECT COUNT(*) FROM user_modded WHERE username='"
            . pg_escape_string(strval($r_username)) . "' AND subreddit='layer7' AND mod_permissions='all'");
        $res = pg_get_result($db);
        $is_admin = ( 1 == (pg_field_is_null($res,0,0) ? null : pg_fetch_result($res,0,0)) );
    } finally {
        pg_close($db);
    }
    return $is_admin;
}

if (!admin_check_inc()) {
    // If not admin, then set the include path to the ROOT directory (up one directory)
    set_include_path(dirname(__DIR__));
    // Include the index.php which will initialize the framework and main website
    include('index.php');
    // Whatever file the non-authorized user tried to request doesn't exist within the
    // main website's route config, which will cause the request to 404 and display
    // the 404 page (which is what we want)

    die; // Die to prevent going forward into the administrative php file
}