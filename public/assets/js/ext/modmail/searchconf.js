init('ModmailSearch', function(opts) {
    app.modmail.searchhandle.init(n.asi('#ModmailSearch__Search').init({
        placeholder: 'search modmails by title, body, or username',
        active_placeholder: 'search modmails by title, body, or username',
        disable_error_completion: true,
        events: {
            complete: function(event) {
                app.modmail.searchhandle.on_complete(this);
            },
            enter: function(event) {
            },
            validate: function(event) {

            },
            textchange: function(event) {
                app.modmail.searchhandle.on_typing(this);
            },
            tokenchange: function(event) {
            }
        },
        multiple_values: false,
        tokens: {
            'in': {
                type: 'text',
                placeholder: 'subreddit',
                suggestions: opts.subreddits,
                validate: {
                    limit_to_suggestions: 'must be a subreddit you mod',
                },
                multiple_values: true,
            },
            'amount': {
                type: 'number:int',
                placeholder: 'max number of results (default: 50)',
                suggestions: [
                    25,
                    50,
                    75,
                    100,
                    125,
                    150,
                    175,
                    200,
                    225,
                    250,
                    275,
                    300,
                ],
                allow_duplicates: false,
                desc_html: 'Maximum amount of search results between 1 and 300 (default: 50).',
            },
            'sort': {
                type: 'text',
                placeholder: 'sort type (default: new)',
                suggestions: ['new', 'relevance', 'old'],
                validate: {
                    limit_to_suggestions: 'invalid sort type',
                },
                allow_duplicates: false,
            },
            /*
            'after': {
                type: 'datetime',
                placeholder: 'date/time',
                use_type_placeholder: true,
                allow_duplicates: false,
            },
            'before': {
                type: 'datetime',
                placeholder: 'date/time',
                use_type_placeholder: true,
                allow_duplicates: false,
            },
            'during': {
                type: 'date',
                placeholder: 'date',
                use_type_placeholder: true,
                allow_duplicates: false,
            },
            'author': {
                type: 'text',
                placeholder: 'modmail author',
                search_label: 'username',
                modifiers: {
                    'includes': {},
                    'starts-with': {},
                    'ends-with': {},
                    'exact': {
                        desc_html: 'Perform a case-sensitive author search, whereas without any modifiers is case-insensitive.',
                    },
                    'regex': {
                        search_label: 'regex',
                        desc_html: 'Search for usernames matching a specific regex.',
                        validate: {
                            check: validate_regex,
                            error: 'invalid regex',
                        }
                    },
                    'shorter-than': {
                        type: 'number:whole',
                        search_label: '# of characters',
                        placeholder: 'number',
                        desc_html: 'Search for usernames <b>shorter than</b> this number of characters.',
                        disable_space_completion: false,
                    },
                    'longer-than': {
                        type: 'number:whole',
                        search_label: '# of characters',
                        placeholder: 'number',
                        desc_html: 'Search for usernames <b>longer than</b> this number of characters.',
                        disable_space_completion: false,
                    }
                },
                desc_html: n.gen(
                'span.asi-helptitle["username - allowed formats"] + \
                  div.asi-desc > \
                    ul > \
                      (li > \
                        span["Reddit link to the user\'s profile page"] \
                      ) + \
                      (li > \
                        code["/u/username"] + span[" or "] + code["u/username"] \
                      ) + \
                      (li > \
                        code["username"] \
                      )'),
                validate: {
                    match: app.util.sanitize.reddit.regex.username,
                    error: 'invalid username',
                },
                valuepass: app.util.sanitize.reddit.pass.username,
                multiple_values: false,
            },
            'id': {
                type: 'text',
                placeholder: 'modmail id',
                search_label: 'modmail id',
                multiple_values: true,
            },
            */
        }
    }));
});