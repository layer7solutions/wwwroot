app.modmail = {
    mdgen: Snudown.getParser(),
    getDirectLink: function(item) {
        var root_id = item.message_root_thing_id;
        var thing_id = item.thing_id;

        var direct_link;

        if (root_id && startsWith(root_id, 'ModmailConversation')) {
            direct_link = 'https://mod.reddit.com/mail/all/' +
                root_id.slice(root_id.indexOf('_')+1);
        } else if (thing_id && startsWith(thing_id, 'ModmailMessage')) {
            direct_link = 'https://mod.reddit.com/mail/all/' +
                thing_id.slice(thing_id.indexOf('_')+1);
        } else if (thing_id) {
            direct_link = app.url.reddit_href('/message/messages/' +
                thing_id.slice(thing_id.indexOf('_')+1));
        }

        return direct_link;
    },
    addHighlighting: function(html, needle) {
        return html.replace(new RegExp(needle, 'gi'), function(str) {
            return app.modmail.components.createHighlightSpan(str, true);
        });
    },
    components: {
        createHighlightSpan: function(str, bolded) {
            var tagName = bolded ? 'strong' : 'span';
            return "<"+tagName+" class='highlight'>" + str + "</"+tagName+">";
        },
        createBody: function(body_text, options) {
            options = options || {};

            var inner_html = app.modmail.mdgen.render(body_text); // also escapes HTML

            if (options.highlight && is_string(options.highlight)) {
                inner_html = app.modmail.addHighlighting(inner_html, options.highlight);
            }

            return n('.md', {
                html: inner_html
            });
        },
        createAddressValueText: function(name, options) {
            name = escape_html(name); // escape HTML

            if (options.highlight && name.toLowerCase() == options.highlight.toLowerCase()) {
                return app.modmail.components.createHighlightSpan(name, true);
            }
            return name;
        },
        createSubjectLine: function(subject_text, options) {
            subject_text = escape_html(subject_text); // escape HTML

            if (options.highlight && is_string(options.highlight)) {
                subject_text = app.modmail.addHighlighting(subject_text, options.highlight);
            }
            return subject_text;
        },
        createMessage: function(item, options) {
            options = options || {};
            return n('.Modmail', {
                "data-id": item.id,
                "data-thing-id": item.thing_id,
                "data-subreddit": item.subreddit,
                "data-subreddit-id": item.subreddit_id,
                "data-message-root-thing-id": item.message_root_thing_id,
                "data-parent-thing-id": item.parent_thing_id,
            }, [
                n('.ModmailAddressBar', [
                    n('.ModmailAddressItem.ModmailAddressItem--message_subreddit', [
                        n('span', {text: 'subreddit'}),
                        n('strong', {
                            html: app.modmail.components.createAddressValueText(item.subreddit, options)
                        }),
                    ]),
                    n('.ModmailAddressItem.ModmailAddressItem--message_to', [
                        n('span', {text: 'to'}),
                        n('strong', {
                            html: app.modmail.components.createAddressValueText(item.message_to, options)
                        }),
                    ]),
                    n('.ModmailAddressItem.ModmailAddressItem--message_from', [
                        n('span', {text: 'from'}),
                        n('strong', {
                            html: app.modmail.components.createAddressValueText(item.message_from, options)
                        }),
                    ]),
                    n('.ModmailAddressItem.ModmailAddressItem--created_utc', [
                        n('span', {text: 'on'}),
                        n('strong', {text: timeConvert(item.created_utc)}),
                    ]),
                    n('.ModmailAddressItem.ModmailAddressItem--direct_link', [
                        n('a', {
                            text: '(direct link)',
                            href: app.modmail.getDirectLink(item),
                        }),
                    ]),
                ]),
                n('.ModmailSubjectBar', [
                    n('.ModmailAddressItem.ModmailAddressItem--subject', [
                        n('span', {text: 'subject:'}),
                        n('strong', {
                            html: app.modmail.components.createSubjectLine(item.subject, options)
                        }),
                    ]),
                ]),
                n('.ModmailBody', {
                    html: app.modmail.components.createBody(item.body, options),
                })
            ]);
        }
    }
};

init('ModmailCommon', function(opts) {
    app.modmail.mdgen = Snudown.getParser();
});