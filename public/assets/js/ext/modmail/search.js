app.modmail.searchhandle = {
    asi_instance: null,
    init: function(asi_instance) {
        this.asi_instance = asi_instance;
    },
    on_typing: function(asi_instance) {

    },
    on_complete: function(asi) {
        var query = this.get_query(asi);

        if (!query) {
            return;
        }

        var srlist = query.subreddits.length ? query.subreddits.join(',') : 'all:modmail';

        var params = {
            query: query.text,
        };

        if (query.amount) {
            params.amount = query.amount;
        }
        if (query.sort) {
            params.order = query.sort;
        }

        r('/r/{}/modmail/search'.format(srlist), {
            data: params,
            start: function() {
                this.result_el = document.getElementById('ModmailSearch__Result');
                n.html(this.result_el, n.gen('.halign > .spacer > .loading'));
                n.removeClass(this.result_el, 'hide');
            },
            success: function(data) {
                n.removeClass(this.result_el, 'hide');

                var children = reduce(data.listing, function(acc, item) {
                    acc.push(app.modmail.components.createMessage(item, {
                        highlight: query.text,
                    }));
                }, [
                    n('p.spacer5', {
                        text: '' + data.listing.length + ' results'
                    })
                ]);

                n.html(this.result_el, children);
            },
        })
    },
    get_query: function(asi) {
        var query = {text: asi.get_state().text, subreddits: [], amount: null, has_error: false};

        forEach(asi.get_state().tokens, function(token) {
            if (token.error !== false) {
                query.has_error = true;
                return false; // break
            }

            switch (token.label) {
                case 'in':
                    query.subreddits = query.subreddits.concat(token.values);
                    break;
                case 'amount':
                    query.amount = token.values[0];
                    break;
                case 'sort':
                    query.sort = token.values[0];
                    break;
            }
        });

        if (!query || !query.text || !query.text.length || query.has_error) {
            return false;
        }

        return query;
    }
};