init('OAuthData', function() {
    var
        request_sensitive = function(app_id, item) {
            function confirm_message(item_name) {
                return confirm(
                    "Are you sure you want to fetch the " + item_name + " for this agent? " +
                    "This will send an HTTP request. Do not perform over an unsafe network.");
            }

            if (!confirm_message(item.replace('_', ' ')))
                return;

            r('/admin/oauth_data?for={}&{}'.format(app_id, item), {
                success: function(result) {
                    app.dialog('<code>'+result['item']+'</code>', app.DIALOG_ALERT);
                },
            });
        },
        CreateForm = {
            submitButton: document.querySelector('.TSBAuth_InsertSubmit'),
            fields: {
                'username':     '#TSBAuth_InsertUsername',
                'password':     '#TSBAuth_InsertPassword',
                'agent_of':     '#TSBAuth_InsertAgentOf',
                'app_id':       '#TSBAuth_InsertAppID',
                'app_secret':   '#TSBAuth_InsertAppSecret',
            },
            getData: function() {
                var data = {};

                forEach(this.fields, function(k, v) {
                    var value = document.querySelector(v).value;
                    if (value.length) {
                        data[k] = value;
                    } else {
                        alert('"' + (k.replace('_', ' ')) + '" must not be empty.');
                        data = null;
                        return false;
                    }
                });

                return data;
            },
            clearFields: function() {
                forEach(this.fields, function(k, v) {
                    document.querySelector(v).value = '';
                });
            },
            submit: function() {
                var data = this.getData();
                if (!isset(data)) {
                    CreateForm.submitButton.removeAttribute('disabled');
                    return;
                }

                r('/admin/oauth_data', 'PUT', {
                    data: data,
                    receive: function() {
                        CreateForm.clearFields();
                    },
                    success: function(result) {
                        alert('Succesfully added: ' + data['username']);
                        CreateForm.addToTable(data);
                    },
                    error: function(error, err_msg) {
                        alert('Failed to add: ' + data['username']);
                    },
                    done: function() {
                        CreateForm.submitButton.removeAttribute('disabled');
                    }
                });
            },
            addToTable: function(data) {
                var row = document.querySelector('.TSBAuth_Row[data-for=template]').cloneNode(true);
                row.setAttribute('data-for', data['app_id']);
                n.removeClass(row, 'hide');

                row.querySelector('.TSBAuth_AgentOf').innerText = data['agent_of'];
                row.querySelector('.TSBAuth_Username').innerText = data['username'];
                row.querySelector('.TSBAuth_AppID').innerText = data['app_id'];

                n.select(row, '[data-for]').attr('data-for', data['app_id']);
                n.select('.TSBAuth_Table tbody').append(row);
            }
        };

    n.on(CreateForm.submitButton, 'click', function() {
        this.setAttribute('disabled', 'disabled');
        CreateForm.submit();
    });

    n.on(document.body, 'click', function(event) {
        console.log(event.target);
        if (n.hasClass(event.target, 'TSBAuth_GetPassword')) {
            console.log('hello!');
            request_sensitive(event.target.getAttribute('data-for'), 'password');
        } else if (n.hasClass(event.target, 'TSBAuth_GetSecret')) {
            console.log('world!');
            request_sensitive(event.target.getAttribute('data-for'), 'app_secret');
        }
    });
});