init('LoginInterstitial', function(opts) {
    n.on('.nav-logo-a', 'click', function(event) {
        event.preventDefault();
        event.stopPropagation();
    });
    if (opts.test) {
        return;
    }
    if (!document.forms || !document.forms['LoginCallback']) {
        return;
    }
    document.forms['LoginCallback'].submit();
});