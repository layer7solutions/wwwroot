app.dirtbag = {
    init: function(opts) {
        var instance = app.dirtbag,
            parent   = n.select(instance.form.element.parent = document.getElementById('DirtbagSettings'));

        parent.html(n('.loading'));

        instance.subreddit = opts.subreddit;

        instance.settings.get(function(settings) {
            if (!isset(settings)) {
                parent.html(n('p', 'Not a DirtBag subreddit.'));
                return;
            }
            if (settings === false) {
                parent.html(n('p', 'Not authorized.'));
                return;
            }
            parent.html([
                n('div.valign', [
                    instance.form.element.save_button,
                    instance.form.status.element.pending_icon,
                    instance.form.status.element.label,
                    instance.form.status.element.save_marker,
                ]),
                instance.handle.load(settings)
            ]);
        });
    },
    form: {
        save_button_handler: function() {
            if (app.dirtbag.settings.set_inProgress) {
                return;
            } else if (!app.dirtbag.settings.has_changed) {
                app.dirtbag.form.status.set('No changes to save', 'no-changes');
                return;
            } else {
                app.dirtbag.form.status.set('pending');
            }

            app.dirtbag.settings.set(app.dirtbag.config,
                // success callback
                function() {
                    app.dirtbag.form.status.set('saved');
                    app.dirtbag.handle.onsave();
                },
                // error calback
                function() {
                    app.dirtbag.form.status.set('An internal error occured.', 'internal-error');
                }
            );
        },
        element: {
            parent: null,
            save_button: n('button.primary.primary--2', {
                'html': 'Save Settings',
                'onclick': function(event) {
                     app.dirtbag.form.save_button_handler(event);
                },
            }),
        },
        status: {
            element: {
                label:          n('span.DirtbagSettings__saveStatus.spacer-left.hide'),
                save_marker:    n('i.icon.zmdi.zmdi-check-circle.save-marker'),
                pending_icon:   n('span.loading.xsmall.spacer-left.hide'),
            },
            set: function(new_status, status_type) {
                n.addClass(this.element.pending_icon, 'hide');

                switch (new_status) {
                    case 'pending':
                        n.removeClass(this.element.pending_icon, 'hide');
                        break;
                    case 'saved':
                        var save_marker = this.element.save_marker;
                        n.addClass(save_marker, 'save-marker--flash');
                        setTimeout(function() {
                            n.removeClass(save_marker, 'save-marker--flash');
                        }, 2000);
                        break;
                    case null:
                        break;
                    default:
                        n.select(this.element.label)
                            .removeClass('hide')
                            .attr('data-type', status_type)
                            .html(new_status);
                        return;
                }

                n.select(this.element.label)
                    .addClass('hide')
                    .attr('data-type', '')
                    .html('');
            },
            get: function() {
                return this.element.label.innerHTML;
            },
            hideIfType: function(type) {
                if (this.element.label.getAttribute('data-type') == type) {
                    this.set(null);
                }
            },
        },
    },
    settings: {
        has_changed: false,
        set_inProgress: false,
        get: function(callback) {
            r('GET', '/r/{}/dirtbag/settings'.format(app.dirtbag.subreddit), {
                success: function(result) {
                    callback(result);
                },
                error: function(error, err_msg) {
                    callback(false);
                },
                status_error: function() {
                    callback(false);
                },
            });
        },
        set: function(new_settings, callback, error_callback) {
            if (app.dirtbag.settings.set_inProgress) {
                return false;
            } else {
                app.dirtbag.settings.set_inProgress = true;
            }

            new_settings = JSON.stringify(new_settings);

            r('PUT', '/r/{}/dirtbag/settings'.format(app.dirtbag.subreddit), {
                data: {
                    'settings': encodeURIComponent(new_settings),
                },
                success: function() {
                    app.dirtbag.settings.has_changed = false;
                    callback.apply(this, arguments);
                },
                error: function() {
                    error_callback.apply(this, arguments);
                },
                done: function() {
                    app.dirtbag.settings.set_inProgress = false;
                },
            });

            return true;
        },
        property: function(path) {
            var path_parts = trim(path,'.').split('.');

            // remove 'config' prefix
            if (path_parts[0] == 'config')
                path_parts.shift();

            var worker      = app.dirtbag.config,
                parent      = null,
                node_name   = null,
                node_path   = path_parts.join('.');

            for (var i = 0; i < path_parts.length; i++) {
                if (!worker.hasOwnProperty(path_parts[i])) {
                    worker = undefined;
                    break;
                } else if (i == path_parts.length - 1) {
                    parent = worker;
                    node_name = path_parts[i];
                    break;
                } else {
                    if (worker[path_parts[i]] == null) {
                        break;
                    }

                    worker = worker[path_parts[i]];
                }
            }

            return {
                set: function(new_value) {
                    if (node_name == null) {
                        return null;
                    }

                    var old_value = parent[node_name];
                    parent[node_name] = new_value;

                    app.dirtbag.settings.has_changed = true;
                    app.dirtbag.handle.onchange(node_path, new_value, old_value);

                    return old_value;
                },
                get: function() {
                    if (node_name == null) {
                        return null;
                    }

                    return parent[node_name];
                },
            };
        },
    }
};

init('DirtbagSettings', app.dirtbag.init);