app.dirtbag.handle = {
    root_container: n('div', {
        'data-name': 'config',
        'data-path': 'config',
        'data-group': 'root',
    }),
    load: function(config) {
        app.dirtbag.config = config;

        var
            mod_group = function(container, name, meta) {
                if (meta.hasOwnProperty('label')) {
                    n.appendChild(
                        n('h3', [
                            n('span', meta['label']),
                        ]),
                        container
                    );
                }
            },
            mod_property = function(name, descriptor, path, initial_value, value_updator) {
                var descriptor_parts = descriptor.split(/\/(.+)/);
                var label = descriptor_parts.length > 1 ? descriptor_parts[1] : name;

                switch (descriptor = descriptor_parts[0]) {
                    case 'info':
                    case 'info:group-mod-user':
                        return n('p', [
                            n('h5', label),
                            n('span', initial_value),
                        ]);
                    case 'info:group-mod-date':
                    case 'info:date':
                        var date;
                        if (is_int(initial_value)) {
                            date = new Date(initial_value * 1000); // assume unix timestamp
                        } else {
                            // assume date string

                            // set to UTC if it doesn't already have a timezone indicator
                            if (isNaN(Date.parse(initial_value + 'Z'))) {
                                date = new Date(initial_value);
                            } else {
                                date = new Date(initial_value + 'Z');
                            }
                        }

                        var human_time = human_timing(date);
                        var date_string = timeConvert(date);

                        if (initial_value == null || (date / 1000 | 0) < 0) {
                            human_time = human_timing(0);
                            date_string = null;
                        }

                        return n('p', [
                            n('h5', label),
                            n('span.human-timing', {
                                'text': human_time,
                                'title': date_string,
                                'data-timestamp': date / 1000 | 0,
                            }),
                        ]);
                    case 'label':
                        return n('h4', initial_value);
                    case 'input:number':
                        return n('label', [
                            n('span', label),
                            n('input[type=number][step=any]', {
                                'value': initial_value,
                                'onchange': function(event) {
                                    value_updator(this.value);
                                },
                            }),
                        ]);
                    case 'input:text':
                        return n('label', [
                            n('span', label),
                            n('input[type=text]', {
                                'value': initial_value,
                                'onchange': function(event) {
                                    value_updator(this.value);
                                },
                            }),
                        ]);
                    case 'toggle:group':
                    case 'toggle':
                        return n('label', [
                            n('span', label),
                            n('.ui-switch.ui-switch--small', [
                                n('div', [
                                    n('input[type=checkbox]' + (initial_value ? '[checked]' : ''), {
                                        'onchange': function(event) {
                                            value_updator(this.checked);
                                        },
                                    }),
                                    n('span.knob'),
                                ]),
                            ]),
                        ]);
                    case 'input:textlist':
                    case 'input:textmaplist':
                        var
                            is_map = (descriptor == 'input:textmaplist'),
                            list_container = n('.DirtbagSettings__listfield'),
                            revalidate_list = function() {
                                var subquery = '.DirtbagSettings__listfieldItem' +
                                    ':not(.DirtbagSettings__listfieldCreateItem)';
                                var new_value =
                                    reduce(
                                        n.select(list_container, subquery)
                                        .map(function(el) {
                                            return n.select(el, 'input[type=text]').map(function(e) {
                                                return e.value;
                                            })
                                        }),
                                        function(acc, fields) {
                                            if (any(fields, empty)) {
                                                return acc;
                                            }
                                            if (is_map) {
                                                acc.push({
                                                    key: fields[0],
                                                    value: fields[1],
                                                });
                                            } else {
                                                acc.push(fields[0]);
                                            }
                                            return acc;
                                        },
                                        []
                                    );

                                value_updator(new_value);
                            },
                            add_item = function(item) {
                                var data = {
                                    key_field: !is_map ? null : n('input[type=text]', {
                                        value:    (item || '') && item.key,
                                        onchange: revalidate_list,
                                        placeholder: 'Item key',
                                    }),
                                    value_field: n('input[type=text]', {
                                        value:    (is_map ? (item || '') && item.value : item),
                                        onchange: revalidate_list,
                                        placeholder: 'Item value',
                                    }),
                                    item_el: n('.DirtbagSettings__listfieldItem'),
                                    close_button: n('i.icon.icon-close.zmdi.zmdi-close', {
                                        'onclick': function() {
                                            data.item_el.remove();
                                            revalidate_list();
                                        },
                                    }),
                                };

                                n.appendChild(data.key_field, data.item_el);
                                n.appendChild(data.value_field, data.item_el);
                                n.appendChild(data.close_button, data.item_el);
                                n.appendChild(data.item_el, list_container);

                                return data;
                            };

                        forEach(initial_value, add_item);

                        return n('div.DirtbagSettings__listfieldParent', [
                            n('h3', label),
                            list_container,
                            n('button.tertiary', {
                                'onclick': function() {
                                    add_item();
                                },
                                'text': 'Add item',
                            })
                        ]);
                }
            },
            process = function(name, object, path, container) {

                // if string - then field
                if (is_string(object))
                {
                    var path        = path + '.' + name,
                        value       = app.dirtbag.settings.property(path).get(),
                        descriptor  = object;

                    n.select(container).mod({
                        'data-name': name,
                        'data-path': path,
                        'data-descriptor': descriptor.split(/\/(.+)/)[0],
                        'data-value': JSON.stringify(value),
                    });

                    if (descriptor == 'ignore') {
                        n.addClass(container, 'hide');
                        return;
                    }

                    var value_updator = function(new_value) {
                        container.setAttribute('data-value', JSON.stringify(new_value));
                        n.addClass(container, 'DirtbagSettings__property--changed');
                        app.dirtbag.settings.property(path).set(new_value);
                    };

                    n.appendChild(
                        mod_property(name, descriptor, path, value, value_updator),
                        container);

                    if (descriptor.split(/\/(.+)/)[0] == 'toggle:group') {
                        n.insertAfter(n('.cf'), container);

                        n.closest(container, '.DirtbagSettings__group').setAttribute('data-group-toggle', path);
                    }
                }

                // if array - then field group
                else
                {
                    forEach(object, function(k,v) {
                        if (k.charAt(0) == '_') return;

                        var meta = {};

                        if (!is_string(v)) {
                            forEach(v, function(sk,sv) {
                                if (sk.charAt(0) != '_') return;
                                meta[sk.slice(1)] = sv;
                            });
                        }

                        var
                            is_group      = contains(meta, 'group'),
                            subpath       = path + (is_group ? ('.'+k) : ''),
                            subcontainer  = n(is_group ? 'section' : 'div', {
                                'class': 'DirtbagSettings__'+(is_group ? 'group' : 'property'),
                                'data-name': k,
                                'data-path': subpath,
                            });

                        forEach(meta, function(k, v) {
                            subcontainer.setAttribute('data-'+k, v);
                        });

                        if (is_group) {
                            mod_group(subcontainer, name, meta);
                        }

                        n.appendChild(subcontainer, container);
                        process(k, v, subpath, subcontainer);
                    });
                }

                return container;
            };

        app.util.human_timing_update_start();

        return process('config', app.dirtbag.template, 'config', app.dirtbag.handle.root_container);
    },
    onchange: function(node_path, new_value, old_value) {
        app.dirtbag.form.status.hideIfType('no-changes');
    },
    onsave: function() {
        var current_timestamp = Date.now() / 1000 | 0,
            current_ISOstring = new Date().toISOString();

        var date_prop_list =
            app.dirtbag.handle.root_container.querySelectorAll('[data-descriptor="info:group-mod-date"]');

        // Update 'Last Modified' and 'Modified By' properties as applicable
        forEach(date_prop_list, function(date_prop) {
            var parent_group = n.closest(date_prop, '[data-group]');

            // check if at least one property in this group was changed
            if (n.select(parent_group, '.DirtbagSettings__property--changed').length) {
                date_prop.setAttribute('data-value', current_ISOstring);
                date_prop.querySelector('[data-timestamp]').setAttribute('data-timestamp', current_timestamp);

                (function(user_prop) {
                    user_prop.setAttribute('data-value', CURRENT_USER);
                    user_prop.querySelector('span').innerHTML = CURRENT_USER;
                })(parent_group.querySelector('[data-descriptor="info:group-mod-user"]'));
            }
        });

        // remove changed status from properties
        n.select(app.dirtbag.handle.root_container, '.DirtbagSettings__property--changed')
            .removeClass('DirtbagSettings__property--changed');

        // restart such that the first update will happen immediately
        app.util.human_timing_update_restart();
    },
};