(function() {
    var template_removalFlair = {
        '_group': 'row',
        '_label': 'Removal Flair',
        'enabled': 'toggle:group/Enabled:',
        'text': 'input:text/Flair Text',
        'class': 'input:text/Flair CSS Class',
        'priority': 'input:number/Priority',
    };

    var template_property = {
        '_group': 'property',
        'name': 'label',
        'enabled': 'toggle:group/Enabled:',
        'value': 'input:text/Value:',
        'weight': 'input:text/Weight:',
    };

    app.dirtbag.template = {
        'subreddit': 'ignore',
        'lastModified': 'info:group-mod-date/Last Modified:',
        'modifiedBy': 'info:group-mod-user/Modified By:',
        'reportScoreThreshold': 'input:number/Report Score Threshold',
        'removeScoreThreshold': 'input:number/Remove Score Threshold',
        'licensingSmasher': {
            '_group': 'module',
            '_label': 'Licensing Smasher',
            'enabled': 'toggle:group/Enabled:',
            'lastModified': 'info:group-mod-date/Last Modified:',
            'modifiedBy': 'info:group-mod-user/Modified By:',
            'removalFlair': template_removalFlair,
            'matchTerms': 'input:textlist/Match Terms:',
            'knownLicensers': 'input:textmaplist/Known Licensers:',
            'scoreMultiplier': 'input:number/Score Multiplier:',
        },
        'youTubeSpamDetector': {
            '_group': 'module',
            '_label': 'Youtube Spam Detector',
            'enabled': 'toggle:group/Enabled:',
            'lastModified': 'info:group-mod-date/Last Modified:',
            'modifiedBy': 'info:group-mod-user/Modified By:',
            'scoreMultiplier': 'input:number/Score Multiplier',
            'removalFlair': template_removalFlair,
            'channelAgeThreshold': template_property,
            'viewCountThreshold': template_property,
            'voteCountThreshold': template_property,
            'negativeVoteRatio': template_property,
            'redditAccountAgeThreshold': template_property,
            'licensedChannel': template_property,
            'channelSubscribersThreshold': template_property,
            'commentCountThreshold': template_property,
        },
        'selfPromotionCombustor': {
            '_group': 'module',
            '_label': 'Self Promotion Combustor',
            'enabled': 'toggle:group/Enabled:',
            'lastModified': 'info:group-mod-date/Last Modified:',
            'modifiedBy': 'info:group-mod-user/Modified By:',
            'removalFlair': template_removalFlair,
            'scoreMultiplier': 'input:number/Score Multiplier:',
            'percentageThreshold': 'input:number/Percentage Threshold:',
            'includePostInPercentage': 'toggle/Include Post In Percentage',
            'gracePeriod': 'input:number/Grace Period',
        }
    }
})();