app.blacklist = {};

app.blacklist.init = function() {
    app.forms.Filter('TSBSubChooser', {
        inputs: '.SubredditChooser--TSBForm .SubredditOption input',
        toggle_all: '.SubredditChooser--TSBForm .SubredditOption--all input',
        exclude_values: ['thesentinelbot']
    });

    // Attach Media target additional field add button handler
    n.on('.MediaTargetAddButton', 'click', function() {
        app.blacklist.targetitem.add();
    });

    n.on('.MediaTargetClearButton', 'click', function() {
        app.blacklist.targetitem.clear_all();
    });

    // Attach submit button handlers
    n.on('.TSBForm__SubmitButton', 'click', function() {
        app.blacklist.form.submit(this.getAttribute('data-action-type'));
    });

    // Add 3 initial items and focus first
    app.blacklist.targetitem.add_multiple(3)
        .shift().querySelector('input[type=url]').focus();
};

app.blacklist.targetitem = {
    next_id: 0,
    clear_all: function() {
        n.value('.MediaTargetItem__input', '');
        n.select('.MediaTargetItem').removeClass(
            'MediaTargetItem--showFauxInput MediaTargetItem--result MediaTargetItem--error')
            .removeAttr('data-id')
            .removeAttr('data-mediacheckval');

        n.select('.MediaTargetItem__fauxInput').addClass('hide');

        n.select('.MediaTargetItem__input').removeClass('hide');
        n.select('.MediaTargetItem .MediaTargetItem__results').removeAllChildren();
    },
    add_multiple: function(amount) {
        var list = [];
        for (var i = 0; i < amount; i++) {
            list.push( app.blacklist.targetitem.add() );
        }
        return list;
    },
    get_results: function() {
        if (arguments.length == 1) {
            return n.select('.MediaTargetItem[data-id="'+arguments[0]+'"] .MediaTargetItem__results');
        } else if (arguments.length == 2) {
            var id = arguments[0] + '-' + arguments[1];
            return n.select('.MediaTargetItem[data-id="'+id+'"] .MediaTargetItem__results');
        }
    },
    create_result_block: function(title, items) {
        return n('.MediaTargetItem__resultBlock', [
            n('h5', title),
            n('ul', {
                'html': items,
            }),
        ]);
    },
    create_result_errblock: function(html) {
        return n('.MediaTargetItem__resultBlock.MediaTargetItem__resultBlock--error', {
            'html': html,
        });
    },
    add: function(should_focus) {
       should_focus = should_focus || false;

        var itemClass = 'MediaTargetItem--' + (app.blacklist.targetitem.next_id++);
        var item = n.select(n.gen(`.MediaTargetItem.${itemClass} >
                input.MediaTargetItem__input[type='url'][placeholder="https://"] +
                .MediaTargetItem__fauxInput.hide +
                .MediaTargetItem__results.results`))
                .appendTo('.MediaTargetList__contents');

        var input_el = item.select('.MediaTargetItem__input');

        item.on('click', function(event) {
            var item = n.select(this),
                input_el = item.select('.MediaTargetItem__input');

            // if input is disabled, do nothing
            if (input_el.hasAttr('disabled')) {
                return;
            }

            // if what was clicked was the media-link or ID or results, then do nothing
            if (n.closest(event.target, '.media-link') || n.closest(event.target, '.code')
                    || n.closest(event.target, '.MediaTargetItem__results')) {
                return;
            }

            // item was clicked, so we enter "edit" mode:
            //   hide faux input, restore real input
            if (item.hasClass('MediaTargetItem--showFauxInput')) {
                item.select('.MediaTargetItem__fauxInput').addClass('hide');
                input_el.removeClass('hide').focus();
            } else {
                input_el.focus();
            }

            input_el.setSelection(0, input_el.value().length)
        });

        input_el.on('keydown', function(event) {
            var input_el = n.select(this),
                item     = input_el.closest('.MediaTargetItem'),
                c        = event.keyCode || event.which;

            // escape key
            if (c == 27) {
                input_el.blur();
                event.preventDefault();
                return;
            }

            // user is making a modification, so remove the error class (if one exists)
            item.removeClass('MediaTargetItem--error');

            // if tab is pressed
            if (c === 9) {
                event.preventDefault();

                if (event.shiftKey) {
                    item[0].previousElementSibling.click();
                } else {
                    // if last media target item
                    if (item[0].nextElementSibling == null) {
                        // create new media target item and go to it
                        app.blacklist.targetitem.add(true);
                    } else {
                        item[0].nextElementSibling.click();
                    }
                }
            }
        }).on('blur', function(event) {
            var input_el = n.select(this),
                item     = input_el.closest('.MediaTargetItem'),
                results  = item.select('.MediaTargetItem__results');

            var was_changed = item.attr('data-mediacheckval') != input_el.value();

            // if the input was changed, then certain things no longer apply
            if (was_changed) {
                item
                    .removeClass('MediaTargetItem--result')
                    .attr('data-id', '')
                    .attr('data-mediacheckval', '');
            }

            // if the input was not changed, and has faux input class, then:
            //   hide real input, restore faux input
            if (!was_changed && item.hasClass('MediaTargetItem--showFauxInput')) {
                // console.log('MediaTargetItem: not changed, has faux input');
                input_el.addClass('hide');
                item.select('.MediaTargetItem__fauxInput').removeClass('hide');
            }

            // if the input was changed
            if (was_changed) {
                // console.log('MediaTargetItem: changed, checking URL');

                var faux_input = item.select('.MediaTargetItem__fauxInput');

                // restore real input, hide faux input
                item.removeClass('MediaTargetItem--showFauxInput');
                input_el.removeClass('hide').addClass('MediaTargetItem__input--checkingMedia');
                faux_input.addClass('hide').removeAllChildren();

                // update mediacheckval
                item.attr('data-mediacheckval', input_el.value());

                if (app.url.check_url(this.value)) {
                    // console.log('MediaTargetItem: valid URL, fetching media info');

                    app.util.media_info(this.value, function(result) {
                        input_el.removeClass('MediaTargetItem__input--checkingMedia');

                        // if not a valid URL, then do nothing as we already
                        // restored the real input up above
                        if (result === false || !result['platform'].length || result['platform'] === 'unknown') {
                            item.addClass('MediaTargetItem--result');
                            results.html(app.blacklist.targetitem.create_result_errblock(
                                "Unknown media platform. See the Supported Platforms list in the " +
                                "sidebar."
                            ));
                            return;
                        }

                        // If valid platform, but empty ID or author
                        if (!result['id'].length || !result['author'].length) {
                            item.addClass('MediaTargetItem--result');
                            results.html(app.blacklist.targetitem.create_result_errblock(
                                "The channel either does not exist or has been terminated by the " +
                                "platform's administrators."
                            ));
                            return;
                        }

                        // hide real input, restore faux input with new data
                        item.addClass('MediaTargetItem--showFauxInput');
                        input_el.addClass('hide');

                        faux_input.mod([
                            n('.valign.grow', [
                                n('a.media-link', {
                                    'href': result['url'],
                                    'target': '_blank',
                                    'rel': 'noopener noreferrer',
                                    'style': 'margin-right:8px',
                                    'html': app.util.create_media_icon(result['platform']),
                                }),
                                n('span', {text: result['author']}),
                            ]),
                            n('.valign', [
                                n('span.code[title="Media Channel ID"]', {text: result['id'] }),
                            ]),
                        ]);
                        faux_input.removeClass('hide');

                        // update attributes
                        item.attr('data-id', result['platform']+'-'+result['id']);
                    });
                } else {
                    // don't show invalid URL error message if value is empty
                    if (this.value.length) {
                        // console.log('MediaTargetItem: invalid URL, skipping');
                        item.addClass('MediaTargetItem--result');
                        results.html(app.blacklist.targetitem.create_result_errblock(
                            "Invalid URL"
                        ));
                    } else {
                        // console.log('MediaTargetItem: empty URL, skipping');
                    }
                }
            }
        });

        if (should_focus) {
            input_el.focus();
        }

        return item[0];
    },
};

app.blacklist.form = {
    in_progress: false,
    set_buttons_enabled: function(state) {
        n.select('.TSBForm__SubmitButton').attr('disabled', !state);
        if (state == true) {
            n.addClass('.TSBForm__SubmitButton .icon--in-progress', 'hide');
            n.removeClass('.TSBForm__SubmitButton .icon--normal', 'hide');
        }
    },
    set_InProgress: function(state, error_class) {
        if (state === false) {
            app.blacklist.form.set_buttons_enabled(true);
            app.blacklist.form.in_progress = false;

            n.select('.MediaTargetItem__input').removeAttr('disabled');

            n.addClass('.TSBError', 'hide');

            if (isset(error_class)) {
                n.removeClass(error_class, 'hide');
            }
        } else {
            n.removeClass('.MediaTargetItem', 'MediaTargetItem--error');

            app.blacklist.form.set_buttons_enabled(false);
            app.blacklist.form.in_progress = true;

            n.select('.MediaTargetItem__input').attr('disabled','');
        }
    },
    submit: function(type) {
        if (app.blacklist.form.in_progress ||
            !contains(['add', 'remove', 'check'], type = type.toLowerCase()) ) {
            return;
        }

        app.blacklist.form.set_InProgress(true);

        var filterObject = app.forms.Filter('TSBSubChooser');
        var subredditString = app.util.getBestFilterString(filterObject, 'tsb', true);

        var
            done = function(error) {
                app.blacklist.form.set_InProgress(false, error);
            },
            http_method = ({
                'add': 'PUT',
                'remove': 'DELETE',
                'check': 'GET',
            })[type],
            subreddits_string = subredditString,
            mediaurls_string =
                n.select('.MediaTargetItem__input').reduceToString(function(acc, el) {
                    if (el.value.length && !app.url.check_url(el.value)) {
                        n.addClass(n.closest(el, '.MediaTargetItem'), 'MediaTargetItem--error');
                        return 'error';
                    }
                    if (el.value.length == 0 || acc == 'error')
                        return acc;
                    return acc + (acc.length ? ',' : '') + encodeURIComponent(el.value);
                });

        if (mediaurls_string.length == 0) {
            return done('.TSBError--no_urls');
        } else if (subreddits_string.length == 0) {
            return done('.TSBError--no_subreddits');
        } else if (mediaurls_string == 'error') {
            return done('.TSBError--invalid_urls');
        }

        n.select('.MediaTargetItem').removeClass('MediaTargetItem--result');
        n.select('.MediaTargetItem .MediaTargetItem__results').removeAllChildren();

        r(http_method, '/r/{}/blacklist?media_url={}'.format(subreddits_string, mediaurls_string), {
            success: function(result) {
                if (typeof result !== 'object') {
                    return app.blacklist.form.handle.error('object');
                }

                app.blacklist.form.handle.success[type](result);

                n.select('.MediaTargetItem').addClass('MediaTargetItem--result');
            },
            error: function(error, error_msg) {
                app.blacklist.form.handle.error(error, error_msg);
            },
            done: function() {
                done();
            },
        });
    },
    handle: {
        error: function(error, error_msg) {
            n.removeClass('.TSBError--internal', 'hide');
        },
        success: {
            add: function(result) {
                var data = result['data'],
                    processed_map = {}, // processed
                    blacklisted = {};   // present

                forEach(result['processed'], function(item) {
                    if (!processed_map.hasOwnProperty(item[0])) {
                        processed_map[item[0]] = [];
                    }
                    processed_map[item[0]].push(item[1]);
                });

                forEach(result['present'], function(item) {
                    if (!blacklisted.hasOwnProperty(item['channel_id'])) {
                        blacklisted[item['channel_id']] = [];
                    }
                    blacklisted[item['channel_id']].push({
                        subreddit: item['subreddit'],
                        moderator: item['moderator'],
                        timestamp: item['timestamp']
                    });
                });

                for (var channel_id in data) {
                    // noinspection JSUnfilteredForInLoop
                    if (!blacklisted.hasOwnProperty(channel_id)) {
                        // noinspection JSUnfilteredForInLoop
                        blacklisted[channel_id] = [];
                    }
                }

                // console.log("ADD callback called");

                forEach(data, function(channel_id, data_item) {
                    var target_results = app.blacklist.targetitem.get_results(
                        data_item['media_platform'], channel_id);

                    // ----- PROCESSED RECORDS (subreddits added to)
                    if (channel_id in processed_map) {
                        var processed_records = processed_map[channel_id],
                            items = [];

                        if (processed_records.length > 0) {
                            forEach(processed_records, function(subreddit_added_to) {
                                items.push(n('li', subreddit_added_to));
                            });
                        }

                        target_results.append(
                            app.blacklist.targetitem.create_result_block(
                                    'Subreddits added to', items));
                    }
                    // ------ PRESENT RECORDS (subreddits already blacklisted in)
                    if (channel_id in blacklisted) {
                        var list_records = blacklisted[channel_id],
                            items = [];

                        if (list_records.length > 0) {
                            forEach(list_records, function(record) {
                                var subreddit = record['subreddit'];
                                var moderator = record['moderator'];
                                var timestamp = record['timestamp'];

                                items.push(n('li', [
                                    n('span', subreddit),
                                    n('span', 'by'),
                                    n('span', moderator),
                                    n('span', 'at'),
                                    n('span', timeConvert(timestamp)),
                                ]));
                            });

                            target_results.append(
                                app.blacklist.targetitem.create_result_block(
                                        'Subreddits already blacklisted in', items));
                        }
                    }
                });
            },
            remove: function(result) {
                var data = result['data'],
                    processed_map = {};

                forEach(result['processed'], function(item) {
                    if (!processed_map.hasOwnProperty(item[0])) {
                        processed_map[item[0]] = [];
                    }
                    processed_map[item[0]].push(item[1]);
                });

                forEach(data, function(channel_id, data_item) {
                    var target_results = app.blacklist.targetitem.get_results(
                        data_item['media_platform'], channel_id);

                    if (channel_id in processed_map) {
                        var processed_records = processed_map[channel_id];
                        // console.log(processed_records);

                        if (processed_records.length == 0) {
                            target_results.append(
                                app.blacklist.targetitem.create_result_block(
                                        'Subreddits removed from',
                                        n('li.MediaTargetItem__resultLabel--None', 'none')));
                        } else {
                            var items = [];

                            forEach(processed_records, function(subreddit_removed_from) {
                                items.push(n('li',subreddit_removed_from ));
                            });

                            target_results.append(
                                app.blacklist.targetitem.create_result_block(
                                        'Subreddits removed from', items));
                        }
                    }
                });
            },
            check: function(result) {
                var blacklisted = {};

                // add channels in 'present'
                forEach(result['present'], function(item) {
                    var id = (item['platform']+'-'+item['channel_id']);

                    if (!blacklisted.hasOwnProperty(id)) {
                        blacklisted[id] = [];
                    }

                    blacklisted[id].push({
                        subreddit: item['subreddit'],
                        moderator: item['moderator'],
                        timestamp: item['timestamp'],
                        humantime: item['humantiming'],
                        platform:  item['platform'],
                    });
                });

                // add channels not in 'present'
                for (var channel_id in result['data']) {
                    // noinspection JSUnfilteredForInLoop
                    var id = (result['data'][channel_id]['media_platform']+'-'+channel_id);
                    if (!blacklisted.hasOwnProperty(id)) {
                        blacklisted[id] = [];
                    }
                }

                forEach(blacklisted, function(mediatargetitem_id, records) {
                    var target_results = app.blacklist.targetitem.get_results(mediatargetitem_id);

                    if (records.length == 0) {
                        target_results.append(
                            app.blacklist.targetitem.create_result_block(
                                    'Subreddits blacklisted in',
                                    n('li.MediaTargetItem__resultLabel--None', 'none')));
                    } else {
                        var items = [];

                        forEach(records, function(record) {
                            var subreddit = record['subreddit'];
                            var moderator = record['moderator'];
                            var humantime = record['humantime'];
                            var timestamp = record['timestamp'];

                            items.push(n('li', [
                                n('span', subreddit),
                                n('span', 'by'),
                                n('span', moderator),
                                n('span', 'at'),
                                n('span', timeConvert(timestamp)),
                            ]));
                        });

                        target_results.append(
                            app.blacklist.targetitem.create_result_block(
                                    'Subreddits blacklisted in', items));
                    }
                });
            },
        },
    },
};

init('BlacklistEdit', app.blacklist.init);