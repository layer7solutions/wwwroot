init('SimpleBlacklist', function(opts) {
    var sbtconf = opts.sbtconf;

    let subjectInputSelector;
    let subjectFilterSelector;
    let subredditInputSelector;
    let subredditFilterSelector;

    function SBT__GetCol(col_type, row) {
        var col = sbtconf[col_type];
        if (contains(sbtconf['aliases'], col)) {
            col = sbtconf['aliases'][col];
        }
        return row[col];
    }
    function SBT__GetColHead(col_type) {
        var col = sbtconf[col_type];
        col = sbtconf['aliases'][col] || col;
        col = sbtconf['display_names'][col] || col;
        return col;
    }

    function SBT__UpdateSubredditInputPlaceholder(new_placeholder) {
        document.querySelector('.subredditInputSelector .selectr-placeholder').innerText = new_placeholder;
    }

    function normalizeValue(value) {
        if (sbtconf['subject_plural'] == 'users') {
            value = value.replace('//', '');
            if (value.indexOf('/') != -1)
                value = value.slice(value.indexOf('/'));
            value = trim(value, '/').replace(/^(u|user)\//, '').split('/')[0];
            if (!value.match(/^[a-zA-Z0-9\-_]+$/)) {
                return false;
            }
        } else if (sbtconf['subject_plural'] == 'domains') {
            if (!value.match(/^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/)) {
                return false;
            }
        }
        return value;
    }

    function SBT__SanitizeInput(str) {
        return string_replace(str)
                .replaceNewLines(',')
                .replace(' ', ',')
                .replace(';', ',')
                .replace('|', ',')
                .replace('\t', ',')
                .replace(',,', ',')
                .get();
    }

    function SBTAction__GetSubjectValue() {
        return SBT__SanitizeInput(subjectInputSelector.value)
            .split(',')
            .map(v => normalizeValue(v))
            .filter(v => !!v)
            .join(',');
    }

    function SBTAction__SubmitForm(banOrUnban) {
        var
            results_p       = document.getElementById('SBTAction__resultsContainer'),
            results         = document.getElementById('SBTAction__results'),
            resultsTitle    = document.getElementById('SBTAction_resultsTitle'),
            resultsArrow    = document.querySelector('#SBTAction__resultsContainer .forward_arrow--top'),
            sr_exopt        = document.getElementById('SBTAction__subredditInputOption--AllExcept'),
            submitbtn       = document.getElementById((banOrUnban ? 'SBTAdd' : 'SBTRemove') + '__submitButton'),
            submitbtnOther  = document.getElementById((banOrUnban ? 'SBTRemove' : 'SBTAdd') + '__submitButton'),
            submitbtnText   = document.getElementById((banOrUnban ? 'SBTAdd' : 'SBTRemove') + '__submitButtonText'),
            submitbtp       = document.getElementById((banOrUnban ? 'SBTAdd' : 'SBTRemove') + '__submitButtonPending');

        // Hide previous results (if not already headen)
        n.addClass(results_p, 'hide');
        n.removeAllChildren(results);

        // Disable form inputs
        n.addClass(submitbtnText, 'hide');
        n.setAttribute(submitbtn, 'disabled', 'disabled');
        n.setAttribute(submitbtnOther, 'disabled', 'disabled');
        n.disabled(subjectInputSelector, true);
        subredditInputSelector.disable();

        // Show loading icon
        n.removeClass(submitbtp, 'hide');

        var
            sr_list = subredditInputSelector.getValue().join(','), // comma separated string
            subject_list = SBTAction__GetSubjectValue(), // comma separated string
            done = function(doClear) {
                // This function resets the form back to normal
                // once the operation is finished.
                n.removeClass(submitbtnText, 'hide');
                n.removeAttribute(submitbtn, 'disabled');
                n.removeAttribute(submitbtnOther, 'disabled');

                // Hide loading icon
                n.addClass(submitbtp, 'hide');

                if (doClear)
                    subredditInputSelector.clear();
                subredditInputSelector.enable();

                if (doClear)
                    subjectInputSelector.value = '';
                n.disabled(subjectInputSelector, false);

                if (banOrUnban) {
                    resultsArrow.style.left = '40px';
                    resultsTitle.innerText = 'BAN RESULTS';
                } else {
                    resultsArrow.style.left = '140px';
                    resultsTitle.innerText = 'UNBAN RESULTS';
                }

                n.removeClass(results_p, 'hide'); // Show results
            },
            statusResult = function(status) {
                n.html(results, n('div.SBTAction__resultsStatus', {
                    text: status
                }));
            };

        if (sr_exopt.checked) {
            // if exclude mode then set to sr_list to 'all:<categ>-<sr_list>'
            if (sr_list.length == 0) {
                sr_list = 'all:' + sbtconf['sr_category'];
            } else {
                sr_list = 'all:' + sbtconf['sr_category'] + '-' + sr_list;
            }
        }

        if (sr_list.length == 0 && subject_list.length == 0) {
            statusResult('No subreddits or {} were entered!'.format(sbtconf['subject_plural']));
            return done(false);
        }

        if (sr_list.length == 0) {
            statusResult('No subreddits were entered!');
            return done(false);
        }

        if (subject_list.length == 0) {
            statusResult('No {} were entered!'.format(sbtconf['subject_plural']));
            return done(false);
        }

        if (banOrUnban) {
            r('PUT', '/r/{}/blacklist?{}={}'.format(sr_list, sbtconf['subject_type'], subject_list), {
                error: function(error, err_msg) {
                    statusResult('An error occurred: ' + error + " - " + err_msg);
                },
                success: function(result) {
                    if (result['banned'].length == 0) {
                        statusResult('Already banned in all chosen subreddits.');
                        return;
                    }

                    var elements = map(result['banned'], function(item) {
                        var subject_value = SBT__GetCol('subject_col', item);
                        return n('div.SBTAction__resultsItem.valign', [
                            n('div.grow',
                                'Banned <strong>{}</strong> from <strong>{}</strong>'.format(subject_value, item['subreddit'])
                            ),
                            n('a.valign', {
                                text: 'undo',
                                onclick: function(event) {
                                    SBT__UnbanActionAndTriggerTextReset(
                                        subject_value, item['subreddit'], this, 'undo', 'undo', 'undone');
                                }
                            })
                        ]);
                    });

                    n(results, elements);
                },
                done: function(label, http_status) {
                    return done(http_status === 200);
                },
            });
        } else {
            SBT__UnbanSingle(subject_list, sr_list, function(http_status, result, error_msg) {
                if (http_status != 200) {
                    statusResult('An error occurred: ' + result + ' -- ' + error_msg);
                    return done(false);
                }

                if (result['unbanned'].length == 0) {
                    statusResult(
                        'None of these {} are banned in any of these subreddit(s).'.format(sbtconf['subject_plural']));
                    return done(false);
                }

                var elements = [];

                forEach(result['unbanned'], function(item) {
                    var subject_value = SBT__GetCol('subject_col', item);
                    elements.push(n('div.SBTRemove__statusItem.valign', [
                        n('div.grow', 'Unbanned <b>{}</b> from <b>{}</b>'.format(subject_value, item['subreddit'])),
                    ]));
                });

                n(results, elements);
                return done(true);
            });
        }
    }

    function SBT__UnbanActionAndTriggerTextReset(subject, subreddit, trigger_element,
            verb, noun, past_participle) {
        trigger_element.appendChild(n('span.loading.x-small.spacer5-left'));
        n.setAttribute(trigger_element, 'style', 'color:rgba(0,0,0,0.4);cursor:default');

        SBT__UnbanSingle(subject, subreddit, function(http_status) {
            if (http_status != 200) {
                n.html(trigger_element, noun + ' failed');
                n.setAttribute(trigger_element, 'style', '');
            } else {
                n.setAttribute(trigger_element, 'style', 'color:rgba(0,0,0,0.4);cursor:default');
                n.html(trigger_element, 'ban ' + past_participle);
                n.off(trigger_element, 'click');
            }
        });
    }

    function SBT__UnbanSingle(subject, subreddit, callback) {
        var subreddit = is_array(subreddit) ? subreddit.join(',') : subreddit;
        var subject = is_array(subject) ? subject.join(',') : subject;

        if (subreddit == 'all') {
            subreddit = 'all:'+sbtconf['sr_category'];
        }

        subreddit = replace_prefix(subreddit, 'all-', 'all:'+sbtconf['sr_category']+'-');

        r('DELETE', '/r/{}/blacklist?{}={}'.format(subreddit, sbtconf['subject_type'], subject), {
            done: function(label, http_status, a, b) {
                callback.apply(null, [http_status, a, b]);
            }
        });
    }

    function SBTView_GetCurrentPage() {
        return parseInt(string_replace(n.select('.SBTView__currentPage').select(0).value(), ',', ''));
    }

    function SBTView_GetMaxPage() {
        return parseInt(string_replace(n.select('.SBTView__maxPage').select(0).text(), ',', ''));
    }

    function SBTView_GetSubredditInputValue() {
        var sr_value = subredditFilterSelector.getValue();

        sr_value = sr_value.length == 0 ?
                'all' : sr_value.join(',');
        return sr_value;
    }

    function SBTView_GetSubjectInputValue() {
        var subject_value = SBT__SanitizeInput(subjectFilterSelector.value)
            .split(',')
            .map(v => normalizeValue(v))
            .filter(v => !!v)
            .join(',');
        if (!subject_value || !subject_value.length) subject_value = 'all';
        return subject_value;
    }

    function SBTView_AttachListeners() {
        n.on('#SBTView__refreshButton', 'click', function() {
            SBTView_Populate(SBTView_GetCurrentPage());
        });

        n.on('.SBTView__currentPage', 'focus', function() {
            this.oldvalue = this.value;
        });
        n.on('.SBTView__currentPage', 'change', function() {
            var new_page = parseInt(string_replace(this.value, ',', ''));
            var pg_max = SBTView_GetMaxPage();

            if (isNaN(new_page) || isNaN(pg_max)) {
                this.value = this.oldvalue;
                return;
            }

            if (new_page < 1 || new_page > pg_max) {
                this.value = this.oldvalue;
                return;
            }

            SBTView_Populate(new_page);
        });

        n.on('.SBTView__prevButton', 'click', function() {
            if (this.disabled) return;

            var pg      = SBTView_GetCurrentPage(),
                pg_max  = SBTView_GetMaxPage();

            if (isNaN(pg) || isNaN(pg_max)) return;

            if (pg == 1) return;

            SBTView_Populate(pg - 1);
        });

        n.on('.SBTView__nextButton', 'click', function() {
            if (this.disabled) return;

            var pg      = SBTView_GetCurrentPage(),
                pg_max  = SBTView_GetMaxPage();

            if (isNaN(pg) || isNaN(pg_max)) return;

            if (pg == pg_max) return;

            SBTView_Populate(pg + 1);
        });

        n.on(subjectInputSelector, 'change', function() {
            subjectInputSelector.value = SBTAction__GetSubjectValue();
            if (subjectInputSelector.value)
                subjectInputSelector.value += ',';
        });

        n.on(subjectFilterSelector, 'change', function() {
            subjectFilterSelector.value = SBTView_GetSubjectInputValue();
            if (subjectFilterSelector.value)
                subjectFilterSelector.value += ',';
        });
    }

    function SBTView_Populate(page) {
        page = page || 1;
        if (isNaN(page)) {
            page = 1;
        }

        // --- GET VALUES
        var sr_value  = SBTView_GetSubredditInputValue();
        var sub_value = SBTView_GetSubjectInputValue();
        var page_size = parseInt(document.getElementById('SBTView__amountPerPage').value);
        var output    = document.getElementById('SBTView__tableBody');

        // --- RESET OUTPUT, ADD LOADING ICON, DISABLE FORM ELEMENTS
        n.removeClass('#SBTView__loading', 'hide');
        n.select('.SBTView__currentPage, .SBTView__prevButton, .SBTView__nextButton')
            .attr('disabled', 'disabled');

        // --- SEND REQUEST
        r('/r/{}/blacklist?list={}&subject={}'.format(sr_value, sbtconf['subject_type'], sub_value), {
            data: {
                'page': page,
                'amount': page_size,
            },
            receive: function() {
                n.removeAllChildren(output);
                n.addClass('#SBTView__loading', 'hide');
            },
            done: function(status, http_status, result) {
                // check for error
                if (status !== 'success' || !isset(result['listing'])) {
                    n.select('.SBTView__currentPage').value("0");
                    n.select('.SBTView__maxPage').text("0");
                    n.html(output, n('tr', [
                        n('td[colspan=5]', [
                            n('div.spacer-all.calign.valign', [
                                n('strong', { text: 'An error occurred'})
                            ])
                        ])
                    ]));
                    return;
                }

                // check for no results
                if (result['listing'].length == 0) {
                    n.select('.SBTView__currentPage').value("0");
                    n.select('.SBTView__maxPage').text("0");
                    n.html(output, n('tr', [
                        n('td[colspan=5]', [
                            n('div.spacer-all.calign.valign', [
                                n('strong', { text: 'NO RESULTS'})
                            ])
                        ])
                    ]));
                    return;
                }

                // update meta information
                var page        = parseInt(result['page']);
                var page_count  = parseInt(result['page_count']);

                console.log('SBTView: ' + page + ' / ' + page_count);

                n.select('.SBTView__currentPage').removeAttr('disabled').value(page);
                n.select('.SBTView__maxPage').text(page_count.toLocaleString());

                if (page == 1) {
                    n.select('.SBTView__prevButton').attr('disabled', 'disabled');
                } else {
                    n.select('.SBTView__prevButton').removeAttr('disabled');
                }

                if (page == page_count) {
                    n.select('.SBTView__nextButton').attr('disabled', 'disabled');
                } else {
                    n.select('.SBTView__nextButton').removeAttr('disabled');
                }

                // populate
                forEach(result['listing'], function(row) {
                    var subject_value = SBT__GetCol('subject_col', row);
                    var time_value = SBT__GetCol('time_col', row);
                    var mod_value = SBT__GetCol('mod_col', row);

                    output.appendChild(n('tr.SBTView__tableRow', [
                        n('td.SBTView__tableItem.SBTView__tableItem--subreddit', {
                            text: row['subreddit'],
                        }),
                        n('td.SBTView__tableItem.SBTView__tableItem--subject', [
                            n('a[target=_blank][rel="noopener noreferrer"]', {
                                href: app.url.reddit_href('/user/'+subject_value),
                                text: subject_value,
                            })
                        ]),
                        n('td.SBTView__tableItem.SBTView__tableItem--banned_by', [
                            n('a[target=_blank][rel="noopener noreferrer"]', {
                                href: app.url.reddit_href('/user/'+mod_value),
                                text: mod_value,
                            })
                        ]),
                        n('td.SBTView__tableItem.SBTView__tableItem--banned_utc', {
                            html: timeConvert(time_value),
                        }),
                        n('td', [
                            n('a', {
                                'class': 'valign',
                                'text': 'unban',
                                'onclick': function(event) {
                                    SBT__UnbanActionAndTriggerTextReset(
                                        subject_value, row['subreddit'], this, 'unban', 'reversal', 'reversed');
                                },
                            })
                        ]),
                    ]));
                });
            }
        });
    }

    (function() {
        n.text('.SBTView__colHead--subject', SBT__GetColHead('subject_col'));
        n.text('.SBTView__colHead--mod', SBT__GetColHead('mod_col'));
        n.text('.SBTView__colHead--time', SBT__GetColHead('time_col'));

        n.text('.SBT__subjectName', sbtconf['subject_name']);
        forEach('.SBT__subjectNamePlaceholder', function(el) {
            var p = el.getAttribute('placeholder') || '';
            el.setAttribute('placeholder', p + sbtconf['subject_name']);
        });

        n.text('.SBT__subjectPlural', sbtconf['subject_plural']);

        forEach('.SBT__subjectPluralPlaceholder', function(el) {
            var p = el.getAttribute('placeholder') || '';
            el.setAttribute('placeholder', p + sbtconf['subject_plural'] + (sbtconf['subject_plural_suffix'] || '').replace('\\n', '\n'));
        });

        subjectInputSelector = document.getElementById('SBTAction__subjectInput');
        subjectFilterSelector = document.getElementById('SBTView__subjectFilter');

        subjectFilterSelector.placeholder = subjectFilterSelector.placeholder + sbtconf['subject_plural'];

        subredditInputSelector = new Selectr(document.getElementById('SBTAction__subredditInput'), {
            placeholder: 'choose subreddits',
            data: sbtconf.select_options,
            multiple: true,
            searchable: true,
            clearable: true,
            customClass: 'subredditInputSelector',
        });
        subredditFilterSelector = new Selectr(document.getElementById('SBTView__subredditInput'), {
            placeholder: 'filter subreddits',
            data: sbtconf.select_options,
            multiple: true,
            searchable: true,
            clearable: true,
            customClass: 'subredditFilterSelector',
        });

        n.on('#SBTAdd__submitButton', 'click', SBTAction__SubmitForm.bind(null, true));
        n.on('#SBTRemove__submitButton', 'click', SBTAction__SubmitForm.bind(null, false));

        SBTView_Populate();
        SBTView_AttachListeners();

        var placeholder__OnlyThese = 'choose subreddits';
        var placeholder__AllExcept = 'choose subreddits to exclude (optional)';

        n.on('#SBTAction__subredditInputOption--OnlyThese', 'change', function() {
            SBT__UpdateSubredditInputPlaceholder(placeholder__OnlyThese);
            localStorage.setItem('SBTAction__subredditInputOption', 'OnlyThese');
        });
        n.on('#SBTAction__subredditInputOption--AllExcept', 'change', function() {
            SBT__UpdateSubredditInputPlaceholder(placeholder__AllExcept);
            localStorage.setItem('SBTAction__subredditInputOption', 'AllExcept');
        });

        n.on('#SBTAction__resultsInnerContainer .close', 'click', function() {
            n.addClass('#SBTAction__resultsContainer', 'hide');
        });

        if (localStorage.getItem('SBTAction__subredditInputOption') == 'AllExcept') {
            n.select('#SBTAction__subredditInputOption--AllExcept')[0].checked = true;
            SBT__UpdateSubredditInputPlaceholder(placeholder__AllExcept);
        } else {
            n.select('#SBTAction__subredditInputOption--OnlyThese')[0].checked = true;
            SBT__UpdateSubredditInputPlaceholder(placeholder__OnlyThese);
        }
    })();
});