app.forms.tsb_view = {
    state: {
        running: false,
        current_page: 1,
        check_timeout: null,
        amount_per_page: 25,
    },

    init: function() {
        app.forms.Filter('TSBViewSubChooser', {
            inputs: '.SubredditChooser--TSBViewForm .SubredditOption input',
            toggle_all: '.SubredditChooser--TSBViewForm .SubredditOption--all input',
            exclude_values: ['thesentinelbot'],
            on_change: function() {
                n.removeClass('#TSBViewForm__pendingOverlay', 'hide');
                n.removeClass('#TSBViewForm__waitingText', 'hide');

                clearTimeout(app.forms.tsb_view.state.check_timeout);
                app.forms.tsb_view.state.check_timeout = setTimeout(function() {
                    app.forms.tsb_view.run(1);
                }, 750);
            }
        });

        n.on('.TSBForm__SRFilterCollapseButton', 'click', function() {
            if (n.hasClass('#TSBForm__SRFilter', 'collapsed')) {
                n.removeClass('#TSBForm__SRFilter', 'collapsed');
                this.innerText = "Collapse";
            } else {
                n.addClass('#TSBForm__SRFilter', 'collapsed');
                this.innerText = "Uncollapse";
            }
        })

        n.on('#TSBViewForm__ItemCountDropdown', 'DropdownStateChanged', function() {
            app.forms.tsb_view.state.amount_per_page = parseInt(event.detail.value);
            app.forms.tsb_view.run_again();
        });

        n.on('.TSBViewForm__resetButton', 'click', function() {
            app.forms.tsb_view.clearQuery();
        });

        n.on('#TSBViewForm__beginExportButton', 'click', function() {
            app.forms.tsb_view.startExport();
        });

        n.on('#TSBViewForm__downloadExportButton', 'click', function() {
            app.forms.tsb_view.downloadExport();
        });
    },

    run_again: function() {
        app.forms.tsb_view.run(app.forms.tsb_view.state.current_page);
    },

    clearQuery: function() {
        if (app.forms.tsb_view.state.running) {
            return;
        }

        clearTimeout(app.forms.tsb_view.state.check_timeout);

        app.forms.Filter('TSBViewSubChooser').clear();

        app.forms.tsb_view.setStatus('no subreddits selected');
        n.addClass('#TSBViewForm__pendingOverlay', 'hide')
    },

    exportListing: [],

    downloadExport: function() {
        function downloadObjectAsJson(exportObj, exportName){
            var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj));
            var downloadAnchorNode = document.createElement('a');
            downloadAnchorNode.setAttribute("href",     dataStr);
            downloadAnchorNode.setAttribute("download", exportName + ".json");
            document.body.appendChild(downloadAnchorNode); // required for firefox
            downloadAnchorNode.click();
            downloadAnchorNode.remove();
        }

        downloadObjectAsJson({listing: app.forms.tsb_view.exportListing}, 'media_blacklist_export');
    },

    startExport: function() {
        if (!document.getElementById('TSBViewForm__downloadExportButton').disabled) {
            alert('Export already complete. Refresh the page if you want to do another export.');
            return;
        }

        var filterObject = app.forms.Filter('TSBViewSubChooser');
        var subreddits_string = app.util.getBestFilterString(filterObject, 'tsb', true);

        var listing = [];
        var page = 1;
        var amount = 500;
        var maxPageCount = 0;

        function completelyDone() {
            app.forms.tsb_view.state.running = true;
            document.getElementById('TSBViewForm__beginExportButton').disabled = true;
            document.getElementById('TSBViewForm__downloadExportButton').disabled = false;
            n.addClass('#TSBViewForm__exportPendingIcon', 'hide');
            n.removeClass('#TSBViewForm__exportReadyLabel', 'hide');
        }

        function nextPage() {
            r('/r/{}/blacklist?list=media_url'.format(subreddits_string), {
                data: {
                    'page': page,
                    'amount': amount,
                },
                success: function(result) {
                    page = (result['page'] || 0) + 1;
                    maxPageCount = result['page_count'] || 0;

                    app.forms.tsb_view.exportListing = app.forms.tsb_view.exportListing.concat(
                        result['listing'] || []
                    );

                    if (page > maxPageCount || result['listing'].length == 0) {
                        completelyDone();
                        return;
                    }

                    nextPage();
                },
                error: function(error, err_msg) {
                    console.error(error, err_msg);
                    app.forms.tsb_view.state.running = false;
                    document.getElementById('TSBViewForm__beginExportButton').disabled = false;
                    n.addClass('#TSBViewForm__exportPendingIcon', 'hide');
                    alert('An error occurred during export process, try again later');
                },
            });
        }

        if (subreddits_string.length == 0) {
            alert('Must select at least one subreddit to start export.');
            return;
        } else {
            if (app.forms.tsb_view.state.running) {
                alert('Already running.');
                return;
            } else {
                app.forms.tsb_view.state.running = true;
            }

            app.forms.tsb_view.exportListing = [];
            document.getElementById('TSBViewForm__beginExportButton').disabled = true;
            n.removeClass('#TSBViewForm__exportPendingIcon', 'hide');
            nextPage();
        }
    },

    run: function(page) {
        var filterObject = app.forms.Filter('TSBViewSubChooser');
        var subreddits_string = app.util.getBestFilterString(filterObject, 'tsb', true);

        if (subreddits_string.length == 0) {
            app.forms.tsb_view.setStatus('no subreddits selected');
            n.addClass('#TSBViewForm__pendingOverlay', 'hide')
        } else {
            if (app.forms.tsb_view.state.running) {
                return;
            } else {
                app.forms.tsb_view.state.running = true;
                document.querySelector('.TSBViewForm__resetButton').disabled = true;
            }

            n.removeClass('#TSBViewForm__pendingOverlay', 'hide');
            n.addClass('#TSBViewForm__waitingText', 'hide');

            r('/r/{}/blacklist?list=media_url'.format(subreddits_string), {
                data: {
                    'page': page,
                    'amount': app.forms.tsb_view.state.amount_per_page,
                },
                success: function(result) {
                    app.forms.tsb_view.handleResult(result);
                    app.forms.tsb_view.state.current_page = result['page'];

                    if (result['listing'].length == 0) {
                        app.forms.tsb_view.setStatus('no results');
                    } else {
                        n.addClass('#TSBViewForm__status', 'hide');
                        n.removeClass('#TSBViewForm__output', 'hide');
                    }
                },
                error: function(error, err_msg) {
                    n.html('#TSBViewForm__output', '<p class="error-notice">'+error+' - '+err_msg+'</p>');
                },
                done: function() {
                    n.addClass('#TSBViewForm__pendingOverlay', 'hide');
                    app.forms.tsb_view.state.running = false;
                    document.querySelector('.TSBViewForm__resetButton').disabled = false;
                },
            });
        }
    },

    setStatus: function(status_text) {
        n.addClass('#TSBViewForm__output', 'hide');
        n.removeClass('#TSBViewForm__status', 'hide');
        n.html('#TSBViewForm__status', status_text);
    },

    handleResult: function(result) {
        var sr_counter = Counter();

        var parameters = {
            rownum: result['listing'].length,
            page: result['page'],
            page_count: result['page_count'],
            rows: function(tr, index, len) {
                var row = result['listing'][index];
                var tds = tr.children;

                sr_counter.add(row['subreddit'].toLowerCase());

                tds[0].innerHTML = '<div class="cf"><b data-subreddit="'+row['subreddit'].toLowerCase()+'">' + row['subreddit'] + '</b></div> \
                                    <div title="'+timeConvert(row['timestamp'])+'" data-timestamp="'+row['timestamp']+'">' +
                                        timeConvert(row['timestamp'], true)+'</div>';
                tds[1].innerHTML = '<div><b>'+escape_html(row['media_author'])+'</b></div>'+
                                   '<div>'+row['media_channel_id']+'</div>'+
                                   '<div>by '+row['moderator']+'</div>';
                tds[2].innerHTML = '<a class="media-link" target="_blank" rel="noopener noreferrer" href="'+row['media_channel_url']+'">'+
                                    app.util.create_media_icon(row['media_platform'])+'</a>';
            },
            prevbtn: function(el) {
                if (result['page'] != 1) {
                    //n.addClass(el, 'blue');
                    n.on(el, 'click', function() {
                        app.forms.tsb_view.run(result['page'] - 1)
                    });
                } else {
                    n.addClass(el, 'disabled');
                }
            },
            nextbtn: function(el) {
                if (result['page'] != result['page_count']) {
                    //n.addClass(el, 'blue');
                    n.on(el, 'click', function() {
                        app.forms.tsb_view.run(result['page'] + 1)
                    });
                } else {
                    n.addClass(el, 'disabled');
                }
            },
            page_chooser: function(el) {
                n.on(el, 'change', function() {
                    var new_page = el.value;
                    if (is_int(new_page)) {
                        new_page = parseInt(new_page);
                        if (new_page >= 1 && new_page <= result['page_count']) {
                            app.forms.tsb_view.run(new_page);
                            return;
                        }
                    }
                    el.value = result['page'];
                });
                return result['page'];
            },
        };

        var output = n.gen(
            '.fsplit.TSBViewPageNav > \
                button.secondary.TSBViewPageButton.TSBViewPageButton--prevPage.{prevbtn}["previous page"] + \
                (.TSBViewPageLabel.page-label > \
                    span["Page"] + \
                    input[type=number][min="1"][max="{page_count}"][value="{page_chooser}"][style="width:60px"] + \
                    span["/"] + \
                    span["{page_count}"] \
                ) + \
                button.secondary.TSBViewPageButton.TSBViewPageButton--nextPage.{nextbtn}["next page"] ^ \
            (.spacer.TSBViewResultWrapper > \
                table > \
                    (tbody > \
                        (tr.{rows} > td + td + td) * {rownum}) \
            ) + .fsplit.TSBViewPageNav > \
                button.secondary.TSBViewPageButton.TSBViewPageButton--prevPage.{prevbtn}["previous page"] + \
                button.secondary.TSBViewPageButton.TSBViewPageButton--nextPage.{nextbtn}["next page"] \
                ', parameters);

        //var sr_length = sr_counter.length();

        //colorset.unshift([204/360.0,90/100.0,60/100.0]);

        function string_to_color(str) {
            var hash = 0;
            for (var i = 0; i < str.length; i++) {
                hash = str.charCodeAt(i) + ((hash << 5) - hash);
            }
            var colour = '#';
            for (var i = 0; i < 3; i++) {
                var value = (hash >> (i * 8)) & 0xFF;
                colour += ('00' + value.toString(16)).substr(-2);
            }
            return colour;
        }

        forEach(sr_counter.map, function(subreddit, x, index) {
            //var hsv_array = colorset[index];
            //var bg_string = 'background:hsl('+(hsv_array[0]*360)+', '+(hsv_array[1]*100)+'%, '+(hsv_array[2]*100)+'%)';
            var bg_string = 'background: ' + string_to_color(subreddit);
            forEach(output.querySelectorAll('[data-subreddit="'+subreddit+'"]'), function(el) {
                el.setAttribute('style', bg_string);
            });
        });

        n.html('#TSBViewForm__output', output);
    }

};

init('BlacklistView', app.forms.tsb_view.init);