(function(handle) {
    init('BlacklistReports', function() {
        // ----- Search on pressing <Enter> in the textfield
        n.on('.MediaReportForm__SubjectInput', 'enter', function(event) {
            var type = this.getAttribute('data-type');
            n.select('#MediaReportForm__SearchButton--'+type).click();
        });

        // ----- When output close button is clicked:
        n.on('.MediaReportResult .close', 'click', function(event) {
            n.select(this)
              .closest('.MediaReportResult')
              .addClass('hide');
            handle.update_hash(); // no parameter -> remove hash
        });

        // ----- On result tab change
        n.on('.MediaReportTab', 'click', function(event) {
            var type = this.getAttribute('data-type');
            var name = this.getAttribute('name');

            n.select('.MediaReportTab[data-type='+type+']').removeClass('selected');
            n.addClass(this, 'selected');

            var output = n.select('#MediaReportOutput--'+type);
            output.select('.MediaReportTabPane').addClass('hide');
            output.select('.MediaReportTabPane[name="'+name+'"]').removeClass('hide');
        });

        // ----- When search button is clicked
        n.on('.MediaReportForm__SearchButton', 'click', function(event) {
            var type = this.getAttribute('data-type');

            var elements = {
                parent      : n.select('#MediaReport__Inner--'+type),
                subject_in  : n.select('#MediaReportForm__SubjectInput--'+type),
                error_out   : n.select('#MediaReportForm__Error--'+type),
                wrapper     : n.select('#MediaReportResult--'+type),
                output      : n.select('#MediaReportOutput--'+type),
                search_btn  : event.currentTarget,
            };

            extend(elements, {
                CurrentAmount: elements.wrapper.select('[name=CurrentAmount]'),
                CurrentSubject: elements.wrapper.select('[name=CurrentSubject]'),
            });

            var value = elements.subject_in.value();

            if (!value.length) {
                elements.error_out.text('Input cannot be blank!').removeClass('hide');
                return;
            }

            handle.send(type, value, elements);
        });

        // ----- on page load, check hash
        if (window.location.hash) {
            handle.form_jump(
                query_param('type',    window.location.hash.substr(1) || 'channel'),
                query_param('subject', window.location.hash.substr(1))
            );
        }
    });
})({
    // VARIABLES
    // ~~~~~~~~~

    result_max: 1000,

    // UTILITY FUNCTIONS
    // ~~~~~~~~~~~~~~~~~

    is_valid_type: function(type) {
        return n.select('#MediaReport--'+type).length;
    },
    form_jump: function(type, subject) {
        if (!isset(type) || !isset(subject) || !this.is_valid_type(type)) return;
        var parent = document.querySelector('#MediaReport--'+type);

        n.select('#MediaReportForm__SubjectInput--'+type).value(subject)
            .then().select('#MediaReportForm__SearchButton--'+type).click()
            .then(this.update_hash, type, subject);

        if (!n.elementInViewport(parent)) {
            if (parent.getAttribute('data-index') == 0) {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            } else {
                parent.scrollIntoView(true);
            }
        }

    },
    update_hash: function(subject_type, subject_value) {
        var query_str = empty(subject_value) ? '' :
            'type='     + subject_type +
            '&subject=' + encodeURIComponent(subject_value.trim());

        history.replaceState(undefined, undefined, '#'+query_str);
    },

    // MAIN HANDLER
    // ~~~~~~~~~~~~

    send: function(subject_type, subject_value, elements, offset, is_continuation) {
        var handle = this;

        offset = offset || 0;
        is_continuation = is_continuation || false;

        r('/r/all:tsb/blacklist/reports', {
            data: {
                type    : subject_type,
                subject : subject_value,
                offset  : offset,
                amount  : this.result_max,
                prefer  : 'no_sr_guard'
            },
            start: function() {
                elements.error_out.addClass('hide');

                if (offset == 0) {
                    elements.wrapper.addClass('hide');
                    elements.parent.append(n('.pending-overlay.posStatic', n('.loading.loading--double')));
                } else {
                    var last_td = elements.output.select('.card-LoadMore');
                    last_td.select('.card-LoadMoreIndicator').remove();
                    last_td.append(n('.dispFlex.calign.spacer20-vert', n('.loading')));
                }

            },
            receive: function() {
                if (offset == 0) {
                    elements.parent.select('.pending-overlay').remove();
                } else {
                    elements.output.select('.card-LoadMore').remove();
                }

                handle.update_hash(subject_type, subject_value);
            },
            success: function(result) {
                result.type = result['matter'][0]['type'];
                result.subject = result['matter'][0]['subject'];

                n(elements.output, ReportComponent, extend(result, {
                    handle: handle,
                    elements: elements,
                    is_continuation: is_continuation,
                }));

                n.removeClass(elements.wrapper, 'hide');
            },
            error: function(error, err_msg) {
                return false;
            },
        });
    }
});