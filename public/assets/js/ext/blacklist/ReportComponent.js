var ReportActiveData = {};

var ReportComponent =  {
    NEW: 0,
    CONTINUATION: 1,
    QUIT: 2,

    types: {
        'channel': 'media_author',
        'user': 'author',
    },

    init: function(props) {
        this.mdgen = Snudown.getParser();
        this.setAmountLabel = function(label_text) {
            n.select('#MediaReportOutput__AmountLabel--'+props.type).text(label_text);
        };
        this.getActiveData = function(property, default_value) {
            var data = ReportActiveData[props.type][props.subject];
            if (empty(property)) {
                return data;
            }
            return data[property] || default_value || undefined;
        };
        this.setActiveData = function(property, value) {
            return (ReportActiveData[props.type][props.subject][property] = value);
        };

        if (!ReportActiveData.hasOwnProperty(props.type)) {
            ReportActiveData[props.type] = {};
        }
        if (!props.is_continuation || !ReportActiveData[props.type].hasOwnProperty(props.subject)) {
            ReportActiveData[props.type][props.subject] = {};
        }
    },
    render: function(props) {
        var self  = this,
            state = null;

        (function(elements) {
            var prev_subject = elements.CurrentSubject.value(),
                prev_amount  = parseInt(elements.CurrentAmount.value());

            elements.wrapper.select('.MediaReportTab')
                .removeClass('selected')
                .select(0).addClass('selected');

            if (props.subject == prev_subject && props.offset != 0) {
                state = self.CONTINUATION;
                elements.CurrentAmount.value(prev_amount + props.amount);
            } else {
                state = self.NEW;
                elements.CurrentSubject.value(props.subject);
                elements.CurrentAmount.value(props.amount);
            }

            var new_amount = parseInt(elements.CurrentAmount.value());
            if (new_amount < props.total) {
                self.setAmountLabel(new_amount + " of " + props.total);
            } else {
                self.setAmountLabel(new_amount || "n/a");
            }

            if (new_amount == 0) {
                state = self.QUIT;
            }
        })(props.elements);

        switch (state) {
            case self.QUIT:
                props.elements.output.html([
                    n('.MediaReportTabPane[name="list"]',
                        n('.output-status.spacer10-bottom', 'No Results')
                    ),
                    n('.MediaReportTabPane.hide[name="group"]',
                        n('.output-status.spacer10-bottom', 'No Results')
                    )
                ]);
                return;
            case self.NEW:
                props.elements.output.html([
                    // BEGIN -- LIST PANE
                    n('.MediaReportTabPane[name="list"]',
                        n('table', [
                            n('thead', [
                                n('th', "Date/Time"),
                                n('th', "Author"),
                                n('th', "Subreddit"),
                                n('th', "Media Author"),
                                n('th', "Channel ID"),
                                n('th'),
                                n('th'),
                                n('th'),
                                n('th')
                            ])
                        ])
                    ),
                    // END -- LIST PANE
                    // BEGIN -- GROUP PANE
                    n('.MediaReportTabPane.hide[name="group"]', [
                        // BEGIN -- GROUP PANE HEADER
                        n('.MediaReportTabPane__TopArea.valign', [
                            n('.MediaReportTabPane__TopLabel', [
                                function() {
                                    if (props.type == 'user') {
                                        return n('span', 'for user: ');
                                    } else if (props.type == 'channel') {
                                        return n('span', 'for channel: ');
                                    } else if (props.type == 'channel_name') {
                                        return n('span', 'for keyword(s): ');
                                    }
                                },
                                function() {
                                    var dataFirstRow = props.listing[0];

                                    if (props.type == 'user') {
                                        return n('a', {
                                            text: dataFirstRow.author || 'undefined',
                                            href: app.url.reddit_href('u/'+dataFirstRow.author),
                                            target: '_blank',
                                            rel: 'noopener noreferrer',
                                        });
                                    } else if (props.type == 'channel') {
                                        return n('a', {
                                            text: dataFirstRow.media_channel_url || 'undefined',
                                            href: dataFirstRow.media_channel_url,
                                            target: '_blank',
                                            rel: 'noopener noreferrer',
                                        });
                                    } else {
                                        return n('span', {
                                            text: props.subject || 'undefined',
                                        });
                                    }
                                }
                            ]),
                            n('.MediaReportTabPane__TopLabel.is--button.dropdown--trigger', [
                                n('span', 'sort:'),
                                n('span.dropdown--label', 'descending'),
                                n('i.icon.zmdi.zmdi-chevron-down'),
                                n('.dropdown.dropdown--choices', [
                                    n('span', 'unsorted'),
                                    n('span.selected', 'descending'),
                                    n('span', 'ascending'),
                                ], {
                                    'onDropdownStateChanged': function(event) {
                                        var pane = n.closest(this, '.MediaReportTabPane');
                                        var rows = n.select(pane, 'table tbody tr');

                                        var reorderFunc = ({
                                            'unsorted': function(row) {
                                                row.removeAttribute('style');
                                            },
                                            'descending': function(row) {
                                                var orderNum = row.getAttribute('data-order-reverse');
                                                row.setAttribute('style', 'order:'+orderNum);
                                            },
                                            'ascending': function(row) {
                                                var orderNum = row.getAttribute('data-order');
                                                row.setAttribute('style', 'order:'+orderNum);
                                            },
                                        })[event.detail.value];

                                        rows.forEach(reorderFunc);
                                    }
                                })
                            ]),
                            n('.MediaReportTabPane__TopLabel.is--button', [
                                n('span', 'create markdown table'),
                            ], {
                                onclick: function() {
                                    var
                                        tabPaneParent = n.closest(this, '.MediaReportTabPane'),
                                        current_sort = tabPaneParent.querySelector(
                                            '.MediaReportTabPane__TopLabel.dropdown--trigger .dropdown--label')
                                            .textContent,
                                        sort_func = null;

                                    if (current_sort == 'descending') {
                                        sort_func = function(a, b) {
                                            a = parseInt(a.getAttribute('data-order-reverse'));
                                            b = parseInt(b.getAttribute('data-order-reverse'));
                                            return a - b;
                                        };
                                    } else if (current_sort == 'ascending') {
                                        sort_func = function(a, b) {
                                            a = parseInt(a.getAttribute('data-order'));
                                            b = parseInt(b.getAttribute('data-order'));
                                            return a - b;
                                        };
                                    }

                                    var contents = n.select(tabPaneParent, 'table thead tr')
                                                    .attr('data-md-table');

                                    n.select(tabPaneParent, 'table tbody tr')
                                        .sort(sort_func)
                                        .forEach(function(row_element) {
                                            contents += row_element.getAttribute('data-md-table') + "\n";
                                        });

                                    app.dialog(n('div', [
                                        n('h4', 'Markdown Table'),
                                        n('textarea.code.DialogSelectAll', {
                                            readonly: 'readonly',
                                            text: contents,
                                            style: 'width: 100%;' +
                                                'min-height: 200px;' +
                                                'resize: vertical;'
                                        })
                                    ]), app.DIALOG_MODAL);
                                }
                            })
                        ]),
                        // END -- GROUP PANE HEADER
                        // BEGIN -- GROUP PANE TABLE
                        n('table', [
                            n('thead',
                                n('tr',
                                    function() {
                                        if (props.type == 'user') {
                                            return {
                                                'html': [
                                                    n('th', 'Channel'),
                                                    n('th', 'Submitted #'),
                                                ],
                                                'data-md-table': "Channel|Submitted #\n-|-\n"
                                            };
                                        } else if (props.type == 'channel') {
                                            return {
                                                'html': [
                                                    n('th', 'Reddit User'),
                                                    n('th', 'Submitted #'),
                                                ],
                                                'data-md-table': "User|Submitted #\n-|-\n"
                                            };
                                        } else if (props.type == 'channel_name') {
                                            return {
                                                'html': [
                                                    n('th', 'Channel'),
                                                    n('th', 'Match Amount'),
                                                ],
                                                'data-md-table': "Channel|Match Amount\n-|-\n"
                                            };
                                        }
                                    }
                                )
                            )
                        ])
                        // END -- GROUP PANE TABLE
                    ]),
                    // END -- GROUP PANE
                    // BEGIN -- GRAPH PANE (not implemented)
                    n('.MediaReportTabPane.hide[name="graph"]', n('span', 'graph pane')),
                    // END -- GRAPH PANE
                ]);
                break;
            case self.CONTINUATION:
                // No need to do anything to the "list" pane because we'll just be appending another tbody
                // For the "group" pane, we have to remove the existing tbody as it'll need to be overwritten
                n.remove(props.elements.output.select('.MediaReportTabPane[name="group"] table tbody')[0]);
                break;
        }

        var groupCounter = self.getActiveData('groupCounter', Counter());
        var groupInfo = self.getActiveData('groupInfo', {});

        // BEGIN -- TABPANE : LIST
        (function() {
            var list_tbody = [
                n('tbody', map(props.listing, function(data) {
                    if (props.type == 'user' || props.type == 'channel_name') {
                        groupCounter.add(data.media_channel_id);
                        if (!groupInfo.hasOwnProperty(data.media_channel_id))
                            groupInfo[data.media_channel_id] = data;
                    } else if (props.type == 'channel') {
                        groupCounter.add(data.author);
                        if (!groupInfo.hasOwnProperty(data.author))
                            groupInfo[data.author] = {author: data.author};
                    }

                    return self.gen_row(self, data, props.handle);
                }, []))
            ];

            if (props.offset + props.amount < props.total) {
                list_tbody.push(
                    n('tbody',
                        n('tr',
                            n('td.card-LoadMore[colspan=8][style="background:transparent!important"]',
                                n('.card-LoadMoreIndicator', [
                                    n('h4', 'And {} more'.format(props.total - (props.offset + props.amount))),
                                    n('span', 'Click to load next ' + props.handle.result_max)
                                ])
                            )
                        ),
                    {onclick: function() {
                        if (!n.hasClass(this, 'is--LoadComplete')) {
                            n.addClass(this, 'is--LoadComplete');
                            props.handle.send(
                                props.type,
                                props.subject,
                                props.elements,
                                props.offset + props.amount, // offset
                                true // is_continuation = true
                            );
                        }
                    }})
                );
            }

            n(props.elements.output.select('.MediaReportTabPane[name="list"] table')[0], list_tbody);
        })();
        // END -- TABPANE : LIST

        self.setActiveData('groupCounter', groupCounter);
        self.setActiveData('groupInfo', groupInfo);

        // BEGIN -- TABPANE : GROUP
        n(props.elements.output.select('.MediaReportTabPane[name="group"] table')[0],
            n('tbody', map(Object.keys(groupCounter.map), function(key) {
                var info = extend(groupInfo[key], {
                    'amount': groupCounter.get(key),
                    'author_link': app.url.reddit_href('u/'+groupInfo[key].author),
                });
                var attrib = {
                    'data-order': String(info.amount),
                    'data-order-reverse': String(-info.amount),
                    'style': 'order:'+String(-info.amount),
                };

                switch (props.type) {
                    case 'user':
                    case 'channel_name':
                        extend(attrib, {
                            'data-md-table':
                                "[{media_author}]({media_channel_url})|{amount}".format(info),
                            'html': [
                                n('td', [
                                    n('a', {
                                        text: info.media_author,
                                        onclick: function() {
                                            props.handle.form_jump('channel', info.media_channel_id);
                                        },
                                    }),
                                    n('a', {
                                        'class': 'fr icon icon28 icon28--noradius zmdi zmdi-open-in-new',
                                        'target': '_blank',
                                        'rel': 'noopener noreferrer',
                                        'href': info.media_channel_url,
                                    })
                                ]),
                                n('td', n('span', String(info.amount))),
                            ]
                        });
                        break;
                    case 'channel':
                        extend(attrib, {
                            'data-md-table':
                                "[{author}]({author_link})|{amount}".format(info),
                            'html': [
                                n('td', [
                                    n('a', {
                                        text: info.author,
                                        onclick: function() {
                                            props.handle.form_jump('user', info.author);
                                        },
                                    }),
                                    n('a', {
                                        'class': 'fr icon icon28 icon28--noradius zmdi zmdi-open-in-new',
                                        'target': '_blank',
                                        'rel': 'noopener noreferrer',
                                        'href': info.author_link,
                                    })
                                ]),
                                n('td', n('span', String(info.amount))),
                            ],
                        });
                        break;
                }

                return n('tr', attrib);
            }, []))
        );
        // END -- TABPANE : GROUP
    },

    gen_row: function(self, data, handle) {
        function prop(name) {
            return n.simple_gen('td', {class: 'MediaReport__ResultColumn--'+name});
        }
        return n('tr', {
            'data-thing_id': data['thing_id'],
            'data-action_id': data['id'],
            'data-action_utc': data['action_utc'],
            'onclick': function() {
                n.toggleClass(this, 'selected');
            }
        }, [
            n(prop('timestamp'), {
                'innerHTML': timeConvert(data['thing_created'], true),
                'title': timeConvert(data['thing_created'], false)
            }),
            n(prop('author'), [
                n('div', {'class': 'valign'}, [
                    n('a', {
                        'text': data['author'],
                        'onclick': function() {
                            handle.form_jump('user', data['author'])
                        }
                    }),
                    n('span', {'class': 'grow'}),
                    n('a', {
                        'class': 'icon icon28 icon28--noradius zmdi zmdi-open-in-new',
                        'target': '_blank',
                        'rel': 'noopener noreferrer',
                        'href': app.url.reddit_href('u/'+data['author']),
                    })
                ])
            ]),
            n(prop('subreddit'),n('span', data['subreddit'])),
            n(prop('media_author'),
                n('div', {'class': 'valign'}, [
                    n('a', {
                        'text': data['media_author'],
                        'onclick': function() {
                            handle.form_jump('channel', data['media_channel_id'])
                        }
                    }),
                    n('span', {'class': 'grow'}),
                    n('a', {
                        'class': 'icon icon28 icon28--noradius zmdi zmdi-open-in-new',
                        'target': '_blank',
                        'rel': 'noopener noreferrer',
                        'href': data['media_channel_url'],
                    })
                ])
            ),
            n(prop('media_channel_id'), n('code', data['media_channel_id'])),
            n(prop('media_platform'),
                n('a', {
                    'target': '_blank',
                    'rel': 'noopener noreferrer',
                    'class': 'media-link',
                    'href': data['media_link'],
                    'data-platform': data['media_platform'],
                    'html': app.util.create_media_icon(data['media_platform']),
                })
            ),
            n(prop('permalink'),
                n('a', {
                    'target': '_blank',
                    'rel': 'noopener noreferrer',
                    'href': data['permalink'],
                    'html': n('i', {'class': 'icon zmdi zmdi-open-in-new'})
                })
            ),
            n(prop('thingpreview ThingPreviewParent'), [
                n('a', {
                    'html': n('i', {'class': 'icon zmdi zmdi-eye'})
                }),
                n('div', {'class': 'ThingPreview'}, function() {
                    var children = [];
                    var is_comment = !isset(data['thing_title']) || !data['thing_title'].length;

                    children.push(n('div', {'class': 'ThingPreview__type'}, [
                        n('span', {'class': 'label'}, (is_comment ? 'COMMENT' : 'POST')),
                        n('span', 'by'),
                        n('a', {
                            'href': app.url.reddit_href(data['author']),
                            'text': '/u/'+data['author'],
                        }),
                    ]));

                    children.push(n('div', {
                        'class': 'ThingPreview__title',
                        'text': data['thing_title'],
                    }));

                    if (isset(data['thing_data']) && data['thing_data'].length) {
                        children.push(n('div', {
                            'class': 'ThingPreview__data',
                            'html': self.mdgen.render(data['thing_data']),
                        }));
                    }

                    return children;
                }),
            ]),
            n(prop('thingtype'), [
                n('span', {'class': 'label'}, function() {
                    var is_comment = !isset(data['thing_title']) || !data['thing_title'].length;
                    return is_comment ? 'C' : 'P';
                })
            ])
        ]);
    },
};