init('BlacklistGlobal', function(opts) {
    const OUTPUT_PENDING   = document.getElementById('GlobalQueueResult--pending');
    const OUTPUT_APPROVED  = document.getElementById('GlobalQueueResult--approved');
    const OUTPUT_DENIED    = document.getElementById('GlobalQueueResult--denied');
    const OUTPUT_ALL       = [OUTPUT_PENDING, OUTPUT_APPROVED, OUTPUT_DENIED];

    const STATE = {
        is_processing: false,
        is_admin: opts.is_admin,
        request_pref: opts.is_admin ? 'admin' : '',
    };

    const PREPARE_OUTPUT = function(output_el) {
        n.select(output_el)
            .removeClass('GlobalQueueResult--NoResults')
            .addClass('GlobalQueueResult--RealOutput')
            .select('.GlobalQueueResult--NoResultsLabel').remove();
        return n.select(
                output_el.querySelector('tbody')
                || n.select(output_el).html(
                    n('table', [
                        n('thead', [
                            n('tr', [
                                n('th.globalqueue_th--platform'),
                                n('th.globalqueue_th--status["Status"]'),
                                n('th.globalqueue_th--channel["Channel"]'),
                                (function() {
                                    return STATE.is_admin ? [
                                        n('th.globalqueue_th--enqueued_by["Req Mod"]'),
                                        n('th.globalqueue_th--reviewer["Reviewer"]'),
                                    ] : null;
                                })(),
                                n('th.globalqueue_th--enqueued_time["Req Time"]'),
                                n('th.globalqueue_th--reason["Reason"]'),
                            ])
                        ]),
                        n('tbody')
                    ])).select('tbody')[0]
                );
    };
    const CHECK_OUTPUT = function(output_el) {
        var tbody = output_el.querySelector('tbody');
        if (tbody && tbody.childElementCount == 0) {
            n.removeAllChildren(output_el);
        }
        if (output_el.childElementCount == 0) {
            n.select(output_el)
                .addClass('GlobalQueueResult--NoResults')
                .removeClass('GlobalQueueResult--RealOutput')
                .append(n('.GlobalQueueResult--NoResultsLabel["None"]'));
        }
    };

    app.forms.tsbglobalqueue = {
        is_processing: function() {
            return STATE.is_processing;
        },

        init: function() {
            n.on('#GlobalQueue__RefreshButton', 'click', app.forms.tsbglobalqueue.populate);

            n.on('#GlobalQueue__EnqueueButton', 'click', function() {
                app.forms.tsbglobalqueue.enqueue(
                    n.popValue('#GlobalQueue__EnqueueURL'),
                    n.popValue('#GlobalQueue_EnqueueReason')
                );
            });

            app.forms.tsbglobalqueue.populate();
        },
        enqueue: function(media_url, reason) {
            if (STATE.is_processing) {
                return;
            }

            r('/blacklist/globalqueue/enqueue', {
                data: {
                    'prefer':       STATE.request_pref,
                    'media_url':    media_url,
                    'reason':       reason,
                },
                start: function() {
                    STATE.is_processing = true;
                    n.disabled('#GlobalQueue__EnqueueButton', true);
                    n.removeClass('#GlobalQueue__EnqueueStatus', 'hide');
                    n.removeAllChildren('#GlobalQueue__EnqueueError');
                },
                success: function(result) {
                    if (result.in_blacklist.length >= 1) {
                        n.text('#GlobalQueue__EnqueueError', 'Already blacklisted');
                        return;
                    }
                    if (result.in_queue.length >= 1) {
                        n.text('#GlobalQueue__EnqueueError', 'Already pending');
                        return;
                    }
                    if (result.enqueued.length < 1) {
                        n.text('#GlobalQueue__EnqueueError', 'Invalid media URL');
                        return;
                    }

                    PREPARE_OUTPUT(OUTPUT_PENDING).prepend(app.forms.tsbglobalqueue.create_row(result.enqueued[0]));
                },
                error: function(error, error_msg) {
                    n.text('#GlobalQueue__EnqueueError', (error == 'BAD_PARAMETER') ?
                            'Please enter a media URL' : 'ERROR: ' + error);
                },
                done: function() {
                    STATE.is_processing = false;
                    n.disabled('#GlobalQueue__EnqueueButton', false);
                    n.addClass('#GlobalQueue__EnqueueStatus', 'hide');
                },
            });
        },
        populate: function() {
            if (STATE.is_processing) {
                return;
            }

            r('/blacklist/globalqueue/get', {
                data: {
                    'prefer': STATE.request_pref,
                },
                start: function() {
                    STATE.is_processing = true;
                    n.removeClass(OUTPUT_ALL, 'GlobalQueueResult--RealOutput');
                    n.html(OUTPUT_ALL, n.gen('.content.row > p["Loading..."]'));
                },
                success: function(result) {
                    forEach([
                        {list: result.pending,  out: OUTPUT_PENDING },
                        {list: result.approved, out: OUTPUT_APPROVED },
                        {list: result.denied,   out: OUTPUT_DENIED },
                    ], function(data) {
                        if (data.list.length == 0) {
                            n.select(data.out)
                                .addClass('GlobalQueueResult--NoResults')
                                .removeClass('GlobalQueueResult--RealOutput')
                                .html(n('.GlobalQueueResult--NoResultsLabel["None"]'));
                            return;
                        }

                        var tbody = PREPARE_OUTPUT(data.out);
                        forEach(data.list, function(row_data) {
                            tbody.append(app.forms.tsbglobalqueue.create_row(row_data));
                        });
                    });
                },
                done: function() {
                    STATE.is_processing = false;
                },
            });
        },
        create_row: function(row) {
            return n('tr.globalqueueitem', [
                n('td.globalqueue--platform', [
                    n('a.media-link', {
                        target: '_blank',
                        rel: 'noopener noreferrer',
                        href: app.url.add_protocol(row.media_url),
                        html: app.util.create_media_icon(row.media_platform),
                    })
                ]),
                n('td.globalqueue--status', function() {
                    if (row.approval_status === 0) { // Pending
                        if (!STATE.is_admin) {
                            return n('span.status.pending', {text: 'Pending'});
                        }
                        return [
                            n('span.push.status.open.statuschange--approve', {
                                text: 'Approve',
                                onclick: function(event) {
                                    var item = n.select(this).closest('.globalqueueitem').addClass('opacity50p');
                                    r('/blacklist/globalqueue/admin_approve', {
                                        data: {
                                            'id': row.id,
                                            'prefer': STATE.request_pref,
                                        },
                                        success: function(result) {
                                            if (!result || result.success !== true) {
                                                return false;
                                            }

                                            item.remove()
                                                .removeClass('opacity50p')
                                                .select('.globalqueue--reviewer')
                                                    .text(CURRENT_USER)
                                                .back().select('.globalqueue--status')
                                                    .html(n.gen('span.green > i.icon.zmdi.zmdi-check.fontWeight700'))
                                                .back()
                                                    .prependTo(PREPARE_OUTPUT(OUTPUT_APPROVED));
                                            CHECK_OUTPUT(OUTPUT_PENDING);
                                        },
                                    });
                                },
                            }),
                            n('span.push.status.closed.statuschange--deny', {
                                text: 'Deny',
                                onclick: function(event) {
                                    var item = n.select(this).closest('.globalqueueitem').addClass('opacity50p');
                                    r('/blacklist/globalqueue/admin_deny', {
                                        data: {
                                            'id': row.id,
                                            'prefer': STATE.request_pref,
                                        },
                                        success: function(result) {
                                            if (!result || result.success !== true) {
                                                return false;
                                            }

                                            item.remove()
                                                .removeClass('opacity50p')
                                                .select('.globalqueue--reviewer')
                                                    .text(CURRENT_USER)
                                                .back().select('.globalqueue--status')
                                                    .html(n.gen('span.red > i.icon.zmdi.zmdi-close.fontWeight700'))
                                                .back()
                                                    .prependTo(PREPARE_OUTPUT(OUTPUT_DENIED));
                                            CHECK_OUTPUT(OUTPUT_PENDING);
                                        },
                                    });
                                },
                            }),
                        ];
                    } else if (row.approval_status === 1) { // Approved
                        return n('span.green', n('i.icon.zmdi.zmdi-check.fontWeight700'));
                    } else if (row.approval_status === 2) { // Denied
                        return n('span.red', n('i.icon.zmdi.zmdi-close.fontWeight700'));
                    }
                }),
                n('td.globalqueue--channel', {style:'text-overflow:ellipsis'}, [
                    n('span', {text: row.media_channel_author}),
                    n('br'),
                    n('a', {
                        target: '_blank',
                        rel: 'noopener noreferrer',
                        href: SITE_URL+'blacklist/reports/#subject=' + row.media_channel_id,
                        text: row.media_channel_id,
                    }),
                ]),
                (function() {
                    return STATE.is_admin ? [
                        n('td.globalqueue--enqueued_by[style="width:110px"]', {
                            text: row.enqueued_by || 'n/a',
                        }),
                        n('td.globalqueue--reviewer[style="width:110px"]', {
                            text: row.reviewer || 'n/a',
                        }),
                    ] : null;
                })(),
                n('td.globalqueue--enqueued_time', {
                    style: 'width:120px',
                    text: timeConvert(row.enqueued_time),
                }),
                n('td.globalqueue--reason' + (empty(row.reason) ? '.globalqueue--reasonNone' : ''), {
                    style: 'width:230px',
                    text: row.reason,
                }),
            ]);
        },
    };
    app.forms.tsbglobalqueue.init();
});