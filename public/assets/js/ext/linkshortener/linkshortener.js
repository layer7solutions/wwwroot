init('LinkShortener', function(opts) {
    var
        getRowFromElement = function(elem) {
            return n.hasClass(elem, 'ShortLinkRow') ? elem : n.closest(elem, '.ShortLinkRow');
        },
        getOriginalLinkElement = function(row) {
            return getRowFromElement(row).querySelector('.ShortLinkRow__originalLink a');
        },
        getShortLinkElement = function(row) {
            return getRowFromElement(row).querySelector('.ShortLinkRow__link a');
        },
        getEditInputElement = function(row) {
            return getRowFromElement(row).querySelector(
                '.ShortLinkRow__originalLink .ShortLinkRowButtons--inEdit input[type=text]');
        },
        resetToRowValue = function(row) {
            row = getRowFromElement(row);
            var row_value = row.getAttribute('data-value');

            n.select(getOriginalLinkElement(row)).text(row_value).attr('href', row_value);
            n.value(getEditInputElement(row), row_value);
        },
        setLinkTooltip = function(el, text) {
            let tippyRef = el._tippy || tippy.one(el, { content: text, trigger: 'manual' });

            tippyRef.setContent(text);
            tippyRef.show();

            setTimeout(function() {
                tippyRef.hide();
            }, 1500);
        }
    ;
    n.on(document.body, 'click', function(event) {
        var copyLinkButton, editLinkButton, cancelEditButton, saveEditButton, deleteButton;

        if (copyLinkButton = n.closest(event.target, '.ShortLinkRow__copyLinkButton')) {
            var link_el = getShortLinkElement(copyLinkButton);

            if (n.copyTextToClipboard(link_el.getAttribute('href'))) {
                setLinkTooltip(link_el, 'Copied!');
            }
        }

        if (editLinkButton = n.closest(event.target, '.ShortLinkRow__editLinkButton')) {
            var col = n.closest(editLinkButton, '.ShortLinkRow__originalLink');
            var row = getRowFromElement(col);

            n.addClass(col, 'ShortLinkRow__originalLink--inEdit');

            var input_el = getEditInputElement(row);
            input_el.focus();
            input_el.select();
        }

        if (cancelEditButton = n.closest(event.target, '.ShortLinkRow__cancelEditButton')) {
            var col = n.closest(cancelEditButton, '.ShortLinkRow__originalLink');
            n.removeClass(col, 'ShortLinkRow__originalLink--inEdit');
            resetToRowValue(col);
        }

        if (saveEditButton = n.closest(event.target, '.ShortLinkRow__saveEditButton')) {
            var col = n.closest(saveEditButton, '.ShortLinkRow__originalLink');
            var row = getRowFromElement(col);
            cancelEditButton = col.querySelector('.ShortLinkRow__cancelEditButton');

            var short_id = row.getAttribute('data-short-id');
            var link_el  = getOriginalLinkElement(row);
            var input_el = getEditInputElement(row);
            var new_url  = n.value(input_el);

            r('PATCH', '/shortlink/{}'.format(short_id), {
                data: {
                    'original_url': new_url,
                },
                start: function() {
                    this.pending_icon = col.querySelector('.loading');
                    n.removeClass(this.pending_icon, 'hide');

                    n.disabled(saveEditButton, true);
                    n.disabled(cancelEditButton, true);
                    n.disabled(input_el, true);
                },
                success: function(result) {
                    if (result['success'] !== true) {
                        app.dialog('Failed to save original URL edit. Try again later.', app.DIALOG_ERROR);
                        return;
                    }

                    row.setAttribute('data-value', new_url);
                    setLinkTooltip(link_el, 'Saved!');
                },
                error: function() {
                    return false;
                },
                done: function() {
                    n.removeClass(col, 'ShortLinkRow__originalLink--inEdit');
                    resetToRowValue(col);

                    n.disabled(saveEditButton, false);
                    n.disabled(cancelEditButton, false);
                    n.disabled(input_el, false);

                    n.addClass(this.pending_icon, 'hide');
                },
            });
        }

        if (deleteButton = n.closest(event.target, '.ShortLinkRow__deleteButton')) {
            n.disabled(deleteButton, true);

            var row = getRowFromElement(deleteButton);
            var short_id = row.getAttribute('data-short-id');

            n.addClass(row, 'yellowbg');

            if (confirm('Are you sure you want to delete the highlighted short link?')) {
                r('DELETE', '/shortlink/{}'.format(short_id), {
                    start: function() {
                        this.icon = deleteButton.querySelector('.icon');
                        this.pending_icon = deleteButton.querySelector('.loading');
                        this.inner_span = deleteButton.querySelector('span');

                        n.addClass(this.inner_span, 'red');
                        n.text(this.inner_span, 'Deleting');

                        n.addClass(this.icon, 'hide');
                        n.removeClass(this.pending_icon, 'hide');
                    },
                    success: function(result) {
                        if (result['success'] !== true) {
                            app.dialog('Failed to delete the short link. Try again later.', app.DIALOG_ERROR);
                            return;
                        }

                        n.addClass(this.pending_icon, 'hide');
                        n.text(this.inner_span, 'Deleted');

                        n.removeClass(n.closest(deleteButton, '.dropdown'), 'dropdown--active');
                        n.removeClass(n.closest(deleteButton, '.dropdown--trigger'), 'dropdown--active');

                        setLinkTooltip(getShortLinkElement(row), 'Deleted!');

                        n.removeClass(row, 'yellowbg');
                        n.addClass(row, 'redbg');

                        row.setAttribute('style', 'transition: opacity 1s');

                        setTimeout(function() {
                            n.addClass(row, 'opacity10p');
                            setTimeout(function() {
                                n.remove(row);
                            }, 1000);
                        }, 1500);
                    },
                    error: function() {
                        n.removeClass(this.inner_span, 'red');
                        n.text(this.inner_span, 'Delete');

                        n.removeClass(this.icon, 'hide');
                        n.addClass(this.pending_icon, 'hide');

                        return false;
                    },
                    done: function() {
                        n.removeClass(row, 'yellowbg');
                    },
                });
            } else {
                n.removeClass(row, 'yellowbg');
                n.disabled(deleteButton, false);
            }
        }
    });
    n.on('.LinkShortenForm__originalURL', 'keyup', function(event) {
        n.addClass('.LinkShortenerFormError', 'hide');
    });
    n.on('.LinkShortenForm__submitButton', 'click', function(event) {
        var form_el   = n.closest(this, '.LinkShortenForm'),
            url_el    = form_el.querySelector('.LinkShortenForm__originalURL');

        n.addClass('.LinkShortenerFormError', 'hide');

        if (!n.value(url_el).length) {
            n.removeClass('.LinkShortenerFormError--emptyURL', 'hide');
            return;
        }

        if (!app.url.check_url(n.value(url_el), true)) {
            n.removeClass('.LinkShortenerFormError--invalidURL', 'hide');
            return;
        }

        r('POST', '/shortlink', {
            data: {
                'original_url': n.value(url_el),
                'prefer': 'html|flashitems',
            },
            start: function() {
                this.pending_icon = form_el.querySelector('.LinkShortenForm__pendingIcon');
                this.button_el    = form_el.querySelector('.LinkShortenForm__submitButton');

                n.removeClass(this.pending_icon, 'hide');
                n.disabled(this.button_el, true);
                n.disabled(url_el, true);
            },
            success: function(result) {
                n.value(url_el, '');
                n.remove('.NoShortenedLinks');

                document.querySelector('.ShortenedLinkTable__body').insertAdjacentHTML('afterbegin', result);

                setTimeout(function() {
                    n.removeClass(document.querySelectorAll('.ShortLinkRow.flash'), 'flash');
                }, 1000);
            },
            error: function() {
                return false;
            },
            done: function() {
                n.addClass(this.pending_icon, 'hide');
                n.disabled(this.button_el, false);
                n.disabled(url_el, false);
            },
        });
    });
});