init('SubredditSettings', function() {
    function switch_component(sr, uri) {
        history.replaceState({}, document.title = sr + " Settings", uri);

        // hide existing components
        n.addClass('.settings__comp', 'hide');

        // check if component-to-fetch already exists
        var already_exists = document.querySelector('.settings__comp[data-for="'+to_lower(sr)+'"]');
        if (already_exists) {
            n.removeClass(already_exists, 'hide');
            return;
        }

        // fetch component
        r('/r/{}/settings?action=get_sr_component'.format(sr = to_lower(sr)), {
            start: function() {
                this.comp = n('div.settings__comp', {'data-for': sr});
            },
            success: function(result) {
                n.html(this.comp, result);
            },
            error: function() {
                n.html(this.comp, n('div', {
                    'class': 'output-status',
                    'text': 'An error occurred',
                }));
            },
            done: function() {
                n.select('.settings__panel').append(this.comp);
            },
        });
    }
    function show_help_component() {
        n.addClass('.settings__comp', 'hide');

        n.removeClass(document.querySelector('.settings__comp.settings__compHelp'), 'hide');
    }

    n.on(document.body, 'click', function(event) {
        var element;
        (function subredditChangeListener() {
            element = n.closest(event.target, '.SubredditSettings__SubredditListItem');

            if (element && !n.hasClass(element, 'selected')) {
                n.removeClass('.settings__sideItem.selected', 'selected');
                n.addClass(element, 'selected');
                switch_component(element.getAttribute('data-subreddit'), element.getAttribute('data-uri'));
            }

            var helpItem = n.closest(event.target, '.settings__sideItemHelp');

            if (helpItem && !n.hasClass(helpItem, 'selected')) {
                n.removeClass('.settings__sideItem.selected', 'selected');
                n.addClass(helpItem, 'selected');
                show_help_component();
            }
        })();
        (function properyChangeListener() {
            if (event.target.getAttribute('type') != 'checkbox'
                    || !(element = n.closest(event.target, '.settingsProp__value'))
                    || n.hasAttribute(event.target, 'disabled') ) {
                return;
            }

            var prop    = event.target.getAttribute('data-property'),
                sr      = event.target.getAttribute('data-subreddit'),
                state   = event.target.checked;

            if (n.hasClass(element, 'is--InProgress')) {
                event.preventDefault();
                alert('Please wait...');
                return;
            }

            r('/r/{}/settings?action=set'.format(sr), {
                data: {
                    'property': prop,
                    'value': state ? 't' : 'f'
                },
                start: function() {
                    n.addClass(element, 'is--InProgress');
                    this.save_marker  = element.querySelector('.save-marker');
                    this.loading_icon = n('div.loading');
                    element.appendChild(this.loading_icon);
                },
                success: function(result) {
                    if (this.save_marker) {
                        n.addClass(this.save_marker, 'save-marker--flash');

                        var that = this;
                        setTimeout(function() {
                            n.removeClass(that.save_marker, 'save-marker--flash');
                        }, 2000);
                    }
                },
                error: function() {
                    event.target.checked = !state;
                    return false;
                },
                done: function() {
                    n.remove(this.loading_icon);
                    n.removeClass(element, 'is--InProgress');
                },
            });
        })();
    });

    // ----- subreddit list filter

    n.on('#SubredditSettings__filter', 'change keyup paste', function(event) {
        var value = this.value.toLowerCase();
        n.select('.SubredditSettings__SubredditListItem').forEach(function(el) {
            if (!value.length || contains(el.getAttribute('data-lcsubreddit'), value)) {
                n.removeClass(el, 'hide');
            } else {
                n.addClass(el, 'hide');
            }
        });
    });

    // ----- static load : set respective subreddit list item to selected state

    var static_loaded_comp = document.querySelector('.settings__comp:not(.settings__compHelp)'),
        static_loaded_item = null;

    if (!static_loaded_comp) {
        static_loaded_item = document.querySelector('.SubredditSettings__SubredditListItem');
    } else if (static_loaded_comp.hasAttribute('data-for')) {
        var item_id = to_lower(static_loaded_comp.getAttribute('data-for'));

        static_loaded_item = document.querySelector(
            '.SubredditSettings__SubredditListItem[data-lcsubreddit="'+item_id+'"]');
    }

    if (static_loaded_item)
        static_loaded_item.click();
});