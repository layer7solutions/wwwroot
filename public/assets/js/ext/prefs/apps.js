

init('PrefsApps', function() {
    (function() {
        var hash = '';
        if (window.location.hash) {
            hash = window.location.hash;
        }
        window.history.replaceState({} , 'Applications', SITE_URL+'prefs/apps'+hash);
    })();

    (function() {
        var did_submit = false;
        function oauth2apps_check_submit() {
            if (did_submit) {
                return false;
            }
            did_submit = true;
            return true;
        }
        n.on('form', 'submit', function(event) {
            return oauth2apps_check_submit();
        });
    })();

    n.on('.are-you-sure__no', 'click', function(event) {
        event.preventDefault();
        n.addClass(n.closest(this, '.are-you-sure'), 'hide');
    });

    n.on('.oapp__RevokeAccess', 'click', function(event) {
        event.preventDefault();
        n.removeClass(n.closest(this,'div').querySelector('.are-you-sure'), 'hide');
    });

    n.on('#toggle_create_app_form', 'click', function(event) {
        n.removeClass('#create_app_form', 'hide');
        n.addClass('#toggle_create_app_form', 'hide');
    });

    n.on('#app_create_cancel_btn', 'click', function(event) {
        event.preventDefault();
        n.addClass('#create_app_form', 'hide');
        n.removeClass('#toggle_create_app_form', 'hide');
    });

    n.on('.oapp__ModifyBtnGroup__editButton', 'click', function(event) {
        event.preventDefault();
        n.toggleClass(n.closest(this, '.oauth2apps_formbox').querySelector('.more'), 'hide');
        if (n.hasClass(this, 'active')) {
            n.removeClass(this, 'active');
            n.removeClass(n.closest(this, '.oauth2apps_formbox'), 'highlighted');
            this.innerHTML = 'edit';
        } else {
            n.addClass(this, 'active');
            this.innerHTML = 'cancel edit';
        }
    });

    n.on('.oapp__ModifyBtnGroup__deleteButton', 'click', function(event) {
        event.preventDefault();
        n.removeClass(n.closest(this,'div').querySelector('.are-you-sure'), 'hide');
    });
});