init('PrefsCommon', function(opts) {
    function onHashChange() {
        if (!window.location.hash)
            return;
        var hashElement = document.querySelector(window.location.hash);
        if (hashElement) {
            setTimeout(function() {
                n.addClass(hashElement, 'flash');
            }, 250);
        }
    }

    window.addEventListener("hashchange", onHashChange, false);
    onHashChange();

    n.on('.UserModdedReSyncForm .UserModdedReSyncButton', 'click', function(event) {
        var button = this;
        var parent = n.closest(button, '.UserModdedReSyncForm');

        if (n.disabled(button))
            return;

        r('POST', '/identity/resync_modded_subs', {
            start: function() {
                this.remove_statuses = function() {
                    n.remove(parent.querySelectorAll('.UserModdedReSyncStatus'));
                };

                this.failed = function(error_text) {
                    // All statuses are removed on `start` handler, if there is a status then that
                    // means another handler got to the result first, let it take precedence
                    if (parent.querySelector('.UserModdedReSyncStatus'))
                        return;

                    n.select(parent).append(n('.UserModdedReSyncStatus.is--error', [
                        n('div', n('i.icon.zmdi.zmdi-alert-octagon.red')),
                        n('span.red', {
                            text: error_text ||
                                'Something went wrong. Try again later or contact us ' +
                                'if the problem persists.'
                        }),
                    ]));
                    n.disabled(button, false);
                };

                this.remove_statuses();

                n.select(parent).append(n('.UserModdedReSyncStatus.is--pending', [
                    n('div', n('.icon.loading.small')),
                    n('span', {
                        html: 'Please wait, this might take some time depending on how many ' +
                        'subreddits you mod. <strong>Don\'t refresh or leave this page</strong>.'
                    }),
                ]));

                n.disabled(button, true);
            },
            receive: function() {
                this.remove_statuses();
            },
            success: function(res) {
                if (!res.success) {
                    this.failed();
                    return;
                }

                n.select(parent).append(
                    n('.UserModdedReSyncStatus.is--success', [
                        n('div', n('i.icon.zmdi.zmdi-check-circle.green')),
                        n('span', {
                            text: 'Successfully completed. There is a 5 minute cooldown before you '
                                + 'can run this operation again.'
                        }),
                    ])
                );
            },
            error: function(error, error_msg) {
                this.failed(error_msg);
            },
        });
    });
    n.on('.UserModdedManualAddForm .UserModdedManualAddInput', 'focus', function(event) {
        n.remove(document.querySelectorAll('.UserModdedManualAddForm .UserModdedReSyncStatus'));
    });
    n.on('.UserModdedManualAddForm .UserModdedManualAddInput', 'enter', function(event) {
        document.querySelector('.UserModdedManualAddForm .UserModdedManualAddButton').click();
    });
    n.on('.UserModdedManualAddForm .UserModdedManualAddButton', 'click', function(event) {
        var button = this;
        var parent = n.closest(button, '.UserModdedManualAddForm');
        var input  = parent.querySelector('.UserModdedManualAddInput');

        if (n.disabled(button))
            return;

        r('POST', '/identity/resync_specific_sub', {
            data: {
                'subreddit': n.value(input),
            },
            start: function() {
                this.remove_statuses = function() {
                    n.remove(parent.querySelectorAll('.UserModdedReSyncStatus'));
                };

                this.failed = function(error_text) {
                    // All statuses are removed on `start` handler, if there is a status then that
                    // means another handler got to the result first, let it take precedence
                    if (parent.querySelector('.UserModdedReSyncStatus'))
                        return;

                    n.select(parent).append(
                        n('.UserModdedReSyncStatus.is--error', [
                            n('div', n('i.icon.zmdi.zmdi-alert-octagon.red')),
                            n('span.red', {
                                text: error_text ||
                                    "Error: either the subreddit doesn't exist or you don't mod it"
                            }),
                        ])
                    );
                };

                this.remove_statuses();

                n.select(parent).append(n('.UserModdedReSyncStatus.is--pending', [
                    n('.icon.loading.small'),
                ]));

                n.disabled(button, true);
                n.disabled(input, true);
            },
            receive: function() {
                this.remove_statuses();
            },
            success: function(res) {
                if (!res.success) {
                    this.failed();
                    return;
                }

                n.select(parent).append(
                    n('.UserModdedReSyncStatus.is--success', [
                        n('i.icon.zmdi.zmdi-check-circle.green'),
                        n('span.green', {
                            text: 'Done.',
                        }),
                    ])
                );

                n.value(input, '');

                setTimeout(function() {
                    this.remove_statuses();
                }.bind(this), 2000);
            },
            error: function(error, error_msg) {
                this.failed(error_msg);
            },
            done: function() {
                n.disabled(button, false);
                n.disabled(input, false);
            },
        });
    });
    n.on('input[data-for-radio]', 'click', function(event) {
        var radio_element = document.getElementById(this.getAttribute('data-for-radio'));

        if (!radio_element)
            return;

        radio_element.checked = true;
    });
    n.on('input[type=radio][data-for-input]', 'change', function(event) {
        var input_element = document.getElementById(this.getAttribute('data-for-input'));

        if (!input_element)
            return;

        if (this.checked) {
            input_element.focus();
        }
    });
    n.on('.UserPrefItem', 'change', function(event) {
        var element = this;

        var name = element.getAttribute('name');
        var label = n.closest(element, 'label');

        var new_value;

        if (element.getAttribute('type') === 'radio' || element.getAttribute('type') === 'checkbox') {
            if (element.checked) {
                new_value = element.getAttribute('data-value1');
            } else {
                new_value = element.getAttribute('data-value0');
            }
        } else {
            new_value = n.value(element);
        }

        if (!new_value.length) {
            return;
        }

        var real_value;

        try {
            // make sure value is valid JSON before sending to server
            real_value = JSON.parse(new_value);
        } catch (e) {
            console.log('bad value', new_value);
            return;
        }

        r('POST', '/identity/prefs', {
            data: {
                property: name,
                value: new_value,
            },
            start: function() {
                this.success_icon = n.gen('i.icon.zmdi.zmdi-check-circle.save-marker.save-marker--flash');
                this.pending_icon = n.gen('.icon.loading.xsmall');
                this.error_icon   = n.gen('.icon.zmdi.zmdi-alert-octagon.save-marker.save-marker--flash.red');

                this.failed = function() {
                    n.addClass(label, 'failed');
                    n.appendChild(this.error_icon, label);
                }.bind(this);

                n.addClass(label, 'in-progress');
                n.appendChild(this.pending_icon, label);
            },
            receive: function() {
                n.remove(this.pending_icon);
                n.removeClass(label, 'in-progress');
            },
            success: function(res) {
                if (!res.success) {
                    this.failed();
                    return;
                }
                n.addClass(label, 'saved');
                n.appendChild(this.success_icon, label);

                if (name === 'nightmode_enabled') {
                    app.tempSetNightMode(!!real_value);
                }
            },
            error: function() {
                this.failed();
            },
            status_error: function() {
                this.failed();
            },
            done: function() {
                setTimeout(function() {
                    n.remove([this.success_icon, this.pending_icon, this.error_icon]);
                    n.remove(n.find(label, '> .icon:last-child'));
                    n.removeClass(label, 'in-progress saved failed');
                }.bind(this), 2000);
            },
        });
    });
    n.on('.SessionList__deleteButton', 'click', function(event) {
        event.preventDefault();

        if (!confirm('Are you sure?')) {
            return;
        }

        var button = this;

        if (n.hasClass(button, 'SessionList__deleteButtonAll')) {
            r('/identity/force_logout', {
                start: function() {
                    this.pending_icon = button.parentElement.querySelector('.loading');

                    n.removeClass(this.pending_icon, 'hide');
                },
                success: function(result) {
                    app.dialog.close();

                    if (!result['success']) {
                        app.dialog('Failed to clear sessions.', app.DIALOG_ERROR);
                        return;
                    }

                    app.dialog(
                        'Succesfully cleared all sessions except the one you\'re ' +
                        'currently on. You can continue using it or logout.', app.DIALOG_TOAST);

                    var parent_rows = n.select('.SessionList__row:not([data-token="{}"])'.format(result['delete_token']));

                    parent_rows.addClass('SessionList__loggedOut');

                    var that = parent_rows;
                    setTimeout(function() {
                        that.remove();
                    }, 500);
                },
                error: function(error, error_msg) {
                    app.dialog.close();
                    app.dialog('Failed to clear sessions.', app.DIALOG_ERROR);
                },
                done: function() {
                    n.addClass(this.pending_icon, 'hide');
                },
            })
        }

        if (n.hasAttribute(button, 'data-token')) {
            r('/identity/force_logout/{}'.format(button.getAttribute('data-token')), {
                start: function() {
                    this.parent_row = n.closest(button, 'tr');
                    this.pending_icon = button.parentElement.querySelector('.loading');

                    n.removeClass(this.pending_icon, 'hide');
                },
                success: function(result) {
                    app.dialog.close();

                    if (!result['success']) {
                        app.dialog('Failed to log out of session.', app.DIALOG_ERROR);
                        return;
                    }

                    if (this.parent_row) {
                        n.addClass(this.parent_row, 'SessionList__loggedOut');

                        var that = this.parent_row;
                        setTimeout(function() {
                            n.remove(that);
                        }, 500);
                    }

                    app.dialog('Succesfully logged out of session.', app.DIALOG_TOAST);
                },
                error: function(error, error_msg) {
                    app.dialog.close();
                    app.dialog('Failed to log out of session.', app.DIALOG_ERROR);
                },
                done: function() {
                    n.addClass(this.pending_icon, 'hide');
                },
            })
        }
    });
});