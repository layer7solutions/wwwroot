init('Overview', function() {
    n.select('.WelcomeCard [data-target]').on('click', function(event) {
        var target = this.getAttribute('data-target');

        n.select('.WelcomePanel').removeClass('active');
        n.select('.WelcomePanel[data-name="'+target+'"]').addClass('active');
    });

    var today = new Date();
    var hour24 = today.getHours();
    var greetingEl = document.querySelector('.DashboardWelcome__greeting');

    if (5 <= hour24 && hour24 < 12) {
        n.text(greetingEl, 'Good morning');
    } else if (12 <= hour24 && hour24 < 17) {
        n.text(greetingEl, 'Good afternoon');
    } else if (17 <= hour24 && hour24 < 21) {
        n.text(greetingEl, 'Good evening');
    } else {
        n.text(greetingEl, 'Good evening');
    }
});