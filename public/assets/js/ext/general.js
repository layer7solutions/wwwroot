init('AppGeneral', function() {
  tippy.setDefaults({
    arrow: true,
    duration: 0,
    zIndex: 99999999,
    theme: 'layer7',
    placement: 'top',
    animation: 'fade',
    duration: [275, 250],
    distance: 10,
    flip: true,
    flipBehavior: 'flip',
  });

  n.on(document.body, 'click', function(event) {
    var parent = n.closest(event.target, '.dropdown');
    var trigger = n.closest(event.target, '.dropdown--trigger');

    if (!parent && !trigger) {
      // did not click trigger or dropdown
      n.removeClass('.dropdown--active', 'dropdown--active');
    } else if (trigger && !parent) {
      // clicked trigger but not dropdown
      n.removeClass('.dropdown--active', 'dropdown--active');

      // dropdown must either be a child of `.dropdown--trigger` or
      // the next element sibling

      var dd = trigger.querySelector('.dropdown');
      if (!dd && trigger.nextElementSibling && n.hasClass(trigger.nextElementSibling, 'dropdown')) {
        dd = trigger.nextElementSibling;
      }

      if (dd) {
        n.toggleClass([trigger, dd], 'dropdown--active');
      }
    } else if (parent && event.target.parentElement == parent) {
      // clicked item in dropdown
      n.removeClass(event.target.parentElement.children, 'selected');
      n.addClass(event.target, 'selected');

      var trigger_el = n.closest(event.target, '.dropdown--trigger'),
        label_el;
      if (trigger_el && (label_el = trigger_el.querySelector('.dropdown--label'))) {
        var old_value = label_el.getAttribute('value') || label_el.innerText;
        var new_value = event.target.getAttribute('value') || event.target.innerText;

        label_el.innerText = new_value;
        label_el.setAttribute('value', new_value);

        n.removeClass([trigger, parent], 'dropdown--active');

        var event = new CustomEvent('DropdownStateChanged', {
          detail: {
            value: new_value,
            old_value: old_value,
          },
        });
        parent.dispatchEvent(event);
      }
    }
  });

  n.on(document.body, 'click', event => {
    var parent = n.closest(event.target, '#app-side');
    var trigger = n.closest(event.target, '.mobi-trigger');

    if (!parent && !trigger) {
      n.removeClass('#app-side', 'mobi-open');
      n.removeClass('body', 'page--mobi_open');
    }
  });

  n.on('#site-nav .mobi-trigger', 'click', event => {
    n.toggleClass('#app-side', 'mobi-open');
    n.toggleClass('body', 'page--mobi_open');
  });
});

Vue.component('app-side', {
  template: `
  <aside id="app-side" class="side">
    <div class="side-mobi-header mobi-only mobi-trigger" @click="toggleMobileMenu">
      <a class="nav-logo-a" style="cursor:default">
        <div class="nav-logo">
          <slot name="logo"></slot>
        </div>
        <span>{{ siteName }}</span>
      </a>
    </div>
    <div class="content">
      <div class="list">
        <slot></slot>
      </div>
    </div>
  </div>
  `,
  props: ['siteName'],
  methods: {
    toggleMobileMenu() {
      n.toggleClass('#app-side', 'mobi-open');
      n.toggleClass('body', 'page--mobi_open');
    },
  },
});

Vue.component('app-side-group', {
  template: `
  <div :class="{'side-group': true, 'side-group-top': top, selected, 'is-open': open, 'is-closed': !open }">
    <div class="header" v-if="!!this.$slots['header']" @click="toggle">
      <slot name="header"></slot>
      <i :class="'icon zmdi zmdi-chevron-' + ( open ? 'up' : 'down' )"></i>
    </div>
    <div class="content" ref="contentBox" :style="{
        height: this.contentHeight && this.contentHeight + 'px',
        transition: 'height ' + this.toggleAnimTime + 'ms',
        overflow: 'hidden',
      }">
      <div class="list" ref="listBox" :style="{ height: this.originalHeight + 'px' }">
        <slot></slot>
      </div>
    </div>
  </div>`,
  props: {
    name: {
      type: String,
      required: true,
    },
    sel: {
      type: Boolean,
      default: false,
    },
    top: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      contentHeight: null,
      originalHeight: null,
      toggleAnimTime: 200,
      toggleAnimDelay: 50,
    };
  },
  computed: {
    open: {
      get() {
        return app.store.getters['appSideGroupState/get'](this.name);
      },
      set(value) {
        app.store.commit('appSideGroupState/put', {
          key: this.name,
          value,
        });
      },
    },
    selected() {
      return (
        this.sel && this.sel.split(',').some(s => document.body.classList.contains('page--' + s))
      );
    },
  },
  created() {
    if (!this.open) {
      this.contentHeight = 0;
    }
  },
  methods: {
    toggle() {
      this.open = !this.open;
      console.log(this.name, this.open);

      if (!this.open) {
        // To be closed
        this.originalHeight = this.$refs.listBox.offsetHeight;
        this.contentHeight = this.originalHeight;

        setTimeout(() => {
          this.contentHeight = 0;
        }, this.toggleAnimDelay);
      } else {
        // To be opened
        if (!this.originalHeight) {
          this.originalHeight = this.$refs.listBox.offsetHeight;
        }

        this.contentHeight = this.originalHeight;
        setTimeout(() => {
          this.contentHeight = null;
          this.originalHeight = null;
        }, this.toggleAnimTime);
      }
    },
  },
});

Vue.component('app-side-item', {
  template: `<a :class="{ 'side-item': true, selected }"><slot></slot></a>`,
  props: {
    sel: {
      type: Boolean,
      default: false,
    },
  },
  computed: {
    selected() {
      return (
        this.sel && this.sel.split(',').some(s => document.body.classList.contains('page--' + s))
      );
    },
  },
});
