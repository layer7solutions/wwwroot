init('LogsView', function(opts) {
    if (is_mobile()) {
        app.forms.logsview.init(null);
        n.html('#LogsView__Header > h2', 'View your Logs');
        n.addClass(document.body, 'asi-unsupported');
        return;
    }
    n.on('#LogView__Settings', 'click', function() {
        app.dialog(n([
            n('h3', 'Logs Search Settings'),
            n('#LogView__SettingsForm.localsettings', [
                n('fieldset', [
                    n('span', 'Show the post/comment preview:'),
                    n('label', [
                        n('input[type=radio][name=ThingPreview][value=MouseOverIcon]'),
                        n('span', 'Upon mouse-over of the preview icon.')
                    ]),
                    n('label', [
                        n('input[type=radio][name=ThingPreview][value=InListing]'),
                        n('span', 'In the listing.')
                    ]),
                    n('label', [
                        n('input[type=radio][name=ThingPreview][value=Never]'),
                        n('span', 'Never.')
                    ]),
                ]),
                n('p', 'Changes save automatically. Changes do not affect the current result, you\'ll need to either refresh or perform a new search.')
            ])
        ]), app.DIALOG_MODAL);

        awaitElement('#LogView__SettingsForm', function(formElement) {
            app.forms.logsview.settings.attach(formElement);
        });
    });

    function update_mod_suggestions(instance) {
        var sr_constraint = instance.tokens.get('in');

        if (empty(sr_constraint)) {
            instance.options.tokens['mod'].suggestions = [CURRENT_USER];
            return;
        }

        if (app.util.moderators.cache.logged.containsAll(sr_constraint)) {
            app.util.moderators.get_logged(sr_constraint, function(modList) {
                instance.options.tokens['mod'].suggestions = flatten(Object.values(modList));
            });
            return;
        }

        instance.options.tokens['mod'].suggestions = new Promise(function(resolve, reject) {
            app.util.moderators.get_logged(sr_constraint, function(result) {
                if (result === false) {
                    resolve([]);
                }
                resolve( flatten(Object.values(result)) );
            });
        }).then(function(modList) {
            instance.options.tokens['mod'].suggestions = modList;
        });
    }

    app.forms.logsview.init(n.asi('#LogsView__Search').init({
        placeholder: 'filter results',
        active_placeholder: 'press enter to search',
        disable_error_completion: true,
        events: {
            complete: function(event) {
                app.forms.logsview.on_complete(this);
            },
            enter: function(event) {
            },
            validate: function(event) {

            },
            textchange: function(event) {
                app.forms.logsview.on_typing(this);
            },
            tokenchange: function(event) {
                if (event.type == 'add_value' && event.subject.label == 'in') {
                    update_mod_suggestions.call(null, this);
                } else if (event.type == 'delete_token' && event.subject.label == 'in') {
                    update_mod_suggestions.call(null, this);
                } else if (event.type == 'set_from_parse') {
                    update_mod_suggestions.call(null, this);
                }

                var instance = this;

                if (event.type == 'create_token' && event.subject.label == 'mod') {
                    if (empty(app.forms.logsview.asi_instance.tokens.get('in'))) {
                        setTimeout(function() {
                            if (instance.tokens.active.label != 'mod') {
                                return;
                            }
                            instance.helper.add_text('Select some subreddit(s) using the <code>in</code> '+
                            'option for more suggestions.')
                        },5);
                    }
                }
            }
        },
        multiple_values: false,
        tokens: {
            'in': {
                type: 'text',
                placeholder: 'subreddit',
                suggestions: opts.subreddits,
                validate: {
                    limit_to_suggestions: 'must be a subreddit you mod',
                },
                multiple_values: true,
            },
            'after': {
                type: 'datetime',
                placeholder: 'date/time',
                use_type_placeholder: true,
                allow_duplicates: false,
            },
            'before': {
                type: 'datetime',
                placeholder: 'date/time',
                use_type_placeholder: true,
                allow_duplicates: false,
            },
            'during': {
                type: 'date',
                placeholder: 'date',
                use_type_placeholder: true,
                allow_duplicates: false,
            },
            'mod': {
                type: 'text',
                placeholder: 'moderator',
                search_label: 'username',
                suggestions: [CURRENT_USER],
                desc_html: n.gen(
                'span.asi-helptitle["username - allowed formats"] + \
                  div.asi-desc > \
                    ul > \
                      (li > \
                        span["Reddit link to the user\'s profile page"] \
                      ) + \
                      (li > \
                        code["/u/username"] + span[" or "] + code["u/username"] \
                      ) + \
                      (li > \
                        code["username"] \
                      )'),
                validate: {
                    match: app.util.sanitize.reddit.regex.username,
                    error: 'invalid username',
                },
                valuepass: app.util.sanitize.reddit.pass.username,
                multiple_values: true,
            },
            'action': {
                type: 'text',
                placeholder: 'mod action',
                suggestions: opts.action_types,
                validate: {
                    limit_to_suggestions: true,
                },
                multiple_values: true,
                allow_strings: false,
            },
            'reason': {
                type: 'text',
                placeholder: 'mod reason',
                modifiers: {
                    'includes-word': {
                    },
                    'includes': {
                    },
                    'starts-with': {
                    },
                    'ends-with': {
                    },
                    'full-exact': {
                    },
                    'full-text': {
                    },
                    'regex': {
                        search_label: 'regex',
                        validate: {
                            check: validate_regex,
                            error: 'invalid regex',
                        }
                    },
                    'shorter-than': {
                        type: 'number:whole',
                        search_label: '# of characters',
                        placeholder: 'number',
                        desc_html: '<b>Shorter than</b> this number of characters.',
                        disable_space_completion: false,
                    },
                    'longer-than': {
                        type: 'number:whole',
                        search_label: '# of characters',
                        placeholder: 'number',
                        desc_html: '<b>Longer than</b> this number of characters.',
                        disable_space_completion: false,
                    }
                },
                desc_html: '<code>includes</code> is the default behavior if no modifier is set.<br/> \
                            These modifiers have the same functionality as AutoModerator.',
                disable_space_completion: true,
                multiple_values: false,
            },
            'author': {
                type: 'text',
                placeholder: 'post/comment author',
                search_label: 'username',
                modifiers: {
                    'includes': {},
                    'starts-with': {},
                    'ends-with': {},
                    'exact': {
                        desc_html: 'Perform a case-sensitive author search, whereas without any modifiers is case-insensitive.',
                    },
                    'regex': {
                        search_label: 'regex',
                        desc_html: 'Search for usernames matching a specific regex.',
                        validate: {
                            check: validate_regex,
                            error: 'invalid regex',
                        }
                    },
                    'shorter-than': {
                        type: 'number:whole',
                        search_label: '# of characters',
                        placeholder: 'number',
                        desc_html: 'Search for usernames <b>shorter than</b> this number of characters.',
                        disable_space_completion: false,
                    },
                    'longer-than': {
                        type: 'number:whole',
                        search_label: '# of characters',
                        placeholder: 'number',
                        desc_html: 'Search for usernames <b>longer than</b> this number of characters.',
                        disable_space_completion: false,
                    }
                },
                desc_html: n.gen(
                'span.asi-helptitle["username - allowed formats"] + \
                  div.asi-desc > \
                    ul > \
                      (li > \
                        span["Reddit link to the user\'s profile page"] \
                      ) + \
                      (li > \
                        code["/u/username"] + span[" or "] + code["u/username"] \
                      ) + \
                      (li > \
                        code["username"] \
                      )'),
                validate: {
                    match: app.util.sanitize.reddit.regex.username,
                    error: 'invalid username',
                },
                valuepass: app.util.sanitize.reddit.pass.username,
                multiple_values: false,
            },
            'thingid': {
                type: 'text',
                placeholder: 'thing id or link',
                search_label: 'thing id or link',
                multiple_values: true,
                validate: {
                    match: app.util.sanitize.reddit.regex.thing,
                    error: 'invalid thing id or link',
                },
                valuepass: function(value) {
                    if (/^[A-Za-z0-9]$/i.test(value)) {
                        // literal thing id
                        return value;
                    } else if (startsWith(value, 't') && contains(value, '_')) {
                        // thing_t#_<...> and t#_<...>
                        var parts = value.split('_');
                        return parts[parts.length - 2] + '_' + parts[parts.length - 1];
                    } else if (contains(value, 'redd.it')) {
                        // redd.it/<...>
                        var parts = trim(value,'/').split('/');
                        return 't3_'+parts[parts.length - 1];
                    } else if (contains(value, 'comments/')) {
                        var parts = trim(trim(value,'/').split('comments/')[1].split('?')[0],'/').split('/');
                        if (parts.length == 1 || parts.length == 2) {
                            return 't3_'+parts[0];
                        } else {
                            return 't1_'+parts[2];
                        }
                    } else {
                        return value;
                    }
                },
            },
            'title': {
                type: 'text',
                placeholder: 'post title',
                search_label: 'post title',
                modifiers: {
                    'includes': {},
                    'starts-with': {
                        desc_html: "Search posts whose title <b>starts with</b> the given text",
                    },
                    'ends-with': {
                        desc_html: "Search posts whose title <b>ends with</b> the given text",
                    },
                    'exact': {
                        desc_html: 'Perform a case-sensitive title search, whereas without any modifiers is case-insensitive.',
                    },
                    'regex': {
                        search_label: 'regex',
                        desc_html: 'Search for titles matching a specific regex.',
                        validate: {
                            check: validate_regex,
                            error: 'invalid regex',
                        }
                    },
                    'shorter-than': {
                        type: 'number:whole',
                        search_label: '# of characters',
                        placeholder: 'number',
                        desc_html: 'Search for titles <b>shorter than</b> this number of characters.',
                        disable_space_completion: false,
                    },
                    'longer-than': {
                        type: 'number:whole',
                        search_label: '# of characters',
                        placeholder: 'number',
                        desc_html: 'Search for titles <b>longer than</b> this number of characters.',
                        disable_space_completion: false,
                    }
                },
                disable_space_completion: true,
                desc_html: "Search whose title <b>contains</b> the given text",
                multiple_values: false,
            },
            'body': {
                type: 'text',
                placeholder: 'post/comment body',
                search_label: 'body text',
                modifiers: {
                    'includes': {},
                    'starts-with': {
                        desc_html: "Search comments and posts whose body <b>starts with</b> the given text",
                    },
                    'ends-with': {
                        desc_html: "Search comments and posts whose body <b>ends with</b> the given text",
                    },
                    'exact': {
                        desc_html: 'Perform a case-sensitive body search, whereas without any modifiers is case-insensitive.',
                    },
                    'regex': {
                        search_label: 'regex',
                        desc_html: 'Search for bodies matching a specific regex.',
                        validate: {
                            check: validate_regex,
                            error: 'invalid regex',
                        }
                    },
                    'shorter-than': {
                        type: 'number:whole',
                        search_label: '# of characters',
                        placeholder: 'number',
                        desc_html: 'Search for bodies <b>shorter than</b> this number of characters.',
                        disable_space_completion: false,
                    },
                    'longer-than': {
                        type: 'number:whole',
                        search_label: '# of characters',
                        placeholder: 'number',
                        desc_html: 'Search for bodies <b>longer than</b> this number of characters.',
                        disable_space_completion: false,
                    }
                },
                disable_space_completion: true,
                desc_html: "Search comments and posts whose body <b>contains</b> the given text",
                multiple_values: false,
            },
        }
    }));
});