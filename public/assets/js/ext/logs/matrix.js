app.matrix = {};

app.matrix.enable_filters = function(past_moderators) {
    app.forms.Filter('MatrixModFilter', {
        inputs: '#ModlogField__moderators .ChooserOption input',
        toggle_all: '#ModlogField__moderators .ChooserOption--all input',
        exclude_values:
            empty(past_moderators) ? null : ['current_mods_only'].concat(past_moderators),
        on_change: function(check_val, new_state) {
            if (check_val !== 'current_mods_only') return;

            var elements = document.querySelectorAll('#ModlogField__moderators .is--past_mod');

            if (new_state) {
                n.addClass(elements, 'hide');
                this.uncheck(elements);
                this.setExcluded(past_moderators, true);
            } else {
                n.removeClass(elements, 'hide');
                this.setExcluded(past_moderators, false);
                this.check(elements);
            }
        }
    });
    app.forms.Filter('MatrixActionFilter', {
        inputs: '#ModlogField__columnsFilter .ChooserOption input',
        toggle_all: '#ModlogField__columnsFilter .ChooserOption--all input'
    });
    app.forms.Filter('MatrixSubredditFilter', {
        inputs: '#ModlogField__subreddits .ChooserOption input',
        toggle_all: '#ModlogField__subreddits .ChooserOption--all input',
        on_change: function() {
            n.setAttribute('#ModlogButton__SelectNext', 'disabled', this.value_count == 0);
        }
    });
};

app.matrix.attach_listeners = function(subreddits) {
    n.on('#ModlogButton__SelectNext', 'click', function() {
        var filterObject = app.forms.Filter('MatrixSubredditFilter');
        var subredditString = app.util.getBestFilterString(filterObject, 'modlog');

        window.location.href = SITE_URL + 'logs/matrix/' + subredditString;
    });
    n.on('#ModlogForm', 'submit', function(event) {
        event.preventDefault();
        app.matrix.submit(subreddits);
    });
    n.on(document.body, 'click', function(event) {
        var element;

        if (n.hasAttribute(event.target, 'data-mod')) {
            element = event.target;
        } else if (n.hasClass(event.target, 'matrix-item')) {
            element = n.closest(event.target, '[data-mod]');
        }

        if (element && n.closest(element, '.matrix')) {
            var mod = element.getAttribute('data-mod');
            n.toggleClass('.matrix [data-mod="' + mod + '"]', 'matrix-highlight');
        }
    });
};

app.matrix.updateSort = function() {
    if (!document.getElementById('modlog-matrix'))
        return;
    return app.matrix.setSort( app.matrix.getSort(), true );
};

app.matrix.getSort = function() {
    var el = document.getElementById('modlog-matrix');
    if (!el)
        return 'no-sort';
    return el.getAttribute('data-current-sort');
};

app.matrix.setSort = function(sort_name, must_reprocess) {
    if (app.matrix.getSort() === sort_name && !must_reprocess) {
        return;
    } else if (document.getElementById('modlog-matrix')) {
        n.setAttribute('#modlog-matrix', 'data-current-sort', sort_name);
    }

    var options = {
        'asc': {
            icon_class: 'icon fa fa-sort-amount-asc',
            label_text: 'Ascending',
            order_attr: 'data-asc',
        },
        'desc': {
            icon_class: 'icon fa fa-sort-amount-desc',
            label_text: 'Descending',
            order_attr: 'data-desc',
        },
        'no-sort': {
            icon_class: 'icon fa fa-sort',
            label_text: 'No Sorting',
            order_attr: '',
        },
    };

    var icon_class = options[sort_name]['icon_class'];
    var label_text = options[sort_name]['label_text'];
    var order_attr = options[sort_name]['order_attr'];

    n.setAttribute('.matrix-toggleSort .icon', 'class', icon_class);
    n.text('.matrix-toggleSort span', label_text);

    forEach('.matrix [data-mod]', function(el) {
        el.setAttribute('style', order_attr && ('order:'+el.getAttribute(order_attr)) );
    });
};

app.matrix.toolbar = {};

app.matrix.toolbar.class_change_tool = function(opts) {
    var target  = document.querySelector(opts.target_selector);
    var icon_el = document.querySelector(opts.button_selector + ' .icon');

    if (!icon_el || !target) {
        return;
    }

    if (!opts.onlyFixIcon)
        n.toggleClass(target, opts.class_selector);

    var has_class = n.hasClass(target, opts.class_selector);
    var class_attr = has_class ? 'data-tstate' : 'data-fstate';
    icon_el.setAttribute('class', icon_el.getAttribute(class_attr));

    if (opts.callback)
        opts.callback(has_class);

    if (!opts.onlyFixIcon) {
        // revalidate immediately
        app.matrix_xscroll.revalidate();

        // wait for DOM/Style update
        if (opts.deltaToZero) {
            setTimeout(function() {
                app.matrix_xscroll.set_delta(0);
            }, 0);
        }
    }
};
app.matrix.toolbar.toggleEmptycolumns = function(_onlyFixIcon, _deltaToZero) {
    app.matrix.toolbar.class_change_tool({
        button_selector: '.matrix-toggleEmptyColumns',
        target_selector: '#modlog-matrix',
        class_selector:  'no-empty-columns',
        onlyFixIcon: _onlyFixIcon || false,
        deltaToZero: _deltaToZero || false,
    });
};
app.matrix.toolbar.toggleFullScreen = function(_onlyFixIcon, _deltaToZero) {
    app.matrix.toolbar.class_change_tool({
        button_selector: '.matrix-toggleFullScreen',
        target_selector: 'body',
        class_selector:  'page--logs_matrix--FullScreen',
        onlyFixIcon: _onlyFixIcon || false,
        deltaToZero: _deltaToZero || false,
        callback: function(has_class) {
            var header_el = document.getElementById('modlog-module-header');
            if (has_class) {
                n.appendChild(header_el, '#site-nav .fl');
            } else {
                n.prependChild(header_el, '#app-content');
            }
            n.removeClass(header_el, 'hide');
        },
    });
};

app.matrix.toolbar.enable = function() {

    // TOGGLE EMPTY COLUMNS
    // ~~~~~~~~~~~~~~~~~~~~

    n.on('.matrix-toggleEmptyColumns', 'click', function(event) {
        app.matrix.toolbar.toggleEmptycolumns(false, true);
    });
    app.matrix.toolbar.toggleEmptycolumns(true);

    // TOGGLE FULL SCREEN
    // ~~~~~~~~~~~~~~~~~~

    n.on('.matrix-toggleFullScreen', 'click', function(event) {
        app.matrix.toolbar.toggleFullScreen();
    });
    app.matrix.toolbar.toggleFullScreen(true);

    // SORT TOOL
    // ~~~~~~~~~

    n.on('.matrix-toggleSort', 'click', function(event) {
        switch (app.matrix.getSort()) {
            case 'no-sort':
                app.matrix.setSort('desc');
                break;
            case 'desc':
                app.matrix.setSort('asc');
                break;
            case 'asc':
                app.matrix.setSort('no-sort');
                break;
        }
    });

    // CSV EXPORT TOOL
    // ~~~~~~~~~~~~~~~

    // courtesy https://stackoverflow.com/a/24922761/2014481
    function doExportToCSV(filename, rows) {
        var processRow = function (row) {
            var finalVal = '';
            for (var j = 0; j < row.length; j++) {
                var innerValue = row[j] === null ? '' : row[j].toString();
                if (row[j] instanceof Date) {
                    innerValue = row[j].toLocaleString();
                }
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/[",\n]/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }
            return finalVal + '\n';
        };

        var csvFile = '';
        for (var i = 0; i < rows.length; i++) {
            csvFile += processRow(rows[i]);
        }

        var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }

    n.on('.matrix-exportToCSV', 'click', app.matrix.exportToCSV = function() {
        var parent = document.getElementById('modlog-matrix');

        if (!parent)
            return;

        var current_sort = app.matrix.getSort(),
            sort_func = (current_sort == 'no-sort') ? null : function(a, b) {
                return parseInt(a.getAttribute('data-'+current_sort))
                    - parseInt(b.getAttribute('data-'+current_sort));
            },
            guardAgainstEmptyCols = function(el) {
                return n.hasClass(parent, 'no-empty-columns') &&
                        el.getAttribute('data-column-count') == 0 ? undefined : true;
            };

        var csv_output_data =
            [n.select(parent, '.matrix-header-col').map(function(item_element) {
                var labelEl = item_element.querySelector('.matrix-header-item-label');
                var label = labelEl
                    ? labelEl.getAttribute('data-name')
                    : item_element.getAttribute('title');

                return guardAgainstEmptyCols(item_element) && label;
            })]
            .concat(n.select(parent, '.matrix-row')
                .sort(sort_func)
                .map(function(row_element) {
                    var row_data = n.select(row_element, '.matrix-item').map(function(item_element) {
                        return guardAgainstEmptyCols(item_element) && item_element.innerText;
                    });

                    row_data.unshift(row_element.getAttribute('data-mod'));
                    return row_data;
                }));

        var subreddits = parent.getAttribute('data-subreddits').replace(/,/g, '-');
        var lowerbound = document.querySelector('.matrix-lowerbound').innerText;
        var upperbound = document.querySelector('.matrix-upperbound').innerText;

        doExportToCSV('matrix_'+subreddits+'_'+lowerbound+'-'+upperbound+'.csv', csv_output_data);
    });

    // GET DIRECT LINK TOOL
    // ~~~~~~~~~~~~~~~~~~~~

    n.on('.matrix-getDirectLink', 'click', function() {
        var matrix = document.querySelector('#modlog-matrix');

        var query = [
            'from=' + matrix.getAttribute('data-lowerbound2'),
            'to=' + matrix.getAttribute('data-upperbound2'),
        ];

        if (matrix.getAttribute('data-moderators') != 'all')
            query.push('moderators=' +
                (matrix.getAttribute('data-moderators2') || matrix.getAttribute('data-moderators')));
        if (matrix.getAttribute('data-actions') != 'all')
            query.push('actions=' +
                (matrix.getAttribute('data-actions2') || matrix.getAttribute('data-actions')));
        if (n.hasClass(document.body, 'page--logs_matrix--FullScreen'))
            query.push('fullscreen=✓');
        if (n.hasClass(matrix, 'no-empty-columns'))
            query.push('noemptycolumns=✓');
        if (app.matrix.getSort() != 'no-sort')
            query.push('sort=' + app.matrix.getSort());

        var link = CURRENT_URL.split('?')[0] + '?' + query.join('&');

        app.dialog(n('div', [
            n('h4', 'Direct Link'),
            n('input', {
                'class': 'DialogSelectAll w100p',
                'style': "line-height: 26px;font-family: 'Open Sans', Arial, sans-serif;margin-bottom:10px",
                'type': 'text',
                'readonly': 'readonly',
                'value': link,
            })
        ]), app.DIALOG_ALERT);
    });
};

app.matrix.enable_datePickers = function() {
    if (typeof Pikaday !== 'undefined') {
        app.matrix.lower_bound = new Pikaday({
            field: document.getElementById('ModlogField__lowerbound'),
            maxDate: new Date(),
            defaultDate: new Date()
        });

        app.matrix.upper_bound = new Pikaday({
            field: document.getElementById('ModlogField__upperbound'),
            maxDate: new Date(),
            defaultDate: new Date()
        });
    }

    n.on('#ModlogField__lowerboundOptionLabel--AllTime', 'click', function(event) {
        if (this.querySelector('input[type=checkbox]').checked) {
            n.setAttribute('#ModlogField__lowerbound', 'disabled');
        } else {
            n.removeAttribute('#ModlogField__lowerbound', 'disabled');
        }
    });

    n.on('#ModlogField__upperboundOptionLabel--DateNow', 'click', function(event) {
        if (this.querySelector('input[type=checkbox]').checked) {
            n.setAttribute('#ModlogField__upperbound', 'disabled');
        } else {
            n.removeAttribute('#ModlogField__upperbound', 'disabled');
        }
    });
};

app.matrix.settings = app.localSettings('LogsMatrix', {
    'DefaultSort': 'no-sort',
    'HideEmptyCols': false,
});

app.matrix.settings_init = function() {
    n.on('#modlog-form-settings', 'click', function() {
        app.dialog(n([
            n('h3', 'Matrix Settings'),
            n('#modlog-form-settings-form.localsettings', [
                n('fieldset', [
                    n('span', 'Default sort of matrix results:'),
                    n('label', [
                        n('input[type=radio][name=DefaultSort][value=no-sort]'),
                        n('span', 'No sorting, i.e. in mod list order (default)')
                    ]),
                    n('label', [
                        n('input[type=radio][name=DefaultSort][value=desc]'),
                        n('span', 'Descending by percentage')
                    ]),
                    n('label', [
                        n('input[type=radio][name=DefaultSort][value=asc]'),
                        n('span', 'Ascending by percentage')
                    ]),
                ]),
                n('fieldset', [
                    n('label', [
                        n('input[type=checkbox][name=HideEmptyCols]'),
                        n('span', 'Hide empty columns by default')
                    ]),
                ]),
                n('p', 'Changes save automatically. Changes do not affect the current result, '+
                       'you\'d need to regenerate the matrix. Note that direct matrix links may ' +
                       'take priority over these settings,'),
            ])
        ]), app.DIALOG_MODAL);

        awaitElement('#modlog-form-settings-form', function(formElement) {
            app.matrix.settings.attach(formElement);
        });
    });
};

app.matrix.submit = function(subreddits) {
    var
        // filter objects
        modFilterObj = app.forms.Filter('MatrixModFilter'),
        actionFilterObj = app.forms.Filter('MatrixActionFilter'),
        // lower bound options
        lowerboundOption = document.getElementById('ModlogField__lowerbound'),
        allTimeOption = document.getElementById('ModlogField__lowerboundOptionValue--AllTime'),
        earliestOption = document.getElementById('ModLogField__lowerboundOptionValue--EarliestUTC'),
        // upper bound options
        upperboundOption = document.getElementById('ModlogField__upperbound'),
        nowOption = document.getElementById('ModlogField__upperboundOptionValue--DateNow');

    r('/r/{}/logs/matrix'.format(subreddits.join(',')), {
        data: {
            'moderators':
                app.util.getBestFilterString(modFilterObj,
                    modFilterObj.isChecked('current_mods_only') ? false : 'logged'),
            'actions':
                app.util.getBestFilterString(actionFilterObj),
            'lowerbound':
                allTimeOption.checked
                    ? parseInt(earliestOption.value)
                    : lowerboundOption.value.length &&
                        app.matrix.lower_bound.getDate().getTime() / 1000 | 0,
            'upperbound':
                nowOption.checked
                    ? Date.now() / 1000 | 0
                    : upperboundOption.value.length &&
                        app.matrix.upper_bound.getDate().getTime() / 1000 | 0,
            'component':
                true,
        },
        start: function() {
            n.removeClass('.matrix-pending', 'hide');
            this.submit_button = document.getElementById('ModlogForm_SubmitButton');
            this.submit_button.disabled = true;
        },
        success: function(result) {
            n.html('#modlog-form-output', result);
            n.setAttribute('#modlog-matrix', 'data-moderators2', this.params.moderators);
            n.setAttribute('#modlog-matrix', 'data-actions2', this.params.actions);

            app.matrix_xscroll.init();

            // app.matrix.updateSort();
            app.matrix.setSort( app.matrix.settings.get('DefaultSort'), true );

            if (app.matrix.settings.get('HideEmptyCols')) {
                // empty columns is off by default, so toggling it now will turn it on
                app.matrix.toolbar.toggleEmptycolumns(false);
            }
        },
        error: function(error, error_msg) {
            app.matrix.setSort('no-sort');
            if (error == 'BAD_PARAMETER') {
                n.html('#modlog-form-output', n('p.error-notice', [
                    n('b',      'Error: '),
                    n('span',   'One or more fields are empty! Please fill them in.'),
                ]));
            } else if (error == 'BAD_DATE_RANGE') {
                n.html('#modlog-form-output', n('p.error-notice', [
                    n('b',      'Error: '),
                    n('span',   'The '),
                    n('b',      'From'),
                    n('span',   ' date must be before the '),
                    n('b',      'To'),
                    n('span',   ' date!'),
                ]));
            } else {
                n.html('#modlog-form-output', n('p.error-notice', [
                    n('b',      'Error: '),
                    n('span',   error),
                    n('span',   ' - '),
                    n('span',   error_msg),
                ]));
            }
        },
        done: function() {
            app.matrix_xscroll.init();
            app.matrix.toolbar.toggleEmptycolumns(true);
            app.matrix.toolbar.toggleFullScreen(true);
            n.removeClass('#modlog-form-output-wrapper', 'hide');
            this.submit_button.disabled = false;
            app.matrix_xscroll.revalidate();
            tippy('.matrix .matrix-header-item-label');
        }
    });
};

init('LogsMatrix', function(opts) {
    app.matrix.settings_init();

    // if static-loaded matrix (from direct link)
    if (opts.has_direct_matrix) {
        // apply preferred sort if the direct matrix doesn't include a sort parameter, or set the
        // sort to the direct matrix sort
        if (n.hasClass('#modlog-matrix', 'no-direct-sort')) {
            app.matrix.setSort( app.matrix.settings.get('DefaultSort'), true );
        } else {
            app.matrix.updateSort();
        }

        // initialize xscroll
        if (app.matrix_xscroll)
            app.matrix_xscroll.init();

        // If user's local settings prefer hiding empty columns, then hide the empty columns if
        // they're not already hidden
        if (app.matrix.settings.get('HideEmptyCols') && !n.hasClass('#modlog-matrix', 'no-empty-columns')) {
            app.matrix.toolbar.toggleEmptycolumns(false);
        }

        n.remove('#modlog-form-output-wrapper .pending-overlay');
        n.removeClass('#modlog-form-output-wrapper', 'hide');
        n.removeClass('#modlog-form-output', 'visHide');
    }

    app.matrix.enable_filters(opts.past_moderators);
    app.matrix.toolbar.enable();
    app.matrix.enable_datePickers();
    app.matrix.attach_listeners(opts.subreddits);
});