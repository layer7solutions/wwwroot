(function() {
    var cls__incomplete = 'LogsView__Result--incomplete',
        cls__complete   = 'LogsView__Result--complete',
        cls__noresult   = 'LogsView__Result--noresult',
        cls__error      = 'LogsView__Result--is_error';

    var result_el;

    app.forms.logsview = {
        // state variables
        asi_instance: null,
        current_query: null,
        current_offset: null,
        is_before: null,
        typing_timeout: null,
        settings: app.localSettings('LogsView', {
            'ThingPreview': 'MouseOverIcon',
        }),

        // asi event handler functions
        update_hash: function(optional_amount) {
            if (optional_amount === 25) {
                optional_amount = null;
            }

            var instance = app.forms.logsview;
            var new_query_string = '';

            if (!isset(instance.asi_instance)) {
                return;
            }

            var stringified = instance.asi_instance.raw.stringify();
            if (stringified.length) {
                new_query_string += 'q='+encodeURIComponent(stringified);
            }

            if (instance.current_offset !== null) {
                if (instance.is_before === true) {
                    if (new_query_string.length) new_query_string += '&';
                    new_query_string += 'before='+instance.current_offset;
                } else if (instance.is_before === false) {
                    if (new_query_string.length) new_query_string += '&';
                    new_query_string += 'after='+instance.current_offset;
                }
            }

            if (isset(optional_amount)) {
                if (new_query_string.length) new_query_string += '&';
                new_query_string += 'amount='+optional_amount;
            }

            history.replaceState(undefined, undefined, '#'+new_query_string);
        },
        on_typing: function(instance) {
            if (!n.hasClass(result_el, cls__incomplete)) {
                n.addClass(result_el.children, 'hide');
                n.removeClass(result_el, cls__complete);
                n.removeClass(result_el, cls__noresult);
                n.addClass(result_el, cls__incomplete);
            }

            if (app.forms.logsview.typing_timeout !== null) {
                clearTimeout(app.forms.logsview.typing_timeout);
            }

            app.forms.logsview.typing_timeout = setTimeout(app.forms.logsview.update_hash, 750);
        },
        get_query: function(asi) {
            var state = asi.get_state(),
                query = {text: state.text, tokens: []},
                has_error = false;

            forEach(state.tokens, function(token) {
                query.tokens.push({
                    label: token.label,
                    modifier: token.modifier,
                    type: token.type,
                    error: token.error,
                    values: token.values,
                });
                if (token.error !== false) {
                    has_error = true;
                    return false; // break
                }
            });

            if (has_error) {
                n.addClass(result_el, cls__error);
                return;
            } else {
                n.removeClass(result_el, cls__error);
            }

            query = JSON.stringify(query);
            return query;
        },
        on_complete: function(instance) {
            var query = app.forms.logsview.get_query(instance);
            if (query === false) {
                return;
            }
            app.forms.logsview.send(query);
        },
        // api request send functions
        send_before: function(query, before, is_preload) {
            app.forms.logsview.send(query, before, true, null, is_preload);
        },
        send_after: function(query, after, is_preload) {
            app.forms.logsview.send(query, after, false, null, is_preload);
        },
        send_again: function(ignore_exists, is_preload) {
            if (isset(app.forms.logsview.current_query)) {
                if (isset(app.forms.logsview.current_offset)) {
                    app.forms.logsview.send(
                        app.forms.logsview.current_query,
                        app.forms.logsview.current_offset,
                        app.forms.logsview.is_before,
                        ignore_exists
                    );
                } else {
                    app.forms.logsview.send(app.forms.logsview.current_query, null, null, ignore_exists, is_preload);
                }
            }
        },
        preloading: {
            list: {},
            prepare: function(query, offset_id, before_or_after) {
                before_or_after = before_or_after === null ? 'neither' : (before_or_after ? 'before' : 'after');
                query = (query == '{"text":"","tokens":[]}') ? '' : query;

                if (!app.forms.logsview.preloading.list[query]) {
                    app.forms.logsview.preloading.list[query] = {
                        'before': {},
                        'after': {},
                        'neither': {},
                    };
                }

                return before_or_after;
            },
            add: function(query, offset_id, before_or_after) {
                before_or_after = app.forms.logsview.preloading.prepare(query, offset_id, before_or_after);
                query = (query == '{"text":"","tokens":[]}') ? '' : query;

                app.forms.logsview.preloading.list[query][before_or_after][offset_id] = {
                    'state': true,
                    'switch_after': false,
                };
            },
            check: function(query, offset_id, before_or_after, switch_after) {
                before_or_after = app.forms.logsview.preloading.prepare(query, offset_id, before_or_after);
                query = (query == '{"text":"","tokens":[]}') ? '' : query;

                var obj = app.forms.logsview.preloading.list[query][before_or_after][offset_id];
                if (!obj) {
                    return false;
                }

                if (switch_after && obj.state) {
                    app.forms.logsview.preloading.list[query][before_or_after][offset_id]['switch_after'] = switch_after;
                }

                return obj.state;
            },
            done: function(query, offset_id, before_or_after, element) {
                before_or_after = app.forms.logsview.preloading.prepare(query, offset_id, before_or_after);
                query = (query == '{"text":"","tokens":[]}') ? '' : query;

                var obj = app.forms.logsview.preloading.list[query][before_or_after][offset_id];

                if (obj && obj.state === true) {
                    app.forms.logsview.preloading.list[query][before_or_after][offset_id]['state'] = false;
                    if (obj.switch_after) {
                        app.forms.logsview.preloading.list[query][before_or_after][offset_id]['switch_after'] = false;
                        if (element) {
                            app.forms.logsview.preloading.switch_to_page(element);
                        }
                    }
                }

            },
            switch_to_page: function(el, is_preload, ignore_exists) {
                var instance = app.forms.logsview;

                // mark as done loading
                app.forms.logsview.preloading.done(
                    el.getAttribute('data-query'), instance.current_offset, instance.is_before, el);

                // if this was a preload, then nothing to do
                if (is_preload === true) {
                    return true;
                }

                // if 'ignore_exists' is true, then we're ignoring the fact that
                // a page already exists for the current query. So remove the existing
                // page and return false to continue towards the AJAX request
                if (ignore_exists === true) {
                    n.remove(el);
                    return false;
                }

                // update hiddens
                n.removeClass(result_el, cls__incomplete);
                n.removeClass(result_el, cls__noresult);
                n.addClass(result_el, cls__complete);
                n.addClass(result_el.children, 'hide');
                n.removeClass(el, 'hide');

                // update the instance cursor variables
                if (el.hasAttribute('data-after')) {
                    instance.is_before = false;
                    instance.current_offset = parseInt(el.getAttribute('data-after'));
                } else if (el.hasAttribute('data-before')) {
                    instance.is_before = true;
                    instance.current_offset = parseInt(el.getAttribute('data-before'));
                } else {
                    instance.is_before = null;
                    instance.current_offset = null;
                }

                // update the hash
                var amount = parseInt(el.getAttribute('data-amount'));
                app.forms.logsview.update_hash(amount == 25 ? null : amount);

                // preload the next page
                app.forms.logsview.send_after(decodeURIComponent(el.getAttribute('data-query')), el.getAttribute('data-last-id'), true);

                return true;
            }
        },
        // before_or_after - true if before, false if after, null if no offset_id
        send: function(query, offset_id, before_or_after, ignore_exists, is_preload) {
            is_preload = is_preload || false;

            if (app.forms.logsview.preloading.check(query, offset_id, before_or_after, !is_preload)) {
                return;
            }

            // clear output area
            if (is_preload !== true) {
                n.removeClass(result_el, cls__incomplete);
                n.removeClass(result_el, cls__noresult);
                n.addClass(result_el, cls__complete);
                n.addClass(result_el.children, 'hide');
            }

            // get amount
            var amount = document.querySelector('#LogsView__PageAmountChooser .option-group-item.selected');
            if (amount && is_int(amount.innerHTML)) {
                amount = parseInt(amount.innerHTML);
            } else {
                amount = 25;
            }

            // ----- BEGIN page already exists check
            var tmp_query = query == '{"text":"","tokens":[]}' ? '' : query; // normalize empty query
            function exists_query(extra) {
                extra = extra || '';
                return result_el.querySelector(
                    '.LogResultsTable[data-query="'+encodeURIComponent(tmp_query)+'"][data-amount="'+amount+'"]' + extra);
            }

            if (exists_query()) {
                var target = null;

                function page_found(el) {
                    return app.forms.logsview.preloading.switch_to_page(el, is_preload, ignore_exists);
                }

                if (before_or_after === true) {
                    // BEFORE
                    if (target = exists_query('[data-before="'+offset_id+'"]')) {
                        if (page_found(target))
                            return false;
                    }

                    var ref = exists_query('[data-first-id="'+offset_id+'"]');
                    if (ref && ref.hasAttribute('data-after')) {
                        target = exists_query('[data-last-id="'+ref.getAttribute('data-after')+'"]');
                        if (target) {
                            if (page_found(target))
                                return false;
                        }
                    }
                } else if (before_or_after === false) {
                    // AFTER
                    if (target = exists_query('[data-after="'+offset_id+'"]')) {
                        if (page_found(target))
                            return false;
                    }

                    var ref = exists_query('[data-last-id="'+offset_id+'"]');
                    if (ref && ref.hasAttribute('data-before')) {
                        target = exists_query('[data-first-id="'+ref.getAttribute('data-before')+'"]');
                        if (target) {
                            if (page_found(target))
                                return false;
                        }
                    }
                } else {
                    // NEITHER
                    var ref = exists_query(':not([data-after]):not([data-before])');
                    if (ref) {
                        if (page_found(ref))
                            return false;
                    }
                }
            }
            // ----- END page already exists check

            // mark as preloading
            app.forms.logsview.preloading.add(query, offset_id, before_or_after);

            // prepare data
            var data = {
                'query': query,
                'amount': amount,
            };

            var logstmt = 'Query: ' + query;

            // check offsets
            if (isset(offset_id) && isset(before_or_after)) {
                if (before_or_after) {
                    // true - before
                    data['before'] = offset_id;
                    logstmt += ' before:' + offset_id;

                    if (is_preload !== true) {
                        app.forms.logsview.current_offset = offset_id;
                        app.forms.logsview.is_before = true;
                    }
                } else {
                    // false - after
                    data['after'] = offset_id;
                    logstmt += ' after:' + offset_id;

                    if (is_preload !== true) {
                        app.forms.logsview.current_offset = offset_id;
                        app.forms.logsview.is_before = false;
                    }
                }
            } else {
                if (is_preload !== true) {
                    app.forms.logsview.current_offset = null;
                    app.forms.logsview.is_before = null;
                }
            }

            console.log(logstmt);

            if (is_preload !== true) {
                app.forms.logsview.update_hash(amount == 25 ? null : amount);
            }

            // send
            r('/logs/search', {
                data: data,
                is_passive: is_preload,
                success: function(result) {
                    if (is_preload !== true) {
                        app.forms.logsview.current_query = query;
                    }
                    var el = app.forms.logsview.handle_result(result, data, is_preload);
                    app.forms.logsview.preloading.done(query, offset_id, before_or_after, el);
                },
                error: function(error, err_msg) {
                    if (is_preload === true) {
                        app.forms.logsview.preloading.done(query, offset_id, before_or_after, null);
                        return;
                    }

                    if (error == 'INVALID_QUERY') {
                        n.appendChild(n.simple_gen('div', {
                            'class': 'output-status',
                            'text': 'invalid search query',
                            'style': 'padding: 30px 0',
                        }), result_el);
                        n.removeClass(result_el, 'hide');
                    } else if (error == 'QUERY_ERROR') {
                        n.appendChild(n.simple_gen('div', {
                            'class': 'output-status',
                            'text': err_msg,
                            'style': 'padding: 30px 0',
                        }), result_el);
                        n.removeClass(result_el, 'hide');
                    }

                    app.forms.logsview.current_query = null;
                    console.log('Error: '+error+': ' +err_msg);
                },
            });
        },
        handle_result: function(result, send_data, is_preload) {
            var mdgen = Snudown.getParser();

            var rows = result['listing'];
            var info = result['datainfo'];
            var settings = app.forms.logsview.settings;

            var params = {
                rowfn: function(row, idx, len) {
                    var data = rows[idx];

                    function get_prop_el(name) {
                        return row.querySelector('.LogProperty--'+name);
                    }

                    // Set meta row attributes
                    row.setAttribute('data-rowidx', idx);
                    row.setAttribute('data-entryid', data.id);
                    row.setAttribute('data-modactionid', data.modactionid);


                    // TIMESTAMP
                    var timestamp = get_prop_el('timestamp');
                    timestamp.innerHTML = data.human_timing;
                    timestamp.setAttribute('data-timestamp', data.timestamp);
                    timestamp.setAttribute('title', timeConvert(data.timestamp));

                    // MODERATOR AND SUBREDDIT
                    if (data.mod.toLowerCase() == '[deleted]') {
                        get_prop_el('mod_sub').innerHTML =
                            '<a title="deleted account">'+data.mod+'</a><span>'+data.subreddit+'</span>';
                    } else if (data.mod.toLowerCase() == 'trust and safety') {
                        get_prop_el('mod_sub').innerHTML =
                            '<a title="Reddit\'s Official Trust & Safety Team">'+data.mod+'</a><span>'+data.subreddit+'</span>';
                    } else if (data.mod.toLowerCase() == 'anti-evil operations') {
                        get_prop_el('mod_sub').innerHTML =
                            '<a title="Reddit\'s official anti-spam/abuse team">'+data.mod+'</a><span>'+data.subreddit+'</span>';
                    } else {
                        get_prop_el('mod_sub').innerHTML =
                            '<a target="_blank" rel="noopener noreferrer" ' +
                            'href="https://www.reddit.com/u/'+data.mod+'">'+data.mod+'</a>' +
                            '<span>'+data.subreddit+'</span>';
                    }

                    // ACTION TYPE
                    get_prop_el('actiontype').setAttribute('title', data.action);
                    get_prop_el('actiontype').innerHTML = '<span class="modactions modactions-'+data.action+'"></span>';

                    // DESC UTILITIES
                    var author = first([data.subject_user, data.author], isset);
                    if (isset(author)) {
                        if (author.toLowerCase() == '[deleted]' || contains(author, ' ')) {
                            author = '<a>'+author+'</a>';
                        } else {
                            author = '<a target="_blank" rel="noopener noreferrer" href="https://www.reddit.com/u/'+author+'">'+author+'</a>';
                        }
                    }
                    var post_or_comment = empty(data.thing_title) ? 'a comment' : 'a post';
                    var reason = first([data.reason, data.description, 'no reason'], isset);
                    var permalink = isset(data.permalink) ? function(text) {
                        return '<a target="_blank" rel="noopener noreferrer" href="'+data.permalink+'">'+text+'</a>';
                    } : function(text) { return text; };

                    var standard_reason = '';
                    if (isset(reason)) {
                        // don't show default reasons
                        if (reason == 'flair_edit'
                            || reason == 'flair_add'
                            || reason == 'flair_delete_template'
                            || reason == 'remove'
                            || reason == 'confirm_ham'
                            || data.action == 'community_widgets'
                            || data.action == 'wikirevise'
                        ) {
                            // do nothing
                        } else if (reason == 'permission_moderator'
                            || reason == 'permission_moderator_invite'
                            || data.action == 'community_styling') {
                            standard_reason = '<br/><span style="color:rgba(0,0,0,0.5)">'+data.description+'</span>';
                        } else if (data.action == 'banuser') {
                            standard_reason = ' <span style="color:rgba(0,0,0,0.5)">'+reason+'</span>';
                            if (data.description) {
                                standard_reason += '<br/><span style="color:rgba(0,0,0,0.5)">'+data.description+'</span>';
                            }
                        } else {
                            standard_reason = ' <span style="color:rgba(0,0,0,0.5)">'+reason+'</span>';
                        }
                    }

                    // THING META
                    var thing_data = isset(data.thing_data) ? data.thing_data : null;
                    var thing_title = isset(data.thing_title) ? data.thing_title : null;
                    var thing_inListing = settings.get('ThingPreview') == 'InListing';

                    // DESC
                    n(get_prop_el('desc'), (function(action_type) {
                        switch (action_type) {
                            case 'banuser':
                                return 'banned ' + author;
                            case 'unbanuser':
                                return 'unbanned ' + author;
                            case 'removelink':
                                return 'removed ' + permalink('a post') + ' by ' + author;
                            case 'approvelink':
                                return 'approved ' + permalink('a post') + ' by ' + author;
                            case 'removecomment':
                                return 'removed ' + permalink('a comment') + ' by ' + author;
                            case 'approvecomment':
                                return 'approved ' + permalink('a comment') + ' by ' + author;
                            case 'spamlink':
                                return 'marked ' + permalink('a post') + ' by ' + author + ' as spam';
                            case 'spamcomment':
                                return 'marked ' + permalink('a comment') + ' by ' + author + ' as spam';

                            case 'addmoderator':
                                return 'added moderator: ' + author;
                            case 'invitemoderator':
                                return 'invited moderator: ' + author;
                            case 'uninvitemoderator':
                                return 'revoked moderator invitation to ' + author;
                            case 'acceptmoderatorinvite':
                                return 'accepted moderator invite';
                            case 'removemoderator':
                                return 'removed moderator: ' + author;
                            case 'setpermissions':
                                return 'changed '+author+'\'s moderator permissions';
                            case 'addcontributor':
                                return 'added contributor: ' + author;
                            case 'removecontributor':
                                return 'removed contributor: ' + author;

                            case 'editsettings':
                                return 'edited settings';
                            case 'editflair':
                                if (reason == 'flair_delete_template') {
                                    return 'deleted flair template';
                                } else if (thing_title == null) {
                                    if (reason == 'flair_add') {
                                        return 'added ' + author + '\'s flair';
                                    } else {
                                        return 'edited ' + author + '\'s flair';
                                    }
                                } else {
                                    if (reason == 'flair_add') {
                                        return 'added link flair on ' + permalink('a post');
                                    } else {
                                        return 'edited link flair on ' + permalink('a post');
                                    }
                                }
                            case 'distinguish':
                                return 'distinguished ' + permalink(post_or_comment);
                            case 'marknsfw':
                                return 'marked a ' + permalink('a post') + ' as NSFW';

                            case 'wikibanned':
                                return 'banned '+author+' from wiki revising';
                            case 'wikiunbanned':
                                return 'unbanned '+author+' from wiki revising';
                            case 'wikipagelisted':
                                return 'changed wiki page listing';
                            case 'wikicontributor':
                                return 'added wiki contributor: '+author;
                            case 'removewikicontributor':
                                return 'removed wiki contributor: '+author;
                            case 'wikirevise':
                                if (startsWith(reason.toLowerCase(), 'updated ')) {
                                    return reason;
                                } else if (startsWith(reason.toLowerCase(), 'page ')) {
                                    var parts = reason.split(' ');
                                    var page = parts.slice(1,-1).join(' ');
                                    if (page == 'usernotes') {
                                        var note_desc = trim(data.description, ' "\'');

                                        if (startsWith(note_desc, 'create')) {
                                            note_desc = '<span style="color:rgba(0,0,0,0.5);margin-left:5px">(created note for '+
                                                note_desc.split(/user(.+)?/, 2)[1].split('"')[0].trim()+')</span>';
                                        } else if (startsWith(note_desc, 'delete')) {
                                            note_desc = '<span style="color:rgba(0,0,0,0.5);margin-left:5px">(deleted note on '+
                                                note_desc.split(/user(.+)?/, 2)[1].split('"')[0].trim()+')</span>';
                                        } else {
                                            note_desc = '';
                                        }

                                        return 'updated toolbox usernotes'+note_desc;
                                    } else if (page == 'toolbox') {
                                        return 'updated toolbox settings';
                                    } else {
                                        return '<span>edited page: </span><span style="font-weight:600">' + page + '</span>';
                                    }
                                }
                                break; // unreachable
                            case 'wikipermlevel':
                                return 'set wiki permissions';

                            case 'ignorereports':
                                return 'ignored reports on ' + permalink(post_or_comment) + ' by ' + author;
                            case 'unignorereports':
                                return 'unignored reports on ' + permalink(post_or_comment) + ' by ' + author;
                            case 'setsuggestedsort':
                                return 'set a suggested sort on ' + permalink(post_or_comment) + ' by ' + author;
                            case 'sticky':
                                return 'stickied ' + permalink('a post') + ' by ' + author;
                            case 'unsticky':
                                return 'unstickied ' + permalink('a post') + ' by ' + author;
                            case 'lock':
                                return 'locked ' + permalink('a post') + ' by ' + author;
                            case 'unlock':
                                return 'unlocked ' + permalink('a post') + ' by ' + author;
                            case 'spoiler':
                                return 'marked as spoiler ' + permalink('a post') + ' by ' + author;
                            case 'unspoiler':
                                return 'unmarked as spoiler ' + permalink('a post') + ' by ' + author;
                            case 'setcontestmode':
                                return 'set contest mode on ' + permalink('a post') + ' by ' + author;
                            case 'unsetcontestmode':
                                return 'unset contest mode on ' + permalink('a post') + ' by ' + author;
                            case 'markoriginalcontent':
                                if (reason === 'remove') {
                                    return 'unmarked original content on ' + permalink('a post') +
                                        ' by ' + author;
                                } else {
                                    return 'marked original content on ' + permalink('a post') +
                                        ' by ' + author;
                                }
                            case 'muteuser':
                                return 'mute user: ' + author;
                            case 'unmuteuser':
                                return 'unmute user: ' + author;
                            case 'createrule':
                                return 'created rule';
                            case 'deleterule':
                                return 'deleted rule';
                            case 'editrule':
                                return 'edited rule';
                            case 'modmail_enrollment':
                                return 'enrolled the subreddit in new modmail';
                            case 'community_styling':
                                return 'styled community:';
                            case 'community_widgets':
                                switch (reason) {
                                    case 'added_widget':
                                        return 'added widget: ' + data.description;
                                    case 'edited_widget':
                                        return 'edited widget: ' + data.description;
                                    case 'removed_widget':
                                        return 'removed widget: ' + data.description;
                                    default:
                                        return reason + ": " + data.description;
                                }
                            default:
                                return 'unknown mod action type';
                        }
                        return 'n/a';
                    })(data.action) + standard_reason);

                    // THING META
                    if (thing_inListing) {
                        n.addClass(get_prop_el('thingmetashow'), 'LogProperty--thingmeta--InListing');
                    }

                    if (settings.get('ThingPreview') == 'Never'
                            || thing_data == null && thing_title == null) {
                        n.remove(get_prop_el('thingmetashow'));
                    } else {
                        if (thing_title == null) {
                            // comment

                            n.addClass(get_prop_el('thingmeta'), 'LogProperty--thingmeta--comment');

                            n.html(get_prop_el('thingmeta'), '\
                                <h4><span class="label">COMMENT</span><span>by</span>'+author+'</h4> \
                                <a target="_blank" rel="noopener noreferrer" href="'+data.permalink+'"> \
                                    <i class="icon zmdi zmdi-open-in-new"></i> \
                                </a> \
                                <div class="LogProperty LogProperty--thingmetabody">'+
                                    mdgen.render(thing_data)
                                +'</div> \
                            ');
                        } else {
                            // post

                            n.addClass(get_prop_el('thingmeta'), 'LogProperty--thingmeta--post');

                            var thing_flair = '';

                            if (isset(data.flair_text)) {
                                var thing_flair_class = '';
                                if (isset(data.flair_class) && data.flair_class.length) {
                                    thing_flair_class =
                                        ' LogProperty--thingmetaflair-'+data.flair_class
                                            .split(' ')
                                            .join(' LogProperty--thingmetaflair-');
                                }
                                thing_flair = '<span class="LogProperty--thingmetaflair'+thing_flair_class+'">'
                                    + data.flair_text
                                    + '</span>';
                            }

                            if (thing_data == null) {
                                n.html(get_prop_el('thingmeta'), '\
                                    <h4><span class="label">POST</span><span>by</span>'+author+'</h4> \
                                    <a target="_blank" rel="noopener noreferrer" href="'+data.permalink+'"> \
                                        <i class="icon zmdi zmdi-open-in-new"></i> \
                                    </a> \
                                    <div class="LogProperty LogProperty--thingmetatitle">'+
                                        '<span>'+thing_title+'</span>' + thing_flair +
                                    '</div> \
                                ');
                            } else {
                                n.html(get_prop_el('thingmeta'), '\
                                    <h4><span class="label">POST</span><span>by</span>'+author+'</h4> \
                                    <a target="_blank" rel="noopener noreferrer" href="'+data.permalink+'"> \
                                        <i class="icon zmdi zmdi-open-in-new"></i> \
                                    </a> \
                                    <div class="LogProperty LogProperty--thingmetatitle">'+
                                        '<span>'+thing_title+'</span>' + thing_flair +
                                    '</div> \
                                    <div class="LogProperty LogProperty--thingmetabody">'+
                                        mdgen.render(thing_data) +
                                    '</div> \
                                ');
                            }
                        }
                    }
                },
                rownum: rows.length,
            };

            var output;
            if (rows.length !== 0) {
                var nav_gen =
                    '(.LogTableNav > \
                        button.LogTableNav--First > \
                            i.icon.fa.fa-angle-double-left + \
                            span["First"] ^ \
                        button.LogTableNav--Prev > \
                            i.icon.fa.fa-angle-left + \
                            span["Prev"] ^ \
                        .grow + \
                        button.LogTableNav--Next > \
                            span["Next"] + \
                            i.icon.fa.fa-angle-right ^ \
                    )';
                output = n.gen(nav_gen +
                    ' + (.LogTable > \
                        (.LogRow.{rowfn} > \
                            .LogProperty.LogProperty--timestamp.human-timing + \
                            .LogProperty.LogProperty--mod_sub + \
                            .LogProperty.LogProperty--actiontype + \
                            (.LogProperty.LogProperty--desc > \
                                .LogProperty.LogProperty--thingmetashow > \
                                    i.icon.zmdi.zmdi-eye + \
                                    .LogProperty.LogProperty--thingmeta \
                            ) \
                        ) * {rownum} \
                    ) + ' + nav_gen, params);

                var firstPgBtn = output.querySelectorAll('.LogTableNav--First');

                var prev = output.querySelectorAll('.LogTableNav--Prev');
                n.setAttribute(prev, 'data-before', info['before']);

                var centerTop = output.querySelector('.LogTableNav .grow');

                var next = output.querySelectorAll('.LogTableNav--Next');
                n.setAttribute(next, 'data-after', info['after']);

                if (isset(info['before'])) {
                    // Not on first page

                    // prev page button
                    n.on(prev, 'click', function() {
                        app.forms.logsview.send_before(send_data['query'], info['before']);
                    });

                    // go to first page button
                    n.on(firstPgBtn, 'click', function() {
                        app.forms.logsview.send(send_data['query']);
                    });
                } else {
                    // On first page
                    n.remove(prev);
                    n.remove(firstPgBtn);

                    // Re-update hash to remove 'before'
                    app.forms.logsview.current_offset = null;
                    app.forms.logsview.is_before = null;
                    app.forms.logsview.update_hash(info['amount']);

                    // Add first-page refresh button
                    var refreshbtn = n.gen1('button.LogTableNavButton["Refresh (first page)"]');
                    n.on(refreshbtn, 'click', function() {
                        app.forms.logsview.send_again(true);
                    });
                    centerTop.appendChild(refreshbtn);
                }

                if (isset(info['after'])) {
                    // next page button
                    n.on(next, 'click', function() {
                        app.forms.logsview.send_after(send_data['query'], info['after']);
                    });

                    // if this isn't a preload itself, preload the next page
                    if (is_preload !== true) {
                        app.forms.logsview.send_after(send_data['query'], info['after'], true);
                    }
                } else {
                    n.remove(next);
                }
            } else {
                n.addClass(result_el, cls__noresult);
                output = n.simple_gen('div', {
                    'class': 'output-status',
                    'text': 'no results',
                });
            }

            var real_output = n.simple_gen('div', {
                'class': 'LogResultsTable',
                'data-query': encodeURIComponent(send_data['query'] == '{"text":"","tokens":[]}' ? '' : send_data['query']),
                'data-amount': send_data['amount'],
                'data-before': send_data['before'],
                'data-after': send_data['after'],
                'data-first-id': rows.length ? rows[0]['id'] : null,
                'data-last-id': rows.length ? rows[rows.length - 1]['id'] : null,
            });

            // if this was a preload, then hide the output
            if (is_preload) {
                n.addClass(real_output, 'hide');
            }

            real_output.appendChild(output);

            n.appendChild(real_output, result_el);
            n.removeClass(result_el, 'hide');

            return real_output;
        },
        init: function(asi) {
            app.forms.logsview.asi_instance = asi;
            result_el = document.getElementById('LogsView__Result');

            n.on('.option-group-item', 'click', function(event) {
                var all = this.parentElement.querySelectorAll('.option-group-item');
                n.removeClass(all, 'selected');
                n.addClass(this, 'selected');
                app.forms.logsview.send_again(true);
            });

            if(window.location.hash) {
                var hash = window.location.hash.substring(1);
                var querystr = '?'+hash;

                var query   = query_param('q', querystr);
                var before  = query_param('before', querystr);
                var after   = query_param('after', querystr);
                var amount  = query_param('amount', querystr);

                if (isset(amount) && is_int(amount)) {
                    amount = parseInt(amount);

                    var amount_opt_el = document.querySelector('#LogsView__PageAmountChooser .option-group-item[data-value="'+amount+'"]');

                    if (amount_opt_el) {
                        n.removeClass(amount_opt_el.parentElement.querySelectorAll('.option-group-item'), 'selected');
                        n.addClass(amount_opt_el, 'selected');
                    }
                }

                if (isset(query) || isset(before) || isset(after)) {
                    query = isset(query) ? query : '';

                    var offset_id = null;
                    var is_before = null;

                    if (isset(before) && is_int(before)) {
                        offset_id = parseInt(before);
                        is_before = true;
                    } else if (isset(after) && is_int(after)) {
                        offset_id = parseInt(after);
                        is_before = false;
                    } else {
                        offset_id = null;
                        is_before = null;
                    }

                    if (asi) {
                        asi.raw.unstringify(query);
                        query = app.forms.logsview.get_query(asi);
                    }

                    if (query === false) {
                        return;
                    }

                    if (isset(offset_id)) {
                        app.forms.logsview.send(query, offset_id, is_before);
                    } else {
                        app.forms.logsview.send(query);
                    }
                } else {
                    app.forms.logsview.send('');
                }
            } else {
                app.forms.logsview.send('');
            }

            app.util.human_timing_update_start();
        },
    };
})();