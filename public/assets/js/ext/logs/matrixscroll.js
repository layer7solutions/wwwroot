(function() {
    var cumulativeOffset = function(element) {
        var top = 0, left = 0;
        do {
            top += element.offsetTop  || 0;
            left += element.offsetLeft || 0;
            if (element.hasAttribute('data-offsetLeft')) {
                left += parseFloat(element.getAttribute('data-offsetLeft'));
            }

            element = element.offsetParent;
        } while(element);

        return {
            top: top,
            left: left
        };
    };

    var offsetLeft = function(element, new_value, callback) {
        callback = callback || null;

        if (isset(new_value)) {
            if (is_string(new_value)) {
                if (endsWith(new_value, 'px')) {
                    new_value = new_value.slice(0, -2);
                }
                new_value = parseFloat(new_value);
            }

            var raf = window.requestAnimationFrame       ||
                      window.webkitRequestAnimationFrame ||
                      window.msRequestAnimationFrame     ||
                      window.mozRequestAnimationFrame    || null;

            var updateFunc = function() {
                element.setAttribute('data-offsetLeft', new_value);
                element.setAttribute('style', 'transform:translateX('+new_value+'px)');
                if (callback) {
                    callback();
                }
            };

            if (raf) {
                raf(updateFunc);
            } else {
                updateFunc();
            }
        } else {
            if (!element.hasAttribute('data-offsetLeft')) {
                return 0;
            }
            return parseFloat(element.getAttribute('data-offsetLeft'));
        }
    };

    var self;

    app.matrix_xscroll = {
        did_init: false,

        body_hover: true,
        area_hover: false,
        track_down: false,
        track_hover: false,
        thumb_down: false,
        thumb_x0: 0,


        // INITIALIZERS
        // --------------------------------------------------------------------------------

        // initializes the matrix x-scroller
        // called upon when a matrix is built, not upon page load
        init: function() {
            if (!document.querySelector('#modlog-matrix .matrix-xscroll__track')) {
                return;
            }

            if (!app.matrix_xscroll.did_init) {
                app.matrix_xscroll.did_init = true;

                self = app.matrix_xscroll;

                var clearActiveState = function(e) {
                    var track  = document.querySelector('#modlog-matrix .matrix-xscroll__track'),
                        thumb  = document.querySelector('#modlog-matrix .matrix-xscroll__thumb');

                    if (app.matrix_xscroll.track_down)
                        e.preventDefault();

                    if (track) {
                        n.removeClass(track, 'active');

                        var tId = track.getAttribute('data-track-down-timeout');
                        if (tId) {
                            track.removeAttribute('data-track-down-timeout');
                            clearTimeout(tId);
                        }
                    }

                    if (thumb) {
                        n.removeClass(thumb, 'active');
                    }

                    app.matrix_xscroll.thumb_down = false;
                    setTimeout(function() {
                        // lift track_down state a little after thumb_down
                        app.matrix_xscroll.track_down = false;
                    }, 100);
                };

                // attach window/body level listeners on first init
                // we only want to initialize these once

                window.addEventListener('scroll', function(e) {
                    app.matrix_xscroll.fixVisibility();
                });
                window.addEventListener('resize', function(e) {
                    app.matrix_xscroll.revalidate();
                });
                n.on(document.body, 'mouseup touchend', clearActiveState);
                n.on(document.body, 'mousemove touchmove', function(e) {
                    app.matrix_xscroll.thumb_scroll(e);
                });
                n.on(document.body, 'wheel', function(e) {
                    app.matrix_xscroll.wheel_scroll(e);
                });
                n.on(document.body, 'mouseleave', function(e) {
                    //clearActiveState(e);
                    app.matrix_xscroll.track_hover = false;
                    app.matrix_xscroll.area_hover  = false;
                    app.matrix_xscroll.body_hover  = false;
                });
                n.on(document.body, 'mouseenter', function(e) {
                    app.matrix_xscroll.body_hover  = true;
                });
                window.addEventListener('mouseup', function(e) {
                    if (!app.matrix_xscroll.body_hover) {
                        // if mouseup outside window
                        clearActiveState(e);
                    }
                });
            }

            // Add listeners for user interactions with the scroll track and thumb
            // These need to be re-attached each time a matrix is built because the
            // elements from the previous matrix are removed from the DOM
            app.matrix_xscroll.attachScrollListeners();

            // Initial revalidation
            app.matrix_xscroll.revalidate();
        },
        attachScrollListeners: function() {
            var
                track  = document.querySelector('#modlog-matrix .matrix-xscroll__track'),
                thumb  = document.querySelector('#modlog-matrix .matrix-xscroll__thumb'),
                area   = document.querySelector('#modlog-matrix .matrix-table');

            n.on(track, 'mousedown touchstart', function(event) {
                event.preventDefault();

                n.addClass(track, 'active');

                var pageX = event.pageX;

                self.track_down = true;
                self.track_click(pageX, false);

                var tId = setTimeout(function() {
                    self.track_click(pageX, true);
                }, 400);
                track.setAttribute('data-track-down-timeout', tId);
            });

            n.on(track, 'mouseenter', function() {
                self.track_hover = true;
            });
            n.on(track, 'mouseleave', function() {
                self.track_hover = false;
            });

            n.on(area, 'mouseenter', function() {
                self.area_hover = true;
            });
            n.on(area, 'mouseleave', function() {
                self.area_hover = false;
            });

            n.on(thumb, 'mousedown touchstart', function(event) {
                event.preventDefault();
                self.thumb_down = true;
                self.thumb_x0 = event.pageX - cumulativeOffset(thumb).left;

                n.addClass(thumb, 'active');
            });

            if (is_mobile()) {
                var mc = new Hammer(area);
                mc.on("panleft panright swipeleft swiperight", self.pan_scroll);
            }
        },


        // SCROLL HELPER FUNCTIONS
        // --------------------------------------------------------------------------------

        // Sets the scroll thumb to the correct width based on the pane and area width
        fixThumbWidth: function() {
            var wW = document.querySelector('#modlog-matrix .matrix-table').getBoundingClientRect().width;
            var pW = document.querySelector('#modlog-matrix .matrix-datawrap').getBoundingClientRect().width;

            var barWidth;
            if (pW < wW) { // no overflow
                barWidth = 0;
            } else {
                barWidth = (wW / pW) * wW;
                if (barWidth < 30)
                    barWidth = 30; // 30 - min width
            }

            var thumb = document.querySelector('#modlog-matrix .matrix-xscroll__thumb');
            var prev  = thumb.getAttribute('style') || '';

            prev = prev.split(';')[0];

            if (prev.length)
                prev = prev + ';';

            thumb.setAttribute('style', prev + ('width:'+barWidth+'px'));
        },
        getThumbWidth: function() {
            var thumb = document.querySelector('#modlog-matrix .matrix-xscroll__thumb');
            return thumb.getBoundingClientRect().width;
        },

        // Returns the maximum possible x-offset of the scrollbar
        get_delta_max: function() {
            // noinspection UnnecessaryLocalVariableJS
            var
                track   = document.querySelector('#modlog-matrix .matrix-xscroll__track'),
                thumb   = document.querySelector('#modlog-matrix .matrix-xscroll__thumb'),
                x_max   = track.getBoundingClientRect().width - thumb.getBoundingClientRect().width;
            return x_max;
        },

        // Returns the expected scrollbar x-offset based on the x-offset of the pane
        get_delta: function() {
            var pane  = document.querySelector('#modlog-matrix .matrix-datawrap'),
                area  = document.querySelector('#modlog-matrix .matrix-table');

            var p_delta = Math.abs(offsetLeft(pane));
            var percentScroll = p_delta / (pane.getBoundingClientRect().width - area.getBoundingClientRect().width);
            // noinspection UnnecessaryLocalVariableJS
            var delta = percentScroll * self.get_delta_max();

            return delta;
        },
        set_delta: function(delta) {
            var x_max = this.get_delta_max(),
                thumb = document.querySelector('#modlog-matrix .matrix-xscroll__thumb');
            if (!thumb)
                return;
            if (delta === false)
                delta = 0;
            if (delta === true)
                delta = x_max;
            if (delta < 0)
                delta = 0;
            if (delta >= x_max)
                delta = x_max;
            offsetLeft(thumb, delta);
            self.scroll_content(delta, x_max);
        },

        // scroll a given amount of "steps"
        //   negative : scroll left
        //   positive : scroll right
        step_scroll: function(steps) {
            var
                track   = document.querySelector('#modlog-matrix .matrix-xscroll__track'),
                thumb   = document.querySelector('#modlog-matrix .matrix-xscroll__thumb'),
                x_max   = track.getBoundingClientRect().width - thumb.getBoundingClientRect().width,
                current_delta   = offsetLeft(thumb),
                delta           = current_delta + (steps * 30); // 30 -> wheel step

            if (!thumb.getBoundingClientRect().width)
                return true;

            if (delta < 0)
                delta = 0;
            if (delta >= x_max)
                delta = x_max;

            // The `delta` is calculated above for both functions, so scroll_content doesn't need to
            // be in the offsetLeft callback.
            offsetLeft(thumb, delta);
            self.scroll_content(delta, x_max);

            return true;
        },

        // sets the x-offset of the scroll pane based on the x-offset of the scroll thumb
        scroll_content: function(delta, x_max) {
            if (x_max == null)
                x_max = self.get_delta_max();

            var
                percentScroll = delta / x_max,
                pane  = document.querySelector('#modlog-matrix .matrix-datawrap'),
                area  = document.querySelector('#modlog-matrix .matrix-table');

            var p_delta = percentScroll * (pane.getBoundingClientRect().width - area.getBoundingClientRect().width);
            offsetLeft(pane, -p_delta, self.fixThumbWidth);

            //self.fixThumbWidth();
        },

        // SCROLL LISTENER FUNCTIONS
        // --------------------------------------------------------------------------------
        // Listener for using mouse to click and drag the scroll thumb
        thumb_scroll: function(event) {
            var
                track  = document.querySelector('#modlog-matrix .matrix-xscroll__track'),
                thumb  = document.querySelector('#modlog-matrix .matrix-xscroll__thumb');

            if (!self.thumb_down || !track || !thumb.getBoundingClientRect().width) {
                if (self.track_down) {
                    event.preventDefault();
                }
                return;
            }

            event.preventDefault();

            var x = event.pageX - cumulativeOffset(track).left;
            var x_max = track.getBoundingClientRect().width - thumb.getBoundingClientRect().width;
            var delta = x - self.thumb_x0;

            if (delta < 0)
                delta = 0;
            if (delta >= x_max)
                delta = x_max;

            // The `delta` is calculated above for both functions, so scroll_content doesn't need to
            // be in the offsetLeft callback.
            offsetLeft(thumb, delta);
            self.scroll_content(delta, x_max);
        },

        // Listener for wheel y-scroll and trackpad two-finger x-scroll
        wheel_scroll: function(event) {
            // allow wheel y-scroll as x-scroll only upon hover of track
            // or x-scroll upon hover of area and horizontal scroll
            if (!self.track_hover && !(self.area_hover && event.deltaX)) {
                return;
            } else {
                event.preventDefault();
            }

            var
                wheelDelta = event.deltaY || event.deltaX,
                detail = event.detail,
                delta = 0;

            if (wheelDelta)
                delta = wheelDelta / 120;
            if (detail)
                delta = detail / 3;

            if (self.step_scroll(delta)) {
                event.preventDefault();
                event.stopPropagation();
            }
        },

        // Listener for one-finger pan using touchscreen
        pan_scroll: function(event) {
            var
                delta = event.deltaX,
                dist  = event.distance,
                velX  = event.velocityX;

            var steps = -velX;
            self.step_scroll(steps);
        },

        // Listener for track click (but not thumb)
        track_click: function(pageX, repeat) {
            var
                track   = document.querySelector('#modlog-matrix .matrix-xscroll__track'),
                thumb   = document.querySelector('#modlog-matrix .matrix-xscroll__thumb'),
                pane    = document.querySelector('#modlog-matrix .matrix-datawrap'),
                area    = document.querySelector('#modlog-matrix .matrix-table');

            if (!track || !self.track_down || self.thumb_down || !thumb.getBoundingClientRect().width)
                return;

            var click_x = pageX - cumulativeOffset(track).left,
                p_delta = Math.abs(offsetLeft(pane));

            if (click_x < offsetLeft(thumb)) {
                p_delta -= area.getBoundingClientRect().width;
            } else if (click_x > (offsetLeft(thumb) + thumb.getBoundingClientRect().width)) {
                p_delta += area.getBoundingClientRect().width;
            } else {
                return;
            }

            var max = pane.getBoundingClientRect().width - area.getBoundingClientRect().width;
            if (p_delta < 0)
                p_delta = 0;
            if (p_delta > max)
                p_delta = max;

            // set the pane x-offset to the p_delta. The p_delta (pane delta) is different from the
            // normal delta (thumb delta), so we need to wait for the pane delta update to finish
            // and then calculate the thumb delta from the new pane offset calculated using the
            // p_delta
            offsetLeft(pane, -p_delta, function() {
                // The `self.get_delta()` is dependent on the completion of the offset of the
                // pane, so the thumb offset update needs to be in the callback of the pane offset
                // update
                offsetLeft(thumb, self.get_delta(), self.fixThumbWidth);

                if (repeat) {
                    setTimeout(function() {
                        self.track_click(pageX, true);
                    }, 100);
                }
            });
        },

        // POSITIONING FUNCTIONS
        // --------------------------------------------------------------------------------

        // This function fixes everything (ex: position of scrollbar on page,
        // width and offset of scrollbar thumb)
        // This function should be called whenever something external to this script
        // caused the width of the scroll pane to be changed.
        revalidate: function() {
            var thumb = document.querySelector('#modlog-matrix .matrix-xscroll__thumb');

            if (!thumb) {
                return;
            }

            self.fixPosition();
            self.fixVisibility();
            self.fixThumbWidth();

            // simulate mousedown, mousemove, mouseup - in this order -
            // in order to trigger the thumb_scroll listener such that
            // the x-offset of the scrollbar thumb and pane can be fixed

            var clickEvent0 = document.createEvent('MouseEvents');
                clickEvent0.initEvent ('mousedown', true, true);

            thumb.dispatchEvent(clickEvent0);

            var clickEvent1 = document.createEvent('MouseEvents');
                clickEvent1.initEvent ('mousemove', true, true);

            thumb.dispatchEvent(clickEvent1);

            var clickEvent2 = document.createEvent('MouseEvents');
                clickEvent2.initEvent ('mouseup', true, true);

            thumb.dispatchEvent(clickEvent2);
        },

        // fixes the position of the floating container of the scrollbar
        fixPosition: function() {
            var xscroll  = document.querySelector('#modlog-matrix .matrix-xscroll');
            var xfloat   = document.querySelector('#modlog-matrix .matrix-xscroll__float');

            if (!xscroll) {
                return;
            }

            var
                co       = cumulativeOffset(xscroll),
                co_left  = co.left,
                xwidth   = xscroll.getBoundingClientRect().width,
                xheight  = xscroll.getBoundingClientRect().height;

            xfloat.setAttribute('style', 'left:'+co_left+'px;width:'+xwidth+'px');
        },

        fixVisibility: function() {
            var xscroll = document.querySelector('#modlog-matrix .matrix-xscroll');

            if (!xscroll) {
                return;
            }

            var
                vh = Math.max(document.documentElement.clientHeight, window.innerHeight || 0), // viewport height
                nh = document.getElementById('site-nav').getBoundingClientRect().height, // fixed nav height

                mt = cumulativeOffset(document.getElementById('modlog-matrix')).top, // matrix top
                mh = document.getElementById('modlog-matrix').getBoundingClientRect().height, // matrix height

                offset = mt + mh + 20 - vh, // 20 -> height of the bar in between the table and the runtime info
                scrollPos = window.scrollY + nh;

            if (scrollPos > offset) {
                n.addClass(xscroll, 'matrix-xscroll--static');
            } else {
                n.removeClass(xscroll, 'matrix-xscroll--static');
                self.fixPosition();
            }
        },
    };
})();