document.addEventListener('DOMContentLoaded', function() {
    var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;

    if (isIE11) {
        document.getElementById('ie11-error-notice').removeAttribute('style');
    }

    var isIELte10 = window.navigator.userAgent.indexOf('Trident/') > 0
                    || window.navigator.userAgent.indexOf('MSIE ') > 0;

    if (isIELte10) {
        document.getElementById('ieLte10-error-notice').removeAttribute('style');
    }
});