n.asi_list = [];
n.asi = function(o) {
    o = is_string(o) ? document.querySelector(o) : o;

    var instance = {
        options: null,
        parent: o,
        first_init: function() {
            if (!n.asi__FirstInitComplete) {
                n.asi__FirstInitComplete = true;

                // attach click handler to body to close asi helpers upon click-off
                n.on(document.body, 'click', function(event) {
                    var parent = n.closest(event.target, '.asi');
                    if (!parent) {
                        forEach(n.asi_list, function(item) {
                            item.helper.set_visible(false);
                        });
                    }
                });
            }
        },
        get_option: function(property, default_value) {
            if (!isset(instance.options[property])) {
                return default_value;
            }
            return instance.options[property];
        },
        // get property from options
        // if token_id is true (default), let tokens with the same property override options
        // if token_id is false, get from options only
        // if token_id is int, let property from the token with that id (if exists) override options
        get_property: function(property, token_id) {
            token_id = isset(token_id) ? token_id : true;

            var token_prop = null;
            if (token_id === false) {
                token_prop = null;
            } else if (token_id === true) {
                token_prop = instance.tokens.get_active_prop(property);
            } else {
                token_prop = instance.tokens.get_prop(token_id, property);
            }

            if (isset(token_prop)) {
                return token_prop;
            } else {
                return instance.options[property];
            }
        },
        init: function(options) {
            this.first_init();
            this.options = options || {};
            this.options['tokens'] = this.options['tokens'] || {};

            if (this.options.tokens) {
                for (var key in this.options.tokens) {
                    if (this.options.tokens.hasOwnProperty(key)) {
                        if (this.options.tokens[key].suggestions) {
                            this.options.tokens[key].suggestions_lower =
                                to_lower(this.options.tokens[key].suggestions);
                        }
                    }
                }
            }

            // create elements
            // ~~~~~~~~~~~~~~~
            n.addClass(o, 'asi');
            n.appendChild(n.gen('(.asi-input > \
                    span.asi-group + \
                    input.asi-text[type=text][style="width:1px"][autocomplete="off"][autocapitalize="off"][autocorrect="off"] + \
                    span.asi-placeholder["{}"] + \
                    i.asi-icon.asi-icon--empty.zmdi.zmdi-search + \
                    i.asi-icon.asi-icon--active.zmdi.zmdi-close-circle.asi-hide > \
                        span.asi-tooltip["clear"] ^ \
                    i.asi-icon.asi-icon--copy.zmdi.zmdi-copy > \
                        span.asi-tooltip["copy"] ^ \
                    span.asi-resizing-span + \
                    span.asi-resizing-span2 + \
                    div.asi-helper.asi-hide \
                )', instance.placeholder.get_default()), o);

            // attach event handlers
            // ~~~~~~~~~~~~~~~~~~~~~
            var wrap = o.querySelector('.asi-input');
            var input = o.querySelector('.asi-text');
            var clear_button = o.querySelector('.asi-icon--active');
            var copy_button = o.querySelector('.asi-icon--copy');

            n.on(wrap,  'click', this.events.click);
            n.on(input, 'keypress', this.events.keypress);
            n.on(input, 'keyup', this.events.keyup);
            n.on(input, 'keydown', this.events.keydown);
            n.on(input, 'focus', this.events.focus);
            n.on(input, 'cut', this.events.cut);
            n.on(input, 'paste', this.events.paste);
            n.on(input, 'copy', this.events.copy);

            // clear icon button handler
            n.on(clear_button, 'click', instance.clear);

            // copy icon button handler
            n.on(copy_button, 'click', function(event) {
                var tooltip = this.querySelector('.asi-tooltip');
                var success = n.copyTextToClipboard(instance.raw.stringify());
                tooltip.innerHTML = 'copied!';
                setTimeout(function() {
                    tooltip.innerHTML = 'copy';
                }, 1500);
            });

            return instance;
        },
        destroy: function() {
            // clear modules
            // ~~~~~~~~~~~~~
            instance.helper.clear();
            instance.tokens.clear();

            // remove event handlers
            // ~~~~~~~~~~~~~~~~~~~~~
            var wrap = o.querySelector('.asi-input');
            var input = o.querySelector('.asi-text');
            var clear_button = o.querySelector('.asi-icon--active');
            var copy_button = o.querySelector('.asi-icon--copy');

            n.off(input);
            n.off(wrap);
            n.off(clear_button);
            n.off(copy_button);

            // remove nodes from DOM
            // ~~~~~~~~~~~~~~~~~~~~~
            n.removeAllChildren(o);
        },
        get_state: function() {
            return {
                'text': o.querySelector('.asi-text').value,
                'tokens': instance.tokens.list,
            };
        },
        focus: function() {
            o.querySelector('.asi-text').focus();
        },
        blur: function() {
            o.querySelector('.asi-text').blur();
        },
        events: {
            // cut event - handles width adjustment upon clipboard cut
            cut: function(event) {
                var prev = this.value;
                var input = this;

                setTimeout(function() {
                    instance.events.input_changed(input.value, prev, event);
                }, 0);
            },
            // cut event - handles width adjustment upon clipboard paste
            paste: function(event) {
                var prev = this.value;
                var input = this;

                // if input is empty, or everything in the input is selected
                if ((prev.length == 0 || n.isTextSelected(input)) && event.clipboardData) {
                    event.preventDefault();
                    instance.raw.unstringify(event.clipboardData.getData('text'), true);
                    return false;
                }

                setTimeout(function() {
                    instance.events.input_changed(input.value, prev, event);
                }, 0);
            },
            // copy event - nothing to do here
            copy: function(event) {
                return;
            },
            // click event - handles focusing the input
            click_count: 0,
            last_click: new Date().getTime(),
            click: function(event) {
                var last_click = instance.events.last_click;
                instance.events.last_click = new Date().getTime();

                if (instance.events.last_click - last_click < 300) {
                    instance.events.click_count += 1;
                } else {
                    instance.events.click_count = 1;
                }

                var click_count = instance.events.click_count;

                if (n.closest(event.target, '.asi-helper') ||
                    n.closest(event.target, '.asi-icon--copy') ||
                    n.closest(event.target, '.asi-group')) {
                    return;
                }

                var input = this.querySelector('.asi-text');
                input.focus();

                // x/y coordinates relative to input element
                var x = event.clientX - this.getBoundingClientRect().left;
                var y = event.clientY - this.getBoundingClientRect().top;

                var word_split = /(\!|@|#|\$|%|\^|&|\*|\(|\)|\-|\+|=| )/;

                var cursor_to_end = function() {
                    if (click_count == 1) {
                        input.setSelectionRange(input.value.length, input.value.length);
                    } else if (click_count == 2) {
                        var split = input.value.split(word_split);
                        var last_word = split[split.length - 1];

                        input.setSelectionRange(input.value.length - last_word.length, input.value.length);
                    } else if (click_count >= 3) {
                        input.setSelectionRange(0, input.value.length);
                    }
                };

                var cursor_to_start = function() {
                    if (click_count == 1) {
                        input.setSelectionRange(0, 0);
                    } else if (click_count == 2) {
                        var first_word = input.value.split(word_split)[0];
                        input.setSelectionRange(0, first_word.length);
                    } else if (click_count >= 3) {
                        input.setSelectionRange(0, input.value.length);
                    }
                };

                // if click on the container element, set cursor position to end
                if (n.hasClass(event.target, 'asi-input')) {
                    if (x <= 10) { // 10px -> left padding of asi-input
                        cursor_to_start();
                    } else {
                        cursor_to_end();
                    }
                }
            },
            focus: function(event) {
                instance.helper.set_visible(true);
                if (o.querySelector('.asi-text').value.length == 0) {
                    instance.events.input_emptied(event);
                }
            },
            blur: function(event) {
                /* nothing to do here, helper hide occurs on user click-off */
            },
            keypress: function(event) {
                var input   = this,
                    code    = event.keyCode || event.which,
                    c       = String.fromCharCode(code),
                    prev    = this.value,
                    handled = false;

                var next = prev.substr(0, input.selectionStart) + c + prev.substr(input.selectionEnd);

                // this prevent_default function also clears 'c' to prevent width change
                function prevent_default() {
                    event.preventDefault();
                    c = '';
                }

                function clear() {
                    prevent_default();
                    input.value = '';
                    next = '';
                }

                if (code == 10 || code == 13) {
                    // Enter
                    handled = true;
                    prevent_default();

                    if (instance.call_user_event('enter').cancelled) {
                        return false;
                    }

                    if (!instance.helper.input.enter(event,false)) {
                        // If token is active, and current input value is not empty, then process the value
                        if (instance.tokens.is_active() && this.value.length !== 0) {
                            var result = instance.tokens.process.value(this.value,false);
                            if (result === true) {
                                clear();
                            } else if (result === false) {
                                return false;
                            }
                        // If token is not active, then enter was pressed at top-level, so complete
                        } else if (!instance.tokens.is_active()) {
                            if (!instance.call_user_event('complete').cancelled) {
                                instance.helper.set_visible(false);
                                o.querySelector('.asi-text').blur();
                            }
                            return false;
                        // If active, and token has values and current input value is empty
                        // Then set to closed (in other words, user changed mind about adding another value)
                        } else {
                            if (instance.tokens.active.values.length >= 1) {
                                instance.tokens.set_closed(instance.tokens.active.id, true);
                            }
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else switch (c) {
                    case ':':
                    case '(':
                    case ')':
                        if ((c == ':' || c == '(') && instance.tokens.process.label(this.value, event)) {
                            handled = true;
                            clear();
                        }

                        if ((c == ':' || c == ')') && instance.tokens.active) {
                            if (instance.tokens.process.modifier(instance.tokens.active.label, this.value, event)) {
                                handled = true;
                                clear();
                            }
                        }
                        break;
                    case ' ':
                        if (instance.tokens.is_active() && this.value.length == 0) {
                            if (instance.tokens.active.values.length >= 1) {
                                instance.tokens.set_closed(instance.tokens.active.id, true);
                            }
                            handled = true;
                            prevent_default();
                            return false;
                        } else if (instance.tokens.active && !instance.get_property('disable_space_completion')) {
                            var result = instance.tokens.process.value(this.value,false);
                            if (result === true) {
                                handled = true;
                                clear();
                            } else if (result === false) {
                                handled = true;
                                prevent_default();
                                return false;
                            }
                        }
                        break;
                    case '+':
                        if (instance.tokens.active && !instance.get_property('disable_plus_completion')) {
                            var pass = false;
                            if (instance.get_property('allow_strings')) {
                                var q1 = startsWith(this.value, "'"),
                                    q2 = startsWith(this.value, '"');
                                if (q1 || q2) {
                                    if (q1 && endsWith(this.value, "'")) {
                                    } else if (q2 && endsWith(this.value, '"')) {
                                    } else {
                                        pass = true;
                                    }
                                }
                            }

                            if (this.value.length == 0) {
                                handled = true;
                                prevent_default();
                                return false;
                            }

                            if (!pass) {
                                var result = instance.tokens.process.value(this.value,true);
                                if (result === true) {
                                    handled = true;
                                    clear();
                                } else if (result === false) {
                                    handled = true;
                                    prevent_default();
                                    return false;
                                }
                            }
                        }
                }

                var curr_type = instance.tokens.get_active_prop('type');
                if (!handled && curr_type && instance.types.checkch(curr_type, c, input.selectionStart, prev) === false) {
                    event.preventDefault();
                    return false;
                } else {
                    instance.placeholder.set_initial(false);
                }

                instance.events.input_changed(next, prev, event);
            },
            keyup: function(event) {
                var code = event.keyCode || event.which;
                switch (code) {
                    case 8:
                    case 46:
                        var prev = instance.getResizingValue();
                        instance.events.input_changed(this.value, prev, event);
                        break;
                }
            },
            keydown: function(event) {
                var code = event.keyCode || event.which,
                    c = String.fromCharCode(code),
                    input = this;

                function clear() {
                    event.preventDefault();
                    return (input.value = '');
                }

                switch (code) {
                    // Left
                    case 37:
                        break;
                    // Up
                    case 38:
                        if (instance.helper.input.up(event)) {
                            event.preventDefault();
                        }
                        break;
                    // Right
                    case 39:
                        break;
                    // Down
                    case 40:
                        if (instance.helper.input.down(event)) {
                            event.preventDefault();
                        }
                        break;
                    // tab
                    case 9:
                        if (instance.call_user_event('tab').cancelled) {
                            return false;
                        }

                        if (instance.helper.input.enter(event,false)) {
                            event.preventDefault();
                        } else if (instance.tokens.process.value(this.value,false)) {
                            var prev = this.value;
                            instance.events.input_changed(clear(), prev, event);
                        } else {
                            event.preventDefault();
                        }
                        break;
                    // backspace
                    case 8:
                        // if both equals 0 or both are identical, then that means
                        // the backspace occurred at the beginning of the input
                        if (this.value.length == 0 || (this.selectionStart == 0 && this.selectionEnd == 0)) {
                            event.preventDefault();
                            // if token is active and has a modifier, remove the modifier
                            // otherwise pop the last token
                            if (instance.tokens.is_active()) {
                                if (instance.tokens.has_modifier(instance.tokens.active.id)) {
                                    instance.tokens.remove_modifier(instance.tokens.active.id);
                                } else {
                                    var active_token = instance.tokens.active;
                                    if (active_token.values.length) {
                                        var value = active_token.values[active_token.values.length-1];
                                        instance.tokens.remove_value(active_token.id, active_token.values.length - 1);

                                        instance.set_text(value+instance.get_text());
                                        this.setSelectionRange(value.length,value.length);
                                    } else {
                                        instance.tokens.pop();
                                    }
                                }
                            } else {
                                var last_token = instance.tokens.peek();
                                if (last_token) {
                                    instance.tokens.set_closed(last_token.id, false);
                                }
                            }
                            return false;
                        }
                    // delete
                    case 46:
                        break;
                }
            },
            input_emptied: function(event) {
                if (!instance.tokens.is_active()) {
                    if (!instance.helper.search_options_exists()) {
                        instance.helper.clear(event).add_search_options();
                    }
                }
                instance.placeholder.set_initial(true);
                instance.helper.input.filter('');
            },
            input_changed: function(text, previous_text, event, call_event) {
                event = event || null;

                if (!isset(call_event) || call_event !== false) {
                    instance.call_user_event('textchange', {
                        text: text,
                        prev: previous_text,
                        jsevent: event,
                    });
                }

                instance.placeholder.check_icons(text);

                if (text.length == 0) {
                    instance.events.input_emptied(event);
                } else {
                    instance.placeholder.set_initial(false);
                    instance.helper.input.filter(text);
                }

                instance.adjustWidth(text);
            }
        },
        call_user_event: function(name, params) {
            var event = params || {};
            event.name = name;
            event.cancelled = false;
            event.set_cancelled = function(state) {
                this.cancelled = state;
            };
            event.is_cancelled = function() {
                return this.cancelled;
            };

            if (instance.event_reporting === false) {
                return event;
            }

            if (instance.options.events && instance.options.events[name]) {
                instance.options.events[name].call(instance, event);
            }

            return event;
        },
        types: {
            get_placeholder: function(type) {
                if (isset(type) && instance.types[type] && instance.types[type].placeholder) {
                    return instance.types[type].placeholder;
                }
                return null;
            },
            get_label: function(type, original) {
                if (isset(original)) {
                    return original;
                }
                if (isset(type) && instance.types[type] && instance.types[type].label) {
                    return instance.types[type].label;
                }
                return 'Search for';
            },
            checkch: function(type, ch, pos, beforetext, flag) {
                type = type.toLowerCase();
                flag = flag || '';
                if (contains(type, ':')) {
                    var typesplit = type.split(':');
                    type = typesplit[0];
                    flag = typesplit[1];
                }

                var typeconf = instance.types[type];
                if (typeconf && typeconf.checkch) {
                    return typeconf.checkch(ch,pos,beforetext,flag.toLowerCase());
                }
                return true;
            },
            validate: function(type, full, flag) {
                type = type.toLowerCase();
                flag = flag || '';
                if (contains(type, ':')) {
                    var typesplit = type.split(':');
                    type = typesplit[0];
                    flag = typesplit[1];
                }

                var typeconf = instance.types[type];
                if (typeconf && typeconf.validate) {
                    return typeconf.validate(full,flag.toLowerCase());
                }
                return true;
            },
            get_error: function(type, token_id, flag) {
                token_id = token_id || false;
                type = type.toLowerCase();
                // if options has type_error defined use that, otherwise use default
                if (instance.tokens.active || token_id) {
                    var type_error;
                    if (token_id) {
                        type_error = instance.tokens.get_prop(token_id, 'type_error');
                    } else {
                        type_error = instance.tokens.get_active_prop('type_error');
                    }
                    if (isset(type_error)) {
                        return type_error;
                    }
                }

                flag = flag || '';
                if (contains(type, ':')) {
                    var typesplit = type.split(':');
                    type = typesplit[0];
                    flag = typesplit[1];
                }

                var typeconf = instance.types[type];
                if (typeconf && typeconf.get_error) {
                    return typeconf.get_error(flag.toLowerCase());
                }
                return 'invalid';
            },
            'datetime': {
                label: 'datetime',
                placeholder: 'MMM dd YYYY hh:mm aa',
                /* matches using the date regex then the time regex below with space(s) between */
                validate: function(full, flag) {
                    return /^((Sun|Mon|Tue|Wed|Thu|Fri|Sat)\s+)?(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|June?|July?|Aug(ust)?|Sep(tember|t)?|Aug(ust)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\s+\d{0,2}\s+\d{4}((\s+(0?[1-9]|1[0-9]|2[0-3])):[0-5][0-9](:[0-5][0-9])?(\s+(am|pm))?)?$/i.test(full);
                },
            },
            'date': {
                label: 'date',
                placeholder: 'MMM dd YYYY',
                /* Match dates as "(Sun )Jan 01 (20)17" - parts in parenthesis are optional
                Don't allow "00/00/0000" format to prevent confusion between US/Europe */
                validate: function(full, flag) {
                    return /^((Sun|Mon|Tue|Wed|Thu|Fri|Sat)\s+)?(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|June?|July?|Aug(ust)?|Sep(tember|t)?|Aug(ust)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\s+\d{0,2}\s+\d{4}$/i.test(full);
                },
            },
            'time': {
                label: 'time',
                placeholder: 'hh:mm aa',
                /* Matches times as "10:10(:10)( [am|pm])"
                Leading zero optional for hour only */
                validate: function(full, flag) {
                    return /^(0?[1-9]|1[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?(\s+(am|pm))?$/i.test(full)
                },
            },
            'number': {
                label: 'number',
                checkch: function(c, pos, beforetext, flag) {
                    switch (flag) {
                        case 'int':
                        case 'integer':
                            var result = (c >= '0' && c <= '9') ||
                                    (c == '-' && pos == 0 && (beforetext.length == 0 || beforetext[0] != '-'));
                            return result;
                        case 'natural':
                            return (c >= '1' && c <= '9');
                        case 'whole':
                            return (c >= '0' && c <= '9');
                        case 'binary':
                            return c == '0' || c == '1';
                        case 'posdecimal':
                        case 'posreal':
                            return (c >= '0' && c <= '9') || (c == '.' && contains(beforetext, '.'));
                        case 'decimal':
                        case 'real':
                        default:
                            return (c >= '0' && c <= '9') || (c == '.' && contains(beforetext, '.')) ||
                                    (c == '-' && pos == 0 && (beforetext.length == 0 || beforetext[0] != '-'));
                    }
                },
                validate: function(full, flag) {
                    switch (flag) {
                        case 'int':
                        case 'integer':
                            return /^\-?\d*$/.test(full);
                        case 'posint':
                        case 'posinteger':
                            return /^\d*$/.test(full);
                        case 'negint':
                        case 'neginteger':
                            return /^\-\d*$/.test(full);
                        case 'natural':
                            return !/^0+$/.test(full) && /^\d+$/.test(full);
                        case 'whole':
                            return /^\d+$/.test(full);
                        case 'binary':
                            return /^(0|1)*$/.test(full)
                        case 'posdecimal':
                        case 'posreal':
                            return /^\d*\.?\d*$/.test(full);
                        case 'negdecimal':
                        case 'negreal':
                            return /^\-\d*\.?\d*$/.test(full);
                        case 'decimal':
                        case 'real':
                        default:
                            return /^\-?\d*\.?\d*$/.test(full);
                    }
                },
                get_error: function(flag) {
                    switch (flag) {
                        case 'int':
                        case 'integer':
                            return 'must be an integer!';
                        case 'posint':
                        case 'posinteger':
                            return 'must be a positive integer!';
                        case 'negint':
                        case 'neginteger':
                            return 'must be a negative integer!';
                        case 'natural':
                            return 'must be a natural number!';
                        case 'whole':
                            return 'must be a whole number!';
                        case 'binary':
                            return 'must be a binary number!';
                        case 'posdecimal':
                        case 'posreal':
                            return 'must be a positive number!';
                        case 'negdecimal':
                        case 'negreal':
                            return 'must be a negative number!';
                        case 'decimal':
                        case 'real':
                        default:
                            return 'must be a number!';
                    }
                },
            },
        },
        placeholder: {
            set: function(text) {
                var placeholder = o.querySelector('.asi-placeholder');
                placeholder.innerHTML = text;
            },
            get_default: function() {
                var ret = 'Search';
                if (!instance.is_token_empty() && instance.is_text_empty()) {
                    if (instance.options && isset(instance.options.active_placeholder)) {
                        ret = instance.options.active_placeholder;
                    }
                } else {
                    if (instance.options && isset(instance.options.placeholder)) {
                        ret = instance.options.placeholder;
                    }
                }
                if (ret === false) {
                    ret = '';
                } else if (ret === true) {
                    ret = 'Search';
                }
                return ret;
            },
            set_initial: function(state) {
                var placeholder = o.querySelector('.asi-placeholder');
                if (state) {
                    var tmp = instance.tokens.get_active_prop('placeholder');

                    if (instance.tokens.active && (!isset(tmp) || instance.tokens.active.use_type_placeholder) && instance.tokens.active.type) {
                        tmp = instance.types.get_placeholder(instance.tokens.active.type);
                    }

                    if (tmp) {
                        placeholder.innerHTML = tmp;
                    } else {
                        placeholder.innerHTML = instance.placeholder.get_default();
                    }
                } else {
                    placeholder.innerHTML = '';
                }
            },
            check_icons: function(text) {
                text = text || o.querySelector('.asi-text').value;
                if (text.length == 0 && instance.tokens.is_empty()) {
                    n.removeClass(o.querySelector('.asi-icon--empty'), 'asi-hide');
                    n.addClass(o.querySelector('.asi-icon--active'), 'asi-hide');
                } else {
                    n.addClass(o.querySelector('.asi-icon--empty'), 'asi-hide');
                    n.removeClass(o.querySelector('.asi-icon--active'), 'asi-hide');
                }
            },
        },
        clear: function() {
            instance.tokens.clear();
            instance.clear_text();
        },
        is_empty: function() {
            return instance.tokens.is_empty() && instance.is_text_empty();
        },
        clear_text: function() {
            instance.set_text('');
        },
        is_token_empty: function() {
            return instance.tokens.is_empty();
        },
        is_text_empty: function() {
            var input = o.querySelector('.asi-text');
            return input.value.length == 0;
        },
        get_text: function() {
            return o.querySelector('.asi-text').value;
        },
        set_text: function(text) {
            var input = o.querySelector('.asi-text'),
                prev = input.value;

            input.value = text;

            instance.events.input_changed(input.value, prev);
        },
        raw: {
            unstringify: function(stringified, keep_existing_tokens) {
                keep_existing_tokens = keep_existing_tokens || false;
                instance.raw.set_from_parse(instance.raw.parse(stringified), keep_existing_tokens);
            },
            stringify: function() {
                var text = '';
                forEach(o.querySelectorAll('.asi-group .asi-token'), function(token_el) {
                    var label = token_el.querySelector('.asi-token--label');
                    var modifier = token_el.querySelector('.asi-token--modifier');
                    var values = token_el.querySelectorAll('.asi-token--value');

                    text += label.innerHTML;
                    if (endsWith(text, ':')) {
                        text = text.slice(0,-1);
                    }

                    if (modifier) {
                        text += '(' + modifier.innerHTML + ')';
                    }

                    text += ':';

                    forEach(values, function(value_el, i, len) {
                        var value = value_el.innerHTML;
                        if (contains(value, ' ')) {
                            text += '"'+value+'"';
                        } else {
                            text += value;
                        }

                        if (i != len-1) {
                            text += '+';
                        }
                    });
                    text += ' ';
                });

                text += o.querySelector('.asi-text').value;
                return text;
            },
            set_from_parse: function(parse_data, keep_existing_tokens) {
                instance.event_reporting = false;
                keep_existing_tokens = keep_existing_tokens || false;

                if (keep_existing_tokens) {
                    instance.clear_text();
                } else {
                    instance.clear();
                }

                forEach(parse_data.tokens, function(token, idx, len) {
                    if (token.values.length == 0 && idx == len-1 && parse_data.text.length == 0) {
                        instance.tokens.process.label(token.label);
                        if (token.modifier) {
                            instance.tokens.process.modifier(token.label, token.modifier);
                        }
                        instance.event_reporting = true;
                        return;
                    }

                    var token_data = instance.tokens.create_token(token.label);
                    if (token_data !== false) {
                        if (token.modifier) {
                            instance.tokens.set_modifier(token_data.id, token.modifier);
                        }

                        forEach(token.values, function(item) {
                            instance.tokens.add_value(token_data.id, item);
                        });
                    }
                });

                if (parse_data.text.length) {
                    instance.set_text(parse_data.text);
                    instance.placeholder.set_initial(false);
                } else if (!instance.tokens.active) {
                    instance.placeholder.set_initial(true);
                }

                instance.placeholder.check_icons();
                instance.event_reporting = true;

                instance.call_user_event('tokenchange', {type: 'set_from_parse'});
            },
            parse: function(raw) {
                var result = {
                    tokens: [],
                    text: '',
                };

                function check_for_modifier(label) {
                    var modifier = null;
                    if (label.indexOf('(') != -1) {
                        var index = label.indexOf('(');

                        modifier = label.slice(index+1,label.length);
                        label = label.slice(0,index);

                        if (endsWith(modifier, ')')) {
                            modifier = modifier.slice(0,-1);
                        }
                    }
                    return {label: label, modifier: modifier};
                }

                var data = raw.trim();
                while ((data = data.split(/:(.+)/, 2)).length == 2) {
                    var token_label = data[0];

                    var mod_check = check_for_modifier(token_label);
                    var modifier = mod_check.modifier;
                    token_label = mod_check.label;

                    if (!instance.options.tokens || !instance.options.tokens[token_label]) {
                        break;
                    }

                    var values = [];
                    var token_data = data[1];
                    while (true) {
                        var quote1 = startsWith(token_data, "'");
                        var quote2 = startsWith(token_data, '"');

                        if (quote1 || quote2) {
                            var quote = quote1 ? "'" : '"';
                            token_data = token_data.slice(1);

                            var quote_index = token_data.indexOf(quote);
                            if (quote_index != -1) {
                                values.push(token_data.slice(0,quote_index));
                                data[1] = token_data.slice(quote_index+1,token_data.length);
                            } else {
                                values.push(token_data.slice(0,token_data.length));
                                data[1] = '';
                            }

                            if (startsWith(data[1], '+')) {
                                token_data = data[1].slice(1);
                            } else {
                                break;
                            }
                        } else {
                            var index = Math.min(token_data.indexOf(' '), token_data.indexOf('+'));
                            if (index == -1) {
                                index = Math.max(token_data.indexOf(' '), token_data.indexOf('+'));
                            }
                            if (index == -1) {
                                values.push(token_data);
                                data[1] = '';
                                break;
                            }

                            values.push(token_data.slice(0, index));

                            data[1] = token_data.slice(index, token_data.length);
                            if (startsWith(data[1], ' ')) {
                                data[1] = data[1].slice(1);
                                break;
                            } else {
                                token_data = data[1].slice(1);
                            }
                        }
                    }

                    data = data[1].trim();

                    result.tokens.push({
                        label: token_label,
                        modifier: modifier,
                        values: values,
                    });
                }

                if (!is_string(data)) {
                    data = data.join(':');
                }

                // check if ends with incomplete token (i.e. no values)
                if (!contains(data, ' ') && endsWith(data, ':')) {
                    var mod_check = check_for_modifier(data.slice(0,-1));
                    var modifier = mod_check.modifier;
                    var token_label = mod_check.label;

                    if (instance.options.tokens && instance.options.tokens[token_label]) {
                        result.tokens.push({
                            label: token_label,
                            modifier: modifier,
                            values: [],
                        });
                        data = '';
                    }
                }

                result.text = data;

                return result;
            }
        },
        adjustTooltips: function() {
            forEach(o.querySelectorAll('.asi-tooltip'), function(el) {
                // 12 -> tooltip side padding
                el.style.width = (instance.getInputWidth2(el.innerText)+12)+'px';
            });
        },
        adjustWidth: function(text) {
            var input = o.querySelector('.asi-text');
            input.setAttribute('style', 'width:'+this.getInputWidth(text)+'px');
        },
        _getInputWidth: function(text, resizing_span) {
            var tmp = o.querySelector(resizing_span);
            tmp.innerHTML = text.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/ /g,'&nbsp;');

            var width = tmp.getBoundingClientRect().width;
            if (width == 0) {
                width = 1;
            } else {
                width += 1;
            }
            return width;
        },
        getInputWidth: function(text) {
            return instance._getInputWidth(text, '.asi-resizing-span');
        },
        getResizingValue: function() {
            return o.querySelector('.asi-resizing-span').innerHTML
                .replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&nbsp;/g, ' ');
        },
        getInputWidth2: function(text) {
            return instance._getInputWidth(text, '.asi-resizing-span2');
        },
        getResizingValue2: function() {
            return o.querySelector('.asi-resizing-span2').innerHTML
                .replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&nbsp;/g, ' ');
        },
        tokens: {
            // list of currently tokens in order
            list: [],
            // map of all tokens (including deleted) by id
            by_id: {},
            // number of not-deleted tokens for each label
            label_count: Counter(),
            // the in-progress token, null if does not exist
            active: null,
            // to prevent overwriting deleted tokens in 'by_id' this should never be reset to 0
            token_next_id: 0,
            // returns true if there are no tokens
            is_empty: function() {
                return instance.tokens.list.length == 0;
            },
            // is there a token in progress?
            is_active: function() {
                return instance.tokens.active !== null;
            },
            // get the last token if exists
            peek: function() {
                if (instance.tokens.list.length == 0) {
                    return null;
                }
                return instance.tokens.list[instance.tokens.list.length - 1];
            },
            // removes the last token if exists
            pop: function() {
                if (instance.tokens.list.length == 0) {
                    return;
                }

                var last_token = instance.tokens.list.pop();
                instance.tokens.delete_token(last_token.id);
            },
            // get values by label
            get: function(label) {
                if (!isset(label)) {
                    return instance.tokens.list;
                }

                var values = [];

                forEach(instance.tokens.list, function(token) {
                    if (token.label === label) {
                        values = values.concat(token.values);
                    }
                });

                return values;
            },
            // remove all tokens
            clear: function() {
                instance.tokens.list = [];
                instance.tokens.active = null;
                instance.tokens.label_count = Counter();

                n.removeAllChildren(o.querySelector('.asi-group'));

                // have to clear the helper here to reset the helper filter
                // if search_options exists, but is filtered, nothing will happen
                // because input_emptied doesn't reset the search_options if it
                // already exists regardless of its filtered state
                instance.helper.clear();
                instance.call_user_event('tokenchange', {type: 'clear'});
            },
            get_active_prop: function(property) {
                if (!instance.tokens.is_active()) {
                    return null;
                }
                return instance.tokens.get_prop(instance.tokens.active.id, property);
            },
            get_prop: function(token_id, property) {
                var mod_prop = instance.tokens.get_mod_prop(token_id, property);
                if (mod_prop !== null) {
                    return mod_prop;
                }
                return instance.tokens.by_id[token_id][property];
            },
            get_mod_prop: function() {
                var a = arguments,
                    l = a.length,
                    token_id = a[0],
                    modifier, property;

                if (l == 2) {
                    modifier = instance.tokens.get_modifier(token_id);
                    property = a[1];
                } else if (l == 3) {
                    modifier = a[1];
                    property = a[2];
                }

                if (!modifier) {
                    return null;
                }

                var token_data = instance.tokens.by_id[token_id];

                if (!isset(token_data.modifiers)) {
                    return null;
                }

                if (is_array(token_data.modifiers)) {
                    return null;
                }

                if (!isset(token_data.modifiers[modifier])) {
                    return null;
                }

                if (!isset(token_data.modifiers[modifier][property])) {
                    return null;
                }

                return token_data.modifiers[modifier][property];
            },
            process: {
                label: function(token_label, event) {
                    event = event || null;

                    if (instance.tokens.is_active() || !instance.options.tokens[token_label]) {
                        return false;
                    }

                    var do_merge_prev = false;

                    var prev_token = instance.tokens.peek();
                    if (prev_token && prev_token.error === false && prev_token.label === token_label) {
                        var multivals = instance.get_property('multiple_values', prev_token.id);
                        if (multivals === true || (is_int(multivals) && prev_token.values.length+1 <= multivals)) {
                            do_merge_prev = true;
                        }
                    }

                    if (do_merge_prev) {
                        instance.tokens.active = prev_token;
                        instance.tokens.set_closed(prev_token.id, false);
                    } else {
                        var token_data = instance.tokens.create_token(token_label, true);
                        if (token_data === false) {
                            return false;
                        }
                    }

                    instance.placeholder.check_icons();
                    instance.helper.fix(event);
                    instance.placeholder.set_initial(true);

                    return true;
                },
                modifier: function(token_label, modifier, event) {
                    event = event || null;

                    if (!instance.tokens.is_active()) {
                        return false;
                    }

                    if (!contains(instance.tokens.active.modifiers, modifier)) {
                        return false;
                    }

                    instance.tokens.set_modifier(instance.tokens.active.id, modifier);
                    instance.helper.fix(event);
                    instance.placeholder.set_initial(true);

                    return true;
                },
                value: function(text, add_another) {
                    add_another = add_another || false;
                    if (!instance.tokens.is_active()) {
                        return;
                    }

                    var multival = instance.get_property('multiple_values');
                    if (multival === false || (multival !== true && instance.tokens.active.values.length+1 >= multival)) {
                        add_another = false;
                    }

                    if (!instance.get_property('disable_space_completion') && instance.get_property('allow_strings')) {
                        if ((startsWith(text, '"') && !endsWith(text, '"')) ||
                            (startsWith(text, "'") && !endsWith(text, "'"))) {
                            return;
                        }

                        if (startsWith(text, '"') || startsWith(text, "'")) {
                            text = text.slice(1,-1);
                        }
                    }

                    var active_id = instance.tokens.active.id;

                    if (instance.get_property('disable_error_completion',active_id) === true) {
                        // if disable_error_completion is enabled, and token value has an error
                        // don't allow the value-add to go through
                        if (instance.tokens.validate(active_id, text) === false) {
                            instance.tokens.add_value(active_id, text, !add_another);
                        } else {
                            return false;
                        }
                    } else {
                        instance.tokens.add_value(active_id, text, !add_another);
                        instance.tokens.validate(active_id);
                    }

                    return true;
                },
            },
            create_token: function(label, set_as_active) {
                set_as_active = set_as_active || false;

                var allow_duplicates = isset(instance.options.tokens[label].allow_duplicates) ?
                                         instance.options.tokens[label].allow_duplicates :
                                         instance.options.allow_duplicates;
                if (instance.tokens.label_count.get(label) >= 1 &&
                        isset(allow_duplicates) && allow_duplicates === false) {
                    return false;
                }

                var id = instance.tokens.token_next_id;

                o.querySelector('.asi-group').appendChild(
                    n.gen('span.asi-token[data-token-id="'+id+'"] > span.asi-token--label["'+label+':"]'));

                var token_data = copy(instance.options.tokens[label]);
                token_data['element'] = o.querySelector('.asi-group .asi-token[data-token-id="'+id+'"]');
                token_data['modifier'] = null;
                token_data['values'] = [];
                token_data['closed'] = false;
                token_data['error'] = false;
                token_data['label'] = label;
                token_data['id'] = id;

                if (token_data.type && token_data.type == 'date' || token_data.type == 'time' || token_data.type == 'datetime') {
                    token_data.disable_space_completion = true;
                }

                token_data.search_label = instance.types.get_label(token_data['type'], token_data['search_label']);

                instance.tokens.list.push(token_data);
                instance.tokens.by_id[id] = token_data;
                instance.tokens.label_count.add(label);

                if (set_as_active) {
                    instance.tokens.active = token_data;
                }

                instance.call_user_event('tokenchange', {type: 'create_token', subject: token_data});

                instance.tokens.token_next_id += 1;
                return token_data;
            },
            get_label: function(token_id) {
                return instance.tokens.by_id[token_id].label;
            },
            set_modifier: function(token_id, modifier) {
                var data = instance.tokens.by_id[token_id];
                var element = data.element;

                data.modifier = modifier;

                n.insertAfter(n.simple_gen('span', {
                    'class': 'asi-token--modifier',
                    'text': modifier,
                }), element.querySelector('.asi-token--label'));

                instance.call_user_event('tokenchange', {type: 'set_modifier', subject: data});

                return true;
            },
            remove_modifier: function(token_id) {
                var token_data = instance.tokens.by_id[token_id];
                token_data.modifier = null;

                var el = token_data.element.querySelector('.asi-token--modifier');
                if (el) {
                    n.removeNode(el);
                    if (instance.tokens.active && instance.tokens.active.id == token_id) {
                        var token_label = instance.tokens.active.label;
                        instance.helper.fix();
                    }
                    instance.call_user_event('tokenchange', {type: 'remove_modifier', subject: token_data});
                }
            },
            get_modifier: function(token_id) {
                return instance.tokens.by_id[token_id].modifier;
            },
            has_modifier: function(token_id) {
                return instance.tokens.by_id[token_id].modifier !== null;
            },
            set_closed: function(token_id, state) {
                var token_data = instance.tokens.by_id[token_id];
                if (state == true) {
                    token_data.closed = true;
                    n.addClass(token_data.element, 'asi-token--closed');
                    if (instance.tokens.active && instance.tokens.active.id == token_id) {
                        instance.tokens.active = null;

                        instance.helper.fix();
                        instance.placeholder.set_initial(true);
                    }
                } else {
                    token_data.closed = false;
                    n.removeClass(token_data.element, 'asi-token--closed');
                    if (!instance.tokens.active && instance.tokens.peek() && instance.tokens.peek().id == token_id) {
                        instance.tokens.active = token_data;
                        instance.placeholder.set_initial(true);
                        instance.helper.fix();

                        if (token_data.values.length) {
                            var list        = token_data.element.querySelectorAll('.asi-token--value'),
                                value_el    = list[list.length - 1],
                                value_text  = value_el.innerHTML;

                            n.remove(value_el);
                            token_data.values.splice(token_data.values.length - 1, 1);

                            instance.set_text(value_text);
                            o.querySelector('.asi-text').setSelectionRange(value_text.length,value_text.length);
                        }
                    }
                }
            },
            add_value: function(token_id, value, set_closed) {
                set_closed = isset(set_closed) ? set_closed : true;
                var token_data  = instance.tokens.by_id[token_id];
                    token_el    = token_data.element;

                if (token_data.valuepass) {
                    value = token_data.valuepass(value);
                }

                token_data.values.push(value);
                token_el.appendChild(n.simple_gen('span', {
                    'class': 'asi-token--value',
                    'text': value,
                }));

                instance.tokens.set_closed(token_id, set_closed);

                instance.call_user_event('tokenchange', {type: 'add_value', subject: token_data});

                return true;
            },
            remove_value: function(token_id, index) {
                var token_data  = instance.tokens.by_id[token_id];
                    token_el    = token_data.element;
                if (index >= 0 && index <= token_data.values.length-1) {
                    token_data.values.splice(index,1);

                    var list = token_el.querySelectorAll('.asi-token--value');
                    n.remove(list[index]);
                } else {
                    return false;
                }

                instance.call_user_event('tokenchange', {type: 'remove_value', subject: token_data});
                return true;
            },
            clear_values: function(token_id) {
                var token_data = instance.tokens.by_id[token_id];
                token_data.values = [];

                n.remove(o.querySelectorAll('.asi-group .asi-token[data-token-id="'+token_id+'"] .asi-token--value'));
                instance.call_user_event('tokenchange', {type: 'clear_values', subject: token_data});
            },
            get_values: function(token_id) {
                return instance.tokens.by_id[token_id].values;
            },
            delete_token: function(token_id) {
                var token_data = instance.tokens.by_id[token_id],
                    token_el = instance.tokens.by_id[token_id].element,
                    input    = o.querySelector('.asi-text')
                    new_list = [];

                forEach(instance.tokens.list, function(item) {
                    if (item['id'] !== token_id) {
                        new_list.push(item);
                    }
                });

                instance.tokens.list = new_list;
                instance.tokens.label_count.subtract(token_data.label);

                if (instance.tokens.active && instance.tokens.active.id == token_id) {
                    instance.tokens.active = null;
                    instance.helper.fix();
                }

                n.removeNode(token_el);
                instance.placeholder.check_icons();
                instance.call_user_event('tokenchange', {type: 'delete_token', subject: token_data});

                return true;
            },
            validate: function(token_id, value_optional) {
                var token_el        = instance.tokens.by_id[token_id].element,
                    token_data      = instance.tokens.by_id[token_id],
                    token_label     = instance.tokens.get_label(token_id),
                    token_values    = value_optional || instance.tokens.get_values(token_id),
                    token_type      = instance.tokens.get_prop(token_id, 'type') || 'text';

                function validator(conf) {
                    conf = conf || {};
                    var state = false;

                    var match = conf['match'] || /(.*?)/;
                    var check = conf['check'] || function() { return true; };
                    var error = conf['error'] || "invalid input";
                    var slimit = conf['limit_to_suggestions'] || false;

                    function value_validate(token_value) {
                        if (state !== false) {
                            return false;
                        }
                        if (token_value.length == 0) {
                            state = false;
                        } else {
                            if (instance.types.validate(token_type, token_value) === false) {
                                state = instance.types.get_error(token_type, token_id);
                            } else {
                                if (match.test(token_value) && check(token_value)) {
                                    state = false;
                                } else {
                                    state = error;
                                }
                            }
                        }

                        if (token_value.length != 0 && slimit && token_data.suggestions) {
                            if (!contains(token_data.suggestions_lower, token_value.toLowerCase())) {
                                if (is_string(slimit)) {
                                    state = slimit;
                                } else {
                                    state = 'Not a valid choice! Choose from dropdown.';
                                }
                            }
                        }
                    }

                    if (is_string(token_values)) {
                        value_validate(token_values);
                    } else {
                        forEach(token_values, value_validate);
                    }

                    return state;
                }

                function check() {
                    var validate_conf = instance.tokens.get_prop(token_id, 'validate'), state;

                    if (validate_conf || token_type != 'text') {
                        state = validator(validate_conf);
                    } else {
                        state = false;
                    }

                    var event = instance.call_user_event('validate', {
                        token: token_data,
                        conf: validate_conf,
                        error: state,
                    });

                    if (event.cancelled) {
                        event.error = false;
                    }

                    return event.error;
                }

                function process(error_state) {
                    if (instance.tokens.active && instance.tokens.active.element == token_el) {
                        instance.helper.set_error(error_state);
                        if (error_state === false) {
                            n.removeClass(token_el, 'asi-token--error');
                            token_data.error = false;
                        } else {
                            n.addClass(token_el, 'asi-token--error');
                            token_data.error = error_state;
                        }
                    } else {
                        if (error_state === false) {
                            n.remove(token_el.querySelector('.asi-tooltip'));
                            n.removeClass(token_el, 'asi-token--error');
                            token_data.error = false;
                            return;
                        } else {
                            var tooltip = n.simple_gen('span', {'class': 'asi-tooltip', 'text': error_state});
                            n.prependChild(tooltip, token_el);
                            n.addClass(token_el, 'asi-token--error');
                            instance.adjustTooltips();
                            token_data.error = error_state;
                        }
                    }
                    return error_state;
                }

                return process(check());
            }
        },
        helper: {
            active_item: null,
            item_list: [],
            filtered_list: [],
            destroy_queue: [],
            filter_queue: [],
            set_visible: function(state) {
                if (state) {
                    n.addClass(o, 'asi--helper-visible');
                    n.removeClass(o.querySelector('.asi-helper'), 'asi-hide');
                } else {
                    n.removeClass(o, 'asi--helper-visible');
                    n.addClass(o.querySelector('.asi-helper'), 'asi-hide');
                }
                return instance.helper;
            },
            clear: function(event) {
                if (event) {
                    event.stopPropagation();
                    o.querySelector('.asi-text').focus();
                }

                instance.helper.item_list = [];
                instance.helper.filtered_list = [];
                instance.helper.active_item = null;

                n.off(o.querySelectorAll('.asi-helper .asi-helpitem'), 'click');

                forEach(instance.helper.destroy_queue, function(item) {
                    if (item.cleanup) {
                        item.cleanup();
                    }
                    if (item.destroy) {
                        item.destroy();
                    }
                });

                instance.helper.destroy_queue = [];
                instance.helper.filter_queue = [];

                n.removeAllChildren(o.querySelector('.asi-helper'));

                return instance.helper;
            },
            set_error: function(message) {
                var helper_el = o.querySelector('.asi-helper');
                var search_for = helper_el.querySelector('.asi--search-for');

                // set_error is called from tokens.validate, which does not consider empty values to be invalid.
                // The "search-for" component exists when the input is not empty, so technically this if statement
                // should never evaluate to be true, unless called externally
                if (!search_for) {
                    return;
                }

                if (message === false) {
                    n.remove(search_for.querySelector('.asi-search-for--error'));
                    search_for.removeAttribute('data-error');
                } else {
                    if (message == search_for.getAttribute('data-error')) {
                        return;
                    } else {
                        search_for.setAttribute('data-error', message);
                    }

                    var error_el = n.simple_gen('div', {'class': 'asi-search-for--error', 'text': message});
                    search_for.appendChild(error_el);
                }
            },
            fix: function(event) {
                var helper_el = o.querySelector('.asi-helper');
                event = event || null;

                if (!instance.tokens.is_active()) {
                    if (!instance.helper.search_options_exists()) {
                        instance.helper.clear(event).add_search_options();
                    }
                } else {
                    var modifier    = instance.tokens.get_modifier(instance.tokens.active.id);
                    var token_label = instance.tokens.active.label;

                    var s1 = !instance.tokens.active.suggestions || instance.helper.token_searcher_exists();
                    var s2 = !instance.tokens.get_active_prop('desc_html') || instance.helper.token_description_exists();
                    var s3 = !instance.tokens.active.modifiers || instance.helper.token_modifiers_exists();

                    if (modifier) {
                        if (!s1 || !s2 || s3) {
                            instance.helper.clear(event)
                                .add_token_description(token_label, modifier)
                                .add_token_searcher(token_label);
                        }
                    } else {
                        if (!s1 || !s2 || !s3 || instance.helper.search_options_exists()) {
                            instance.helper.clear(event)
                                .add_token_description(token_label)
                                .add_token_modifiers(token_label)
                                .add_token_searcher(token_label);
                        }
                    }
                }

                if (instance.tokens.active) {
                    var type = instance.tokens.active.type || false;
                    helper_el.setAttribute('data-token-for--label', instance.tokens.active.label);
                    helper_el.setAttribute('data-token-for--id', instance.tokens.active.id);
                    helper_el.setAttribute('data-token-for--type', type);
                } else {
                    helper_el.removeAttribute('data-token-for--label');
                    helper_el.removeAttribute('data-token-for--id');
                    helper_el.removeAttribute('data-token-for--type');
                }
            },
            /* ------ KEY NAVIGATION */
            input: {
                filter: function(text) {
                    var helper_el = o.querySelector('.asi-helper');
                    var original_text = text;
                    text = text.toLowerCase();

                    if (instance.helper.filter_queue.length) {
                        forEach(instance.helper.filter_queue, function(item) {
                            if (item.filter) {
                                item.filter(original_text);
                            }
                        });
                    }

                    if (instance.helper.item_list.length != 0) {
                        var new_filtered_list = [];
                        var bucket = {};

                        forEach(instance.helper.item_list, function(item) {
                            var data = item.getAttribute('data-value').toLowerCase();
                            var state = true;

                            if (text.length != 0 && (!data || !startsWith(data, text))) {
                                n.addClass(item, 'asi-hide');
                                if (item == instance.helper.active_item) {
                                    instance.helper.active_item = null;
                                }
                                state = false;
                            } else {
                                new_filtered_list.push(item);
                                n.removeClass(item, 'asi-hide');
                                state = true;
                            }

                            var section = item.getAttribute('data-parent');
                            if (section) {
                                if (!bucket[section]) {
                                    bucket[section] = 0;
                                }
                                bucket[section] += (state ? 1 : 0);
                            }
                        });

                        instance.helper.filtered_list = new_filtered_list;

                        // hide sections with no items in its filtered list
                        for (section in bucket) {
                            if (bucket.hasOwnProperty(section)) {
                                var section_el = helper_el.querySelector('section[data-section='+section+']');
                                if (bucket[section] == 0) {
                                    n.addClass(section_el, 'asi-hide');
                                } else {
                                    n.removeClass(section_el, 'asi-hide');
                                }
                            }
                        }
                    }

                    // update "search for"
                    var search_for = helper_el.querySelector('.asi--search-for');
                    var search_for__input = helper_el.querySelector('.asi-search-for--input');
                    var search_for__label = (instance.get_property('search_label') || 'Search for')+':';

                    if (!search_for__input) {
                        n.prependChild(n.gen('section.asi--search-for[data-section=search-for] > \
                            span.asi-search-for--label["{}"] + \
                            input.asi-search-for--input[type=text][disabled] + \
                            kbd["ENTER"] \
                            ', search_for__label), helper_el);

                        search_for = helper_el.querySelector('.asi--search-for');
                        search_for__input = helper_el.querySelector('.asi-search-for--input');
                    }

                    if (original_text.length == 0)  {
                        n.addClass(search_for, 'asi-hide');
                        search_for__input.value = original_text;
                    } else {
                        n.removeClass(search_for, 'asi-hide');
                        search_for__input.value = original_text;
                    }

                    if (instance.tokens.active) {
                        n.addClass(search_for, 'asi-search-for--token-active');
                        if (instance.get_property('disable_space_completion')) {
                            n.addClass(search_for, 'asi--no-space-completion');
                        } else {
                            n.removeClass(search_for, 'asi--no-space-completion');
                        }
                        instance.tokens.validate(instance.tokens.active.id, original_text);
                    } else {
                        n.removeClass(search_for, 'asi-search-for--token-active');
                    }
                },
                down: function(event) {
                    if (instance.helper.filtered_list.length == 0) {
                        return false;
                    }

                    if (instance.helper.active_item == null) {
                        instance.helper.active_item = instance.helper.filtered_list[0];
                    } else {
                        n.removeClass(instance.helper.active_item, 'asi-selected');

                        var i = instance.helper.filtered_list.indexOf(instance.helper.active_item);
                        if (i == -1) return false;
                        if ((++i) > instance.helper.filtered_list.length-1) {
                            i = 0;
                        }

                        instance.helper.active_item = instance.helper.filtered_list[i];
                    }

                    instance.helper.ensure_item_visibility(instance.helper.active_item, true);
                    n.addClass(instance.helper.active_item, 'asi-selected');
                    return true;
                },
                up: function(event) {
                    if (instance.helper.filtered_list.length == 0) {
                        return false;
                    }

                    if (instance.helper.active_item == null) {
                        instance.helper.active_item = instance.helper.filtered_list[instance.helper.filtered_list.length-1];
                    } else {
                        n.removeClass(instance.helper.active_item, 'asi-selected');

                        var i = instance.helper.filtered_list.indexOf(instance.helper.active_item);
                        if (i == -1) return false;
                        if ((--i) < 0) {
                            i = instance.helper.filtered_list.length-1;
                        }

                        instance.helper.active_item = instance.helper.filtered_list[i];
                    }

                    instance.helper.ensure_item_visibility(instance.helper.active_item, false);
                    n.addClass(instance.helper.active_item, 'asi-selected');
                    return true;
                },
                // finds the current active helper item and simulates a click if it exists
                enter: function(event,add_another) {
                    add_another = add_another || false;
                    if (instance.helper.active_item !== null) {
                        var input = o.querySelector('.asi-text');
                        var data = instance.helper.active_item.getAttribute('data-value');

                        if (input.value.length && startsWith(data.toLowerCase(), input.value.toLowerCase())) {
                            instance.set_text('');
                        }

                        var active_item = instance.helper.active_item;
                        active_item.setAttribute('data-add-another', add_another);
                        active_item.click();
                        return true;
                    }
                    return false;
                }
            },
            ensure_item_visibility: function(active_item, direction_down) {
                if (!active_item) return;

                var container   = n.closest(active_item, '.asi-helplisting');
                var itemIndex   = Array.prototype.indexOf.call(container.childNodes, active_item);

                var relPos      = container.scrollTop;
                var relHeight   = container.getBoundingClientRect().height;
                var itemHeight  = active_item.getBoundingClientRect().height;
                var itemPos     = itemIndex * itemHeight;

                if (relPos < itemPos && (itemPos+itemHeight) <= (relPos + relHeight)) {
                } else {
                    if (direction_down) {
                        container.scrollTop = itemPos-relHeight+itemHeight;
                    } else {
                        container.scrollTop = itemPos;
                    }
                }
            },
            create_component: function(section_id, section_title, itemlist) {
                var enter = o.querySelector('.asi-helper');

                if (itemlist === false) {
                    enter.appendChild(
                        n.gen('section.asi--{sectionid}.asi--sectionLoading[data-section={sectionid}] > \
                            span.asi-helptitle["{sectiontitle}"] + \
                            div.halign.spacer10-bottom > \
                                .loading.smaller',
                            {
                                sectionid: section_id,
                                sectiontitle: section_title,
                            })
                        );
                    return instance.helper;
                }

                if (itemlist.length === 0) {
                    enter.appendChild(
                        n.gen('section.asi--{sectionid}.asi--sectionLoading[data-section={sectionid}] > \
                            span.asi-helptitle["{sectiontitle}"] + \
                            p["No suggestions available."]',
                            {
                                sectionid: section_id,
                                sectiontitle: section_title,
                            })
                        );
                    return instance.helper;
                }

                var options = {
                    sectionid: section_id,
                    sectiontitle: section_title,
                    items: function(el, idx) {
                        var item = itemlist[idx],
                            item_html = item.html,
                            item_click = item.click,
                            item_text  = item.value;

                        // stop click event to prevent unfocusing the text element
                        // and causing the helper visibility to go away
                        n.on(el, 'click', function(event) {
                            event.preventDefault();
                            event.stopPropagation();
                            item_click.call(this, event);
                            return false;
                        });

                        el.innerHTML = item_html;
                        el.setAttribute('data-value', item_text);
                        el.setAttribute('data-parent', section_id);

                        instance.helper.item_list.push(el);
                        instance.helper.filtered_list.push(el);
                    },
                    item_count: itemlist.length,
                };

                var section =
                    n.gen('section.asi--{sectionid}[data-section={sectionid}] > \
                    span.asi-helptitle["{sectiontitle}"] + \
                    ul.asi-helplisting > \
                        li.asi-helpitem.{items} * {item_count}', options);

                enter.appendChild(section);
                return instance.helper;
            },
            component_exists: function(id) {
                return o.querySelector('.asi-helper section.asi--'+id) !== null;
            },
            /* ------ TOKEN SEARCHER */
            add_token_searcher: function(token_label) {
                if (!instance.options || !instance.options.tokens) {
                    return instance.helper;
                }

                var token_data = instance.options.tokens[token_label];
                if (!token_data) return instance.helper;

                if (token_data.suggestions) {
                    var loadingSection =
                        o.querySelector('.asi-helper .asi--token-searcher.asi--sectionLoading');

                    var title = token_data['placeholder'] ? token_data['placeholder'] : 'Suggestions',
                        items = [];

                    if (is_promise(token_data.suggestions)) {
                        setTimeout(function() {
                            instance.helper.add_token_searcher(token_label);
                        }, 50);

                        if (loadingSection) {
                            return instance.helper;
                        }

                        return instance.helper.create_component('token-searcher', title, false);
                    } else {
                        n.remove(loadingSection);
                    }

                    forEach(token_data.suggestions, function(suggestion_text) {
                        var html = '<span class="asi-helpitem--label">' + token_label + ':</span> ' +
                                   '<span class="asi-helpitem--placeholder">' + suggestion_text + '</span>';
                       items.push({html: html, value: suggestion_text, click: function(event) {
                            var add_another = this.getAttribute('data-add-another') == 'true';
                            this.removeAttribute('data-add-another');

                            instance.tokens.process.value(suggestion_text, add_another);
                            instance.clear_text();
                            setTimeout(function() {
                                instance.events.input_emptied(event);
                                instance.focus();
                            });
                        }})
                    });

                    return instance.helper.create_component('token-searcher', title, items);
                } else if (token_data.type) {
                    var enter = o.querySelector('.asi-helper'), html;
                    var typedata = token_data.type.toLowerCase().split(':');
                    var type = typedata[0];
                    var flag = typedata.length == 2 ? typedata[1] : null;

                    var do_date = type == 'datetime' || type == 'date';
                    var do_time = type == 'datetime' || type == 'time';

                    if (do_date || do_time) {
                        html = instance.helper._token_searcher__datetime(type, flag, do_date, do_time);
                    }

                    if (html) {
                        enter.appendChild(html);
                    }
                } else {
                    return instance.helper;
                }
            },
            _token_searcher__datetime: function(type, flag, do_date, do_time) {
                var component = {
                    html: n.gen('section.asi--type-{0}[data-section=type-{0}] >\
                        span.asi-helptitle > \
                            span["Choose {0}"] + \
                            span.grow + \
                            button.asi-helpbtn.asi-helpbtn--cancel["Cancel"] + \
                            button.asi-helpbtn.asi-helpbtn--done["Done"] ^ \
                        div.asi-helpbody.asi-component--datetime[data-date={1}][data-time={2}]',
                        type, do_date, do_time),
                    date_picker: null,
                    time_picker: null,
                    set_value: function(value) {
                        var parts = value.split(' ');

                        var day_name = null;
                        var month = null;
                        var day = null;
                        var year = null;

                        var hour = null;
                        var minute = null;
                        var second = null;
                        var period = null;

                        var offset = 0;

                        if (do_date) {
                            if (/^(Sun|Mon|Tue|Wed|Thu|Fri|Sat)$/i.test(parts[0])) {
                                day_name = parts[0];
                                offset = 1;
                            }

                            if (parts.length >= 3) {
                                if (parts[offset].length >= 3) {
                                    month = parts[offset];
                                }
                                offset++;
                                if (is_int(parts[offset])) {
                                    day = parseInt(parts[offset]);
                                }
                                offset++;
                                if (is_int(parts[offset]) && parts[offset].length == 4) {
                                    year = parseInt(parts[offset]);
                                }
                                offset++;
                            }
                        }

                        if (do_time) {
                            if (parts[offset] && contains(parts[offset],':')) {
                                var time_parts = parts[offset].split(':');
                                if (time_parts.length >= 2) {
                                    hour = parseInt(time_parts[0]);
                                    minute = parseInt(time_parts[1]);
                                    if (time_parts.length == 3) {
                                        second = parseInt(time_parts[2]);
                                    }
                                }
                                offset++;
                                if (parts[offset]) {
                                    period = parts[offset];
                                }
                            }
                        }

                        if (do_date && isset(month) && isset(day) && isset(year) && !isNaN(day) && !isNaN(year)) {
                            component.set_date_value(month, day, year);
                        }

                        if (do_time && isset(hour) && isset(minute) && !isNaN(hour) && !isNaN(minute)) {
                            if (!isset(period)) {
                                period = 'pm';
                            }
                            component.set_time_value(hour, minute, period);
                        }
                    },
                    set_date_value(month, day, year) {
                        // will also trigger onSelect and thus update the 'data-date-val' attribute
                        component.date_picker.setDate(month+' '+day+' '+year,true);
                        component.html.setAttribute('data-date-val',
                            component.date_picker.toString('MMM DD YYYY'));
                    },
                    set_time_value(hour, minute, period) {
                        component.time_picker.setTime(hour,minute,period,true);
                    },
                    get_value: function() {
                        var value = '';
                        if (do_date) {
                            var val = component.html.getAttribute('data-date-val');
                            if (val !== 'false') {
                                value += val;
                            }
                        }
                        if (do_time) {
                            var val = component.html.getAttribute('data-time-val');
                            if (val !== 'false') {
                                if (value.length) {
                                    value += ' ';
                                }
                                value += val;
                            }
                        }
                        return value;
                    },
                    cleanup: function() {
                        if (do_time) {
                            component.time_picker.destroy();
                        }
                        if (do_date) {
                            component.date_picker.destroy();
                        }

                        n.off(component.html.querySelector('.asi-helpbtn--cancel'));
                        n.off(component.html.querySelector('.asi-helpbtn--done'));
                    },
                    cancel: function(event) {
                        event.preventDefault();
                        event.stopPropagation();

                        component.cleanup();
                        instance.tokens.pop();
                        instance.set_text('');

                        instance.focus();
                        return false;
                    },
                    update: function() {
                        instance.set_text(component.get_value());
                        instance.focus();

                        var input = o.querySelector('.asi-text');
                        input.setSelectionRange(input.value.length, input.value.length);
                    },
                    finish: function(event) {
                        event.preventDefault();
                        event.stopPropagation();

                        var value = component.get_value();
                        if (value.trim().length == 0) {
                            return false;
                        }

                        if (instance.tokens.process.value(value, false) !== false) {
                            component.cleanup();
                            instance.set_text('');

                            setTimeout(function() {
                                instance.events.input_emptied(event);
                            });
                        }

                        return false;
                    },
                    filter: function(value) {
                        var current = component.get_value().toLowerCase();
                        if (value.toLowerCase() !== current) {
                            component.set_value(value);
                        }
                    },
                    init: function() {
                        n.on(this.html.querySelector('.asi-helpbtn--cancel'), 'click', this.cancel);
                        n.on(this.html.querySelector('.asi-helpbtn--done'), 'click', this.finish);

                        if (do_date) {
                            this.date_picker = new Pikaday({
                                defaultDate: new Date(),
                                maxDate: new Date(),
                                onSelect: function(date) {
                                    component.html.setAttribute('data-date-val',
                                        component.date_picker.toString('MMM DD YYYY'));
                                    component.update();
                                }
                            });
                            this.html.setAttribute('data-date-val', this.date_picker.toString('MMM DD YYYY'));
                            this.html.querySelector('.asi-helpbody').appendChild(this.date_picker.el);
                        }

                        if (do_time) {
                            this.html.setAttribute('data-time-val', false);

                            this.time_picker = instance.helper._create_time_chooser(this.html, this.update);
                            this.html.querySelector('.asi-helpbody').appendChild(this.time_picker.el);
                        }
                        return this;
                    },
                };

                component.init();
                instance.helper.destroy_queue.push(component);
                instance.helper.filter_queue.push(component);

                return component.html;

            },
            _create_time_chooser: function(parent, update_function) {
                var html = n.gen('.asi-timechooser > \
                        (.asi-clock[data-hour="false"][data-minute="false"][data-period="pm"] > \
                            .asi-clock--hourhand + \
                            .asi-clock--minutehand + \
                            (.asi-innerclock.asi-innerclock--hour > \
                                .asi-clockopt[data-value="12"]["12"] + \
                                .asi-clockopt[data-value="1"]["1"] + \
                                .asi-clockopt[data-value="2"]["2"] + \
                                .asi-clockopt[data-value="3"]["3"] + \
                                .asi-clockopt[data-value="4"]["4"] + \
                                .asi-clockopt[data-value="5"]["5"] + \
                                .asi-clockopt[data-value="6"]["6"] + \
                                .asi-clockopt[data-value="7"]["7"] + \
                                .asi-clockopt[data-value="8"]["8"] + \
                                .asi-clockopt[data-value="9"]["9"] + \
                                .asi-clockopt[data-value="10"]["10"] + \
                                .asi-clockopt[data-value="11"]["11"] \
                            ) + \
                            (.asi-innerclock.asi-innerclock--minute > \
                                .asi-clockopt[data-value="00"]["0"] + \
                                .asi-clockopt[data-value="05"]["5"] + \
                                .asi-clockopt[data-value="10"]["10"] + \
                                .asi-clockopt[data-value="15"]["15"] + \
                                .asi-clockopt[data-value="20"]["20"] + \
                                .asi-clockopt[data-value="25"]["25"] + \
                                .asi-clockopt[data-value="30"]["30"] + \
                                .asi-clockopt[data-value="35"]["35"] + \
                                .asi-clockopt[data-value="40"]["40"] + \
                                .asi-clockopt[data-value="45"]["45"] + \
                                .asi-clockopt[data-value="50"]["50"] + \
                                .asi-clockopt[data-value="55"]["55"] \
                            ) ^ \
                        ) + \
                        (.asi-timeperiod > \
                            .asi-timeperiodopt.asi-timeperiodopt--am[data-value="am"]["am"] + \
                            .asi-timeperiodopt.asi-timeperiodopt--pm[data-value="pm"]["pm"].asi-timeperiodopt--selected \
                        ) \
                    ');

                var clock_el    = html.querySelector('.asi-clock');

                var hour_hand   = html.querySelector('.asi-clock--hourhand');
                var hour_opts   = html.querySelectorAll('.asi-innerclock--hour .asi-clockopt');

                var minute_hand = html.querySelector('.asi-clock--minutehand');
                var minute_opts = html.querySelectorAll('.asi-innerclock--minute .asi-clockopt');

                var period_opts = html.querySelectorAll('.asi-timeperiodopt');

                var no_update_function = false;

                function update_parent() {
                    if (!parent) return;

                    var value = clock_el.getAttribute('data-hour') + ':' +
                                clock_el.getAttribute('data-minute') + ' ' +
                                clock_el.getAttribute('data-period');

                    // "false" is the unassigned value (see gen above)
                    // if contains not yet assigned value, don't update parent
                    if (!contains(value, 'false')) {
                        parent.setAttribute('data-time-val', value);
                        if (!no_update_function) {
                            update_function(value);
                        }
                    }
                }

                n.on(hour_opts, 'click', function(event) {
                    n.addClass(clock_el, 'asi-clock--hourselected');
                    clock_el.setAttribute('data-hour', this.getAttribute('data-value'));

                    n.removeClass(hour_opts, 'asi-clockopt--selected');
                    n.addClass(this, 'asi-clockopt--selected');

                    var itemIndex   = Array.prototype.indexOf.call(hour_opts, this);
                    hour_hand.setAttribute('style', 'transform: translate(-50%) rotate('+(30.0*itemIndex)+'deg)');

                    update_parent();
                    return false;
                });

                n.on(minute_opts, 'click', function(event) {
                    n.addClass(clock_el, 'asi-clock--minuteselected');
                    clock_el.setAttribute('data-minute', this.getAttribute('data-value'));

                    n.removeClass(minute_opts, 'asi-clockopt--selected');
                    n.addClass(this, 'asi-clockopt--selected');

                    var itemIndex   = Array.prototype.indexOf.call(minute_opts, this);
                    minute_hand.setAttribute('style', 'transform: translate(-50%) rotate('+(30.0*itemIndex)+'deg)');

                    update_parent();
                    return false;
                });

                n.on(period_opts, 'click', function(event) {
                    clock_el.setAttribute('data-period', this.getAttribute('data-value'));

                    n.removeClass(period_opts, 'asi-timeperiodopt--selected');
                    n.addClass(this, 'asi-timeperiodopt--selected');

                    update_parent();
                    return false;
                });

                return {
                    el: html,
                    destroy: function() {
                        n.off(hour_opts);
                        n.off(minute_opts);
                        n.off(period_opts);
                        if (html.parentElement) {
                            n.remove(html);
                        }
                    },
                    setTime: function(hour, minute, period, no_update) {
                        hour   = '' + hour;
                        minute = '' + minute;
                        period = period.toLowerCase();

                        if (minute.length == 1) {
                            minute = '0'+minute;
                        }

                        if (period.length == 1) {
                            period = period+'m';
                        }

                        if (period != 'am' && period != 'pm') {
                            period = 'pm';
                        }

                        if (no_update) {
                            no_update_function = true;
                        }

                        var hour_opt   = html.querySelector('.asi-innerclock--hour [data-value="'+hour+'"]');
                        var minute_opt = html.querySelector('.asi-innerclock--minute [data-value="'+minute+'"]');
                        var period_opt = html.querySelector('.asi-timeperiodopt[data-value="'+period+'"]');

                        if (hour_opt) hour_opt.click();
                        if (minute_opt) minute_opt.click();
                        if (period_opt) period_opt.click();

                        no_update_function = false;
                    },
                    getTime: function() {
                        return {
                            hour: parseInt(clock_el.getAttribute('data-hour')),
                            minute: parseInt(clock_el.getAttribute('data-minute')),
                            period: clock_el.getAttribute('data-period'),
                        };
                    },
                };
            },
            token_searcher_exists: function() {
                return instance.helper.component_exists('token-searcher');
            },
            /* ------ TOKEN MODIFIERS */
            add_token_modifiers: function(token_label) {
                var enter = o.querySelector('.asi-helper');

                if (!instance.options || !instance.options.tokens) {
                    return instance.helper;
                }

                var token_data = instance.options.tokens[token_label];
                if (!token_data || !token_data.modifiers) {
                    return instance.helper;
                }

                var modifiers_list = is_array(token_data.modifiers) ? token_data.modifiers :
                                        Object.keys(token_data.modifiers),
                    items = [];

                forEach(modifiers_list, function(modifier) {
                    items.push({
                        html: '<span class="asi-helpitem--label">mod:</span> ' +
                              '<span class="asi-helpitem--placeholder">'+modifier+'</span>',
                        value: modifier,
                        click: function(event) {
                            instance.tokens.process.modifier(token_label, modifier, event);
                            instance.clear_text();
                            instance.focus();
                        }
                    });
                });

                return instance.helper.create_component('token-modifiers', 'Modifiers (optional)', items);
            },
            token_modifiers_exists: function(token_label) {
                return instance.helper.component_exists('token-modifiers');
            },
            /* ------ TOKEN DESCRIPTION */
            add_token_description: function(token_label, modifier) {
                var enter = o.querySelector('.asi-helper');

                if (!instance.options || !instance.options.tokens) {
                    return instance.helper;
                }

                var token_data = instance.options.tokens[token_label];
                var desc_html = token_data.desc_html;

                if (modifier && token_data.modifiers && token_data.modifiers[modifier]
                    && token_data.modifiers[modifier].desc_html){
                    desc_html = token_data.modifiers[modifier].desc_html;
                }

                if (!token_data || !desc_html) {
                    return instance.helper;
                }

                var section = n.gen1('section.asi--token-description[data-section=token-description]');
                if (is_string(desc_html)) {
                    var desc_el = document.createElement('div');
                    desc_el.setAttribute('class', 'asi-desc');
                    desc_el.innerHTML = desc_html;
                    section.appendChild(desc_el);
                } else {
                    section.appendChild(desc_html.cloneNode(true));
                }

                enter.appendChild(section);

                return instance.helper;
            },
            token_description_exists: function() {
                return instance.helper.component_exists('token-description');
            },
            /* ------ TOKEN DESCRIPTION */
            add_text: function(desc_html) {
                var enter = o.querySelector('.asi-helper');

                if (!desc_html) {
                    return instance.helper;
                }

                var section = n.gen1('section.asi--text[data-section=text]');
                if (is_string(desc_html)) {
                    var desc_el = document.createElement('div');
                    desc_el.setAttribute('class', 'asi-desc');
                    desc_el.innerHTML = desc_html;
                    section.appendChild(desc_el);
                } else {
                    section.appendChild(desc_html.cloneNode(true));
                }

                enter.appendChild(section);

                return instance.helper;
            },
            text_exists: function() {
                return instance.helper.component_exists('text');
            },
            /* ------ MAIN SEARCH OPTIONS */
            add_search_options: function() {
                var enter = o.querySelector('.asi-helper');

                if (!instance.options || !instance.options.tokens) {
                    return instance.helper;
                }

                var tokens = instance.options.tokens,
                    items = [];

                forEach(tokens, function(token_label) {
                    var html = '<span class="asi-helpitem--label">' + token_label + ':</span> ';

                    if (instance.options.tokens[token_label]['placeholder']) {
                        html += '<span class="asi-helpitem--placeholder">'+
                                instance.options.tokens[token_label]['placeholder']+'</span>';
                    }

                    items.push({html:html, value:token_label, click:function(event) {
                        instance.tokens.process.label(token_label, event);
                        instance.clear_text();
                        instance.focus();
                    }});
                });

                return instance.helper.create_component('search-options', 'Search Options', items);
            },
            search_options_exists: function() {
                return instance.helper.component_exists('search-options');
            },
        }
    };

    n.asi_list.push(instance);

    return instance;
};