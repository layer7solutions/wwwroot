#! /usr/local/bin/node
// "use strict";

var snudown = require('./snudown');
var md = snudown.getParser();

var md_wiki = (function() {
    var redditCallbacks = snudown.getRedditCallbacks();
    var rendererConfig = snudown.defaultRenderState();
    rendererConfig.flags = snudown.DEFAULT_WIKI_FLAGS;
    rendererConfig.html_element_whitelist = snudown.DEFAULT_HTML_ELEMENT_WHITELIST;
    rendererConfig.html_attr_whitelist = snudown.DEFAULT_HTML_ATTR_WHITELIST;

    return snudown.getParser({
        callbacks: redditCallbacks,
        context: rendererConfig
    });
})();

var fs = require('fs');

colors = {
	BLACK: '\033[30m',
	RED: '\033[31m',
	GREEN: '\033[32m',
	YELLOW: '\033[33m',
	BLUE: '\033[34m',
	MAGENTA: '\033[35m',
	CYAN: '\033[36m',
	WHITE: '\033[37m',
	RESET: '\033[0m'
}

cases = {
    '': '',
    'http://www.reddit.com':
        '<p><a href="http://www.reddit.com">http://www.reddit.com</a></p>\n',

    'http://www.reddit.com/a\x00b':
        '<p><a href="http://www.reddit.com/ab">http://www.reddit.com/ab</a></p>\n',

    'foo@example.com':
        '<p><a href="mailto:foo@example.com">foo@example.com</a></p>\n',

    '[foo](http://en.wikipedia.org/wiki/Link_(film\\))':
        '<p><a href="http://en.wikipedia.org/wiki/Link_(film)">foo</a></p>\n',

    '(http://tsfr.org)':
        '<p>(<a href="http://tsfr.org">http://tsfr.org</a>)</p>\n',

    '[A link with a /r/subreddit in it](/lol)':
        '<p><a href="https://www.reddit.com/lol">A link with a /r/subreddit in it</a></p>\n',

    '[A link with a http://www.url.com in it](/lol)':
        '<p><a href="https://www.reddit.com/lol">A link with a http://www.url.com in it</a></p>\n',

    '[Empty Link]()':
        '<p>[Empty Link]()</p>\n',

    'http://en.wikipedia.org/wiki/café_racer':
        '<p><a href="http://en.wikipedia.org/wiki/caf%C3%A9_racer">http://en.wikipedia.org/wiki/café_racer</a></p>\n',

    '#####################################################hi':
        '<h6>###############################################hi</h6>\n',

    '[foo](http://bar\nbar)':
        '<p><a href="http://bar%0Abar">foo</a></p>\n',

    '[foo](http://bar\nbar "lorem ipsum")':
        '<p><a href="http://bar%0Abar" title="lorem ipsum">foo</a></p>\n',

    '/r/test':
        '<p><a href="https://www.reddit.com/r/test">/r/test</a></p>\n',

    'Words words /r/test words':
        '<p>Words words <a href="https://www.reddit.com/r/test">/r/test</a> words</p>\n',

    '/r/':
        '<p>/r/</p>\n',

    'escaped \\/r/test':
        '<p>escaped /r/test</p>\n',

    'ampersands http://www.google.com?test&blah':
        '<p>ampersands <a href="http://www.google.com?test&amp;blah">http://www.google.com?test&amp;blah</a></p>\n',

    '[_regular_ link with nesting](/test)':
        '<p><a href="https://www.reddit.com/test"><em>regular</em> link with nesting</a></p>\n',

    ' www.a.co?with&test':
        '<p><a href="http://www.a.co?with&amp;test">www.a.co?with&amp;test</a></p>\n',

    'Normal^superscript':
        '<p>Normal<sup>superscript</sup></p>\n',

    'Escape\\^superscript':
        '<p>Escape^superscript</p>\n',

    '~~normal strikethrough~~':
        '<p><del>normal strikethrough</del></p>\n',

    '\\~~escaped strikethrough~~':
        '<p>~~escaped strikethrough~~</p>\n',

    'anywhere\x03, you':
        '<p>anywhere, you</p>\n',

    '[Test](//test)':
        '<p><a href="https://www.reddit.com//test">Test</a></p>\n',

    '[Test](//#test)':
        '<p><a href="https://www.reddit.com//#test">Test</a></p>\n',

    '[Test](#test)':
        '<p><a href="#test">Test</a></p>\n',

    '[Test](git://github.com)':
        '<p><a href="git://github.com">Test</a></p>\n',

    '[Speculation](//?)':
        '<p><a href="https://www.reddit.com//?">Speculation</a></p>\n',

    '/r/sr_with_underscores':
        '<p><a href="https://www.reddit.com/r/sr_with_underscores">/r/sr_with_underscores</a></p>\n',

    '[Test](///#test)':
        '<p><a href="https://www.reddit.com///#test">Test</a></p>\n',

    '/r/multireddit+test+yay':
        '<p><a href="https://www.reddit.com/r/multireddit+test+yay">/r/multireddit+test+yay</a></p>\n',

    '<test>':
        '<p>&lt;test&gt;</p>\n',

    'words_with_underscores':
        '<p>words_with_underscores</p>\n',

    'words*with*asterisks':
        '<p>words<em>with</em>asterisks</p>\n',

    '~test':
        '<p>~test</p>\n',

    '/u/test':
        '<p><a href="https://www.reddit.com/u/test">/u/test</a></p>\n',

    'blah \\':
        '<p>blah \\</p>\n',

    '/r/whatever: fork':
        '<p><a href="https://www.reddit.com/r/whatever">/r/whatever</a>: fork</p>\n',

    '/r/t:timereddit':
        '<p><a href="https://www.reddit.com/r/t:timereddit">/r/t:timereddit</a></p>\n',

    '/r/reddit.com':
        '<p><a href="https://www.reddit.com/r/reddit.com">/r/reddit.com</a></p>\n',

    '/r/not.cool':
        '<p><a href="https://www.reddit.com/r/not">/r/not</a>.cool</p>\n',

    '/r/very+clever+multireddit+reddit.com+t:fork+yay':
        '<p><a href="https://www.reddit.com/r/very+clever+multireddit+reddit.com+t:fork+yay">/r/very+clever+multireddit+reddit.com+t:fork+yay</a></p>\n',

    '/r/all-minus-something':
        '<p><a href="https://www.reddit.com/r/all-minus-something">/r/all-minus-something</a></p>\n',

    '/r/notall-minus':
        '<p><a href="https://www.reddit.com/r/notall">/r/notall</a>-minus</p>\n',

    '/r/t:heatdeathoftheuniverse':
        '<p><a href="https://www.reddit.com/r/t:heatdeathoftheuniverse">/r/t:heatdeathoftheuniverse</a></p>\n',

    'a /u/reddit':
        '<p>a <a href="https://www.reddit.com/u/reddit">/u/reddit</a></p>\n',

    'u/reddit':
        '<p><a href="https://www.reddit.com/u/reddit">u/reddit</a></p>\n',

    'a u/reddit':
        '<p>a <a href="https://www.reddit.com/u/reddit">u/reddit</a></p>\n',

    'a u/reddit/foobaz':
        '<p>a <a href="https://www.reddit.com/u/reddit/foobaz">u/reddit/foobaz</a></p>\n',

    'foo:u/reddit':
        '<p>foo:<a href="https://www.reddit.com/u/reddit">u/reddit</a></p>\n',

    'fuu/reddit':
        '<p>fuu/reddit</p>\n',

    // Don't treat unicode punctuation as a word boundary for now
    'a。u/reddit':
        '<p>a。u/reddit</p>\n',

    '\\/u/me':
        '<p>/u/me</p>\n',

    '\\\\/u/me':
        '<p>\\<a href="https://www.reddit.com/u/me">/u/me</a></p>\n',

    '\\u/me':
        '<p>\\<a href="https://www.reddit.com/u/me">u/me</a></p>\n',

    '\\\\u/me':
        '<p>\\<a href="https://www.reddit.com/u/me">u/me</a></p>\n',

    'u\\/me':
        '<p>u/me</p>\n',

    '*u/me*':
        '<p><em><a href="https://www.reddit.com/u/me">u/me</a></em></p>\n',

    'foo^u/me':
        '<p>foo<sup><a href="https://www.reddit.com/u/me">u/me</a></sup></p>\n',

    '*foo*u/me':
        '<p><em>foo</em><a href="https://www.reddit.com/u/me">u/me</a></p>\n',

    'u/me':
        '<p><a href="https://www.reddit.com/u/me">u/me</a></p>\n',

    '/u/me':
        '<p><a href="https://www.reddit.com/u/me">/u/me</a></p>\n',

    'u/m':
        '<p>u/m</p>\n',

    '/u/m':
        '<p>/u/m</p>\n',

    '/f/oobar':
        '<p>/f/oobar</p>\n',

    'f/oobar':
        '<p>f/oobar</p>\n',

    'a /r/reddit.com':
        '<p>a <a href="https://www.reddit.com/r/reddit.com">/r/reddit.com</a></p>\n',

    'a r/reddit.com':
        '<p>a <a href="https://www.reddit.com/r/reddit.com">r/reddit.com</a></p>\n',

    'foo:r/reddit.com':
        '<p>foo:<a href="https://www.reddit.com/r/reddit.com">r/reddit.com</a></p>\n',

    'foobar/reddit.com':
        '<p>foobar/reddit.com</p>\n',

    'a。r/reddit.com':
        '<p>a。r/reddit.com</p>\n',

    '/R/reddit.com':
        '<p>/R/reddit.com</p>\n',

    '/r/irc://foo.bar/':
        '<p><a href="https://www.reddit.com/r/irc">/r/irc</a>://foo.bar/</p>\n',

    '/r/t:irc//foo.bar/':
        '<p><a href="https://www.reddit.com/r/t:irc//foo">/r/t:irc//foo</a>.bar/</p>\n',

    '/r/all-irc://foo.bar/':
        '<p><a href="https://www.reddit.com/r/all-irc">/r/all-irc</a>://foo.bar/</p>\n',

    '/r/foo+irc://foo.bar/':
        '<p><a href="https://www.reddit.com/r/foo+irc">/r/foo+irc</a>://foo.bar/</p>\n',

    '/r/www.example.com':
        '<p><a href="https://www.reddit.com/r/www">/r/www</a>.example.com</p>\n',

    '.http://reddit.com':
        '<p>.<a href="http://reddit.com">http://reddit.com</a></p>\n',

    '[r://<http://reddit.com/>](/aa)':
        '<p><a href="https://www.reddit.com/aa">r://<a href="http://reddit.com/">http://reddit.com/</a></a></p>\n',

    '/u/http://www.reddit.com/user/reddit':
        '<p><a href="https://www.reddit.com/u/http">/u/http</a>://<a href="http://www.reddit.com/user/reddit">www.reddit.com/user/reddit</a></p>\n',

    'www.http://example.com/':
        '<p><a href="http://www.http://example.com/">www.http://example.com/</a></p>\n',

    '&thetasym;': '<p>&thetasym;</p>\n',
    '&foobar;': '<p>&amp;foobar;</p>\n',
    '&nbsp': '<p>&amp;nbsp</p>\n',
    '&#foobar;': '<p>&amp;#foobar;</p>\n',
    '&#xfoobar;': '<p>&amp;#xfoobar;</p>\n',
    '&#9999999999;': '<p>&amp;#9999999999;</p>\n',
    '&#99;': '<p>&#99;</p>\n',
    '&#x7E;': '<p>&#x7E;</p>\n',
    '&#X7E;': '<p>&#x7E;</p>\n',
    '&frac12;': '<p>&frac12;</p>\n',
    'aaa&frac12;aaa': '<p>aaa&frac12;aaa</p>\n',
    '&': '<p>&amp;</p>\n',
    '&;': '<p>&amp;;</p>\n',
    '&#;': '<p>&amp;#;</p>\n',
    '&#x;': '<p>&amp;#x;</p>\n',

    '> quotey mcquoteface':
        '<blockquote>\n<p>quotey mcquoteface</p>\n</blockquote>\n',

    '> quotey mcquoteface\nnew line of text what happens?':
        '<blockquote>\n<p>quotey mcquoteface\nnew line of text what happens?</p>\n</blockquote>\n',

    '> quotey mcquoteface\n\ntwo new lines then text what happens?':
        '<blockquote>\n<p>quotey mcquoteface</p>\n</blockquote>\n\n<p>two new lines then text what happens?</p>\n',

    '> quotey mcquoteface\n> more quotey':
        '<blockquote>\n<p>quotey mcquoteface\nmore quotey</p>\n</blockquote>\n',

    '> quotey macquoteface\n\n> another quotey':
        '<blockquote>\n<p>quotey macquoteface</p>\n\n<p>another quotey</p>\n</blockquote>\n',

    '>! spoily mcspoilerface':
        '<blockquote class="md-spoiler-text">\n<p>spoily mcspoilerface</p>\n</blockquote>\n',

    '>! spoily mcspoilerface\nmore spoilage goes here':
        '<blockquote class="md-spoiler-text">\n<p>spoily mcspoilerface\nmore spoilage goes here</p>\n</blockquote>\n',

    '>! spoily mcspoilerface > incorrect quote syntax':
        '<blockquote class="md-spoiler-text">\n<p>spoily mcspoilerface &gt; incorrect quote syntax</p>\n</blockquote>\n',

    '>! spoily mcspoilerface\n\n':
        '<blockquote class="md-spoiler-text">\n<p>spoily mcspoilerface</p>\n</blockquote>\n',

    '>! spoily mcspoilerface\n\nnormal text here':
        '<blockquote class="md-spoiler-text">\n<p>spoily mcspoilerface</p>\n</blockquote>\n\n<p>normal text here</p>\n',

    '>! spoily mcspoilerface\n>! blockspoiler continuation':
        '<blockquote class="md-spoiler-text">\n<p>spoily mcspoilerface\nblockspoiler continuation</p>\n</blockquote>\n',

    '>! spoily mcspoilerface\n> quotey mcquoteface':
        '<blockquote class="md-spoiler-text">\n<p>spoily mcspoilerface</p>\n\n<blockquote>\n<p>quotey mcquoteface</p>\n</blockquote>\n</blockquote>\n',

    '>! spoiler p1\n>!\n>! spoiler p2\n>! spoiler p3':
        '<blockquote class="md-spoiler-text">\n<p>spoiler p1</p>\n\n<p>spoiler p2\nspoiler p3</p>\n</blockquote>\n',

    '>>! spoiler p1\n>!\n>! spoiler p2\n>! spoiler p3':
        '<blockquote>\n<blockquote class="md-spoiler-text">\n<p>spoiler p1</p>\n\n<p>spoiler p2\nspoiler p3</p>\n</blockquote>\n</blockquote>\n',

    '>>! spoiler p1\n>!\n>! spoiler p2\n\nnew text':
        '<blockquote>\n<blockquote class="md-spoiler-text">\n<p>spoiler p1</p>\n\n<p>spoiler p2</p>\n</blockquote>\n</blockquote>\n\n<p>new text</p>\n',

    '>>! spoiler p1\n>!\n>! spoiler p2\n\n>! new blockspoiler':
        '<blockquote>\n<blockquote class="md-spoiler-text">\n<p>spoiler p1</p>\n\n<p>spoiler p2</p>\n</blockquote>\n</blockquote>\n\n<blockquote class="md-spoiler-text">\n<p>new blockspoiler</p>\n</blockquote>\n',

    '! this is not a spoiler':
        '<p>! this is not a spoiler</p>\n',

    '>!\nTesting':
        '<blockquote class="md-spoiler-text">\n<p>Testing</p>\n</blockquote>\n',

    '>!\n\nTesting':
        '<blockquote class="md-spoiler-text">\n</blockquote>\n\n<p>Testing</p>\n',

    '>!':
        '<blockquote class="md-spoiler-text">\n</blockquote>\n',

    '>!\n>!':
        '<blockquote class="md-spoiler-text">\n</blockquote>\n',

    '>':
        '<blockquote>\n</blockquote>\n',

    '> some quote goes here\n>':
        '<blockquote>\n<p>some quote goes here</p>\n</blockquote>\n',

    'This is an >!inline spoiler!< sentence.':
        '<p>This is an <span class="md-spoiler-text">inline spoiler</span> sentence.</p>\n',

    '>!Inline spoiler!< starting the sentence':
        '<p><span class="md-spoiler-text">Inline spoiler</span> starting the sentence</p>\n',

    'Inline >!spoiler with *emphasis*!< test':
        '<p>Inline <span class="md-spoiler-text">spoiler with <em>emphasis</em></span> test</p>\n',

    '>! This is an illegal blockspoiler >!with an inline spoiler!<':
        '<p>&gt;! This is an illegal blockspoiler <span class="md-spoiler-text">with an inline spoiler</span></p>\n',

    'This is an >!inline spoiler with some >!additional!< text!<':
        '<p>This is an <span class="md-spoiler-text">inline spoiler with some &gt;!additional</span> text!&lt;</p>\n'
}

function repeat(str, n) {
    var out = '';
    for (var i = 0; i < n; i++) {
        out += str;
    }
}

cases[repeat('|', 5) + '\n' + repeat('-|', 5) + '\n|\n'] = '<table><thead>\n<tr>\n' + repeat('<th></th>\n', 4) + '</tr>\n</thead><tbody>\n<tr>\n<td colspan="4" ></td>\n</tr>\n</tbody></table>\n';

cases[repeat('|', 2) + '\n' + repeat('-|', 2) + '\n|\n'] = '<table><thead>\n<tr>\n' + repeat('<th></th>\n', 1) + '</tr>\n</thead><tbody>\n<tr>\n<td></td>\n</tr>\n</tbody></table>\n';

cases[repeat('|', 65) + '\n' + repeat('-|', 65) + '\n|\n'] = '<table><thead>\n<tr>\n' + repeat('<th></th>\n', 64) + '</tr>\n</thead><tbody>\n<tr>\n<td colspan="64" ></td>\n</tr>\n</tbody></table>\n';

cases[repeat('|', 66) + '\n' + repeat('-|', 66) + '\n|\n'] = '<p>' + repeat('|', 66) + '\n' + repeat('-|', 66) + '\n|' + '</p>\n';


function *range(from ,to) {
    for (var i = from; i < to; i++) yield i;
}
// Test that every illegal numeric entity is encoded as it should be.
var ILLEGAL_NUMERIC_ENT_RANGES = [
    range(0, 9),
    range(11, 13),
    range(14, 32),
    range(55296, 57344),
    range(65534, 65536),
]

var invalid_ent_test_key = '';
var invalid_ent_test_val = '';
for (var r of ILLEGAL_NUMERIC_ENT_RANGES) {
    for (var i of r) {
        invalid_ent_test_key += '&#' + i + ';';
        invalid_ent_test_val += '&amp;#' + i + ';';
    }
}
// for i in itertools.chain(*ILLEGAL_NUMERIC_ENT_RANGES):

cases[invalid_ent_test_key] = '<p>' + invalid_ent_test_val + '</p>\n';






wiki_cases = {
    '<table scope="foo"bar>':
        '<p><table scope="foo"></p>\n',

    '<table scope="foo"bar colspan="2">':
        '<p><table scope="foo" colspan="2"></p>\n',

    '<table scope="foo" colspan="2"bar>':
        '<p><table scope="foo" colspan="2"></p>\n',

    '<table scope="foo">':
        '<p><table scope="foo"></p>\n',

    '<table scop="foo">':
        '<p><table></p>\n',

    '<table ff= scope="foo">':
        '<p><table scope="foo"></p>\n',

    '<table colspan= scope="foo">':
        '<p><table scope="foo"></p>\n',

    '<table scope=ff"foo">':
        '<p><table scope="foo"></p>\n',

    '<table scope="foo" test="test">':
        '<p><table scope="foo"></p>\n',

    '<table scope="foo" longervalue="testing test" scope="test">':
        '<p><table scope="foo" scope="test"></p>\n',

    '<table scope=`"foo">':
        '<p><table scope="foo"></p>\n',

    '<table scope="foo bar">':
        '<p><table scope="foo bar"></p>\n',

    '<table scope=\'foo colspan="foo">':
        '<p><table></p>\n',

    '<table scope=\'foo\' colspan="foo">':
        '<p><table scope="foo" colspan="foo"></p>\n',

    '<table scope=>':
        '<p><table></p>\n',

    '<table scope= colspan="test" scope=>':
        '<p><table colspan="test"></p>\n',

    '<table colspan="\'test">':
        '<p><table colspan="&#39;test"></p>\n',

    '<table scope="foo" colspan="2">':
        '<p><table scope="foo" colspan="2"></p>\n',

    '<table scope="foo" colspan="2" ff="test">':
        '<p><table scope="foo" colspan="2"></p>\n',

    '<table ff="test" scope="foo" colspan="2" colspan=>':
        '<p><table scope="foo" colspan="2"></p>\n',

    ' <table colspan=\'\'\' a="" \' scope="foo">':
        '<p><table scope="foo"></p>\n',
}



console.log("Running Snudown test cases.");
var showSuccesses = false
for (var text in cases) {

	var result = md.render(text);
	if (cases[text] == result) {
		if (showSuccesses) {
			console.log(text);
			console.log(cases[text]);
			console.log(result);
			console.log();
		}
	} else {
		console.log(colors.RED, text, colors.RESET);
		console.log(cases[text]);
		console.log(result);
		console.log();
	}
}


console.log("Running Snudown test cases for the wiki format.");
for (var text in wiki_cases) {

    var result = md_wiki.render(text);
    if (wiki_cases[text] == result) {
        if (showSuccesses) {
            console.log(text);
            console.log(wiki_cases[text]);
            console.log(result);
            console.log();
        }
    } else {
        console.log(colors.RED, text, colors.RESET);
        // console.log(wiki_cases[text]);
        console.log(colors.GREEN, wiki_cases[text], colors.RESET);
        console.log(result);
        console.log();
    }
}


try {
    var htmlutil = require('html-util');
} catch (e) {
    console.error('Could not load module "html-util", module must be installed to run tests.');
    return 1;
}

console.log('Retrieving test data from /r/all');
const request = require('request');
request({
    url: "https://www.reddit.com/r/all.json",
    headers: {
        'User-Agent': 'snudown.js Test Suite'
    }
}, function(error, response, body) {
    var data = JSON.parse(body);

    console.assert(data != null);

    var list = [];
    for (var i = 0; i < data.data.children.length; i++) {
        list.push(data.data.children[i]);
    }

    var fullTable = {};
    function scan_page_loop(list) {
        var obj = list.shift();
        console.log('GET[' + (25-list.length) + '/25]', obj.data.permalink);
        request({
            url: 'https://www.reddit.com' + obj.data.permalink+'.json',
            headers: {
                'User-Agent': 'snudown.js Test Suite'
            }
        }, function(error, response, body) {
            if (response.statusCode != 200) {
                console.log(response.statusCode);
                return;
            }

            var data = JSON.parse(body);
            parseListing(data[0], fullTable);
            parseListing(data[1], fullTable);
            if (list.length > 0) {
                setTimeout(scan_page_loop, 2000, list);
            } else {
                finishRun(fullTable);
            }
        });

    }
    setTimeout(scan_page_loop, 100, list);
});

function parseListing(listing, table) {
    if (listing == null || listing.kind !== 'Listing') return;
    if (listing != null && listing.data != null && listing.data.children != null) {
        for (var i = 0; i < listing.data.children.length; i++) {
            parseComment(listing.data.children[i], table);
        }
    }
}

function parseComment(comment, table) {
    if (comment.kind === 'more') return;
    if (comment.kind === 't1') {
        table[comment.data.name] = {
            markdown: comment.data.body,
            html: htmlutil.unescapeEntities(comment.data.body_html).slice(16, -6),
            link: comment.data.link_id.match(/^t3_([a-z0-9]+)$/)[1]
        };
    } else if (comment.kind === 't3' && comment.data.is_self === true && comment.data.selftext.length > 0) {

        table[comment.data.name] = {
            markdown: comment.data.selftext,
            html: htmlutil.unescapeEntities(comment.data.selftext_html).slice(31, -20)
        }
    }
    if (comment.data.replies) {
        parseListing(comment.data.replies, table);
    }
}

function calcURL(id, entry) {
    var parts = id.match(/^(t[13])_([a-z0-9]+)$/);
    if (parts[1] === 't3') {
        return 'http://www.reddit.com/comments/' + parts[2] + '/';
    } else if (parts[1] === 't1') {
        return 'http://www.reddit.com/comments/' + entry.link + '/_/' + parts[2] + '/';
    }
}

function checkOutput(id, entry) {
    var rendered = snudown.getParser().render(htmlutil.unescapeEntities(entry.markdown));
    var html = entry.html.replace(/href=("|')\//g, 'href=$1https://www.reddit.com/');
    if (html !== rendered) {
        console.log(colors.RED, "RENDER ERROR", colors.RESET, calcURL(id, entry));
        console.log("MARKDOWN:");
        console.log(entry.markdown);
        console.log(JSON.stringify(entry.markdown));
        console.log("REDDIT:");
        console.log(colors.GREEN, html, colors.RESET);
        console.log(JSON.stringify(html));
        console.log("snudown:");
        console.log(colors.RED, rendered, colors.RESET);
        console.log(JSON.stringify(rendered));
        console.log();
    }
}

function finishRun(newTable) {
    console.log('Testing against new data.');
    // console.log(newTable);
    for (var id in newTable) {
        // console.log(id);
        checkOutput(id, newTable[id]);
    }
    if (fs.existsSync('./snudown_testData.json')) {
        console.log('Testing against old data.');
        var data = fs.readFileSync('snudown_testData.json', 'utf-8');
        var oldTable = JSON.parse(data);
        for (var id in oldTable) {
            checkOutput(id, oldTable[id]);
        }
        console.log('Merging data.');
        for (var id in newTable) {
            oldTable[id] = newTable[id];
        }
        console.log('Saving.');
        fs.writeFileSync('snudown_testData.json', JSON.stringify(oldTable), 'utf-8');
    } else {
        console.log('Saving.');
        fs.writeFileSync('snudown_testData.json', JSON.stringify(newTable), 'utf-8');
    }
}