init('Docs', function() {
    var pre_list = document.querySelectorAll('pre');

    for (var i = 0; i < pre_list.length; i++) {
        var pre = pre_list[i];

        var lines = pre.textContent.split(/\r\n|\r|\n/g),
            count = lines.length;
        if (count === 0) {
            continue;
        }

        var slice = lines[0].match(/^\s*/)[0].length;
        var output = '';

        for (var j = 0; j < count; j++) {
            var line = lines[j];
            output += line.slice(slice);
            if (j !== count-1) {
                output += "\n";
            }
        }

        pre.textContent = output;
    }

    // Get first h1 and set title to its contents (if it exists)
    var h1 = document.querySelector('article header h1');
    if (h1) {
        if (h1.hasAttribute('data-doctitle')) {
            document.title = h1.getAttribute('data-doctitle');
        } else {
            document.title = h1.textContent + ' | Docs';
        }
    }
});

