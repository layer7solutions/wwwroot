const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const virtualConsole = new jsdom.VirtualConsole();
virtualConsole.sendTo(console);

global.app = {};
global.dom = new JSDOM('<html><body></body></html>', { virtualConsole });
global.window = dom.window;
global.document = window.document;
global.Storage = {prototype:{}};
global.SITE_IS_STAGING = false;

global.assert = require('better-assert');

global.eval(require('fs').readFileSync(require('path').join(__dirname, '../../ProductionClient.js'), 'utf8'));