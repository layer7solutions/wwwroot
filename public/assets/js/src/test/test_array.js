require('./constants_nodejs');

// is_array
assert( is_array([]) );
assert( is_array([true]) );
assert( is_array(null) === false );
assert( is_array({}) === false );
assert( is_array({0: true, length: 1}) === false );
assert( is_array({0: true, 1: false, length: 2}) === false);

// is_array_like
assert( is_array_like([]) );
assert( is_array_like([true]) );
assert( is_array_like({0: true, length: 1}) );
assert( is_array_like({0: true, 1: false, length: 2}) );

// array_equals
assert( array_equals([], []) === true );
assert( array_equals([1,2,3], [1,2,3]) === true );
assert( array_equals([1,2,3], [1,3,2]) === false );

// to_array
assert( is_array(to_array({0: true, 1: false, length: 2})) );
assert( array_equals(to_array({0: true, 1: false, length: 2}), [true, false]) );

// in_array
var ex0 = [1,2,3,'a','b','c',null];
var ex1 = {length: 3, '0': 'a', '1': 'b', '2': 'c'};
assert( in_array(ex0, null) );
assert( in_array(ex0, 1) );
assert( in_array(ex0, 2) );
assert( in_array(ex0, 3) );
assert( in_array(ex0, 'a') );
assert( in_array(ex0, 'b') );
assert( in_array(ex0, 'c') );
assert( in_array(ex0, '1') === false );
assert( in_array(ex0, 4) === false );
assert( in_array(ex0, 'd') === false );
assert( in_array([], 1) === false );
assert( in_array(null, 1) === false );
assert( in_array(undefined, 1) === false );
assert( in_array(ex1, 'a') === true );
assert( in_array(ex1, 'b') === true );
assert( in_array(ex1, 'c') === true );
assert( in_array(ex1, 'd') === false );
// any
assert( any([1]) );
assert( any([]) === false );
assert( any([1], is_int) );
assert( any([''], is_int) === false );
assert( any(['', 1], is_int) );
assert( any([1, ''], is_int) );
assert( any([null]) === false );
assert( any([null, undefined]) === false );
assert( any([null, undefined, 1]) === true );
assert( any([null], is_null) === true );

// every
assert( every([]) === false );
assert( every([1]) === true );
assert( every([null, 1]) === false );
assert( every([1, null]) === false );
assert( every([1, 1]) === true );
assert( every([1, null], is_null) === false );
assert( every([null, null], is_null) === true );
assert( every([null, undefined, '', ' ', 0, false], empty) === true );
assert( every([null, undefined, '', ' ', 0, 1], empty) === false );

// first
assert( first([1,2,3]) === 1 );
assert( first([null,1,2,3]) === null );
assert( first([null,1,2,3], isset) === 1 );
assert( first([undefined,null,1,2,3], isset) === 1 );
assert( first([0,undefined,null,1,2,3], isset) === 0 );

// last
assert( last([1,2,3]) === 3 );
assert( last([1,2,3,null]) === null );
assert( last([1,2,3,null], isset) === 3 );
assert( last([1,2,3,null,undefined], isset) === 3 );
assert( last([1,2,3,null,undefined,4], isset) === 4 );

// intersect
assert(array_equals( intersect([1,2,3,4,5,6], [4,5,6,7,8,9]) , [4,5,6]));
assert(array_equals( intersect([1,2,3,4,5,6], [7,8,9]) , []));
assert(array_equals( intersect([3,4,5], [1,2,3]) , [3]));

// diff
assert(array_equals( diff([1,2,3,4,5,6], [4,5,6,7,8,9]) , [1,2,3]));
assert(array_equals( diff([1,2,3,4,5,6], []) , [1,2,3,4,5,6]));
assert(array_equals( diff([4,5,6,7,8,9], [1,2,3,4,5,6]) , [7,8,9]));
assert(array_equals( diff([], []) , []));
assert(array_equals( diff([], [1,2,3]) , []));

// union
assert(array_equals( union([1,2,3,4,5,6], [4,5,6,7,8,9]) , [1,2,3,4,5,6,7,8,9]));
assert(array_equals( union([1,2,3,4,5,6], []) , [1,2,3,4,5,6]));
assert(array_equals( union([], [1,2,3,4,5,6]) , [1,2,3,4,5,6]));
assert(array_equals( union([1,2,3,4,5,6], [1,2,3,4,5,6]) , [1,2,3,4,5,6]));

// reduce
var reduce_test_arr = [1,2,3,4,5,6];
var reduce_test_idx = 0;
var reduce_test_result =
    reduce(reduce_test_arr, function(acc, el, i) {
        assert( i === (reduce_test_idx++) );
        assert( el === reduce_test_arr[i] );

        if (i == 0) {
            assert(acc === 0);
        }

        return acc + el;
    }, 0);
assert( reduce_test_result === (1+2+3+4+5+6) );
assert( reduce([1, 2, 3], function(acc, num){ return acc + num; }, 0) === 6 );
assert( reduce([1, 2, 3], function(acc, num){ return acc + num; }) === 6 );
assert( reduce([1, 2, 3], function(acc, num){ return acc + num; }, 1) === 7 );
assert( is_undefined( reduce([], function() {}) ) );
assert( reduce([], function() {}, 6) === 6 );
assert( reduce(null, function() {}, 6) === 6 );

// flatten
assert(array_equals( flatten([]) , [] ));
assert(array_equals( flatten([],[[]]) , [] ));
assert(array_equals( flatten([],[[[1]]]) , [1] ));
assert(array_equals( flatten([1,2,3]) , [1,2,3] ));
assert(array_equals( flatten([[1],[2]],[3]) , [1,2,3] ));
assert(array_equals( flatten([[1,2],3]) , [1,2,3] ));
assert(array_equals( flatten(0, [[1,[2,[]]],3], [], [4,[5,[6]], []], 7) , [0,1,2,3,4,5,6,7] ));

console.log('test_array : done');