function append_result(text) {
    var el = document.createElement('div');
    el.innerHTML = text;
    document.body.appendChild(el);
    console.log(el.innerText);
}

function separator() {
    var el = document.createElement('hr');
    document.body.appendChild(el);
    console.log('----------------------------------------------------------------------');
}

function stringify(o) {
    if (n.isElement(o)) {
        return element_to_string(o);
    } else {
        return JSON.stringify(o);
    }
}

function assertFalse(a, b, fn) {
    assert(a, b, function(a,b) {
        if (fn) {
            return fn(a,b) === false;
        }
        if (typeof b === 'undefined') {
            return a === false;
        }
        return a !== b;
    }, fn && fn.name ? '!'+fn.name : '!=');
}


function assertTrue(a, b, fn) {
    assert(a, b, function(a,b) {
        if (fn) {
            return fn(a,b) === true;
        }
        if (typeof b === 'undefined') {
            return a === true;
        }
        return a === b;
    }, fn && fn.name ? '!'+fn.name : '!=');
}

function assert(a, b, fn, fn_name) {
    var r;

    if (typeof fn === 'function') {
        r = fn(a,b);
    } else {
        r = a === b;
    }

    fn_name = fn_name || fn.name || '==';

    var output = '';
    if (r) {
        output += '<strong style="color:green"><code>&nbsp; OK</code></strong><code> : </code>';
    } else {
        output += '<strong style="color:red"><code>FAIL</code></strong><code> : </code>';
    }

    output += '<code>';
    if (!contains(fn_name, '=')) {
        output += fn_name+' '+stringify(a)+' '+stringify(b);
    } else {
        output += stringify(a)+' '+fn_name+' '+stringify(b);
    }
    output += '</code>';

    console.log(output);
    append_result(output);
}

var pad_cache = [
  '',
  ' ',
  '  ',
  '   ',
  '    ',
  '     ',
  '      ',
  '       ',
  '        ',
  '         '
];

function pad(ch, len) {
    if (len <= 0) return '';
    if (ch === ' ' && len < 10) return pad_cache[len];
    var pad = '';
    while (true) {
        // noinspection JSBitwiseOperatorUsage
        if (len & 1) pad += ch;
        len >>= 1;
        if (len) ch += ch;
        else break;
    }
    return pad;
}

function element_to_string(el) {
    var str = '<' + el.tagName.toLowerCase() + ' ';

    for (var i = 0; i < el.attributes.length; i++) {
        var attrib = el.attributes[i];
        if (attrib.specified) {
            str += attrib.name + '="' + attrib.value + '" ';
        }
    }

    return str.trim() + '>';
}

function test_element(el, tagName, expected_attributes, child_verifies, is_child) {
    var result = true;

    is_child = is_child || 0;

    if (tagName.toUpperCase() != el.tagName) {
        result = 'expected ' + tagName.toUpperCase() + ' tag, got ' + el.tagName;
    }
    else
    for (var key in expected_attributes) {
        var value = expected_attributes[key];
        if (value === false && el.hasAttribute(key)) {
            result = 'expected to not have "' + key + '" attribute.';
            break;
        }
        if (!el.hasAttribute(key)) {
            result = 'expected to have "'+key+'" attribute.';
            result = false;
            break;
        }
        if (value !== true && el.getAttribute(key) !== value) {
            result = 'expected "'+key+'" attribute to be `'
                +String(value)+'` but got `'+el.getAttribute(key)+'`';
            break;
        }
    }

    if (child_verifies && is_int(child_verifies)) {
        if (el.children.length < child_verifies) {
            result = 'expected ' + child_verifies + ' children, but got ' + el.children.length;
        }
    } else if (child_verifies && child_verifies.length) {
        if (el.children.length < child_verifies.length) {
            result = 'expected ' + child_verifies.length + ' children, but got ' + el.children.length;
        } else
        forEach(child_verifies, function(verify_data, idx) {
            test_element(el.children[idx], verify_data[0], verify_data[1], verify_data[2], is_child + 1);
        });
    }

    var output = pad(is_child);

    if (result === true) {
        output += '<strong style="color:green"><code>&nbsp; OK</code></strong><code> : </code>';
        output += '<code>' + escape_html(element_to_string(el)) + '</code> ';
    } else {
        output += '<strong style="color:red"><code>FAIL</code></strong><code> : </code>';
        output += '<code>' + escape_html(element_to_string(el)) + '</code> ';
        output += result;
    }

    append_result(output);
}