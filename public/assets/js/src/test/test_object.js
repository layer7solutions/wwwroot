require('./constants_nodejs');

var ex0, ex1, ex2, ex3, ex4, ex5, ans;

// is_object
assert( is_object({}) === true);
assert( is_object(null) === false);
assert( is_object(undefined) === false);
assert( is_object(0) === false);
assert( is_object('') === false);

// empty
assert( empty(false) );
assert( empty(0) );
assert( empty('') );
assert( empty([]) );
assert( empty({}) );
assert( empty('  ') );
assert( empty({length:0}));
assert( empty(null) );
assert( empty(undefined) );

// isset
assert( isset(false) === true );
assert( isset(0) === true );
assert( isset('') === true );
assert( isset([]) === true );
assert( isset({}) === true );
assert( isset(null) === false );
assert( isset(undefined) === false );

// not_set
assert( not_set(false) === false );
assert( not_set(0) === false );
assert( not_set('') === false );
assert( not_set([]) === false );
assert( not_set({}) === false );
assert( not_set(null) === true );
assert( not_set(undefined) === true );

// forEach
ex0 = [1,2,3];
ex1 = {0: 1, 1: 2, 2: 3, length: 3};
ex2 = {one: 1, two: 2, three: 3};

forEach(ex0, function(num, idx, len) {
    assert(num === idx + 1);
    assert(len === 3);
});

ans = [];
forEach(ex0, function(num, idx) {
    ans.push(num);
    if (idx == 1) {
        return false; // break foreach
    }
});
assert( array_equals(ans, [1,2]) );

ans = [];
forEach(ex0, function(num){ ans.push(num); });
assert( array_equals(ans, [1,2,3]) );

ans = [];
forEach(ex1, function(num){ ans.push(num); });
assert( array_equals(ans, [1,2,3]) );

ans = [];
forEach(ex2, function(key, value){ ans.push(value); });
assert( array_equals(ans, [1,2,3]) );

ans = [];
forEach(ex2, function(key, value){ ans.push(key); });
assert( array_equals(ans, ['one', 'two', 'three']) );

// the functions shouldn't run at all for the next 4, so we assert false
forEach(false, function() { assert(false); });
forEach(null, function() { assert(false); });
forEach(undefined, function() { assert(false); });
forEach([], function() { assert(false); });

// map
ans = map(ex0, function(value) { return value * 2; });
assert( array_equals(ans, [2,4,6]) );

ans = map(ex1, function(value) { return value * 2; });
assert( array_equals(ans, [2,4,6]) );

ans = map(ex2, function(key,value) { return value * 2; });
assert( array_equals(Object.values(ans), [2,4,6]) );

ans = map(4, function(value) { return value * 2; });
assert( ans === 8);

ans = map([4], function(value) { return value * 2; });
assert( ans[0] === 8);

// filter
assert(array_equals( filter([]) , [] ));
assert(array_equals( filter([null]) , [] ));
assert(array_equals( filter([1,2,3,false,null,undefined]) , [1,2,3,false] ));
assert(array_equals( filter([1,2,3,true,false], is_bool) , [true,false] ));
assert(array_equals( filter([1,2,3,true,false], is_int) , [1,2,3] ));
assert(array_equals( filter([1,2,3], is_bool) , [] ));
assert(array_equals( filter([null], empty) , [null] ));
assert(array_equals( filter([null,1,2,3], empty) , [null] ));

// find
assert( find([null,1,2,3], 2) === 2 );
assert( find([null,1,2,3]) === 1 );
assert( find([null,1,2,3], isset) === 1 );
assert( find([true,false,1,2,3], isset) === true );
assert( find([true,false,1,2,3], is_int) === 1 );
assert( find([true,false,1,3,4], function(arg) {
    return is_int(arg) && arg % 2 == 0; // find first even integer
}) === 4 );

assert(is_undef( find([null,1,2,3], 5) ));
assert(is_undef( find([null,null], isset) ));

// contains
ex0 = [1,2,3];
ex1 = {foo: 'bar'};
ex2 = "hello world";

assert( contains([], 1) === false );
assert( contains(null, 1) === false );
assert( contains({}, 1) === false );
assert( contains(undefined, 1) === false );

assert( contains(ex0, 1) );
assert( contains(ex0, 2) );
assert( contains(ex0, 3) );
assert( !contains(ex0, 4) );

assert( contains(ex1, 'foo') );
assert( !contains(ex1, 'bar') ); // contains only checks keys for objects

assert( contains(ex2, "hello") );
assert( contains(ex2, "hello world") );
assert( contains(ex2, "llo") );

// copy
var ex0 = {foo: 'bar'};
var ex1 = ex0;
assert( ex0 === ex1 );
assert( ex0.foo === ex1.foo );
ex1 = copy(ex0);
assert( ex1.hasOwnProperty('foo') );
assert( ex1['foo'] === 'bar' );
assert( ex0 !== ex1 );

ex0 = [1,2,3];
ex1 = ex0;
assert(ex0 === ex1);
ex1 = copy(ex0);
assert(ex0 !== ex1);
assert(array_equals(ex0,ex1));

// extend
ex0 = [1,2,3];
extend(ex0, []);
assert(array_equals([1,2,3], [1,2,3]));
extend(ex0, [4,5,6]);
assert(array_equals(ex0, [1,2,3,4,5,6]));

ex0 = {foo: 'bar'};
extend(ex0, {bar: 'foobar'});
assert(ex0.hasOwnProperty('foo'));
assert(ex0['foo'] === 'bar');
assert(ex0.hasOwnProperty('bar'));
assert(ex0['bar'] === 'foobar');

console.log('test_object : done');