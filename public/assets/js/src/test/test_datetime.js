require('./constants_nodejs');

// timeConvert
assert( timeConvert(1499987722) == "13 Jul 2017 16:15:22");
assert( timeConvert(1499987722, true) == "13 Jul 2017");

console.log('test_datetime : done');