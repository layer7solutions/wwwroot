require('./constants_nodejs');

assert( is_int(0) );
assert( is_int(5) );
assert( is_int('') === false );
assert( is_int(null) === false );
assert( is_int(true) === false );
assert( is_int(false) === false );

assert( is_function(function() {}) );
assert( is_function(is_function) );
assert( is_function(null) === false);

assert( is_null(null) );
assert( is_null([]) === false );
assert( is_null({}) === false );
assert( is_null(0) === false );
assert( is_null(false) === false );

assert( is_undefined(undefined) );
assert( is_undefined(null) === false );
assert( is_undefined(false) === false );
assert( is_undefined(0) === false );

assert( is_bool(true) );
assert( is_bool(false) );
assert( is_bool("") === false );
assert( is_bool(0) === false );
assert( is_bool(1) === false );

assert( bool(1) === true );
assert( bool(0) === false );
assert( bool("foo") === true );
assert( bool("") === false );

console.log('test_general : done');