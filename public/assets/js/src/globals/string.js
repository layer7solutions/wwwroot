// GENERAL
// -------

function is_string(obj) {
    return typeof obj === 'string' || obj instanceof String;
}

function startsWith(str, needle) {
    return str.length >= needle.length && str.substring(0, needle.length) === needle;
}

function endsWith(str, needle) {
    return str.length >= needle.length && str.substring(str.length - needle.length) === needle;
}

function replace_prefix(str, prefix, replacement) {
    replacement = replacement || '';
    if (str.slice(0, prefix.length) == prefix) {
        str = replacement + str.slice(prefix.length);
    }
    return str;
}
function remove_prefix(str, prefix) {
    return replace_prefix(str, prefix);
}

function replace_suffix(str, suffix, replacement) {
    replacement = replacement || '';
    if (str.slice(-suffix.length) == suffix) {
        str = str.slice(0,-suffix.length) + replacement;
    }
    return str;
}
function remove_suffix(str, suffix) {
    return replace_suffix(str, suffix);
}

function to_lower(o) {
    return map(o, function(v) {
        return is_string(v) ? v.toLowerCase() : v;
    });
}

function to_upper(o) {
    return map(o, function(v) {
        return is_string(v) ? v.toUpperCase() : v;
    });
}


// split a string
// @param o - the string
// @param ch - the delimeter (can be array for multiple)
// @param keep_empty - if true, keep empty elements, otherwise remove empty elements (default)
function split(o, ch, keep_empty) {
    if (is_array(ch)) {
        ch = new RegExp(map(ch, escape_regex).join('|'));
    }
    var ret = o.split(ch || ' ');
    return keep_empty ? ret : filter(ret, function(v) {
        if (v.length == 0) return false;
    });
}

// HTML AND REGEX AND INSERT/REPLACE
// ---------------------------------

// escape html
function escape_html(unsafe) {
    var text = document.createTextNode(unsafe);
    var div  = document.createElement('div');
    div.appendChild(text);
    return div.innerHTML;
}

// escape regex
function escape_regex(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

// validate regex
function validate_regex(pattern, options) {
    try {
        new RegExp(pattern, options || '');
        return true;
    } catch(e) {
        return false;
    }
}

function string_replace(str, find, replace) {
    function replace_fn(find, replace) {
        str = str.replace(new RegExp(escape_regex(find), 'g'), replace);
        return str;
    }

    if (arguments.length != 1) {
        return replace_fn(find, replace);
    } else {
        return {
            replace: function(find, replace) {
                do {
                    str = replace_fn(find, replace);
                }  while(contains(str, find));
                return this;
            },
            trim: function(chmask) {
                str = trim(str, chmask);
                return this;
            },
            replaceNewLines: function(replace) {
                this.replace("\r\n", "\n")
                    .replace("\n\r", "\n")
                    .replace("\r", "\n")
                    .replace("\n", replace);
                return this;
            },
            get: function() {
                return str;
            },
            set: function(new_string) {
                str = new_string;
                return this;
            },
            toString: function() {
                return str;
            }
        };
    }
}

function string_insert(str, idx, insert, remove) {
    return str.slice(0, idx) + insert + str.slice(idx + (Math.abs(remove) || 0));
}

// MISC
// ----

// count number of lines in a string
function linecount(str) {
    return str.split(/\r*\n/).length;
}

// convert first character to uppercase
function ucfirst(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

// convert first character of every word to uppercase
function ucwords(str) {
    return map(split(str, ' ', true), ucfirst).join(' ');
}

// PHP trim() equivalent
function trim(str, char_mask, mode) {
    if (typeof str !== 'string') {
        str += '';
    }

    var l = str.length,
        i = 0;

    if (!l) return '';

    if (char_mask) {
        char_mask = char_mask+'';
        if (!char_mask.length) return str;
    } else {
        char_mask = trim.whitespace;
    }

    mode = mode || (1 | 2);

    // noinspection JSBitwiseOperatorUsage
    if (mode & 1) {
        for (i = 0; i < l; i++) {
            if (char_mask.indexOf(str.charAt(i)) === -1) {
                str = str.substring(i);
                break;
            }
        }
        if (i == l) return '';
    }

    // noinspection JSBitwiseOperatorUsage
    if (mode & 2) {
        for (i = l - 1; i >= 0; i--) {
            if (char_mask.indexOf(str.charAt(i)) === -1) {
                str = str.substring(0, i + 1);
                break;
            }
        }
        if (i == -1) return '';
    }

    return str;
}
trim.whitespace = [
    ' ',
    '\n',
    '\r',
    '\t',
    '\f',
    '\x0b',
    '\xa0',
    '\u2000',
    '\u2001',
    '\u2002',
    '\u2003',
    '\u2004',
    '\u2005',
    '\u2006',
    '\u2007',
    '\u2008',
    '\u2009',
    '\u200a',
    '\u200b',
    '\u2028',
    '\u2029',
    '\u3000'
].join('');

function ltrim(str, char_mask) {
    return trim(str, char_mask, 1);
}
function rtrim(str, char_mask) {
    return trim(str, char_mask, 2);
}