function is_int(value) {
    var x;
    if (isNaN(value)) {
        return false;
    }
    x = parseFloat(value);
    return (x | 0) === x;
}

function is_function(o) {
    return typeof o === 'function';
}

function is_null(o) {
    return o === null;
}

function is_undefined(o) {
    return typeof o === 'undefined';
}

function is_undef(o) {
    return typeof o === 'undefined';
}

function is_bool(o) {
    return o === true || o === false || o.toString() === '[object Boolean]';
}

function bool(o) {
    return !!o;
}

function is_truthy(o) {
    return !!o === true;
}

function is_falsey(o) {
    return !!o === false;
}

function is_promise(o) {
    return o &&  (
        Promise.resolve(o) === o
        || (o instanceof Promise)
        || Object.prototype.toString.call(o) === "[object Promise]"
        || (typeof o.then === 'function')
    );
}