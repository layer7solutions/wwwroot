function Counter() {
    return {
        map: {},
        add: function(key) {
            if (this.map.hasOwnProperty(key)) {
                this.map[key] = this.map[key] + 1;
            } else {
                this.map[key] = 1;
            }
        },
        subtract: function(key) {
            if (this.map.hasOwnProperty(key)) {
                this.map[key] = Math.max(this.map[key] - 1, 0);
            } else {
                this.map[key] = 1;
            }
        },
        set: function(key, value) {
            this.map[key] = parseInt(value || 1);
        },
        get: function(key) {
            return this.map[key];
        },
        length: function() {
            return Object.keys(this.map).length;
        },
    };
}

function Dict(obj, pass) {
    obj = obj || {};

    return {
        put: function (key, value) {
            obj[key] = value;
        },
        get: function (key) {
            return obj[key];
        },
        remove: function(key) {
            delete obj[key];
        },
        keys: function() {
            return Object.keys(obj);
        },
        values: function() {
            return Object.values(obj);
        },
        length: function() {
            return Object.keys(obj).length;
        },
        has: function(key) {
            return obj.hasOwnProperty(key);
        },
        containsAll: function(keys) {
            for (var i = 0, len = keys.length; i < len; i++) {
                if (!this.contains(keys[i])) {
                    return false;
                }
            }
            return true;
        },
        contains: function(key) {
            return key in obj;
        },
        forEach: function(fn) {
            var keys = Object.keys(obj);
            for (var i = 0, len = keys.length; i < len; i++) {
                if (fn(keys[i], obj[keys[i]], i, pass) === false) { // fn(key, value, index, pass)
                    break;
                }
            }
        }
    };
}