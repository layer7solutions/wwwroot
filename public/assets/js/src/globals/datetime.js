function timeConvert(UNIX_timestamp, no_time) {
    if (empty(UNIX_timestamp)) {
        return String(UNIX_timestamp);
    }

    var a;

    if (UNIX_timestamp instanceof Date) {
        a = moment(UNIX_timestamp);
    } else {
        a = moment(UNIX_timestamp * 1000);
    }

    var format = no_time ? 'MMM DD YYYY' : 'MMM DD YYYY hh:mm:ss a';

    if (CURRENT_USER) {
        var ret = a.utcOffset(USER_TZ_OFFSET).format(format);
        if (!no_time) {
            ret += " " + USER_TZ_ABRV;
        }
        return ret;
    } else {
        return a.format(format)
    }
}

function human_timing(time, suffix) {
    suffix = suffix || null;

    if (time instanceof Date)
        time = time / 1000 | 0;
    if (time === null)
        return null;
    if (time <= 0)
        return 'never';

    time = Math.floor(Date.now() / 1000) - time;
    suffix = isset(suffix) ? suffix : (time < 0 ? 'from now' : 'ago');
    time = Math.abs(time);

    if (time <= 1)
        return 'Just now';

    var tokens = [
        [31536000, 'year'],
        [2592000, 'month'],
        [604800, 'week'],
        [86400, 'day'],
        [3600, 'hour'],
        [60, 'minute'],
        [1, 'second']
    ];

    var ret = null;

    forEach(tokens, function(token) {
        var unit = token[0];
        var text = token[1];

        if (time < unit)
            return;

        var numberOfUnits = Math.floor(time / unit);
        ret = (numberOfUnits+' '+text+((numberOfUnits>1)?'s':'')) + ' ' + suffix;
        return false;
    });

    return ret;
}