function awaitElement(selector, callback) {
    function try_await() {
        var element = document.querySelector(selector);

        if (!element) {
            window.requestAnimationFrame(try_await);
        }else {
            callback(element);
        }
    }
    try_await();
}

function awaitElements(selector, callback) {
    function try_await() {
        var elements = document.querySelectorAll(selector);

        if (!elements.length) {
            window.requestAnimationFrame(try_await);
        }else {
            callback(elements);
        }
    }
    try_await();
}

function extract_domain(url) {
    var domain;
    //find & remove protocol (http, ftp, etc.) and get domain
    if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
    } else {
        domain = url.split('/')[0];
    }

    //find & remove port number
    domain = domain.split(':')[0];

    return domain;
}

function query_param(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("(?:^|[?&])" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function is_mobile() {
    return !!(navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/Opera Mini/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/IEMobile/i)
        || navigator.userAgent.match(/WPDesktop/i)
        || navigator.userAgent.match(/Windows Phone/i));
}