function is_object(o) {
    return o !== null && typeof o === 'object';
}

function empty(o) {
    if (!isset(o)) {
        return true;
    }
    if (n.isNode(o)) {
        return false;
    }
    if (is_object(o)) {
        if (o.constructor === Object && Object.keys(o).length === 0) {
            return true;
        }
    }
    if (o.hasOwnProperty('length') && o.length === 0) {
        return true;
    }
    return o == false;
}

function not_empty(o) {
    return !empty(o);
}

function isset(o) {
    var a = arguments,
        l = a.length,
        i = 0,
        undef;

    if (l === 0) {
        return false;
    }

    while (i !== l) {
        // noinspection JSUnusedAssignment
        if (a[i] === undef || a[i] === null) {
            return false;
        }
        i++;
    }
    return true;
}

function not_set() {
    return !isset.apply(null, arguments);
}

function forEach(obj, callback) {
    if (is_string(obj)) {
        obj = document.querySelectorAll(obj);
    }

    if (is_object(obj)) {
        if ('length' in obj && (obj.length !== 0 && '0' in obj)) {
            if (obj.length == 1) {
                callback(obj[0], 0, 1);
                return;
            }

            for (var i = 0; i < obj.length; i++) {
                var ret = callback(obj[i], i, obj.length);
                if (ret === false) {
                    break;
                }
            }
        } else {
            var keys = Object.keys(obj);
            for (var i = 0, len = keys.length; i < len; i++) {
                if (callback(keys[i], obj[keys[i]], i) === false) { // fn(key, value, index)
                    break;
                }
            }
        }
    }
}

function map(o, fn) {
    if (!isset(o)) {
        return [];
    } else if (is_array_like(o)) {
        var ret = [];
        forEach(o, function(v) {
            ret.push(fn(v));
        });
        return ret;
    } else if (is_object(o)) {
        var ret = {};
        forEach(o, function(k, v) {
            ret[k] = fn(k,v);
        });
        return ret;
    } else {
        return fn(o);
    }
}

function filter(o, predicate) {
    if (not_set(predicate)) {
        predicate = isset;
    }

    var pred_is_func = is_function(predicate);

    if (!isset(o)) {
        return [];
    } else if (is_array_like(o)) {
        var ret = [];
        forEach(o, function(v) {
            if (pred_is_func ? predicate(v) === true : predicate === v)
                ret.push(v);
        });
        return ret;
    } else if (is_object(o)) {
        var ret = {};
        forEach(o, function(k, v) {
            if (pred_is_func ? predicate(k,v) === true : predicate === v)
                ret[k] = v;
        });
        return ret;
    } else {
        return [];
    }
}

function reject(o, predicate) {
    if (not_set(predicate)) {
        predicate = not_set;
    }

    var pred_is_func = is_function(predicate);

    if (!isset(o)) {
        return [];
    } else if (is_array_like(o)) {
        var ret = [];
        forEach(o, function(v) {
            if (pred_is_func ? predicate(v) !== true : predicate !== v)
                ret.push(v);
        });
        return ret;
    } else if (is_object(o)) {
        var ret = {};
        forEach(o, function(k, v) {
            if (pred_is_func ? predicate(k,v) !== true : predicate !== v)
                ret[k] = v;
        });
        return ret;
    } else {
        return [];
    }
}

function find(o, predicate) {
    if (not_set(predicate)) {
        predicate = isset;
    }

    var pred_is_func = is_function(predicate);

    var ret = undefined;
    if (!isset(o)) {
        return ret;
    } else if (is_array_like(o)) {
        forEach(o, function(v) {
            if (pred_is_func ? predicate(v) === true : predicate === v) {
                ret = v;
                return false; // break foreach
            }
        });
    } else if (is_object(o)) {
        forEach(o, function(k,v) {
            if (pred_is_func ? predicate(k,v) === true : predicate === v) {
                ret = k;
                return false; // break foreach
            }
        });
    }
    return ret;
}

function contains(haystack, needle) {
    if (typeof haystack === 'undefined' || haystack == null) {
        return false;
    }

    // string contains sequence
    if (is_string(haystack)) {
        return haystack.indexOf(needle) > -1;
    }

    // array contains item
    else if (is_array_like(haystack)) {
        return in_array(haystack, needle);
    }

    // nodelist contains node
    else if (n.isNodeList(haystack) && n.isNode(needle)) {
        for (var i = 0; i < haystack.length; i++) {
            if (needle == haystack[i]) {
                return true;
            }
        }
        return false;
    }

    // node contains node
    else if (n.isNode(haystack) && n.isNode(needle)) {
        return haystack.contains(needle);
    }

    // object contains property
    else if (is_object(haystack)) {
        return haystack.hasOwnProperty(needle);
    }

    return false;
}

function reduce(obj, callback, memo) {
    var acc = typeof memo !== 'undefined' ? memo : (is_array(obj) ? obj.shift() : {});

    forEach(obj, function() {
        var res = callback.apply(null, [acc].concat([].slice.call(arguments)));
        if (!is_undef(res)) {
            acc = res;
        }
    });

    return acc;
}

function reduceToString(obj, callback) {
    return reduce(obj, callback, '');
}

function reduceToArray(obj, callback) {
    return reduce(obj, callback, []);
}

function reduceToMap(obj, callback) {
    return reduce(obj, callback, {});
}

function copy(o) {
    var to;

    if (n.isNode(o)) {
        if (n.clone) {
            to = n.clone(o);
        } else {
            to = o.cloneNode(true);
        }
    } else if (is_object(o)) {
        if (o.constructor != Object && o.constructor != Array) return o;
        if (o.constructor == Date || o.constructor == RegExp || o.constructor == Function ||
            o.constructor == String || o.constructor == Number || o.constructor == Boolean)
            return new o.constructor(o);

        to = new o.constructor();

        for (var name in o) {
            // noinspection JSUnfilteredForInLoop
            to[name] = typeof(to[name]) === 'undefined' ? copy(o[name]) : to[name];
        }
    } else {
        to = o;
    }

    return to;
}

function extend(obj, other) {
    if (is_array(obj)) {
        obj.extend(other);
    } else if (is_object(obj) || is_function(obj)) {
        for (var k in other) {
            if (other.hasOwnProperty(k)) {
                obj[k] = other[k];
            }
        }
    }
    return obj;
}