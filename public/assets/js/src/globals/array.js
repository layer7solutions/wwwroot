function is_array(o) {
    return (Object.prototype.toString.call(o) === '[object Array]');
}

function is_array_like(o) {
    if (is_array(o)) return true;
    if (!isset(o) || is_string(o)) return false;

    if (typeof o !== 'object') return false;
    if (n.isNode(o)) return false;
    if (typeof o.length !== 'number') return false;
    if (o.length < 0) return false;
    if (o.length > 0) return o.hasOwnProperty(0);
    return true;
}

function array_equals(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }

    return true;
}

function to_array(o) {
    if (typeof o === 'string') {
        return [o];
    }
    return Array.prototype.slice.call(o);
}

function in_array(array, item) {
    if (is_array(array) && typeof Array.prototype.indexOf === 'function') {
        return array.indexOf(item) > -1;
    } else if (is_array_like(array)) {
        for(var i = 0; i < array.length; i++) {
            if(array[i] === item) {
                return true;
            }
        }
    }
    return false;
}

// general
// -------

function any(a, predicate) {
    if (!isset(a) || a.length == 0) return false;
    return !is_undefined(first(a, predicate || isset));
}

function every(a, predicate) {
    if (!isset(a) || a.length == 0) return false;
    return filter(a, predicate || isset).length === a.length;
}

function first(a, predicate) {
    if (!isset(a)) return undefined;
    if (!isset(predicate)) {
        return a[0];
    }
    for (var i = 0; i < a.length; i++) {
        if (predicate(a[i])) {
            return a[i];
        }
    }
    return undefined;
}

function last(a, predicate) {
    if (!isset(a)) return undefined;
    if (!isset(predicate)) {
        return a[a.length - 1];
    }
    for (var i = a.length - 1; i >= 0; i--) {
        if (predicate(a[i])) {
            return a[i];
        }
    }
    return undefined;
}

function intersect(a1,a2) {
    var a3 = [];

    forEach(a1, function(itm) {
        if (contains(a2, itm)) {
            a3.push(itm);
        }
    });

    return a3;
}

function diff(a1,a2) {
    var a3 = [];

    forEach(a1, function(itm) {
        if (!contains(a2, itm)) {
            a3.push(itm);
        }
    });

    return a3;
}

function union(a1,a2) {
    var a3 = [];

    var arr = a1.concat(a2),
        len = arr.length;

    while (len--) {
        var itm = arr[len];
        if (a3.indexOf(itm) === -1) {
            a3.unshift(itm);
        }
    }

    return a3;
}

function flatten() {
    function f(args) {
        var ret = [];
        forEach(args, function(item) {
            if (is_array(item)) {
                ret.extend(f(item));
            } else {
                ret.push(item);
            }
        });
        return ret;
    }
    return f(arguments);
}