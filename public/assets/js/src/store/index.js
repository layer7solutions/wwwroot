app.store = new Vuex.Store({
  modules: {
    appSideGroupState: {
      namespaced: true,
      state: {},
      mutations: {
        put(state, payload) {
          Vue.set(state, payload.key, payload.value);
        },
      },
      getters: {
        get: state => key => {
          return !state.hasOwnProperty(key) ? true : state[key];
        },
      },
    },
  },
  plugins: [
    createPersistedState({
      paths: ['appSideGroupState'],
    }),
  ],
});
