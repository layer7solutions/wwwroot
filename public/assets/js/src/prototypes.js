
if (!Array.prototype.extend) {
    Array.prototype.extend = function() {
        if (arguments.length == 1 && Object.prototype.toString.call(arguments[0]) === '[object Array]') {
            // noinspection JSUnusedAssignment
            arguments = arguments[0];
        }
        for (var i = 0; i < arguments.length; i++) {
            this.push(arguments[i]);
        }
        return this;
    }
}

if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments, d = {};

        // loop over arguments and populate 'd' ...
        for (var i = 0, j = 0; i < args.length; i++) {
            var o = args[i]; // arg

            // if object and not array
            if (o !== null && typeof o === 'object' && Object.prototype.toString.call(o) !== '[object Array]') {
                // merge the object 'o' into 'd'
                var keys = Object.keys(o);
                for (var k = 0, len = keys.length; k < len; k++) {
                    d[keys[k]] = o[keys[k]];
                }
            }
            // otherwise
            else {
                d[j++] = o;
            }
        }

        var i = 0;
        return this.replace(/\{([A-z0-9]*)\}/g, function(match, key) {
            // match -> with curly braces
            // key -> without curly braces

            key = key || i++;
            return typeof d[key] !== 'undefined'
                ? d[key]
                : match
            ;
        });
    };
}


if (!Storage.prototype.setObject) {
    // Store an object
    // @param key - the key
    // @param value - the value
    // @param expires - seconds to expire (optional, won't expire if not provided)
    Storage.prototype.setObject = function(key, value, expires) {
        this.setItem(key, JSON.stringify({
            value: value,
            expires: expires && (Date.now() / 1000 | 0) + expires
        }));
    }
}

if (!Storage.prototype.getObject) {
    // Get object by key
    // @return the object associated with the key or `undefined` if already expired or non-existant
    Storage.prototype.getObject = function(key) {
        var stringified = this.getItem(key),
            data        = stringified && JSON.parse(stringified),
            expires     = (data ? data.expires : null) || Infinity;

        if (!data) return undefined;

        return (Date.now() / 1000 | 0) >= expires ? undefined : data.value;
    }
}

if (!Storage.prototype.removeObject) {
    Storage.prototype.removeObject = function(key) {
        this.removeItem(key);
    }
}