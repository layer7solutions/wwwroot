/*
util.js
~~~~~~~
General DOM manipulation and other utilities. */

var n = function() {
    return n.create.apply(n, arguments);
};

n.find = function() {
    var el,
        selector;

    if (arguments.length == 1) {
        el = document;
        selector = arguments[0];
    } else if (arguments.length == 2) {
        el = arguments[0];
        selector = arguments[1];
    } else {
        return false;
    }

    if (/(^\s*|,\s*)>/.test(selector)) {
        var id, removeId = false;

        if (!el.hasAttribute('id')) {
            el.setAttribute('id', id = 'ID_' + new Date().getTime());
            removeId = true;
        } else {
            id = el.getAttribute('id');
        }

        selector = selector.replace(/(^\s*|,\s*)>/g, '$1#' + id + ' >');

        var result = document.querySelectorAll(selector);

        if (removeId) {
            el.removeAttribute('id');
        }

        return result;
    } else {
        return el.querySelectorAll(selector);
    }
};

n.select = function() {
    var elements;

    var o = null, source = document;

    if (arguments.length == 1) {
        o = arguments[0];
    } else if (arguments.length == 2) {
        source = arguments[0];
        o = arguments[1];
    } else {
        return false;
    }

    if (is_string(o) && o.length) {
        try {
            elements = n.find(source, o);
        } catch(ex) {
            return false;
        }
    } else if (n.isNode(o)) {
        elements = [o];
    } else if (is_array_like(o)) {
        elements = o;
    } else {
        return false;
    }

    var self = {
        _back: null,
        _select: function() {
            var x = n.select.apply(null, arguments);
            x._back = this;
            return x;
        },

        back: function(amount) {
            amount = amount || 1;
            if (amount == 1) {
                return this._back;
            }

            var x = this;
            do {
                x = x._back;
            } while(x !== null && amount-- > 0);
            return x;
        },

        then: function(fn) {
            if (!fn) {
                return this._select(document.body);
            }
            fn.apply(null, flatten(to_array(arguments).slice(1)));
            return this;
        },

        select: function(selector) {
            if (is_string(selector)) {
                try {
                    var result = n.find(elements[0], selector);

                    if (elements.length > 1) {
                        for (var i = 1; i < elements.length; i++) {
                            var nextResult = n.find(elements[i], selector);
                            result = n.mergeNodeList(result, nextResult);
                        }
                    }

                    return this._select(result);
                } catch(ex) {
                    return false;
                }
            } else if (n.isNode(selector) || is_array_like(selector)) {
                return this._select(selector);
            } else if (is_int(selector) && selector >= 0 && selector < elements.length) {
                return this._select(elements[selector]);
            } else {
                return false;
            }
        },

        forEach: function(callback) {
            forEach(elements, callback);
            return self;
        },
        filter: function(callback) {
            var new_list = [];
            forEach(elements, function(element, i, len) {
                if (callback(element, i, len)) {
                    new_list.push(element);
                }
            });
            return this._select(new_list);
        },
        map: function(callback) {
            var results = [];
            forEach(elements, function(element, i, len) {
                var ret = callback(element, i, len);
                if (ret !== undefined)
                    results.push(ret);
            });
            return results;
        },
        sort: function(compareFunction) {
            if (!isset(compareFunction)) {
                return self;
            }
            elements = Array.prototype.slice.call(elements);
            elements.sort(compareFunction);
            return self;
        },
        reduce: function(callback, initial_value) {
            if (elements.length == 0) {
                return initial_value || null;
            }

            var acc = initial_value,
                skip_first = false;

            if (!isset(acc)) {
                acc = elements[0];
                skip_first = true;
            }

            forEach(elements, function(element, i, len) {
                if (i == 0 && skip_first) {
                    return;
                }
                acc = callback(acc, element, i, elements);
            });

            return acc;
        },
        reduceToString: function(callback) {
            return this.reduce(callback, '');
        },
        reduceToArray: function(callback) {
            return this.reduce(callback, []);
        },
        allMatches: function(callback) {
            var result = true;
            forEach(elements, function(element, i, len) {
                if (callback(element, i, len) === false) {
                    result = false;
                    return false;
                }
            });
            return result;
        },
        anyMatches: function(callback) {
            var result = false;
            forEach(elements, function(element, i, len) {
                if (callback(element, i, len) === true) {
                    result = true;
                    return false;
                }
            });
            return result;
        },
        length: elements.length,

        previousElement: function() {
            if (elements.length)
                return elements[0].previousElementSibling;
            return null;
        },
        nextElement: function() {
            if (elements.length)
                return elements[0].nextElementSibling;
            return null;
        },
        previous: function() {
            if (elements.length)
                return elements[0].previousSibling;
            return null;
        },
        next: function() {
            if (elements.length)
                return elements[0].nextSibling;
            return null;
        },

        addClass: function(cls) {
            n.addClass(elements, cls);
            return self;
        },
        removeClass: function(cls) {
            n.removeClass(elements, cls);
            return self;
        },
        hasClass: function(cls) {
            return n.hasClass(elements, cls);
        },
        setClass: function(cls, state) {
            n.setClass(elements, cls, state);
            return self;
        },
        swapClass: function(prev, next) {
            n.removeClass(elements, prev);
            n.addClass(elements, next);
            return self;
        },

        hide: function() {
            n.hide(elements);
            return self;
        },
        show: function() {
            n.show(elements);
            return self;
        },

        setSelection: function(start, end) {
            forEach(elements, function(element) {
                element.setSelectionRange(start || 0, end || element.value.length)
            });
        },

        frag: function() {
            var result = n.frag(elements[0]);
            if (result.firstChild == result.lastChild) {
                result = result.firstChild;
            }
            return this._select(result);
        },
        html: function(new_html) {
            if (typeof new_html === 'undefined' || new_html == null) {
                return n.html(elements);
            } else {
                n.html(elements, new_html);
                return self;
            }
        },
        value: function(new_value) {
            if (typeof new_value === 'undefined' || new_value == null) {
                return n.value(elements);
            } else {
                n.value(elements, new_value);
                return self;
            }
        },
        text: function(new_text) {
            if (typeof new_text === 'undefined' || new_text == null) {
                return n.text(elements);
            } else {
                n.text(elements, new_text);
                return self;
            }
        },
        closest: function(sel) {
            return this._select(n.closest(elements, sel));
        },
        parent: function() {
            return this._select(elements[0].parentNode);
        },

        remove: function() {
            n.remove(elements);
            return self;
        },
        removeAllChildren: function() {
            n.removeAllChildren(elements);
            return self;
        },


        focus: function() {
            forEach(elements, function(element) {
                element.focus();
            });
            return self;
        },
        blur: function() {
            forEach(elements, function(element) {
                element.blur();
            });
            return self;
        },
        click: function() {
            forEach(elements, function(element) {
                element.click();
            });
            return self;
        },

        on: function(event, handler) {
            n.on(elements, event, handler);
            return self;
        },
        off: function(event, handler) {
            n.off(elements, event, handler);
            return self;
        },

        insertBefore: function(node) {
            n.insertBefore(node, elements);
            return self;
        },
        insertAfter: function(node) {
            n.insertAfter(node, elements);
            return self;
        },
        append: function(node) {
            n.appendChild(node, elements);
            return self;
        },
        appendTo: function(node) {
            forEach(elements, function(element) {
                n.appendChild(element, node);
            });
            return self;
        },
        prepend: function(node) {
            n.prependChild(node, elements);
            return self;
        },
        prependTo: function(node) {
            forEach(elements, function(element) {
                n.prependChild(element, node);
            });
            return self;
        },
        replace: function(node) {
            n.replaceNode(node, elements);
            return self;
        },

        attr: function(attrKey, attrVal) {
            if (typeof attrVal === 'undefined' || attrVal == null) {
                return n.getAttribute(elements, attrKey);
            }

            n.setAttribute(elements, attrKey, attrVal);

            return self;
        },
        hasAttr: function(attrKey) {
            return n.hasAttribute(elements, attrKey);
        },
        removeAttr: function(attrKey) {
            n.removeAttribute(elements, attrKey);
            return self;
        },

        id: function(new_id) {
            if (typeof new_id === 'undefined' || new_id == null) {
                return n.getAttribute(elements[0], 'id');
            } else {
                n.setAttribute(elements[0], 'id', new_id);
                return self;
            }
        },

        modify: function() {
            var args = [].slice.apply(arguments);
            args.unshift(null);

            for (var i = 0; i < elements.length; i++) {
                args[0] = elements[i];
                n.create.apply(null, args);
            }
            return self;
        },
        mod: function() {
            self.modify.apply(self, [].slice.apply(arguments));
            return self;
        },
    };

    for (var i = 0; i < elements.length; i++) {
        self[i] = elements[i];
    }

    return self;
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

n.isNode = function(o) {
    return (
        typeof Node === "object" ? o instanceof Node :
        o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName==="string"
    );
};

n.isElement = function(o) {
    return (
        typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
        o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
    );
};

n.isFragment = function(o) {
    return (
        typeof DocumentFragment === "object" ? o instanceof DocumentFragment : //DOM2
        o && typeof o === "object" && o !== null && o.nodeType === 11 && typeof o.nodeName==="string"
    );
};

n.isNodeList = function(nodes) {
    var stringRepr = Object.prototype.toString.call(nodes);

    return typeof nodes === 'object' &&
        /^\[object (HTMLCollection|NodeList|Object)\]$/.test(stringRepr) &&
        (typeof nodes.length === 'number') &&
        (nodes.length === 0 || (typeof nodes[0] === "object" && nodes[0].nodeType > 0));
};

n.mergeNodeList = function(a, b) {
    var slice = Array.prototype.slice;
    return slice.call(a).concat(slice.call(b));
};

n.isNodeLike = function(o) {
    return n.isNode(o) || n.isNodeList(o);
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

n.frag = function(html) {
    if (!isset(html)) {
        return document.createDocumentFragment();
    }

    if (is_function(html)) {
        html = html();
    }

    if (n.isElement(html) && html.tagName.toUpperCase() == 'TEMPLATE') {
        html = html.innerHTML.trim();
    }

    var frag = document.createDocumentFragment();

    if (is_array_like(html)) {
        function recursiveAppend(childList) {
            forEach(childList, function(child) {
                if (is_function(child)) {
                    child = child();
                }
                if (is_array_like(child)) {
                    recursiveAppend(child);
                    return;
                }
                n.appendChild(child, frag);
            });
        }
        recursiveAppend(html);
    } else {
        var div = n.isNode(html) ? html : document.createElement("div");
        if (is_string(html)) {
            div.innerHTML = html;
        }
        var child;
        while ( (child = div.firstChild) ) {
            frag.appendChild(child);
        }
    }

    return frag;
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(function() {
    const LISTENERS = {};

    var next_id = 0;

    function get_listener_id(element, no_issue) {
        var my_id = null;

        if (empty(element) || !n.isElement(element)) {
            // do nothing
        } else if (element.hasAttribute('data-evtid')) {
            my_id = parseInt(element.getAttribute('data-evtid'));
        } else if (!no_issue) {
            my_id = next_id++;
            element.setAttribute('data-evtid', my_id);
        }

        return isNaN(my_id) ? null : my_id;
    }

    n.getListeners = function(element, eventName) {
        var id = get_listener_id(element, true);

        if (!isset(id)) {
            if (eventName) return [];
            return {};
        }

        if (eventName) {
            if (LISTENERS[id][eventName]) {
                return LISTENERS[id][eventName];
            } else {
                return [];
            }
        }

        return LISTENERS[id];
    };

    n.clone = function(o, deep, copy_listeners) {
        deep = is_undef(deep) ? true : deep;
        copy_listeners = is_undef(copy_listeners) ? true : copy_listeners;

        var id = get_listener_id(o, true);

        var node = o.cloneNode();
        if (n.isElement(o) && o.hasAttribute('data-evtid')) {
            node.removeAttribute('data-evtid');
        }

        if (copy_listeners && isset(id) && n.isElement(node) && LISTENERS[id]) {
            forEach(LISTENERS[id], function(eventName, handlerList) {
                forEach(handlerList, function(eventHandler) {
                    n.on(node, eventName, eventHandler);
                });
            });
        }

        if (deep && o.childNodes.length) {
            for (var i = 0, len = o.childNodes.length; i < len; i++) {
                node.appendChild(n.clone(o.childNodes[i], deep, copy_listeners));
            }
        }

        return node;
    };

    n.off = function(o, eventName, eventHandler) {
        o = is_string(o) ? document.querySelectorAll(o) : o;

        if (empty(o)) {
            return;
        }

        function detach_listener(el) {
            var id = get_listener_id(el, true);

            if (!eventHandler) {
                if (!LISTENERS[id])
                    return false;

                if (!eventName) {
                    forEach(LISTENERS[id], function(eventName, handlerList) {
                        forEach(handlerList, function(eventHandler) {
                            el.removeEventListener(eventName, eventHandler);
                        });
                    });
                    LISTENERS[id] = {};
                } else if (LISTENERS[id][eventName]) {
                    var handlerList = LISTENERS[id][eventName];
                    forEach(handlerList, function(eventHandler) {
                        el.removeEventListener(eventName, eventHandler);
                    });
                    LISTENERS[id][eventName] = [];
                } else {
                    return false;
                }
            } else {
                if (LISTENERS[id] && LISTENERS[id][eventName]) {
                    var index = LISTENERS[id][eventName].indexOf(eventHandler);
                    if (index > -1) {
                        LISTENERS[id][eventName].splice(index, 1);
                    }
                }

                el.removeEventListener(eventName, eventHandler);
            }
            return true;
        }

        if (is_array_like(o)) {
            for (var i = 0, len = o.length; i < len; i++) {
                detach_listener(o[i]);
            }
        } else {
            detach_listener(o);
        }
    };

    n.on = function(o, eventName, eventHandler) {
        o = is_string(o) ? document.querySelectorAll(o) : o;

        if (empty(o)) {
            return;
        }

        var multi_evt = eventName.split(' ');
        if (multi_evt.length >= 2) {
            for (var i = 0, len = multi_evt.length; i < len; i++) {
                n.on(o, multi_evt[i], eventHandler);
            }
            return;
        }

        if (eventName === 'enter') {
            eventName = 'keyup';
            var real_handler = eventHandler;

            eventHandler = function(event) {
                if (event.key === 'Enter') {
                    real_handler.call(this, event);
                } else if (event.code === 'Enter') {
                    real_handler.call(this, event);
                } else if (event.which === 13) {
                    real_handler.call(this, event);
                } else if (event.keyCode === 13) {
                    real_handler.call(this, event);
                }
            };
        }

        function attach_listener(el) {
            var id = get_listener_id(el);

            if (!LISTENERS[id]) {
                LISTENERS[id] = {};
            }

            if (!LISTENERS[id][eventName]) {
                LISTENERS[id][eventName] = [];
            }

            LISTENERS[id][eventName].push(eventHandler);
            el.addEventListener(eventName, eventHandler);
        }

        if (is_array_like(o)) {
            for (var i = 0, len = o.length; i < len; i++) {
                attach_listener(o[i]);
            }
        } else {
            attach_listener(o);
        }
    };
})();

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

n.insertBefore = function(newNode, ref) {
    ref = is_string(ref) ? document.querySelector(ref) : ref;

    if (empty(newNode) || empty(ref)) {
        return;
    }

    if (is_string(newNode)) {
        newNode = n.frag(newNode);
    }

    if (is_object(ref) && ref.length && ref.length == 1 && ref.hasOwnProperty(0))
        ref = ref[0];

    if (is_array_like(ref)) {
        for (var i = 0, len = ref.length; i < len; i++) {
            n.insertBefore(n.clone(newNode), ref[i]);
        }
        return;
    }

    ref.parentNode.insertBefore(newNode, ref);
};

n.insertAfter = function(newNode, ref) {
    ref = is_string(ref) ? document.querySelector(ref) : ref;

    if (empty(newNode) || empty(ref)) {
        return;
    }

    if (is_string(newNode)) {
        newNode = n.frag(newNode);
    }

    if (is_object(ref) && ref.length && ref.length == 1 && ref.hasOwnProperty(0))
        ref = ref[0];

    if (is_array_like(ref)) {
        for (var i = 0, len = ref.length; i < len; i++) {
            n.insertAfter(n.clone(newNode), ref[i]);
        }
        return;
    }

    if (ref.nextSibling)
        ref.parentNode.insertBefore(newNode, ref.nextSibling);
    else
        ref.parentNode.appendChild(newNode);
};

n.appendChild = function(newNode, ref) {
    ref = is_string(ref) ? document.querySelector(ref) : ref;

    if (empty(newNode) || empty(ref)) {
        return;
    }

    if (is_string(newNode)) {
        newNode = n.frag(newNode);
    }

    if (is_object(ref) && ref.length && ref.length == 1 && ref.hasOwnProperty(0))
        ref = ref[0];

    if (is_array_like(ref)) {
        for (var i = 0, len = ref.length; i < len; i++) {
            n.appendChild(n.clone(newNode), ref[i]);
        }
        return;
    }

    ref.appendChild(newNode);
};

n.prependChild = function(newNode, ref) {
    ref = is_string(ref) ? document.querySelector(ref) : ref;

    if (empty(newNode) || empty(ref)) {
        return;
    }

    if (is_string(newNode)) {
        newNode = n.frag(newNode);
    }

    if (is_object(ref) && ref.length && ref.length == 1 && ref.hasOwnProperty(0))
        ref = ref[0];

    if (is_array_like(ref)) {
        for (var i = 0, len = ref.length; i < len; i++) {
            n.prependChild(n.clone(newNode), ref[i]);
        }
        return;
    }

    if (ref.firstChild) {
        ref.insertBefore(newNode, ref.firstChild);
    } else {
        ref.appendChild(newNode);
    }
};

n.removeNode = function(el) {
    el.parentNode.removeChild(el);
};

n.replaceNode = function(el, newNode) {
    if (empty(el) || empty(newNode)) {
        return;
    }

    if (is_string(newNode)) {
        newNode = n.frag(newNode);
    }

    if (is_array_like(el)) {
        for (var i = 0, len = el.length; i < len; i++) {
            n.insertBefore(n.clone(newNode), el[i]);
        }
        return;
    }

    el.parentNode.replaceChild(newNode, el);
};

n.remove = function(o) {
    if (empty(o)) {
        return;
    }

    o = is_string(o) ? document.querySelectorAll(o) : o;

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++) {
            if (!o[i] || !o[i].parentNode) {
                return;
            }
            o[i].parentNode.removeChild(o[i]);
        }
    } else {
        if (!o || !o.parentNode) {
            return;
        }
        o.parentNode.removeChild(o);
    }
};

n.removeAllChildren = function(o) {
    if (empty(o)) {
        return;
    }

    o = is_string(o) ? document.querySelectorAll(o) : o;

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++) {
            while (o[i].firstChild) {
                o[i].removeChild(o[i].firstChild);
            }
        }
    } else {
        while (o.firstChild) {
            o.removeChild(o.firstChild);
        }
    }
};


n.matches = (function() {
    var matchesFn;

    return function(el, selector) {
        if (!matchesFn) {
            var vendor = [
                'matches','webkitMatchesSelector','mozMatchesSelector',
                'msMatchesSelector','oMatchesSelector'
            ];

            for (var i = 0; i < vendor.length; i++) {
                if (typeof document.body[vendor[i]] === 'function') {
                    matchesFn = vendor[i];
                    break;
                }
            }
        }
        return el && el[matchesFn](selector);
    };
})();

n.closestDescendent = function(el, selector) {
    if (is_array_like(el)) {
        el = el[0];
    }

    if (!el) {
        return false;
    }

    if (n.matches(el, selector)) {
        return el;
    }

    return el.querySelector(selector);
};
n.closest = function(el, selector) {
    if (is_array_like(el)) {
        el = el[0];
    }

    if (n.matches(el, selector)) {
        return el;
    }

    var parent;

    // traverse parents
    while (el) {
        parent = el.parentElement;

        if (n.matches(parent, selector)) {
            return parent;
        }
        el = parent;
    }

    return false;
};

n.text = function(o, new_text) {
    o = is_string(o) ? document.querySelectorAll(o) : o;

    if (!isset(new_text)) {
        if (is_array_like(o)) {
            o = o[0];
        }
        return o.textContent;
    }

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++) {
            o[i].textContent = new_text;
        }
    } else {
        o.textContent = new_text;
    }

    return o;
};

n.html = function(o, new_html) {
    o = is_string(o) ? document.querySelectorAll(o) : o;

    if (empty(new_html)) {
        if (is_array_like(o)) {
            o = o[0];
        }
        return o.innerHTML;
    }

    if (is_function(new_html)) {
        new_html = new_html();
    }

    if (is_string(new_html)) {
        if (is_array_like(o)) {
            for (var i = 0, len = o.length; i < len; i++) {
                o[i].innerHTML = new_html;
            }
        } else {
            o.innerHTML = new_html;
        }
        return;
    }

    n.removeAllChildren(o);

    if (is_array_like(new_html)) {
        function recursiveAppend(childList) {
            forEach(childList, function(child) {
                if (is_function(child)) {
                    child = child();
                }
                if (is_array_like(child)) {
                    recursiveAppend(child);
                    return;
                }
                n.appendChild(child, o);
            });
        }
        recursiveAppend(new_html);
    } else {
        n.appendChild(new_html, o);
    }

    return o;
};

n.value = function(o, new_value) {
    o = is_string(o) ? document.querySelectorAll(o) : o;

    if (!isset(new_value)) {
        if (is_array_like(o)) {
            o = o[0];
        }
        return o.value;
    }

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++) {
            o[i].value = new_value;
        }
    } else {
        o.value = new_value;
    }

    return o;
};

n.popValue = function(o) {
    o = is_string(o) ? document.querySelector(o) : o;

    if (empty(o))
        return null;

    var value = n.value(o);
    n.value(o, '');

    return value;
};

n.disabled = function(o, new_state) {
    o = is_string(o) ? document.querySelectorAll(o) : o;

    if (typeof new_state === 'undefined' || new_state == null) {
        if (is_array_like(o)) {
            o = o[0];
        }
        return o.disabled;
    }

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++) {
            o[i].disabled = new_state;
        }
    } else {
        o.disabled = new_state;
    }

    return o;
};

n.setAttribute = function(o, attr_key, attr_val) {
    if (attr_val === false) {
        return n.removeAttribute(o, attr_key);
    }

    o = is_string(o) ? document.querySelectorAll(o) : o;
    attr_val = (attr_val || '');
    attr_val = (attr_val === true) ? '' : attr_val;

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++) {
            o[i].setAttribute(attr_key, attr_val);
        }
    } else {
        o.setAttribute(attr_key, attr_val);
    }

    return o;
};

n.popAttribute = function(o, attr_key) {
    var value = n.getAttribute(o, attr_key);
    n.removeAttribute(o, attr_key);
    return value;
};

n.getAttribute = function(o, attr_key) {
    o = is_string(o) ? document.querySelector(o) : o;

    if (is_array_like(o))
        o = o[0];

    return o.getAttribute(attr_key);
};

n.hasAttribute = function(o, attr_key) {
    o = is_string(o) ? document.querySelector(o) : o;

    if (is_array_like(o))
        o = o[0];

    return o.hasAttribute(attr_key);
};

n.removeAttribute = function(o, attr_key) {
    o = is_string(o) ? document.querySelectorAll(o) : o;

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++) {
            o[i].removeAttribute(attr_key);
        }
    } else {
        o.removeAttribute(attr_key);
    }

    return o;
};

n.hide = function(o) {
    o = is_string(o) ? document.querySelectorAll(o) : o;

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++) {
            o[i].style.display = 'none';
        }
    } else {
        o.style.display = 'none';
    }

    return o;
};
n.show = function(o) {
    o = is_string(o) ? document.querySelectorAll(o) : o;

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++) {
            o[i].style.display = '';
        }
    } else {
        o.style.display = '';
    }

    return o;
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

n.addClass = function(o, className) {
    if (empty(o) || empty(className))
        return;

    o = is_string(o) ? document.querySelectorAll(o) : o;

    function do_stuff(el) {
        if (el.classList) {
            if (contains(className, ' ')) {
                forEach(className.split(' '), function(item) {
                    el.classList.add(item);
                });
            } else {
                el.classList.add(className);
            }
        } else if (!n.hasClass(el, className)) {
            el.className += ' ' + className;
        }
    }

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++)
            do_stuff(o[i]);
    } else do_stuff(o);

    return o;
};

n.removeClass = function(o, className) {
    if (empty(o) || empty(className))
        return;

    o = is_string(o) ? document.querySelectorAll(o) : o;

    function do_stuff(el) {
        if (el.classList) {
            if (contains(className, ' ')) {
                forEach(className.split(' '), function(item) {
                    el.classList.remove(item);
                });
            } else {
                el.classList.remove(className);
            }
        } else {
            var regExp = new RegExp('(?:^|\\s)' + className + '(?!\\S)', 'g');
            el.className = el.className.replace(regExp);
        }
    }

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++)
            do_stuff(o[i]);
    } else do_stuff(o);

    return o;
};

n.toggleClass = function(o, className) {
    if (empty(o) || empty(className))
        return;

    o = is_string(o) ? document.querySelectorAll(o) : o;

    function do_stuff(el) {
        if (el.classList) {
            el.classList.toggle(className);
        } else {
            if (n.hasClass(el, className)) {
                n.removeClass(el, className);
            } else {
                el.className += ' ' + className;
            }
        }
    }

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++)
            do_stuff(o[i]);
    } else do_stuff(o);

    return o;
};

n.hasClass = function(o, className) {
    if (empty(o) || empty(className))
        return false;

    o = is_string(o) ? document.querySelectorAll(o) : o;

    function do_stuff(el) {
        if (el.classList) {
            if (contains(className, ' ')) {
                forEach(className.split(' '), function(item) {
                    if (!el.classList.contains(item)) {
                        return false;
                    }
                });
                return true;
            } else {
                return el.classList.contains(className);
            }
        } else {
            var regExp = new RegExp('(?:^|\\s)' + className + '(?!\\S)', 'g');
            return el.className.match(regExp);
        }
    }

    if (is_array_like(o)) {
        for (var i = 0, len = o.length; i < len; i++)
            if (!do_stuff(o[i]))
                return false;
    } else return do_stuff(o);

    return true;
};


n.setClass = function(o, className, state) {
    if (state) {
        n.addClass(o, className);
    } else {
        n.removeClass(o, className);
    }

    return o;
};

/*
Misc utility
~~~~~~~~~~~~
*/


// copy the given text to the clipboard,
// this function must be called within a user-action event (ex: click)
n.copyTextToClipboard = function(text) {
    var textArea = document.createElement("textarea");

    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';


    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();

    var successful = true;
    try {
        successful = document.execCommand('copy');
    } catch (err) {
        successful = false;
    }

    document.body.removeChild(textArea);
    return successful;
};

n.isTextSelected = function(input) {
    if (typeof input.selectionStart == "number") {
        return input.selectionStart == 0 && input.selectionEnd == input.value.length;
    } else if (typeof document.selection != "undefined") {
        input.focus();
        return document.selection.createRange().text == input.value;
    }
};

n.elementInViewport = function(el) {
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while(el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
    }

    return (
        top < (window.pageYOffset + window.innerHeight) &&
        left < (window.pageXOffset + window.innerWidth) &&
        (top + height) > window.pageYOffset &&
        (left + width) > window.pageXOffset
    );
};