A utility for easily generating HTML.

    This utility does not check for syntax errors: exceptions are not caught.

    If no tag name is given, DIV will be used. The tag name must always come first.
    The order of all classes and attributes afterwards does not matter.

Child: >
    "nav > ul > li"

        <nav>
            <ul>
                <li></li>
            </ul>
        </nav>

Sibling: +
    "div + p + blockquote"

        <div></div>
        <p></p>
        <blockquote></blockquote>

Climb Up: ^
    "div + div > p > span + em ^ blockquote"

        <div></div>
        <div>
            <p><span></span><em></em></p>
            <blockquote></blockquote>
        </div>

    "div + div > p > span + em ^^ blockquote"

        <div></div>
        <div>
            <p><span></span><em></em></p>
        </div>
        <blockquote></blockquote>

Multiplication:
    "ul > li * 5"

        <ul>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>

    "ul > li > span * 3"

        <ul>
            <li>
                <span></span>
                <span></span>
                <span></span>
            </li>
        </ul>

    "ul > (li > span) * 3"

        <ul>
            <li>
                <span></span>
            </li>
            <li>
                <span></span>
            </li>
            <li>
                <span></span>
            </li>
        </ul>

    Multiplication by Zero

        "ul > li * 0"

            <ul></ul>

        "div > span * 0 + em"

            <div>
                <em></em>
            </div>

        "div > span * 0 ^ em"

            <div></div>
            <em></em>

Grouping
    "div > (header > ul > li * 2 > a) + footer > p"

        <div>
            <header>
                <ul>
                    <li><a></a></li>
                    <li><a></a></li>
                </ul>
            </header>
            <footer>
                <p></p>
            </footer>
        </div>

    "(div > dl > (dt + dd)*3) + footer > p"

        <div>
            <dl>
                <dt></dt>
                <dd></dd>
                <dt></dt>
                <dd></dd>
                <dt></dt>
                <dd></dd>
            </dl>
        </div>
        <footer>
            <p></p>
        </footer>

    "(ul > li * 3) * 2"

        <ul>
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <ul>
            <li></li>
            <li></li>
            <li></li>
        </ul>

    "div > (span + span) ^ div"

        <div>
            <span></span>
            <span></span>
        </div>
        <div></div>

    "ul > (li + li) > span"

        <ul>
            <li></li>
            <li>
                <span></span>
            </li>
        </ul>

    "div + (span + span) > strong"

        <div></div>
        <span></span>
        <span>
            <strong></strong>
        </span>

    "div + (.foo + (.bar) + .foobar) + div"

        <div></div>
        <div class="foo"></div>
        <div class="bar"></div>
        <div class="foobar"></div>
        <div></div>

Text:

    "p['lorem ipsum']"

        <p>lorem ipsum</p>

Counters:
    If you don't give a counter ID, the counter will be automatically assigned one starting from 0

        n.gen('p[attr={}]', 'blah')

            <p attr="blah"></p>

        n.gen('p["{}"]', 'blah')

            <p>blah</p>

        n.gen('p[a={}][b={}]', 'foo', 'bar')

            <p a="foo" b="bar"></p>

    You can also assign your own counter ID

        n.gen('p[a={foo}]', {foo: 'bar'});

            <p a="bar"></p>

    Multiplication with counters

        n.gen('ul > li * {}', 5)

            <ul>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>

    Using functions:

        n.gen('ul>li[item={}]*3', function(el, idx, len) {
            return 'foo-'+idx;
        });

            <ul>
                <li item="foo-0"></li>
                <li item="foo-1"></li>
                <li item="foo-2"></li>
            </ul>

        The callback function is always given those three parameters: el, idx and len

            el   - the current element
            idx  - the current index
            len  - how many elements have the same callback Id (in the above example, it'd be 3)

    If you give a counter ID that's the same as an automatically assigned one, then...

        n.gen('p[a={}][b={0}][c={1}][d={}]', 'foo', 'bar')

            <p a="foo" b="foo" c="bar" d="bar"></p>

        The automatic counters increment normally for attributes "a" and "b"
        without care of whatever the named parameters are.
        And attributes "b" and "c" are manually set to 0 and 1. So "a" and "b"
        end up using the same counter, and likewise for "c" and "d"

Special counters:
    Special counters start with '$'. These are just to make things easier
    for counters you might commonly use...

    List of special counters:
        $, $+   -> ascend numerically
        $+#     -> ascend numerically from #
        $-      -> descend numerically
        $-#     -> descend numerically from #
        $a      -> ascend alphabetically lowercase
        $A      -> ascend alphabetically uppercase

    Examples:

        n.gen('(ul>li[item={$}]*5) + (ul>li[item={$}]*3)')

            <ul>
                <li item="1"></li>
                <li item="2"></li>
                <li item="3"></li>
                <li item="4"></li>
                <li item="5"></li>
            </ul>
            <ul>
                <li item="1"></li>
                <li item="2"></li>
                <li item="3"></li>
            </ul>

        n.gen('ul>li[item={$}]*5')

            <ul>
                <li item="1"></li>
                <li item="2"></li>
                <li item="3"></li>
                <li item="4"></li>
                <li item="5"></li>
            </ul>

        n.gen('ul>li[item={$+}]*5')

            <ul>
                <li item="1"></li>
                <li item="2"></li>
                <li item="3"></li>
                <li item="4"></li>
                <li item="5"></li>
            </ul>

        n.gen('ul>li[item={$+10}]*5')

            <ul>
                <li item="10"></li>
                <li item="12"></li>
                <li item="13"></li>
                <li item="14"></li>
                <li item="15"></li>
            </ul>

        n.gen('ul>li[item={$-}]*5')

            <ul>
                <li item="5"></li>
                <li item="4"></li>
                <li item="3"></li>
                <li item="2"></li>
                <li item="1"></li>
            </ul>

        n.gen('ul>li[item={$-20}]*5')

            <ul>
                <li item="20"></li>
                <li item="19"></li>
                <li item="18"></li>
                <li item="17"></li>
                <li item="16"></li>
            </ul>

        n.gen('ul>li[item={$a}]*5')

            <ul>
                <li item="a"></li>
                <li item="b"></li>
                <li item="c"></li>
                <li item="d"></li>
                <li item="e"></li>
            </ul>

        n.gen('ul>li[item={$A}]*5')

            <ul>
                <li item="A"></li>
                <li item="B"></li>
                <li item="C"></li>
                <li item="D"></li>
                <li item="E"></li>
            </ul>
