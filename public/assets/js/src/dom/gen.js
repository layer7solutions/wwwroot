/*
n.gen
~~~~~

This module provides utility functions primarily for generating large HTML fragments

See usage.md for selector documentation
*/

// generate html - allows multiple elements
n.gen = function(selector) {
    if (n.isNode(selector))
        return selector;
    selector = selector.replace(/\n/g, ' ');
    var callbacks = Array.prototype.slice.call(arguments, 1);
    var result_node = n.gen.walk(selector, 0, callbacks).node;
    if (result_node == null) {
        return n.frag();
    }
    return n.gen.count(result_node, callbacks);
};

// generate html - only allows one element
n.gen1 = function(selector) {
    if (n.isNode(selector))
        return selector;
    selector = selector.replace(/\n/g, ' ');
    var callbacks = Array.prototype.slice.call(arguments, 1);
    var result_node = n.gen.create(selector, 0, callbacks).node;
    if (result_node == null) {
        return n.frag();
    }
    return n.gen.count(result_node, callbacks);
};

n.genTag = function(tagName) {
    if (n.isNode(tagName))
        return tagName;
    return document.createElement(tagName);
};

n.simple_gen = function(element, attrList) {
    if (typeof element == "string") {
        element = document.createElement(element);
    }

    if (!isset(attrList)) {
        return element;
    }

    for (var attrKey in attrList) {
        if (attrList.hasOwnProperty(attrKey)) {
            var lkey = attrKey.toLowerCase();
            var value = attrList[attrKey];

            if (typeof value == 'undefined' || attrList[attrKey] == null) {
                continue;
            }

            if (lkey == 'textcontent' || lkey == 'text') {
                element.textContent = value;
                continue;
            }

            if (lkey == 'innerhtml' || lkey == 'html') {
                if (typeof value === 'function') {
                    value = value(element);
                }
                n.html(element, value);
                continue;
            }

            if (startsWith(lkey, 'on')) {
                n.on(element, attrKey.substring(2), value);
                continue;
            }

            if (typeof value === 'boolean') {
                if (value) {
                    element.setAttribute(attrKey, '');
                } else {
                    element.removeAttribute(attrKey);
                }
                continue;
            }

            if (startsWith(lkey, '+') && isset(element.getAttribute(attrKey = attrKey.slice(1)))) {
                element.setAttribute(attrKey, element.getAttribute(attrKey) + ' ' + value);
            } else {
                element.setAttribute(attrKey, value);
            }
        }
    }

    return element;
};

n.create = function() {
    var a = [].slice.apply(arguments),
        l = a.length,
        el,
        attrList = {},
        children = [],
        a_offset = 1;

    if (l == 0) {
        return n.genTag('div');
    }

    el = a[0];

    if ((is_object(el) || is_array(el)) && !n.isNode(el)) {
        el = null;
        a_offset = 0;
    } else if (is_string(el)) {
        // this regex tests if valid tag name
        if (/^[a-zA-Z][a-zA-Z\-]+[a-zA-Z]$/.test(el)) {
            el = n.genTag(el);
        } else {
            el = n.gen1(el);
        }
    }

    var component = null;

    for (var i = a_offset; i < l; i++) {
        var tmp = a[i];

        if (typeof tmp === 'function') {
            tmp = tmp(el);
        }

        if (is_array(tmp) || n.isNode(tmp) || is_string(tmp) || n.isFragment(tmp)) {
            children.push(tmp);
        } else if (is_object(tmp)) {
            if (tmp.hasOwnProperty('render')) {
                component = copy(tmp);
            } else {
                extend(attrList, tmp);
            }
        }
    }

    if (!component) {
        el = n.simple_gen(el || n.genTag('div'), attrList);
    } else {
        el = el || document.createDocumentFragment();

        if (component.hasOwnProperty('init')) {
            component.init.call(component, attrList);
        }
        children = component.render.call(component, attrList);
    }

    if (children) {
        if (typeof children === 'function') {
            children = children(el);
        }

        if (is_array(children) && children.length == 1) {
            children = children[0];
        }

        if (is_array(children)) {
            function f(child) {
                if (typeof child === 'function') {
                    child = child(el);
                }

                if (is_array(child)) {
                    forEach( flatten(child), f);
                    return;
                }

                if (is_string(child)) {
                    child = document.createTextNode(child);
                }
                if (!n.isNode(child) && !n.isFragment(child)) {
                    return;
                }
                el.appendChild(child);
            }

            forEach(children, f);
        } else if (n.isNode(children)) {
            el.appendChild(children);
        } else if (n.isFragment(children)) {
            el.appendChild(children);
        } else if (is_string(children)) {
            if (contains(n.create.text_elements, el.tagName.toLowerCase())) {
                el.appendChild(document.createTextNode(children));
            } else {
                el.appendChild(n.frag(children));
            }
        } else {
            return false;
        }
    }

    if (isset(component) && n.isFragment(el) && el.firstChild === el.lastChild) {
        return el.firstChild;
    }

    return el;
};

n.create.text_elements = [
    'a', 'u', 's', 'i', 'b', 'em', 'sup', 'sub', 'strong', 'span', 'small', 'code',
    'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];

n.create.text = function(element, text) {
    return n.create(element, {'text': text});
};

// Handles counters

// Parameters:
//   node: an html element whose children or itself has any attributes
//         by the name of the variable "n.gen.cattr" which contains the
//         counter token(s) identifying that counter
//
//         The counter token should be in this format:
//             'counter_id:subject'
//           or if subject is 'attr':
//             'counter_id:subject:attr_key'
//
//   callbacks: an array of items that will be converted into an object
//     these items can be: functions, objects, strings, numbers
//     if an item is an object, the object's properties will be merged in

n.gen.cattr    = 'data-n-counter';
n.gen.cstart   = '{';
n.gen.cend     = '}';

n.gen.count = function(node, callbacks) {
    // This count function needs to be called after mutliplication
    // Because before mutliplying, it would be hard to tell how many counters there are
    var list = Array.prototype.slice.call(node.querySelectorAll('['+n.gen.cattr+']')),
        indices = {};

    // Check "node" since performing querySelectorAll on it doesn't include itself
    if (!(node instanceof DocumentFragment) && node.hasAttribute(n.gen.cattr))
        list.unshift(node);

    // Return if no counters
    if (!list.length)
        return node;

    // Convert callbacks to object
    callbacks = n.gen.count.convert_callbacks(callbacks);

    // keep track of how many of a counter there is
    var len_count = {};

    // Run callback from callback id, if no callback exists, return null
    function run_callback(id, el, idx, token) {
        // Figure out how much of this counter there is if we haven't figured it out already
        if (!contains(len_count, token)) {
            var tmp = node.querySelectorAll('['+n.gen.cattr+'~="'+token+'"]').length;
            if (!(node instanceof DocumentFragment) &&  node.hasAttribute(n.gen.cattr) &&
                    contains(node.getAttribute(n.gen.cattr), token))
                tmp++;
            len_count[token] = tmp;
        }

        if (startsWith(id, '$'))
            return n.gen.run_special_callback(el, idx, len_count[token], id);
        if (id in callbacks) {
            if (typeof callbacks[id] == 'function')
                return callbacks[id](el, idx, len_count[token]);
            else
                return callbacks[id];
        }
        return null;
    }

    // Loop over list of all elements with counters
    for (var i = 0, len = list.length; i < len; i++) {
        var el = list[i];
        var data = el.getAttribute(n.gen.cattr).split(' ');

        // Loop over each counter in the element
        for (var j = 0; j < data.length; j++) {
            var token    = data[j],
                cdata    = token.split(':'),
                id       = cdata[0],
                subject  = cdata[1],
                idx;

            // Update the index
            if (contains(indices, token)) {
                idx = (indices[token] = indices[token] + 1);
            } else {
                idx = (indices[token] = 0);
            }

            // Run the callback with the target element and index
            var val = run_callback(id, el, idx, token);
            // Set val to empty string if callback returned nothing...
            if (typeof val === 'undefined' || val === null) {
                val = '';
            }

            // Update the counter with the callback result value
            if (subject == 'attr') {
                n.gen.count.check(subject, id, el, val, cdata[2]);
            } else {
                n.gen.count.check(subject, id, el, val);
            }
        }
        // Remove attribute afterwards so we can check lengths...
        el.removeAttribute(n.gen.cattr);
    }

    return node;
};

n.gen.value_only = function(id, callbacks) {
    callbacks = n.gen.count.convert_callbacks(callbacks);
    if (typeof callbacks[id] == 'function')
        return callbacks[id]();
    else
        return callbacks[id];
};

// Convert callbacks to an object
n.gen.count.convert_callbacks = function(callbacks) {
    var tmp = {};
    for (var i = 0, j = 0, len = callbacks.length; i < len; i++) {
        var callback = callbacks[i];

        if (typeof callback === 'function') {
            tmp[j++] = callback;
        } else if (is_string(callback)) {
            tmp[j++] = callback;
        } else if (typeof callback == 'object') {
            // if an item is an object, merge it
            for (var propName in callback) {
                if (callback.hasOwnProperty(propName)) {
                    tmp[propName] = callback[propName];
                }
            }
        } else {
            tmp[j++] = callback;
        }
    }
    return tmp;
};

// provide 'attr_key' param only if subject is 'attr'
n.gen.count.check = function(subject, id, el, val, attr_key) {
    id = n.gen.cstart+id+n.gen.cend;
    switch (subject) {
        case 'attr':
            // Check if attribute key name contains a counter
            if (contains(attr_key, n.gen.cstart)) {
                var new_attr_key = string_replace(attr_key, id, val);
                el.setAttribute(new_attr_key, el.getAttribute(attr_key));
                attr_key = new_attr_key;
            }
            // Fall through
        case 'class':
        case 'id':
            var attr_name = subject == 'attr' ? attr_key : subject;
            el.setAttribute(attr_name, string_replace(el.getAttribute(attr_name), id, val) );
            break;
        case 'text':
            el.textContent = string_replace(el.textContent, id, val);
    }
};


// Special/default counters
n.gen.counters = {
    ascend: function(el, idx, len) {
        return idx+1;
    },
    ascendOffset: function(el, idx, len, offset) {
        return idx + offset;
    },

    descend: function(el, idx, len) {
        return len-(idx);
    },
    descendOffset: function(el, idx, len, offset) {
        return offset - idx;
    },

    alphaLower: function(el, idx, len) {
        return String.fromCharCode(97 + idx);
    },
    alphaUpper: function(el, idx, len) {
        return String.fromCharCode(65 + idx);
    },
};

// Handle special counters (counters starting with $)
n.gen.run_special_callback = function(el, idx, len, id) {
    var action = id.substring(1);
        action = action.substring(action.indexOf('$')+1);

    if (action == '' || action == '+') {
        return n.gen.counters.ascend(el, idx, len);
    } else if (startsWith(action, '+')) {
        return n.gen.counters.ascendOffset(el, idx, len, parseInt(action.substring(1)));
    }

    if (action == '-') {
        return n.gen.counters.descend(el, idx, len);
    } else if (startsWith(action, '-')) {
        return n.gen.counters.descendOffset(el, idx, len, parseInt(action.substring(1)));
    }

    if (action == 'a') {
        return n.gen.counters.alphaLower(el, idx, len);
    } else if (action == 'A') {
        return n.gen.counters.alphaUpper(el, idx, len);
    }
};



// The walk function is for handling delimiters between element definitions and also groups and multiplication


// Delimiters such as: '+', '>' and '^'
// Multiplication is handled after everything else because the node(s) being multiplied could have children

n.gen.walk = function(selector, nextCounter, callbacks) {
    if (!selector || !selector.length)
        return document.createElement('div');

    nextCounter = nextCounter || 0;
    callbacks   = callbacks   || [];

    var container = document.createDocumentFragment(),
        action = '>',
        startOffset = 0,
        workingNode = container,
        multiply_list = [],
        remove_list = [];

    // Assuming that at paren_offset, there is an open parenthesis, this function
    // finds the associated closing parenthesis
    function paren_match(paren_offset) {
        for (var j = paren_offset, k = 0, len = selector.length; j < len; j++) {
            var c = selector.charAt(j);
            if (c == '(')
                k++;
            if (c == ')') {
                if (k == 0) return j;
                k--;
            }
        }
        return -1;
    }

    while (true) {
        var result = n.gen.create(selector, startOffset, nextCounter),
            newIndex = result.index + 1,
            nextCounter = result.nextCounter;

        // Look ahead to see if the next "node" is a group
        // If so, pretend that group is a single node and rewrite the current result
        if (result.next == '(') {
            var groupSelector = selector.substring(newIndex, newIndex = paren_match(newIndex)),
                group_walk_result = n.gen.walk(groupSelector, nextCounter, callbacks); // walk the group
            result.node = group_walk_result.node;
            nextCounter = result.nextCounter = group_walk_result.nextCounter;

            var walk_del_result = n.gen.walk_to_delimiter(selector, newIndex+1);
            result.index = walk_del_result.index;
            result.next = walk_del_result.next;

            newIndex = result.index + 1;
        }

        // Add the node to be multiplied to the multiplication list and handle multiplication after this loop
        if (result.next == '*') {
            var walk_del_result = n.gen.walk_to_delimiter(selector, newIndex);

            result.index = walk_del_result.index;
            result.next = walk_del_result.next;

            var factor = selector.substring(newIndex, result.index).trim(),
                multiply_times = parseInt(factor),
                is_frag = n.isFragment(result.node);

            if (startsWith(factor, '{') && endsWith(factor, '}')) {
                var mult_callback_name = factor.substring(1, factor.length-1);
                if (mult_callback_name.length == 0) {
                    mult_callback_name = nextCounter++;
                }
                multiply_times = n.gen.value_only(mult_callback_name, callbacks);
            }

            if (multiply_times && multiply_times > 1) {
                // for some reason, if result.node is a fragment, I have to clone the node before
                // pushing it to the list otherwise it won't work
                // but if it isn't a fragment but rather a single node, it actually will not work
                // if I clone the node (maybe something to do with cloning an already cloned node
                // breaks something?)
                // Maybe having to clone a fragment but not a single node is a safeguard against
                // reusing elements already in the DOM?
                multiply_list.push({
                    node:       is_frag ? result.node.cloneNode(true) : result.node,
                    refnode:    is_frag ? result.node.lastElementChild : result.node,
                    isfrag:     is_frag,
                    times:      multiply_times,
                });
            }

            // if times 0, then add the node being "multiplied" to list of nodes to be removed
            if (multiply_times != null && multiply_times == 0) {
                if (is_frag) {
                    for (var k = 0; k < result.node.children.length; k++) {
                        remove_list.push(result.node.children[k]);
                    }
                } else {
                    remove_list.push(result.node);
                }
            }

            newIndex = result.index + 1;
        }

        // Handle whatever the action is
        switch (action) {

            case '^':
                // Climb-Up action
                // Set the working node to its parent then run Sibling Action
                workingNode = workingNode.parentNode;

                // fall-through
            case '+':
                // Sibling Action
                if (result.node) {
                    n.insertAfter(result.node, workingNode);

                    if (n.isFragment(result.node)) {
                        // if fragment, then set the new working node to the last immediate child element of the fragment
                        workingNode = workingNode.parentNode.lastElementChild;
                    } else {
                        workingNode = result.node;
                    }
                }
                break;

            case '>':
                // Child action
                if (result.node) {
                    workingNode.appendChild(result.node);

                    // we can't set the working node to a fragment (no parent information), so if
                    // the working node is a fragment then set the new working node to the last
                    // element in the current working node (which should be the last immediate child element of the fragment)

                    if (n.isFragment(result.node)) {
                        workingNode = workingNode.lastElementChild;
                    } else {
                        workingNode = result.node;
                    }
                }
                break;
        }

        // Set the action for the next node result
        if ((action = result.next) == null) {
            break;
        }

        startOffset = newIndex;
    }

    // Handle multiplication
    for (var j = 0; j < multiply_list.length; j++) {
        var item  = multiply_list[j],
            times = item.times - 1, // minus 1 to count for original node
            ref   = item.refnode;
        for (var k = 0; k < times; k++) {
            n.insertAfter(item.node.cloneNode(true), ref);
        }
    }

    // Remove nodes in this list
    for (var j = 0; j < remove_list.length; j++) {
        n.removeNode(remove_list[j]);
    }

    // If fragment contains only one immediate child, return that child
    // Otherwise return fragment
    return {node: container.firstElementChild == container.lastElementChild ?
            container.firstElementChild : container,
            nextCounter: nextCounter,
            endWorkingNode: workingNode };
};

// Go to the next delimiter and returns the index it is at and what the delimiter is
n.gen.walk_to_delimiter = function(selector, startOffset) {
    var next_step = null;
    for (var len = selector.length; startOffset < len; startOffset++) {
        var c = selector.charAt(startOffset);
        if (n.gen.is_delimiter(c)) {
            next_step = c;
            break;
        }
    }
    return {  next: next_step,
             index: startOffset, }
};

// Check if a character is a delimiter
n.gen.is_delimiter = function(c, subject) {
    if (subject == 'attr' || subject == 'text')
        return false;
    switch (c) {
        case '>':
        case '^':
        case '+':
        case '(':
        case '*':
            return true;
    }
};


// The create function is for creating an element from the element definition
// This is an internal function, use n.gen1 instead

// Parameters:
//    selector: the selector, described below
//    startOffset: start offset for selector string
//    nextCounter: if no counter id is specified, we need to give it one
//                 the walk function keeps track of the counter id and gives the current
//                 counter id to this function
//                 If this function should encounter a counter, nextCounter should be incremented
//                 and the new value returned back to the walk function
//
//    startOffset and nextCounter are optional, 0 will be used if not they are not specified

// Selector info:
//    The tag name must be at the start, the order of the rest does not matter
//    If no tag name is specified, 'div' will be used
//
//    Multiple attributes must be separated like this: [a=value1][b=value2]
//    If no value is specified for an attribute (ex. '[disabled]'), an empty string value will be assigned
//
//    Quotes are not required for text content or attribute values
//    However, without quotes, starting and ending whitespace will be trimmed off
//
//    Single and double quotes are interchangeable
//    Attribute keys cannot have any quotes

n.gen.create = function(selector, startOffset, nextCounter) {
    var el = null,
        sb = '',               // string builder
        c = null,              // current character
        i = startOffset || 0,  // start offset for 'selector'
        next_step = null,      // the next action for the walk function
        subject = 'tag',       // the first subject should always be the tag
        subject_map = {        // maps symbol to subject
            '.': 'class',
            '#':    'id',
            '[': 'attr-or-text',
            ']':  'null',
        };
        nextCounter = nextCounter || 0;

    function remove_quotes(str) {
        str = str.trim();
        if (startsWith(str, '"') || startsWith(str, "'"))
            return str.substring(1, str.length-1);
        return str;
    }

    // checks if current subject is attribute or text
    function is_attr_text() {
        return (subject == 'attr-or-text' || subject == 'attr' || subject == 'text');
    }

    // Evaluates the stringbuilder text based on whatever the current subject is
    // Then changes the subject to whatever is next
    function subject_end() {
        switch (subject) {
            case 'tag':
                el = document.createElement(sb.length ? sb : 'div');
                break;
            case 'class':
                n.addClass(el, attach_counter(sb));
                break;
            case 'id':
                el.setAttribute('id', attach_counter(sb));
                break;
            case 'attr':
            case 'attr-or-text':
                var isTextContent = false;
                sb = sb.trim();

                // Check if text content
                if (startsWith(sb, '"') || startsWith(sb, "'")) {
                    if (contains(sb, '=')) {
                        var tmp = sb.split('=')[0].trim();
                        isTextContent = !( endsWith(tmp, "'") || endsWith(tmp, '"') );
                    } else {
                        isTextContent = true;
                    }
                }

                if (isTextContent) {
                    subject = 'text';
                    el.textContent = attach_counter(sb = remove_quotes(sb));
                } else {
                    subject = 'attr';
                    var data = sb.split('='), attr_val,
                        attr_key = attach_counter(data[0], null, true); // don't add counter attribute with possibly incorrect key

                    attr_key = attach_counter(attr_key, attr_key);
                    attr_val = attach_counter(typeof data[1] == 'undefined' ? '' : remove_quotes(data[1]), attr_key);

                    el.setAttribute(attr_key, attr_val);
                }
                break;
            case 'text':
                el.textContent = attach_counter(sb = remove_quotes(sb));
                break;
        }
        sb = '';
        subject = subject_map[c]; // set the next subject
    }

    function is_symbol() {
        if (subject_map.hasOwnProperty(c)) {
            return !(subject_map[c] != 'null' && is_attr_text());
        }
        return false;
    }

    // If there's a counter symbol, make note of it by attaching a special attribute to the element
    function attach_counter(text, attr_key, skipTagging, startOffset) {
        // Check for counter symbol
        if (!contains(text, n.gen.cstart))
            return text;

        // If custom counter id specified, use that, otherwise assign one

        var counterId    = null,
            startOffset  = startOffset || 0,
            startIdx     = text.indexOf(n.gen.cstart, startOffset), // counter start index
            endIdx       = text.indexOf(n.gen.cend, startIdx);

        // If startIdx and endIdx are not right next to each other, then there is a custom id,
        // otherwise assign id and insert that id...
        if (startIdx + 1 != endIdx) {
            counterId = text.substring(startIdx+1, endIdx).trim();
            // a counter starting with $ is a special counter, but we need to give it an id then
            // change it from "$whatever" to "$ID$whatever"
            if (startsWith(counterId, '$')) {
                var origCounterId = counterId;
                counterId = ('$' + (nextCounter++));
                text = string_insert(text, startIdx+1, counterId);
                counterId = counterId + origCounterId;
            }
        } else {
            counterId = nextCounter++;
            text = string_insert(text, startIdx+1, counterId);
        }

        // Check if there's another counter in the text
        var nextIdx = text.indexOf(n.gen.cstart, endIdx);
        if (nextIdx != -1) {
            text = attach_counter(text, attr_key, skipTagging, nextIdx);
        }

        // Skip adding the attribute if this parameter is true...
        if (skipTagging)
            return text;

        // Set the temperorary counter attribute
        counterId = counterId+':'+subject+(subject == 'attr' ? ':'+attr_key : '');
        if (el.hasAttribute(n.gen.cattr)) {
            el.setAttribute(n.gen.cattr, el.getAttribute(n.gen.cattr) + ' ' + counterId);
        } else {
            el.setAttribute(n.gen.cattr, counterId);
        }

        return text;
    }

    // Append any char unless if space and subject is not attr or text
    function append_char() {
        if (c != ' ' || (c == ' ' && is_attr_text()) ) sb += c;
    }

    // Loop over selector until hit end or delimiter
    for (var len = selector.length; i < len; i++) {
        c = selector.charAt(i);

        // If delimiter, exit
        if (n.gen.is_delimiter(c, subject) && !is_attr_text()) {
            next_step = c;
            break;
        }

        // If symbol, end and change the subject, otherwise append char
        if (is_symbol())
            subject_end();
        else
            append_char();
    }
    subject_end(); // handle last stringbuilder/subject since there is no "end" symbol

    return {
        node: startOffset == i ? null : el,
        next: next_step,
        index: i,
        nextCounter: nextCounter,
    };
};