/*
dialog.js
~~~~~~~~~

This file provides utilities for showing alert/modal dialog boxes.
*/

app.DIALOG_ALERT = 0;
app.DIALOG_MODAL = 1;
app.DIALOG_ERROR = 2;
app.DIALOG_TOAST = 3;

app.dialog = function(contents, dialog_type, opts) {
    app.dialog.close();
    if (!isset(contents)) {
        return;
    }

    opts = opts || {};

    var inner, type_name;

    dialog_type = dialog_type || 0;

    if (dialog_type == app.DIALOG_ALERT) {
        type_name = 'alert';
        inner = n([
            n('.AppDialog_Content', {
                html: contents
            }),
            n('.AppDialog_ButtonGroup', [
                n('button.tertiary.AppDialog_CloseTrigger', 'OK')
            ])
        ]);
    } else if (dialog_type == app.DIALOG_MODAL) {
        type_name = 'modal';
        inner = n([
            n('.AppDialog_Content', {
                html: contents,
            }),
            n('.close.small.AppDialog_CloseTrigger')
        ]);
    } else if (dialog_type == app.DIALOG_ERROR || dialog_type == app.DIALOG_TOAST) {
        type_name = 'toast';
        inner = n([
            n('i', {
                'class': 'icon ' + (opts.icon || (dialog_type == app.DIALOG_ERROR ? 'zmdi zmdi-alert-triangle' : 'zmdi zmdi-info')),
            }),
            n('.AppDialog_Content', {
                html: contents,
            }),
            n('.close.small.AppDialog_CloseTrigger')
        ]);
    }

    n.select(document.body).append(
        n('.AppDialogOuter[data-type={}]'.format(type_name), [
            n('.AppDialog[data-type={}]'.format(type_name), [
                n('.AppDialog_Inner', {
                    'html': inner
                })
            ])
        ])
    );

    setTimeout(function() {
        var selInput = document.querySelector('.DialogSelectAll');
        if (selInput) {
            selInput.focus();
            selInput.select();
            selInput.scrollTop = 0;
        }
    }, 0);

    n.on(document.body, 'keyup', app.dialog.active_listener);
};
app.dialog.active_listener = function(e) {
    var tag = e.target.tagName.toUpperCase();

    var key = e.which || e.keyCode || 0;
    if (key === 13 && tag != 'TEXTAREA' && tag != 'INPUT') app.dialog.close();
    if (key === 27) app.dialog.close();
};
app.dialog.close = function() {
    n.remove('.AppDialogOuter');
    n.off(document.body, 'keyup', app.dialog.active_listener);
};

app.dialog.confirm_terms_notice = function() {
    var element = document.querySelector('.TermsOfServiceNotice');

    n.addClass(element, 'confirmed');
    setTimeout(function() {
        n.remove(element);
    }, 200);

    r('/identity/mark_terms_seen', {
        success: function(result) {
            if (result['success'] !== true) {
                console.log('confirm_terms_notice: internal error');
            } else {
                console.log('confirm_terms_notice: success');
            }
        },
        error: function(error) {
            console.log('confirm_terms_notice: internal error');
        },
    });
};

init('AppDialog', function() {
    n.on(document.body, 'click', function(event) {
        if (n.hasClass(event.target, 'AppDialog_CloseTrigger')) {
            app.dialog.close();
        }
    });
    n.on('.TermsOfServiceNotice__confirm', 'click', function() {
        app.dialog.confirm_terms_notice();
    });
});