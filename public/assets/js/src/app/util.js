/*
util.js
~~~~~~~
application specific utilities
*/
app.util = {
    media_info_cache: Dict(),
    media_info: function(media_url, callback) {
        if (app.util.media_info_cache.contains(media_url)) {
            callback(app.util.media_info_cache.get(media_url));
            return;
        }

        r('/data/media_info', {
            data: {
                url: media_url,
            },
            success: function(result) {
                if (result['platform'] == 'unknown') {
                    result = false;
                }

                app.util.media_info_cache.put(media_url, result);
                callback(result);
            },
        });
    },
    moderators: {
        cache: {
            real: Dict(),
            logged: Dict(),
        },
        get_logged: function(subreddits, callback) {
            return app.util.moderators._get('logged', subreddits, callback);
        },
        get: function(subreddits, callback) {
            return app.util.moderators._get('real', subreddits, callback);
        },
        _get: function(type, subreddits, callback) {
            if (!app.util.moderators.cache.hasOwnProperty(type)) {
                callback(false);
                return;
            }

            subreddits = is_array(subreddits) ? subreddits : subreddits.split(",");

            var srlist = [];

            var res0 = reduceToMap(subreddits, function(acc, subreddit) {
                var srLower = subreddit.toLowerCase();
                if (app.util.moderators.cache[type].contains(srLower)) {
                    acc[subreddit] = app.util.moderators.cache[type].get(srLower);
                } else {
                    srlist.push(subreddit);
                }
            });

            if (!srlist.length) {
                callback(res0);
                return;
            }

            r('/r/{}/info'.format(srlist.join(',')), {
                data: {
                    fields: 'moderators.' + type,
                },
                success: function(res1) {
                    res1 = res1['moderators'][type];

                    forEach(res1, function(subreddit, moderators) {
                        app.util.moderators.cache[type].put(subreddit.toLowerCase(), moderators);
                    });

                    callback(extend(res0, res1));
                },
                error: function(error) {
                    callback(false);
                },
            });
        },
    },
    create_media_icon: function(platform) {
        platform = platform.toLowerCase();
        switch (platform) {
            case 'yt':
            case 'youtube':
                return '<i class="icon media-icon media-icon--youtube zmdi zmdi-youtube"></i>';
            case 'vimeo':
                return '<i class="icon media-icon media-icon--vimeo zmdi zmdi-vimeo"></i>';
            case 'vid.me':
            case 'vidme':
                return '<img class="icon media-icon media-icon--vidme" src="'+
                    SITE_URL+'assets/images/media-host/vidme_icon.svg" />';
            case 'dailymotion':
                return '<img class="icon media-icon media-icon--dailymotion" src="'+
                    SITE_URL+'assets/images/media-host/dailymotion_icon.svg" />';
            case 'instagram':
                return '<i class="icon media-icon media-icon--instagram zmdi zmdi-instagram"></i>';
            case 'soundcloud':
                return '<i class="icon media-icon media-icon--soundcloud zmdi zmdi-soundcloud"></i>';
            case 'twitch':
                return '<i class="icon media-icon media-icon--twitch zmdi zmdi-twitch"></i>';
            case 'spotify':
                return '<i class="icon media-icon media-icon--spotify fa fa-spotify"></i>';
            case 'tumblr':
                return '<i class="icon media-icon media-icon--tumblr zmdi zmdi-tumblr"></i>';
            case 'twitter':
                return '<i class="icon media-icon media-icon--twitter zmdi zmdi-twitter"></i>';
            case 'vine':
                return '<i class="icon media-icon media-icon--vine fa fa-vine"></i>';
            case 'facebook':
                return '<i class="icon media-icon media-icon--facebook zmdi zmdi-facebook-box"></i>';
            case 'etsy':
                return '<i class="icon media-icon media-icon--etsy fa fa-etsy"></i>';
        }
        return '';
    },
    human_timing_update_timeout: null,
    human_timing_update_start: function() {
        var list = document.querySelectorAll('.human-timing[data-timestamp]');

        forEach(list, function(el) {
            var timestamp = parseInt(el.getAttribute('data-timestamp'));
            var new_timing = human_timing(timestamp);
            n.setAttribute(el, "title", timeConvert(timestamp));
            n.html(el, new_timing);
        });

        app.util.human_timing_update_timeout = setTimeout(app.util.human_timing_update_start, 10000);
    },
    human_timing_update_stop: function() {
        clearTimeout(app.util.human_timing_update_timeout);
    },
    human_timing_update_restart: function() {
        app.util.human_timing_update_stop();
        app.util.human_timing_update_start();
    },
    sanitize: {
        reddit: {
            regex: {
                thing: /^https?:\/\/((www\.)?reddit.com\/(r\/[A-Za-z0-9_\-]{1,21}\/)?comments\/[A-Za-z0-9]+(\/.*)?|redd\.it\/[A-Za-z0-9]+\/?)$|^((thing_)?t\d_)?[A-Za-z0-9]+$/i,
                username: /^((\/|((https?:\/\/)?www.)?reddit.com\/)?(user|u)\/)?([A-Za-z0-9_\-]{1,20}|Trust and Safety|Anti\-Evil Operations)\/?$/i,
            },
            pass: {
                username: function(value) {
                    var lowerValue = value.toLowerCase();
                    if (lowerValue === 'trust_and_safety' || lowerValue === 'trust and safety'
                            || lowerValue === 'trust-and-safety') {
                        return 'Trust and Safety';
                    }
                    if (lowerValue === 'anti-evil operations' || lowerValue === 'anti evil operations') {
                        return 'Anti-Evil Operations';
                    }
                    var parts = value.split('/');
                    var last_part = parts[parts.length - 1];
                    if (last_part.length == 0) { // if there's a trailing slash
                        last_part = parts[parts.length - 2];
                    }
                    return last_part;
                },
            }
        }
    },
    getBestFilterString: function(filter_obj, category, include_excluded) {
        var allString = category ? 'all:'+category : 'all';

        var opts = ({
            1: {prefix: allString, getter: 'nonValues', del: '-'},
            0: {prefix: false,     getter: 'values',    del: '+'},
        })[Math.round(filter_obj.getPercentage(include_excluded))];

        var res = filter_obj[opts.getter](include_excluded);
        if (opts.prefix)
            res.unshift(opts.prefix);

        return res.join(opts.del);
    }
};
/* URL related utilities */
app.url = {
    reddit_href: function(x) {
        return 'https://www.reddit.com/'+trim(x, '/ ');
    },
    check_url: function(URL, require_protocol) {
        var
            parts     = URL.split(/\/\/(.*)/), // split on first "//" only

            protocol  = (parts.length == 1) ? ""        :  parts[0],
            URN       = (parts.length == 1) ? parts[0]  :  parts[1];

        require_protocol = require_protocol || false;

        if (require_protocol && parts.length <= 1) {
            return false;
        }

        /*
        URN:
            ([a-zA-Z0-9]+\.)+       match at least 1 domain, and up to unlimited subdomains

            [a-zA-Z]{2,}            match TLD, at least 2 characters

            (\/(.*?))?$             match path, very complicated so not worth validating
                                    if the URL has at least a correct resource name that's
                                    enough for us
        PROTOCOL:
            we only care about http and https, also allow empty string if the given URL
            did not have a protocol
        */

        // noinspection UnnecessaryLocalVariableJS
        var result =
            /^([a-zA-Z0-9]+\.)+[a-zA-Z]{2,}(\/(.*?))?$/.test(URN) &&
            (require_protocol ?
                /https?:/.test(protocol)
                : /^$|https?:/.test(protocol)
                );

        return result;
    },
    add_protocol: function(URL, protocol) {
        protocol = rtrim(protocol || 'https', ':/');

        var
            parts     = URL.split(/\/\/(.*)/), // split on first "//" only
            URN       = (parts.length == 1) ? parts[0] : parts[1];

        return protocol + '://' + URN;
    },
};
init('AppUtil', function() {
    n.select('[data-media-icon-platform]').forEach(function(el) {
        el.innerHTML = app.util.create_media_icon(el.getAttribute('data-media-icon-platform'));
    });
});