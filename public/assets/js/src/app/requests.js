/*
requests.js
~~~~~~~~~~~

This file provides functions for creating and sending AJAX requests.
*/

function r() {
    var d = {}, f, args = Array.prototype.slice.call(arguments);

    (f = function(a) {
        forEach(a, function(v) {
            if (is_string(v)) {
                if (contains(app.requests.methods, v.toUpperCase())) {
                    d['method'] = v;
                } else {
                    d['url'] = v;
                }
            } else if (is_object(v)) {
                if (is_array(v)) {
                    f(v);
                } else {
                    forEach(v, function(vk,vv) {
                        if (vk === 'data') {
                            vk = 'params';
                        }
                        d[vk] = vv;
                    });
                }
            }
        });
    })(args);

    return app.requests.send.call(null, d);
}

extend(r, {
    get: function() {
        r.apply(null, Array.prototype.slice.call(arguments).extend('GET'));
    },
    post: function() {
        r.apply(null, Array.prototype.slice.call(arguments).extend('POST'));
    },
    put: function() {
        r.apply(null, Array.prototype.slice.call(arguments).extend('PUT'));
    },
    delete: function() {
        r.apply(null, Array.prototype.slice.call(arguments).extend('DELETE'));
    }
});

init('AppRequests', function(opts) {
    var AUTH_TOKEN = opts.CSRF_TOKEN || null;
    var next_req_id = 0;
    var running_requests = {};

    app.requests = {
        methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS', 'HEAD'],
        getRunningCount: function() {
            return Object.keys(running_requests).length;
        },
        getRunningCountObj: function() {
            return running_requests;
        },
        prepare: {
            buildURI: function(uri, params) {
                var parts = [];

                if (uri.charAt(0) == '/') {
                    parts.push(SITE_API);
                }

                parts.push(uri);
                parts.push(uri.indexOf('?') !== -1 ? '&' : '?');
                parts.push(app.requests.prepare.encodeQueryData(params));

                return parts.join('');
            },
            encodeQueryData: function(data) {
                var ret = [];
                for (var d in data) {
                    if (data.hasOwnProperty(d)) {
                        ret.push(d + '=' + encodeURIComponent(data[d]));
                    }
                }
                return ret.join('&');
            },
        },
        send: function(data) {
            var http_method   = data['method']      || 'GET',
                no_auth       = data['no_auth']     || false,
                is_passive    = data['is_passive']  || false,
                params        = data['params']      || {},
                url           = app.requests.prepare.buildURI(data['url'], params),
                req_id        = next_req_id++;

            var request = new XMLHttpRequest();
            request.open(http_method, url, true);
            request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

            if (!no_auth && !empty(AUTH_TOKEN)) {
                request.setRequestHeader('X-CSRF-Token', AUTH_TOKEN);
            }

            function run_callback(callback_name, args_array) {
                var ret = false;

                if (is_function(data[callback_name])) {
                    ret = data[callback_name].apply(data, args_array || []);
                } else if (data[callback_name] === false) {
                    // If the callback itself is `false`, rather than returning false, then that
                    // means we explicitly decide not to handle this callback, and not proceed
                    // to the default handler
                    return;
                } else if (callback_name === 'status_error') {
                    // If there is no callback, and the callback type is 'status_error', then
                    // do nothing. Only if there is a callback that returns `false` do we proceed
                    // to the default 'status_error' handler
                    return;
                }

                if (ret === false) {
                    // Default handlers if callback not supplied.
                    //
                    // A callback can return `false` to manually trigger
                    // the default operation.
                    switch (callback_name) {
                        case 'start':
                        case 'receive':
                        case 'success':
                            // Do nothing
                            break;
                        case 'error':
                        case 'status_error': // see above comment regarding "status_error"
                        case 'parse_error':
                            if (args_array[0] === "NOT_AUTHORIZED"
                                    && args_array[2] && args_array[2].moreinfo) {
                                if (args_array[2].moreinfo[0] === "IP_CMP_FAIL") {
                                    app.dialog(
                                        "Network changed (you might've connected to or been " +
                                        "disconnected from a Wi-Fi network). " +
                                        "Refresh the page and try again.",
                                        app.DIALOG_ERROR);
                                } else if (args_array[2].moreinfo[0] === "TOKEN_EXPIRED") {
                                    app.dialog(
                                        "Authentication expired. Refresh the page and try again.",
                                        app.DIALOG_ERROR);
                                } else {
                                    app.dialog(
                                        "Authentication failed. Try logging in and back out.",
                                        app.DIALOG_ERROR);
                                }
                            } else {
                                // Show "internal error" dialog
                                app.dialog(
                                    "Sorry, an internal error occurred. " +
                                    "Please try again later.", app.DIALOG_ERROR);
                            }
                            break;
                        case 'network_error':
                            // Show "network error" dialog
                            app.dialog(
                                "A network error occurred, please check your internet " +
                                "connection and try again later.", app.DIALOG_ERROR);
                            break;
                    }
                }
            }

            var handlers = {
                on_response: function() {
                    delete running_requests[req_id];

                    var is_success = request.status >= 200 && request.status < 300,
                        label,
                        info,
                        response_token = request.getResponseHeader('X-CSRF-Token'),
                        content_type = request.getResponseHeader('Content-Type'),
                        raw_data = request.responseText,
                        res;

                    console.log('Response: ' + request.status + ' ' + content_type);
                    run_callback('receive', [request]);

                    if (!is_success) {
                        run_callback(label = 'status_error', [request]);
                    }
                    if (startsWith(raw_data, 'while(1);')) {
                        raw_data = raw_data.slice(9);
                    }

                    if (response_token) {
                        AUTH_TOKEN = response_token;
                        console.log('New Token: ' + AUTH_TOKEN.slice(0,6) + '..');
                    }

                    try {
                        switch (content_type) {
                            case 'application/json':
                                res = JSON.parse(raw_data);
                                if (res && res.error) {
                                    run_callback(label = 'error', info = [
                                        res.error || null,
                                        res.error_description || null,
                                        res.error_detail || null
                                    ]);
                                }
                                break;
                            default:
                                res = raw_data;
                                break;
                        }
                        if (is_success) {
                            run_callback(label = 'success', info = [res]);
                        }
                    } catch(e) {
                        console.log(e);
                        console.log(raw_data);
                        run_callback(label = 'parse_error', info = [raw_data]);
                    }
                    run_callback('done', [label, request.status].concat(info));
                },
                on_network_error: function() {
                    delete running_requests[req_id];

                    run_callback('receive', [request]);
                    run_callback('network_error');
                    run_callback('done', ['network_error', request]);
                },
                on_start: function() {
                    console.log(http_method + ': ' + url);
                    if (!is_passive)
                        running_requests[req_id] = true;
                },
                on_end: function() {
                    delete running_requests[req_id];
                },
            };

            request.onloadstart = handlers.on_start;
            request.onloadend = handlers.on_end;
            request.onerror = handlers.on_network_error;
            request.onload = handlers.on_response;

            run_callback('start', [request]);
            request.send();
        },
    };
});