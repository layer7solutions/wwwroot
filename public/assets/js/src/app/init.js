/*
init.js
~~~~~~~

Application init file.
*/

const app = {};
app.forms = {};

const PREREQUISITE_INIT_NAME = 'LoadInitOpts';

const init = (function() {
    var initopts = {};
    var STATUS_PENDING = {};
    var STATUS_COMPLETE = {};
    var AWAIT_PREQREQ = [];

    return function(initName, handler) {
        if (STATUS_PENDING[initName] || !is_string(initName)) {
            return;
        } else {
            STATUS_PENDING[initName] = true;
        }

        if (!STATUS_COMPLETE[PREREQUISITE_INIT_NAME] && initName !== PREREQUISITE_INIT_NAME) {
            STATUS_PENDING[initName] = false;
            AWAIT_PREQREQ.push(function() {
                init(initName, handler);
            }.bind(null));
            return;
        }

        var options = initopts[initName] || {};

        console.log('init: ' + initName);

        function handle_result(result) {
            if (initName === PREREQUISITE_INIT_NAME) {
                initopts = result;
            }

            STATUS_COMPLETE[initName] = true;
            console.log('init-complete: ' + initName);

            if (initName === PREREQUISITE_INIT_NAME) {
                forEach(AWAIT_PREQREQ, function(fn) {
                    fn();
                });
            }
        }

        if (/comp|inter|loaded/.test(document.readyState)) {
            // run handler immediantly if DOM Content is already done loading
            handle_result(handler(options));
        } else {
            document.addEventListener('DOMContentLoaded', function() {
                handle_result(handler(options));
            }, false);
        }
    };
})();

init(PREREQUISITE_INIT_NAME, function() {
    var element = document.head.querySelector('meta[name="x-init-opts"]'),
        loaded_opts = {};

    if (!element || this.loaded) {
        return loaded_opts;
    } else {
        element.parentNode.removeChild(element);
        this.loaded = true;
    }

    var safename_list = element.getAttribute('content').split(',');

    forEach(safename_list, function(safe_name) {
        var safe_data = element.getAttribute('data-' + safe_name);

        if (!safe_data)
            return;

        var opt_data = JSON.parse(safe_data),
            src_path = safe_name.split('-'),
            initName = src_path.shift(),
            opt_name = src_path.pop(),
            source;

        if (initName === 'var') {
            source = reduce(src_path, function(acc, item) {
                return acc[item];
            }, (src_path[0] === 'app') ? src_path.shift() && app : window);
        } else {
            source = loaded_opts[initName] || (loaded_opts[initName] = {});
        }

        source[opt_name] = opt_data;
    });

    return loaded_opts;
});

init('Basic', function(opts) {
    if (SITE_IS_STAGING) {
        var ael = Node.prototype.addEventListener,
            rel = Node.prototype.removeEventListener;
        Node.prototype.addEventListener = function (a, b, c) {
            console.log('Listener', 'added', '('+a+')', this, b, c);
            ael.apply(this, arguments);
        };
        Node.prototype.removeEventListener = function (a, b, c) {
            console.log('Listener', 'removed', '('+a+')', this, b, c);
            rel.apply(this, arguments);
        };
    } else {
        if (!console) window.console = {};
        console.log = function() {};
    }

    // THEME RELATED UTILITIES
    // ~~~~~~~~~~~~~~~~~~~~~~~
    app.tempSetNightMode = function(state) {
        if (state) {
            document.body.classList.add('nightmode');
            document.body.classList.remove('daymode');
        } else {
            document.body.classList.add('daymode');
            document.body.classList.remove('nightmode');
        }
    };

    // WINDOW UNLOAD LISTENERS
    // ~~~~~~~~~~~~~~~~~~~~~~~
    /* window.onbeforeunload = function() {
        if (isset(app.requests) && isset(app.requests.getRunningCount())) {
            if (app.requests.getRunningCount() > 0) {
                return "You have a pending request running. Are you sure you want to exit this page?";
            }
        }
    };*/

    // HASH CHANGE LISTENERS
    // ~~~~~~~~~~~~~~~~~~~~~

    function updateCurrentHashGlobal(first_call) {
        if (!window.location.hash)
            return;
        forEach('.add-hash', function(el) {
            var default_url = el.getAttribute('data-default') || el.getAttribute('href') || '';

            el.setAttribute('href', default_url + window.location.hash);

            if (first_call) {
                el.setAttribute('data-default', default_url);
            }
        });
    }

    updateCurrentHashGlobal(true);

    window.onhashchange = function() {
        updateCurrentHashGlobal();
    };

    // CURRENT URL CHANGE LISTENERS
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    const PUSH_STATE = window.history.pushState;
    const REPLACE_STATE = window.history.replaceState;

    function updateCurrentURLGlobal() {
        window['CURRENT_URL'] = window.location.href;
    }

    window.history.pushState = function() {
        PUSH_STATE.apply(window.history, arguments);
        updateCurrentURLGlobal();
    };
    window.history.replaceState = function() {
        REPLACE_STATE.apply(window.history, arguments);
        updateCurrentURLGlobal();
    };
    window.onpopstate = function() {
        updateCurrentURLGlobal();
    };

    // CREATE VUE INSTANCE
    // ~~~~~~~~~~~~~~~~~~~
    if (window.Vue) {
        new Vue({ el: '#website' });

        if (SITE_IS_STAGING) {
            Vue.config.devtools = true;
        }
    }
});