app.localSettings = function(name, defaultSettings) {
    var settings = {};

    function formatKey(key) {
        return name + "_" + key;
    }

    forEach(defaultSettings, function(key, defaultValue) {
        var currentValue = window.localStorage.getObject(formatKey(key));

        if (is_undef(currentValue)) {
            currentValue = defaultValue;
            window.localStorage.setObject(formatKey(key), currentValue);
        }

        settings[key] = currentValue;
    });

    var instance = {
        name: name,
        get: function(key) {
            if (!settings.hasOwnProperty(key)) {
                return undefined;
            }
            return settings[key];
        },
        has: function(key) {
            return settings.hasOwnProperty(key);
        },
        put: function(key, value) {
            window.localStorage.setObject(formatKey(key), value);
            settings[key] = value;
        },
        forEach: function(callback) {
            forEach(settings, callback);
        },
        _getInputElementData: function(el) {
            var type  = el.getAttribute('type').toLowerCase();
            var tag   = el.tagName.toUpperCase();
            var value = el.value;

            if (tag === 'TEXTAREA')
                type = 'textarea';

            if (tag === 'INPUT' && type === 'checkbox') {
                if (el.checked) {
                    value = el.hasAttribute('value') ? value : true;
                } else {
                    value = false;
                }
            }

            return {
                element: el, // element
                tag: tag, // tag name
                type: type, // input type
                value: value, // new value
                settingsKey: el.getAttribute('name'), // key name
                settingsValue: instance.get( el.getAttribute('name') ), // current value
            };
        },
        _onChange: null,
        _listener: function(event) {
            var data = instance._getInputElementData(this);
            instance.put(data.settingsKey, data.value);

            if (instance._onChange) {
                instance.onChange.apply(instance, {
                    element: data.element,
                    key: data.settingsKey,
                    value: data.value,
                    previous_value: data.settingsValue,
                });
            }
        },
        attach: function(formElement, onChange) {
            var inputElements = formElement.querySelectorAll('input[name], textarea[name]');

            this._onChange = onChange;

            forEach(inputElements, function(el) {
                var data = instance._getInputElementData(el);

                if (is_undef(data.settingsValue)) {
                    return;
                }

                switch (data.type) {
                    case 'radio':
                        el.checked = el.value == data.settingsValue;
                        break;
                    case 'checkbox':
                        el.checked = !!data.settingsValue;
                        break;
                    default:
                        el.value = data.settingsValue;
                        break;
                }
                n.on(el, 'change', instance._listener);
            });

        },
        deattach: function(formElement) {
            var inputElements = formElement.querySelectorAll('input[name], textarea[name]');
            n.off(inputElements, 'change', this._listener);
        },
    };
    return instance;
};