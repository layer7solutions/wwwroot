(function() {
    var _STATE = {};

    var _findInputs = function(elements, getValue) {
        return reduceToArray(elements, function(acc, el) {
            el = n.closestDescendent(el, 'input[type=checkbox]');
            if (el && getValue)
                el = el.getAttribute('data-value');
            if (el)
                acc.push(el);
        });
    };

    var _elements = function(o, one) {
        if (!o)
            return false;
        if (is_string(o))
            return document[one ? 'querySelector' : 'querySelectorAll'](o);
        return o;
    };

    var _inputByStateAndValue = function(state_name, value) {
        return document.querySelector(
            'input[type=checkbox][data-state="{}"][data-value="{}"]'.format(state_name, value));
    };

    app.forms.Filter = function(name, _opts) {
        if (name in _STATE)
            return _STATE[name];

        var instance = _STATE[name] = {
            state_name: name,

            check_inputs: _elements(_opts.inputs),
            toggle_all: _elements(_opts.toggle_all, true),
            value_excluded:
                reduceToMap(_opts.exclude_values || [], function(acc, item) { acc[item] = true; }),

            check_state: {},

            real_count: 0,
            real_total: 0,
            value_count: 0,
            value_total: 0,

            _events: {
                call: function(name, args) {
                    if (this[name] && is_function(this[name])) {
                        return this[name].apply(instance, args);
                    }
                },
                on_change: _opts.on_change,
                before_change: _opts.before_change,
            },

            // STATE MODIFICATION FUNCTIONS
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            getExcluded: function() {
                return Object.keys(instance.value_excluded);
            },

            setExcluded: function(items, new_state) {
                items = n.isNodeLike(items) ? _findInputs(items, true) : items;

                forEach(items, function(item) {
                    var prev_state = contains(instance.value_excluded, item);
                    if (prev_state && !new_state) {
                        delete instance.value_excluded[item];
                        if (_inputByStateAndValue(instance.state_name, item).checked) {
                            instance.value_count++;
                        }
                    } else if (!prev_state && new_state) {
                        instance.value_excluded[item] = true;
                        if (instance.check_state[item]) {
                            instance.value_count--;
                        }
                    }
                });

                var excluded_total = Object.keys(instance.value_excluded).length;

                instance.value_total = instance.real_total - excluded_total;

                instance.toggle_all.checked = (instance.value_count == instance.value_total);
            },

            check: function(elements) {
                forEach(_findInputs(elements), function(el) {
                    instance._toggleCheckValue(el, null, true);
                });
            },
            uncheck: function(elements) {
                forEach(_findInputs(elements), function(el) {
                    instance._toggleCheckValue(el, null, false);
                });
            },
            isChecked: function(names) {
                if (!is_array_like(names))
                    return this.check_state[names];
                return every(forEach(names, function(name) {
                    return instance.check_state[name];
                }), is_truthy);
            },
            clear: function() {
                this.real_count = 0;
                this.value_count = 0;

                forEach(this.check_inputs, function(el) {
                    el.checked = false;
                });
                forEach(this.check_state, function(key, value) {
                    instance.check_state[key] = false;
                });
            },

            // STATE GET & STATISTICAL FUNCTIONS
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            isAllChecked: function(include_excluded) {
                if (include_excluded) {
                    return this.real_count == this.real_total;
                } else {
                    return this.value_count == this.value_total;
                }
            },
            getPercentage: function(include_excluded) {
                if (include_excluded) {
                    return instance.real_count / instance.real_total;
                } else {
                    return instance.value_count / instance.value_total;
                }
            },
            values: function(include_excluded) {
                return reduceToArray(this.check_state, function(acc, k, v) {
                    var not_excluded = include_excluded || !contains(instance.value_excluded, k);
                    if (v && not_excluded) acc.push(k);
                });
            },
            nonValues: function(include_excluded) {
                return reduceToArray(this.check_state, function(acc, k, v) {
                    var not_excluded = include_excluded || !contains(instance.value_excluded, k);
                    if (!v && not_excluded) acc.push(k);
                });
            },

            // INTERNAL FUNCTIONS
            // ~~~~~~~~~~~~~~~~~~

            _toggleCheckValue: function(input_el, event, new_state) {
                var isAllToggle = input_el == instance.toggle_all;
                var check_val = input_el.getAttribute('data-value');

                if (!event && !new_state)
                    new_state = !input_el.checked;
                else if (!new_state)
                    new_state = input_el.checked;
                if (!event && input_el.checked === new_state)
                    return;

                var beforeChangeEventResult = instance._events.call('before_change',
                    [check_val, new_state, isAllToggle]);

                // if before_change returns false, then the toggle is cancelled
                if (beforeChangeEventResult === false) {
                    if (event) {
                        event.stopPropagation();
                        event.preventDefault();
                    }
                    return;
                }

                if (!isAllToggle) {
                    instance.check_state[check_val] = new_state;

                    instance.real_count += new_state ? 1 : -1;

                    if (!contains(instance.value_excluded, check_val)) {
                        instance.value_count += new_state ? 1 : -1;
                    }

                    if (instance.toggle_all) {
                        instance.toggle_all.checked = (instance.value_count == instance.value_total);
                    }
                } else {
                    new_state = instance.value_count !== instance.value_total;

                    var value_count_excluded = 0;

                    forEach(instance.check_inputs, function(element) {
                        if (element == instance.toggle_all)
                            return;

                        var value = element.getAttribute('data-value');

                        if (contains(instance.value_excluded, value)) {
                            value_count_excluded += element.checked ? 1 : 0;
                            return;
                        }

                        element.checked = new_state;
                        instance.check_state[value] = new_state;
                    });

                    instance.value_count = new_state ? instance.value_total : 0;
                    instance.real_count = instance.value_count + value_count_excluded;
                }

                instance._events.call('on_change', [check_val, new_state, isAllToggle]);
            },

            _init: function() {
                forEach(instance.check_inputs, function(element) {
                    if (element == instance.toggle_all) return;

                    var value = element.getAttribute('data-value');
                    var bit = element.checked ? 1 : 0;

                    if (typeof element.checked === 'undefined' || !value)
                        throw "Invalid app.forms.Filter declaration";

                    element.setAttribute('data-state', instance.state_name);
                    instance.check_state[value] = !!bit;

                    if (!contains(instance.value_excluded, value)) {
                        instance.value_count += bit;
                        instance.value_total += 1;
                    }

                    instance.real_count += bit;
                    instance.real_total += 1;
                });

                n.on(document.body, 'click', function(event) {
                    if (typeof event.target.checked === 'undefined')
                        return;
                    var state_name = event.target.getAttribute('data-state');
                    if (!state_name || state_name != instance.state_name)
                        return;
                    instance._toggleCheckValue(event.target, event);
                });

                if (instance.toggle_all) {
                    instance.toggle_all.checked = (instance.value_total === instance.value_count);
                    n.on(instance.toggle_all, 'click', function(event) {
                        instance._toggleCheckValue(this, event);
                    });
                }
            },
        };

        _STATE[name]._init();
    }
})();