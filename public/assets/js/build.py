#!python3
import os
from jsmin import jsmin

src_dir   = os.path.dirname(os.path.abspath(__file__)) + '/src/'
dist_file = os.path.dirname(os.path.abspath(__file__)) + '/ProductionClient.js'
util_dist_file = os.path.dirname(os.path.abspath(__file__)) + '/ProductionUtil.js'

files = [
    'prototypes.js',
    'globals/array.js',
    'globals/browser.js',
    'globals/datetime.js',
    'globals/general.js',
    'globals/object.js',
    'globals/string.js',
    'globals/utility.js',
    'dom/n.js',
    'dom/gen.js',
    'app/init.js',
    'app/util.js',
    'app/FilterForm.js',
    'app/requests.js',
    'app/dialog.js',
    'app/localsettings.js',
    'store/index.js',
]

util_exclude = [
    'app/util.js',
    'app/FilterForm.js',
    'app/dialog.js',
    'app/localsettings.js',
    'store/index.js',
]

if __name__ == '__main__':
    minified = ''
    util_minified = ''

    for f in files:
        with open(src_dir + f) as js_file:
            jsText =  jsmin(js_file.read()) + ';'

            minified += jsText

            if f not in util_exclude:
                util_minified += jsText

    with open(dist_file, 'r+') as outfile:
        outfile.seek(0)
        outfile.write(minified)
        outfile.truncate()
        print('ProductionClient Done')

    with open(util_dist_file, 'r+') as outfile:
        outfile.seek(0)
        outfile.write(util_minified)
        outfile.truncate()
        print('ProductionUtil Done')