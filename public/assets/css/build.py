#!python3
import os
from csscompressor import compress

src_dir     = os.path.dirname(os.path.abspath(__file__)) + '/src/'
src_ui_dir  = os.path.dirname(os.path.abspath(__file__)) + '/src/ui/'
dist_file   = os.path.dirname(os.path.abspath(__file__)) + '/ProductionClient.css'

files = [
    'ColorTheme.css',
    'SimpleReset.css',
    'Base.css',
    'Helpers.css',
    'Sidebar.css',
    'AppTopNav.css',
    'AppAside.css',
    'AppDialog.css',
    *['ui/' + f for f in os.listdir(src_ui_dir) if os.path.isfile(os.path.join(src_ui_dir, f))],
    'misc.css',
]

if __name__ == '__main__':
    minified = ''

    for f in files:
        with open(src_dir + f) as css_file:
            minified += compress(css_file.read())

    with open(dist_file, 'r+') as outfile:
        outfile.seek(0)
        outfile.write(minified)
        outfile.truncate()
        print('Done')