<?php

ini_set('xdebug.var_display_max_depth', -1);
ini_set('xdebug.var_display_max_children', -1);
ini_set('xdebug.var_display_max_data', -1);

if (function_exists('xdebug_disable')) {
    xdebug_disable();
}

set_include_path(__DIR__);

require 'config.php';

$GLOBALS['__application_test_mode'] = true;

$_SERVER['REQUEST_METHOD'] = 'GET';
$_SERVER['REQUEST_URI'] = '/';

require(BOOTSTRAP_ROOT . 'bootstrap.php');

require('tests/BaseTest.php');