# layer7.solutions

This is the codebase that powers [layer7.solutions](https://layer7.solutions/).

If you have any questions about this website or TheSentinelBot or any of our other bots,
please stop by [/r/Layer7](https://www.reddit.com/r/Layer7/). Any notices of major changes
will also be posted to this subreddit.

## Got a question or problem?

If you have a question on how to do something on our website or any other general question,
please ask this question on our subreddit: [/r/Layer7](https://www.reddit.com/r/Layer7/)

## Found an issue or bug?

If you find a bug, you can help us by [submitting an issue](https://bitbucket.org/layer7solutions/wwwroot/issues/new)
or better yet, if you can, submitting a [Pull Request](https://bitbucket.org/layer7solutions/wwwroot/pull-requests/new) with a fix.

## Want a feature?

You can request a feature by [submitting a post on our subreddit](https://www.reddit.com/r/layer7/submit?selftext=true&title=[Feature]%20briefly%20describe%20feature%20here...)

## Testing

We use [Phan](https://github.com/phan/phan) and [PHPUnit](https://phpunit.de/) to test our program.

 - Run `./phan` to run static analysis tests.

 - Run `./test` to run all PHPUnit tests